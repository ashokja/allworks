function [sol_seeds, goodSeeds,solAtSeeds] = zerosAtProjected(n1,n2,n3,coeff,seedNum)
    % %  TO DO % %
    % Need to fix multiNewton to not leave the interval (-1,1)%
    % %
    switch nargin
        case 2, % mapxy and mapxyV
            mapxy= n1; mapxyV = n2;
            n1 = 4; n2 = 4; n3 =4 ;
            coeff = buildCoeff(n1,n2,n3,mapxy,mapxyV);
            [uCoeffRed,uCoeffRot] = divideVel(n1,n2,n3,coeff,mapxy);
            seedNum = 4;
            coeff = uCoeffRot;
        case 4,
            seedNum =4;
    end
    
    rand('seed',417);
    seeds = rand(seedNum,2)*2-1;
    sol_seeds = zeros(seedNum,6,3);
    solAtSeeds = zeros(seedNum,6,3);
    goodSeeds = zeros(seedNum,6);
    
    
    for i = 1:seedNum
        %face1
        [X,Y,Z] = meshgrid(-1,(-1:.1:1),(-1:.1:1));
        y2d = PlotFaceOnGrid(X,Y,Z,n1,n2,n3,coeff,1,2,3,seeds(i,:));
        sol_seeds(i,1,:) = [-1,y2d(1),y2d(2)];
        %face2
        [X,Y,Z] = meshgrid(1,(-1:.1:1),(-1:.1:1));
        y2d = PlotFaceOnGrid(X,Y,Z,n1,n2,n3,coeff,2,2,3,seeds(i,:));
        sol_seeds(i,2,:) = [1,y2d(1),y2d(2)];
        %face3
        [X,Y,Z] = meshgrid((-1:.2:1),-1,(-1:.2:1));
        y2d = PlotFaceOnGrid(X,Y,Z,n1,n2,n3,coeff,3,1,3,seeds(i,:));
        sol_seeds(i,3,:) = [y2d(1),-1,y2d(2)];
        %face4
        [X,Y,Z] = meshgrid((-1:.2:1),+1,(-1:.2:1));
        y2d = PlotFaceOnGrid(X,Y,Z,n1,n2,n3,coeff,4,1,3,seeds(i,:));
        sol_seeds(i,4,:) = [y2d(1),1,y2d(2)];
        %face5
        [X,Y,Z] = meshgrid((-1:.1:1),(-1:.1:1),-1);
        y2d= PlotFaceOnGrid(X,Y,Z,n1,n2,n3,coeff,5,1,2,seeds(i,:));
        sol_seeds(i,5,:) = [y2d(1),y2d(2),-1];
        %face6
        [X,Y,Z] = meshgrid((-1:.1:1),(-1:.1:1),+1);
        y2d = PlotFaceOnGrid(X,Y,Z,n1,n2,n3,coeff,6,1,2,seeds(i,:));
        sol_seeds(i,6,:) = [y2d(1),y2d(2),1];
    end
  
    for i = 1:seedNum
        for j = 1:6
            solAtSeeds(i,j,:) = B3_eval(n1,n2,n3,coeff,sol_seeds(i,j,:));
            val = squeeze(solAtSeeds(i,j,:));
            y = squeeze(sol_seeds(i,j,:));
%            if (norm(abs(val)) < 10^(-1)) && (sum(abs(y)>1) <1)
            if ( (sum(abs(y)>1) <1))
                goodSeeds(i,j) = true;
            else
                goodSeeds(i,j) = false;
            end
        end
    end
    
    
    function y = PlotFaceOnGrid( X,Y,Z,n1,n2,n3,coeff,c,p1,p2,x)
        %face1;
        dataInF(:,1) = reshape(X,[],1);
        dataInF(:,2) = reshape(Y,[],1);
        dataInF(:,3) = reshape(Z,[],1);
        dataOutF = B3_eval(n1,n2,n3,coeff,dataInF);
        n = [n1,n2,n3];
        dataInF2D(:,1) = dataInF(:,p1); dataInF2D(:,2) = dataInF(:,p2);
        dataOutF2D(:,1) = dataOutF(:,p1); dataOutF2D(:,2) = dataOutF(:,p2);
        coeff2D = buildCoeff2D(n(p1),n(p2),dataInF2D,dataOutF2D);
        y = MultiNewtonBasis2D(n(p1), n(p2), coeff2D,x,20);
        
        %figure(c),
        subplot(2,3,c),
        quiver(dataInF(:,p1),dataInF(:,p2),dataOutF(:,p1),dataOutF(:,p2)); hold on;
        subplot(2,3,c),
        plot(x(1),x(2),'r*'); hold on;
        if( sum(abs(y) >1) < 1 )
           subplot(2,3,c),
           plot(y(1), y(2),'g*'); hold on;
        end
        
        
        
    end

    function x=MultiNewtonBasis2D(n1,n2,coeff,x,NumIters)
    %Performs multidimensional Newton's method for the function defined in f
    %starting with x and running NumIters times.
        y = B2_eval(n1,n2,coeff,x);
        ko = B2_der_eval(n1,n2,coeff,x);
        dy = reshape(ko,2,2);
        %[res1,res2] = f_der(
        %dy = ;
        for j=1:NumIters
            s=pinv(dy')*y';
            x=x-0.25*s';
            y = B2_eval(n1,n2,coeff,x);
            dy = reshape(B2_der_eval(n1,n2,coeff,x),2,2);
        end 
        if norm(abs(y)) > 10^(-1)
            x = [2,-2];
        end
    end    


    function x=MultiNewtonBasis(n1,n2,n3,coeff,x,NumIters,val,id)
    %Performs multidimensional Newton's method for the function defined in f
    %starting with x and running NumIters times.
        y = B3_eval(n1,n2,n3,coeff,x);
        ko = B3_der_eval(n1,n2,n3,coeff,x);
        dy = reshape(ko,3,3);
        %[res1,res2] = f_der(
        %dy = ;
        for j=1:NumIters
            s=pinv(dy')*y';
            x=x-s';
            x(id) = val;
            y = B3_eval(n1,n2,n3,coeff,x);
            dy = reshape(B3_der_eval(n1,n2,n3,coeff,x),3,3);
        end 
    end

    

    function [rest] =  B2(n1,k1,n2,k2,dataIn)
        v1 = B(n1,k1,dataIn(:,1));
        v2 = B(n2,k2,dataIn(:,2));
        rest = v1.*v2;
    end
    
    function [rest] = B3(n1,k1,n2,k2,n3,k3,dataIn)
        v1 = B(n1,k1,dataIn(:,1));
        v2 = B(n2,k2,dataIn(:,2));
        v3 = B(n3,k3,dataIn(:,3));
        rest = v1.*v2.*v3;    
    end
    
    
    function [rest] = B2_der(n1,k1,n2,k2,dataIn)
        v1 =     B(n1,k1,dataIn(:,1));
        v1_der = B_der(n1,k1,dataIn(:,1));
        v2 =     B(n2,k2,dataIn(:,2));
        v2_der = B_der(n2,k2,dataIn(:,2));
        
        rest(:,1) = v1_der.*v2;
        rest(:,2) = v1.*v2_der;
    end

    function [rest] = B3_der(n1,k1,n2,k2,n3,k3,dataIn)
        v1 =     B(n1,k1,dataIn(:,1));
        v1_der = B_der(n1,k1,dataIn(:,1));
        v2 =     B(n2,k2,dataIn(:,2));
        v2_der = B_der(n2,k2,dataIn(:,2));
        v3 =     B(n3,k3,dataIn(:,3));
        v3_der = B_der(n3,k3,dataIn(:,3));
        rest(:,1) = v1_der.*v2.*v3;
        rest(:,2) = v1.*v2_der.*v3;
        rest(:,3) = v1.*v2.*v3_der;
    end

    function [rest] = B2_eval(n1,n2,coeff,dataIn)
       count =1;
       rest = zeros(size(dataIn));
       for k1 = 0:n1
           for k2 = 0:n2
               rest(:,1) = rest(:,1)  + coeff(count,1)*B2(n1,k1,n2,k2,dataIn);
               rest(:,2) = rest(:,2)  + coeff(count,2)*B2(n1,k1,n2,k2,dataIn);
               count= count +1;
           end
       end
    end

    function [rest] = B2_eval_3(n1,n2,coeff,dataIn)
       count =1;
       [a,b] = size(dataIn);
       rest = zeros(a,b+1);
       for k1 = 0:n1
           for k2 = 0:n2
               rest(:,1) = rest(:,1)  + coeff(count,1)*B2(n1,k1,n2,k2,dataIn);
               rest(:,2) = rest(:,2)  + coeff(count,2)*B2(n1,k1,n2,k2,dataIn);
               rest(:,3) = rest(:,3)  + coeff(count,3)*B2(n1,k1,n2,k2,dataIn);
               count= count +1;
           end
       end
    end

    function [rest] = B3_eval(n1,n2,n3,coeff,dataIn)
       count =1;
       rest = zeros(size(dataIn));
       for k1 = 0:n1
           for k2 = 0:n2
               for k3= 0:n3
                   rest(:,1) = rest(:,1)  + coeff(count,1)*B3(n1,k1,n2,k2,n3,k3,dataIn);
                   rest(:,2) = rest(:,2)  + coeff(count,2)*B3(n1,k1,n2,k2,n3,k3,dataIn);
                   rest(:,3) = rest(:,3)  + coeff(count,3)*B3(n1,k1,n2,k2,n3,k3,dataIn);
                   count= count +1;
               end
           end
       end
    end

    function [rest] = B3_der_eval(n1,n2,n3,coeff,dataIn)
        count = 1;
       [a,b] = size(dataIn);
       rest = zeros(a,b+6);
       for k1 = 0:n1
           for k2 = 0:n2
               for k3 = 0:n3
                    rest(:,1:3) = rest(:,1:3)  + coeff(count,1)*B3_der(n1,k1,n2,k2,n3,k3,dataIn);
                    rest(:,4:6) = rest(:,4:6)  + coeff(count,2)*B3_der(n1,k1,n2,k2,n3,k3,dataIn);
                    rest(:,7:9) = rest(:,7:9)  + coeff(count,3)*B3_der(n1,k1,n2,k2,n3,k3,dataIn);
                    count= count +1;
               end
           end
       end        
    end

    function [rest] = B2_der_eval(n1,n2,coeff,dataIn)
       count =1;
       [a,b] = size(dataIn);
       rest = zeros(a,b+2);
       for k1 = 0:n1
           for k2 = 0:n2
               rest(:,1:2) = rest(:,1:2)  + coeff(count,1)*B2_der(n1,k1,n2,k2,dataIn);
               rest(:,3:4) = rest(:,3:4)  + coeff(count,2)*B2_der(n1,k1,n2,k2,dataIn);
               count= count +1;
           end
       end
    end

    function [res] = B(n,k,data)
        % assumed data varies from -1 to 1
        if n<k || k<0
            res = zeros(size(data));
            return
        end
        data = (data+1)/2;
        res = nchoosek(n,k)*((data.^k).*((1-data).^(n-k)));
    end

    function [res] = B_der(n,k,data)
        res = n*(B(n-1,k-1,data) - B(n-1,k,data))/2;
    end
end