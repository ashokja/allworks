#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "OneSidedSIAC.h"
#include "SmoothieSIAC1D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh1D.h"
#include <omp.h>
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

 
int main(int argc, char* argv[])
{
	if ( argc != 3 )
	{
		cout << "1st order			" << endl;
		cout << "2nd kernel shift	" << endl;
		cout << "3rd derivative		" << endl;
	}

	// Without derivative
	OneSidedSIAC oneSIAC( atoi(argv[1]), OneSidedSIAC::OneSidedFilterType::BASIC_SIAC_2kp1);
	oneSIAC.EvaluateCoefficients(atof(argv[2]));
	oneSIAC.printNekArray( oneSIAC.m_coeffs,0);

	// With derivative
		//OneSidedSIAC oneSIAC1( atoi(argv[1]), OneSidedSIAC::OneSidedFilterType::N_Der_SMOOTH_BASIC_SIAC_2kp1,atoi(argv[3]));
	OneSidedSIAC oneSIAC1( atoi(argv[1]), OneSidedSIAC::OneSidedFilterType::Der_SMOOTH_BASIC_SIAC_2kp1,atoi(argv[3]));
	oneSIAC1.EvaluateCoefficients(atof(argv[2]));
	oneSIAC1.printNekArray( oneSIAC1.m_coeffs,0);

	int parts = 801;
	Array<OneD,NekDouble> x_pos(parts), v_pos(parts), v_pos1(parts), v_pos2(parts), v_pos3(parts);

	for (int i =0; i < parts; i++)
	{
		x_pos[i] =  -4.0+ ( (NekDouble)i/(NekDouble)(parts-1.0))*8.0;
	}

	double startTime = omp_get_wtime();
	for(int i =0; i < 10000; i++)
	{
		oneSIAC.EvaluateFilter(x_pos, v_pos, 1, atof(argv[2]) );
	}
	double timeTaken = omp_get_wtime()-startTime;
	std::cout << "Time units taken\t:" << timeTaken <<endl;

	cout << "test number of threads:";
	int numThreads;
	cin >> numThreads;
	omp_set_num_threads(numThreads);
	double startTime_par = omp_get_wtime();
	#pragma omp parallel for
	for(int i =0; i < 10000; i++)
	{
		oneSIAC.EvaluateFilter(x_pos, v_pos, 1, atof(argv[2]) );
	}
	double timeTaken_par = omp_get_wtime()-startTime_par;
	std::cout << "Time openmp taken\t:" << timeTaken_par <<endl;

	oneSIAC.writeNekArray(x_pos,"xpos.txt");
	oneSIAC.writeNekArray(v_pos,"vpos.txt");
	
	oneSIAC1.EvaluateFilter(x_pos, v_pos1, 1, atof(argv[2]) );
	oneSIAC1.writeNekArray(v_pos1,"vpos1.txt");

	return 0;
}
