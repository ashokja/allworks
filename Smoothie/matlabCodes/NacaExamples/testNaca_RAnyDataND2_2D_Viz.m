% Read data from testNaca_RAnyResD2
% s is used in variables but it ss data.

clear;
close all;


UsebkupData = 'cur_feb25/'; % has only data Ux_ss so remember to comment below also.
UsebkupDataD = 'cur_feb25/'; % has only data Ux_ss so remember to comment below also.
%UsebkupDataD = 'cur_feb13/' % has only data Ux_ss so remember to comment below also.
UsebkupDataC = 'cur_feb12/';

%UsebkupData = 'cur_feb10/'

%UsebkupData = ''
%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05';
filename = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_Any2LSIACVARND2' );
filenameD = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_Any2LSIACVARN' );
%filenameD = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_Any2LSIACVAR' );
filenameC = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ACRFDNVar' );

filepath = strcat('data/',UsebkupData,filename);
filepathD = strcat('data/',UsebkupDataD,filenameD);
filepathC = strcat('data/',UsebkupDataC,filenameC);

xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );

usfname = strcat(filepathC,'_pU_ss.txt' );
vsfname = strcat(filepathC,'_pV_ss.txt' );
wsfname = strcat(filepathC,'_pW_ss.txt' );

uxsfname = strcat(filepathD, '_pUx_ss.txt');
uysfname = strcat(filepathD, '_pUy_ss.txt');
uzsfname = strcat(filepathD, '_pUz_ss.txt');

vxsfname = strcat(filepathD, '_pVx_ss.txt');
vysfname = strcat(filepathD, '_pVy_ss.txt');
vzsfname = strcat(filepathD, '_pVz_ss.txt');

wxsfname = strcat(filepathD, '_pWx_ss.txt');
wysfname = strcat(filepathD, '_pWy_ss.txt');
wzsfname = strcat(filepathD, '_pWz_ss.txt');


uxxsfname = strcat(filepath, '_pUxx_ss.txt');
uxysfname = strcat(filepath, '_pUxy_ss.txt');
uxzsfname = strcat(filepath, '_pUxz_ss.txt');

uyxsfname = strcat(filepath, '_pUyx_ss.txt');
uyysfname = strcat(filepath, '_pUyy_ss.txt');
uyzsfname = strcat(filepath, '_pUyz_ss.txt');

uzxsfname = strcat(filepath, '_pUzx_ss.txt');
uzysfname = strcat(filepath, '_pUzy_ss.txt');
uzzsfname = strcat(filepath, '_pUzz_ss.txt');

vxxsfname = strcat(filepath, '_pVxx_ss.txt');
vxysfname = strcat(filepath, '_pVxy_ss.txt');
vxzsfname = strcat(filepath, '_pVxz_ss.txt');

vyxsfname = strcat(filepath, '_pVyx_ss.txt');
vyysfname = strcat(filepath, '_pVyy_ss.txt');
vyzsfname = strcat(filepath, '_pVyz_ss.txt');

vzxsfname = strcat(filepath, '_pVzx_ss.txt');
vzysfname = strcat(filepath, '_pVzy_ss.txt');
vzzsfname = strcat(filepath, '_pVzz_ss.txt');

wxxsfname = strcat(filepath, '_pWxx_ss.txt');
wxysfname = strcat(filepath, '_pWxy_ss.txt');
wxzsfname = strcat(filepath, '_pWxz_ss.txt');

wyxsfname = strcat(filepath, '_pWyx_ss.txt');
wyysfname = strcat(filepath, '_pWyy_ss.txt');
wyzsfname = strcat(filepath, '_pWyz_ss.txt');

wzxsfname = strcat(filepath, '_pWzx_ss.txt');
wzysfname = strcat(filepath, '_pWzy_ss.txt');
wzzsfname = strcat(filepath, '_pWzz_ss.txt');



xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');

uspos = textread(usfname, '%f\t');
vspos = textread(vsfname, '%f\t');
wspos = textread(wsfname, '%f\t');

uxspos = textread( uxsfname,'%f\t');
uyspos = textread( uysfname,'%f\t');
uzspos = textread( uzsfname,'%f\t');
vxspos = textread( vxsfname,'%f\t');
vyspos = textread( vysfname,'%f\t');
vzspos = textread( vzsfname,'%f\t');
wxspos = textread( wxsfname,'%f\t');
wyspos = textread( wysfname,'%f\t');
wzspos = textread( wzsfname,'%f\t');


uxxspos = textread( uxxsfname,'%f\t');
uxyspos = textread( uxysfname,'%f\t');
uxzspos = textread( uxzsfname,'%f\t');
uyxspos = textread( uyxsfname,'%f\t');
uyyspos = textread( uyysfname,'%f\t');
uyzspos = textread( uyzsfname,'%f\t');
uzxspos = textread( uzxsfname,'%f\t');
uzyspos = textread( uzysfname,'%f\t');
uzzspos = textread( uzzsfname,'%f\t');

vxxspos = textread( vxxsfname,'%f\t');
vxyspos = textread( vxysfname,'%f\t');
vxzspos = textread( vxzsfname,'%f\t');
vyxspos = textread( vyxsfname,'%f\t');
vyyspos = textread( vyysfname,'%f\t');
vyzspos = textread( vyzsfname,'%f\t');
vzxspos = textread( vzxsfname,'%f\t');
vzyspos = textread( vzysfname,'%f\t');
vzzspos = textread( vzzsfname,'%f\t');

wxxspos = textread( wxxsfname,'%f\t');
wxyspos = textread( wxysfname,'%f\t');
wxzspos = textread( wxzsfname,'%f\t');
wyxspos = textread( wyxsfname,'%f\t');
wyyspos = textread( wyysfname,'%f\t');
wyzspos = textread( wyzsfname,'%f\t');
wzxspos = textread( wzxsfname,'%f\t');
wzyspos = textread( wzysfname,'%f\t');
wzzspos = textread( wzzsfname,'%f\t');


epos = floor(epos +0.0001);
s = floor( (max(size(xpos)))^(1/2) +0.1);


xVpos = reshape(xpos,s,s);
yVpos = reshape(ypos,s,s);
zVpos = reshape(zpos,s,s);

usVpos = reshape(uspos,s,s);  vsVpos = reshape(vspos,s,s);  wsVpos = reshape(wspos,s,s);

uxxsVpos = reshape(uxxspos,s,s);
uxysVpos = reshape(uxyspos,s,s);
uxzsVpos = reshape(uxzspos,s,s);
uyxsVpos = reshape(uyxspos,s,s);
uyysVpos = reshape(uyyspos,s,s);
uyzsVpos = reshape(uyzspos,s,s);
uzxsVpos = reshape(uzxspos,s,s);
uzysVpos = reshape(uzyspos,s,s);
uzzsVpos = reshape(uzzspos,s,s);

vxxsVpos = reshape(vxxspos,s,s);
vxysVpos = reshape(vxyspos,s,s);
vxzsVpos = reshape(vxzspos,s,s);
vyxsVpos = reshape(vyxspos,s,s);
vyysVpos = reshape(vyyspos,s,s);
vyzsVpos = reshape(vyzspos,s,s);
vzxsVpos = reshape(vzxspos,s,s);
vzysVpos = reshape(vzyspos,s,s);
vzzsVpos = reshape(vzzspos,s,s);

wxxsVpos = reshape(wxxspos,s,s);
wxysVpos = reshape(wxyspos,s,s);
wxzsVpos = reshape(wxzspos,s,s);
wyxsVpos = reshape(wyxspos,s,s);
wyysVpos = reshape(wyyspos,s,s);
wyzsVpos = reshape(wyzspos,s,s);
wzxsVpos = reshape(wzxspos,s,s);
wzysVpos = reshape(wzyspos,s,s);
wzzsVpos = reshape(wzzspos,s,s);


% calculate acceleration.
auspos = uxspos.*uspos + uyspos.*vspos + uzspos.*wspos;
avspos = vxspos.*uspos + vyspos.*vspos + vzspos.*wspos;
awspos = wxspos.*uspos + wyspos.*vspos + wzspos.*wspos;
 %
% veclocity cross acceleration. 
cuspos = vspos.*awspos - wspos.*avspos;
cvspos = wspos.*auspos - uspos.*awspos;
cwspos = uspos.*avspos - vspos.*auspos;

curvature_pos = (cuspos.^(2) + cvspos.^(2) + cwspos.^(2)).^(1/2);
curvature_Vpos = reshape(curvature_pos,s,s);

figure(10)
surf(yVpos,zVpos,curvature_Vpos);
shading interp;

auxspos = (uxxspos + uyxspos+uzxspos).*uspos + (uxspos+uyspos+uzspos).*uxspos;
auyspos = (uxyspos + uyyspos+uzyspos).*uspos + (uxspos+uyspos+uzspos).*uyspos;
auzspos = (uxzspos + uyzspos+uzzspos).*uspos + (uxspos+uyspos+uzspos).*uzspos;

avxspos = (vxxspos + vyxspos+vzxspos).*vspos + (vxspos+vyspos+vzspos).*vxspos;
avyspos = (vxyspos + vyyspos+vzyspos).*vspos + (vxspos+vyspos+vzspos).*vyspos;
avzspos = (vxzspos + vyzspos+vzzspos).*vspos + (vxspos+vyspos+vzspos).*vzspos;

awxspos = (wxxspos + wyxspos+wzxspos).*wspos + (wxspos+wyspos+wzspos).*wxspos;
awyspos = (wxyspos + wyyspos+wzyspos).*wspos + (wxspos+wyspos+wzspos).*wyspos;
awzspos = (wxzspos + wyzspos+wzzspos).*wspos + (wxspos+wyspos+wzspos).*wzspos;

% calculate b.
buspos = auxspos.*uspos + auyspos.*vspos + auzspos.*wspos;
bvspos = avxspos.*uspos + avyspos.*vspos + avzspos.*wspos;
bwspos = awxspos.*uspos + awyspos.*vspos + awzspos.*wspos;

% torsion = (velocity cross a).b ;
torsion_pos = cuspos.*buspos + cvspos.*bvspos + cwspos.*bwspos;
torsion_Vpos = reshape(torsion_pos,s,s);


% vdotb
velnorm = (uspos.*uspos + vspos.*vspos + wspos.*wspos).^(1/2);
usposN = uspos./velnorm;
vsposN = vspos./velnorm;
wsposN = wspos./velnorm;
bnorm = (buspos.*buspos + bvspos.*bvspos + bwspos.*bwspos).^(1/2);
busposN = buspos./bnorm;
bvsposN = bvspos./bnorm;
bwsposN = bwspos./bnorm;
vDotb_pos = usposN.*busposN + vsposN.*bvsposN + wsposN.*bwsposN;
vDotb_Vpos = reshape(vDotb_pos,s,s);




%%
figure(11)
surf(yVpos,zVpos,torsion_Vpos); hold on;
shading interp;
view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.92 ]),colorbar();
title('torsion');
%print('images/Feb22/torsionN_3dirLSIAC_surf','-dpng', '-r500')



%%
figure(12)
contour3(yVpos,zVpos,torsion_Vpos,[-1,0,1],'LineColor','k'); hold on;
surf(yVpos,zVpos,torsion_Vpos), shading interp;
view(0,90);
colorbar();
title('torison with [-1 0 1] contours')
%print('images/Feb22/torsionN_3dirLSIAC_contour','-dpng', '-r500')



%%
figure(13)
surf(yVpos,zVpos,vDotb_Vpos); hold on;
shading interp;

figure(14)
contour(yVpos,zVpos,vDotb_Vpos); hold on;
shading interp;
%%

figure(15)
surf(yVpos,zVpos,torsion_Vpos,abs(vDotb_Vpos)); hold on;
contour(yVpos,zVpos,torsion_Vpos,[-0.1 0 0.1]); hold on;
shading interp;
title('torison');
view(0,90);
colorbar();

%%
figure(26),
contour(yVpos,zVpos,torsion_Vpos,[ 0 1]); hold on;
contour(yVpos,zVpos,abs(vDotb_Vpos),linspace(0.4,0.8,10)); hold on;
grid on; grid minor;
%print('images/Feb22/torsionN_3dirLSIAC_sol','-dpng', '-r500')

figure(27),
contour(yVpos,zVpos,curvature_Vpos,linspace(0,4,10)); hold on;
grid on; grid minor;
%print('images/Feb22/curvatureN_3dirLSIAC_sol','-dpng', '-r500')


%%
startx = min(yVpos(:)):0.01:max(yVpos(:));
starty = min(zVpos(:)):0.01:max(zVpos(:));
figure(28),
quiver(yVpos,zVpos,vsVpos,wsVpos); hold on;
streamline(yVpos,zVpos,vsVpos,wsVpos,startx,starty); hold on;

% figure(1)
% subplot(3,3,1),contour(uxxsVpos);
% subplot(3,3,2),contour(uxysVpos);
% subplot(3,3,3),contour(uxzsVpos);
% subplot(3,3,4),contour(uyxsVpos);
% subplot(3,3,5),contour(uyysVpos);
% subplot(3,3,6),contour(uyzsVpos);
% subplot(3,3,7),contour(uzxsVpos);
% subplot(3,3,8),contour(uzysVpos);
% subplot(3,3,9),contour(uzzsVpos);
% 
% figure(2)
% subplot(3,3,1),contour(vxxsVpos);
% subplot(3,3,2),contour(vxysVpos);
% subplot(3,3,3),contour(vxzsVpos);
% subplot(3,3,4),contour(vyxsVpos);
% subplot(3,3,5),contour(vyysVpos);
% subplot(3,3,6),contour(vyzsVpos);
% subplot(3,3,7),contour(vzxsVpos);
% subplot(3,3,8),contour(vzysVpos);
% subplot(3,3,9),contour(vzzsVpos);
% 
% 
% figure(3)
% subplot(3,3,1),contour(wxxsVpos);
% subplot(3,3,2),contour(wxysVpos);
% subplot(3,3,3),contour(wxzsVpos);
% subplot(3,3,4),contour(wyxsVpos);
% subplot(3,3,5),contour(wyysVpos);
% subplot(3,3,6),contour(wyzsVpos);
% subplot(3,3,7),contour(wzxsVpos);
% subplot(3,3,8),contour(wzysVpos);
% subplot(3,3,9),contour(wzzsVpos);
% 
