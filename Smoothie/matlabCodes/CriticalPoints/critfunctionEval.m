%function [y dy]=critfunctionEval(x)
function [val, dval,ret]=critfunctionEval(x,y,xV,yV,uV,vV,uxV,uyV,vxV,vyV,latticeGrid)

%Example function to try out Newton?s Method
    %
%     n=length(x);
%     y=zeros(size(x)); %Not necessary for a small vector
%     dy=zeros(n,n); %Not necessary for a small matrix
%     y(1)=-x(1)^3+x(2);
%     y(2)=x(1)^2+x(2)^2-1;
%     dy(1,1)=-3*x(1)^2; dy(1,2)=1;
%     dy(2,1)=2*x(1); dy(2,2)=2*x(2);
    
    dval = zeros(2,2);
    val = zeros(2,1);
    
    %check for out of bounds.
    
    minXlimit = min(xV(:))+1/(latticeGrid-1);
    maxXlimit = max(xV(:))-1/(latticeGrid-1);

    minYlimit = min(yV(:))+1/(latticeGrid-1);
    maxYlimit = max(yV(:))-1/(latticeGrid-1);
    ret = 1;
    if ( x < minXlimit || x>maxXlimit)
        disp(x);
        disp(y);
        disp('out x of bounds');
        ret =0;
        return;
    end
    
    if ( y < minYlimit || y>maxYlimit)
        disp(x);
        disp(y);
        disp('out y of bounds');
        ret =0;
        return
    end
    
    val(1) = interp2(xV,yV,uV,x,y);
    val(2) = interp2(xV,yV,vV,x,y);

    dval(1,1) = interp2(xV,yV,uxV,x,y);
    dval(1,2) = interp2(xV,yV,uyV,x,y);
    dval(2,1) = interp2(xV,yV,vxV,x,y);
    dval(2,2) = interp2(xV,yV,vyV,x,y);

end