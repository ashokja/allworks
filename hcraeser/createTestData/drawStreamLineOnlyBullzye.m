function [] = drawStreamLineOnlyBullzye(mapxy,mapxyV,coeff)
    switch nargin
        case 3,
            sampledata =6;
            curveSampledata = 20;
    end
    data = linspace(-1,1,sampledata);
    cdata = linspace(-1,1,curveSampledata);
    [a,~] = size(mapxy);
    b = int64(a^(1/3));
    x = reshape(mapxy(:,1),b,b,b);
    y = reshape(mapxy(:,2),b,b,b);
    z = reshape(mapxy(:,3),b,b,b);
    u = reshape(mapxyV(:,1),b,b,b);
    v = reshape(mapxyV(:,2),b,b,b);
    w = reshape(mapxyV(:,3),b,b,b);
    xmin = min(x(:)); xmax = max(x(:));
    ymin = min(y(:)); ymax = max(y(:));
    zmin = min(z(:)); ymax = max(z(:));
    
    
    zmin = min(z(:));

    wind_speed = sqrt(u.^2 + v.^2 + w.^2);
%     hsurfaces = slice(x,y,z,wind_speed,xmax,[ymin,0,ymax],zmin);
%     set(hsurfaces,'FaceColor','interp','EdgeColor','none')
%     colormap jet
%     hold on;
    
    xdata = B_eval(coeff(1,:),data);    cxdata = B_eval(coeff(1,:),cdata); 
    ydata = B_eval(coeff(2,:),data);    cydata = B_eval(coeff(2,:),cdata);
    zdata = B_eval(coeff(3,:),data);    czdata = B_eval(coeff(3,:),cdata);
    xderdata = B_der_eval(coeff(1,:),data);
    yderdata = B_der_eval(coeff(2,:),data);
    zderdata = B_der_eval(coeff(3,:),data);
    
    plot3(cxdata,cydata,czdata,'r'); hold on;
    plotBoundingBox();
%    quiver3(xdata,ydata,zdata,xderdata,yderdata,zderdata); hold on;
    xlabel('x'); ylabel('y'); zlabel('z');

    for i = 1:sampledata
        point = [xdata(i),ydata(i), zdata(i)];
        nNormal = [xderdata(i),yderdata(i), zderdata(i)];
        pNormal= [0,0,1];
        nNormal = nNormal/norm(nNormal);
        theta = acos(dot(nNormal,pNormal)/(norm(pNormal)*norm(nNormal)) );
        axis = cross(nNormal,pNormal);
        axis = axis/norm(axis);
        
        hsp = surf(point(1)+linspace(-0.25,0.25,20),point(2)+linspace(-0.25,0.25,20),...
            point(3)+zeros(20) );
        
        rotate(hsp,axis,-theta/pi*180,point);
        %rv = cross(v,[v(1),v(2),0])
        %rotate(hsp,rv,90-PHI*180/pi,[xls,yls,zls]);
        set(hsp,'FaceColor','interp','EdgeColor','none');
        xd = hsp.XData;
        yd = hsp.YData;
        zd = hsp.ZData;
        delete(hsp);
        hsurf = slice(x,y,z,wind_speed,xd,yd,zd);
        set(hsurf,'FaceColor','interp','EdgeColor','none');

    end

    
%     hcont = contourslice(x,y,z,wind_speed,xmax,[ymin,0,ymax],zmin);
%     set(hcont,'EdgeColor',[0.7 0.7 0.7],'LineWidth',0.5)
    
%     startx = (-1:.4:1);
%     startz = (-1:1:1);
%     starty = (-1:.4:1);
%     [sx,sy,sz] = meshgrid(startx,starty,startz);
%     colorbar;

%     hlines = streamline(x,y,z,u,v,w,sx(:),sy(:),sz(:));
%     set(hlines,'LineWidth',2,'Color','cyan');
    hold on;
%    plot3(sx(:),sy(:),sz(:),'r*'); hold on;
end