% test DG solution.

% Read xc0 and xc1 from file.
% Write u and v to file.
function [Res] = crit_el_debug(x0,filenam)
    %close all;
  %  x0= [0;0];
    %clear all;
     %if (nargin ==0)
     %    x0 = [0;0];
     %end
    a1 = 0.74+0.35i;
    a2 = 0.68-0.59i;
    a3 =-0.11-0.72i;
    b1 =-0.58+0.64i;
    b2 = 0.51-0.27i;
    b3 =-0.12+0.84i;


    rate = 2;
    filename = strcat('P',int2str(rate),'_quad_simple_41_DEBG_El');

    xfname = strcat('../data/NekMeshes/',filename,'_x.txt' );
    yfname = strcat('../data/NekMeshes/',filename,'_y.txt' );

    ufname = strcat('../data/NekMeshes/',filename,'_u.txt' );
    vfname = strcat('../data/NekMeshes/',filename,'_v.txt' );

    uxfname = strcat('../data/NekMeshes/',filename,'_ux.txt' );
    uyfname = strcat('../data/NekMeshes/',filename,'_uy.txt' );

    vxfname = strcat('../data/NekMeshes/',filename,'_vx.txt' );
    vyfname = strcat('../data/NekMeshes/',filename,'_vy.txt' );

    xv = textread(xfname,'%f\t');
    yv = textread(yfname,'%f\t');

    uv = textread(ufname,'%f\t');
    vv = textread(vfname,'%f\t');

    uxv = textread(uxfname,'%f\t');
    uyv = textread(uyfname,'%f\t');

    vxv = textread(vxfname,'%f\t');
    vyv = textread(vyfname,'%f\t');

    s = sqrt(max(size(xv)));

    xV = reshape(xv,[s,s]);
    yV = reshape(yv,[s,s]);

    uV = reshape(uv,[s,s]);
    vV = reshape(vv,[s,s]);

    uxV = reshape(uxv,[s,s]);
    uyV = reshape(uyv,[s,s]);

    vxV = reshape(vxv,[s,s]);
    vyV = reshape(vyv,[s,s]);
 %   [x0mg, y0mg] = meshgrid(x0(1),x0(2));
 
    u_inter = interp2(xV',yV',uV',x0(1),x0(2),'linear'); 
    v_inter = interp2(xV',yV',vV',x0(1),x0(2),'linear');
    surf(xV,yV,uV); hold on;
    surf(xV,yV,uxV); hold on;
    surf(xV,yV,uyV); hold on;
    surf(xV,yV,vxV); hold on;
    surf(xV,yV,vyV); hold on;
    xlabel('x'); ylabel('y'); zlabel('z')
    
    
    Res = [u_inter;v_inter];
end