% test DG solution.

% Read xc0 and xc1 from file.
% Write u and v to file.

close all;
clear all;
%  %field one;
field = 1;

if (field ==1)
a1 = 0.74+0.35i;
a2 = 0.68-0.59i;
a3 =-0.11-0.72i;
b1 =-0.58+0.64i;
b2 = 0.51-0.27i;
b3 =-0.12+0.84i;
end

if (field ==2)
 %field two;
a1 = 0.74+0.35i;
a2 =-0.18-0.19i;
a3 =-0.11-0.72i;
b1 =-0.58+0.64i;
b2 = 0.51-0.27i;
%b3 =-0.12+0.84i;
end


rate = 2;
filename = strcat('P',int2str(rate),'_quad_simple_21');

xfname = strcat('../data/NekMeshes/',filename,'_xc0','.txt' );
yfname = strcat('../data/NekMeshes/',filename,'_xc1','.txt' );

SeedxDGfname = strcat('../data/NekMeshes/',filename,'_seedX_DG','.txt' );
SeedyDGfname = strcat('../data/NekMeshes/',filename,'_seedY_DG','.txt' );

SeeduDGfname = strcat('../data/NekMeshes/',filename,'_seedU_DG','.txt' );
SeedvDGfname = strcat('../data/NekMeshes/',filename,'_seedV_DG','.txt' );

xV = textread(xfname, '%f\t');
yV = textread(yfname, '%f\t');
x_DG = textread(SeedxDGfname, '%f\t');
y_DG = textread(SeedyDGfname, '%f\t');
u_DG = textread(SeeduDGfname, '%f\t');
v_DG = textread(SeedvDGfname, '%f\t');



xV = 2.0*xV-1;
yV = 2.0*yV-1;

zV = xV+yV*1i;
zVb = xV-yV*1i;

if field == 1
    Val = (zV-a1).*(zV-a2).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2;
end
if field ==2
    Val = (zV-a1).*(zV-a2).*(zV-a3).*(zVb-b1).*(zVb-b2);
end

u = real(Val); 
v = -1*imag(Val);



% figure(3)
% plot3(xV,yV, abs(u-u_DG));
% figure(4)
% plot3(xV,yV, abs(v-v_DG));
% 
% L2_norm = max((sum((u-u_DG).^2)))/max(size(u(:)));
% disp(L2_norm);

 n_uv = sqrt(u.*u+v.*v);
 u = u./n_uv;
 v = v./n_uv;
 
 figure(1),
 quiver(xV,yV,u,v);

 figure(2),
 plot3(x_DG,y_DG,abs(u_DG)+abs(v_DG),'*');
 hold on;
if field ==2
 plot3( real(a1),imag(a1),0.0 ,'-r*');
 plot3( real(a2),imag(conj(a2)),0.0 ,'-r*');
 plot3( real(a3),imag(a3),0.0 ,'-r*');
 plot3( real(b1),imag(conj(b1)),0.0 ,'-r*');
 plot3( real(b2),imag(conj(b2)),0.0 ,'-r*');
end

if field == 1
    plot3( real(a1),imag(a1),0.0 ,'-r*');
    plot3( real(a2),imag(a2),0.0 ,'-r*');
    plot3( real(a3),imag(a3),0.0 ,'-r*');
    plot3( real(b1),imag(conj(b1)),0.0 ,'-r*');
    plot3( real(b2),imag(conj(b2)),0.0 ,'-r*');
    plot3( real(b3),imag(conj(b3)),0.0 ,'-r*');
end
 xlim([-1,1]); ylim([-1,1])
 
 DGPos = x_DG+1i*y_DG;
 DGRes = u_DG+1i*v_DG;
 
 [a1Res,l] = min( DGPos- a1);
 ai = a1;
 aRes = a1Res;
 dgRes = DGRes(l);
 disp( strcat(  '$(', num2str(real(ai),'%1.2e'), ',',num2str(imag(ai),'%1.2e'),...
            ')$&$(', num2str(real(aRes),'%1.1e'), ',',num2str(imag(aRes),'%1.1e'), ')$&$('...
            ,num2str(real(dgRes),'%1.1e'),',', num2str(imag(dgRes),'%1.1e'),')$') );
 
 [a2Res,l] = min( DGPos- (a2));
 ai = conj(a2);
 aRes = a2Res;
 dgRes = DGRes(l);
 disp( strcat(  '$(', num2str(real(ai),'%1.2e'), ',',num2str(imag(ai),'%1.2e'),...
            ')$&$(', num2str(real(aRes),'%1.1e'), ',',num2str(imag(aRes),'%1.1e'), ')$&$('...
            ,num2str(real(dgRes),'%1.1e'),',', num2str(imag(dgRes),'%1.1e'),')$') );

 [a3Res,l] = min( DGPos- a3);
 ai = a3;
 aRes = a3Res;
 dgRes = DGRes(l);
 disp( strcat(  '$(', num2str(real(ai),'%1.2e'), ',',num2str(imag(ai),'%1.2e'),...
            ')$&$(', num2str(real(aRes),'%1.1e'), ',',num2str(imag(aRes),'%1.1e'), ')$&$('...
            ,num2str(real(dgRes),'%1.1e'),',', num2str(imag(dgRes),'%1.1e'),')$') );

 [b1Res,l] = min( DGPos- conj(b1));
 ai = conj(b1);
 aRes = b1Res;
 dgRes = DGRes(l);
 disp( strcat(  '$(', num2str(real(ai),'%1.2e'), ',',num2str(imag(ai),'%1.2e'),...
            ')$&$(', num2str(real(aRes),'%1.1e'), ',',num2str(imag(aRes),'%1.1e'), ')$&$('...
            ,num2str(real(dgRes),'%1.1e'),',', num2str(imag(dgRes),'%1.1e'),')$') );


 [b2Res,l] = min( DGPos- conj(b2));
 ai = conj(b2);
 aRes = b2Res;
 dgRes = DGRes(l);
 disp( strcat(  '$(', num2str(real(ai),'%1.2e'), ',',num2str(imag(ai),'%1.2e'),...
            ')$&$(', num2str(real(aRes),'%1.1e'), ',',num2str(imag(aRes),'%1.1e'), ')$&$('...
            ,num2str(real(dgRes),'%1.1e'),',', num2str(imag(dgRes),'%1.1e'),')$') );

 b3Res = norm(min( DGPos- conj(b3)))
 
 
 
 array(1) = norm (a1Res)
 array(2) = norm (a2Res) 
 array(3) = norm (a3Res)
 array(4) = norm (b1Res)
 array(5) = norm (b2Res)
 array(6) = norm (b3Res)
 
 % check for all  6 solutions.
 
 
 
 % 
%  n_uvDG = sqrt(u_DG.*u_DG+v_DG.*v_DG);
%  u_DG = u_DG./n_uvDG;
%  v_DG = v_DG./n_uvDG;
% 
%  
% figure(1),
% quiver(xV,yV,u,v);
% figure(2)
% quiver(xV,yV,u_DG,v_DG);

%dlmwrite(strcat('../data/NekMeshes/',filename,'_u.txt'),u','delimiter','\t','precision',25);
%dlmwrite(strcat('../data/NekMeshes/',filename,'_v.txt'),v','delimiter','\t','precision',25);

%plot3(xV,yV,u);