#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 6)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		cout << "4th arg is derivative" << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM2D->LoadMesh(var[0]);
	HNM2D->LoadMesh(var[1]);
	HNM2D->LoadMesh(var[2]);
	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1,fceDx,fceDy, eceDx, eceDy;
	fce = Array<OneD,NekDouble>(tNquadPts);
	fceDx = Array<OneD,NekDouble>(tNquadPts);
	fceDy = Array<OneD,NekDouble>(tNquadPts);
	eceDx = Array<OneD,NekDouble>(tNquadPts);
	eceDy = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);
//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);
/*
	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]));
		//fce[i] = std::cos(2.0*M_PI*(xc0[i]))*std::cos(2.0*M_PI*(xc1[i]));
	}
*/
   for (int i=0;  i< tNquadPts; i++)
    {   
		fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]));
        //`fce[i] = std::cos(2.0*M_PI*xc0[i]);
        if (2==atoi(argv[4]))
        {   
            fceDx[i] = -1.0*4.0*M_PI*M_PI*std::cos( 2.0*M_PI*(xc0[i]+ xc1[i]) );
            fceDy[i] = -1.0*4.0*M_PI*M_PI*std::cos( 2.0*M_PI*(xc0[i]+ xc1[i]) );
        }else
        {   
            fceDx[i] = -1.0*2.0*M_PI*std::sin( 2.0*M_PI*(xc0[i]+xc1[i]) );
            fceDy[i] = -1.0*2.0*M_PI*std::sin( 2.0*M_PI*(xc0[i]+xc1[i]) );
        }   
    }   


	
	HNM2D->m_expansions[0]->FwdTrans(fce,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	 ece = HNM2D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM2D->m_expansions[0]->GetCoeffs();

	HNM2D->m_expansions[0]->PhysDeriv(0,ece,eceDx);
	HNM2D->m_expansions[0]->PhysDeriv(1,ece,eceDy);

// Evaluate on a new equal space grid mesh.
	int gPts = atoi(argv[5]);
	int totPts = gPts*gPts;
	int Nx = gPts, Ny=gPts;
	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0);
	
	SmoothieSIAC2D sm(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]), atoi(argv[4])); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	Array<OneD,NekDouble> directionX(3,0.0), coord(3,0.0), directionY(3,0.0) ;
	directionX[0] = 1.0; directionY[1] =1.0;
	Array<OneD,NekDouble> pX(totPts), pY(totPts), pV(totPts), pP(totPts), pS(totPts);
	Array<OneD,NekDouble> pVDx(totPts), pPDx(totPts), pSDx(totPts);
	Array<OneD,NekDouble> pVDy(totPts), pPDy(totPts), pSDy(totPts);

	for( int i =0; i < Nx; i++)
	{
		for(int j=0; j< Ny; j++)
		{
			int index = i*Nx+j;
			pX[index] = i*sx;
			pY[index] = j*sy;
			pV[index] = std::cos(2.0*M_PI*(pX[index] + pY[index]) );
        	if (2==atoi(argv[4]))
        	{   
        	    pVDx[index] = -1.0*4.0*M_PI*M_PI*std::cos( 2.0*M_PI*(pX[index]+ pY[index]) );
        	    pVDy[index] = -1.0*4.0*M_PI*M_PI*std::cos( 2.0*M_PI*(pX[index]+ pY[index]) );
        	}else
        	{   
        	    pVDx[index] = -1.0*2.0*M_PI*std::sin( 2.0*M_PI*(pX[index]+pY[index]) );
        	    pVDy[index] = -1.0*2.0*M_PI*std::sin( 2.0*M_PI*(pX[index]+pY[index]) );
        	}   
			//pV[index] = std::cos(2.0*M_PI*(pX[index]))*std::cos(2.0*M_PI*(pY[index]));
			sm.EvaluateAt(pX[index],pY[index],0.0,pSDx[index],valY,valZ,directionX, atof(argv[3]),0);
			sm.EvaluateAt(pX[index],pY[index],0.0,pSDy[index],valY,valZ,directionY, atof(argv[3]),0);

			coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = 0.0;
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(coord);
			int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elId);
			const Array<OneD,NekDouble> el_phys = ece.CreateWithOffset(
										ece, coeffOffset);
			pP[index] = lexp->PhysEvaluate(coord,el_phys);
			const Array<OneD,NekDouble> el_physX = eceDx.CreateWithOffset(
										eceDx, coeffOffset);
			pPDx[index] = lexp->PhysEvaluate(coord,el_physX);
			const Array<OneD,NekDouble> el_physY = eceDy.CreateWithOffset(
										eceDy, coeffOffset);
			pPDy[index] = lexp->PhysEvaluate(coord,el_physY);
			cout << j << endl;

		}
		cout << i << endl;
	}

	NektarBaseClass k;
	k.writeNekArray(pX,fname+"_"+argv[2]+"_D_"+argv[4]+"_pX_2D_OneSided2kp1.txt");
	k.writeNekArray(pY,fname+"_"+argv[2]+"_D_"+argv[4]+"_pY_2D_OneSided2kp1.txt");
	k.writeNekArray(pV,fname+"_"+argv[2]+"_D_"+argv[4]+"_pV_2D_OneSided2kp1.txt");
	k.writeNekArray(pVDx,fname+"_"+argv[2]+"_D_"+argv[4]+"_pVDx_2D_OneSided2kp1.txt");
	k.writeNekArray(pVDy,fname+"_"+argv[2]+"_D_"+argv[4]+"_pVDy_2D_OneSided2kp1.txt");
	k.writeNekArray(pP,fname+"_"+argv[2]+"_D_"+argv[4]+"_pP_2D_OneSided2kp1.txt");
	k.writeNekArray(pPDx,fname+"_"+argv[2]+"_D_"+argv[4]+"_pPDx_2D_OneSided2kp1.txt");
	k.writeNekArray(pPDy,fname+"_"+argv[2]+"_D_"+argv[4]+"_pPDy_2D_OneSided2kp1.txt");
	k.writeNekArray(pS,fname+"_"+argv[2]+"_D_"+argv[4]+"_pS_2D_OneSided2kp1.txt");
	k.writeNekArray(pSDx,fname+"_"+argv[2]+"_D_"+argv[4]+"_pSDx_2D_OneSided2kp1.txt");
	k.writeNekArray(pSDy,fname+"_"+argv[2]+"_D_"+argv[4]+"_pSDy_2D_OneSided2kp1.txt");

/*
	 string out = vSession->GetSessionName() + ".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (HNM2D->m_expansions[0]->GetFieldDefinitions()).at(0);
    FieldDef[0]->m_fields.push_back("u");
    FieldDef[0]->m_fields.push_back("v");
    FieldDef[0]->m_fields.push_back("w");
    HNM2D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_fld->Write(out, FieldDef, FieldData);
*/
	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
