function [U,V,W] = testRoth(x,y,z)
switch nargin
    case 0,    
        x = (-1:0.1:1);
        y = (-1:.1:1);
        z = (-1:.1:1);
end

xold = x;
yold = y;
zold = z;

x = (x+1)*3/2+0.3;
y = (y+1)*3/2+0.3;
z = z/2*3;

w = 2; 
R = 1.5;
g = 1;

[X,Y,Z] = meshgrid(x,y,z);
X = permute(X,[2,1,3]); Y = permute(Y,[2,1,3]); Z = permute(Z,[2,1,3]);
r_XY = ((X).^2+(Y).^2+0.01).^(1/2);

U = -g*Y./r_XY - w*Z.*X./r_XY.^2;
V = g*X./r_XY -w*Z.*Y./r_XY.^2;
W = w*(1-R./r_XY);


% figure(1)
% VizIsoSurface(xold,yold,zold,U,V,W,[-1],[-1],[]); hold on;
% %c = [-0.25:.01:-0.15]
% c = [-0.22];
% [sx, sy, sz] = meshgrid(c,-1,[0]);
% %[sx, sy, sz] = meshgrid([-0.3:.3:0.3],[-1],[-0.3:.3:0.3]);
% 
% %figure(2)
% %VizStreamLines(x,y,z,U,V,W,1.73,1,0); hold on;
% %VizStreamLines(x,y,z,U,V,W,1.75,1,0); hold on;
% VizStreamLines(xold,yold,zold,U,V,W,sx,sy,sz,'g'); hold on;    
% plotBoundingBox();


% % Images for ARO meeting.
% figure(1)
% va = -3;
% vb = 26;
% 
% rad = 0.15;
% for teta = 0:pi/4:2*pi
%     sxx =rad*cos(teta);
%     szz = rad*sin(teta);
%     VizStreamLines(xold,yold,zold,U,V,W,sxx,-1,szz,'k'); hold on;    
% end
% rad = 0.15;
% teta = 0:pi/8:2*pi;
% plot3(rad*cos(teta),zeros(size(teta))-1,rad*sin(teta),'k'); hold on;
% plot3(zeros(size(teta))-1,rad*cos(teta),rad*sin(teta),'k');
% 
% VizStreamLines(xold,yold,zold,U,V,W,0,-1,0,'r',4); hold on;
% xlim([-1,0.5]); ylim([-1,0.5]); zlim([-3/4,3/4]);
% 
% set(gca,'color','none')
% set(gca, 'XTick', []);
% set(gca, 'YTick', []);
% set(gca, 'ZTick', []);
% set(gca, 'visible', 'off');

end












% % test for rothWorks
% clear;
% addpath ../createTestData/
% 
% t = (-10:.7:10);
% [X,Y,Z] = meshgrid(t,t,t);
% 
% data(:,1) = X(:);
% data(:,2) = Y(:);
% data(:,3) = Z(:);
% wa = 2; Ga = 7; R = 50;
% s_d = max(size(data));
% 
% for i = 1:s_d
%     pos = data(i,:);
%     x = pos(1); y = pos(2); z = pos(3);
%     r = sqrt(x^2+y^2)+0.5;
%     u = -Ga*y/r - wa*z*x/r^2;
%     v = Ga*x/r - wa*z*y/r^2;
%     w = wa*(1-R/r);
%     dataOut(i,:) = [u,v,w];
% end
% figure(1)
% quiver3(data(:,1),data(:,2),data(:,3),dataOut(:,1),dataOut(:,2),dataOut(:,3));
% view(-90,0);
% figure(2)
% drawStreamLine(data,dataOut,ones(6,3));
% view(-90,0);
% colorbar;

