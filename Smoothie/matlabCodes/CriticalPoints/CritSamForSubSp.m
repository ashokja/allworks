close all;
clear;

rate = 2;
DgGrid = 21;

%lattices = [11,21,31,51,61,71,81,91,101];
%lattices = [11,21,41,51,81,101,121,131];
%lattices= 41;
%lattices = [11,21,41,61,81,121,141,161,201]; %p3,41

a(1,:) = [0.74;+0.35];
a(2,:) = [0.68;-0.59];
a(3,:) = [-0.11;-0.72];
a(4,:) = [-0.58;-0.64];
a(5,:) = [0.51;+0.27];

a(6,:) = [-0.12;-0.84];

%for loop = 1:max(size(lattices))
   
 %   latticeGrid = lattices(loop);
filename = strcat('P',int2str(rate),'_quad_simple_',int2str(DgGrid),'_SubSp');

xfname = strcat('../../data/NekMeshes/',filename,'_x.txt' );
yfname = strcat('../../data/NekMeshes/',filename,'_y.txt' );

ufname = strcat('../../data/NekMeshes/',filename,'_u.txt' );
vfname = strcat('../../data/NekMeshes/',filename,'_v.txt' );

eofname = strcat('../../data/NekMeshes/',filename,'_eo.txt' );


xv = textread(xfname, '%f\t');
yv = textread(yfname, '%f\t');

uv = textread(ufname, '%f\t');
vv = textread(vfname, '%f\t');
    
eov = floor( textread(eofname, '%f\t' )+0.001);


ForUnique = floor(xv*1000000000+yv*1000);
[Val,elID, relID] = unique(ForUnique);
xvSorted = xv(elID);    
yvSorted = yv(elID);    

uvSorted = accumarray(relID,uv,[],@mean);
vvSorted = accumarray(relID,vv,[],@mean);

s = sqrt(max(size(elID)));

%create triangle list.
Tri=[];
count = 0;
for i = 1:s-1
    for j = 1:s-1
        count = count+1;
        Tri(count,:) = [(i-1)*s+j, (i)*s+j, (i)*s+j+1 ];
        count = count+1;
        Tri(count,:) = [(i-1)*s+j, (i-1)*s+j+1, (i)*s+j+1];
    end
end


% visualize trisurf.
figure(1),
subplot(1,2,1),
trisurf(Tri,xvSorted,yvSorted,uvSorted);
subplot(1,2,2),
trisurf(Tri,xvSorted,yvSorted,vvSorted);

% for each triangle calculate constant.
uxTri = zeros(max(size(Tri)),1); uyTri = zeros(max(size(Tri)),1);
vxTri = zeros(max(size(Tri)),1); vyTri = zeros(max(size(Tri)),1);
for t = 1:max(size(Tri))
    P1 = [ xvSorted(Tri(t,1)); yvSorted(Tri(t,1)); uvSorted(Tri(t,1)) ];
    P2 = [ xvSorted(Tri(t,2)); yvSorted(Tri(t,2)); uvSorted(Tri(t,2)) ];
    P3 = [ xvSorted(Tri(t,3)); yvSorted(Tri(t,3)); uvSorted(Tri(t,3)) ];
    Cp = cross(P3-P1,P2-P1);
    uxTri(t) = -1.0*Cp(1)/Cp(3);
    uyTri(t) = -1.0*Cp(2)/Cp(3);
end

for t = 1:max(size(Tri))
    P1 = [ xvSorted(Tri(t,1)); yvSorted(Tri(t,1)); vvSorted(Tri(t,1)) ];
    P2 = [ xvSorted(Tri(t,2)); yvSorted(Tri(t,2)); vvSorted(Tri(t,2)) ];
    P3 = [ xvSorted(Tri(t,3)); yvSorted(Tri(t,3)); vvSorted(Tri(t,3)) ];
    Cp = cross(P3-P1,P2-P1);
    vxTri(t) = -1.0*Cp(1)/Cp(3);
    vyTri(t) = -1.0*Cp(2)/Cp(3);
end

TriArea = zeros(max(size(Tri)),1);
%calcualte and store each area of triangle.
for t = 1:max(size(Tri))
    P1 = [ xvSorted(Tri(t,1)); yvSorted(Tri(t,1)); vvSorted(Tri(t,1)) ];
    P2 = [ xvSorted(Tri(t,2)); yvSorted(Tri(t,2)); vvSorted(Tri(t,2)) ];
    P3 = [ xvSorted(Tri(t,3)); yvSorted(Tri(t,3)); vvSorted(Tri(t,3)) ];
    xts = xvSorted(Tri(t,:));
    yts = yvSorted(Tri(t,:));
    TriArea(t) = polyarea(xts,yts);
end


% for each vertex
% loop through all triangles calcuate list.
% weight based vertex value.
numV = max(size(xvSorted));
for v = 1:numV
    listTids = [];
    for t = 1:max(size(Tri))
        ids = Tri(t,:);
        %listTids(end+1) = find(ids==v);
        %disp(find(ids==v));
        if (find(ids==v))
            listTids(end+1) = t;
        end
    end
    disp(listTids);
%    vertexToTriMap(v,:) = listTids;
    sumArea = 0;
    uxV =0; uyV=0; vyV=0; vxV=0;
    for lt = 1:max(size(listTids))
        tid = listTids(lt);
        sumArea = sumArea + TriArea(tid);
        uxV = uxV+ TriArea(tid)*uxTri(tid);
        uyV = uyV+ TriArea(tid)*uyTri(tid);
        vxV = vxV+ TriArea(tid)*vxTri(tid);
        vyV = vyV+ TriArea(tid)*vyTri(tid);
    end
    uxvSorted(v) = uxV/sumArea;
    uyvSorted(v) = uyV/sumArea;
    vxvSorted(v) = vxV/sumArea;
    vyvSorted(v) = vyV/sumArea;
end

%%
figure(2),
subplot(2,2,1),
trisurf(Tri,xvSorted,yvSorted,uxvSorted);
subplot(2,2,2),
trisurf(Tri,xvSorted,yvSorted,uyvSorted);
subplot(2,2,3),
trisurf(Tri,xvSorted,yvSorted,vxvSorted);
subplot(2,2,4),
trisurf(Tri,xvSorted,yvSorted,vyvSorted);

%%
%P = [2.5 8.0; 6.5 8.0; 2.5 5.0; 6.5 5.0; 1.0 6.5; 8.0 6.5];
P = [xvSorted yvSorted];
%T = [5 3 1; 3 2 1; 3 4 2; 4 6 2];

TR = triangulation(Tri,P);

x3 = linspace(0.001,0.999,100);
[X,Y] = meshgrid(x3,x3);
%U,V,Ux,Uy,Vx,Vy;
for i = 1:100
    for j = 1:100
        [val,dval,ret] = critSubEval(X(i,j),Y(i,j) , TR, Tri,xvSorted,yvSorted, uvSorted,uxvSorted,uyvSorted,vvSorted, vxvSorted, vyvSorted);
        U(i,j) = val(1); V(i,j) = val(2);
        Ux(i,j) = dval(1,1); Uy(i,j) = dval(1,2);
        Vx(i,j) = dval(2,1); Vy(i,j) = dval(2,2);    
        disp(i);
        disp(j);
    end
end

figure(3),
subplot(1,2,1),
surf(X,Y,U);
subplot(1,2,2),
surf(X,Y,V);

figure(4),
subplot(2,2,1),
surf(X,Y,Ux);
subplot(2,2,2),
surf(X,Y,Uy);
subplot(2,2,3),
surf(X,Y,Vx);
subplot(2,2,4),
surf(X,Y,Vy);



%%
for i = 1:6
        xin = ((a(i,:)+1)/2)';
        [pt,ret,debugX,debugY,ptVal]= critsubmultinewton(xin,100,TR, Tri,xvSorted,yvSorted, uvSorted,uxvSorted,uyvSorted,vvSorted, vxvSorted, vyvSorted);
        yout(i,:) = pt*2-1;
        yeval(i) = ret;
        ptValSave(i) = norm(ptVal);
    %disp(pt*2-1);
    %disp(ret);
end

    disp(yout);
    disp(yeval);
    error = (yout-a);
    disp(error);
%%
    for i =1 :6
        %if ( 0 == yeval(i))
        %   errornorm(i) = inf; 
        %else
           errornorm(i) = norm(error(i,:));
        %end
    end
    disp(errornorm' );
%critsubmultinewton()

input.data = [errornorm']
%%
latex = latexTable(input);



