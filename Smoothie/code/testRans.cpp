#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include "OneSidedSIAC.h"
#include <MultiRegions/DisContField2D.h>
#include <omp.h>
#include <boost/foreach.hpp>
#include <iostream>
#include <iomanip> // std::setprecision
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
#define TOLERENCE_VIZ 1e-6

using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg is polynomial order of LSIAC" << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();
	string fldname = fname + ".fld";
	HNM2D->LoadData( fldname ,var);

	SpatialDomains::MeshGraphSharedPtr graph2D =
            SpatialDomains::MeshGraph::Read(vSession);
	
	int poly = atoi(argv[2]);
	graph2D->SetExpansionsToPolyOrder( 2*(poly-1)+1+1 );
	MultiRegions::ExpListSharedPtr	Exp_Result;
	Exp_Result = MemoryManager<MultiRegions::DisContField2D>
            ::AllocateSharedPtr(vSession,graph2D,vSession->GetVariable(0));
	HNM2D->LoadExpListIntoRTree();
	
	int New_tNquadPts = Exp_Result->GetTotPoints();
	int New_tNcoeffs = Exp_Result->GetNcoeffs();
	cout << "Exp Sizei:\t" << Exp_Result->GetExpSize()<< endl;
	cout << "New_tNcoeffs:\t" << New_tNquadPts << endl;
	Array<OneD,NekDouble> rho_new(New_tNquadPts);
	Array<OneD,NekDouble> xc0_new(New_tNquadPts),xc1_new(New_tNquadPts),xc2_new(New_tNquadPts);
	Array<OneD,NekDouble> p_new(New_tNquadPts), r_new(New_tNquadPts);
	Array<OneD,NekDouble> p_C(New_tNcoeffs),r_C(New_tNcoeffs);

	// initialize the LSIAC filter.
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
    {
        case 2:
            Exp_Result->GetCoords(xc0_new,xc1_new);
            Vmath::Zero(New_tNquadPts,&xc2_new[0],1);
            break;
        default:
            assert( false && "looks dim not taken into account");
            cout << "opps did not plan for this" << endl;
    }

	SmoothieSIAC2D sm(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp2, HNM2D, 2, 0.5); 
	//atoi(argv[3]), atof(argv[4]));
	HNM2D->CalculateDynamicScaling();
	Array<OneD,NekDouble> direction(3,0.0);
	direction[0] = 1.0;
	NekDouble valY,valZ;
	Array<OneD,NekDouble> coord(3);
	for(int e=0; e< Exp_Result->GetExpSize(); e++)
	{
		int phys_offset = Exp_Result->GetPhys_Offset(e);
		for(int i=0; i < Exp_Result->GetExp(e)->GetTotPoints(); i++)
		{
			coord[0] = xc0_new[phys_offset+i]; coord[1] = xc1_new[phys_offset+i]; 
			coord[2] = xc2_new[phys_offset+i];
			NekDouble scaling = HNM2D->GetDynamicScaling(coord,e,1.0);
			if (scaling > 0.2)
				{
					scaling = 0.2;
					cout << "corrected scaline \te:\t" << e <<"\ti:\t" << i <<endl;
				}
			sm.EvaluateAt(xc0_new[phys_offset+i],xc1_new[phys_offset+i],xc2_new[phys_offset+i],
							p_new[phys_offset+i],valY,valZ,direction,scaling,2);
			sm.EvaluateAt(xc0_new[phys_offset+i],xc1_new[phys_offset+i],xc2_new[phys_offset+i],
							r_new[phys_offset+i],valY,valZ,direction,scaling,3);
		}
		//cout << "element:\t" <<e <<endl;
	}

	Exp_Result->FwdTrans(p_new,p_C);
	Exp_Result->FwdTrans(r_new,r_C);
  	string out = vSession->GetSessionName() + "_new.fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (Exp_Result->GetFieldDefinitions()).at(0);
    FieldDef[0]->m_fields.push_back("p");
    FieldDef[0]->m_fields.push_back("rho");
    Exp_Result->AppendFieldData(FieldDef[0], FieldData[0],p_C);
    Exp_Result->AppendFieldData(FieldDef[0], FieldData[0],r_C);
    HNM2D->m_fld->Write(out, FieldDef, FieldData);
	
	return 0;

}

