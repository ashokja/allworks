class SurfaceDataCurves  //<>//
{
  int ku;
  ArrayList<Float> Knotu;
  int Tu;
  int numCptsu;
  int numCurves;
  ArrayList<PVector> CptsAu;

  SurfaceDataCurves(int tku, ArrayList<Float> tKnotu, int tnumCptsu, ArrayList<PVector> tCptsAu, int tnumCurves)
  {
     ku = tku;
     Knotu = tKnotu;
     Knotu = CreateKnot( tnumCptsu, tku);
     Tu = Knotu.size();
     numCptsu = tnumCptsu;
     CptsAu = tCptsAu; 
     numCurves = tnumCurves;
  }
  
  
  void DrawGivenCurves()
  {
    // degree ku
    // for each curve draw them.
    for (int i =0; i < numCurves; i++)
    {
//      void DrawCurve(ArrayList<Float> KnotA, ArrayList<PVector> CPtA, int numCPt, int Deg )
      // get number of points
      ArrayList<PVector> cpt = new ArrayList<PVector> ( CptsAu.subList(i*numCptsu,(i+1)*numCptsu));
      DrawCurve(Knotu, cpt, numCptsu,ku);
    }
    
  }
  
  
  void printData()
  {
    println(ku);
    println(Knotu);
    println(Tu);
    println(numCptsu);
    println(numCurves);
    println(CptsAu);
  }
  
  void GetC(int kv)
  {
    // loop through num of cpts
    
      // loop through num of curves.
        //collect the points.  

    ArrayList<Float> Knotv ;
    Knotv = CreateKnot( numCurves , kv);
    ArrayList<PVector> C = new ArrayList<PVector> ();
    //Send the points through dataControl get a new list of points.
    for(int i =0; i < numCptsu; i++)
    {
     ArrayList<PVector> a_j = new ArrayList<PVector> ();
      for(int c =0; c < numCurves; c++)
      {
        a_j.add(CptsAu.get(c*numCptsu+i));
      }
      ArrayList<PVector> C_i = dataToControlPoints( a_j, Knotv,  kv);
      C.addAll(C_i);
    }

    sc = new SurfaceCurves( ku, kv, Knotu.size(), Knotv.size(), Knotu, Knotv, C);        
    
  }// End of function.
  
  
}

ArrayList<Float> CreateKnot(int NumCPt, int Deg)
{
    float count =0;
    ArrayList<Float> KnotA = new ArrayList<Float> ();
    for (int i =0; i < NumCPt+Deg+1; i++)
    {
      if (i <= Deg || i >= NumCPt+1)
      {
      } else
      {
        count++;
      }
      float kv = count;
      KnotA.add(kv);
    }
    KnotA.set(NumCPt+Deg, KnotA.get(NumCPt+Deg)+1);
    return KnotA;
}
