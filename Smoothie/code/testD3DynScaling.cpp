#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include <MultiRegions/DisContField3D.h>
#include "HandleNekMesh3D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField3D.h>
//#include <SpatialDomains/MeshGraph3D.h>


using namespace std;

 
int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM3D->LoadMesh(var[0]);
	string fname = vSession->GetSessionName();

	int tNquadPts = HNM3D->m_expansions[0]->GetTotPoints();
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);
	cout << "fc:" << tNquadPts<< endl;

	SpatialDomains::MeshGraphSharedPtr graph3D =
             SpatialDomains::MeshGraph::Read(vSession);
	graph3D->SetExpansionsToPolyOrder( 2*(atoi(argv[2])-1)+1+1);
	MultiRegions::ExpListSharedPtr Exp_u = MemoryManager<MultiRegions::DisContField3D>
 			::AllocateSharedPtr(vSession,graph3D,vSession->GetVariable(0));
	int New_tNquadPts = Exp_u->GetTotPoints();

	HNM3D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sceS1,ece;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sceS1 = Array<OneD,NekDouble>(tNquadPts);

//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);

	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(20.0*M_PI*(xc0[i]+xc1[i]+xc2[i]));
	}
	
	HNM3D->CalculateDynamicScaling();
	
	HNM3D->m_expansions[0]->FwdTrans(fce,HNM3D->m_expansions[0]->UpdateCoeffs() );
	HNM3D->m_expansions[0]->BwdTrans( HNM3D->m_expansions[0]->GetCoeffs(),
										HNM3D->m_expansions[0]->UpdatePhys());
	 ece = HNM3D->m_expansions[0]->GetPhys();
	HNM3D->m_Arrays.push_back(ece);
	
	HNM3D->LoadExpListIntoRTree();

	SmoothieSIAC3D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]), atof(argv[3]) ); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
    cout << HNM3D->m_expansions.size() << endl;

	Array<OneD,NekDouble> direction(3,0.0);
	direction[0] = 1.0;
	NekDouble scaling = atof(argv[3]);

//	Find a list of elements in the interested region.
//	Loop through all quadrature points to evaluate LSIAC
	vector<int> elIds,glIds;
	HNM3D->IntersectWithBoxUsingRTree( 0.20, 0.05, -0.05,
									    0.30, 0.15,  0.05, elIds,glIds);
	//HNM3D->IntersectWithBoxUsingRTree( -0.5+0.00001, -0.5+0.00001, -0.001,
	//								   0.5-0.00001, 0.5-0.00001, 0.001, elIds,glIds);
	Array<OneD,NekDouble> lcoord(3,0.0),glCoordTemp(3,0.0);
	Array<OneD,NekDouble> SceN(New_tNquadPts,0.0), xcN0(New_tNquadPts), xcN1(New_tNquadPts), xcN2(New_tNquadPts);
	Array<OneD,NekDouble> SceDN(New_tNquadPts,0.0),fceN(New_tNquadPts,0.0), eceN(New_tNquadPts,0.0);
	Exp_u->GetCoords(xcN0,xcN1,xcN2);
	
	cout << "Total no of elements considered are " << elIds.size() << endl;
	for ( NekDouble mu = 1.0; mu < 1.01; mu=mu+0.25)
	{	
	for( int i=0; i< elIds.size(); i++)
	{
		int elmID = elIds[i];
		int physOffsetN = Exp_u->GetPhys_Offset(elmID);
		LocalRegions::ExpansionSharedPtr lexp = HNM3D->m_expansions[0]->GetExp( elmID);
		Array<OneD,NekDouble> u_phys_old = ece.CreateWithOffset(ece,HNM3D->m_expansions[0]->GetPhys_Offset(elmID));
		for(int j=0; j< Exp_u->GetExp(elmID)->GetTotPoints();j++)
		{
			int index = j+physOffsetN;
			// constant scaling
		//	sm.EvaluateAt(xcN0[index],xcN1[index],xcN2[index],SceN[index],valY,valZ,direction, scaling,0);
			// dynamic scaling
			glCoordTemp[0] = xcN0[index]; glCoordTemp[1] = xcN1[index]; glCoordTemp[2] = xcN2[index];
			sm.EvaluateAt(xcN0[index],xcN1[index],xcN2[index],SceDN[index],valY,valZ,direction, HNM3D->GetDynamicScaling(glCoordTemp,elmID,mu),0);
			// actual function
			fceN[index] = std::cos(20.0*M_PI*(xcN0[index]+xcN1[index]+xcN2[index]));
			eceN[index] = lexp->PhysEvaluate(glCoordTemp, u_phys_old);
		}
		cout << "eid:\t" << elmID << "\t"<< i <<endl;
	}

    NekDouble L2_SIAC=0.0, L2_DG=0.0, L2_Dyn_SIAC=0.0;
    NekDouble Linf_SIAC=0.0, Linf_DG=0.0, Linf_Dyn_SIAC=0.0;
    for(int i=0; i<elIds.size() ;i++)
    {   
        int index = elIds[i];
        LocalRegions::ExpansionSharedPtr lexp = Exp_u->GetExp( index );
        int phys_offset = Exp_u->GetPhys_Offset(index);
        Array<OneD,NekDouble> el_VF = fceN.CreateWithOffset( fceN, phys_offset);
        Array<OneD,NekDouble> el_VP = eceN.CreateWithOffset( eceN, phys_offset);
        Array<OneD,NekDouble> el_VS1 = SceN.CreateWithOffset( SceN, phys_offset);
        Array<OneD,NekDouble> el_VDS = SceDN.CreateWithOffset( SceDN, phys_offset);
        L2_DG += ( lexp->L2(el_VF,el_VP)*lexp->L2(el_VF,el_VP)) ; 
        L2_SIAC += ( lexp->L2(el_VF,el_VS1)*lexp->L2(el_VF,el_VS1));
        L2_Dyn_SIAC += ( lexp->L2(el_VF,el_VDS)*lexp->L2(el_VF,el_VDS));
        Linf_DG = std::max(Linf_DG,  lexp->Linf(el_VF,el_VP)); 
        Linf_SIAC =std::max(Linf_SIAC, lexp->Linf(el_VF,el_VS1));
        Linf_Dyn_SIAC =std::max(Linf_Dyn_SIAC, lexp->Linf(el_VF,el_VDS));
//      cout << index <<"\t" << L2_DG <<"\t"<< L2_SIAC << endl;  
//      cout << index <<"\t" << lexp->L2(el_VF,el_VP) <<"\t"<< lexp->L2(el_VF,el_VS1) << endl;  
	}   

	cout << "Error calculated at selective places" << endl; 
	cout << "mu:\t" << mu << endl;   
    cout << "L2 error DG:\t" << std::sqrt(L2_DG);
    cout << "\t SIAC\t" <<  std::sqrt(L2_SIAC);
    cout << "\t SIACDyn\t" <<  std::sqrt(L2_Dyn_SIAC) << endl;

    cout << "Linf error DG:\t" << Linf_DG ;
    cout << "\t SIAC\t" <<Linf_SIAC;
    cout << "\t SIACDyn\t" <<Linf_Dyn_SIAC << endl;

	} // end of mu for loop
	return 0;
}
