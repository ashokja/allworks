% Explanation: Polynomial projection error table along the analytical core
% line for voritices (Vector feild.)

clear;
close all;

N = [4:9];
k = [2:5];
%for NN = N
va = -70;
vb = 10;
addpath ../PolyLib
addpath ../createTestData
load('testData/jacX');
load('testData/jacWX');
sampleData = 50;
%    clearvars -except NN N
once = 5;
%Bring validation step here to increase the speed up;

    % % Data required for Validation step
     xvali = linspace(-1,1,6);
     yvali = linspace(-1,1,6);
     zvali = linspace(-1,1,6);
     [XV,YV,ZV] = meshgrid(xvali,yvali,zvali);
      XV = permute(XV,[2,1,3]); YV = permute(YV,[2,1,3]); ZV = permute(ZV,[2,1,3]);
     mapxyvali(:,1) = XV(:); mapxyvali(:,2) = YV(:); mapxyvali(:,3) = ZV(:);
     [xy,distance,t_a,mapxyVTrue,curveCoeff] = test_runXYZExt(1,mapxyvali);
     % curve location data.
     data = linspace(-1,1,sampleData);
     Cxdata = B_eval(curveCoeff(1,:),data);
     Cydata = B_eval(curveCoeff(2,:),data);
     Czdata = B_eval(curveCoeff(3,:),data);
     Cdata = [Cxdata;Cydata;Czdata]'; % Cdata is curve location data.
     [~,~,~,CVdata_Act,~] = test_runXYZExt(1,Cdata); %CVdata is value of Velcoity at Cdata.
     
     NN = 5;
     n = 2;
for NN = N
    for n = k
        tic    
        %    clearvars -except NN N
            NX =NN; NY=NN; NZ=NN;
            nx=n;ny=n;nz=n;
            px =6*NN; py =6*NN; pz =6*NN;
            %QX = 8; QY = 9; QZ = 10;
            addpath ../createTestData
            % call test_run to get the function defined at that location.

                    XQ = XQN{NX+2+nx}; WXQ = WXQN{NX+2+nx};
                    YQ = XQN{NY+2+ny}; WYQ = WXQN{NY+2+ny};
                    ZQ = XQN{NZ+2+nz}; WZQ = WXQN{NZ+2+nz};
            %%
                    clearvars mapxy mapxyV mapxyV_vali vel acc bi

        %    NX = 28; NY = 28; NZ = 28;
        %    nx = 2; ny = 2; nz = 2;
            [X,Y,Z] = meshgrid(XQ,YQ,ZQ);

            X = permute(X,[2,1,3]); Y = permute(Y,[2,1,3]); Z = permute(Z,[2,1,3]);
            mapxy(:,1) = X(:); mapxy(:,2) = Y(:); mapxy(:,3) = Z(:);
            [xy,distance,t_a,mapxyV,curveCoeff] = test_runXYZExt(1,mapxy);

            UAtGrid = reshape(mapxyV(:,1),[max(size(XQ)),max(size(YQ)),max(size(ZQ))]);
            VAtGrid = reshape(mapxyV(:,2),[max(size(XQ)),max(size(YQ)),max(size(ZQ))]);
            WAtGrid = reshape(mapxyV(:,3),[max(size(XQ)),max(size(YQ)),max(size(ZQ))]);

            coeffU = CalLegendreCoeff(UAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);
            coeffV = CalLegendreCoeff(VAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);
            coeffW = CalLegendreCoeff(WAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);

            [DChebX,xcheb] = cheb(NX+nx+px);
            [DChebY,ycheb] = cheb(NY+ny+py);
            [DChebZ,zcheb] = cheb(NZ+nz+pz);

            UAtCheb = CalValueAt(coeffU,xcheb,ycheb,zcheb);
            VAtCheb = CalValueAt(coeffV,xcheb,ycheb,zcheb);
            WAtCheb = CalValueAt(coeffW,xcheb,ycheb,zcheb);

            UAtCheb_DX = CalDerivative(UAtCheb,DChebX,'X');
            UAtCheb_DY = CalDerivative(UAtCheb,DChebY,'Y');
            UAtCheb_DZ = CalDerivative(UAtCheb,DChebZ,'Z');

            VAtCheb_DX = CalDerivative(VAtCheb,DChebX,'X');
            VAtCheb_DY = CalDerivative(VAtCheb,DChebY,'Y');
            VAtCheb_DZ = CalDerivative(VAtCheb,DChebZ,'Z');

            WAtCheb_DX = CalDerivative(WAtCheb,DChebX,'X');
            WAtCheb_DY = CalDerivative(WAtCheb,DChebY,'Y');
            WAtCheb_DZ = CalDerivative(WAtCheb,DChebZ,'Z');

            % Calculate Acceleration
            AccXatCheb = UAtCheb_DX.*UAtCheb + UAtCheb_DY.*VAtCheb + UAtCheb_DZ.*WAtCheb;
            AccYatCheb = VAtCheb_DX.*UAtCheb + VAtCheb_DY.*VAtCheb + VAtCheb_DZ.*WAtCheb;
            AccZatCheb = WAtCheb_DX.*UAtCheb + WAtCheb_DY.*VAtCheb + WAtCheb_DZ.*WAtCheb;

            % Calculate of JJv
            JJvXatCheb = UAtCheb_DX.*AccXatCheb + UAtCheb_DY.*AccYatCheb + UAtCheb_DZ.*AccZatCheb;
            JJvYatCheb = VAtCheb_DX.*AccXatCheb + VAtCheb_DY.*AccYatCheb + VAtCheb_DZ.*AccZatCheb;
            JJvZatCheb = WAtCheb_DX.*AccXatCheb + WAtCheb_DY.*AccYatCheb + WAtCheb_DZ.*AccZatCheb;

            % T 2nd Derivative or 3rd order tensor.
            UAtCheb_DX_DX = CalDerivative(UAtCheb_DX,DChebX,'X');
            UAtCheb_DX_DY = CalDerivative(UAtCheb_DX,DChebY,'Y');
            UAtCheb_DX_DZ = CalDerivative(UAtCheb_DX,DChebZ,'Z');    

            UAtCheb_DY_DX = CalDerivative(UAtCheb_DY,DChebX,'X');
            UAtCheb_DY_DY = CalDerivative(UAtCheb_DY,DChebY,'Y');
            UAtCheb_DY_DZ = CalDerivative(UAtCheb_DY,DChebZ,'Z');

            UAtCheb_DZ_DX = CalDerivative(UAtCheb_DZ,DChebX,'X');
            UAtCheb_DZ_DY = CalDerivative(UAtCheb_DZ,DChebY,'Y');
            UAtCheb_DZ_DZ = CalDerivative(UAtCheb_DZ,DChebZ,'Z');

            VAtCheb_DX_DX = CalDerivative(VAtCheb_DX,DChebX,'X');
            VAtCheb_DX_DY = CalDerivative(VAtCheb_DX,DChebY,'Y');
            VAtCheb_DX_DZ = CalDerivative(VAtCheb_DX,DChebZ,'Z');    

            VAtCheb_DY_DX = CalDerivative(VAtCheb_DY,DChebX,'X');
            VAtCheb_DY_DY = CalDerivative(VAtCheb_DY,DChebY,'Y');
            VAtCheb_DY_DZ = CalDerivative(VAtCheb_DY,DChebZ,'Z');

            VAtCheb_DZ_DX = CalDerivative(VAtCheb_DZ,DChebX,'X');
            VAtCheb_DZ_DY = CalDerivative(VAtCheb_DZ,DChebY,'Y');
            VAtCheb_DZ_DZ = CalDerivative(VAtCheb_DZ,DChebZ,'Z');

            WAtCheb_DX_DX = CalDerivative(WAtCheb_DX,DChebX,'X');
            WAtCheb_DX_DY = CalDerivative(WAtCheb_DX,DChebY,'Y');
            WAtCheb_DX_DZ = CalDerivative(WAtCheb_DX,DChebZ,'Z');    

            WAtCheb_DY_DX = CalDerivative(WAtCheb_DY,DChebX,'X');
            WAtCheb_DY_DY = CalDerivative(WAtCheb_DY,DChebY,'Y');
            WAtCheb_DY_DZ = CalDerivative(WAtCheb_DY,DChebZ,'Z');

            WAtCheb_DZ_DX = CalDerivative(WAtCheb_DZ,DChebX,'X');
            WAtCheb_DZ_DY = CalDerivative(WAtCheb_DZ,DChebY,'Y');
            WAtCheb_DZ_DZ = CalDerivative(WAtCheb_DZ,DChebZ,'Z');

            % Calucate biNor_T  = JJv+Tvv
            biNorX_T = (UAtCheb_DX_DX.*UAtCheb + UAtCheb_DX_DY.*VAtCheb + UAtCheb_DX_DZ.*WAtCheb).*UAtCheb+...
                       (UAtCheb_DY_DX.*UAtCheb + UAtCheb_DY_DY.*VAtCheb + UAtCheb_DY_DZ.*WAtCheb).*VAtCheb+...
                       (UAtCheb_DZ_DX.*UAtCheb + UAtCheb_DZ_DY.*VAtCheb + UAtCheb_DZ_DZ.*WAtCheb).*WAtCheb+...
                       JJvXatCheb;
            biNorY_T = (VAtCheb_DX_DX.*UAtCheb + VAtCheb_DX_DY.*VAtCheb + VAtCheb_DX_DZ.*WAtCheb).*UAtCheb+...
                       (VAtCheb_DY_DX.*UAtCheb + VAtCheb_DY_DY.*VAtCheb + VAtCheb_DY_DZ.*WAtCheb).*VAtCheb+...
                       (VAtCheb_DZ_DX.*UAtCheb + VAtCheb_DZ_DY.*VAtCheb + VAtCheb_DZ_DZ.*WAtCheb).*WAtCheb+...
                       JJvYatCheb;
            biNorZ_T = (WAtCheb_DX_DX.*UAtCheb + WAtCheb_DX_DY.*VAtCheb + WAtCheb_DX_DZ.*WAtCheb).*UAtCheb+...
                       (WAtCheb_DY_DX.*UAtCheb + WAtCheb_DY_DY.*VAtCheb + WAtCheb_DY_DZ.*WAtCheb).*VAtCheb+...
                       (WAtCheb_DZ_DX.*UAtCheb + WAtCheb_DZ_DY.*VAtCheb + WAtCheb_DZ_DZ.*WAtCheb).*WAtCheb+...
                       JJvZatCheb;

        % 
        %     % Calcuate spatial Derivative D(Acc)/DS
        %     AccXatCheb_DX = CalDerivative(AccXatCheb,DChebX,'X');
        %     AccXatCheb_DY = CalDerivative(AccXatCheb,DChebY,'Y');
        %     AccXatCheb_DZ = CalDerivative(AccXatCheb,DChebZ,'Z');
        % 
        %     AccYatCheb_DX = CalDerivative(AccYatCheb,DChebX,'X');
        %     AccYatCheb_DY = CalDerivative(AccYatCheb,DChebY,'Y');
        %     AccYatCheb_DZ = CalDerivative(AccYatCheb,DChebZ,'Z');
        % 
        %     AccZatCheb_DX = CalDerivative(AccZatCheb,DChebX,'X');
        %     AccZatCheb_DY = CalDerivative(AccZatCheb,DChebY,'Y');
        %     AccZatCheb_DZ = CalDerivative(AccZatCheb,DChebZ,'Z');
        % 
        % 
        %     % Calculate biNormal
        %     biNorX_T = AccXatCheb_DX.*UAtCheb + AccXatCheb_DY.*VAtCheb + AccXatCheb_DZ.*WAtCheb;
        %     biNorY_T = AccYatCheb_DX.*UAtCheb + AccYatCheb_DY.*VAtCheb + AccYatCheb_DZ.*WAtCheb;
        %     biNorZ_T = AccZatCheb_DX.*UAtCheb + AccZatCheb_DY.*VAtCheb + AccZatCheb_DZ.*WAtCheb;

            % Calculate curvature
            vel(:,1) = UAtCheb(:);   vel(:,2) = VAtCheb(:);   vel(:,3) = WAtCheb(:); 
            acc(:,1) = AccXatCheb(:);acc(:,2) = AccYatCheb(:);acc(:,3) = AccZatCheb(:);
            bi(:,1) = biNorX_T(:);bi(:,2) = biNorY_T(:);bi(:,3) = biNorZ_T(:);

            v_cross_a = cross(vel,acc,2);
            divi_C = (sum(vel.^2,2)).^(3/2);
            cur = v_cross_a./[divi_C,divi_C,divi_C];

            curXAtGrid = reshape(cur(:,1),[max(size(xcheb)),max(size(ycheb)),max(size(zcheb))]);
            curYAtGrid = reshape(cur(:,2),[max(size(xcheb)),max(size(ycheb)),max(size(zcheb))]);
            curZAtGrid = reshape(cur(:,3),[max(size(xcheb)),max(size(ycheb)),max(size(zcheb))]);

            scalarTp = dot(v_cross_a,bi,2);
            divi_T = sum(v_cross_a.^2,2);
            tor_D = scalarTp./(divi_T+0.1);
            tor = reshape(tor_D,[max(size(xcheb)),max(size(ycheb)),max(size(zcheb))]);
            
            % All calculations of torsion and other stuff are done.
            for smo = 1:5
                biNorX_T = smooth3(biNorX_T,'box',5);
                biNorY_T = smooth3(biNorY_T,'box',5);
                biNorZ_T = smooth3(biNorZ_T,'box',5);
            end
            bi(:,1) = biNorX_T(:);bi(:,2) = biNorY_T(:);bi(:,3) = biNorZ_T(:);

            norm_vel = sqrt(sum(vel.^2,2));norm_bi = sqrt(sum(bi.^2,2));
            velN = vel./[norm_vel,norm_vel,norm_vel];
            
            biN = bi./[norm_bi,norm_bi,norm_bi];
            BV_D = 1-abs(dot(velN,biN,2));
            BV = reshape(BV_D,[max(size(xcheb)),max(size(ycheb)),max(size(zcheb))]);

        %     %    % % Validation step
        %     xvali = linspace(-1,1,11);
        %     yvali = linspace(-1,1,11);
        %     zvali = linspace(-1,1,11);
        % 
        %     clear mapxy;
        %     [XV,YV,ZV] = meshgrid(xvali,yvali,zvali);
        %     XV = permute(XV,[2,1,3]); YV = permute(Y,[2,1,3]); ZV = permute(ZV,[2,1,3]);
        %     mapxy(:,1) = XV(:); mapxy(:,2) = YV(:); mapxy(:,3) = ZV(:);
        %     [xy,distance,t_a,mapxyV,curveCoeff] = test_runXYZ(1,mapxy);
        %     UAtVali = CalValueAt(coeffU,xvali,yvali,zvali);
        %     VAtVali = CalValueAt(coeffV,xvali,yvali,zvali);
        %     WAtVali = CalValueAt(coeffW,xvali,yvali,zvali);
        %     mapxyV_Vali(:,1) = UAtVali(:); mapxyV_Vali(:,2) = VAtVali(:); mapxyV_Vali(:,3) = WAtVali(:);
        %     er_cheb = sum(abs(mapxyV(:) - mapxyV_Vali(:)))

%         %%
%             f=1;
%             f1 = figure(f); f=f+1;
%             VizIsoSurface(xcheb,ycheb,zcheb,UAtCheb,VAtCheb,WAtCheb);hold on;
%         %    VizStreamLines(xcheb,ycheb,zcheb,UAtCheb,VAtCheb,WAtCheb); hold on;
%             Plot3DCurve(curveCoeff); hold on; view(va,vb)
%             title('norm(Velocity)'); 
% 
%             f2= figure(f); f = f+1;
%             VizIsoSurface(xcheb,ycheb,zcheb,AccXatCheb,AccYatCheb,AccZatCheb);hold on;
%         %    VizStreamLines(xcheb,ycheb,zcheb,AccXatCheb,AccYatCheb,AccZatCheb);hold on;
%             Plot3DCurve(curveCoeff); title('norm(Acceleration)'); view(va,vb); 
%             %caxis([0,1]);
% 
%             f3 = figure(f); f= f+1;
%             VizIsoSurface(xcheb,ycheb,zcheb,biNorX_T,biNorY_T,biNorZ_T);hold on;
%         %    VizStreamLines(xcheb,ycheb,zcheb,biNorX_T,biNorY_T,biNorZ_T);hold on;
%             Plot3DCurve(curveCoeff); title('norm(Binormal)'); view(va,vb); 
%             %caxis([0,1]);
%         %%
%             f6 = figure(6);
%             Plot3DCurve(curveCoeff); title('Vortex axis curve'); view(va,vb)
%             VizStreamLines(XQ,YQ,ZQ,UAtGrid,VAtGrid,WAtGrid); hold on;
%         %%
%             f4 = figure(4);
%             VizIsoSurface(xcheb,ycheb,zcheb,curXAtGrid,curYAtGrid,curZAtGrid);hold on;
%             Plot3DCurve(curveCoeff); title('norm(Curvature)'); view(va,vb);% caxis([0,1]);
% 
%             f5 = figure(5);
%             VizIsoSurface(xcheb,ycheb,zcheb,tor,tor,tor);hold on;
%             Plot3DCurve(curveCoeff); title('Torsion'); view(va,vb); 
% 
%             f7 = figure(7);
%             pts = VizSurface(xcheb,ycheb,zcheb,curXAtGrid,curYAtGrid,curZAtGrid,0.2);
%             Plot3DCurve(curveCoeff); title('norm(Curvature)'); view(va,vb);% caxis([0,1]);
% 
%             Npts = FindLargestComp(pts);
%             f8 = figure(8);
%             Sol_curveCoeff= ExtractCurFromIsoSurface(6,Npts);
%             VizSurface(xcheb,ycheb,zcheb,curXAtGrid,curYAtGrid,curZAtGrid,0.2);
%             Plot3DCurve(Sol_curveCoeff); title('norm(Curvature)'); view(va,vb);% caxis([0,1]);
% 
%             f10 = figure(10);
%             VizSurface(xcheb,ycheb,zcheb,tor,tor,tor,0.2);hold on;
%             Plot3DCurve(curveCoeff); title('Torsion'); view(va,vb); 
            
            
            %Calculation of Curvature.
            
            pts = VizSurfacePts(xcheb,ycheb,zcheb,curXAtGrid,curYAtGrid,curZAtGrid,0.1);
            Npts = FindLargestComp(pts);
            Sol_curveCoeff= ExtractCurFromIsoSurface(6,Npts);            
            
            Sol_Cxdata = B_eval(Sol_curveCoeff(1,:),data);
            Sol_Cydata = B_eval(Sol_curveCoeff(2,:),data);
            Sol_Czdata = B_eval(Sol_curveCoeff(3,:),data);
            Sol_Cdata = [Sol_Cxdata;Sol_Cydata;Sol_Czdata]'; % Cdata is curve location data.
            
            MaxErCur(NN,n) = max(abs(Sol_Cdata(:)-Cdata(:)));
            AvgErCur(NN,n) = sum(abs(Sol_Cdata(:)-Cdata(:)))/max(size(Sol_Cdata(:)));

            %Calculation of Torsion
            
            pts = VizSurfacePtsScalar(xcheb,ycheb,zcheb,tor,0);
            Npts = FindLargestCompTor(pts);
            Sol_torCoeff= ExtractCurFromIsoSurface(6,Npts);            
        
            Sol_Txdata = B_eval(Sol_torCoeff(1,:),data);
            Sol_Tydata = B_eval(Sol_torCoeff(2,:),data);
            Sol_Tzdata = B_eval(Sol_torCoeff(3,:),data);
            Sol_Tdata = [Sol_Txdata;Sol_Tydata;Sol_Tzdata]'; % Cdata is curve location data.
            
            MaxErTor(NN,n) = max(abs(Sol_Tdata(:)-Cdata(:)));
            AvgErTor(NN,n) = sum(abs(Sol_Tdata(:)-Cdata(:)))/max(size(Sol_Tdata(:)));
            
            %Calculation of BV
            
            pts = VizSurfaceScalar(xcheb,ycheb,zcheb,BV,0.05);
            Npts = FindLargestCompTor(pts);
            Sol_BVCoeff= ExtractCurFromIsoSurface(6,Npts);            
        
            Sol_BVxdata = B_eval(Sol_BVCoeff(1,:),data);
            Sol_BVydata = B_eval(Sol_BVCoeff(2,:),data);
            Sol_BVzdata = B_eval(Sol_BVCoeff(3,:),data);
            Sol_BVdata = [Sol_BVxdata;Sol_BVydata;Sol_BVzdata]'; % Cdata is curve location data.
            
            MaxErBV(NN,n) = max(abs(Sol_BVdata(:)-Cdata(:)));
            AvgErBV(NN,n) = sum(abs(Sol_BVdata(:)-Cdata(:)))/max(size(Sol_BVdata(:)));
            
            toc
    end
end

%%
    er_cheb = MaxErCur;
figure(1)
    plot( N,er_cheb(N,2),'*-c',...
          N,er_cheb(N,3),'*-r',...
          N,er_cheb(N,4),'o-g',...
          N,er_cheb(N,5),'o-r'); hold on;
    legend('k 2','k 3','k 4','k 5');
    text(2,er_cheb(2,2),' k-2');
    text(2,er_cheb(2,3),' k-3');
    text(2,er_cheb(2,4),' k-4');
    text(2,er_cheb(2,5),' k-5');
    xlabel('Polynomial Degree(N)')
    ylabel('Max Dist among 20 points')
    title('Maximum Distance between A-core and C-core lines. (Qpts=N+2+k)')
    %Each curve/line has quadraturepoints(N+2+k)
    %The aim of this graph is to show, as projection basis functions
    %change. Increasing quadrature points will decrease the error in
    %consistent way.
    
    
er_cheb =AvgErCur;
figure(2)
    plot( N,er_cheb(N,2),'*-c',...
          N,er_cheb(N,3),'*-r',...
          N,er_cheb(N,4),'o-g',...
          N,er_cheb(N,5),'o-r'); hold on;
    legend('k 2','k 3','k 4','k 5');
    text(2,er_cheb(2,2),' k-2');
    text(2,er_cheb(2,3),' k-3');
    text(2,er_cheb(2,4),' k-4');
    text(2,er_cheb(2,5),' k-5');
    xlabel('Polynomial Degree(N)')
    ylabel('Avg Dist among 20 points')
    title('Average Distance between A-core and C-core lines. (Qpts=N+2+k)')
    %Each curve/line has quadraturepoints(N+2+k)
    %The aim of this graph is to show, as projection basis functions
    %change. Increasing quadrature points will decrease the error in
    %consistent way.

    
        er_cheb = MaxErTor;
figure(3)
    plot( N,er_cheb(N,2),'*-c',...
          N,er_cheb(N,3),'*-r',...
          N,er_cheb(N,4),'o-g',...
          N,er_cheb(N,5),'o-r'); hold on;
    legend('k 2','k 3','k 4','k 5');
    text(2,er_cheb(2,2),' k-2');
    text(2,er_cheb(2,3),' k-3');
    text(2,er_cheb(2,4),' k-4');
    text(2,er_cheb(2,5),' k-5');
    xlabel('Polynomial Degree(N)')
    ylabel('Max Dist among 20 points')
    title('Maximum Distance between A-core and T-core lines. (Qpts=N+2+k)')
    %Each curve/line has quadraturepoints(N+2+k)
    %The aim of this graph is to show, as projection basis functions
    %change. Increasing quadrature points will decrease the error in
    %consistent way.
    
    
er_cheb =AvgErTor;
figure(4)
    plot( N,er_cheb(N,2),'*-c',...
          N,er_cheb(N,3),'*-r',...
          N,er_cheb(N,4),'o-g',...
          N,er_cheb(N,5),'o-r'); hold on;
    legend('k 2','k 3','k 4','k 5');
    text(2,er_cheb(2,2),' k-2');
    text(2,er_cheb(2,3),' k-3');
    text(2,er_cheb(2,4),' k-4');
    text(2,er_cheb(2,5),' k-5');
    xlabel('Polynomial Degree(N)')
    ylabel('Avg Dist among 20 points')
    title('Average Distance between A-core and T-core lines. (Qpts=N+2+k)')
    %Each curve/line has quadraturepoints(N+2+k)
    %The aim of this graph is to show, as projection basis functions
    %change. Increasing quadrature points will decrease the error in
    %consistent way.

        er_cheb = MaxErBV;
figure(5)
    plot( N,er_cheb(N,2),'*-c',...
          N,er_cheb(N,3),'*-r',...
          N,er_cheb(N,4),'o-g',...
          N,er_cheb(N,5),'o-r'); hold on;
    legend('k 2','k 3','k 4','k 5');
    text(2,er_cheb(2,2),' k-2');
    text(2,er_cheb(2,3),' k-3');
    text(2,er_cheb(2,4),' k-4');
    text(2,er_cheb(2,5),' k-5');
    xlabel('Polynomial Degree(N)')
    ylabel('Max Dist among 20 points')
    title('Maximum Distance between A-core and BV-core lines. (Qpts=N+2+k)')
    %Each curve/line has quadraturepoints(N+2+k)
    %The aim of this graph is to show, as projection basis functions
    %change. Increasing quadrature points will decrease the error in
    %consistent way.
    
    
er_cheb =AvgErBV;
figure(6)
    plot( N,er_cheb(N,2),'*-c',...
          N,er_cheb(N,3),'*-r',...
          N,er_cheb(N,4),'o-g',...
          N,er_cheb(N,5),'o-r'); hold on;
    legend('k 2','k 3','k 4','k 5');
    text(2,er_cheb(2,2),' k-2');
    text(2,er_cheb(2,3),' k-3');
    text(2,er_cheb(2,4),' k-4');
    text(2,er_cheb(2,5),' k-5');
    xlabel('Polynomial Degree(N)')
    ylabel('Avg Dist among 20 points')
    title('Average Distance between A-core and BV-core lines. (Qpts=N+2+k)')
    %Each curve/line has quadraturepoints(N+2+k)
    %The aim of this graph is to show, as projection basis functions
    %change. Increasing quadrature points will decrease the error in
    %consistent way.

