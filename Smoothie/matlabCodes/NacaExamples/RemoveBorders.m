function [res] =  RemoveBorders(elpos)
    
    if nargin ==0
        elpos = linspace(1,5,100);
        elpos = floor(elpos);
    end
    %enlpos = elpos;
    s = sqrt( max(size(elpos)));
    enlpos = reshape(elpos,[s s]);
    %enlpos = padarray(enlpos,[1 1],'both');
    Y = circshift(enlpos,[1,1]);
    X = circshift(enlpos,[-1,-1]);

    
    
    endgeYArray = (enlpos == Y);
    endgeXArray = (enlpos == X);
    endgeArray = (endgeYArray == endgeXArray)
    %disp(enlpos);
    res = enlpos.*endgeArray;
    res = res(2:end-1,2:end-1);
    res = padarray(res,[1 1],'both');
    res = res(:);
    %disp(size(res))
end
