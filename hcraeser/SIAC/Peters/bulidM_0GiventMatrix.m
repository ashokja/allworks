function [M0,c] = bulidM_0GiventMatrix(deg,tMatrix)
    

    [nBsplines, test] = size(tMatrix);
    if (test ~= deg+2)
        echo('Input wrong. Wont get meaning ful results');
    end
    %calculate J
    j = nBsplines-deg-1;
    
    
    M0 = zeros(deg+j+1,deg+j+1);
    h = @monomial;
    %degree is given as deg
    for r = 1:deg+j+1
        for l = 1:deg+j+1
            delta = r-1;
            gamma = l;
            t = tMatrix(gamma,:);
            if l == deg+j+1
                M0(r,l) = dividedDiff(1,deg+2,t,0.0,h,deg+delta+1);
%                  dividedDiff(1,3,t,0.0,h,deg+delta+1)
%                  dividedDiff(2,3,t,0.0,h,deg+delta+1)
%                  dividedDiff(1,2,t,0.0,h,deg+delta+1)
%                  dividedDiff(1,1,t,0.0,h,deg+delta+1)
%                  dividedDiff(2,2,t,0.0,h,deg+delta+1)
            else
                M0(r,l) = dividedDiff(1,deg+2,t,0.0,h,deg+delta+1);
            end
        end
    end
    
    function result = monomial(t,x,der,D)
        y = t-x;
        if y~=0
%            result = prod(D-der+1:D)*(y)^(D-der);
            result = y^(D-der)*prod(1:D)/prod(1:D-der);
        else
            result = 0;
        end
    end

    e1 = zeros(deg+1+j,1);
    e1(1) =1;
    c = M0\e1;
end