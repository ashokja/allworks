#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"

#include <iomanip>

#define MAX_ITER 1000
#define MAX_ITER_NI 100
#define TOLERENCE_S 1e-16
#define TOLERENCE_F 1e-7
#define TOLERENCE_P 1e-16
#define TOLERENCE_A 1e-14
#define TOLERENCE_NN 1e-8
#define ALPHAMAX 1.0

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

bool NewtonIteration( const NekDouble Sx, const NekDouble Sy, const NekDouble scaling,SmoothieSIAC &sm, SmoothieSIAC &smD, 
				NekDouble &sx,NekDouble &sy );

bool NewtonChkAnypt( StdRegions::StdExpansionSharedPtr Sexp,
									const Array<OneD,NekDouble> &u_gPhys, const Array<OneD,NekDouble> &v_gPhys,
                                    const Array<OneD,NekDouble> &u_gPhys_x, const Array<OneD,NekDouble> &u_gPhys_y,
                                    const Array<OneD,NekDouble> &v_gPhys_x, const Array<OneD,NekDouble> &v_gPhys_y,
                                    Array<OneD,NekDouble>  StCoord, Array<OneD,NekDouble> Lcoord );
bool NewtonChkAnyptSIAC( const NekDouble scaling, SmoothieSIAC &sm, SmoothieSIAC &smD, 
			const Array<OneD,NekDouble>  StCoord, Array<OneD,NekDouble> RetCoord );


void NewtonChkAllGauss( StdRegions::StdExpansionSharedPtr Sexp, 
									const Array<OneD,NekDouble> &u_gPhys, const Array<OneD,NekDouble> &v_gPhys,
                                    const Array<OneD,NekDouble> &u_gPhys_x, const Array<OneD,NekDouble> &u_gPhys_y,
                                    const Array<OneD,NekDouble> &v_gPhys_x, const Array<OneD,NekDouble> &v_gPhys_y,
                                                vector<NekDouble> &xsc0, vector<NekDouble> &ysc0);
void NewtonChkAllGaussSIAC( const Array<OneD,NekDouble> &xc0, const Array<OneD,NekDouble> &xc1,
			 const NekDouble scaling, SmoothieSIAC &sm, SmoothieSIAC &smD, 
                                                vector<NekDouble> &xsc0, vector<NekDouble> &ysc0);

bool PrintAllPts( StdRegions::StdExpansionSharedPtr Sexp, 
									LocalRegions::ExpansionSharedPtr fexp,
									const Array<OneD,NekDouble> &u_gPhys, const Array<OneD,NekDouble> &v_gPhys,
                                    const Array<OneD,NekDouble> &u_gPhys_x, const Array<OneD,NekDouble> &u_gPhys_y,
                                    const Array<OneD,NekDouble> &v_gPhys_x, const Array<OneD,NekDouble> &v_gPhys_y,
									int Res,	string fileName );


int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg 0/1"<<endl<<"\t 0 for writing data"<< endl<<"\t 1 for reading data." << endl;
		cout << "3nd arg polynomial degree filter you want to apply" << endl;
		cout << "4rd arg meshscaling you want to use." << endl;
	//	cout << "4th Angle of SIAC derivative in deg -90 to 90 allowed" << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();

//	HNM2D->LoadData( fname+ ".chk",var);

	HNM2D->LoadMesh(var[0]);
	HNM2D->LoadMesh(var[1]);

	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fPhys:" << tNquadPts<< endl;
	cout << "fCoeffs:" << HNM2D->m_expansions[0]->GetNcoeffs()<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);
	Array<OneD,NekDouble> u(tNquadPts);
	Array<OneD,NekDouble> v(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}

	NektarBaseClass k;
	if( 0 == atoi(argv[2]) )
	{	
		k.writeNekArray(xc0,fname+"_xc0."+"txt");
		k.writeNekArray(xc1,fname+"_xc1."+"txt");
		return 0;
	}
	else
	{
		k.readNekArray(u, fname+"_u.txt");
		k.readNekArray(v, fname+"_v.txt");
		//k.printNekArray(v,0);
	}

	// Now use Newton-Rapson to find the zero u and zero v.
	
	HNM2D->m_expansions[0]->FwdTrans_IterPerExp( u,HNM2D->m_expansions[0]->UpdateCoeffs());
	HNM2D->m_expansions[0]->BwdTrans_IterPerExp( HNM2D->m_expansions[0]->GetCoeffs(),
			HNM2D->m_expansions[0]->UpdatePhys());
	
	HNM2D->m_expansions[1]->FwdTrans( v,HNM2D->m_expansions[1]->UpdateCoeffs());
	HNM2D->m_expansions[1]->BwdTrans( HNM2D->m_expansions[1]->GetCoeffs(),
			HNM2D->m_expansions[1]->UpdatePhys());

	Array<OneD,NekDouble> u_DG = HNM2D->m_expansions[0]->GetPhys();	
	Array<OneD,NekDouble> v_DG = HNM2D->m_expansions[1]->GetPhys();

	Array<OneD,NekDouble> ux_DG(tNquadPts), uy_DG(tNquadPts), vx_DG(tNquadPts), vy_DG(tNquadPts);
	HNM2D->m_expansions[0]->PhysDeriv( u_DG, ux_DG, uy_DG);
	HNM2D->m_expansions[1]->PhysDeriv( v_DG, vx_DG, vy_DG);

	SmoothieSIAC2D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[3]), atof(argv[4]), 1);	
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_4kp1, HNM2D, atoi(argv[3]), atof(argv[4]));
	NekDouble scaling = atof(argv[4]);

	HNM2D->m_Arrays.push_back(u_DG);
	HNM2D->m_Arrays.push_back(v_DG);
	HNM2D->LoadExpListIntoRTree();

	NekDouble sx,sy;
	cout << "reached till here" << endl;
	//input seed 
	vector<NekDouble> inPutSx, outPutSx;
	vector<NekDouble> inPutSy, outPutSy;
	
	inPutSx.push_back(0.74);inPutSy.push_back(0.35);
	inPutSx.push_back(0.68);inPutSy.push_back(-0.59);
	inPutSx.push_back(-0.11);inPutSy.push_back(-0.72);
	inPutSx.push_back(-0.58);inPutSy.push_back(-0.64);
	inPutSx.push_back(0.51);inPutSy.push_back(+0.27);
	inPutSx.push_back(-0.12);inPutSy.push_back(-0.84);
//	inPutSx.push_back(-0.15);inPutSy.push_back(-0.85);
//	 inPutSx.push_back(0.7);inPutSy.push_back(0.3);
//	 inPutSx.push_back(0.6);inPutSy.push_back(-0.5);
//	 inPutSx.push_back(-0.1);inPutSy.push_back(-0.7);
//	 inPutSx.push_back(-0.5);inPutSy.push_back(-0.6);
//	 inPutSx.push_back(0.5);inPutSy.push_back(+0.2);
//	 inPutSx.push_back(-0.1);inPutSy.push_back(-0.8);
	//inPutSx.push_back(0.7);inPutSy.push_back(0.3);
	//inPutSx.push_back(-0.1);inPutSy.push_back(+0.15);
	//inPutSx.push_back(-0.1);inPutSy.push_back(-0.7);
	//inPutSx.push_back(-0.5);inPutSy.push_back(-0.6);
	//inPutSx.push_back(0.5);inPutSy.push_back(+0.2);
	//inPutSx.push_back(-0.1);inPutSy.push_back(-0.8);

	for(int i =0 ;i< inPutSx.size();i++)
	{
		NekDouble Sx = inPutSx[i];
		NekDouble Sy = inPutSy[i];

		//Sx = (Sx+1.0)/2.0;
		//Sy = (Sy+1.0)/2.0;
		Sx = (Sx+2.0)/4.0;
		Sy = (Sy+2.0)/4.0;
		bool result = NewtonIteration( Sx,Sy, scaling, sm, smD, sx,sy);
		cout << "result" << result << endl;
	//	cout << "sx:\t" << sx*2.0-1.0 <<"sy:\t"<<sy*2.0-1.0 << endl;
		cout << "sx:\t" << sx*4.0-2.0 <<"sy:\t"<<sy*4.0-2.0 << endl;
		
	//	outPutSx.push_back(sx*2.0-1.0);
	//	outPutSy.push_back(sy*2.0-1.0);
		outPutSx.push_back(sx*4.0-2.0);
		outPutSy.push_back(sy*4.0-2.0);
	}

	for(int i =0 ;i< outPutSx.size();i++)
	{
		cout <<"$"<< outPutSx[i] << "\t\t"<< outPutSy[i] << "\t\t";
		cout <<"$"<< std::sqrt(std::pow((outPutSx[i]-inPutSx[i]),2)+std::pow((outPutSy[i]- inPutSy[i]),2)) << endl;
	}
	for(int i =0 ;i< outPutSx.size();i++)
	{
		cout <<"$"<< std::sqrt(std::pow((outPutSx[i]-inPutSx[i]),2)+std::pow((outPutSy[i]- inPutSy[i]),2)) << endl;
	}
//	SmoothieSIAC2D sm_sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3])); 
	//Array<OneD,NekDouble> directionX(3,0.0), coord(3,0.0), directionY(3,0.0) ;
	//		sm_sm.EvaluateAt(xc0[i],xc1[i],0.0,u_phys[i],valY,valZ,directionX, atof(argv[3]),0);


	//SIAC Seeds checkPoints;
/*
	
	vector<NekDouble> seedSx,seedSy,seedSu,seedSv;
	NekDouble valY,valZ;
	seedSx.clear(); seedSy.clear(); seedSu.clear();seedSv.clear();
	NewtonChkAllGaussSIAC( xc0, xc1, scaling, sm,smD,seedSx ,seedSy );
	Array<OneD,NekDouble> directionX(3,0.0), directionY(3,0.0);
	directionX[0] = 1.0; directionY[1] = 1.0;
	for (int i =0; i< seedSx.size(); i++)
	{
		NekDouble u,v;
		sm.EvaluateAt(seedSx[i],seedSy[i],0.0,u,valY,valZ,directionX, scaling,0);
		sm.EvaluateAt(seedSx[i],seedSy[i],0.0,v,valY,valZ,directionY, scaling,1);
		seedSu.push_back(u);
		seedSv.push_back(v);
	}
	
	k.writeNekArray(seedSx,fname+"_seedX_SI.txt");
	k.writeNekArray(seedSy,fname+"_seedY_SI.txt");
	k.writeNekArray(seedSu,fname+"_seedU_SI.txt");
	k.writeNekArray(seedSy,fname+"_seedV_SI.txt");
*/
	cout << "SIAC are done" << endl;

/*
	// DG Seed below.
	
	vector< Array<OneD,NekDouble> > seedPts;
	vector< NekDouble> seedPtsX, seedPtsY, seedPtsU, seedPtsV;
	seedPts.clear(); seedPtsX.clear(); seedPtsY.clear(); seedPtsU.clear(); seedPtsV.clear();
	// going through each element and checking for a point.
	//Array<OneD,NekDouble> dummyCoord(2,0.0); dummyCoord[0] = 0.87; dummyCoord[1] = 0.675;
	Array<OneD,NekDouble> dummyCoord(2,0.0); dummyCoord[0] = 0.84; dummyCoord[1] = 0.205;
	int dummyIndex = HNM2D->m_expansions[0]->GetExpIndex(dummyCoord);
	for (int i =0; i< HNM2D->m_expansions[0]->GetExpSize(); i++)
	{
	
//		if (!( i==dummyIndex ) )
//		{
//			continue;
//		}
	
		LocalRegions::ExpansionSharedPtr fexp = HNM2D->m_expansions[0]->GetExp(i);
		int NumFCoefs = fexp->GetNcoeffs();
		int numFPhys = fexp->GetTotPoints();
		
		StdRegions::StdExpansionSharedPtr fStdExp = fexp->GetStdExp();
		vector<NekDouble> LseedX, LseedY;
		int elC_Offset = HNM2D->m_expansions[0]->GetCoeff_Offset(i);
		int elP_Offset = HNM2D->m_expansions[0]->GetPhys_Offset(i);

		const Array<OneD,NekDouble> ePhys_u  = u_DG.CreateWithOffset( u_DG, elP_Offset);
		const Array<OneD,NekDouble> ePhys_v  = v_DG.CreateWithOffset( v_DG, elP_Offset);
		const Array<OneD,NekDouble> ePhys_ux = ux_DG.CreateWithOffset( ux_DG, elP_Offset);
		const Array<OneD,NekDouble> ePhys_uy = uy_DG.CreateWithOffset( uy_DG, elP_Offset);
		const Array<OneD,NekDouble> ePhys_vx = vx_DG.CreateWithOffset( vx_DG, elP_Offset);
		const Array<OneD,NekDouble> ePhys_vy = vy_DG.CreateWithOffset( vy_DG, elP_Offset);
//		PrintAllPts( fStdExp, fexp, ePhys_u, ePhys_v, ePhys_ux, ePhys_uy, ePhys_vx, ePhys_vy, 21, fname+"_DEBG_");

		NewtonChkAllGauss(fStdExp, ePhys_u, ePhys_v, ePhys_ux, ePhys_uy, ePhys_vx, ePhys_vy, LseedX, LseedY);

		Array<OneD,NekDouble> lcoord(2), xcoord(3);
		int numSeedsFound = LseedX.size();
		for( int es=0; es< numSeedsFound;es++)
		{	
			lcoord[0] = LseedX[es]; lcoord[1] = LseedY[es];
			Array<OneD,NekDouble> seed(3,0.0);
			fexp->GetCoord(lcoord,seed);
			seedPtsU.push_back(fStdExp->PhysEvaluate(lcoord, ePhys_u));
			seedPtsV.push_back(fStdExp->PhysEvaluate(lcoord, ePhys_v));
			seedPts.push_back(seed);
			//cout << seed[0]*2.0-1.0 << "\t" << seed[1]*2.0-1.0<< "\t" << endl;
			seedPtsX.push_back(seed[0]*2.0-1.0);
			seedPtsY.push_back(seed[1]*2.0-1.0);
		}
	}
	

	k.writeNekArray(seedPtsX,fname+"_seedX_DG.txt");
	k.writeNekArray(seedPtsY,fname+"_seedY_DG.txt");
	k.writeNekArray(seedPtsU,fname+"_seedU_DG.txt");
	k.writeNekArray(seedPtsV,fname+"_seedV_DG.txt");
*/

//	k.writeNekArray(u_DG, fname+"_u_DG.txt");
//	k.writeNekArray(v_DG, fname+"_v_DG.txt");

//	cout << "L2 U: "<< HNM2D->m_expansions[0]->L2(u_DG,u) << endl;	
//	cout << "L2 V: "<<HNM2D->m_expansions[1]->L2(v_DG,v)<< endl;	

//	cout << "Linf U: "<<HNM2D->m_expansions[0]->Linf(u_DG,u) << endl;	
//	cout << "Linf V: "<<HNM2D->m_expansions[1]->Linf(v_DG,v) << endl;	
	return 0;
}


bool NewtonIterationElbyEl()
{
	// particular element under consideration.
	return false;	
}


bool NewtonIteration( const NekDouble Sx, const NekDouble Sy, const NekDouble scaling,SmoothieSIAC &sm, SmoothieSIAC &smD, 
				NekDouble &sx,NekDouble &sy )
{
	// Given seedpoint sx sy;
	int iter =0;
	NekDouble u,v,valY,valZ;
	sx = Sx; sy= Sy;
	Eigen::Matrix2d J;
	Eigen::Vector2d F,P;
	Array<OneD,NekDouble> directionX(3,0.0), directionY(3,0.0);
	directionX[0] = 1.0; directionY[1] = 1.0;
	P(0) = -1.0; P(1) = -1.0;
	while(iter < MAX_ITER)
	{
		smD.EvaluateAt(sx,sy,0.0,J(0,0),valY,valZ,directionX, scaling,0);
		smD.EvaluateAt(sx,sy,0.0,J(0,1),valY,valZ,directionY, scaling,0);
		smD.EvaluateAt(sx,sy,0.0,J(1,0),valY,valZ,directionX, scaling,1);
		smD.EvaluateAt(sx,sy,0.0,J(1,1),valY,valZ,directionY, scaling,1);
		
		sm.EvaluateAt(sx,sy,0.0,F(0),valY,valZ,directionX, scaling,0);
		sm.EvaluateAt(sx,sy,0.0,F(1),valY,valZ,directionY, scaling,1);
		F(0) = -1.0*F(0);	
		F(1) = -1.0*F(1);	
		
		if( std::abs(F(0)) + std::abs(F(1)) <1e-15 )
		{
			cout << "Minimum" << F(0) << "\t" << F(1) << endl;
			cout << "took "<< iter << " iterations " << sx <<"\t"<<sy<< endl;
			return true;
		}
		if( sx < 0.0 || sx > 1.0)
		{
			cout << "x out of bound : " << sx << endl;
		}
		
		if( sy < 0.0 || sy > 1.0)
		{
			cout << "y out of bound : " << sy << endl;
		}
		if ( (abs(P(0)) + abs(P(1))) < TOLERENCE_P )
		{
			cout << "Movement is very small " << TOLERENCE_P << endl;
			cout << "Minimum" << F(0) << "\t" << F(1) << endl;
			return false;
		}
		
		//solveForPk(J,F,p);
		P = J.colPivHouseholderQr().solve(F);
		//UpdateSeed(sx,sy);
		sx = sx+ 0.25*P(0);
		sy = sy+ 0.25*P(1);
		//cout << "J" << endl;
		//cout << J << endl;
		//cout << "P" << endl;
		//cout << P << endl;
		//cout << "F" << endl;
		//cout << F << endl;
		//cout << std::setprecision(19) << sx << "\t" << sy << endl;
		iter++;
	}
	cout << "Minimum" << F(0) << "\t" << F(1) << endl;
	return false;
}

bool PrintAllPts( StdRegions::StdExpansionSharedPtr Sexp, 
									LocalRegions::ExpansionSharedPtr fexp,
									const Array<OneD,NekDouble> &u_gPhys, const Array<OneD,NekDouble> &v_gPhys,
                                    const Array<OneD,NekDouble> &u_gPhys_x, const Array<OneD,NekDouble> &u_gPhys_y,
                                    const Array<OneD,NekDouble> &v_gPhys_x, const Array<OneD,NekDouble> &v_gPhys_y,
									int Res,	string fileName )
{
	int ResX,ResY;ResX = Res;ResY=Res;
	NekDouble spx = 1.0/(ResX-1.0), spy = 1.0/(ResY-1.0);
	vector<NekDouble> xEl_pos(ResX*ResY), yEl_pos(ResX*ResY);
	vector<NekDouble> uEl_pos(ResX*ResY), vEl_pos(ResX*ResY);
	vector<NekDouble> uxEl_pos(ResX*ResY), vxEl_pos(ResX*ResY);
	vector<NekDouble> uyEl_pos(ResX*ResY), vyEl_pos(ResX*ResY);
	Array<OneD,NekDouble> Lcoord(2),seed(2);
	NektarBaseClass k;
			
	for (int i =0; i< ResX; i++)
	{
		for (int j=0; j<ResY; j++)
		{

			Lcoord[0] = i*spx*2.0-1.0; Lcoord[1] = j*spy*2.0-1.0;
			fexp->GetCoord(Lcoord,seed);
			
			//xEl_pos[j*ResY+i] = seed[0];
			//yEl_pos[j*ResY+i] = seed[1];
			xEl_pos[j*ResY+i] = Lcoord[0];
			yEl_pos[j*ResY+i] = Lcoord[1];

			uEl_pos[j*ResY+i] =  Sexp->PhysEvaluate( Lcoord, u_gPhys);
			vEl_pos[j*ResY+i] =  Sexp->PhysEvaluate( Lcoord, v_gPhys);

			uxEl_pos[j*ResY+i] = Sexp->PhysEvaluate( Lcoord, u_gPhys_x);
			uyEl_pos[j*ResY+i] = Sexp->PhysEvaluate( Lcoord, u_gPhys_y);

			vxEl_pos[j*ResY+i] = Sexp->PhysEvaluate( Lcoord, v_gPhys_x);
			vyEl_pos[j*ResY+i] = Sexp->PhysEvaluate( Lcoord, v_gPhys_y);
		}
	}
	k.writeNekArray( xEl_pos,  fileName+"El_x.txt"); 
	k.writeNekArray( yEl_pos,  fileName+"El_y.txt"); 
	k.writeNekArray( uEl_pos,  fileName+"El_u.txt"); 
	k.writeNekArray( vEl_pos,  fileName+"El_v.txt"); 
	k.writeNekArray( uxEl_pos, fileName+"El_ux.txt"); 
	k.writeNekArray( uyEl_pos, fileName+"El_uy.txt"); 
	k.writeNekArray( vxEl_pos, fileName+"El_vx.txt"); 
	k.writeNekArray( vyEl_pos, fileName+"El_vy.txt"); 
	return true;
}


bool NewtonChkAnypt( StdRegions::StdExpansionSharedPtr Sexp, 
									const Array<OneD,NekDouble> &u_gPhys, const Array<OneD,NekDouble> &v_gPhys,
                                    const Array<OneD,NekDouble> &u_gPhys_x, const Array<OneD,NekDouble> &u_gPhys_y,
                                    const Array<OneD,NekDouble> &v_gPhys_x, const Array<OneD,NekDouble> &v_gPhys_y,
                                    const Array<OneD,NekDouble>  StCoord, Array<OneD,NekDouble> RetCoord )
{
	int iter =0;
	//NekDouble u,v,valY,valZ;
	//sx = StCoord[0]; sy= StCoord[1];
	Array<OneD,NekDouble> temp(2,0.0),Lcoordt(3,0.0),Lcoordt1(3,0.0);
	Lcoordt[0] = StCoord[0]; Lcoordt[1]= StCoord[1]; 
	Lcoordt1[0] = StCoord[0]+20; Lcoordt1[1]= StCoord[1]+20; 
	//cout <<Lcoordt[0] << " "<<Lcoordt[1] << endl;
	Eigen::Matrix2d J;
	Eigen::Vector2d F,P,Ptemp, Ftemp;
	P(0) = -1; P(1) = -1;
	//Array<OneD,NekDouble> directionX(3,0.0), directionY(3,0.0);
	//directionX[0] = 1.0; directionY[1] = 1.0;
	NekDouble alpha = 1.0;
	int count =0;
	while(iter < MAX_ITER)
	{
		// Calcualte J
		J(0,0) = Sexp->PhysEvaluate( Lcoordt,u_gPhys_x );
		J(0,1) = Sexp->PhysEvaluate( Lcoordt,u_gPhys_y );
		J(1,0) = Sexp->PhysEvaluate( Lcoordt,v_gPhys_x );
		J(1,1) = Sexp->PhysEvaluate( Lcoordt,v_gPhys_y );
	
		F(0) = Sexp->PhysEvaluate( Lcoordt, u_gPhys);	
		F(1) = Sexp->PhysEvaluate( Lcoordt, v_gPhys);	
		
		F(0) = -1.0*F(0);	
		F(1) = -1.0*F(1);	
		//solveForPk(J,F,p);
		P = J.colPivHouseholderQr().solve(F);
		//cout <<"\t"<<Lcoordt[0] << " "<<Lcoordt[1] <<"\t"<< iter << endl;
		//cout << "\t\t" <<F(0) << " " << F(1) << endl;
		//cout << setprecision(19) <<"\t\t\t" <<F.norm() << endl;
		bool dontExit = true;
		while(dontExit)
		{
		  temp[0] = Lcoordt[0] + alpha*P(0);
		  temp[1] = Lcoordt[1] + alpha*P(1);
		  Ftemp(0) = Sexp->PhysEvaluate( temp, u_gPhys);	
		  Ftemp(1) = Sexp->PhysEvaluate( temp, v_gPhys);	
//			cout << "\t\t\t" <<F.norm() << endl;
//			cout << "\t\t\t" <<Ftemp.norm() << endl;
	
//		  if ( (Ftemp.norm() < F.norm()) && (abs(temp[0]) <1.0 && abs(temp[1])<1.0) )
		  if ( (abs(temp[0]) <1.0 && abs(temp[1])<1.0) )
		  {
				Lcoordt1[0] = temp[0]; Lcoordt1[1] = temp[1];
				alpha = ALPHAMAX;
				break;
		  }else
		  {
				if ( alpha< TOLERENCE_A )
				{
					Lcoordt1[0] = Lcoordt[0]; 
					Lcoordt1[1] = Lcoordt[1]; 
					break;
				}
				alpha = alpha/2.0;
		  }
		}
		
		if ( alpha*P.norm() < TOLERENCE_NN*(1+abs(Lcoordt[0])+abs(Lcoordt[1]) ) && count++ >5 )
		{
	
			RetCoord[0] = Lcoordt1[0];
			RetCoord[1] = Lcoordt1[1];
			if(Ftemp.norm() < TOLERENCE_F)
			{
				return true;
			}
			return false;
		}
		Lcoordt[0] = Lcoordt1[0];	Lcoordt[1] = Lcoordt1[1];
		iter++;
	}
	RetCoord[0] = Lcoordt1[0];
	RetCoord[1] = Lcoordt1[1];
	return false;
}

void NewtonChkAllGaussSIAC( const Array<OneD,NekDouble> &xc0, const Array<OneD,NekDouble> &xc1,
			 const NekDouble scaling, SmoothieSIAC &sm, SmoothieSIAC &smD, 
                                                vector<NekDouble> &xsc0, vector<NekDouble> &ysc0)
{
    Array<OneD,NekDouble> Lcoord(2), OutCoord(2);
    //vector<NekDouble> xsc0,ysc0;
    vector<NekDouble>::iterator itx,ity;
    bool bSeedAlreadyExist, bFoundSeed;
    NekDouble SeedDistTol = TOLERENCE_S;

	for (int p=0; p< xc0.num_elements(); p++)
	{
        Lcoord[0] = xc0[p]; Lcoord[1] = xc1[p];
		bFoundSeed = NewtonChkAnyptSIAC( scaling, sm, smD, 
			Lcoord, OutCoord );
		cout << bFoundSeed << endl;
        if (bFoundSeed)
        {
            bSeedAlreadyExist = false;
            for( itx = xsc0.begin(), ity = ysc0.begin();
                        itx< xsc0.end() && ity< ysc0.end();
                            ++itx,++ity)
            {
                if ( SeedDistTol > (std::abs(*itx-OutCoord[0]) + std::abs(*ity-OutCoord[1])))
                {
                    bSeedAlreadyExist = true;
                }
            }
            if (!bSeedAlreadyExist)
            {
                // Found new zero. Add it into the vector.
                xsc0.push_back(OutCoord[0]);
                ysc0.push_back(OutCoord[1]);
            }
        }

		
	}	
}



void NewtonChkAllGauss( StdRegions::StdExpansionSharedPtr Sexp, 
									const Array<OneD,NekDouble> &u_gPhys, const Array<OneD,NekDouble> &v_gPhys,
                                    const Array<OneD,NekDouble> &u_gPhys_x, const Array<OneD,NekDouble> &u_gPhys_y,
                                    const Array<OneD,NekDouble> &v_gPhys_x, const Array<OneD,NekDouble> &v_gPhys_y,
                                                vector<NekDouble> &xsc0, vector<NekDouble> &ysc0)
{
    int nPhys = Sexp->GetTotPoints();
    Array<OneD,NekDouble> xc0(nPhys), xc1(nPhys);
    Array<OneD,NekDouble> Lcoord(2), OutCoord(2);
    Sexp->GetCoords(xc0,xc1);
    //vector<NekDouble> xsc0,ysc0;
    vector<NekDouble>::iterator itx,ity;
    bool bSeedAlreadyExist, bFoundSeed;
    NekDouble SeedDistTol = TOLERENCE_S;

    for (int p=0; p< nPhys; p++)
    {
        // call newton function with a seed point.
        // get coord and true/false.
        Lcoord[0] = xc0[p]; Lcoord[1] = xc1[p];
        bFoundSeed = NewtonChkAnypt( Sexp, u_gPhys, v_gPhys, u_gPhys_x,
						u_gPhys_y, v_gPhys_x, v_gPhys_y, Lcoord, OutCoord );
        if (bFoundSeed)
        {
            bSeedAlreadyExist = false;
            for( itx = xsc0.begin(), ity = ysc0.begin();
                        itx< xsc0.end() && ity< ysc0.end();
                            ++itx,++ity)
            {
                if ( SeedDistTol > (std::abs(*itx-OutCoord[0]) + std::abs(*ity-OutCoord[1])))
                {
                    bSeedAlreadyExist = true;
                }
            }
            if (!bSeedAlreadyExist)
            {
                // Found new zero. Add it into the vector.
                xsc0.push_back(OutCoord[0]);
                ysc0.push_back(OutCoord[1]);
            }
        }

    }
}


bool NewtonChkAnyptSIAC( const NekDouble scaling, SmoothieSIAC &sm, SmoothieSIAC &smD, 
			const Array<OneD,NekDouble>  StCoord, Array<OneD,NekDouble> RetCoord )
{
    int iter =0;
    NekDouble valY,valZ;
    //sx = StCoord[0]; sy= StCoord[1];
    Array<OneD,NekDouble> temp(2,0.0),Lcoordt(3,0.0),Lcoordt1(3,0.0);
    Lcoordt[0] = StCoord[0]; Lcoordt[1]= StCoord[1];
    Lcoordt1[0] = StCoord[0]; Lcoordt1[1]= StCoord[1];

    Eigen::Matrix2d J;
    Eigen::Vector2d F,P,Ptemp, Ftemp;
    P(0) = -1; P(1) = -1;
    Array<OneD,NekDouble> directionX(3,0.0), directionY(3,0.0);
    directionX[0] = 1.0; directionY[1] = 1.0;
    NekDouble alpha = 1.0;
    while(iter < MAX_ITER)
    {
        // Calcualte J
		smD.EvaluateAt(Lcoordt[0],Lcoordt[1],0.0,J(0,0),valY,valZ,directionX, scaling,0);
		smD.EvaluateAt(Lcoordt[0],Lcoordt[1],0.0,J(0,1),valY,valZ,directionY, scaling,0);
		smD.EvaluateAt(Lcoordt[0],Lcoordt[1],0.0,J(1,0),valY,valZ,directionX, scaling,1);
		smD.EvaluateAt(Lcoordt[0],Lcoordt[1],0.0,J(1,1),valY,valZ,directionY, scaling,1);
		
		sm.EvaluateAt(Lcoordt[0],Lcoordt[1],0.0,F(0),valY,valZ,directionX, scaling,0);
		sm.EvaluateAt(Lcoordt[0],Lcoordt[1],0.0,F(1),valY,valZ,directionY, scaling,1);

        F(0) = -1.0*F(0);
        F(1) = -1.0*F(1);
        //solveForPk(J,F,p);
        P = J.colPivHouseholderQr().solve(F);

        bool dontExit = true;
        while(dontExit)
        {
          temp[0] = Lcoordt[0] + alpha*P(0);
          temp[1] = Lcoordt[1] + alpha*P(1);
          	if ( (abs(temp[0]) <1.0 && abs(temp[1])<1.0) && ((temp[0]) >0.0 && (temp[1])>0.0) )
			{
		  		sm.EvaluateAt(temp[0],temp[1],0.0,Ftemp(0),valY,valZ,directionX, scaling,0);
		  		sm.EvaluateAt(temp[0],temp[1],0.0,Ftemp(1),valY,valZ,directionY, scaling,1);
			}else
			{
				Ftemp(0) = 2.0*F(0);
				Ftemp(1) = 2.0*F(1);
			}
		

          if ( (Ftemp.norm() < F.norm()) && (abs(temp[0]) <1.0 && abs(temp[1])<1.0) && ((temp[0]) >0.0 && (temp[1])>0.0) )
          {
                Lcoordt1[0] = temp[0]; Lcoordt1[1] = temp[1];
                //if (Ftemp.norm() < TOLERENCE_F)
                //{
                //  return true;
                //}
                if (alpha < ALPHAMAX)
                {
                    alpha = alpha*2.0;
                }else
                {
                    alpha = ALPHAMAX;
                }
                break;
          }else
          {
                alpha = alpha/2.0;
                //Ptemp(0) = alpha*P(0); Ptemp(1) = alpha*P(1);
                if ( alpha < TOLERENCE_A)
                {
                    Lcoordt1[0] = Lcoordt[0];
                    Lcoordt1[1] = Lcoordt[1];
                    break;
                }
          }
        }

        if ( ( abs(Lcoordt[0]-Lcoordt1[0]) < TOLERENCE_A) &&  ( abs(Lcoordt[1]-Lcoordt1[1]) < TOLERENCE_A) )
        {
            RetCoord[0] = Lcoordt1[0];
            RetCoord[1] = Lcoordt1[1];

		  	sm.EvaluateAt(Lcoordt1[0],Lcoordt1[1],0.0,F(0),valY,valZ,directionX, scaling,0);
		  	sm.EvaluateAt(Lcoordt1[0],Lcoordt1[1],0.0,F(1),valY,valZ,directionY, scaling,1);

            if ( F.norm() < TOLERENCE_F)
            {
                return true;
            }
            return false;
        }
        Lcoordt[0] = Lcoordt1[0];   Lcoordt[1] = Lcoordt1[1];

/*              if (Ptemp.norm() < TOLERENCE_A)
                {
                    // cannot move anyfuther so exit
                    if (Ftemp.norm() < TOLERENCE_F)
                    {
                        return true;
                    }
                    return false;
                }
*/

        iter++;
    }
    RetCoord[0] = Lcoordt1[0];
    RetCoord[1] = Lcoordt1[1];
    return false;
}


