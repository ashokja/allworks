import papaya.*;

void setup()
{
  size(400,450);
  ArrayList<Float> knotA = new ArrayList<Float> ();
  ArrayList<PVector> pv = new ArrayList<PVector> ();
  knotA.add(0.0); pv.add(new PVector(0.0,0.0,0.0));
  knotA.add(0.0); pv.add(new PVector(1.0,0.0,0.0));
  knotA.add(0.0); pv.add(new PVector(1.0,1.0,0.0));
  knotA.add(0.0); pv.add(new PVector(0.0,1.0,0.0));
  knotA.add(1.0);
  knotA.add(1.0);
  knotA.add(1.0);
  knotA.add(1.5);
  print(b_splineATt(0,1,knotA,1.0)+"\n");
  //print(KnotToInterParam(knotA,3));
  //Matrix Mat;
  // intialize identity array and get its inverse.
  float[][] identMatrix = Mat.identity(4);
  
  for(int i =0; i < 4 ; i++)
  {
    for(int j =0; j < 4; j++)
    {
      print(identMatrix[i][j] + "\t");
    }
    print("\n");
  }
  QR qr= new QR(identMatrix);
  float[] y = new float[4];
  y[0] =1.0; y[1] = 2.0; y[3] = 3.0; y[2] = 4.0;
  double[] x = qr.solve(y);
  print( x[0]+"\t"+x[1]+"\t"+x[2]+"\t"+x[3] );
  
  ArrayList<PVector> pts = dataToControlPoints(pv,knotA,3);
  print("\n"+pts);
  
}

void draw()
{
  stroke(255,102,0);
  background(255,255,255);
  stroke(0,102,255);
  
  // given list of data points and degree and knot vector.
  
  
}
