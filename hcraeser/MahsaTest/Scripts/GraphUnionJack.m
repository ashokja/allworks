%Graph union jack 8-113 for P1, P2, DG and PR.

clear;
close all;
MR = (1:16)*7+1;
%mshEl = [168, 672, 2688, 10752, 43008];
mshEl(1) = 168;    mshEl(2) = 672;    mshEl(3) = 1512;   mshEl(4) = 2688;
mshEl(5) = 4200;   mshEl(6) = 6048;   mshEl(7) = 8232;   mshEl(8) = 10752;
mshEl(9) = 13608;  mshEl(10) = 16800; mshEl(11) = 20328; mshEl(12) = 24192;
mshEl(13) = 28392; mshEl(14) = 32928; mshEl(15) = 37800; mshEl(16) = 43008;

addpath('..');
pPRCartfilename = '../PostP7_2/log_hex_unionjack_PR_CART_MR';
pPRMaxfilename = '../PostP7_1/log_hex_unionjack_PR_MAXSIZE_MR';
pDGCartfilename = '../PostP7_2/log_hex_unionjack_DG_CART_MR';
pDGMaxfilename = '../PostP7_1/log_hex_unionjack_DG_MAXSIZE_MR';

width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 0.4;      % LineWidth
msz = 8;       % MarkerSize
atX = '5p8';


P = '1';
KF ='0';
UF = '0';
count =1;
for mr = MR
    filename = strcat(pPRCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_PR_C(count,:) = ReadFile(filename);
    count= count+1;
end
figure(11)
plot(log10(mshEl),log10(C_P1_PR_C(:,1)),'-b*'); hold on;
plot(log10(mshEl),log10(C_P1_PR_C(:,2)),'-g*'); hold on;
plot(log10(mshEl),log10(C_P1_PR_C(:,3)),'-b.'); hold on;
plot(log10(mshEl),log10(C_P1_PR_C(:,4)),'-g.'); hold off;
title('PR P1 Filter Cart');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;

KF ='0';
count =1;
for mr = MR
    filename = strcat(pPRMaxfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_PR_M(count,:) = ReadFile(filename);
    count= count+1;
end
figure(12)
plot(log10(mshEl),log10(C_P1_PR_M(:,1)),'-b*'); hold on;
plot(log10(mshEl),log10(C_P1_PR_M(:,2)),'-g*'); hold on;
plot(log10(mshEl),log10(C_P1_PR_M(:,3)),'-b.'); hold on;
plot(log10(mshEl),log10(C_P1_PR_M(:,4)),'-g.'); hold off;
title('PR P1 Filter MaxSize');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;

KF ='1';
count =1;
for mr = MR
    filename = strcat(pPRCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_PR_H(count,:) = ReadFile(filename);
    count= count+1;
end
figure(13)
plot(log10(mshEl),log10(C_P1_PR_H(:,1)),'-b*'); hold on;
plot(log10(mshEl),log10(C_P1_PR_H(:,2)),'-g*'); hold on;
plot(log10(mshEl),log10(C_P1_PR_H(:,3)),'-b.'); hold on;
plot(log10(mshEl),log10(C_P1_PR_H(:,4)),'-g.'); hold off;
title('PR P1 Filter Hex');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;

P = '1';
KF ='0';
UF = '0';
count =1;
for mr = MR
    filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_DG_C(count,:) = ReadFile(filename);
    count= count+1;
end
figure(31)
plot(log10(mshEl),log10(C_P1_DG_C(:,1)),'-b*'); hold on;
plot(log10(mshEl),log10(C_P1_DG_C(:,2)),'-g*'); hold on;
plot(log10(mshEl),log10(C_P1_DG_C(:,3)),'-b.'); hold on;
plot(log10(mshEl),log10(C_P1_DG_C(:,4)),'-g.'); hold off;
title('DG P1 Filter Cart');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;

KF ='0';
count =1;
for mr = MR
    filename = strcat(pDGMaxfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_DG_M(count,:) = ReadFile(filename);
    count= count+1;
end
figure(32)
plot(log10(mshEl),log10(C_P1_DG_M(:,1)),'-b*'); hold on;
plot(log10(mshEl),log10(C_P1_DG_M(:,2)),'-g*'); hold on;
plot(log10(mshEl),log10(C_P1_DG_M(:,3)),'-b.'); hold on;
plot(log10(mshEl),log10(C_P1_DG_M(:,4)),'-g.'); hold off;
title('DG P1 Filter MaxSize');

KF ='1';
count =1;
for mr = MR
    filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_DG_H(count,:) = ReadFile(filename);
    count= count+1;
end
figure(33)
plot(log10(mshEl),log10(C_P1_DG_H(:,1)),'-b*'); hold on;
plot(log10(mshEl),log10(C_P1_DG_H(:,2)),'-g*'); hold on;
plot(log10(mshEl),log10(C_P1_DG_H(:,3)),'-b.'); hold on;
plot(log10(mshEl),log10(C_P1_DG_H(:,4)),'-g.'); hold off;
title('DG P1 Filter Hex');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;





% %use HL and VL
% KF ='0';
% P = '1';
% KF ='0';
% UF = '0';
% count =1;
% for mr = MR
%     filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
%     C_P1_PR_C(count,:) = ReadFile(filename);
%     count= count+1;
% end
% figure(3)
% plot(log10(mshEl),log10(C_P1_PR_C(:,1)),'-b*'); hold on;
% plot(log10(mshEl),log10(C_P1_PR_C(:,2)),'-g*'); hold on;
% plot(log10(mshEl),log10(C_P1_PR_C(:,3)),'-b.'); hold on;
% plot(log10(mshEl),log10(C_P1_PR_C(:,4)),'-g.'); hold off;
% title('PR P1 Filter EdgeSize');
% 
% %use HL and VL
% KF ='0';
% P = '2';
% KF ='0';
% UF = '0';
% count =1;
% for mr = MR
%     filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
%     C_P2_DG_C(count,:) = ReadFile(filename);
%     count= count+1;
% end
% figure(2)
% plot(log10(mshEl(1:4)),log10(C_P2_DG_C(1:4,1)),'-r*'); hold on;
% plot(log10(mshEl(1:4)),log10(C_P2_DG_C(1:4,2)),'-g*'); hold on;
% plot(log10(mshEl(1:4)),log10(C_P2_DG_C(1:4,3)),'-r.'); hold on;
% plot(log10(mshEl(1:4)),log10(C_P2_DG_C(1:4,4)),'-g.'); hold off;
% title('DG P2 Filter EdgeSize');
% 







P = '2';
KF ='0';
UF = '0';
count =1;
for mr = MR
    filename = strcat(pPRCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_PR_C(count,:) = ReadFile(filename);
    count= count+1;
end
figure(21)
plot(log10(mshEl),log10(C_P2_PR_C(:,1)),'-b*'); hold on;
plot(log10(mshEl),log10(C_P2_PR_C(:,2)),'-g*'); hold on;
plot(log10(mshEl),log10(C_P2_PR_C(:,3)),'-b.'); hold on;
plot(log10(mshEl),log10(C_P2_PR_C(:,4)),'-g.'); hold off;
title('PR P2 Filter Cart');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;

KF ='0';
count =1;
for mr = MR
    filename = strcat(pPRMaxfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_PR_M(count,:) = ReadFile(filename);
    count= count+1;
end
figure(22)
plot(log10(mshEl),log10(C_P2_PR_M(:,1)),'-b*'); hold on;
plot(log10(mshEl),log10(C_P2_PR_M(:,2)),'-g*'); hold on;
plot(log10(mshEl),log10(C_P2_PR_M(:,3)),'-b.'); hold on;
plot(log10(mshEl),log10(C_P2_PR_M(:,4)),'-g.'); hold off;
title('PR P2 Filter MaxSize');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;

KF ='1';
count =1;
for mr = MR
    filename = strcat(pPRCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_PR_H(count,:) = ReadFile(filename);
    count= count+1;
end
figure(23)
plot(log10(mshEl),log10(C_P2_PR_H(:,1)),'-b*'); hold on;
plot(log10(mshEl),log10(C_P2_PR_H(:,2)),'-g*'); hold on;
plot(log10(mshEl),log10(C_P2_PR_H(:,3)),'-b.'); hold on;
plot(log10(mshEl),log10(C_P2_PR_H(:,4)),'-g.'); hold off;
title('PR P2 Filter Hex');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;





P = '2';
KF ='0';
UF = '0';
count =1;
for mr = MR
    filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_DG_C(count,:) = ReadFile(filename);
    count= count+1;
end
figure(41)
plot(log10(mshEl),log10(C_P2_DG_C(:,1)),'-b*'); hold on;
plot(log10(mshEl),log10(C_P2_DG_C(:,2)),'-g*'); hold on;
plot(log10(mshEl),log10(C_P2_DG_C(:,3)),'-b.'); hold on;
plot(log10(mshEl),log10(C_P2_DG_C(:,4)),'-g.'); hold off;
title('DG P2 Filter Cart');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;

KF ='0';
count =1;
for mr = MR
    filename = strcat(pDGMaxfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_DG_M(count,:) = ReadFile(filename);
    count= count+1;
end
figure(42)
plot(log10(mshEl),log10(C_P2_DG_M(:,1)),'-b*'); hold on;
plot(log10(mshEl),log10(C_P2_DG_M(:,2)),'-g*'); hold on;
plot(log10(mshEl),log10(C_P2_DG_M(:,3)),'-b.'); hold on;
plot(log10(mshEl),log10(C_P2_DG_M(:,4)),'-g.'); hold off;
title('DG P2 Filter MaxSize');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;

KF ='1';
count =1;
for mr = MR
    filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_DG_H(count,:) = ReadFile(filename);
    count= count+1;
end
figure(43)
plot(log10(mshEl),log10(C_P2_DG_H(:,1)),'-b*'); hold on;
plot(log10(mshEl),log10(C_P2_DG_H(:,2)),'-g*'); hold on;
plot(log10(mshEl),log10(C_P2_DG_H(:,3)),'-b.'); hold on;
plot(log10(mshEl),log10(C_P2_DG_H(:,4)),'-g.'); hold off;
title('DG P2 Filter Hex');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;




figure(51)
plot(log10(mshEl),log10(C_P1_PR_C(:,1)),'-b*'); hold on;
plot(log10(mshEl),log10(C_P1_PR_C(:,2)),'-g*'); hold on;
plot(log10(mshEl),log10(C_P1_PR_M(:,2)),'-c*'); hold on;
plot(log10(mshEl),log10(C_P1_PR_H(:,2)),'-r*'); hold on;
title('Projection P1 Filter L2');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;
legend('Before','Post(Cart)','Post(Max)','Post(Hex)')

figure(52)
plot(log10(mshEl),log10(C_P1_PR_C(:,3)),'-bo'); hold on;
plot(log10(mshEl),log10(C_P1_PR_C(:,4)),'-go'); hold on;
plot(log10(mshEl),log10(C_P1_PR_M(:,4)),'-co'); hold on;
plot(log10(mshEl),log10(C_P1_PR_H(:,4)),'-ro'); hold on;
title('Projection P1 Filter Linf');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;
legend('Before','Post(Cart)','Post(Max)','Post(Hex)')

figure(53)
plot(log10(mshEl),log10(C_P1_DG_C(:,1)),'-b*'); hold on;
plot(log10(mshEl),log10(C_P1_DG_C(:,2)),'-g*'); hold on;
plot(log10(mshEl),log10(C_P1_DG_M(:,2)),'-c*'); hold on;
plot(log10(mshEl),log10(C_P1_DG_H(:,2)),'-r*'); hold on;
title('DG P1 Filter L2');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;
legend('Before','Post(Cart)','Post(Max)','Post(Hex)')

figure(54)
plot(log10(mshEl),log10(C_P1_DG_C(:,3)),'-bo'); hold on;
plot(log10(mshEl),log10(C_P1_DG_C(:,4)),'-go'); hold on;
plot(log10(mshEl),log10(C_P1_DG_M(:,4)),'-co'); hold on;
plot(log10(mshEl),log10(C_P1_DG_H(:,4)),'-ro'); hold on;
title('DG P1 Filter Linf');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;
legend('Before','Post(Cart)','Post(Max)','Post(Hex)')

figure(61)
plot(log10(mshEl),log10(C_P2_PR_C(:,1)),'-b*'); hold on;
plot(log10(mshEl),log10(C_P2_PR_C(:,2)),'-g*'); hold on;
plot(log10(mshEl),log10(C_P2_PR_M(:,2)),'-c*'); hold on;
plot(log10(mshEl),log10(C_P2_PR_H(:,2)),'-r*'); hold on;
title('Projection P2 Filter L2');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;
legend('Before','Post(Cart)','Post(Max)','Post(Hex)')

figure(62)
plot(log10(mshEl),log10(C_P2_PR_C(:,3)),'-bo'); hold on;
plot(log10(mshEl),log10(C_P2_PR_C(:,4)),'-go'); hold on;
plot(log10(mshEl),log10(C_P2_PR_M(:,4)),'-co'); hold on;
plot(log10(mshEl),log10(C_P2_PR_H(:,4)),'-ro'); hold on;
title('Projection P2 Filter Linf');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;
legend('Before','Post(Cart)','Post(Max)','Post(Hex)')

LC = 11;
figure(63)
plot(log10(mshEl(1:LC)),log10(C_P2_DG_C(1:LC,1)),'-b*'); hold on;
plot(log10(mshEl(1:LC)),log10(C_P2_DG_C(1:LC,2)),'-g*'); hold on;
plot(log10(mshEl(1:LC)),log10(C_P2_DG_M(1:LC,2)),'-c*'); hold on;
plot(log10(mshEl(1:LC)),log10(C_P2_DG_H(1:LC,2)),'-r*'); hold on;
title('DG P2 Filter L2');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;
legend('Before','Post(Cart)','Post(Max)','Post(Hex)')

figure(64)
plot(log10(mshEl(1:LC)),log10(C_P2_DG_C(1:LC,3)),'-bo'); hold on;
plot(log10(mshEl(1:LC)),log10(C_P2_DG_C(1:LC,4)),'-go'); hold on;
plot(log10(mshEl(1:LC)),log10(C_P2_DG_M(1:LC,4)),'-co'); hold on;
plot(log10(mshEl(1:LC)),log10(C_P2_DG_H(1:LC,4)),'-ro'); hold on;
title('DG P2 Filter Linf');
xlabel('log10(MshSize)');
ylabel('log10(Error)')
axis equal;
legend('Before','Post(Cart)','Post(Max)','Post(Hex)')


saveas(51,'images/PR_P1_L2.jpg','jpg')
saveas(52,'images/PR_P1_LI.jpg','jpg')
saveas(53,'images/DG_P1_L2.jpg','jpg')
saveas(54,'images/DG_P1_LI.jpg','jpg')
saveas(61,'images/PR_P2_L2.jpg','jpg')
saveas(62,'images/PR_P2_LI.jpg','jpg')
saveas(63,'images/DG_P2_L2.jpg','jpg')
saveas(64,'images/DG_P2_LI.jpg','jpg')
