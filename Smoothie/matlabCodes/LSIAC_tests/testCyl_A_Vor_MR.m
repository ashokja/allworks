clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [81];

poly =2;
order = 2;
alpha =1;
count =1;

Angles = [0,45,90];
%Angles = [60,75,90];
%Angles = [0,15,30,45];
fileName = 'Cy3l_Re500_';
spacingStrg='0.657';
spacingStrgS='0p657';
%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize
atX = '5p8';

Cyl_Max = 6.3;
Cyl_Min = -5.2571;

limits=[2.5 4.5];


for angle = Angles
    r = 81;
    filenameFrPrint = strcat(fileName,int2str(poly),'_A_',int2str(angle),'_',spacingStrgS);
    xfname = strcat('datatxt/',fileName,int2str(order),'_A_',int2str(angle),'_pX_2D_OneSided2kp1',int2str(r),spacingStrg,'.txt' );
    yfname = strcat('datatxt/',fileName,int2str(order),'_A_',int2str(angle),'_pY_2D_OneSided2kp1',int2str(r),spacingStrg,'.txt' );
        
    pI_pfname = strcat('datatxt/',fileName,int2str(order),'_A_',int2str(angle),'_pI_p_2D_OneSided2kp1',int2str(r),spacingStrg,'.txt' );
    pI_fname = strcat('datatxt/',fileName,int2str(order),'_A_',int2str(angle),'_pI_2D_OneSided2kp1',int2str(r),spacingStrg,'.txt' );
    
    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');
        
    pI_pval = textread(pI_pfname, '%f\t');     pI_val = textread(pI_fname, '%f\t');
    
    s = sqrt(max(size(xpos)));
    xVpos = reshape(xpos,s,s);    yVpos = reshape(ypos,s,s);    
    pI_pVal = reshape(pI_pval,s,s);     pI_Val = reshape(pI_val,s,s);
    
    
%Curvature,     

figure(18)
    surf(xVpos,yVpos, pI_pVal,'EdgeAlpha',0.1); 
    colorbar;
    title(strcat('Proj P',int2str(poly),' Vorticity'));
    %view([0,90]);
    %saveas(18,strcat(fileName,'_',int2str(r),'_A_',int2str(angle),'_','p_C'),'jpg');

figure(19)
    surf(xVpos,yVpos, pI_Val,'EdgeAlpha',0.1); 
    colorbar;
    title(strcat('SIAC P',int2str(poly),' Vorticity'));
    %view([0,90]);    
    %saveas(19,strcat(fileName,'_',int2str(r),'_A_',int2str(angle),'_','S_C'),'jpg');

figure(20)
   contour(xVpos,yVpos, -1.0*pI_Val,[Cyl_Min :.3: Cyl_Max],'LineWidth',lw ); 
   axis off;
%print(strcat('paperUseImagesF/',filenameFrPrint,'Surf_SIAC_Contour'), '-dpng','-r500');
%plotNekMesh();
%line([3.7,3.7],[-4,4],'lineStyle','--','Color',[0.5 0.5 0.5],'linewidth',lw); hold on;
   axis equal;
   xlim(limits); 
   ylim([-4,4]);
   print(strcat('paperUseImagesF/','AngleLSIAC/',filenameFrPrint,'LSIACN_c3'), '-dpng','-r500');
%   print(strcat('paperUseImagesF/',filenameFrPrint,'Surf_SIAC_Contour_MESH'), '-dpng','-r500');

    
% figure(20)
%     surf(xVpos,yVpos, pI_pVal,'EdgeAlpha',0.1); 
%     colorbar;
%     title(strcat('Proj P',int2str(poly),' Vorticity'));
%     view([0,90]);
%     %saveas(20,strcat(fileName,'_',int2str(r),'_A_',int2str(angle),'_','p_C_v2'),'jpg');
% 
% figure(21)
%     surf(xVpos,yVpos, pI_Val,'EdgeAlpha',0.1); 
%     colorbar;
%     title(strcat('SIAC P',int2str(poly),' Vorticity'));
%     view([0,90]);    
%    % saveas(21,strcat(fileName,'_',int2str(r),'_A_',int2str(angle),'_','S_C_v2'),'jpg');
%     
% figure(22)
%     surf(xVpos,yVpos, pI_pVal ,'EdgeAlpha',0.1); 
%     colorbar;
%     title(strcat('Proj P',int2str(poly),'_A_',int2str(angle),' C'));
%     view([0,90]);
%     
% figure(23)
%     contour(xVpos,yVpos, pI_Val,[min(pI_Val(:)) :.1: max(pI_Val(:))] ); 
%     %saveas(23,strcat(fileName,'_',int2str(r),'_A_',int2str(angle),'_','S_C_v3'),'jpg');
%     
% figure(24)
%     contour(xVpos,yVpos, pI_pVal,[min(pI_pVal(:)) :.1: max(pI_pVal(:))] ); 
%     %saveas(24,strcat(fileName,'_',int2str(r),'_A_',int2str(angle),'_','P_C_v3'),'jpg');
%     
%     
end

