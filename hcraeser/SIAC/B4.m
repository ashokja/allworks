function v = B4(x)
    %x = linspace(-2,2, 4 * x);
    
    v = zeros(size(x));
    
    I = find(abs(x)>=1 & abs(x)<=2);
    v(I) = ((2-abs(x(I))).^3)./6;
    
    I = find(abs(x)>=0 & abs(x)<=1);
    v(I) = (4-6.*abs(x(I)).^2+3.*abs(x(I)).^3)./6;
    
    %plot(x,v)
return