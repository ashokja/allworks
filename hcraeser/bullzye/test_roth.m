%test_roth idea
clear;
close all;
addpath ../VortexOnBasisEl;
addpath ../Bfuncs;
addpath ../createTestData;
av = -66.5; bv = 8;
N = [7,8,9,10,11,12];
%N = [4,5];
coeff_poly = {}; dataV_poly = {};
count = 1;
check = 1
nodes = 25;
%%
for n = N
%    for ratio = (0.1:.1:1)
    for ratio = 0.4
    %n =5;
%    [xy,distance,t_a,mapxy,mapxyV,curveCoeff] = test_runNodes(1,15,3,1,2,ratio);
    curveDataIn = linspace(-1,1,nodes);
    mapxy = changeGridToArray(curveDataIn);
    [xy,distance,t_a,mapxyV,curveCoeff] = test_runXYZExt(1,mapxy);
    [coeff] = buildCoeff(n,n,n,mapxy,mapxyV);
    Vbar = B3_der_eval(n,n,n,coeff,mapxy);
    Vel = B3_eval(n,n,n,coeff,mapxy);
    %%
    f1 = figure(1);
    drawStreamLine(mapxy,Vel,curveCoeff);
    view(av,bv);
    V_bar = reshape(Vbar,[],3,3);
    [s_Mxy,~] = size(mapxy);
    dataEigComp = zeros(s_Mxy,1);
    
    Acceler = zeros(size(mapxy));
    MapxyCurvature = zeros(size(mapxy));
    check =2
    %%
    for i =1:s_Mxy
        pos = mapxy(i,:);
        jj(:,:) = V_bar(i,:,:);
        v = Vel(i,:);
        jj = jj';
        a = jj*v';
%        a = a/norm(a);
        Acceler(i,:) = a';
        vnorm = v/norm(v);
        anorm = a/norm(a);
        curvature = cross(v,a)/((norm(v))^3);
        MapxyCurvature(i,:) = curvature;
    end
    check =3
    %% Curvature
    f2 = figure(2);
    drawStreamLine(mapxy,MapxyCurvature,curveCoeff); 
    title('Curvature');colorbar; caxis([0,1]); view(av,bv);
    %%
    %%Acceleration
    f3 = figure(3);
    drawStreamLine(mapxy,Acceler,curveCoeff);
    title('Accerleration');colorbar; caxis([0,1]); view(av,bv);
    check =4
    %%
    
    AccCoeff = buildCoeff(n,n,n,mapxy,Acceler);
    Accbar = B3_der_eval(n,n,n,AccCoeff,mapxy);
    Acc_bar = reshape(Accbar,[],3,3);
    MapxyTorsion = zeros(size(mapxy));
    biNormalMap =  zeros(size(mapxy));
    %%
    for i = 1:s_Mxy
       aa(:,:) = Acc_bar(i,:,:);
       aa = aa';
       v = Vel(i,:);
       a = Acceler(i,:);
%              a = a/norm(a);
       b = aa*v';
       biNormalMap(i,:) = b;
%       b = b/norm(b);
        %cva = cross(v,a);
       vtor = v/norm(v);
       btor = b/norm(b);
       torsion = dot(cross(v,a),b);
       MapxyTorsion(i,:) = torsion/(norm(cross(v,a))^2+0.1);
%       torsion = cross(vtor,btor);
%       MapxyTorsion(i,:) = torsion;
    end
    check =5
    %%
    f4 = figure(4);
    drawStreamLine(mapxy,MapxyTorsion,curveCoeff);
    title('Torsion');colorbar; caxis([0,1]); view(av,bv);
    %%
    f5 = figure(5);
    drawStreamLine(mapxy,biNormalMap,curveCoeff);
    title('Binormal');colorbar; caxis([0,1]); view(av,bv);
    %%
    f6 = figure(6);
    drawIsoSurfaceScalar(mapxy,MapxyTorsion,curveCoeff,0);
%    drawIsoSurface(mapxy,MapxyTorsion,curveCoeff,0.09);
%    drawIsoSurface(mapxy,MapxyTorsion,curveCoeff,0.15);
    title('Iso surfaces Torsion');colorbar; caxis([0,1]); view(av,bv);
%%    
    f7 = figure(7);
    drawIsoSurface(mapxy,MapxyCurvature,curveCoeff,0.00);
    drawIsoSurface(mapxy,MapxyCurvature,curveCoeff,0.09);
%    drawIsoSurface(mapxy,MapxyCurvature,curveCoeff,0.15);
    title('Iso surfaces Curvature');colorbar; caxis([0,1]); view(av,bv);
    %%
%      savefig(f1,strcat('images/vel_b_W_N_10_p_21_',int2str(ratio*10)));
%      savefig(f2,strcat('images/acc_b_W_N_10_p_21_',int2str(ratio*10)));
%      savefig(f3,strcat('images/binor_b_W_N_10_p_21_',int2str(ratio*10)));
%      savefig(f4,strcat('images/tor_b_W_N_10_p_21_',int2str(ratio*10)));
%      savefig(f5,strcat('images/cur_b_W_N_10_p_21_',int2str(ratio*10)));
%      savefig(f7,strcat('images/CurIsoSur_b_W_N_10_p_21_',int2str(ratio*10)));
      savefig(f6,strcat('images/TorIsoSur_N_',int2str(n)));      
      close all;
%      save(strcat('testdata/data_mat',int2str(ratio*10),'_.mat'));    
    end 
end
%%

%%