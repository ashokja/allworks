% directly projecting the velocity on plane.
clear;
n = 7;
addpath ../VortexOnBasisEl;
[xy,distance,t_a,mapxy,mapxyV,curveCoeff] = test_runNodes(1,n+4,1);

[coeff] = buildCoeff(n,n,n,mapxy,mapxyV);
dataV = buildData(n,n,n,coeff,mapxy);
figure(1)
    drawStreamLine(mapxy,dataV,curveCoeff);
    hold off
    title(strcat('Input Velocity',int2str(n)));
    xlabel('x'); ylabel('y'); zlabel('z'); hold off;
    view(-110.5,16);
    %%
figure(112);
[sol_seeds,goodSeeds,solAtSeeds] = zerosAtProjected(n,n,n,coeff,3);
%%
figure(113);
plotBoundingBox();hold on;
plot3(sol_seeds(:,:,1).*goodSeeds,sol_seeds(:,:,2).*goodSeeds,sol_seeds(:,:,3).*goodSeeds,'g*'); hold on;
drawOnlyCurve(curveCoeff);

