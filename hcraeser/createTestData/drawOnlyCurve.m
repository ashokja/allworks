function [] = drawOnlyCurve(coeff)
    switch nargin
        case 1,
            curveSampledata = 20;
            cdata = linspace(-1,1,curveSampledata);
    end
    cxdata = B_eval(coeff(1,:),cdata); 
    cydata = B_eval(coeff(2,:),cdata);
    czdata = B_eval(coeff(3,:),cdata);
%     xderdata = B_der_eval(coeff(1,:),data);
%     yderdata = B_der_eval(coeff(2,:),data);
%     zderdata = B_der_eval(coeff(3,:),data);
    
    plot3(cxdata,cydata,czdata,'r'); hold on;
    plotBoundingBox();
    
end