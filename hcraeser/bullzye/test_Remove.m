%clear
pts = rand(10,3)*2-1;
ptsKp = pts;
x = (1:10);
[X,Y,Z] = meshgrid(x,x,x);
spts = max(size(pts));
pts = (pts +1)/2*10 -0.0001;
pts = floor(pts)+1;

Img = zeros(10,10,10);
CImg = Img;
for i = 1:spts
    p = pts(i,:);
    Img(p(1),p(2),p(3)) =1;
end

CC = bwconncomp(Img);
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest,idx] = max(numPixels);
CImg(CC.PixelIdxList{idx}) = 1;

count = 1;
for i = 1:spts
    p = pts(i,:);
    if CImg(p(1),p(2),p(3))==1
        Npts(count,:) = ptsKp(i,:);
    end
end
