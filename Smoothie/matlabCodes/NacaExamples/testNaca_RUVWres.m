% testNaca_RUre.cpp evaluate.
% testNaca_RVre.cpp evaluate.
% testNaca_RWre.cpp evaluate.

clear;
close all;

UsebkupData = 'cur_feb11/'

%UsebkupData = ''
%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05'
filename = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ACRFDBVar' );
filepath = strcat('data/',UsebkupData,filename);

xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );

U_sfname = strcat(filepath,'_pU_s.txt' );
U_ssfname = strcat(filepath,'_pU_ss.txt' );
V_sfname = strcat(filepath,'_pV_s.txt' );
V_ssfname = strcat(filepath,'_pV_ss.txt' );
W_sfname = strcat(filepath,'_pW_s.txt' );
W_ssfname = strcat(filepath,'_pW_ss.txt' );

xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');

%prespos = textread(presfname, '%f\t');
%presspos = textread(pressfname, '%f\t');

U_spos = textread(U_sfname, '%f\t');
U_sspos = textread(U_ssfname, '%f\t');
V_spos = textread(V_sfname, '%f\t');
V_sspos = textread(V_ssfname, '%f\t');
W_spos = textread(W_sfname, '%f\t');
W_sspos = textread(W_ssfname, '%f\t');



epos = floor(epos +0.0001);
s = floor( (max(size(xpos)))^(1/2) +0.1);
    
%xVpos = reshape(xpos,s,s,s);
%yVpos = reshape(ypos,s,s,s);
%zVpos = reshape(zpos,s,s,s);
%eVpos = reshape(epos,s,s,s);

    xVpos = reshape(xpos,s,s);
%    xVpos = permute(xVpos,[2,3,1]);
    yVpos = reshape(ypos,s,s);
%    yVpos = permute(yVpos,[2,3,1]);
    zVpos = reshape(zpos,s,s);
%    zVpos = permute(zVpos,[2,3,1]);


U_sVpos = reshape (U_spos,s,s);
U_ssVpos = reshape (U_sspos,s,s);
V_sVpos = reshape (V_spos,s,s);
V_ssVpos = reshape (V_sspos,s,s);
W_sVpos = reshape (W_spos,s,s);
W_ssVpos = reshape (W_sspos,s,s);

figure(1);
subplot(1,3,1),surf(yVpos,zVpos,-1*U_sVpos);
shading interp;
subplot(1,3,2),surf(yVpos,zVpos,-1*U_ssVpos);
shading interp;
subplot(1,3,3),surf(yVpos,zVpos,U_ssVpos-U_sVpos);
shading interp;

figure(2);
subplot(1,3,1),surf(yVpos,zVpos,V_sVpos);
shading interp;
subplot(1,3,2),surf(yVpos,zVpos,V_ssVpos);
shading interp;
subplot(1,3,3),surf(yVpos,zVpos,V_ssVpos-V_sVpos);
shading interp;

figure(3);
subplot(1,3,1),surf(yVpos,zVpos,W_sVpos);
shading interp;
subplot(1,3,2),surf(yVpos,zVpos,W_ssVpos);
shading interp;
subplot(1,3,3),surf(yVpos,zVpos,W_ssVpos-W_sVpos);
shading interp;



figure(4);
subplot(1,3,1),contour(yVpos,zVpos,U_sVpos);
subplot(1,3,2),contour(yVpos,zVpos,U_ssVpos);
subplot(1,3,3),contour(yVpos,zVpos,U_ssVpos-U_sVpos);

figure(5);
subplot(1,3,1),contour(yVpos,zVpos,V_sVpos);
subplot(1,3,2),contour(yVpos,zVpos,V_ssVpos);
subplot(1,3,3),contour(yVpos,zVpos,V_ssVpos-V_sVpos);

figure(6);
subplot(1,3,1),contour(yVpos,zVpos,W_sVpos);
subplot(1,3,2),contour(yVpos,zVpos,W_ssVpos);
subplot(1,3,3),contour(yVpos,zVpos,W_ssVpos-W_sVpos);



