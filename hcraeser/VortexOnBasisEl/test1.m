clear;
[n1,w1] = lglnodes(10);
[n2,w2] = lglnodes(10);
%ep2 = JacobiGQ(0,0,5);
%ep1 = JacobiGQ(0,0,5);

%n1 = (-1:.2:1);
%n2 = (-1:.2:1);

[x,y] = meshgrid(n1,n2);

[n1_size,~] = size(n1);
[n2_size,~] = size(n2);
pn1 = 2*x;
pn2 = -2*y;
figure (1);
subplot(2,2,1), quiver(x,y,pn1,pn2);

count = 1;
for e1 = 1:n1_size
    for e2 = 1:n2_size-1
        ep1(count) = (1+ n1(e1))*(1-n2(e2))/2-1;
        ep2(count) = n2(e2);
        count = count +1;
    end
end
    ep1(count) = -1;
    ep2(count) = 1;

%plot(ep1,ep2,'*');
px = 2*(ep1+0.5);
py = -2*(ep2+0.65);
subplot(2,2,2),
quiver(ep1,ep2,px,py);


