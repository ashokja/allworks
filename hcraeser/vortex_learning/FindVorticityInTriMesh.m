% test for paper equation 1.
function [attach_lines, sep_lines] = FindVorticityInTriMesh(X,Y,px,py)
%saddle test.
    switch nargin
        case 0
            x = (-1:.15:1);
            y = (-1:.15:1);
            [X,Y] = meshgrid( x,y );
            %Z= X.^2 -Y.^2;
            %[px,py] = gradient(Z);
            px = 2*(X)+1;
            py = -2*(Y)-1;
                %mag = sqrt(px.*px + py.*py);
                %px = px./mag;
                %py = py./mag;
    end
quiver(X,Y,px,py,'b');
hold on;
    [X_size,~] = size(X);
    [~,Y_size] = size(Y);

    coords1 = zeros(3,2);
    coords2 = zeros(3,2);
    vel1 = zeros(3,2);
    vel2 = zeros(3,2);
    attach_lines = [];
    sep_lines = [];
    for i = 1: X_size-1
        for j = 1:Y_size-1
            %triangle one.
            coords1(2,:) = [X(i,j),Y(i,j)];
            coords1(1,:) = [X(i+1,j),Y(i+1,j)];
            coords1(3,:) = [X(i,j+1),Y(i,j+1)];
            vel1(2,:) = [px(i,j),py(i,j)];
            vel1(1,:) = [px(i+1,j),py(i+1,j)];
            vel1(3,:) = [px(i,j+1),py(i,j+1)];
            [a_l,s_l] = FindVorticityTri(coords1,vel1);
            if ~isempty(a_l)
                 attach_lines(end+1,:,:) = a_l;
            end
            if ~isempty(s_l)
                sep_lines(end+1,:,:) = s_l;
            end
            %triangle two.
            coords2(2,:) = [X(i+1,j),Y(i+1,j)];
            coords2(1,:) = [X(i+1,j+1),Y(i+1,j+1)];
            coords2(3,:) = [X(i,j+1),Y(i,j+1)];
            vel2(2,:) = [px(i+1,j),py(i+1,j)];
            vel2(1,:) = [px(i+1,j+1),py(i+1,j+1)];
            vel2(3,:) = [px(i,j+1),py(i,j+1)];
            [a_l,s_l] = FindVorticityTri(coords2,vel2);
             if ~isempty(a_l)
                 attach_lines(end+1,:,:) = a_l;
            end
            if ~isempty(s_l)
                sep_lines(end+1,:,:) = s_l;
            end
        end
    end

    %% Draw the plots.
    [num_att,~,~] = size(attach_lines);
    for i = 1:num_att
        plot( attach_lines(i,:,1), attach_lines(i,:,2),'g');
        hold on;
    end

    [num_att,~,~] = size(sep_lines);
    for i = 1:num_att
        plot( sep_lines(i,:,1), sep_lines(i,:,2),'r');
        hold on;
    end
    hold off;
end