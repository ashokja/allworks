function [t] = findParamTCloseToLine(c_lpts,c_rpts,pts)
    p1 = ones(size(pts));
    p1(:,1) = c_lpts(1); p1(:,2) = c_lpts(2); p1(:,3) = c_lpts(3);

    t = -(p1-pts)*(c_rpts-c_lpts)'/norm(c_rpts-c_lpts)^2;
    % step to convert t to [-1,1] range
    t = 2*t -1;
    
end