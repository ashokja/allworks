% open SIAC test.

%% Test mass matrix mapping with more mapping points.
clear;
close all;
addpath('../PolyLib');
addpath('../SIAC');

Degrees =(3);
for Deg = Degrees
%Deg = 4;
    a = 0; b = 2*pi;
    Nv = 4;
    Ne = Nv-1;
	filter_H = (b-a)/Ne;
	filter_m = 0.5;
    v_x = a+ linspace(0,1,Nv)*(b-a);
	siacSamples = Deg+3;
    vizSamples= Deg+3;
    np = Deg+3;
    [r,w] = JacobiGLZW(np,0,0);

    x = zeros(np,Ne);
    h_e = zeros(Ne,1);
    for e = 1:Ne
        x(:,e) = v_x(e) + (r+1)/2*(v_x(e+1)-v_x(e));
        h_e(e) = v_x(e+1)-v_x(e);
    end
    % calculate u_act using function 
    u_act = sin(x);

    %Build and use mass matrix for coeffecints.
    M = zeros(Deg+1,Deg+1);
    for i = 0:Deg
        for j =0:Deg
            M(i+1,j+1) = sum( JacobiPoly(i,r,0,0) ...
                                .*JacobiPoly(j,r,0,0) .* ...
                                w);
        end
    end
    
    rhs = zeros(Deg+1,1);
    u_hat = zeros(Deg+1,Ne);
    for e =1:Ne
        for i=0:Deg
            rhs(i+1) = sum(JacobiPoly(i,r,0,0).*w.*u_act(:,e));
        end
        u_hat(:,e) = M\rhs;
    end
    
    %evaluate at only quadrature points.
    
    %
    [rviz,wviz] = JacobiGLZW(vizSamples,0,0);
    x_viz = zeros(vizSamples,Ne);
    for e = 1:Ne
        x_viz(:,e) = v_x(e) + (rviz+1)/2*(v_x(e+1)-v_x(e));
    end

    Vld_viz = zeros(vizSamples,Deg+1);
    for d = 0:Deg
       Vld_viz(:,d+1) = JacobiPoly(d,rviz,0,0); 
    end
    
    u_sim_viz = Vld_viz*u_hat;
    % Elementwise Local Derivative.

    figure(1),
    plot(x_viz,u_sim_viz); 
    %hold on;
    %plot(x_viz,sin(x_viz),'r-');
    title('Function Projection');

    u_er_P_Act(Deg) = sum(sum(abs( (u_sim_viz-sin(x_viz))/max(size(x_viz(:))))));
    
    % let us try to evaluate at all the quass qudrature points.
    % 1st loop through all the points for evaluations.
    % Basing the filter at points. Find intersections and elments.
    % Break the elements using the overlap.
    % integrate on each element.
   
	% do siac filtering at these points. 
	[r_sEval,~] = JacobiGLZW(siacSamples,0,0);
	x_sEval = zeros(siacSamples,Ne);
	for e= 1:Ne
		x_sEval(:,e) = v_x(e) + (r_sEval+1)/2*(v_x(e+1)-v_x(e));
	end
	
	filter_Width = 3*(Deg)+1;
	mH_filW = filter_m*filter_H*filter_Width;
    listOfP = x_sEval(:);
	for i = 1:max(size(listOfP))
        p = listOfP(i); 
		f_Lpx = p-mH_filW/2;
		f_Rpx = p+mH_filW/2;
		if (f_Lpx < a)
			f_Lpx = a;
		end
		if (f_Rpx >b)
			f_Rpx = b;
		end
		eL = getElementID_X(f_Lpx,v_x);
		eR = getElementID_X(f_Rpx,v_x);
		% Just realized we will need split the filter also using Kernel knots.
        % Kernel knot position might be tricky than we thought.
        % (Actually not they are fixed)
        % Let us only do by element boudaries.
        eL
        eR
        for e= (eL:eR)
            %each element x_start and x_end
            if (e ==eL)
                %first element
                x_start = f_Lpx; x_end = v_x(e);
            elseif (e==eR)
                x_start = v_x(e); x_end = f_Rpx;
            else
                %all middle elements.
                x_start = v_x(e); x_end = v_x(e+1);
            end
            eLength = x_end-x_start;
            intSum =0;
            for r = r_sEval
              	epi = x_start + (r+1)/2*(x_end-x_start);
                
            end
        end
		% GetListOfElement(p,v_x,mH,Deg) 		
	end

end
