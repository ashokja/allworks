% test calibration vorticity.



clear;
close all;
%rate = 3;
%samplerate = 6;
%r = [2,3,4];

%r = [2,3];
%samples= [10,20];

r = [2];
samples= [20];
Spoly = '2';

for rate = r
    for samplerate = samples
        filename = strcat('P',int2str(rate),'_CubeGrid_',int2str(samplerate));

        xfname = strcat('../../data/NekMeshes/',filename,'_xc0','.txt' );
        yfname = strcat('../../data/NekMeshes/',filename,'_xc1','.txt' );
        zfname = strcat('../../data/NekMeshes/',filename,'_xc2','.txt' );
        
        VorXfname = strcat('../../data/NekMeshes/',filename,'_VorX.txt');
        VorYfname = strcat('../../data/NekMeshes/',filename,'_VorY.txt');
        VorZfname = strcat('../../data/NekMeshes/',filename,'_VorZ.txt');

        ux_DGfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'ux_DG.txt');
        uy_DGfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'uy_DG.txt');
        uz_DGfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'uz_DG.txt');
        
        vx_DGfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'vx_DG.txt');
        vy_DGfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'vy_DG.txt');
        vz_DGfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'vz_DG.txt');
        
        wx_DGfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'wx_DG.txt');
        wy_DGfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'wy_DG.txt');
        wz_DGfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'wz_DG.txt');
        
        ux_SIfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'ux_SI.txt');
        uy_SIfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'uy_SI.txt');
        uz_SIfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'uz_SI.txt');
        
        vx_SIfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'vx_SI.txt');
        vy_SIfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'vy_SI.txt');
        vz_SIfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'vz_SI.txt');
        
        wx_SIfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'wx_SI.txt');
        wy_SIfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'wy_SI.txt');
        wz_SIfname = strcat('../../data/NekMeshes/',filename,'_S_',Spoly,'wz_SI.txt');
        
        
        xpos = textread(xfname,'%f\t');
        ypos = textread(yfname,'%f\t');    
        zpos = textread(zfname,'%f\t');
        
        VorXval = textread( VorXfname, '%f\t');
        VorYval = textread( VorYfname, '%f\t');        
        VorZval = textread( VorZfname, '%f\t');
        
        ux_DGval = textread( ux_DGfname, '%f\t');	vx_DGval = textread( vx_DGfname, '%f\t');	wx_DGval = textread( wx_DGfname, '%f\t');   
        uy_DGval = textread( uy_DGfname, '%f\t');   vy_DGval = textread( vy_DGfname, '%f\t');   wy_DGval = textread( wy_DGfname, '%f\t');   
        uz_DGval = textread( uz_DGfname, '%f\t');   vz_DGval = textread( vz_DGfname, '%f\t');   wz_DGval = textread( wz_DGfname, '%f\t');  

        ux_SIval = textread( ux_SIfname, '%f\t');	vx_SIval = textread( vx_SIfname, '%f\t');	wx_SIval = textread( wx_SIfname, '%f\t');   
        uy_SIval = textread( uy_SIfname, '%f\t');   vy_SIval = textread( vy_SIfname, '%f\t');   wy_SIval = textread( wy_SIfname, '%f\t');   
        uz_SIval = textread( uz_SIfname, '%f\t');   vz_SIval = textread( vz_SIfname, '%f\t');   wz_SIval = textread( wz_SIfname, '%f\t');  

        
        VorX_DG = (wy_DGval-vz_DGval);
        VorY_DG = (uz_DGval-wx_DGval);
        VorZ_DG = (vx_DGval-uy_DGval);

        VorX_SI = (wy_SIval-vz_SIval);
        VorY_SI = (uz_SIval-wx_SIval);
        VorZ_SI = (vx_SIval-uy_SIval);

        ErVorXDG = (abs(VorXval - VorX_DG));
        ErVorYDG = (abs(VorYval - VorY_DG));
        ErVorZDG = (abs(VorZval - VorZ_DG));
        
        ErVorXSI = (abs(VorXval - VorX_SI));
        ErVorYSI = (abs(VorYval - VorY_SI));
        ErVorZSI = (abs(VorZval - VorZ_SI));
        
    end
end


figure(1)
subplot(3,2,1),
histogram(abs(VorXval - VorX_DG)); title('error DGx');
subplot(3,2,2),
histogram(abs(VorXval - VorX_SI)); title('error SIx');

subplot(3,2,3),
histogram(abs(VorYval - VorY_DG)); title('error DGy');
subplot(3,2,4),
histogram(abs(VorYval - VorY_SI)); title('error SIy');

subplot(3,2,5),
histogram(abs(VorZval - VorZ_DG)); title('error DGz');
subplot(3,2,6),
histogram(abs(VorZval - VorZ_SI)); title('error SIz');


figure(2),
scatter3(xpos,ypos,log10(abs(VorXval - VorX_DG)));

figure(3),
scatter3(xpos,ypos,log10(abs(VorXval - VorX_SI)));

% 
% figure(3)
% histogram(abs(VorX_DG))
% 
% figure(4)
% histogram(abs(VorX_SI))
