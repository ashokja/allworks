#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include "MetricTensor.h"


using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg conditions file." << endl;
		cout << "3rd arg resolutoin" << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM2D->LoadMesh(var[0]);
	string fname = vSession->GetSessionName();
	MetricTensor* metricT = new MetricTensor();
	metricT->LoadMetricTensor(HNM2D);
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}

	NekDouble minX,minY,maxX,maxY;
	minX = Vmath::Vmin(tNquadPts,xc0,1); 
	minY = Vmath::Vmin(tNquadPts,xc1,1); 
	maxY = Vmath::Vmax(tNquadPts,xc1,1); 
	maxX = Vmath::Vmax(tNquadPts,xc0,1); 
	cout << "min and max of in X:\t" << minX << "\t" <<maxX << endl;
	cout << "min and max of in Y:\t" << minY << "\t" <<maxY << endl;

	HNM2D->m_Arrays.push_back(HNM2D->m_expansions[0]->GetPhys());
	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();


	//Initialize SmoothieSIAC2D
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM2D, 2, 0.01);

	Array<OneD,NekDouble> direction(3,0.0), knotVec(5,0.0);
	direction[0] = 1.0;
	sm.Cal_NUK_ConstMetricTensor(0.5,0.5,0.0,0.1,direction,knotVec);




/*	
	int gPtsLy = atoi(argv[3])+1;
	int totPtsLy = gPtsLy*gPtsLy;
	NekDouble sx = (maxX-minX)/(gPtsLy-1);	
	NekDouble sy = (maxY-minY)/(gPtsLy-1);	
	Array<OneD,NekDouble> pXLy(totPtsLy), pYLy(totPtsLy);
	Array<OneD,NekDouble> pSMet1Ly(totPtsLy),pSMet2Ly(totPtsLy);
	Array<OneD,NekDouble> pEigV1(totPtsLy),pEigV2(totPtsLy);
	Array<OneD,NekDouble> pEigVec11(totPtsLy),pEigVec12(totPtsLy);
	Array<OneD,NekDouble> pEigVec21(totPtsLy),pEigVec22(totPtsLy);
	NekDouble lambda;
	Array<OneD,NekDouble> eig(3,0.0),glCoords(3,0.0);
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();
	for (int i =0 ; i< gPtsLy; i++)
	{
		for (int j =0 ; j< gPtsLy; j++)
		{
			int index = j+i*gPtsLy;
			pXLy[index] = minX+ i*sx;
			pYLy[index] = minY+ j*sy;
		
			glCoords[0] = pXLy[index]; glCoords[1] = pYLy[index];
			int elid = HNM2D->GetExpansionIndexUsingRTree(glCoords);
			metricT->GetEigenPair(glCoords, elid, 1,lambda,eig);
			pEigV1[index] = lambda;
			pEigVec11[index] = eig[0]; pEigVec12[index] = eig[1];
			metricT->GetEigenPair(glCoords, elid, 2,lambda,eig);
			pEigV2[index] = lambda;
			pEigVec21[index] = eig[0]; pEigVec22[index] = eig[1];
		}
	}
	
	NektarBaseClass k;
	k.writeNekArray(pXLy,fname+"_pX.txt");
	k.writeNekArray(pYLy,fname+"_pY.txt");
	k.writeNekArray(pEigV1,fname+"_pEigV1.txt");
	k.writeNekArray(pEigV2,fname+"_pEigV2.txt");
	k.writeNekArray(pEigVec11,fname+"_pEigVec11.txt");
	k.writeNekArray(pEigVec12,fname+"_pEigVec12.txt");
	k.writeNekArray(pEigVec21,fname+"_pEigVec21.txt");
	k.writeNekArray(pEigVec22,fname+"_pEigVec22.txt");
	// Evaluate on line x =0.5; x = -0.5;
*/
	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}

