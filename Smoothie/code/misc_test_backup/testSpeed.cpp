#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>
#include <limits>   //std::numeric_limits

#include <time.h>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
//#include "HandleNekMesh2D.h"
#include "HandleNekMesh3D.h"

#include <iomanip>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>

#include <boost/geometry/index/rtree.hpp>


//to store queries results.
#include <vector>

//just for output
#include <iostream>
#include <boost/foreach.hpp>

using namespace SIACUtilities;
using namespace std;

namespace Boostg = boost::geometry;
namespace Boostgi = boost::geometry::index;

typedef Boostg::model::point<NekDouble, 3, Boostg::cs::cartesian> RPoint;
typedef Boostg::model::box<RPoint> RBox;
typedef std::tuple<RBox,unsigned,unsigned> RValue;
//typedef Boostgi::rtree<RValue, Boostgi::rstar<2> > RTree;

void GetBoundingOfElement(SpatialDomains::GeometrySharedPtr elGeom, RBox &b);
void LoadMeshGraphIntoRtree( SpatialDomains::MeshGraphSharedPtr mesh_graph, RTree &rtree);
void LoadExpListIntoRtree( MultiRegions::ExpListSharedPtr expList, RTree &rtree);

void IntersectWithFaces(MultiRegions::ExpListSharedPtr expList ,const RTree &rtree,
			const Array<OneD,NekDouble> &dir, const Array<OneD,NekDouble> &pt, 
			const NekDouble t1, const NekDouble t2,
        	vector<NekDouble> &tvalT );



int main( int argc, char* argv[])
{

//create the rtree using default constructor
	//Boostgi::rtree<RValue, Boostgi::rstar<2> > rtree;
	RTree rtree;
	clock_t t;
	t = clock();
//load the mesh file.
	LibUtilities::SessionReaderSharedPtr vSession
                = LibUtilities::SessionReader::CreateInstance(argc,argv);
    vector<string> var = vSession->GetVariables();
      
 
    // get mesh graph.
    SpatialDomains::MeshGraphSharedPtr mesh_graph = SpatialDomains::MeshGraph::Read(vSession);
    int meshDim = mesh_graph->GetMeshDimension();
    int meshSpa = mesh_graph->GetSpaceDimension();

	HandleNekMesh* HNM3D = new HandleNekMesh3D(vSession);
    for (int i=0; i < var.size(); i++)
    {   
    	HNM3D->LoadMesh(var[i]);    
    }   
	cout << "loading data file " << clock()-t << endl;
	t = clock();
//	LoadMeshGraphIntoRtree( mesh_graph, rtree);
	LoadExpListIntoRtree( HNM3D->m_expansions[0], rtree);
	
	cout << "loading data into tree " << clock()-t << endl;
	t = clock();

//find RValues intersecting some area defined by a RBox.
	RBox query_RBox(RPoint(0,0,-0.1),RPoint(0.5,0.5,+0.1));
	std::vector<RValue> result_s;
	//rtree.query(Boostgi::intersects(query_RBox),std::back_inserter(result_s));
	//rtree.query(Boostgi::within(query_RBox),std::back_inserter(result_s));

	//rtree.query(Boostgi::intersects(point(0.1,0.1,0)),std::back_inserter(result_s));
	//rtree.query(Boostgi::contains(point(0.1,0.1,0)),std::back_inserter(result_s));
	rtree.query(Boostgi::nearest(RPoint(0.1,0.10000001,0),1),std::back_inserter(result_s));
	cout << "Nearest neighbour " << clock()-t << endl;
	t = clock();

	std::cout << "spatial query RBox:" << std::endl;
	std::cout << "spatial query result" << std::endl;

	BOOST_FOREACH( RValue const& v, result_s)
	{
		int id = std::get<1>(v);
		int id2 = std::get<2>(v);
		RBox b = std::get<0>(v);
		RPoint p1 = b.min_corner();
		RPoint p2 = b.max_corner();
		std::cout << id << "\t"<<id2<<std::endl;
		cout << "\tp:\t"<<p1.get<0>() << "\t" << p1.get<1>() << "\t"<< p1.get<2>() << endl;
		cout << "\tp:\t"<<p2.get<0>() << "\t" << p2.get<1>() << "\t"<< p2.get<2>() << endl;
	}

	Array<OneD,NekDouble> pAry(3,0.0),dir(3,0.0);
	pAry[0] = 0.5; pAry[1] =0.5;
	dir[0] = 1.0;
	vector<NekDouble> tvalT;
	IntersectWithFaces( HNM3D->m_expansions[0] ,rtree, dir, pAry, -0.5,0.5, tvalT);

	cout << "LineIntersection " << clock()-t << endl;
	t = clock();

	return 0;
}

void LoadExpListIntoRtree( MultiRegions::ExpListSharedPtr expList, RTree &rtree)
{
	int expSize = expList->GetExpSize();
	for (int e=0; e< expSize; ++e)
	{
		SpatialDomains::GeometrySharedPtr geom = expList->GetExp(e)->GetGeom();
		int gid = geom->GetGlobalID();
		RBox b; 
		GetBoundingOfElement( geom, b);
		rtree.insert(std::make_tuple( b,e,gid ) );
	}	
}


void LoadMeshGraphIntoRtree( SpatialDomains::MeshGraphSharedPtr mesh_graph, RTree &rtree)
{
	//SpatialDomains::CompositeMap comMap = mesh_graph->GetComposites();
	const std::vector<SpatialDomains::CompositeMap> comMapVec = mesh_graph->GetDomain(); 
	std::vector<SpatialDomains::CompositeMap>::const_iterator comMap;
	for ( comMap = comMapVec.cbegin(); comMap!=comMapVec.end(); ++comMap)
	{ 
		
	for ( SpatialDomains::CompositeMapConstIter comMapit=comMap->begin(); comMapit!=comMap->end(); ++comMapit)
	{
	//	comMapit->second();
		for (SpatialDomains::GeometryVectorIter geomVecIt= comMapit->second->begin(); 
					geomVecIt!= comMapit->second->end(); geomVecIt++)
		{
			RBox b;
			GetBoundingOfElement( *geomVecIt, b);
			//rtree.insert(std::make_pair( b, (*geomVecIt)->GetGlobalID() ) );
			rtree.insert(std::make_tuple( b, (*geomVecIt)->GetGlobalID(),comMapit->first ) );
		}
	}
	}
	return ;
}


NekDouble getMax( NekDouble a, NekDouble b)
{
	if (a<b)
		return b;
	else 
		return a;
}
NekDouble getMin( NekDouble a, NekDouble b)
{
	if (a<b)
		return a;
	else 
		return b;
}
void GetBoundingOfElement(SpatialDomains::GeometrySharedPtr elGeom, RBox &b)
{
	// Loop through all point in the vertices.
	// create min and max in all coordinates.	
	// load the RBox data structure
	// return
	NekDouble elX,elY,elZ;
	NekDouble minX,minY,minZ;
	minX = std::numeric_limits<NekDouble>::max();
	minY = minX; minZ = minX;
	NekDouble maxX,maxY,maxZ;
	maxX = std::numeric_limits<NekDouble>::lowest();
	maxY = maxX; maxZ=maxZ;
	int numPts = elGeom->GetNumVerts();
	for (int v =0; v<numPts;v++)
	{
		elGeom->GetVertex(v)->GetCoords(elX,elY,elZ);
		minX = getMin(minX,elX);
		minY = getMin(minY,elY);
		minZ = getMin(minZ,elZ);
		maxX = getMax(maxX,elX);
		maxY = getMax(maxY,elY);
		maxZ = getMax(maxZ,elZ);
	}
	
	Boostg::set<Boostg::min_corner, 0> (b, minX-TOLERENCE_MESH_COMP);
	Boostg::set<Boostg::min_corner, 1> (b, minY-TOLERENCE_MESH_COMP);
	Boostg::set<Boostg::min_corner, 2> (b, minZ-TOLERENCE_MESH_COMP);
	Boostg::set<Boostg::max_corner, 0> (b, maxX+TOLERENCE_MESH_COMP);
	Boostg::set<Boostg::max_corner, 1> (b, maxY+TOLERENCE_MESH_COMP);
	Boostg::set<Boostg::max_corner, 2> (b, maxZ+TOLERENCE_MESH_COMP);
	return ;
}

void BoundingBoxOfLineSeg( const Array<OneD,NekDouble> &dir, 
			const Array<OneD,NekDouble> &pt, const NekDouble t1, const NekDouble t2,
        	RBox &b )
{
	NekDouble pl_0 = pt[0] + dir[0]*t1;
	NekDouble pl_1 = pt[1] + dir[1]*t1;
	NekDouble pl_2 = pt[2] + dir[2]*t1;
	
	NekDouble pr_0 = pt[0] + dir[0]*t2;
	NekDouble pr_1 = pt[1] + dir[1]*t2;
	NekDouble pr_2 = pt[2] + dir[2]*t2;
/*
	Boostg::set<Boostg::min_corner, 0> (b, getMin(pl_0,pr_0)-TOLERENCE_MESH_COMP);
	Boostg::set<Boostg::min_corner, 1> (b, getMin(pl_1,pr_1)-TOLERENCE_MESH_COMP);
	Boostg::set<Boostg::min_corner, 2> (b, getMin(pl_2,pr_2)-TOLERENCE_MESH_COMP);
	Boostg::set<Boostg::max_corner, 0> (b, getMax(pl_0,pr_0)+TOLERENCE_MESH_COMP);
	Boostg::set<Boostg::max_corner, 1> (b, getMax(pl_1,pr_1)+TOLERENCE_MESH_COMP);
	Boostg::set<Boostg::max_corner, 2> (b, getMax(pl_2,pr_2)+TOLERENCE_MESH_COMP);
*/
	Boostg::set<Boostg::min_corner, 0> (b, getMin(pl_0,pr_0));
	Boostg::set<Boostg::min_corner, 1> (b, getMin(pl_1,pr_1));
	Boostg::set<Boostg::min_corner, 2> (b, getMin(pl_2,pr_2));
	Boostg::set<Boostg::max_corner, 0> (b, getMax(pl_0,pr_0));
	Boostg::set<Boostg::max_corner, 1> (b, getMax(pl_1,pr_1));
	Boostg::set<Boostg::max_corner, 2> (b, getMax(pl_2,pr_2));
}

void IntersectWithFaces(MultiRegions::ExpListSharedPtr expList ,const RTree &rtree,
			const Array<OneD,NekDouble> &dir, const Array<OneD,NekDouble> &pt, 
			const NekDouble t1, const NekDouble t2,
        	vector<NekDouble> &tvalT )
{
	RBox b;
	BoundingBoxOfLineSeg( dir, pt, t1, t2,b);
		
		RPoint p1 = b.min_corner();
		RPoint p2 = b.max_corner();
		cout << "\tp:\t"<<p1.get<0>() << "\t" << p1.get<1>() << "\t"<< p1.get<2>() << endl;
		cout << "\tp:\t"<<p2.get<0>() << "\t" << p2.get<1>() << "\t"<< p2.get<2>() << endl;

	//rtree.query()
	std::vector<RValue> result_s;
	rtree.query( Boostgi::intersects(b),std::back_inserter(result_s) );

	BOOST_FOREACH( RValue const& v, result_s)
	{
		int eId = std::get<1>(v);
		int gId = std::get<2>(v);
		cout << "elID: \t"<<eId << "\tGlobalID:\t"<<gId<< endl;
		SpatialDomains::GeometrySharedPtr geomSPtr = expList->GetExp(eId)->GetGeom();
//		SpatialDomains::GeometrySharedPtr geomSPtr= mesh_graph->GetCompositeItem( gid,comId);
//		cout << "compositeId: \t"<<comId << "\tGlobalID:\t"<<gid<< endl;
		cout << geomSPtr->GetNumFaces()<< endl;
		int nFaces = geomSPtr->GetNumFaces();
		for (int f =0; f< nFaces; f++)
		{
			cout << "\t\t" << geomSPtr->GetFid(f) << endl; 
		}
	}
}

