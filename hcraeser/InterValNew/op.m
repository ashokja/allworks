% Newton interval 
function res = op(i,a,b)
    switch nargin
        case 0,
            a = [-1,1]; b =[-1,1]; i =1;
    end
    
    switch i
        case {'+', 1 } ,
            % add.
            res(1) = a(1)+b(1);
            res(2) = a(2)+b(2);
        case {'-',2},
            % sub
            res(1) = a(1) -b(2);
            res(2) = a(2)-b(1);
        case {'*',3},
            res(1) = min([a(1)*b(1), a(2)*b(1), a(1)*b(2),a(2)*b(2)]);
            res(2) = max([a(1)*b(1), a(2)*b(1), a(1)*b(2),a(2)*b(2)]);
        case {'/',4},
            if (b(1)*b(2) > 0)
                res(1) = min([a(1)/b(1), a(2)/b(1), a(1)/b(2),a(2)/b(2)]);
                res(2) = max([a(1)/b(1), a(2)/b(1), a(1)/b(2),a(2)/b(2)]);
            else
                res(1) = -inf;
                res(2) = inf;
            end
    end
    
