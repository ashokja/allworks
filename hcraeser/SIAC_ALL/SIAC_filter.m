function [v_eval] = SIAC_filter(x_eval,h,k,lambda)

    switch nargin
        case 0
            x_eval = -3.5:.1:3.5;
            h = 1.0;
            k = 1;
            lambda = 0;
        case 3
            lambda = 0;
    end
    
    coeff = SIAC_coeff(lambda, k);
    
    count =1;
    v_eval = zeros(size(x_eval));
    for it = -k:1:k
        v_eval= v_eval+ coeff(count)*BSpl_central(x_eval/h+it,k);
        count= count+1;
    end
    v_eval= v_eval/h;
    
    
end