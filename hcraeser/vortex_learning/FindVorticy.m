function retP = FindVorticy(coords,vel)

switch nargin
    case 0
        % re-writing for coords and vel.
        coords(1,:) = [0;0;0];
        coords(2,:) = [1;0;0];
        coords(3,:) = [0;1;0];
        coords(4,:) = [0;0;1];

        vel(1,:) = [ 1/2;-1/2; 1/2];
        vel(2,:) = [ 1/2; 1/2; 1/2];
        vel(3,:) = [-1/2;-1/2; 1/2];
        vel(4,:) = [ 1/2;-1/2;-1/2];
end

[~,V_f] = gradientTensorTet(coords,vel);

%V_f= V_f'; % wrong.
lambda = eig(V_f);

tet_complex =false;
if ~isreal(lambda)
    'tet contains complex number';
    tet_complex = true;
end

if (tet_complex)
    % find the real rool index. 
   real_index  = find(imag(lambda)==0);
end

if (tet_complex)
    [A,B] = eig(V_f);
    n = A(:,real_index)'/norm(A(:,real_index));
    w(1,:) = vel(1,:) - dot(vel(1,:),n)*n;
    w(2,:) = vel(2,:) - dot(vel(2,:),n)*n;
    w(3,:) = vel(3,:) - dot(vel(3,:),n)*n;
    w(4,:) = vel(4,:) - dot(vel(4,:),n)*n;
    [c_w,V_w] = gradientTensorTet(coords,w);
end

% Given plane equations and tet Coordinates.
% Find the line with intersects both planes.
% Find if any line segment passes through the tet 
%   and interset the faces of the tet planes.

if (tet_complex)
    retP = PlaneTetInter(V_w,c_w,coords);
else
    retP =[];
end

