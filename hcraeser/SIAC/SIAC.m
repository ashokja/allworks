function v = SIAC( x, h, order )

if( order == 3)
    c3 = -41.0/7560.0;
    c2 = 311.0/5040.0;
    c1 = -919.0/2520.0;
    c0 = 12223.0/7560.0;

    v = (c3.*B4(x./h-3) + c2.*B4(x./h-2) + c1.*B4(x./h-1) + c0.*B4(x./h) + ...
        c3.*B4(x./h+3) + c2.*B4(x./h+2) + c1.*B4(x./h+1))./h;
elseif (order == 2)
    c2 = 37.0/1920.0;
    c1 = -97.0/480.0;
    c0 = 437.0/320.0;

    v = (c2.*B3(x./h-2) + c1.*B3(x./h-1) + c0.*B3(x./h) + ...
        c2.*B3(x./h+2) + c1.*B3(x./h+1))./h; 
elseif (order == 1)
    c1 = -1.0/12.0;
    c0 = 7.0/6.0;

    v = (c1.*B2(x./h-1) + c0.*B2(x./h) + ...
        c1.*B2(x./h+1))./h; 
elseif (order ==0)
    c0 = 1.0;
    
    v = c0.*B1(x./h);
else
    disp('the order is not supported ! ')
end
end

