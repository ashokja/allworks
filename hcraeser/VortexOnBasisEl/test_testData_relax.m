clear;
% complex example looking for relaxation methods.

%initialize variables
n1 = 4; n2 = 4; n3 =4;
seedNum = 1;

addpath ../createTestData;
[xy,distance,t_a,mapxy,mapxyV] = test_run(5);
dataIn = mapxy;
dataOut = mapxyV;

% acutal code
coeff = buildCoeff(n1,n2,n3,dataIn,dataOut);
[uCoeffRed,uCoeffRot] = divideVel(n1,n2,n3,coeff,dataIn,dataOut);
% 
% figure(2);
% vizualizeCoeff(n1,n2,n3,uCoeffRed);
% 
% figure(3);
% vizualizeCoeff(n1,n2,n3,uCoeffRot);
% xlabel('x');ylabel('y');zlabel('z');
%%
y = (-1:.3:1);
x = zeros(size(y));
z = x;
[~,sx] = size(y);
figure(4);
plot3(x,y,z,'g*'); hold on;
pOut = zeros(sx,3);
for i = 1:sx
    pointIn = [x(i),y(i),z(i)];
    pOut(i,:) = closestZeroP(n1,n2,n3,uCoeffRot,uCoeffRed,pointIn,0.125);
end
%%

plotBoundingBox(); hold on;
plot3(pOut(:,1),pOut(:,2),pOut(:,3),'r*');
hold off;
