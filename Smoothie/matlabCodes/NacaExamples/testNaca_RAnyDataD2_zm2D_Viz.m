% Read data from testNaca_RAnyResD2
% s is used in variables but it ss data.

clear;
close all;


UsebkupData = 'cur_apr16/'; % has only data Ux_ss so remember to comment below also.
%UsebkupDataD = 'cur_feb20/'; % has only data Ux_ss so remember to comment below also.
UsebkupDataD = 'cur_apr16/' % has only data Ux_ss so remember to comment below also.
UsebkupDataC = 'cur_apr16/';

%UsebkupData = 'cur_feb10/'

%UsebkupData = ''
%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05';
filename = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_Any2LSIACzm4VARND2' );
%filenameD = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_use1dir3DSIAC' );
filenameD = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_Any2zm4LSIACVARN' );
filenameC = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ACRFDzm4Var' );

% filename = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_Any2LSIACzm2VARND2' );
% %filenameD = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_use1dir3DSIAC' );
% filenameD = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_Any2zm2LSIACVARN' );
% filenameC = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ACRFDzmVar' );


filepath = strcat('data/',UsebkupData,filename);
filepathD = strcat('data/',UsebkupDataD,filenameD);
filepathC = strcat('data/',UsebkupDataC,filenameC);

xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );

usfname = strcat(filepathC,'_pU_ss.txt' );
vsfname = strcat(filepathC,'_pV_ss.txt' );
wsfname = strcat(filepathC,'_pW_ss.txt' );

uxsfname = strcat(filepathD, '_pUx_ss.txt');
uysfname = strcat(filepathD, '_pUy_ss.txt');
uzsfname = strcat(filepathD, '_pUz_ss.txt');

vxsfname = strcat(filepathD, '_pVx_ss.txt');
vysfname = strcat(filepathD, '_pVy_ss.txt');
vzsfname = strcat(filepathD, '_pVz_ss.txt');

wxsfname = strcat(filepathD, '_pWx_ss.txt');
wysfname = strcat(filepathD, '_pWy_ss.txt');
wzsfname = strcat(filepathD, '_pWz_ss.txt');


uxxsfname = strcat(filepath, '_pUxx_ss.txt');
uxysfname = strcat(filepath, '_pUxy_ss.txt');
uxzsfname = strcat(filepath, '_pUxz_ss.txt');

uyxsfname = strcat(filepath, '_pUyx_ss.txt');
uyysfname = strcat(filepath, '_pUyy_ss.txt');
uyzsfname = strcat(filepath, '_pUyz_ss.txt');

uzxsfname = strcat(filepath, '_pUzx_ss.txt');
uzysfname = strcat(filepath, '_pUzy_ss.txt');
uzzsfname = strcat(filepath, '_pUzz_ss.txt');

vxxsfname = strcat(filepath, '_pVxx_ss.txt');
vxysfname = strcat(filepath, '_pVxy_ss.txt');
vxzsfname = strcat(filepath, '_pVxz_ss.txt');

vyxsfname = strcat(filepath, '_pVyx_ss.txt');
vyysfname = strcat(filepath, '_pVyy_ss.txt');
vyzsfname = strcat(filepath, '_pVyz_ss.txt');

vzxsfname = strcat(filepath, '_pVzx_ss.txt');
vzysfname = strcat(filepath, '_pVzy_ss.txt');
vzzsfname = strcat(filepath, '_pVzz_ss.txt');

wxxsfname = strcat(filepath, '_pWxx_ss.txt');
wxysfname = strcat(filepath, '_pWxy_ss.txt');
wxzsfname = strcat(filepath, '_pWxz_ss.txt');

wyxsfname = strcat(filepath, '_pWyx_ss.txt');
wyysfname = strcat(filepath, '_pWyy_ss.txt');
wyzsfname = strcat(filepath, '_pWyz_ss.txt');

wzxsfname = strcat(filepath, '_pWzx_ss.txt');
wzysfname = strcat(filepath, '_pWzy_ss.txt');
wzzsfname = strcat(filepath, '_pWzz_ss.txt');



xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');

uspos = textread(usfname, '%f\t');
vspos = textread(vsfname, '%f\t');
wspos = textread(wsfname, '%f\t');

uxspos = textread( uxsfname,'%f\t');
uyspos = textread( uysfname,'%f\t');
uzspos = textread( uzsfname,'%f\t');
vxspos = textread( vxsfname,'%f\t');
vyspos = textread( vysfname,'%f\t');
vzspos = textread( vzsfname,'%f\t');
wxspos = textread( wxsfname,'%f\t');
wyspos = textread( wysfname,'%f\t');
wzspos = textread( wzsfname,'%f\t');


uxxspos = textread( uxxsfname,'%f\t');
uxyspos = textread( uxysfname,'%f\t');
uxzspos = textread( uxzsfname,'%f\t');
uyxspos = textread( uyxsfname,'%f\t');
uyyspos = textread( uyysfname,'%f\t');
uyzspos = textread( uyzsfname,'%f\t');
uzxspos = textread( uzxsfname,'%f\t');
uzyspos = textread( uzysfname,'%f\t');
uzzspos = textread( uzzsfname,'%f\t');

vxxspos = textread( vxxsfname,'%f\t');
vxyspos = textread( vxysfname,'%f\t');
vxzspos = textread( vxzsfname,'%f\t');
vyxspos = textread( vyxsfname,'%f\t');
vyyspos = textread( vyysfname,'%f\t');
vyzspos = textread( vyzsfname,'%f\t');
vzxspos = textread( vzxsfname,'%f\t');
vzyspos = textread( vzysfname,'%f\t');
vzzspos = textread( vzzsfname,'%f\t');

wxxspos = textread( wxxsfname,'%f\t');
wxyspos = textread( wxysfname,'%f\t');
wxzspos = textread( wxzsfname,'%f\t');
wyxspos = textread( wyxsfname,'%f\t');
wyyspos = textread( wyysfname,'%f\t');
wyzspos = textread( wyzsfname,'%f\t');
wzxspos = textread( wzxsfname,'%f\t');
wzyspos = textread( wzysfname,'%f\t');
wzzspos = textread( wzzsfname,'%f\t');


epos = floor(epos +0.0001);
s = floor( (max(size(xpos)))^(1/2) +0.1);


xVpos = reshape(xpos,s,s);
yVpos = reshape(ypos,s,s);
zVpos = reshape(zpos,s,s);

usVpos = reshape(uspos,s,s);  vsVpos = reshape(vspos,s,s);  wsVpos = reshape(wspos,s,s);

uxxsVpos = reshape(uxxspos,s,s);
uxysVpos = reshape(uxyspos,s,s);
uxzsVpos = reshape(uxzspos,s,s);
uyxsVpos = reshape(uyxspos,s,s);
uyysVpos = reshape(uyyspos,s,s);
uyzsVpos = reshape(uyzspos,s,s);
uzxsVpos = reshape(uzxspos,s,s);
uzysVpos = reshape(uzyspos,s,s);
uzzsVpos = reshape(uzzspos,s,s);

vxxsVpos = reshape(vxxspos,s,s);
vxysVpos = reshape(vxyspos,s,s);
vxzsVpos = reshape(vxzspos,s,s);
vyxsVpos = reshape(vyxspos,s,s);
vyysVpos = reshape(vyyspos,s,s);
vyzsVpos = reshape(vyzspos,s,s);
vzxsVpos = reshape(vzxspos,s,s);
vzysVpos = reshape(vzyspos,s,s);
vzzsVpos = reshape(vzzspos,s,s);

wxxsVpos = reshape(wxxspos,s,s);
wxysVpos = reshape(wxyspos,s,s);
wxzsVpos = reshape(wxzspos,s,s);
wyxsVpos = reshape(wyxspos,s,s);
wyysVpos = reshape(wyyspos,s,s);
wyzsVpos = reshape(wyzspos,s,s);
wzxsVpos = reshape(wzxspos,s,s);
wzysVpos = reshape(wzyspos,s,s);
wzzsVpos = reshape(wzzspos,s,s);


% calculate acceleration.
auspos = uxspos.*uspos + uyspos.*vspos + uzspos.*wspos;
avspos = vxspos.*uspos + vyspos.*vspos + vzspos.*wspos;
awspos = wxspos.*uspos + wyspos.*vspos + wzspos.*wspos;
 %
% veclocity cross acceleration. 
cuspos = vspos.*awspos - wspos.*avspos;
cvspos = wspos.*auspos - uspos.*awspos;
cwspos = uspos.*avspos - vspos.*auspos;

curvature_pos = (cuspos.^(2) + cvspos.^(2) + cwspos.^(2)).^(1/2);
curvature_Vpos = reshape(curvature_pos,s,s);
%%
figure(10)
surf(yVpos,zVpos,curvature_Vpos);
shading interp;
title('curvature');
print('images/Apr12/3Cur_sur','-dpng', '-r500');
%%

auxspos = (uxxspos + uyxspos+uzxspos).*uspos + (uxspos+uyspos+uzspos).*uxspos;
auyspos = (uxyspos + uyyspos+uzyspos).*uspos + (uxspos+uyspos+uzspos).*uyspos;
auzspos = (uxzspos + uyzspos+uzzspos).*uspos + (uxspos+uyspos+uzspos).*uzspos;

avxspos = (vxxspos + vyxspos+vzxspos).*vspos + (vxspos+vyspos+vzspos).*vxspos;
avyspos = (vxyspos + vyyspos+vzyspos).*vspos + (vxspos+vyspos+vzspos).*vyspos;
avzspos = (vxzspos + vyzspos+vzzspos).*vspos + (vxspos+vyspos+vzspos).*vzspos;

awxspos = (wxxspos + wyxspos+wzxspos).*wspos + (wxspos+wyspos+wzspos).*wxspos;
awyspos = (wxyspos + wyyspos+wzyspos).*wspos + (wxspos+wyspos+wzspos).*wyspos;
awzspos = (wxzspos + wyzspos+wzzspos).*wspos + (wxspos+wyspos+wzspos).*wzspos;

% calculate b.
buspos = auxspos.*uspos + auyspos.*vspos + auzspos.*wspos;
bvspos = avxspos.*uspos + avyspos.*vspos + avzspos.*wspos;
bwspos = awxspos.*uspos + awyspos.*vspos + awzspos.*wspos;

% torsion = (velocity cross a).b ;
torsion_pos = cuspos.*buspos + cvspos.*bvspos + cwspos.*bwspos;
torsion_Vpos = reshape(torsion_pos,s,s);


% vdotb
velnorm = (uspos.*uspos + vspos.*vspos + wspos.*wspos).^(1/2);
usposN = uspos./velnorm;
vsposN = vspos./velnorm;
wsposN = wspos./velnorm;
bnorm = (buspos.*buspos + bvspos.*bvspos + bwspos.*bwspos).^(1/2);
busposN = buspos./bnorm;
bvsposN = bvspos./bnorm;
bwsposN = bwspos./bnorm;
vDotb_pos = usposN.*busposN + vsposN.*bvsposN + wsposN.*bwsposN;
vDotb_Vpos = reshape(vDotb_pos,s,s);




%%
figure(11)
surf(yVpos,zVpos,torsion_Vpos); hold on;
shading interp;
view(156,36);
contour3(yVpos,zVpos,torsion_Vpos,[0 100000000],'LineColor','c'); hold on;

%xlim([ 0.15 0.35 ]), ylim([ 0.73 0.92 ]),colorbar();
title('torsion');
axis();
%print('images/Feb22/torsion_3dirLSIAC_surf','-dpng', '-r500')
print('images/Apr12/3torsion_sur','-dpng', '-r500');



%%
figure(12)
contour3(yVpos,zVpos,torsion_Vpos,[0 100000000],'LineColor','c'); hold on;
%surf(yVpos,zVpos,torsion_Vpos), shading interp;
contour3(yVpos,zVpos,torsion_Vpos,10,'LineColor','k'); hold on;
surf(yVpos,zVpos,torsion_Vpos), shading interp;
axis off;
grid off;
view(0,90);
%print('images/Apr12/3torsion_sur_xyView','-dpng', '-r500');

%colorbar();
%title('torison with [-1 0 1] contours')
%print('images/Feb22/torsion_3dirLSIAC_contour','-dpng', '-r500')



%%
figure(13)
surf(yVpos,zVpos,vDotb_Vpos); hold on;
shading interp;view([-22.5,12]);
print('images/Apr12/3vDotb_sur_xyView','-dpng', '-r500');

figure(14)
contour(yVpos,zVpos,vDotb_Vpos); hold on;
shading interp;
print('images/Apr12/3vDotb_cont','-dpng', '-r500');
%%

figure(15)
surf(yVpos,zVpos,torsion_Vpos,abs(vDotb_Vpos)); hold on;
contour(yVpos,zVpos,torsion_Vpos,[-0.1 0 0.1],'lineColor','c'); hold on;
shading interp;
title('torison');
view(0,90);
colorbar();

%%
figure(26),
surf(yVpos,zVpos,torsion_Vpos,abs(vDotb_Vpos)); hold on; shading interp
%contour(yVpos,zVpos,torsion_Vpos,[-0.1 0 0.1],'lineColor','c'); hold on;
contour(yVpos,zVpos,abs(vDotb_Vpos),linspace(0.75,1.0,5),'lineColor','g'); hold on;
%axis([ 0.2725 0.2775 0.83 0.835] );
%grid on; grid minor;
axis off; grid off;
view(0,90);
%ax = gca;
%print('images/Feb22/torsion_3dirLSIAC_sol','-dpng', '-r500')
%print('images/Apr12/3Torsion_zero_vDb','-dpng', '-r500');
print('images/Bob/3Torsion_zero_vDb_2','-dpng', '-r500');
%print('images/Bob/3Torsion_zero_vDb_3','-dpng', '-r500');

%%
figure(27),
contour(yVpos,zVpos,curvature_Vpos,linspace(0,4,10)); hold on;
contour(yVpos,zVpos,curvature_Vpos,linspace(0,1,10)); hold on;
grid off; axis off;
%grid on; grid minor;
%print('images/Feb22/curvature_3dirLSIAC_sol','-dpng', '-r500')
%print('images/Apr12/3cur_cont','-dpng', '-r500');
print('images/Bob/3cur_cont','-dpng', '-r500');
%%
figure(29),
contour3(yVpos,zVpos,curvature_Vpos,linspace(0,0.5,6),'LineColor','r'); hold on;
contour3(yVpos,zVpos,curvature_Vpos,linspace(0,4,10),'LineColor','k'); hold on;
surf(yVpos,zVpos,curvature_Vpos), shading interp; hold on;
%contour3(yVpos,zVpos,curvature_Vpos,10); hold on;
view(0,90);
grid off;
axis off;
print('images/Bob/3cur_cont_1','-dpng', '-r500');


%%
startx = min(ypos(:)):0.001:max(ypos(:));
starty = min(zpos(:)):0.001:max(zpos(:));
[SX,SY] = meshgrid(startx,starty);
figure(28),
quiver(yVpos,zVpos,vsVpos,wsVpos); hold on;
streamline(yVpos,zVpos,vsVpos,wsVpos,SX,SY); hold on;
%%
figure(1)
subplot(3,3,1),contour(uxxsVpos);axis off; title('uxx');
subplot(3,3,2),contour(uxysVpos);axis off; title('uxy');
subplot(3,3,3),contour(uxzsVpos);axis off; title('uxz');
subplot(3,3,4),contour(uyxsVpos);axis off; title('uyx');
subplot(3,3,5),contour(uyysVpos);axis off; title('uyy');
subplot(3,3,6),contour(uyzsVpos);axis off; title('uyz');
subplot(3,3,7),contour(uzxsVpos);axis off; title('uzx');
subplot(3,3,8),contour(uzysVpos);axis off; title('uzy');
subplot(3,3,9),contour(uzzsVpos);axis off; title('uzz');
print('images/Apr12/3u_2ndDer','-dpng', '-r500');


figure(2)
subplot(3,3,1),contour(vxxsVpos);axis off; title('vxx');
subplot(3,3,2),contour(vxysVpos);axis off; title('vxy');
subplot(3,3,3),contour(vxzsVpos);axis off; title('vxz');
subplot(3,3,4),contour(vyxsVpos);axis off; title('vyx');
subplot(3,3,5),contour(vyysVpos);axis off; title('vyy');
subplot(3,3,6),contour(vyzsVpos);axis off; title('vyz');
subplot(3,3,7),contour(vzxsVpos);axis off; title('vzx');
subplot(3,3,8),contour(vzysVpos);axis off; title('vzy');
subplot(3,3,9),contour(vzzsVpos);axis off; title('vzz');
print('images/Apr12/3v_2ndDer','-dpng', '-r500');


figure(3)
subplot(3,3,1),contour(wxxsVpos);axis off; title('wxx');
subplot(3,3,2),contour(wxysVpos);axis off; title('wxy');
subplot(3,3,3),contour(wxzsVpos);axis off; title('wxz');
subplot(3,3,4),contour(wyxsVpos);axis off; title('wyx');
subplot(3,3,5),contour(wyysVpos);axis off; title('wyy');
subplot(3,3,6),contour(wyzsVpos);axis off; title('wyz');
subplot(3,3,7),contour(wzxsVpos);axis off; title('wzx');
subplot(3,3,8),contour(wzysVpos);axis off; title('wzy');
subplot(3,3,9),contour(wzzsVpos);axis off; title('wzz');
print('images/Apr12/3w_2ndDer','-dpng', '-r500');

% 

uxsVpos = reshape(uxspos,s,s); 
uysVpos = reshape(uyspos,s,s); 
uzsVpos = reshape(uzspos,s,s);
vxsVpos = reshape(vxspos,s,s);
vysVpos = reshape(vyspos,s,s);
vzsVpos = reshape(vzspos,s,s);
wxsVpos = reshape(wxspos,s,s);
wysVpos = reshape(wyspos,s,s);
wzsVpos = reshape(wzspos,s,s);

figure(4)
subplot(3,3,1),contour(uxsVpos); axis off; title('ux');
subplot(3,3,2),contour(uysVpos); axis off; title('uy');
subplot(3,3,3),contour(uzsVpos); axis off; title('uz');
subplot(3,3,4),contour(vxsVpos); axis off; title('vx');
subplot(3,3,5),contour(vysVpos); axis off; title('vy');
subplot(3,3,6),contour(vzsVpos); axis off; title('vz');
subplot(3,3,7),contour(wxsVpos); axis off; title('wx');
subplot(3,3,8),contour(wysVpos); axis off; title('wy');
subplot(3,3,9),contour(wzsVpos); axis off; title('wz');
print('images/Apr12/3uvw_1stDer','-dpng', '-r500');

figure(5)
subplot(1,3,1),contour(usVpos); axis('equal'); axis off; title('u');
subplot(1,3,2),contour(vsVpos);  axis('equal'); axis off; title('v');
subplot(1,3,3),contour(wsVpos);  axis('equal'); axis off; title('w');
print('images/Apr12/3uvw','-dpng', '-r500');


eVpos = reshape(epos,s,s);
%%
figure(6)
%contour(yVpos,zVpos,eVpos,10);hold on;
%contour(vzxsVpos); hold on;
contour(yVpos,zVpos,curvature_Vpos,linspace(0,1,10),'linecolor','r'); hold on;
contour(yVpos,zVpos,abs(vDotb_Vpos),linspace(0.8,1.0,7),'linecolor','g');
axis off; grid off;
%print('images/Apr12/3Cur_Tor_Overlap','-dpng', '-r500');
axis([.272 .278 .828 .833])
%axis on;
%print('images/Bob/3Cur_Tor_Overlap','-dpng', '-r500');
%axis([.2735 .275 .83 .8315])
%print('images/Bob/3Cur_Tor_Overlap_zm','-dpng', '-r500');
%print('images/Apr12/3Cur_Tor_Overlap_zm','-dpng', '-r500');

%%
%figure(7)


