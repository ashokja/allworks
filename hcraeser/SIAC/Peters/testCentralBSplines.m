% test script to build and standard and coefficents.
close all;
clear all;
degree = 5;
%for j = 1:degree
j = degree
    tknots = -(degree+1)/2 - (degree+j)/2:1: (degree+1)/2+(degree+j)/2;
    shift =0;
    %shift = -1*((degree+1)/2+(degree+j)/2)+1;
    %+ ((degree+1)/2+degree);



    tknots = tknots +shift;
    %get coeffecients.
    [M0,c] = bulidM_0Sym_NonStandarad(degree,tknots);
    disp(M0);
    disp('condition Number');
    disp(cond(M0));
    disp(c);
    %x_list for solution to be evaluated.
    x_list = (min(min(tknots)):.001:max(max(tknots)));
    v_list = zeros(size(x_list));

    %points to calculate for central Bsplines.
    for loop = 1:max(size(c))
    %    delta = loop-degree-1;
        v_list = v_list+ c(loop)*CalBernUsingDividedDiff(degree,x_list,tknots(loop:loop+degree+1));
    end


    plot(x_list,v_list); hold on;
    disp(sum(sum(v_list(:)))/max(size(v_list(:)))*abs(max(max(tknots))-min(min(tknots))));
%end