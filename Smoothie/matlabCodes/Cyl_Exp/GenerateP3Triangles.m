function [res] = GenerateP3Triangles(elType, startIndex,Special)
    switch nargin
        case 0,
            elType = 3;
            startIndex = 20;
            Special = 89;
    end
    startIndex = startIndex-1;
    Special = Special - startIndex;
    if ( 3 == elType)
        res = [1,2,5;
              2,6,5;
              2,3,6;
              3,7,6;
              3,4,7;
              4,8,7;
               5,6,9;
              6,10,9;
              6,7,10;
              7,11,10;
              7,8,11;
              8,12,11;
              9,10,Special;
              10,11,Special;
              11,12,Special];
    else 
        %quad
%         res = [1,2,5;
%               2,6,5;
%               2,3,6;
%               3,7,6;
%               3,4,7;
%               4,8,7;
%               5,6,9;
%               6,7,9;
%               7,10,9;
%               7,11,10;
%               7,8,11;
%               8,12,11;
%               9,10,13;
%               10,14,13;
%               10,11,14;
%               11,15,14;
%               11,12,15;
%               12,16,15;];
        res = [1,2,5;
              2,6,5;
              2,3,6;
              3,7,6;
              3,4,7;
              4,8,7;
              5,6,9;
              6,10,9;
              6,7,10;
              7,11,10;
              7,8,11;
              8,12,11;
              9,10,13;
              10,14,13;
              10,11,14;
              11,15,14;
              11,12,15;
              12,16,15;];
    end
    
    
    res = res + startIndex;


end