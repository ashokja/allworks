
function [vel,U,V,W,Vor_x,Vor_y,Vor_z] = CalSpiralFlow_M2(X,Y,Z,w,R,G)
    switch nargin
        case 0,
            x = linspace(.1,2,10);
            y = linspace(.1,2,10);
            z = linspace(.1,2,10);

            [X,Y,Z] = meshgrid(x,y,z);
            X = X(:); Y= Y(:); Z=Z(:);
            w = 10;
            R = 1;
            G = 1;
        case 3,
            w = 1;
            R = 1.5;
            G = 1;
            
    end

% r := (x^2 + y^2 + 0.01)^(1/2)
    r2 = (X.^2 + Y.^2);
    r = (r2).^(1/2);
% w := 10
% R := 1.5
% G := 1
% Vx[x_, y_, z_] := -y*G/r - w*z*x/r^2
% Vy[x_, y_, z_] := x*G/r - w*z*y/r^2
% Vz[x_, y_, z_] := w*(1 - R/r)

    U = -Y.*G./r -w.*Z.*X./r2;
    V =  X.*G./r -w.*Z.*Y./r2;
    W =w.*R.*(ones(size(X)) - R./r);
    vel = [U,V,W];
    
    Vor_x = w*Y./(r2).*(1+R./r);
    Vor_y = -1.0*w*X./(r2).*(1+R./r);
    Vor_z = G./r;
    %disp(vel);
    %disp(size(vel));
    %disp(X(1)); disp(Y(1)); disp(Z(1));
    
end
