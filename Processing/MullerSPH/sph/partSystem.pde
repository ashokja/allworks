
class partSystem {
  ArrayList<particle> pList;

  partSystem()
  {
    pList = new ArrayList<particle> ();
  }

  partSystem(ArrayList<particle> plist)
  {
    pList = plist;
  }

  void addParticle(particle p)
  {
    pList.add(p);
  }

  void addParticle(PVector p)
  {
    println("came here");
    particle ptmp = new particle(p);
    pList.add(ptmp);
  }

  void draw()
  {
    for (int i = 0; i < pList.size (); i++)
    {
      pList.get(i).draw();
    }
  }

  void calNeigh(int k)
  {
    // println("came here 1");
    for (int i =0; i < pList.size (); i++)
    { // for each particle.
      for (int j =0; j< k; j++)
      {
        pList.get(i).neigh.add(-1);
        pList.get(i).neighDist.add(1000000000.0);
      }
    }
    // println("came here 2");    
    for (int i =0; i < pList.size (); i++)
    { // for each particle.
      //int i =0;
      for (int j=0; j< pList.size (); j++)
      { // check if neighbour.
        if (i !=j) // dont calculate same same particle.
        {
          float dist = pList.get(i).getDist( pList.get(j) );
          //println(dist);
          pList.get(i).addToNeigh(j, dist, k);
        }
      }
    }
    //  println("came here 3 Ashok jallepalli");
    /*
    for (  i =0; i < pList.size(); i++)
     { // for each particle.
     for(int j =k-1 ; j > -1; j--)
     {
     if (-1 == pList.get(i).neigh.get(j) )
     {
     pList.get(i).neigh.remove(pList.get(i).neigh.size() -1);
     pList.get(i).neighDist.remove(pList.get(i).neighDist.size() -1);
     }
     }
     }
     */
    println("came here 4");
  }

  void init()
  {
    for (int i =0; i< pList.size (); i++)
    {
      pList.get(i).init();
    }
  }

  void printc()
  {
    for (int i =0; i< pList.size (); i++)
    {
      pList.get(i).printc();
      println();
    }
  }

  void printGrad()
  {
    for (int p=0; p< pList.size (); p++)
    {
      float[][] tem = pList.get(p).gradientAtPoint();
      for ( int i =0; i < 3; i++)
      {
        for (int j =0; j< 3; j++)
        {
          print( tem[i][j]+  " ");
        }
        println();
      }
    }
    println();
  }

  void step(float dt)
  {
    // set forces to zero.
    for (int i =0; i < pList.size (); i++)
    {
      pList.get(i).m_for.x =0.0;
      pList.get(i).m_for.y =0.0;
      pList.get(i).m_for.z =0.0;
    }

    for (int par =0; par < pList.size (); par++)
    {
      particle p = pList.get(par);
      //set forces to zero.
      float[][] I = Mat.identity(3);
      float[][] grad = p.gradientAtPoint();
      float[][] J = Mat.sum(grad, I);
      float[][] Jt = matTranspose(J, 3, 3); //Mat.sum(grad, I).transpose();
      float[][] strain = Mat.multiply(J, Jt); // Multiply this properly.
      strain = Mat.sum(strain, Mat.multiply(I, -1)); // right stress.

      // Compute Stress
      float trace_strain = strain[0][0] + strain[1][1] + strain[2][2];
      float diagValue = LAMBDA * trace_strain ;
      float[][] S = Mat.sum( Mat.multiply( Mat.identity(3), diagValue ), Mat.multiply(strain, 2.0*MU));

      // Covert to 1st Piola-Kirchhoff stress.
      float[][] P = Mat.multiply(J, S);

      // Compute Forces.
      p.computeForces(J, S);
    }

    for (int par =0; par< pList.size (); par++)
    {
      particle p = pList.get(par);
      //PVector accel = new PVector(0.0,-9.8,0.0);
      PVector accel = new PVector(0.0, 0.0, 0.0);
      accel.add( PVector.mult(p.m_for, 1/p.m_mas));
      PVector newVel = PVector.add(p.m_vel, PVector.mult(accel, dt*DAMPING));
      p.mp.add( PVector.mult(newVel, dt));
      p.m_vel.x = newVel.x;
      p.m_vel.y = newVel.y;
      p.m_vel.z = newVel.z;
      if (p.mp.y < -1.0)
      {
        p.mp.y = -1.0;
      }
    }
  }

  float[][] matTranspose(float[][] mat, int di, int dj)
  {
    float[][] matT = new float[3][3];
    for (int i =0; i < di; i++)
    {
      for (int j =0; j< dj; j++)
      {
        matT[j][i] = mat[i][j];
      }
    }
    return matT;
  }
}

