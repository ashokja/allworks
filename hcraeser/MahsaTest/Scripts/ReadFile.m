function [C] = ReadFile(filename)
%filename = '../PostP2_2/hex_test2_unionjack_periodic_quadrature_points_projection_inversematlablatticescaling_MR15_P1_KF1_UF0.txt';

fid = fopen(filename);

tline = fgetl(fid);
C = zeros(1,4);
while ischar(tline)
    %disp(tline)
    tline = fgetl(fid);
    if ( strcmp('L2-norm: ',tline))
        tline = fgetl(fid);
        C(1) = cell2mat(textscan(tline, 'before, error on all elements: %f'));
        %disp(tline)
        tline = fgetl(fid);
        C(2) =cell2mat(textscan(tline, 'after, error on all elements: %f'));
        %disp(tline)
    end
    
    if ( strcmp('Linf-norm: ',tline))
        tline = fgetl(fid);
        C(3) = cell2mat(textscan(tline, 'before, error on all elements: %f'));
        %disp(tline)
        tline = fgetl(fid);
        C(4) = cell2mat(textscan(tline, 'after, error on all elements: %f'));
        %disp(tline)
    end
end
fclose(fid);
end