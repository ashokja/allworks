clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20];

abc = 'data/triangleMesh_O1_I2_2_S_0.1_R_100_pP_2D.txt ';

R = 100;
poly =2;
order = poly;
count =1;
scaling1 = '0.1';
scaling2 = '0.2';
%prefixFilename = 'data/P4_triSplit6_L4_R1_';
%prefixFilename = 'data/P2_triSpli_L2_R1_';
datafile1 = 'dataCos';
datafile2 = 'dataCos2';
for poly = [2,3,4]
    for i = [2]
        for j = [1,2]
            if i ==1
                datafile = datafile1;
            else
                datafile = datafile2;
            end
            if j==1
                scaling = scaling1;
            else
                scaling = scaling2;
            end
            prefixFilename = strcat('data/',datafile,'/P',int2str(poly),'_triSplit6_L2_R1_');
            %int2str(int16(str2double(scaling)*10))
            prefixImg = strcat('Images/',datafile,'_P',int2str(poly),'_triSplit6_L2_R1_','_S_',int2str(int16(str2double(scaling)*10)),'_R_',int2str(R));

            %prefixFilename = strcat('data/',datafile,'/P',int2str(poly),'_triSplit6_L4_R1_');
            %prefixImg = strcat('Images/',datafile,'_P',int2str(poly),'_triSplit6_L4_R1_','_S_',int2str(int16(str2double(scaling)*10)),'_R_',int2str(R));


            xfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pX_2DDyn.txt' );
            yfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pY_2DDyn.txt' );
            ffname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pV_2DDyn.txt' );
            pfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pP_2DDyn.txt' );
            sfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pS_2DDyn.txt' );
            sDynfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pSDyn_2DDyn.txt' );
            dynsfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pDyn_2DDyn.txt' );

            xpos = textread(xfname, '%f\t');
            ypos = textread(yfname, '%f\t');
            fpos = textread(ffname, '%f\t');
            ppos = textread(pfname, '%f\t');
            spos = textread(sfname, '%f\t');
            sDynpos = textread(sDynfname, '%f\t');
            dynspos = textread(dynsfname, '%f\t');

            xVpos = reshape(xpos,[R,R]);
            yVpos = reshape(ypos,[R,R]);
            fVpos = reshape(fpos,[R,R]);
            pVpos = reshape(ppos,[R,R]);
            sVpos = reshape(spos,[R,R]);
            sDynVpos = reshape(sDynpos,[R,R]);
            dynsVpos = reshape(dynspos,[R,R]);

            figure(1),
            surf(xVpos,yVpos,fVpos);

            figure(2),
            surf(xVpos,yVpos,pVpos);

            figure(3),
            surf(xVpos,yVpos,sVpos);

            %%
            figure(4),
            surf(xVpos,yVpos,abs(fVpos-pVpos));
            title('projection errors');
            figure(5),
            surf(xVpos,yVpos,abs(fVpos-sVpos));
            title('SIAC errors'); hold off;
            figure(51),
            surf(xVpos,yVpos,abs(fVpos-sDynVpos));
            title('SIAC Dyn errors');
            %%

            figure(6),
            contourf(xVpos,yVpos,log10(abs(fVpos-pVpos)));
            title('projection errors'); colorbar(); caxis([-10,0]);
            view(0,90);
            %print(strcat(prefixImg,'Proj'), '-dpng', '-r100');

            figure(7),
            contourf(xVpos,yVpos,log10(abs(fVpos-sVpos)));
            title(strcat('SIAC errors (H=',scaling,')')); colorbar(); caxis([-10,0]);
            view(0,90); hold on;
            %print(strcat(prefixImg,'LSIAC_scal'), '-dpng', '-r100');

            figure(8),
            contourf(xVpos,yVpos,log10(abs(fVpos-sDynVpos)));
            title('SIAC Dyn errors'); colorbar(); caxis([-10,0]);
            view(0,90);
            %print(strcat(prefixImg,'LSIAC_Dyn'), '-dpng', '-r100');

            figure(9),
            surf(xVpos,yVpos, dynsVpos);
            shading interp; colorbar(); 
            view(0,90); hold off;
            print(strcat('Images/triSplit6_L4_R1_','Scaling'), '-dpng', '-r100');
            
            disp('Proj');
            disp(sum(sum((abs(fVpos-pVpos)))));
            disp(max(max((abs(fVpos-pVpos)))));
            disp('SIAC');
            disp(sum(sum((abs(fVpos-sVpos)))));
            disp(max(max((abs(fVpos-sVpos)))));

            disp('SIAC Dyn');
            disp(sum(sum((abs(fVpos-sDynVpos)))));
            disp(max(max((abs(fVpos-sDynVpos)))));
        end
    end
end

%%
figure (10);
    addpath('../');
    plotNekMesh('data/dataCos2/P2_triSplit6_L4_R1.xml');
    xlim([-1,1]);
    ylim([-1,1]);
    plot3([-1,1],[0,0],[32,32],'--r')
    print(strcat('Images/triSplit6_L4_R1_','meshFile'), '-dpng', '-r100');
%%    
    
% figure(9),
% surf(xVpos,yVpos,dynsVpos);
% title('dynscaling'); shading Interp;
% 
% figure(10),
% contourf(xVpos,yVpos,dynsVpos);
% title('dynscaling'); 


