function [x,ret,debugX,debugY,y]=critsubmultinewton(x,NumIters, TR, Tri,xvSorted,yvSorted, uvSorted,uxvSorted,uyvSorted,vvSorted, vxvSorted, vyvSorted)
%Performs multidimensional Newton?s method for the function defined in f
%starting with x and running NumIters times.
    [y,dy,ret]=critSubEval(x(1),x(2),TR, Tri,xvSorted,yvSorted, uvSorted,uxvSorted,uyvSorted,vvSorted, vxvSorted, vyvSorted);
    for j=1:NumIters
        s=dy\y;
        x=x-s;
        debugX(j,:) = x;
        debugY(j,:) = y;
        [y,dy,ret]=critSubEval(x(1),x(2),TR, Tri,xvSorted,yvSorted, uvSorted,uxvSorted,uyvSorted,vvSorted, vxvSorted, vyvSorted);
        if (0==ret)
            disp(j);
            return;
        end
        
    end

end