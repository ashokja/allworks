#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include <boost/math/special_functions/sinc.hpp>
#include <boost/timer.hpp>
#include <chrono>
#include "MetricTensor.h"
#include <math.h>

#define PI 3.14159265

typedef std::chrono::high_resolution_clock Clock;


using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 7)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2st arg conditions.xml file." << endl;
		cout << "3nd arg polynomial degree filter you want to apply" << endl;
		cout << "4th arg Resolution of output." << endl;
		cout << "5th minScaling" << endl;
		cout << "6th maxScaling" << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();
	HNM2D->LoadData(fname+".fld",var);

	MetricTensor* metricT = new MetricTensor();
	metricT->LoadMetricTensor(HNM2D);
	metricT->CalculateMetricTensorAtNodes();
	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();

	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;

	Array<OneD,NekDouble> fce = HNM2D->m_expansions[0]->GetPhys();
	LibUtilities::EquationSharedPtr ffunc
					= vSession->GetFunction("ExactSolution", 0); 


	NekDouble minx, miny,minz,maxx,maxy,maxz;
	minx = 0.0; miny= 0.0; minz=0.0;
	maxx= 1.0; maxy=1.0; maxz=1.0;

	vector<int> elementIds, gl_elementIds;
	HNM2D->IntersectWithBoxUsingRTree( minx-0.0001, miny, minz, maxx+0.0001,maxy,maxz,
		elementIds, gl_elementIds);
	cout << "Total Elem in bounds " << elementIds.size() << endl;
	
	// pick a location and work on it.	
	NekDouble minLength, maxLength;
	minLength = atof(argv[5]);
	maxLength = atof(argv[6]);
	int Res = atoi(argv[4]);
	
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1, HNM2D, atoi(argv[1+2]), scaling ); 
	SmoothieSIAC2D sm_NONSYM(SIACUtilities::eSYM_UNEVEN_2kp1, HNM2D, atoi(argv[1+2]), scaling ); 
	SmoothieSIAC2D sm_NONSYM_NSK2(SIACUtilities::eSYM_UNEVEN_2kp1, HNM2D, atoi(argv[1+2]), scaling ); 

	Array<OneD,NekDouble> glCoords(3,0.0),eig1(3,0.0),eig2(3,0.0);
	//eig[0]= 1.0;
	Array<OneD,NekDouble> pSMetLy(Res,0.0), pScale(Res,0.0),pSMetEr(Res,0.0);
	Array<OneD,NekDouble> pSMetLy2(Res,0.0), pScale2(Res,0.0),pSMetEr2(Res,0.0);
	Array<OneD,NekDouble> pXLy(Res,0.0), pYLy(Res,0.0), pVLy(Res,0.0), pLamb1(Res,0.0), pLamb2(Res,0.0),pValue(Res,0.0);
	NekDouble lambda1, lambda2;

	NekDouble valY,valZ;
	valY=0.0; valZ=0.0;
	int Ny=Res;
	NekDouble sy = 1.0/(Ny-1.0);

	NektarBaseClass k;
	for (int j=0;j<Res;j++)
	{
		int index =j;
		pYLy[index] = 0.5;
		pXLy[index] = j*sy;		
		pVLy[index] = ffunc->Evaluate(pXLy[index],pYLy[index],0.0,0.0);
	
		glCoords[0] = pXLy[index]; glCoords[1] = pYLy[index];
		metricT->GetEigenPair(glCoords,-1,1,lambda1,eig1);
		metricT->GetEigenPair(glCoords,-1,2,lambda2,eig2);
		pLamb1[index] = lambda1;
		pLamb2[index] = lambda2;
		for (int i=0;i < Res; i++)
		{
			lambda1 = minLength + (maxLength-minLength)*(i/(Res-1.0));
			lambda2 = minLength + (maxLength-minLength)*(i/(Res-1.0));
			pScale[i] = lambda1;
			pScale2[i] = lambda2;
			sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMetLy[i], valY, valZ, eig1, lambda1,0);
			sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMetLy2[i], valY, valZ, eig2, lambda2,0);
			pSMetEr[i] = pSMetLy[i]-pVLy[index];
			pSMetEr2[i] = pSMetLy2[i]-pVLy[index];
		}

		k.writeNekArray(pScale,fname+"_j_"+to_string(j)+"_"+argv[3]+"_Res_"+argv[4]+"_pScale.txt");
		k.writeNekArray(pSMetLy,fname+"_j_"+to_string(j)+"_"+argv[3]+"_Res_"+argv[4]+"_pMetLy.txt");
		k.writeNekArray(pSMetEr,fname+"_j_"+to_string(j)+"_"+argv[3]+"_Res_"+argv[4]+"_pMetEr.txt");
	
		k.writeNekArray(pScale2,fname+"_j_"+to_string(j)+"_"+argv[3]+"_Res_"+argv[4]+"_pScale2.txt");
		k.writeNekArray(pSMetLy2,fname+"_j_"+to_string(j)+"_"+argv[3]+"_Res_"+argv[4]+"_pMetLy2.txt");
		k.writeNekArray(pSMetEr2,fname+"_j_"+to_string(j)+"_"+argv[3]+"_Res_"+argv[4]+"_pMetEr2.txt");
	}
	
	k.writeNekArray(pXLy,fname+"_"+argv[3]+"_Res_"+argv[4]+"_pX.txt");
	k.writeNekArray(pVLy,fname+"_"+argv[3]+"_Res_"+argv[4]+"_pV.txt");
	k.writeNekArray(pLamb1,fname+"_"+argv[3]+"_Res_"+argv[4]+"_pLmab1.txt");
	k.writeNekArray(pLamb2,fname+"_"+argv[3]+"_Res_"+argv[4]+"_pLamb2.txt");

	return 0;
}

