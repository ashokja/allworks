function coeff = buildCoeff2D(n1,n2,dataIn,dataOut)

    switch nargin 
        case 0,
            n1=4; n2=4; 
            xx = (-1:.4:1)';
            yy= (-1:.4:1)';
            [X,Y] = meshgrid(xx,yy);
            dataIn(:,1) = reshape(X,[],1);
            dataIn(:,2) = reshape(Y,[],1);
            dataOut = dataIn;
    end
    
    
    count=1;
    for k1 = 0:n1
        for k2 = 0:n2
                mat(:,count) = B2(n1,k1,n2,k2,dataIn);
                count = count +1;
        end
    end
    
    coeff(:,1) = mat\dataOut(:,1);
    coeff(:,2) = mat\dataOut(:,2);


    function [rest] =  B2(n1,k1,n2,k2,dataIn)
        v1 = B(n1,k1,dataIn(:,1));
        v2 = B(n2,k2,dataIn(:,2));
        rest = v1.*v2;
    end
    
    function [rest] = B3(n1,k1,n2,k2,n3,k3,dataIn)
        v1 = B(n1,k1,dataIn(:,1));
        v2 = B(n2,k2,dataIn(:,2));
        v3 = B(n3,k3,dataIn(:,3));
        rest = v1.*v2.*v3;    
    end
    
    
    function [rest] = B2_der(n1,k1,n2,k2,dataIn)
        v1 =     B(n1,k1,dataIn(:,1));
        v1_der = B_der(n1,k1,dataIn(:,1));
        v2 =     B(n2,k2,dataIn(:,2));
        v2_der = B_der(n2,k2,dataIn(:,2));
        
        rest(:,1) = v1_der.*v2;
        rest(:,2) = v1.*v2_der;
    end

    function [rest] = B3_der(n1,k1,n2,k2,n3,k3,dataIn)
        v1 =     B(n1,k1,dataIn(:,1));
        v1_der = B_der(n1,k1,dataIn(:,1));
        v2 =     B(n2,k2,dataIn(:,2));
        v2_der = B_der(n2,k2,dataIn(:,2));
        v3 =     B(n3,k3,dataIn(:,3));
        v3_der = B_der(n3,k3,dataIn(:,3));
        rest(:,1) = v1_der.*v2.*v3;
        rest(:,2) = v1.*v2_der.*v3;
        rest(:,3) = v1.*v2.*v3_der;
    end

    function [rest] = B2_eval(n1,n2,coeff,dataIn)
       count =1;
       rest = zeros(size(dataIn));
       for kk1 = 0:n1
           for kk2 = 0:n2
               rest(:,1) = rest(:,1)  + coeff(count,1)*B2(n1,kk1,n2,kk2,dataIn);
               rest(:,2) = rest(:,2)  + coeff(count,2)*B2(n1,kk1,n2,kk2,dataIn);
               count= count +1;
           end
       end
    end

    function [rest] = B2_eval_3(n1,n2,coeff,dataIn)
       count =1;
       [a,b] = size(dataIn);
       rest = zeros(a,b+1);
       for kk1 = 0:n1
           for kk2 = 0:n2
               rest(:,1) = rest(:,1)  + coeff(count,1)*B2(n1,kk1,n2,kk2,dataIn);
               rest(:,2) = rest(:,2)  + coeff(count,2)*B2(n1,kk1,n2,kk2,dataIn);
               rest(:,3) = rest(:,3)  + coeff(count,3)*B2(n1,kk1,n2,kk2,dataIn);
               count= count +1;
           end
       end
    end

    function [rest] = B3_eval(n1,n2,n3,coeff,dataIn)
       count =1;
       rest = zeros(size(dataIn));
       for kk1 = 0:n1
           for kk2 = 0:n2
               for k3= 0:n3
                   rest(:,1) = rest(:,1)  + coeff(count,1)*B3(n1,kk1,n2,kk2,n3,k3,dataIn);
                   rest(:,2) = rest(:,2)  + coeff(count,2)*B3(n1,kk1,n2,kk2,n3,k3,dataIn);
                   rest(:,3) = rest(:,3)  + coeff(count,3)*B3(n1,kk1,n2,kk2,n3,k3,dataIn);
                   count= count +1;
               end
           end
       end
    end

    function [rest] = B3_der_eval(n1,n2,n3,coeff,dataIn)
        count = 1;
       [a,b] = size(dataIn);
       rest = zeros(a,b+6);
       for kk1 = 0:n1
           for kk2 = 0:n2
               for kk3 = 0:n3
                    rest(:,1:3) = rest(:,1:3)  + coeff(count,1)*B3_der(n1,kk1,n2,kk2,n3,kk3,dataIn);
                    rest(:,4:6) = rest(:,4:6)  + coeff(count,2)*B3_der(n1,kk1,n2,kk2,n3,kk3,dataIn);
                    rest(:,7:9) = rest(:,7:9)  + coeff(count,3)*B3_der(n1,kk1,n2,kk2,n3,kk3,dataIn);
                    count= count +1;
               end
           end
       end        
    end

    function [rest] = B2_der_eval(n1,n2,coeff,dataIn)
       count =1;
       [a,b] = size(dataIn);
       rest = zeros(a,b+2);
       for kk1 = 0:n1
           for kk2 = 0:n2
               rest(:,1:2) = rest(:,1:2)  + coeff(count,1)*B2_der(n1,kk1,n2,kk2,dataIn);
               rest(:,3:4) = rest(:,3:4)  + coeff(count,2)*B2_der(n1,kk1,n2,kk2,dataIn);
               count= count +1;
           end
       end
    end

    function [res] = B(n,k,data)
        % assumed data varies from -1 to 1
        if n<k || k<0
            res = zeros(size(data));
            return
        end
        data = (data+1)/2;
        res = nchoosek(n,k)*((data.^k).*((1-data).^(n-k)));
    end

    function [res] = B_der(n,k,data)
        res = n*(B(n-1,k-1,data) - B(n-1,k,data))/2;
    end
end