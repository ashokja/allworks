function [n1,n2,n3,coeff] = test_3Dbuildsolve2ndIter(n1,n2,n3)
            % created a attachment and dettachment line which are not straight.
    switch nargin
        case 0,
            n1 = 6; n2 = 6; n3 = 6;
    end
%     [xx,w1] = lglnodes(d2);
%     [yy,w2] = lglnodes(d1);
%     [zz,w3] = lglnodes(d3);
    xx = (-1:.2:1)';
    yy= (-1:.2:1)';
    zz = (-1:.2:1)';
    %x = (-1:2/d1:1);
    %y = (-1:2/d2:1);
    [X,Y,Z] = meshgrid(xx,yy,zz);
    [sx,~] = size(xx);
    [sy,~] = size(yy);
    [sz,~] = size(zz);

    for i = 1:sx
    	for j = 1:sy
        	for k = 1:sz
            	%rc = rand(1,3);
                 x =  X(i,j,k)+1.001; y = Y(i,j,k)+1; z = Z(i,j,k);
                 teta = atan(y/x);
                 a = [x-cos(teta),y-sin(teta),z];
                 vel = cross(a,[sin(teta),-cos(teta),0]) + [cos(teta),sin(teta),0]*.1;
                
                % vel = vel./norm(vel);
                if ( j <sx+1)
                    U(i,j,k) = vel(1) ;
                    V(i,j,k) = vel(2);
                    W(i,j,k) = vel(3);
                else
                    U(i,j,k) = 0;
                    V(i,j,k) = 1;
                    W(i,j,k) = 0;
                end
             end
        end
    end
            %Complicated function.
%     px = sin(2*X+(2*Y).^2);
%     py = cos(2*X+(2*Y).^2);
%     pz = Z;
            %Simple Saddle function.
    px = U;
    py = V;
    pz = W;

            % viz
    figure (1),
    quiver3(X,Y,Z,px,py,pz);


    

end