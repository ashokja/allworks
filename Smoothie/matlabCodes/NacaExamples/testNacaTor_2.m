% test naca for calculating torsion across a plane.
clear;
close all;

%UsebkupData = 'bkup_SI_TOR/'
UsebkupData = 'bkup_Jan24/';
%UsebkupData = ''
%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05'
filename = strcat('P0000009_So_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ATRVar' );
filepath = strcat('data/',UsebkupData,filename);
disp(filepath);


xfname =  strcat(filepath,'_pX.txt' )
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );
%torPfname =  strcat(filepath,'_pTor_p.txt' );
torSfname =  strcat(filepath,'_pTor_s.txt' );
%DbvSfname =  strcat(filepath,'_pDbv_s.txt' );
%testSfname =  strcat(filepath,'_pVz_s.txt' );




xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');
%torPval = textread(torPfname, '%f\t');
torSval = textread(torSfname, '%f\t');
%DbvSval = textread(DbvSfname, '%f\t');
%testSval = textread(testSfname, '%f\t');

epos = floor(epos +0.0001);

s = floor( (max(size(xpos)))^(1/2) +0.1);

%upfname = strcat(filepath,'_pU_p.txt' );

xVpos = reshape(xpos,s,s);
yVpos = reshape(ypos,s,s);
zVpos = reshape(zpos,s,s);

%torPVval = reshape(torPval,s,s);
torSVval = reshape(torSval,s,s);
%DbvSVval = reshape(DbvSval,s,s);
%testSVval = reshape(testSval,s,s);

figure(2),
surf(yVpos,zVpos,torSVval)
shading interp;
grid on;
grid minor
%%
figure(1),
contourf(yVpos,zVpos,torSVval,10),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
contourcbar
title('Torsion Contour (2)');
%print('torsion_contour_t2','-dpng','-r300');

figure(3),
contour3(yVpos,zVpos,torSVval,[0 100000000],'LineColor','c'); hold on;
%surf(yVpos,zVpos,torsion_Vpos), shading interp;
contour3(yVpos,zVpos,torSVval,12,'LineColor','k'); hold on;
surf(yVpos,zVpos,torSVval), shading interp;
xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
axis off;
grid off;
view(0,90);
print('images/Feb22/torsion_1dirLSIAC_contour','-dpng', '-r500')



% figure(3);
% contour(yVpos,zVpos,torSVval,[-1,0,1])
% contourcbar
% 
% 
% figure(4);
% surf(yVpos,zVpos,DbvSVval)
% shading interp;
% colorbar;
% grid on;
% grid minor
% 
% figure(5);
% contour(yVpos,zVpos,DbvSVval,[-1.1,-1.0,-0.8])
% contourcbar
% 
% 
% figure(6);
% surf(yVpos,zVpos,testSVval)
% shading interp;
% colorbar;
% grid on;
% grid minor

%figure(7);
%contour(yVpos,zVpos,testSVval,[-1.1,-1.0,-0.8])
%contourcbar



