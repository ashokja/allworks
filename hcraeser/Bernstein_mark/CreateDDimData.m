function[A_out] = CreateDDimData(X,d) % Create d Dim Data
   % d should be >= 0 .i.e. start from 1;
   % X = Vector of (1,q);
   if 1 == d
      A_out = X';
      return
   end
   A = CreateDDimData(X,d-1);
   [numA,de]=size(A);
   [de,q] = size(X);
   count =1;
   for i_l =1:q
       for i_numA = 1:numA
           A_out(count,:) = [X(i_l),A(i_numA,:)];
           count = count+1;
%           A_out(i_l*numA,:) = [X(i_l),A(i_numA,:)]
       end
   end

end