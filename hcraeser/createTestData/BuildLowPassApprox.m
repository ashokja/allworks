function [UR,VR,WR] = BuildLowPassApprox(U,V,W,n,nw)

    switch nargin 
        case 0
            load wmri;
            n = 2;
            nw = 2;
        case 3
            n = 2;
            nw = 2;
        case 4
            nw = n;
    end
    
    w = 'sym4'; %Near symmetric wavelet
    UT = wavedec3(U,n,w);
    VT = wavedec3(V,n,w);
    WT = wavedec3(W,n,w);
    
    UA = cell(1,n);VA = UA; WA = UA;
    UD = cell(1,n);VD = UD; WD = UD;
    for k =1 :n
        UA{k} = waverec3(UT,'a',k);
        UD{k} = waverec3(UT,'d',k);
        VA{k} = waverec3(VT,'a',k);
        VD{k} = waverec3(VT,'d',k);
        WA{k} = waverec3(WT,'a',k);
        WD{k} = waverec3(WT,'d',k);
    end
    
    UR = waverec3(UT,'a',nw);
    VR = waverec3(VT,'a',nw);
    WR = waverec3(WT,'a',nw);

end