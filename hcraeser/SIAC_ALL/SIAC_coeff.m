function coeff = SIAC_coeff(lambda,k,r)

    switch nargin
        case 0
            lambda = 0;
            k=1;
            r=2*k;
        case 1
            lambda = 1;
            k=1;
             r=2*k;
        case 2
            r = 2*k;
    end
    addpath('../PolyLib');
    % r and w for integration.
    
    rSize = r+1;
    cSize = r+1;
    linM = zeros(rSize,cSize);
    bSplKnots = -(k+1)/2:1:(k+1)/2;
    for row = 0:1:rSize-1
        for c = 0:1:cSize-1
            gamma = c;
            x_g = -r/2+gamma+lambda;
            for i =0:1:max(size(bSplKnots))-2
                bGamma = [ bSplKnots(i+1),bSplKnots(i+1+1)];
                % s,w for integration.
                % degree for integration is k+r
                dI = k+1+row;
                no_s = ceil(((dI)+3)/2);
                [sPts,w] = JacobiGZW(no_s,0,0);
                xPts = bGamma(1) + (bGamma(2)-bGamma(1))*(sPts+1)/2;
                disp(xPts+x_g);
                integral = sum(BSpl_central(xPts,k).*((xPts+x_g).^row).*w*abs(bGamma(2)-bGamma(1))/2);
                linM(row+1,c+1) = linM(row+1,c+1)+ integral;
            end
        end
    end
    if (nargin <3)
        disp(linM)
    end
    disp(cond(linM));
    rhs = zeros(1,rSize);rhs(1) =1;
    %rhs = rhs+1;
    coeff = linM\rhs';
    %linM
    %rhs
    %coeff
    

end