#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include "OneSidedSIAC.h"

#include <iomanip> // std::setprecision
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
#define TOLERENCE_VIZ 1e-6

using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 7)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg Type of Filter to use." << endl;
		cout << "4th arg Res. " << endl;
		cout << "5th arg [1,2] 1 constant func 1.0, 2 cosy. " << endl;
		cout << "6th arg index you want to check. " << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM2D->LoadMesh(var[0]);
	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);
//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);

	for (int i=0;  i< tNquadPts; i++)
	{
		if (atoi(argv[5])==1)
		{
			fce[i] = 1.0; //std::cos(2.0*M_PI*(xc1[i]));
		}else
		{
			fce[i] = std::cos(2.0*M_PI*(xc1[i]));
		}
		//fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]));
		//fce[i] = std::cos(2.0*M_PI*(xc0[i]))*std::cos(2.0*M_PI*(xc1[i]));
	}
	
	

	HNM2D->m_expansions[0]->FwdTrans(fce,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	 ece = HNM2D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM2D->m_expansions[0]->GetCoeffs();
	HNM2D->m_Arrays.push_back(ece);
	HNM2D->LoadExpListIntoRTree();
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();

// Evaluate on a new equal space grid mesh.
	int gPts = atoi(argv[4]);
	int totPts = gPts;
	int Nx = gPts, Ny=gPts;
	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0);


	SIACUtilities::FilterType filter = SIACUtilities::eSYM_2kp1_1SIDED_2kp1;
	if (atoi(argv[3]) ==1 ) // 2kp1
	{
		filter = SIACUtilities::eSYM_2kp1_1SIDED_2kp1;	
	}else if (atoi(argv[3]) ==2 ) // 4kp1
	{
		filter = SIACUtilities::eSYM_2kp1_1SIDED_4kp1;	
	}else if (atoi(argv[3]) ==3 ) // Xli
	{
		filter = SIACUtilities::eSYM_2kp1_1SIDED_2kp2;	
	}
	SmoothieSIAC2D sm(filter , HNM2D, atoi(argv[2]), scaling ); 

	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	Array<OneD,NekDouble> direction(3,0.0), coord(3,0.0);
	direction[1] = 1.0;
	Array<OneD,NekDouble> pX(totPts), pY(totPts), pV(totPts), pP(totPts), pS(totPts);
	for(int j=0; j< Ny; j++)
	{
	j = atoi(argv[6]);
		int index = j;
		pX[index] = 0.5278;
		pY[index] = j*sy;
		if (atoi(argv[5])==1)
		{
			pV[index] = 1.0;// std::cos(2.0*M_PI*(pY[index]) );
		}else{
			pV[index] = std::cos(2.0*M_PI*(pY[index]) );
		}
		//pV[index] = std::cos(2.0*M_PI*(pX[index] + pY[index]) );
		//pV[index] = std::cos(2.0*M_PI*(pX[index]))*std::cos(2.0*M_PI*(pY[index]));
		sm.EvaluateAt(pX[index],pY[index],0.0,pS[index],valY,valZ,direction, scaling,0);
		coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = 0.0;
		LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(coord);
		int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
		int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elId);
		const Array<OneD,NekDouble> el_phys = ece.CreateWithOffset(
									ece, coeffOffset);
		pP[index] = lexp->PhysEvaluate(coord,el_phys);
	cout <<index<< ":\t"<< pV[index] <<"\t"<< pP[index] <<"\t"<< pS[index] << endl;
	cout <<index<< ":\t"<< pV[index] <<"\t"<< pP[index]-pV[index] <<"\t"<< pS[index]-pV[index] << endl;
	return 0;
	}

	NektarBaseClass k;
	k.writeNekArray(pX,fname+"_"+argv[2]+"_"+argv[3]+"_"+argv[4]+"_pX_2D.txt");
	k.writeNekArray(pY,fname+"_"+argv[2]+"_"+argv[3]+"_"+argv[4]+"_pY_2D.txt");
	k.writeNekArray(pV,fname+"_"+argv[2]+"_"+argv[3]+"_"+argv[4]+"_pV_2D.txt");
	k.writeNekArray(pP,fname+"_"+argv[2]+"_"+argv[3]+"_"+argv[4]+"_pP_2D.txt");
	k.writeNekArray(pS,fname+"_"+argv[2]+"_"+argv[3]+"_"+argv[4]+"_pS_2D.txt");
	return 0;

}

