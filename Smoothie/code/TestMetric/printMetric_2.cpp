#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include "MetricTensor.h"


using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg conditions file." << endl;
		cout << "3rd arg resolutoin" << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM2D->LoadMesh(var[0]);
	string fname = vSession->GetSessionName();
	MetricTensor* metricT = new MetricTensor();
	metricT->LoadMetricTensor(HNM2D);
	metricT->CalculateMetricTensorAtNodes();
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}

	NekDouble minX,minY,maxX,maxY;
	minX = Vmath::Vmin(tNquadPts,xc0,1); 
	minY = Vmath::Vmin(tNquadPts,xc1,1); 
	maxY = Vmath::Vmax(tNquadPts,xc1,1); 
	maxX = Vmath::Vmax(tNquadPts,xc0,1); 
	cout << "min and max of in X:\t" << minX << "\t" <<maxX << endl;
	cout << "min and max of in Y:\t" << minY << "\t" <<maxY << endl;

/*
    // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);

	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]));
	}
	
	HNM2D->m_expansions[0]->FwdTrans(fce,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	ece = HNM2D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM2D->m_expansions[0]->GetCoeffs();

	// Evaluating on line y =0;
	// Evaluate on a new equal space grid mesh.
*/
	HNM2D->m_Arrays.push_back(HNM2D->m_expansions[0]->GetPhys());
	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();

	
	int gPtsLy = atoi(argv[3])+1;
	int totPtsLy = gPtsLy*gPtsLy;
	NekDouble sx = (maxX-minX)/(gPtsLy-1);	
	NekDouble sy = (maxY-minY)/(gPtsLy-1);	
	Array<OneD,NekDouble> pXLy(totPtsLy), pYLy(totPtsLy);
	Array<OneD,NekDouble> pSMet1Ly(totPtsLy),pSMet2Ly(totPtsLy);
	Array<OneD,NekDouble> pEigV1(totPtsLy),pEigV2(totPtsLy);
	Array<OneD,NekDouble> pEigVec11(totPtsLy),pEigVec12(totPtsLy);
	Array<OneD,NekDouble> pEigVec21(totPtsLy),pEigVec22(totPtsLy);
	Array<OneD,NekDouble> pEigV3(totPtsLy),pEigV4(totPtsLy);
	Array<OneD,NekDouble> pEigVec31(totPtsLy),pEigVec32(totPtsLy);
	Array<OneD,NekDouble> pEigVec41(totPtsLy),pEigVec42(totPtsLy);
	NekDouble lambda;
	Array<OneD,NekDouble> eig(3,0.0),glCoords(3,0.0);
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();
	for (int i =0 ; i< gPtsLy; i++)
	{
		for (int j =0 ; j< gPtsLy; j++)
		{
			int index = j+i*gPtsLy;
			pXLy[index] = minX+ i*sx;
			pYLy[index] = minY+ j*sy;
			//cout << "index" << index << endl;
			glCoords[0] = pXLy[index]; glCoords[1] = pYLy[index];
			int elid = HNM2D->GetExpansionIndexUsingRTree(glCoords);
			metricT->GetEigenPair(glCoords, elid, 1,lambda,eig);
			pEigV1[index] = lambda;
			pEigVec11[index] = eig[0]; pEigVec12[index] = eig[1];
			metricT->GetEigenPair(glCoords, elid, 2,lambda,eig);
			pEigV2[index] = lambda;
			pEigVec21[index] = eig[0]; pEigVec22[index] = eig[1];


			metricT->GetEigenPairUsingIP(glCoords, elid, 1,lambda,eig);
			pEigV3[index] = lambda;
			pEigVec31[index] = eig[0]; pEigVec32[index] = eig[1];

			metricT->GetEigenPairUsingIP(glCoords, elid, 2,lambda,eig);
			pEigV4[index] = lambda;
			pEigVec41[index] = eig[0]; pEigVec42[index] = eig[1];
		}
	}
	
	NektarBaseClass k;
	k.writeNekArrayBin(pXLy,fname+"_R_"+argv[3]+"_pX.bin");
	k.writeNekArrayBin(pYLy,fname+"_R_"+argv[3]+"_pY.bin");
	k.writeNekArrayBin(pEigV1,fname+"_R_"+argv[3]+"_pEigV1.bin");
	k.writeNekArrayBin(pEigV2,fname+"_R_"+argv[3]+"_pEigV2.bin");
	k.writeNekArrayBin(pEigVec11,fname+"_R_"+argv[3]+"_pEigVec11.bin");
	k.writeNekArrayBin(pEigVec12,fname+"_R_"+argv[3]+"_pEigVec12.bin");
	k.writeNekArrayBin(pEigVec21,fname+"_R_"+argv[3]+"_pEigVec21.bin");
	k.writeNekArrayBin(pEigVec22,fname+"_R_"+argv[3]+"_pEigVec22.bin");
	
	k.writeNekArrayBin(pEigV3,fname+"_R_"+argv[3]+"_pEigV3.bin");
	k.writeNekArrayBin(pEigV4,fname+"_R_"+argv[3]+"_pEigV4.bin");
	k.writeNekArrayBin(pEigVec31,fname+"_R_"+argv[3]+"_pEigVec31.bin");
	k.writeNekArrayBin(pEigVec32,fname+"_R_"+argv[3]+"_pEigVec32.bin");
	k.writeNekArrayBin(pEigVec41,fname+"_R_"+argv[3]+"_pEigVec41.bin");
	k.writeNekArrayBin(pEigVec42,fname+"_R_"+argv[3]+"_pEigVec42.bin");

	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}

