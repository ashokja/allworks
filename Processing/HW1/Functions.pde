
String[] myGLoadCRVfile(String s)
{
//  println(s);
  String[] lines = loadStrings(s);
  String line;

  myGNextLine =0;
  line = myGetNextLine(lines);
  // Number of Points.  
  String[] list = split(line,',');
  if (list[0].equals("P"))
  {
     //CurveType =1;
       //print rational polynomial.
  }
  myGNumCRVPoints = Integer.parseInt(trim(list[1]));
  myGCRVPoints = new ArrayList<PVector> ();
  for (int i =0; i < myGNumCRVPoints; i++)
  {
      list  = split(myGetNextLine(lines),',');
      PVector p0 = new PVector(0.0,0.0,1.0);
      p0.x = Float.parseFloat(list[0]);
      p0.y = Float.parseFloat(list[1]);
     // println(p0);
      myGCRVPoints.add(p0);    
  }  
  return lines;
}


void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    String filepath = selection.getAbsolutePath();
    println("User selected " + filepath);
    // load file here
    myLoadFile(filepath);
//    txtFile = loadStrings(filepath);
  }
}


void myDrawControlBox()
{  
  fill(128);
  rect( ScreenX ,0 ,CScreenX,ScreenY);
}

void myDrawLines(ArrayList<PVector> points)
{
  if (1 == myGLinesVisible)
  {
//    println(myGLinesVisible);
    for (int i =1; i < points.size(); i++)
    {
      PVector p1,p2;
      p1 = points.get(i-1);
      p2 = points.get(i);
      //line(CToS(p1.x,1),CToS(p1.y,2),CToS(p2.x,1),CToS(p2.y,2));
      line(CToS(p1.x/p1.z,1),CToS(p1.y/p1.z,2),CToS(p2.x/p2.z,1),CToS(p2.y/p2.z,2));
    }
  }
}

void myDrawButtons()
{
  if ( 1 == myGCPVisible)
  {
  //  println(points_b.size());
    for (int c =0; c < curves_b.size(); c++)
    {
      ArrayList<Draggable> points_b = curves_b.get(c);
      ArrayList<PVector> points = curves.get(c);
      for (int i =0; i < points_b.size(); i++)
      {
        Draggable d = points_b.get(i);
        PVector p = points.get(i);
        //d.x = CToS(p.x/p.z,1);
        //d.y = CToS(p.y/p.z,1);
        d.rollover(mouseX,mouseY);
        d.drag(mouseX,mouseY);
        d.display(c,i);
      }
    }
  }
}

int CToS(float x, int cord)
{
  // cord 1 = x cord y = 1
  if( 1 == cord)
  {
    return (int)((x-ScreenXMin)/(ScreenXMax-ScreenXMin)*((float)ScreenX));
  }else
  {
    return (int)((x-ScreenYMin)/(ScreenYMax-ScreenYMin)*((float)ScreenY));
  } 
}

float SToC(int sx, int cord)
{
  if( 1 == cord)
  {
    return (float)( sx*(ScreenXMax-ScreenXMin)/((float)ScreenX) + ScreenXMin );
  }else
  {
    return (float)( sx*(ScreenYMax-ScreenYMin)/((float)ScreenY) + ScreenYMin );
  } 
}

PVector myPlotAt(ArrayList<PVector> points, float t, int i,int j)
{
  if (j<= i)
  {
    println("Error j <= i; this should not happen");
    return new PVector(0.0,0.0,0.0);
  }
  PVector ret = new PVector(0.0,0.0,1.0);
  if (j == i+1)
  {
    PVector p0 = points.get(i);
    PVector p1 = points.get(j);
    ret = new PVector(0.0,0.0,1.0);
    ret.x = t*p0.x + (1-t)*p1.x;
    ret.y = t*p0.y + (1-t)*p1.y;
    ret.z = t*p0.z + (1-t)*p1.z;
    //println(i+" "+j+" "+ret);
    return ret;
  }
  if (points2D[i][j].z < -100.0)
  {
    PVector p0 = myPlotAt(points,t,i,j-1);
    PVector p1 = myPlotAt(points,t,i+1,j);
    ret = new PVector(0.0,0.0,1.0);
    ret.x = t*p0.x + (1-t)*p1.x;
    ret.y = t*p0.y + (1-t)*p1.y;
    ret.z = t*p0.z + (1-t)*p1.z; 
    points2D[i][j] = ret;
  }
  else{
    ret = points2D[i][j];
  }
  //println(i+" "+j+" "+ret);
  return ret;
}

String myGetNextLine(String[] lines)
{
  //check if myGNextLine < lines.size();
  if (lines.length <= myGNextLine)
     return null;
  // Find the next line which is not a comment.
  int b = 0;
  while (b == 0)
  {
    // if next line comment ignore and increase line number.
     if (trim(lines[myGNextLine]).equals("") || lines[myGNextLine].substring(0,1).equals("#")  )
     {
//       println("Comment line " + lines[myGNextLine] );
       myGNextLine++;
       continue;
     }
     myGNextLine++;
     b =1;
  }
 return lines[myGNextLine-1];      
}

String[] myLoadFile(String s)
{
  // Determine which kind of file.
  String[] sList = split(s,'.');
  String[] lines ;
  if (sList[1].equals("crv"))
  {
    myGLoadedFileCRV = 1;
    myGNumOfCurves =1;
    curves = new ArrayList<ArrayList<PVector>> ();
    curves_b = new ArrayList<ArrayList<Draggable>> ();
    ArrayList<PVector> points = new ArrayList<PVector>();
    ArrayList<Draggable> points_b = new ArrayList<Draggable> ();
    
    curves.add(points);
    curves_b.add(points_b);
    PVector p0 = new PVector(0.0,0.0,1.0);
    PVector p1 = new PVector(1.0,1.0,1.0);
    points.add(p0); points.add(p1);
    Draggable d0 = new Draggable(CToS(p0.x/p0.z,1),CToS(p0.y/p0.z,2),10,10);
    points_b.add(d0);
    Draggable d1 = new Draggable(CToS(p1.x/p1.z,1),CToS(p1.y/p1.z,2),10,10);
    points_b.add(d1);
    for (int c =0; c < curves.size(); c++)
    {
      println(curves.get(c).size()-1);      
    }

    lines = myGLoadCRVfile(s);
  }else
  {
    myGLoadedFileCRV = 0;
  lines = loadStrings(s);
  String line;

  myGNextLine =0;
  line = myGetNextLine(lines);
  // Number of Curves.
  myGNumOfCurves = Integer.parseInt(trim(line));
  curves = new ArrayList<ArrayList<PVector>> ();
  curves_b = new ArrayList<ArrayList<Draggable>> ();
  
  
  for (int c =0; c < myGNumOfCurves; c++)
  {
    ArrayList<PVector> points = new ArrayList<PVector>();
    ArrayList<Draggable> points_b = new ArrayList<Draggable> ();
    
    curves.add(points);
    curves_b.add(points_b);
    line = myGetNextLine(lines);
    String[] list = split(line,',');
    int CurveType = 0;
    if (list[0].equals("P"))
    {
      CurveType =1;
       //print rational polynomial.
    }
    if (list[0].equals("Q"))
    {
      CurveType =2;
       // Print Polynomial Curve.
    }
    myGNumOfPoints = Integer.parseInt(trim(list[1]));

    for (int n =0; n< myGNumOfPoints; n++)
    {
      line = myGetNextLine(lines);
      list = split(line,',');
      PVector p0 = new PVector(0.0,0.0,1.0);
      p0.x = Float.parseFloat(list[0]);
      p0.y = Float.parseFloat(list[1]);
      if( 2 == CurveType )
      {
        p0.z = Float.parseFloat(list[2]);
      //  p0.x = p0.x/p0.z;
      //  p0.y = p0.y/p0.z;
      //  p0.z = 1.0;
      }else{
        p0.z = 1.0;
      }
      (curves.get(c)).add(p0);
      // Add Draggable Point also.
      Draggable d = new Draggable(CToS(p0.x/p0.z,1),CToS(p0.y/p0.z,2),10,10);
      points_b.add(d);
    }
  }
  }
 // println("Ahsok + curves number" + curves.size());
  //curves.get(0).size();
  return lines;
}
