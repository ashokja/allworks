function [] = VizStreamLines(xcheb,ycheb,zcheb,U,V,W,sx,sy,sz,c,t)
    switch nargin
        case 6,
            [sx, sy, sz] = meshgrid([-0.3:.1:0.3],[-1,1],[0]);
%            [sx, sy, sz] = meshgrid((-.2:.2:.2),[-1,0,1],(-.2:.2:.2));
            c = 'b';
            t = 1;
        case 9,
            c = 'b';
            t = 1.5;
        case 10,
            t = 1.5;
    end
    [X,Y,Z] = meshgrid(xcheb,ycheb,zcheb);
    U = permute(U,[2,1,3]);
    V = permute(V,[2,1,3]);
    W = permute(W,[2,1,3]);

    hlines = streamline(X,Y,Z,U,V,W,sx,sy,sz)  ;
    set(hlines,'LineWidth',t,'Color',c);
    xlabel('x'); ylabel('y'); zlabel('z');
end