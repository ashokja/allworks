%test fslove del.

function F = tethsolve3(x)

%   load the DG solution.


%   use interp method to calculate

F = [2*x(1)-x(2)-exp(-x(1));
        -1*x(1)+2*x(2)-exp(-x(2))];



end