#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField2D.h>
//#include <MultiRegions/DisContField2D.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"

#define MAXSCALING 2.0
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();

	string fname = vSession->GetSessionName();
	HNM2D->LoadData(fname+".chk", var);
	//HNM2D->LoadMesh(var[0]);
	//HNM2D->LoadMesh(var[1]);
	//HNM2D->LoadMesh(var[2]);
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	int tNCoeffs = HNM2D->m_expansions[0]->GetNcoeffs();
	cout << "Totpts:" << tNquadPts<< endl;
	cout << "NCoefs:" << tNCoeffs<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}

	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());

	HNM2D->m_expansions[1]->BwdTrans( HNM2D->m_expansions[1]->GetCoeffs(),
										HNM2D->m_expansions[1]->UpdatePhys());
	
	HNM2D->m_expansions[2]->BwdTrans( HNM2D->m_expansions[2]->GetCoeffs(),
										HNM2D->m_expansions[2]->UpdatePhys());

	Array<OneD,NekDouble> u_Phys, v_Phys, p_Phys;
	Array<OneD,NekDouble> u_Coeffs_upd(tNCoeffs), v_Coeffs_upd(tNCoeffs), p_Coeffs_upd(tNCoeffs);
	Array<OneD,NekDouble> u_Phys_upd(tNquadPts), v_Phys_upd(tNquadPts), p_Phys_upd(tNquadPts);
	u_Phys = HNM2D->m_expansions[0]->GetPhys();
	v_Phys = HNM2D->m_expansions[1]->GetPhys();
	p_Phys = HNM2D->m_expansions[2]->GetPhys();
	
	SmoothieSIAC2D sm(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]),1 ); 
	//SmoothieSIAC2D smu(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]),1 ); 
	//SmoothieSIAC2D smv(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]),1 ); 
	//SmoothieSIAC2D smp(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]),1 ); 

	Array<OneD,NekDouble> direction_x(3,0.0),direction_v(3,0.0) ;
	direction_x[0] = 1.0;
	direction_v[0] = 1.0;
	NekDouble valX,valY, valZ;

//  TEST CSE BEGIN
	//sm.EvaluateAt(18.88505,-5.874895, 0.0, valX ,valY,valZ,direction_x, 4.33698, 0);
	//return 0;
//  TEST CSE End



	for (int i=0; i< tNquadPts; i++)
	{
		if ( ( (4.0 <=xc0[i]) && (20.0 >=xc0[i]) ) && ( (-1.0<=xc1[i])&&(1.0>= xc1[i]) ) )
		{
			NekDouble largeElementSize = HNM2D->GetElLargestEdgeSize( xc0[i], xc1[i], xc2[i] );
			cout << "IN LargeEl SIze: /t" << largeElementSize<< endl;
			if (largeElementSize > MAXSCALING)
			{
				largeElementSize = MAXSCALING;
			}
			cout << xc0[i] << "\t" << xc1[i] << "\t" << xc2[i] << endl;
			sm.EvaluateAt(xc0[i],xc1[i], xc2[i], u_Phys_upd[i],valY,valZ,direction_x, largeElementSize, 0);
			sm.EvaluateAt(xc0[i],xc1[i], xc2[i], v_Phys_upd[i],valY,valZ,direction_x, largeElementSize, 1);
			sm.EvaluateAt(xc0[i],xc1[i], xc2[i], p_Phys_upd[i],valY,valZ,direction_x, largeElementSize, 2);
		}else
		{
			cout << "OUT LargeEl SIze: /t" << endl;
			u_Phys_upd[i] = u_Phys[i];
			v_Phys_upd[i] = v_Phys[i];
			p_Phys_upd[i] = p_Phys[i];
		}
	}

	cout << "Completely out " << endl;
	HNM2D->m_expansions[0]->FwdTrans(	u_Phys_upd, u_Coeffs_upd); 
	HNM2D->m_expansions[0]->FwdTrans(	v_Phys_upd, v_Coeffs_upd); 
	HNM2D->m_expansions[0]->FwdTrans(	p_Phys_upd, p_Coeffs_upd); 


	string out = vSession->GetSessionName() + ".fld";
	int DefSize = HNM2D->m_expansions[0]->GetFieldDefinitions().size();

	std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(DefSize);
    std::vector<std::vector<NekDouble> > FieldData(DefSize);
	for (int i =0; i < DefSize; i++)
	{
    	FieldDef[i] = (HNM2D->m_expansions[0]->GetFieldDefinitions()).at(i);
    	FieldDef[i]->m_fields.push_back("u");
    	FieldDef[i]->m_fields.push_back("v");
    	FieldDef[i]->m_fields.push_back("p");
    	FieldDef[i]->m_fields.push_back("u_upd");
    	FieldDef[i]->m_fields.push_back("v_upd");
    	FieldDef[i]->m_fields.push_back("p_upd");
    	HNM2D->m_expansions[0]->AppendFieldData(FieldDef[i], FieldData[i]);
    	HNM2D->m_expansions[1]->AppendFieldData(FieldDef[i], FieldData[i]);
    	HNM2D->m_expansions[2]->AppendFieldData(FieldDef[i], FieldData[i]);
    	HNM2D->m_expansions[2]->AppendFieldData(FieldDef[i], FieldData[i], u_Coeffs_upd);
    	HNM2D->m_expansions[2]->AppendFieldData(FieldDef[i], FieldData[i], v_Coeffs_upd);
    	HNM2D->m_expansions[2]->AppendFieldData(FieldDef[i], FieldData[i], p_Coeffs_upd);
		
	}
	HNM2D->m_fld->Write(out, FieldDef, FieldData);

	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
