%testNacaSpeed
%%%

clear;
close all;
va = -51.5;
vb = 18;

%alpha =1;
Poly = 3;
scaling = '0.05' ;
%Xres = 81; Yres = 13; Zres= 13;
%cx = 0; cy =-1; cz = 0;

Xres = 801; Yres = 121; Zres= 121;
cx = 0; cy =0; cz = 0;

Q = 10;
unqID = 0;

filename = strcat('testdata3_Speed3D_P_',int2str(Poly),'_Sca_' , scaling,'_X_',int2str(Xres+cx),'_Y_',int2str(Yres+cy),'_Z_',int2str(Zres+cz),'_Q_',int2str(Q),'_unqID',int2str(unqID));
filepath = strcat('dataSpeed/',filename);

xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
ufname =  strcat(filepath,'_pU.txt' );
vfname =  strcat(filepath,'_pV.txt' );
wfname =  strcat(filepath,'_pW.txt' );
pfname =  strcat(filepath,'_pP.txt' );

xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
uval = textread(ufname, '%f\t');
vval = textread(vfname, '%f\t');
wval = textread(wfname, '%f\t');
pval = textread(pfname, '%f\t');

xVpos = reshape(xpos,Xres,Yres,Zres);
xVpos = permute(xVpos,[3,1,2]);
yVpos = reshape(ypos,Xres,Yres,Zres);
yVpos = permute(yVpos,[3,1,2]);
zVpos = reshape(zpos,Xres,Yres,Zres);
zVpos = permute(zVpos,[3,1,2]);
uVval = reshape(uval,Xres,Yres,Zres);
uVval = permute(uVval,[3,1,2]);
vVval = reshape(vval,Xres,Yres,Zres);
vVval = permute(vVval,[3,1,2]);
wVval = reshape(wval,Xres,Yres,Zres);
wVval = permute(wVval,[3,1,2]);
pVval = reshape(pval,Xres,Yres,Zres);
pVval = permute(pVval,[3,1,2]);


figure(4),hold off
fv = patch(isosurface(xVpos,yVpos,zVpos,pVval,-0.50)); hold on;
isonormals(xVpos,yVpos,zVpos,pVval,fv)
fv.FaceColor = 'red';
fv.EdgeColor = 'none';
daspect([1,1,1])
view([va,vb])
camlight 
lighting gouraud
axis equal;
%hold off;
%axis off;
axis([0.4,2.4,0.0,0.45,0.65,0.95]); view([0,90])




resP = 1/400;
[ux,uy,uz] = gradient(uVval,resP,resP,resP);
[vx,vy,vz] = gradient(vVval,resP,resP,resP);
[wx,wy,wz] = gradient(wVval,resP,resP,resP);

Vorticity_s = ((uy- vy).^(2) + (uz- wx).^(2) + (wy- vz).^(2) ).^(1/2);

%VorticityS = smooth(Vorticity_s);

figure(5),hold off
fv = patch(isosurface(xVpos,yVpos,zVpos,Vorticity_s,20)); hold on;
isonormals(xVpos,yVpos,zVpos,Vorticity_s,fv)
fv.FaceColor = 'red';
fv.EdgeColor = 'none';
daspect([1,1,1])
view([va,vb])
camlight 
lighting gouraud
axis equal;
hold off;
axis off;
axis([0.4,2.4,0.0,0.5,0.6,1.0])
%print('images/Vorticity_IsoSur_DG_ISo_20_P_2','-dpng', '-r500');
%axis([2.2,2.35,0.2,0.35,0.75,0.9])
% line([2.25;2.25], [0.2;0.2], [0.75,0.9], 'color','k','LineWidth',1.4)
% line([2.25;2.25], [0.35;0.35], [0.75,0.9], 'color','k','LineWidth',1.4)
% line([2.25;2.25], [0.2;0.35], [0.75,0.75], 'color','k','LineWidth',1.4)
% line([2.25;2.25], [0.2;0.35], [0.9,0.9], 'color','k','LineWidth',1.4)
%hcs = contourslice(xVpos, yVpos, zVpos, Vorticity_p, 2.25,[], [], [valueIso valueIso]) ;
%set(hcs,'EdgeColor',[1 1 0]);
%set(hcs,'linewidth',2);
%set(hcs,'linestyle','--');


%%
VorticityS = smooth3(Vorticity_s,'box',5);

figure(6),hold off
fv = patch(isosurface(xVpos,yVpos,zVpos,VorticityS,20)); hold on;
isonormals(xVpos,yVpos,zVpos,VorticityS,fv)
fv.FaceColor = 'red';
fv.EdgeColor = 'none';
daspect([1,1,1])
view([va,vb])
camlight 
lighting gouraud
axis equal;
hold off;
axis off;
axis([0.4,2.4,0.0,0.5,0.6,1.0])


%[SX,SY,SZ] = meshgrid(linspace(2.1,6.1,Xres), linspace(0.0,0.5,Yres),linspace(0.6,1.0,Zres));

















% %[SX,SY,SZ] = meshgrid(linespace(2.5,6.0,100),)
% s1 = 1601;
% s2 = 121;
% s3 = 121;
% 
% %[SX,SY,SZ] = meshgrid(linspace(2.1,6.1,s1), linspace(0.0,0.5,s2),linspace(0.6,1.0,s3))
% 
% linspace(2.1,6.1,s1);
% linspace(0.2,0.5,s2);
% linspace(0.7,1.0,s3)
