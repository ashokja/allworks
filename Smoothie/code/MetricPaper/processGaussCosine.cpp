#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include <boost/math/special_functions/sinc.hpp>
#include <boost/timer.hpp>
#include <chrono>
#include "MetricTensor.h"
#include <math.h>

#define PI 3.14159265

typedef std::chrono::high_resolution_clock Clock;

using namespace SIACUtilities;
using namespace std;

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2st arg conditions.xml file." << endl;
		cout << "3nd arg polynomial degree filter you want to apply" << endl;
		cout << "4th arg Resolution of output." << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();
	HNM2D->LoadData(fname+".fld",var);

	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();
	MetricTensor* metricT = new MetricTensor();
	metricT->LoadMetricTensor(HNM2D);
	metricT->CalculateMetricTensorAtNodes();
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;

	Array<OneD,NekDouble> fce = HNM2D->m_expansions[0]->GetPhys();
	
    // Define forcing function for first variable defined in file
	LibUtilities::EquationSharedPtr ffunc
					= vSession->GetFunction("ExactSolution", 0); 

	NekDouble minx, miny,minz,maxx,maxy,maxz;
	minx = 0.0; miny= 0.0; minz=0.0;
	maxx= 1.0; maxy=1.0; maxz=1.0;

    //visualize a line
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp2, HNM2D, atoi(argv[1+2]), scaling ); 
	SmoothieSIAC2D sm_NONSYM(SIACUtilities::eSYM_UNEVEN_2kp1, HNM2D, atoi(argv[1+2]), scaling );
    
	int gPtsLy = atoi(argv[1+3]);
	int totPtsLy = gPtsLy;
    NekDouble sy = 1.0/(totPtsLy-1.0);
    Array<OneD,NekDouble> pYLy(totPtsLy),pXLy(totPtsLy),pVLy(totPtsLy),glCoords(3,0.0);
    Array<OneD,NekDouble> pPLy(totPtsLy), pSLy(totPtsLy),pDynLy(totPtsLy);
	Array<OneD,NekDouble> pLPx(totPtsLy), pELy(totPtsLy);
    Array<OneD,NekDouble> pSMet1Ly(totPtsLy), pSMet2Ly(totPtsLy);
    Array<OneD,NekDouble> pSMet3Ly(totPtsLy), pSMet4Ly(totPtsLy);
    NekDouble lambda,lambda2,valY,valZ;
    NekDouble lambda3,lambda4;
    Array<OneD,NekDouble> eig(3,0.0),eig2(3,0.0);
    Array<OneD,NekDouble> eig3(3,0.0),eig4(3,0.0);

	for (int i =0 ; i< gPtsLy; i++)
	{
		int index = i;
		pYLy[index] = 0.5;
		pXLy[index] = i*sy; 
		pVLy[index] = ffunc->Evaluate(pXLy[index],pYLy[index],0.0,0.0);
		glCoords[0] = pXLy[index]; glCoords[1] = pYLy[index];

		//L-SIAC dynamic Scaling
		NekDouble dynScaling = HNM2D->GetDynamicScaling(glCoords);
		pDynLy[index] = dynScaling;

	    //L-SIAC maximum Edge length Scaling

		//dG Error 
		int elid = HNM2D->GetExpansionIndexUsingRTree(glCoords);
		LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
		int physOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
		const Array<OneD,NekDouble> el_phys = fce.CreateWithOffset(
										fce,physOffset);
		pPLy[index] = lexp->PhysEvaluate(glCoords,el_phys);
		pELy[index] = elid;

		// L-SIAC Metric Error
		metricT->GetEigenPair(glCoords,elid,1,lambda,eig);
		sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMet1Ly[index], valY, valZ, eig, lambda,0);
		metricT->GetEigenPair(glCoords,elid,2,lambda2,eig2);
		sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMet2Ly[index], valY, valZ, eig2, lambda2,0);

        //L-SIAC Interpolated Metric Error
	    metricT->GetEigenPairUsingIP(glCoords, elid,1,lambda3,eig3);
		sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMet3Ly[index], valY, valZ, eig3, lambda3,0);
	    metricT->GetEigenPairUsingIP(glCoords, elid,2,lambda4,eig4);
		sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMet4Ly[index], valY, valZ, eig4, lambda4,0);

	}

	cout << "Done writing Line" << endl;
	NektarBaseClass k;
	k.writeNekArrayBin(pXLy,fname+"_"+argv[1+2]+"_METFLD_R_"+argv[1+3]+"_pXLy.bin");
	k.writeNekArrayBin(pYLy,fname+"_"+argv[1+2]+"_METFLD_R_"+argv[1+3]+"_pYLy.bin");
	k.writeNekArrayBin(pELy,fname+"_"+argv[1+2]+"_METFLD_R_"+argv[1+3]+"_pELy.bin");
	k.writeNekArrayBin(pVLy,fname+"_"+argv[1+2]+"_METFLD_R_"+argv[1+3]+"_pVLy.bin");
	k.writeNekArrayBin(pPLy,fname+"_"+argv[1+2]+"_METFLD_R_"+argv[1+3]+"_pPLy.bin");
	k.writeNekArrayBin(pSLy,fname+"_"+argv[1+2]+"_METFLD_R_"+argv[1+3]+"_pSLy.bin");
	k.writeNekArrayBin(pSMet1Ly,fname+"_"+argv[1+2]+"_METFLD_R_"+argv[1+3]+"_pSMet1Ly.bin");
	k.writeNekArrayBin(pSMet2Ly,fname+"_"+argv[1+2]+"_METFLD_R_"+argv[1+3]+"_pSMet2Ly.bin");
	k.writeNekArrayBin(pSMet3Ly,fname+"_"+argv[1+2]+"_METFLD_R_"+argv[1+3]+"_pSMet3Ly.bin");
	k.writeNekArrayBin(pSMet4Ly,fname+"_"+argv[1+2]+"_METFLD_R_"+argv[1+3]+"_pSMet4Ly.bin");

	return 0;
}

