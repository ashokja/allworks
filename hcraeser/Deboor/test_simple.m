% testing/undersanding Deboor algorithm.
% http://www.cs.mtu.edu/~shene/COURSES/cs3621/NOTES/spline/de-Boor.html 
%   (implemeted this algorithm)

close;
clear all;
TOLERENCE = 1e-10
p = 2; % degree

u = 12.1;

u_knots = [9,10,11,12,13];

for uk = 1:size(u_knots(:))
    if ( u_knots(uk) > u )
        k = uk-1;
        break;
    end
end

% find multiplicity.
count = 0;
if ( abs(u_knots(k) - u) < TOLERENCE)
    for kc = k-1:-1:1
        if ( abs(u_knots(kc) - u) < TOLERENCE )
            count = count +1;
        else
            break;
        end
    end
end
s = count;

% get all the numbers needed for calculations now.
h = p-s

Ef_cps(k-p:k-s,1) = u_knots( k-p:k-s);


for r = 1:h
    for i = k-p+r: k-s
        disp([r,i]);
        disp(i+p-r+1)
        a(i,r) = ( u - u_knots(i) )/(u_knots(i+p-r+1) - u_knots(i) );
        Ef_cps(i,r+1) = (1-a(i,r))* Ef_cps(i-1,r) + a(i,r)* Ef_cps(i,r);
    end
end





