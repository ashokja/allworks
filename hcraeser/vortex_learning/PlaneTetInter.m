function retP =  PlaneTetInter(lhs_w,rhs_w,tet_coords)

switch nargin
    case 0
        rhs_w = [0.5;-0.5;0];
        lhs_w = [0,-1,0; 1,0,0; 0,0,0];
        tet_coords = [0,0,0;1,0,0;0,1,0;0,0,1];
end

count =1;
index = zeros(2,1);
for i = 1:3
    if ~isempty(lhs_w(1,:) ==0);
        index(count) = i;
        count = count+1;
        if count ==3
            break;
        end
    end
end

A = zeros(3,3);
B = zeros(3,1);
A(1,:) = lhs_w(index(1),:);
A(2,:) = lhs_w(index(2),:);

B(1,:) = -rhs_w(index(1));
B(2,:) = -rhs_w(index(2));
count =1;
retP =[];
% face 1 on the tet.
[lhs, rhs] = EquationOfPlane(tet_coords(1:3,:)); %debug
A(3,:) = lhs;
B(3,:) = rhs;
p = A\B;
if (isempty(find(isnan(p), 1)) && isempty(find(isinf(p), 1)))
    if ( pointTriangleDistance(tet_coords(1:3,:),p) < 10^(-5))
       retP(count,:) = p;
       count = count+1;
    end
end

%face 2 on the tet.
[lhs, rhs] = EquationOfPlane(tet_coords(2:4,:)); %debug
A(3,:) = lhs;
B(3,:) = rhs;
p = A\B;
if (isempty(find(isnan(p), 1)) && isempty(find(isinf(p), 1)))
    if ( pointTriangleDistance(tet_coords(2:4,:),p) < 10^(-5))
       retP(count,:) = p;
       count = count+1;
    end
end

%face 3 on the tet.
[lhs, rhs] = EquationOfPlane(tet_coords([1,2,4],:)); %debug
A(3,:) = lhs;
B(3,:) = rhs;
p = A\B;
if (isempty(find(isnan(p), 1)) && isempty(find(isinf(p), 1)))
    if ( pointTriangleDistance(tet_coords([1,2,4],:),p) < 10^(-5))
       retP(count,:) = p;
       count = count+1;
    end
end

%face 4 on the tet.
[lhs, rhs] = EquationOfPlane(tet_coords([1,3,4],:)); %debug
A(3,:) = lhs;
B(3,:) = rhs;
p = A\B;
if (isempty(find(isnan(p), 1)) && isempty(find(isinf(p), 1)))
    if ( pointTriangleDistance(tet_coords([1,3,4],:),p) < 10^(-5))
       retP(count,:) = p;
       count = count+1;
    end
end

if count >3 
   % more than one point of intersection.
   % if all points are same ignore.
   
   % Find the max distance between them and take those extremes.
   retP = retP(1:2,:);
elseif count ==2
    retP(2,:) = retP(1,:);
end

end





