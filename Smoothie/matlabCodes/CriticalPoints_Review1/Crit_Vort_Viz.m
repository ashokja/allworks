% This file is the first attempt to vizualize vorticity on a mesh.
% 
close all;
clear;

rate = 2;
DgGrid = 41;

lattices = [101];
%lattices = [11,21,31,51,61,71,81,91,101];
%lattices = [11,21,41,51,81,101,121,131];
%lattices= 61;
%lattices = [11,21,41,61,81,121,141,161,201]; %p3,41

for loop = 1:max(size(lattices))
    latticeGrid = lattices(loop);
    filename = strcat('P',int2str(rate),'_quad_simple_',int2str(DgGrid),'_',int2str(latticeGrid),'_SampleDeriv');

    xfname = strcat('../../data/NekMeshes/',filename,'_x.txt' );
    yfname = strcat('../../data/NekMeshes/',filename,'_y.txt' );

    rufname = strcat('data/SampleRate_',int2str(latticeGrid),'_u.txt');
    ruxfname = strcat('data/SampleRate_',int2str(latticeGrid),'_ux.txt');
    ruyfname = strcat('data/SampleRate_',int2str(latticeGrid),'_uy.txt');

    rvfname = strcat('data/SampleRate_',int2str(latticeGrid),'_v.txt');
    rvxfname = strcat('data/SampleRate_',int2str(latticeGrid),'_vx.txt');
    rvyfname = strcat('data/SampleRate_',int2str(latticeGrid),'_vy.txt');


    
    ufname = strcat('../../data/NekMeshes/',filename,'_u.txt' );
    uxfname = strcat('../../data/NekMeshes/',filename,'_ux.txt' );
    uyfname = strcat('../../data/NekMeshes/',filename,'_uy.txt' );
    vfname = strcat('../../data/NekMeshes/',filename,'_v.txt' );
    vxfname = strcat('../../data/NekMeshes/',filename,'_vx.txt' );
    vyfname = strcat('../../data/NekMeshes/',filename,'_vy.txt' );
    
    sufname = strcat('../../data/NekMeshes/',filename,'_su.txt' );
    suxfname = strcat('../../data/NekMeshes/',filename,'_sux.txt' );
    suyfname = strcat('../../data/NekMeshes/',filename,'_suy.txt' );
    svfname = strcat('../../data/NekMeshes/',filename,'_sv.txt' );
    svxfname = strcat('../../data/NekMeshes/',filename,'_svx.txt' );
    svyfname = strcat('../../data/NekMeshes/',filename,'_svy.txt' );

    
    
    xv = textread(xfname, '%f\t');
    yv = textread(yfname, '%f\t');

    uv = textread(ufname, '%f\t');
    uxv = textread(uxfname, '%f\t');
    uyv = textread(uyfname, '%f\t');
    
    vv = textread(vfname, '%f\t');
    vxv = textread(vxfname, '%f\t');
    vyv = textread(vyfname, '%f\t');
    
    suv = textread(sufname, '%f\t');
    suxv = textread(suxfname, '%f\t');
    suyv = textread(suyfname, '%f\t');

    svv = textread(svfname, '%f\t');
    svxv = textread(svxfname, '%f\t');
    svyv = textread(svyfname, '%f\t');
    
    ruv = textread(rufname, '%f\t');
    ruxv = textread(ruxfname, '%f\t');
    ruyv = textread(ruyfname, '%f\t');

    rvv = textread(rvfname, '%f\t');
    rvxv = textread(rvxfname, '%f\t');
    rvyv = textread(rvyfname, '%f\t');



    
    xV = reshape(xv, [latticeGrid,latticeGrid]);
    yV = reshape(yv, [latticeGrid,latticeGrid]);

    % testcase
    %uV = xV-0.5;
    %vV = yV-0.5;
    uV = reshape(uv, [latticeGrid,latticeGrid]);
    uxV = reshape(uxv, [latticeGrid,latticeGrid]);
    uyV = reshape(uyv, [latticeGrid,latticeGrid]);
    vV = reshape(vv, [latticeGrid,latticeGrid]);
    vxV = reshape(vxv, [latticeGrid,latticeGrid]);
    vyV = reshape(vyv, [latticeGrid,latticeGrid]);
    
    suV = reshape(suv, [latticeGrid,latticeGrid]);
    suxV = reshape(suxv, [latticeGrid,latticeGrid]);
    suyV = reshape(suyv, [latticeGrid,latticeGrid]);
    svV = reshape(svv, [latticeGrid,latticeGrid]);
    svxV = reshape(svxv, [latticeGrid,latticeGrid]);
    svyV = reshape(svyv, [latticeGrid,latticeGrid]);
    
    ruV = reshape(ruv, [latticeGrid,latticeGrid]);
    rvV = reshape(rvv, [latticeGrid,latticeGrid]);
    ruxV = reshape(ruxv, [latticeGrid,latticeGrid]);
    rvxV = reshape(rvxv, [latticeGrid,latticeGrid]);
    ruyV = reshape(ruyv, [latticeGrid,latticeGrid]);
    rvyV = reshape(rvyv, [latticeGrid,latticeGrid]);
    
    n_uV = sqrt(suV.*suV+svV.*svV);
    u = suV./n_uV;
    v = svV./n_uV;
 
 figure(1),
 quiver(xV,yV,u,v);

 
% %     figure(1)
% %       subplot(2,1,1)
% %     surf(xV,yV,uV); hold on;
% %            subplot(2,1,2)
% %     surf(xV,yV,vV);

%     figure(2)
%     surf(xV,yV, vxV-uyV);
    
%     figure(3)
%     surf(xV,yV, log(abs(vxV-svxV)));
%     
%     figure(4)
%     surf(xV,yV, log(abs(uyV-suyV)));
% 
    figure(5)
    surf(xV,yV, log(abs(rvV-svV)));
    colormap winter;
%    figure(6)
    hold on;
    surf(xV,yV, log(abs(vV-rvV)));
    colormap summer;
    
    figure(8)
    contour(log(abs(rvV-svV)),'fill','on');
    title('SIAC'); contourcbar;
    figure(9), hold on;
    contour(log(abs(rvV-vV)),'fill','on')
    title('DG'); contourcbar;
    
    disp('SIAC ER');
    disp(sum(sum(abs(rvV-svV)))/(latticeGrid*latticeGrid));
    disp('DG ER');
    disp(sum(sum(abs(rvV-vV)))/(latticeGrid*latticeGrid));

    disp('SIAC VX ER');
    disp(sum(sum(abs(rvxV-svxV)))/(latticeGrid*latticeGrid));
    disp('DG VX ER');
    disp(sum(sum(abs(rvxV-vxV)))/(latticeGrid*latticeGrid));

    ares =30; bres = 70;%latticeGrid-30;
    disp('SIAC ER');
    disp(sum(sum(abs(rvV(ares:bres,ares:bres)-svV(ares:bres,ares:bres)))));
    disp('DG ER');
    disp(sum(sum(abs(rvV(ares:bres,ares:bres)-vV(ares:bres,ares:bres)))));
    
    disp('SIAC UX ER');
    disp(sum(sum(abs(ruxV(ares:bres,ares:bres)-suxV(ares:bres,ares:bres)))));
    disp('DG UX ER');
    disp(sum(sum(abs(ruxV(ares:bres,ares:bres)-uxV(ares:bres,ares:bres)))));
    
    disp('SIAC VX ER');
    disp(sum(sum(abs(rvxV(ares:bres,ares:bres)-svxV(ares:bres,ares:bres)))));
    disp('DG VX ER');
    disp(sum(sum(abs(rvxV(ares:bres,ares:bres)-vxV(ares:bres,ares:bres)))));
    
    disp('SIAC UY ER');
    disp(sum(sum(abs(ruyV(ares:bres,ares:bres)-suyV(ares:bres,ares:bres)))));
    disp('DG UY ER');
    disp(sum(sum(abs(ruyV(ares:bres,ares:bres)-uyV(ares:bres,ares:bres)))));
    
    disp('SIAC VY ER');
    disp(sum(sum(abs(rvyV(ares:bres,ares:bres)-svyV(ares:bres,ares:bres)))));
    disp('DG VY ER');
    disp(sum(sum(abs(rvyV(ares:bres,ares:bres)-vyV(ares:bres,ares:bres)))));
    %%
    figure (10),
    contour(log(abs(ruxV-uxV)),'fill','on');
    title('DG ux'); contourcbar;
    %%%%%%%%%%%
    n_udV = sqrt(uxV.*uxV+uyV.*uyV);
    nux = uxV./n_udV;
    nuy = uyV./n_udV;
    figure(11),
    quiver(xV,yV,nux,nuy);
    title('DG ud');

    %%%%%%%%%%%
    n_rudV = sqrt(ruxV.*ruxV+ruyV.*ruyV);
    nrux = ruxV./n_rudV;
    nruy = ruyV./n_rudV;
    figure(12),
    quiver(xV,yV,nrux,nruy);
    title('Raw ud');

    %%%%%%%%%%
    n_sudV = sqrt(suxV.*suxV+suyV.*suyV);
    nsux = suxV./n_sudV;
    nsuy = suyV./n_sudV;
    figure(13),
    quiver(xV,yV,nsux,nsuy);
    title('SIAC ud');
    
    
    %%%%%%%%%%% v
    n_vdV = sqrt(vxV.*vxV+vyV.*vyV);
    nvx = vxV./n_vdV;
    nvy = vyV./n_vdV;
    figure(11),
    quiver(xV,yV,nvx,nvy);
    title('DG vd');

    %%%%%%%%%%%
    n_rvdV = sqrt(rvxV.*rvxV+rvyV.*rvyV);
    nrvx = rvxV./n_rvdV;
    nrvy = rvyV./n_rvdV;
    figure(12),
    quiver(xV,yV,nrvx,nrvy);
    title('Raw vd');

    %%%%%%%%%%
    n_svdV = sqrt(svxV.*svxV+svyV.*svyV);
    nsvx = svxV./n_svdV;
    nsvy = svyV./n_svdV;
    figure(13),
    quiver(xV,yV,nsvx,nsvy);
    title('SIAC vd');
    
    
    disp('SIAC UY ER');
    disp(sum(sum(abs(nruy(ares:bres,ares:bres)-nsuy(ares:bres,ares:bres)))));
    disp('DG UY ER');
    disp(sum(sum(abs(nruy(ares:bres,ares:bres)-nuy(ares:bres,ares:bres)))));
    
    disp('SIAC VX ER');
    disp(sum(sum(abs(nruy(ares:bres,ares:bres)-nsuy(ares:bres,ares:bres)))));
    disp('DG VX ER');
    disp(sum(sum(abs(nruy(ares:bres,ares:bres)-nuy(ares:bres,ares:bres)))));
    
    vort_Raw = ruyV - rvxV;
    vort_DG = uyV - vxV;
    vort_SIAC = suyV - svxV;
    disp('DG Vort');
    disp(sum(sum(abs(vort_Raw(ares:bres,ares:bres)-vort_DG(ares:bres,ares:bres)))));
    disp('SIAC Vort');
    disp(sum(sum(abs(vort_Raw(ares:bres,ares:bres)-vort_SIAC(ares:bres,ares:bres)))));
    
    disp('DG Vort');
    disp(sum(sum(abs((vort_Raw(ares:bres,ares:bres)-vort_DG(ares:bres,ares:bres))./(vort_Raw(ares:bres,ares:bres)+0.1)))));
    disp('SIAC Vort');
    disp(sum(sum(abs((vort_Raw(ares:bres,ares:bres)-vort_SIAC(ares:bres,ares:bres))./(vort_Raw(ares:bres,ares:bres)+0.1)))));
    
    figure(21);
    contour(log(abs(vort_Raw-vort_DG)),linspace(-10,4,20),'fill','on');
    title('DG vort'); contourcbar;    
    figure(22);
    contour(log(abs(vort_Raw-vort_SIAC)),linspace(-10,4,20),'fill','on');
    title('SIAC vort'); contourcbar;    

    
    
    %%
%     n_uV = sqrt(ruV.*ruV+rvV.*rvV);
%     u = ruV./n_uV;
%     v = rvV./n_uV;
%      figure(7),
%     quiver(xV,yV,u,v);
%     


    
end
