clear;
close all;

%UsebkupData = 'cur_feb8/'
UsebkupData = 'bkup/'
%UsebkupData = ''
%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05'
filename = strcat('P0000009_So_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_AllVar' );
filepath = strcat('data/',UsebkupData,filename);

xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );

upfname = strcat(filepath,'_pU_p.txt' );
vpfname = strcat(filepath,'_pV_p.txt' );
wpfname = strcat(filepath,'_pW_P.txt' );
ppfname = strcat(filepath,'_pP_p.txt' );

usfname = strcat(filepath,'_pU_s.txt' );
vsfname = strcat(filepath,'_pV_s.txt' );
wsfname = strcat(filepath,'_pW_s.txt' );
psfname = strcat(filepath,'_pP_s.txt' );

uxsfname = strcat(filepath, '_pUx_s.txt');
uysfname = strcat(filepath, '_pUy_s.txt');
uzsfname = strcat(filepath, '_pUz_s.txt');

vxsfname = strcat(filepath, '_pVx_s.txt');
vysfname = strcat(filepath, '_pVy_s.txt');
vzsfname = strcat(filepath, '_pVz_s.txt');

wxsfname = strcat(filepath, '_pWx_s.txt');
wysfname = strcat(filepath, '_pWy_s.txt');
wzsfname = strcat(filepath, '_pWz_s.txt');




xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');

uppos = textread(upfname, '%f\t');
vppos = textread(vpfname, '%f\t');
wppos = textread(wpfname, '%f\t');
pppos = textread(ppfname, '%f\t');

uspos = textread(usfname, '%f\t');
vspos = textread(vsfname, '%f\t');
wspos = textread(wsfname, '%f\t');
pspos = textread(psfname, '%f\t');

uxspos = textread( uxsfname,'%f\t');
uyspos = textread( uysfname,'%f\t');
uzspos = textread( uzsfname,'%f\t');

vxspos = textread( vxsfname,'%f\t');
vyspos = textread( vysfname,'%f\t');
vzspos = textread( vzsfname,'%f\t');

wxspos = textread( wxsfname,'%f\t');
wyspos = textread( wysfname,'%f\t');
wzspos = textread( wzsfname,'%f\t');

epos = floor(epos +0.0001);

s = floor( (max(size(xpos)))^(1/2) +0.1);

    
%xVpos = reshape(xpos,s,s,s);
%yVpos = reshape(ypos,s,s,s);
%zVpos = reshape(zpos,s,s,s);
%eVpos = reshape(epos,s,s,s);

    xVpos = reshape(xpos,s,s);
%    xVpos = permute(xVpos,[2,3,1]);
    yVpos = reshape(ypos,s,s);
%    yVpos = permute(yVpos,[2,3,1]);
    zVpos = reshape(zpos,s,s);
%    zVpos = permute(zVpos,[2,3,1]);

upVpos = reshape(uppos,s,s);  usVpos = reshape(uspos,s,s);
vpVpos = reshape(vppos,s,s);  vsVpos = reshape(vspos,s,s);
wpVpos = reshape(wppos,s,s);  wsVpos = reshape(wspos,s,s);
ppVpos = reshape(pppos,s,s);  psVpos = reshape(pspos,s,s);

uxsVpos = reshape(uxspos,s,s);  vxsVpos = reshape(vxspos,s,s);  wxsVpos = reshape(wxspos,s,s);
uysVpos = reshape(uyspos,s,s);  vysVpos = reshape(vyspos,s,s);  wysVpos = reshape(wyspos,s,s);
uzsVpos = reshape(uzspos,s,s);  vzsVpos = reshape(vzspos,s,s);  wzsVpos = reshape(wzspos,s,s);


%upVpos = permute(upVpos,[2,3,1]);   usVpos = permute(usVpos,[2,3,1]);
%vpVpos = permute(vpVpos,[2,3,1]);   vsVpos = permute(vsVpos,[2,3,1]);
%wpVpos = permute(wpVpos,[2,3,1]);   wsVpos = permute(wsVpos,[2,3,1]);
%ppVpos = permute(ppVpos,[2,3,1]);   psVpos = permute(psVpos,[2,3,1]);

figure(1);
surf(yVpos,zVpos,upVpos)
shading interp;

figure(2);
surf(yVpos,zVpos,usVpos)
shading interp;

%%
figure(3);
contour(yVpos,zVpos,ppVpos);

figure(4);
contour(yVpos,zVpos,psVpos);


% figure(5)
% contour(yVpos,zVpos,uxsVpos);
% figure(6)
% contour(yVpos,zVpos,uysVpos);
% figure(7)
% contour(yVpos,zVpos,uzsVpos);
% 
% figure(8)
% contour(yVpos,zVpos,vxsVpos);
% figure(9)
% contour(yVpos,zVpos,vysVpos);
% figure(10)
% contour(yVpos,zVpos,vzsVpos);
% 
% figure(11)
% contour(yVpos,zVpos,wxsVpos);
% figure(12)
% contour(yVpos,zVpos,wysVpos);
% figure(13)
% contour(yVpos,zVpos,wzsVpos);
%%
figure(7);
subplot(3,3,1),contour(yVpos,zVpos,uxsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
subplot(3,3,2),contour(yVpos,zVpos,uysVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
subplot(3,3,3),contour(yVpos,zVpos,uzsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);

subplot(3,3,4),contour(yVpos,zVpos,vxsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
subplot(3,3,5),contour(yVpos,zVpos,vysVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
subplot(3,3,6),contour(yVpos,zVpos,vzsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);

subplot(3,3,7),contour(yVpos,zVpos,wxsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
subplot(3,3,8),contour(yVpos,zVpos,wysVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
subplot(3,3,9),contour(yVpos,zVpos,wzsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
%print('images/F3b13/LowResUVW_Der_countour_1dir','-dpng', '-r500')

figure(8);
subplot(3,3,1),surf(yVpos,zVpos,uxsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90);
subplot(3,3,2),surf(yVpos,zVpos,uysVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90);
subplot(3,3,3),surf(yVpos,zVpos,uzsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90);

subplot(3,3,4),surf(yVpos,zVpos,vxsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90);
subplot(3,3,5),surf(yVpos,zVpos,vysVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90);
subplot(3,3,6),surf(yVpos,zVpos,vzsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90);

subplot(3,3,7),surf(yVpos,zVpos,wxsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90);
subplot(3,3,8),surf(yVpos,zVpos,wysVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90);
subplot(3,3,9),surf(yVpos,zVpos,wzsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90);
%print('images/F3b13/LowResUVW_Der_surf_1dir','-dpng', '-r500')

%%
figure(14)
contour(yVpos,zVpos,(uzsVpos - wxsVpos) );


figure(15)
contour(yVpos,zVpos,(vzsVpos - wysVpos) );

%%
figure(16)
contour(yVpos,zVpos,((vzsVpos - wysVpos).^2 + (uzsVpos - wxsVpos).^2+ (uysVpos - vxsVpos).^2).^(1/2) ,100);

%%
figure(17)
surf(yVpos,zVpos,((vzsVpos - wysVpos).^2 + (uzsVpos - wxsVpos).^2+ (uysVpos - vxsVpos).^2).^(1/2))
shading interp;
%%
figure(19)
surf(yVpos,zVpos,-1.0*ppVpos)
shading interp;
%%

% caculate curvature from  above values.

% calculate acceleration.
 auspos = uxspos.*uspos + uyspos.*vspos + uzspos.*wspos;
 avspos = vxspos.*uspos + vyspos.*vspos + vzspos.*wspos;
 awspos = wxspos.*uspos + wyspos.*vspos + wzspos.*wspos;
 %%
% veclocity cross acceleration. 
cuspos = vspos.*awspos - wspos.*avspos;
cvspos = wspos.*auspos - uspos.*awspos;
cwspos = uspos.*avspos - vspos.*auspos;

curvature_pos = (cuspos.^(2) + cvspos.^(2) + cwspos.^(2)).^(1/2);
curvature_Vpos = reshape(curvature_pos,s,s);
%%
figure(20)
surf(yVpos,zVpos,curvature_Vpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90);
shading interp;


%%
figure(21)
contourf(yVpos,zVpos,curvature_Vpos ,20);
%title('Curvature Calculations for LSAIC Scaling 0.01')
xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ])
%print('images/F3b13/LowResCurvature_Der_contourf_1dir','-dpng', '-r500')
%%
figure(22)
contour3(yVpos,zVpos,curvature_Vpos,20,'LineColor','k'); hold on;
surf(yVpos,zVpos,curvature_Vpos), shading interp;
xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ])
axis off;
grid off;
%colorbar();
view(0,90);
%print('images/Feb22/Curvature_1dirLSIAC_contour','-dpng', '-r500')
%view(100,-14);
%print('images/Feb22/Curvature_1dirLSIAC_contour_AngleView','-dpng', '-r500')
%%

figure(9)
subplot(4,3,1), contour(yVpos,zVpos,usVpos); 
title('u(1dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,2), contour(yVpos,zVpos,vsVpos); 
title('v(1dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,3), contour(yVpos,zVpos,wsVpos); 
title('w(1dir-LSIAC)'); 
grid off, axis off;

subplot(4,3,4), contour(yVpos,zVpos,uxsVpos); 
title('u_x(1dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,5), contour(yVpos,zVpos,vxsVpos); 
title('v_x(1dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,6), contour(yVpos,zVpos,wxsVpos); 
title('w_x(1dir-LSIAC)'); 
grid off, axis off;

subplot(4,3,7), contour(yVpos,zVpos,uysVpos); 
title('u_y(1dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,8), contour(yVpos,zVpos,vysVpos); 
title('v_y(1dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,9), contour(yVpos,zVpos,wysVpos); 
title('w_y(1dir-LSIAC)'); 
grid off, axis off;

subplot(4,3,10), contour(yVpos,zVpos,uzsVpos); 
title('u_z(1dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,11), contour(yVpos,zVpos,vzsVpos); 
title('v_z(1dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,12), contour(yVpos,zVpos,wzsVpos); 
title('w_z(1dir-LSIAC)'); 
grid off, axis off;
%print('images/Feb22/Contour_1dirLSIAC_Allvar','-dpng', '-r500');
%%

figure(10)
subplot(4,3,1), surf(yVpos,zVpos,usVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); 
title('u(1dir-LSIAC)');  
grid off, axis off;
subplot(4,3,2), surf(yVpos,zVpos,vsVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); 
title('v(1dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,3), surf(yVpos,zVpos,wsVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
title('w(1dir-LSIAC)'); 
grid off, axis off;

subplot(4,3,4), surf(yVpos,zVpos,uxsVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); 
title('u_x(1dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,5), surf(yVpos,zVpos,vxsVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
title('v_x(1dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,6), surf(yVpos,zVpos,wxsVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
title('w_x(1dir-LSIAC)'); 
grid off, axis off;

subplot(4,3,7), surf(yVpos,zVpos,uysVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); 
title('u_y(1dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,8), surf(yVpos,zVpos,vysVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
title('v_y(1dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,9), surf(yVpos,zVpos,wysVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
title('w_y(1dir-LSIAC)'); 
grid off, axis off;

subplot(4,3,10), surf(yVpos,zVpos,uzsVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
title('u_z(1dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,11), surf(yVpos,zVpos,vzsVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
title('v_z(1dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,12), surf(yVpos,zVpos,wzsVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
title('w_z(1dir-LSIAC)'); 
grid off, axis off;
%print('images/Feb22/Surf_1dirLSIAC_Allvar','-dpng', '-r500');





