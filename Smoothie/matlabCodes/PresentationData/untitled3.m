clear;
close all;

figure(1)
x=linspace(0,1,10);
plot(x,sin(2*pi*x),'k*-','linewidth',2);
axis off;
y = sin(2*pi*x);
print('sinCurve','-dpng','-r500');

figure(2)
for i = 1:9
    tx = [x(i),x(i+1)];
    ty = ones(size(tx))*cos(2*pi*(x(i)+x(i+1))/2);
    
    plot(tx, ty,'k*-','linewidth',2); hold on;
end
axis off;
print('femDerivative','-dpng','-r500');