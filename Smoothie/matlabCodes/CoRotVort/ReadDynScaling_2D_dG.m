clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20];

%abc = '../../data/UnsTriMeshes/NM2_unsMeshvar1_0125_2_R_100_pX_2DDyn.txt';

R = 1000;
poly =2;
order = poly;
count =1;

prefixName = '../../data/CorotatingVort_3/';

width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
fsw = 20;
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize
Rp = R;

%for poly = [2,3,4]
%          FileName = strcat('NM',int2str(poly),'_NPUniTriMesh05_d10_');
%           FileName = strcat('NM',int2str(poly),'_NUTriMesh_05_');
%           FileName = strcat('NM',int2str(poly),'_UniTriMesh_05_');
%            FileName = strcat('bentvort_1');
            FileName = strcat('vort5_mesh');
            
            prefixFilename = strcat( prefixName,FileName,'_P_',int2str(poly),'_scaling_0.02_xVar_0p3_R_',int2str(R),'_DLISAC');
            prefixFilenameP = strcat( prefixName,FileName,'_P_',int2str(2),'_p.aling_0.02_xVar_0p3_R_',int2str(Rp),'_DLISAC');
            prefixFilenameDyn = strcat( prefixName,FileName,'_P_',int2str(poly),'_sDyncaling_0.06_xVar_0p3_R_',int2str(R),'_DLISAC');

            prefixFilename = strcat( prefixName,FileName,'_P_',int2str(poly),'_scaling_0.034_xVar_0p3_R_',int2str(R),'_DLISAC');
            %prefixFilenameP = strcat( prefixName,FileName,'_P_',int2str(poly),'_scaling_0.034_xVar_0p3_R_',int2str(R),'_DLISAC');
            prefixFilenameP = strcat( prefixName,FileName,'_P_',int2str(2),'_scaling_0.034_xVar_0p3_R_',int2str(R),'_DLISAC');
            
            
            xfname = strcat(prefixFilename,'_pX.txt' );
            yfname = strcat(prefixFilename,'_pY.txt' );
            zfname = strcat(prefixFilename,'_pZ.txt' );
            efname = strcat(prefixFilename,'_pE.txt' );
           
            ux_pfname = strcat(prefixFilenameP,'_pUx_p.txt');
            vx_pfname = strcat(prefixFilenameP,'_pVx_p.txt');
            wx_pfname = strcat(prefixFilenameP,'_pWx_p.txt');
 
            uy_pfname = strcat(prefixFilenameP,'_pUy_p.txt');
            vy_pfname = strcat(prefixFilenameP,'_pVy_p.txt');
            wy_pfname = strcat(prefixFilenameP,'_pWy_p.txt');
 
            uz_pfname = strcat(prefixFilenameP,'_pUz_p.txt');
            vz_pfname = strcat(prefixFilenameP,'_pVz_p.txt');
            wz_pfname = strcat(prefixFilenameP,'_pWz_p.txt');         

            xpos = textread(xfname, '%f\t');
            ypos = textread(yfname, '%f\t');
            zpos = textread(zfname, '%f\t');
             epos = textread(efname, '%f\t');
                     
            uxpval = textread(ux_pfname,'%f\t');
            vxpval = textread(vx_pfname,'%f\t');
            wxpval = textread(wx_pfname,'%f\t');
            
            uypval = textread(uy_pfname,'%f\t');
            vypval = textread(vy_pfname,'%f\t');
            wypval = textread(wy_pfname,'%f\t');
            
            uzpval = textread(uz_pfname,'%f\t');
            vzpval = textread(vz_pfname,'%f\t');
            wzpval = textread(wz_pfname,'%f\t');
            
 
            Vort = ((uypval- vxpval).^(2) + (uzpval- wxpval).^(2) + (wypval- vzpval).^(2) ).^(1/2);
            
            %VorVpos_p = ((wypVpos-uzpVpos).^2 + (uzpVpos - wxpVpos).^2 + (vxpVpos - uypVpos).^2).^(1/2);
            
            delel = epos;
            
%x = (1:1000);
%[Ypos,Zpos] = meshgrid(x);
%ypos = Ypos(:);
%zpos = Zpos(:);
delyl = ypos;
delzl = zpos;
VortV = reshape(Vort,[R,R]);
VortV= fliplr(rot90(VortV,-1));
Vort = VortV(:);

delvpl = Vort;
delel = epos;
valueIso = 14;


[evals, elids,relids] = unique(delel);
addpath('../LSIAC_tests');
addpath('../NacaExamples');
num_unique_ids = (max(size(evals)));

for etag = evals'
    
    tri = ReturnTriangles(delel,etag);
    disp(etag);
    %figure(11), hold on;
    %trisurf(tri,delyl,delzl,delvpl);
	%shading interp, hold on;
    if (max(size(tri)) <7)
        continue;
    end
        
    if (max(etag == [98059]))
        continue;
    end

    figure(12), hold on;
    tricontour([delyl,delzl],tri,delvpl,linspace(0,130,50)); hold on;
    %set(hcs,'linewidth',2);
    %set(hcs,'linestyle','--');
    
    [nouse hcs] = tricontour([delyl,delzl],tri,delvpl,[valueIso valueIso]); hold on;
%     %[nouse hcs] = tricontour([delyl,delzl],tri,delvpl,linspace(0,20,1)); hold on;
     set(hcs,'EdgeColor',[1 0 0]);
     set(hcs,'linewidth',2);
     set(hcs,'linestyle','--');
%     
%     disp(etag);
%     if (etag < 1)
%         continue;
%     end
%     %disp('next');
%     el_indices = find(delel ==etag);
%     if (max(size(el_indices)) < 7)
%         continue;
%     end
%     %disp(delel(el_indices));
%     
%     ytripos = delyl(el_indices);
%     ztripos = delzl(el_indices);
%     vtripos = delvpl(el_indices);
% %    tri = delaunay(ytripos,ztripos);
%     tri = ReturnTriangles(dele,etag)
%     
%     %figure(11), hold on;    
%     %trisurf(tri,ytripos,ztripos,vtripos);
%     %shading interp, hold on;
%     
%     figure(12), hold on;
%     tricontour( [ytripos,ztripos], tri, vtripos,linspace(0,100,40)); hold on;
%     
%     [nouse hcs] = tricontour([ytripos,ztripos], tri, vtripos,[valueIso valueIso]); hold on;
%     %hcs = contourslice(xVpos, yVpos, zVpos, Vorticity_p, 2.25,[], [], ) ;
%      set(hcs,'EdgeColor',[1 0 0]);
%      set(hcs,'linewidth',2);
%      set(hcs,'linestyle','--');
%     
% %    tricontour(tri,ytripos,ztripos,vtripos,linspace(0,100,40)); hold on;
%     
end
%%
figure(12), hold on;
axis off;
grid off;
%axis equal;
colorbar();
%%
            ax = gca;
            ax.FontSize = fsw;
print('images/Contours_DG_8','-dpng', '-r500')