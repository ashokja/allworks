clear;
close all;

% %UsebkupData = 'cur_feb8/'
% UsebkupData = 'bkup/'
% %UsebkupData = ''
% %alpha =1;
% SIACFilterPoly = 2;
% scaling = '0.05'
% filename = strcat('P0000009_So_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_AllVar' );
qpts = 4;
filepath = strcat('data/');

xfname =  strcat(filepath,'test_LineX.txt' );
vfname =  strcat(filepath,'test_Line_ValTest.txt' );

xsfname =  strcat(filepath,'test_params.txt' );
vs4fname =  strcat(filepath,'test_vals',int2str(qpts),'.txt' );
vs7fname =  strcat(filepath,'test_vals',int2str(7),'.txt' );
vs10fname =  strcat(filepath,'test_vals',int2str(10),'.txt' );
vSIACfname =  strcat(filepath,'test_paramsS.txt' );


t = linspace(0,1,100);
offset = 0.454545;
%offset = 0.555555555;
%offset = 0.0;
%val = cos(2*pi*(t+0.555555555));
val = cos(2*pi*(t+offset));


xpos = textread(xfname, '%f\t');
vpos = textread(vfname, '%f\t');

xspos = textread(xsfname, '%f\t');
vs4pos = textread(vs4fname, '%f\t');
vs7pos = textread(vs7fname, '%f\t');
vs10pos = textread(vs10fname, '%f\t');

vSIACpos = textread(vSIACfname, '%f\t');

figure(1)
plot(xpos,vpos,'r--'); hold on;
plot(xspos,vs10pos,'g--'); hold on;
plot(t,val,'r');

figure(2)
% plot(xpos,log10(abs(cos(2*pi*(xpos+offset))-vpos)),'-'); hold on;
% plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vs4pos)),'g-'); hold on;
% plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vs7pos)),'c-'); hold on;
% plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vs10pos)),'k-'); hold on;
% plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vSIACpos)),'r-'); hold on;
plot(xpos,log10(abs(xpos-vpos)),'-'); hold on;
plot(xspos,log10(abs(xspos-vs4pos)),'g-'); hold on;
plot(xspos,log10(abs(xspos-vs7pos)),'c-'); hold on;
plot(xspos,log10(abs(xspos-vs10pos)),'k-'); hold on;
plot(xspos,log10(abs(xspos-vSIACpos)),'r-'); hold on;
%xlim([0.3,0.7])
%xlim([0.27,0.73])
%xlim([0.25,0.75])
ylabel('log10(Error)'); xlabel('x(aty=0.555555555)');
legend('Proj Er',strcat('Speed LSIAC q=',int2str(qpts)),strcat('Speed LSIAC q=',int2str(7)),strcat('Speed LSIAC q=',int2str(10)),' Constent LSIAC')
title('Errors of LSIAC vs Speed LSIAC for different quadrature. P-4,R-80-LSIAC')
%print('images/P4_81_LSIAC','-dpng', '-r300');
%title('Errors of LSIAC vs Speed LSIAC for different quadrature. P-4,R-80-SIAC')
%print('images/P4_81_SIAC','-dpng', '-r300');
% 


% figure(4)
% plot(xpos,log10(abs(cos(2*pi*(xpos+offset))-vpos)),'b'); hold on;
% plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vs4pos)),'g'); hold on;
% plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vs7pos)),'c'); hold on;
% plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vs10pos)),'k'); hold on;
% plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vSIACpos)),'r'); hold on;
% %xlim([0.25,0.75])
% ylabel('log10(Error)'); ylabel('x(aty=0.555555555)');
% title('Errors of LISAC vs Speed LSIAC for different quadrature')
% legend('Proj Er',strcat('Speed LSIAC q=',int2str(qpts)),strcat('Speed LSIAC q=',int2str(7)),strcat('Speed LSIAC q=',int2str(10)),' Constent LSIAC')
% print('images/P2_41_LSIAC_v1','-dpng', '-r300');


figure(6)
plot(xpos,log10(abs(cos(2*pi*(xpos+offset))-vpos)),'b'); hold on;
plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vs4pos)),'g'); hold on;
plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vs7pos)),'c'); hold on;
plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vs10pos)),'k*'); hold on;
plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vSIACpos)),'r'); hold on;
%xlim([0.25,0.75])
ylabel('log10(Error)'); ylabel('x(aty=0.454545)');
title('Errors of LISAC vs Speed LSIAC for different quadrature');
legend('Proj Er',strcat('Speed LSIAC q=',int2str(4)),strcat('Speed LSIAC q=',int2str(7)),strcat('Speed LSIAC q=4,10'),' Consistent LSIAC');
print('images/P2_41_LSIAC_v3','-dpng', '-r300');

figure(3)
plot(xpos,log10(abs(cos(2*pi*(xpos+offset))-vpos)),'*-'); hold on;
plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vs4pos)),'go-'); hold on;
plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vs7pos)),'co-'); hold on;
plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vs10pos)),'ko-'); hold on;
plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vSIACpos)),'ro--'); hold on;
%xlim([0.25,0.75])
ylabel('log10(Error)'); ylabel('x(aty=0.555555555)');
title('Errors of LISAC vs Speed LSIAC for different quadrature. P-4,R-20')
legend('Proj Er',strcat('Speed LSIAC q=',int2str(qpts)),strcat('Speed LSIAC q=',int2str(7)),strcat('Speed LSIAC q=',int2str(10)),' Constent LSIAC')
%print('images/P4_21_LSIAC','-dpng', '-r300');

figure(5)
plot(xpos,log10(abs(cos(2*pi*(xpos+offset))-vpos)),'*-'); hold on;
plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vs4pos)),'go-'); hold on;
plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vs7pos)),'co-'); hold on;
plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vs10pos)),'ko-'); hold on;
plot(xspos,log10(abs(cos(2*pi*(xspos+offset))-vSIACpos)),'ro--'); hold on;
%xlim([0.25,0.75])
ylabel('log10(Error)'); ylabel('x(aty=0.555555555)');
title('Errors of LISAC vs Speed LSIAC for different quadrature. P-4,R-20')
legend('Proj Er',strcat('Speed LSIAC q=',int2str(qpts)),strcat('Speed LSIAC q=',int2str(7)),strcat('Speed LSIAC q=',int2str(10)),' Constent LSIAC')
%print('images/P4_21_LSIAC','-dpng', '-r300');

