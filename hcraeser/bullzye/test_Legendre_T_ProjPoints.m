% first say at what points you want the function to be defined.
clear;
close all;
load('testData/jacX');
load('testData/jacWX');

N = [4:7];
K = [2:5];
%for NN = N
va = -8
vb = 14

%NN = 7
n = 2;

epislon = 0.1;
%for ei = 1:e_size
%    epislon = e(ei);
    NN = 9;
    
tic    
%    clearvars -except NN N
    NX =NN; NY=NN; NZ=NN;
    nx=n;ny=n;nz=n;
    px =6*(NN)+2; py =6*(NN)+2; pz =6*(NN)+2;
    %QX = 8; QY = 9; QZ = 10;
    addpath ../createTestData
    % call test_run to get the function defined at that location.

            XQ = XQN{NX+2+nx}; WXQ = WXQN{NX+2+nx};
            YQ = XQN{NY+2+ny}; WYQ = WXQN{NY+2+ny};
            ZQ = XQN{NZ+2+nz}; WZQ = WXQN{NZ+2+nz};
    %%
%    NX = 28; NY = 28; NZ = 28;
%    nx = 2; ny = 2; nz = 2;
    [X,Y,Z] = meshgrid(XQ,YQ,ZQ);

    X = permute(X,[2,1,3]); Y = permute(Y,[2,1,3]); Z = permute(Z,[2,1,3]);
    mapxy(:,1) = X(:); mapxy(:,2) = Y(:); mapxy(:,3) = Z(:);
    [xy,distance,t_a,mapxyV,curveCoeff] = test_runXYZExt_R(1,mapxy);

    UAtGrid = reshape(mapxyV(:,1),[max(size(XQ)),max(size(YQ)),max(size(ZQ))]);
    VAtGrid = reshape(mapxyV(:,2),[max(size(XQ)),max(size(YQ)),max(size(ZQ))]);
    WAtGrid = reshape(mapxyV(:,3),[max(size(XQ)),max(size(YQ)),max(size(ZQ))]);

    coeffU = CalLegendreCoeff(UAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);
    coeffV = CalLegendreCoeff(VAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);
    coeffW = CalLegendreCoeff(WAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);

    [DChebX,xcheb] = cheb(NX+nx+px);
    [DChebY,ycheb] = cheb(NY+ny+py);
    [DChebZ,zcheb] = cheb(NZ+nz+pz);

    UAtCheb = CalValueAt(coeffU,xcheb,ycheb,zcheb);
    VAtCheb = CalValueAt(coeffV,xcheb,ycheb,zcheb);
    WAtCheb = CalValueAt(coeffW,xcheb,ycheb,zcheb);
    
    

    UAtCheb_DX = CalDerivative(UAtCheb,DChebX,'X');
    UAtCheb_DY = CalDerivative(UAtCheb,DChebY,'Y');
    UAtCheb_DZ = CalDerivative(UAtCheb,DChebZ,'Z');

    VAtCheb_DX = CalDerivative(VAtCheb,DChebX,'X');
    VAtCheb_DY = CalDerivative(VAtCheb,DChebY,'Y');
    VAtCheb_DZ = CalDerivative(VAtCheb,DChebZ,'Z');

    WAtCheb_DX = CalDerivative(WAtCheb,DChebX,'X');
    WAtCheb_DY = CalDerivative(WAtCheb,DChebY,'Y');
    WAtCheb_DZ = CalDerivative(WAtCheb,DChebZ,'Z');

        
           
    % Calculate Acceleration
    AccXatCheb = UAtCheb_DX.*UAtCheb + UAtCheb_DY.*VAtCheb + UAtCheb_DZ.*WAtCheb;
    AccYatCheb = VAtCheb_DX.*UAtCheb + VAtCheb_DY.*VAtCheb + VAtCheb_DZ.*WAtCheb;
    AccZatCheb = WAtCheb_DX.*UAtCheb + WAtCheb_DY.*VAtCheb + WAtCheb_DZ.*WAtCheb;

    % Calculate of JJv
    JJvXatCheb = UAtCheb_DX.*AccXatCheb + UAtCheb_DY.*AccYatCheb + UAtCheb_DZ.*AccZatCheb;
    JJvYatCheb = VAtCheb_DX.*AccXatCheb + VAtCheb_DY.*AccYatCheb + VAtCheb_DZ.*AccZatCheb;
    JJvZatCheb = WAtCheb_DX.*AccXatCheb + WAtCheb_DY.*AccYatCheb + WAtCheb_DZ.*AccZatCheb;
    
    % T 2nd Derivative or 3rd order tensor.
    
    UAtCheb_DX_DX = CalDerivative(UAtCheb_DX,DChebX,'X');
    UAtCheb_DX_DY = CalDerivative(UAtCheb_DX,DChebY,'Y');
    UAtCheb_DX_DZ = CalDerivative(UAtCheb_DX,DChebZ,'Z');    
    
    UAtCheb_DY_DX = CalDerivative(UAtCheb_DY,DChebX,'X');
    UAtCheb_DY_DY = CalDerivative(UAtCheb_DY,DChebY,'Y');
    UAtCheb_DY_DZ = CalDerivative(UAtCheb_DY,DChebZ,'Z');
    
    UAtCheb_DZ_DX = CalDerivative(UAtCheb_DZ,DChebX,'X');
    UAtCheb_DZ_DY = CalDerivative(UAtCheb_DZ,DChebY,'Y');
    UAtCheb_DZ_DZ = CalDerivative(UAtCheb_DZ,DChebZ,'Z');
    
    VAtCheb_DX_DX = CalDerivative(VAtCheb_DX,DChebX,'X');
    VAtCheb_DX_DY = CalDerivative(VAtCheb_DX,DChebY,'Y');
    VAtCheb_DX_DZ = CalDerivative(VAtCheb_DX,DChebZ,'Z');    
    
    VAtCheb_DY_DX = CalDerivative(VAtCheb_DY,DChebX,'X');
    VAtCheb_DY_DY = CalDerivative(VAtCheb_DY,DChebY,'Y');
    VAtCheb_DY_DZ = CalDerivative(VAtCheb_DY,DChebZ,'Z');
    
    VAtCheb_DZ_DX = CalDerivative(VAtCheb_DZ,DChebX,'X');
    VAtCheb_DZ_DY = CalDerivative(VAtCheb_DZ,DChebY,'Y');
    VAtCheb_DZ_DZ = CalDerivative(VAtCheb_DZ,DChebZ,'Z');
    
    WAtCheb_DX_DX = CalDerivative(WAtCheb_DX,DChebX,'X');
    WAtCheb_DX_DY = CalDerivative(WAtCheb_DX,DChebY,'Y');
    WAtCheb_DX_DZ = CalDerivative(WAtCheb_DX,DChebZ,'Z');    
    
    WAtCheb_DY_DX = CalDerivative(WAtCheb_DY,DChebX,'X');
    WAtCheb_DY_DY = CalDerivative(WAtCheb_DY,DChebY,'Y');
    WAtCheb_DY_DZ = CalDerivative(WAtCheb_DY,DChebZ,'Z');
    
    WAtCheb_DZ_DX = CalDerivative(WAtCheb_DZ,DChebX,'X');
    WAtCheb_DZ_DY = CalDerivative(WAtCheb_DZ,DChebY,'Y');
    WAtCheb_DZ_DZ = CalDerivative(WAtCheb_DZ,DChebZ,'Z');
    
    % Calucate biNor_T  = JJv+Tvv
    biNorX_T = (UAtCheb_DX_DX.*UAtCheb + UAtCheb_DX_DY.*VAtCheb + UAtCheb_DX_DZ.*WAtCheb).*UAtCheb+...
               (UAtCheb_DY_DX.*UAtCheb + UAtCheb_DY_DY.*VAtCheb + UAtCheb_DY_DZ.*WAtCheb).*VAtCheb+...
               (UAtCheb_DZ_DX.*UAtCheb + UAtCheb_DZ_DY.*VAtCheb + UAtCheb_DZ_DZ.*WAtCheb).*WAtCheb+...
               JJvXatCheb;
    biNorY_T = (VAtCheb_DX_DX.*UAtCheb + VAtCheb_DX_DY.*VAtCheb + VAtCheb_DX_DZ.*WAtCheb).*UAtCheb+...
               (VAtCheb_DY_DX.*UAtCheb + VAtCheb_DY_DY.*VAtCheb + VAtCheb_DY_DZ.*WAtCheb).*VAtCheb+...
               (VAtCheb_DZ_DX.*UAtCheb + VAtCheb_DZ_DY.*VAtCheb + VAtCheb_DZ_DZ.*WAtCheb).*WAtCheb+...
               JJvYatCheb;
    biNorZ_T = (WAtCheb_DX_DX.*UAtCheb + WAtCheb_DX_DY.*VAtCheb + WAtCheb_DX_DZ.*WAtCheb).*UAtCheb+...
               (WAtCheb_DY_DX.*UAtCheb + WAtCheb_DY_DY.*VAtCheb + WAtCheb_DY_DZ.*WAtCheb).*VAtCheb+...
               (WAtCheb_DZ_DX.*UAtCheb + WAtCheb_DZ_DY.*VAtCheb + WAtCheb_DZ_DZ.*WAtCheb).*WAtCheb+...
               JJvZatCheb;


    % Calcuate spatial Derivative D(Acc)/DS
    AccXatCheb_DX = CalDerivative(AccXatCheb,DChebX,'X');
    AccXatCheb_DY = CalDerivative(AccXatCheb,DChebY,'Y');
    AccXatCheb_DZ = CalDerivative(AccXatCheb,DChebZ,'Z');

    AccYatCheb_DX = CalDerivative(AccYatCheb,DChebX,'X');
    AccYatCheb_DY = CalDerivative(AccYatCheb,DChebY,'Y');
    AccYatCheb_DZ = CalDerivative(AccYatCheb,DChebZ,'Z');

    AccZatCheb_DX = CalDerivative(AccZatCheb,DChebX,'X');
    AccZatCheb_DY = CalDerivative(AccZatCheb,DChebY,'Y');
    AccZatCheb_DZ = CalDerivative(AccZatCheb,DChebZ,'Z');


    % Calculate biNormal
    biNorX_T = AccXatCheb_DX.*UAtCheb + AccXatCheb_DY.*VAtCheb + AccXatCheb_DZ.*WAtCheb;
    biNorY_T = AccYatCheb_DX.*UAtCheb + AccYatCheb_DY.*VAtCheb + AccYatCheb_DZ.*WAtCheb;
    biNorZ_T = AccZatCheb_DX.*UAtCheb + AccZatCheb_DY.*VAtCheb + AccZatCheb_DZ.*WAtCheb;

    % Calculate curvature
    vel(:,1) = UAtCheb(:);   vel(:,2) = VAtCheb(:);   vel(:,3) = WAtCheb(:); 
    acc(:,1) = AccXatCheb(:);acc(:,2) = AccYatCheb(:);acc(:,3) = AccZatCheb(:);
    bi(:,1) = biNorX_T(:);bi(:,2) = biNorY_T(:);bi(:,3) = biNorZ_T(:);
    
    v_cross_a = cross(vel,acc,2);
    divi_C = (sum(vel.^2,2)).^(3/2);
    cur = v_cross_a./[divi_C,divi_C,divi_C];
    %cur = 
    curXAtGrid = reshape(cur(:,1),[max(size(xcheb)),max(size(ycheb)),max(size(zcheb))]);
    curYAtGrid = reshape(cur(:,2),[max(size(xcheb)),max(size(ycheb)),max(size(zcheb))]);
    curZAtGrid = reshape(cur(:,3),[max(size(xcheb)),max(size(ycheb)),max(size(zcheb))]);

    %
    % calculate torsion.
%                     norm_vel = sqrt(sum(vel.^2,2));norm_acc = sqrt(sum(acc.^2,2));norm_bi = sqrt(sum(bi.^2,2));
%                     velN = vel./[norm_vel,norm_vel,norm_vel];
%                     accN = acc./[norm_acc,norm_acc,norm_acc];
%                     biN = bi./[norm_bi,norm_bi,norm_bi];
%                      scalarTpN = dot(cross(velN,accN,2),biN,2);
%                      divi_TN = sum(cross(velN,accN).^2,2);
%                     tor_DN = sum(abs(velN.*biN),2);
% %                    tor_DN = scalarTpN;%./(divi_TN+0.1);
%                     tor = reshape(tor_DN,[max(size(xcheb)),max(size(ycheb)),max(size(zcheb))]);
    
    scalarTp = dot(v_cross_a,bi,2);
    divi_T = sum(v_cross_a.^2,2);
    tor_D = scalarTp;%./(divi_T+epislon);
    %
    norm_vel = sqrt(sum(vel.^2,2));norm_acc = sqrt(sum(acc.^2,2));norm_bi = sqrt(sum(bi.^2,2));
    velN = vel./[norm_vel,norm_vel,norm_vel];
    biN = bi./[norm_bi,norm_bi,norm_bi];
    BV_D = abs(dot(velN,biN,2));
    %%
    tor = reshape(tor_D,[max(size(xcheb)),max(size(ycheb)),max(size(zcheb))]);
    BV = reshape(BV_D,[max(size(xcheb)),max(size(ycheb)),max(size(zcheb))]);
    
%     %    % % Validation step
%     xvali = linspace(-1,1,11);
%     yvali = linspace(-1,1,11);
%     zvali = linspace(-1,1,11);
% 
%     clear mapxy;
%     [XV,YV,ZV] = meshgrid(xvali,yvali,zvali);
%     XV = permute(XV,[2,1,3]); YV = permute(Y,[2,1,3]); ZV = permute(ZV,[2,1,3]);
%     mapxy(:,1) = XV(:); mapxy(:,2) = YV(:); mapxy(:,3) = ZV(:);
%     [xy,distance,t_a,mapxyV,curveCoeff] = test_runXYZ(1,mapxy);
%     UAtVali = CalValueAt(coeffU,xvali,yvali,zvali);
%     VAtVali = CalValueAt(coeffV,xvali,yvali,zvali);
%     WAtVali = CalValueAt(coeffW,xvali,yvali,zvali);
%     mapxyV_Vali(:,1) = UAtVali(:); mapxyV_Vali(:,2) = VAtVali(:); mapxyV_Vali(:,3) = WAtVali(:);
%     er_cheb = sum(abs(mapxyV(:) - mapxyV_Vali(:)))

% %%
%%
    f=1;
    f1 = figure(f); f=f+1;
    %VizIsoSurface(xcheb,ycheb,zcheb,UAtCheb,VAtCheb,WAtCheb);hold on;
    [sx, sy, sz] = meshgrid([-1,1],[-0.3:.1:0.3],[0]);
    VizStreamLines(xcheb,ycheb,zcheb,UAtCheb,VAtCheb,WAtCheb,sx,sy,sz); hold on;
    Plot3DCurve(curveCoeff); hold on; view(va,vb)
    title('norm(Velocity)'); 
    %%
% %%
% 
%     %figure(f); f =f+1;
%     %quiver3(X,Y,Z,UAtCheb,VAtCheb,WAtCheb);
%     %VizIsoSurface(xcheb,ycheb,zcheb,UAtCheb,VAtCheb,WAtCheb);hold on;
% 
% 
%     f2= figure(f); f = f+1;
%     VizIsoSurface(xcheb,ycheb,zcheb,AccXatCheb,AccYatCheb,AccZatCheb);hold on;
% %    VizStreamLines(xcheb,ycheb,zcheb,AccXatCheb,AccYatCheb,AccZatCheb);hold on;
%     Plot3DCurve(curveCoeff); title('norm(Acceleration)'); view(va,vb); 
%     %caxis([0,1]);
% 
%     f3 = figure(f); f= f+1;
%     VizIsoSurface(xcheb,ycheb,zcheb,biNorX_T,biNorY_T,biNorZ_T);hold on;
% %    VizStreamLines(xcheb,ycheb,zcheb,biNorX_T,biNorY_T,biNorZ_T);hold on;
%     Plot3DCurve(curveCoeff); title('norm(Binormal)'); view(va,vb); 
%     %caxis([0,1]);
% 
%     f6 = figure(6);
%     Plot3DCurve(curveCoeff); title('Vortex axis curve'); view(va,vb)
%     VizStreamLines(XQ,YQ,ZQ,UAtGrid,VAtGrid,WAtGrid); hold on;
% 
%     f4 = figure(4);
%     VizIsoSurface(xcheb,ycheb,zcheb,curXAtGrid,curYAtGrid,curZAtGrid);hold on;
%     Plot3DCurve(curveCoeff); title('norm(Curvature)'); view(va,vb);% caxis([0,1]);
%  %%
%     f5 = figure(5);
%     VizIsoSurfaceScalar(xcheb,ycheb,zcheb,tor);hold on;
%     Plot3DCurve(curveCoeff); title('Torsion'); view(va,vb); 
%     
%     f11 = figure(11);
%     VizIsoContoursScalar(xcheb,ycheb,zcheb,tor);hold on;
%     Plot3DCurve(curveCoeff); title('torsion'); view(va,vb); 
% 
%   %%
%     f7 = figure(7);
%     pts = VizSurface(xcheb,ycheb,zcheb,curXAtGrid,curYAtGrid,curZAtGrid,0.2);
%     Plot3DCurve(curveCoeff); title('norm(Curvature)'); view(va,vb);% caxis([0,1]);
%     
%     Npts = FindLargestComp(pts);
%     f8 = figure(8);
%     Sol_curveCoeff= ExtractCurFromIsoSurface(6,Npts);
%     VizSurface(xcheb,ycheb,zcheb,curXAtGrid,curYAtGrid,curZAtGrid,0.2);
%     Plot3DCurve(Sol_curveCoeff); title('norm(Curvature)'); view(va,vb);% caxis([0,1]);
%     %%
%     f11 = figure(11);
%     VizIsoContoursScalar(xcheb,ycheb,zcheb,tor);hold on;
%     title('torsion'); view(va,vb); 
%     Npts = FindLargestComp(pts);
%     Sol_curveCoeff= ExtractCurFromIsoSurface(6,Npts);
%     Plot3DCurve(curveCoeff); 
%     Plot3DCurve(Sol_curveCoeff);
%     
%     %%
%     
%     f12 = figure(12);
% %    T_Pts = VizSurfaceScalar(xcheb,ycheb,zcheb,tor,0.1); hold on;
%     [T_fvs,T_Pts] = VizSurfaceScalar(xcheb,ycheb,zcheb,tor,0); hold on;
%     T_Npts = FindLargestCompTor(T_Pts);
%     Sol_TorCoeff= ExtractCurFromIsoSurface(6,T_Npts);
%     Plot3DCurve(curveCoeff,20,'-r'); 
%     Plot3DCurve(Sol_TorCoeff,20,'-g');
    %%
    f12 = figure(12);
    [T_fvs,T_Pts] = VizSurfaceScalar(xcheb,ycheb,zcheb,tor,0); hold on;
    T_Npts = FindLargestCompTor(T_Pts);

    f13 = figure(13);
    [Xc,Yc,Zc] = meshgrid(xcheb,ycheb,zcheb);
    BVc = ipermute(BV,[2,1,3]);
    BV_Tor_0 = interp3(Xc,Yc,Zc,BVc,T_Pts(:,1),T_Pts(:,2),T_Pts(:,3));
    %BV_Tor_9 = (BV_Tor_0 > 0.5)+0.1;
    patch('Faces',T_fvs,'Vertices',T_Pts,'FaceVertexCData', BV_Tor_0, 'FaceColor','interp','edgecolor', 'interp'); hold on;
    Plot3DCurve(curveCoeff,20,'-r');view([va,vb]);
    toc
    
%     
%      f14 = figure(14);
%      BV_Tor_sc = interp3(Xc,Yc,Zc,BVc,T_Npts(:,1),T_Npts(:,2),T_Npts(:,3));
%      scatter3(T_Npts(:,1),T_Npts(:,2),T_Npts(:,3),ones(size(BV_Tor_sc)), BV_Tor_sc); hold on;
%      Plot3DCurve(curveCoeff,20,'-r');view([va,vb]);

    %%
% tic
% close all;
% va = -24; vb = 12;
%  for ij = 1:9
%     f13 = figure(ij);
%     BVc = ipermute(BV,[2,1,3]);
%     BV_Tor_0 = interp3(Xc,Yc,Zc,BVc,T_Pts(:,1),T_Pts(:,2),T_Pts(:,3));
%     BV_Tor_9 = (BV_Tor_0 > 1-ij/10)+0.1;
%     patch('Faces',T_fvs,'Vertices',T_Pts,'FaceVertexCData', BV_Tor_9, 'FaceColor','interp','edgecolor', 'interp'); hold on;
%     Plot3DCurve(curveCoeff,20,'-r');view([va,vb]);
%     title(strcat('Dot(B,V) at threshold = ',num2str(1-ij/10)));
% %    print(strcat('-f',int2str(ij)),strcat('images/ThresReport/ThreshldValN_R_6_',int2str(ij)),'-djpeg');
%  end
% toc

    %%
%     f14 = figure(14);
%     scatter3(T_Pts(:,1),T_Pts(:,2),T_Pts(:,3),ones(size(BV_Tor_0)), BV_Tor_0); hold on;
%     Plot3DCurve(curveCoeff);view([va,vb]);
    
%    savefig(f13,strcat('images/TorLegN_7_eiTest_',int2str(ei)));      
%    close all;
%end 
% %% Cur ISO Surfaces
%     close all;
%     figure(1);
%     pts = VizSurface(xcheb,ycheb,zcheb,curXAtGrid,curYAtGrid,curZAtGrid,0.2);
%     Npts = FindLargestComp(pts);
%     Sol_curveCoeff2= ExtractCurFromIsoSurface(6,Npts);
%     Plot3DCurve(Sol_curveCoeff2,20,'b');
%     Plot3DCurve(curveCoeff,20,'r'); 
%     title('norm(Curvature),Iso=0.2'); view(va,vb);% caxis([0,1]);
%     
%     figure(2);
%     pts = VizSurface(xcheb,ycheb,zcheb,curXAtGrid,curYAtGrid,curZAtGrid,0.1);
%     Npts = FindLargestComp(pts);
%     Sol_curveCoeff1= ExtractCurFromIsoSurface(6,Npts);
%     Plot3DCurve(Sol_curveCoeff1,20,'g');
%     Plot3DCurve(curveCoeff,20,'r'); title('norm(Curvature),Iso=0.1'); view(va,vb);% caxis([0,1]);
%     
%     figure(3);
%     Npts = VizSurface(xcheb,ycheb,zcheb,curXAtGrid,curYAtGrid,curZAtGrid,0.05);
%     %Npts = FindLargestComp(pts);
%     Sol_curveCoeff05= ExtractCurFromIsoSurface(6,Npts);
%     Plot3DCurve(Sol_curveCoeff05,20,'c');
%     Plot3DCurve(curveCoeff,20,'r'); title('norm(Curvature),Iso=0.05'); view(va,vb);% caxis([0,1]);
%     
%     figure(4);
%     pr = Plot3DCurve(curveCoeff,20,'r');
%     pb = Plot3DCurve(Sol_curveCoeff2,20,'b');
%     pg = Plot3DCurve(Sol_curveCoeff1,20,'g');
%     pc = Plot3DCurve(Sol_curveCoeff05,20,'c');
%     legend([pr,pb,pg,pc],'Core Line','IsoCur-0.2','IsoCur-0.1','IsoCur-0.05')
%     title('Plot of all Iso-Curvature core lines in one graph');
%     
%     figure(5);
%     cdata = linspace(-1,1,20);
%     Acx = B_eval(curveCoeff(1,:),cdata); 
%     Acy = B_eval(curveCoeff(2,:),cdata);
%     Acz = B_eval(curveCoeff(3,:),cdata);
%     C2cx = B_eval(Sol_curveCoeff2(1,:),cdata); 
%     C2cy = B_eval(Sol_curveCoeff2(2,:),cdata);
%     C2cz = B_eval(Sol_curveCoeff2(3,:),cdata);
%     C1cx = B_eval(Sol_curveCoeff1(1,:),cdata); 
%     C1cy = B_eval(Sol_curveCoeff1(2,:),cdata);
%     C1cz = B_eval(Sol_curveCoeff1(3,:),cdata);
%     C05cx = B_eval(Sol_curveCoeff05(1,:),cdata); 
%     C05cy = B_eval(Sol_curveCoeff05(2,:),cdata);
%     C05cz = B_eval(Sol_curveCoeff05(3,:),cdata);
%     C2 = abs(Acx-C2cx) + abs(Acy-C2cy) + abs(Acz-C2cz);
%     C1 = abs(Acx-C1cx) + abs(Acy-C1cy) + abs(Acz-C1cz);
%     C05 = abs(Acx-C05cx) + abs(Acy-C05cy) + abs(Acz-C05cz);
%     plot(cdata,C2,'b',cdata,C1,'g',cdata,C05,'c');
%     legend('IsoCur-0.2','IsoCur-0.1','IsoCur-0.05');
%     ylim([0.28,0.4]);
%     title('Distance of Iso-Curvature from the A-core.');
%     ylabel('distance from A-coreline'); xlabel('t=[-1,1]');
% % End of Cur Iso surfaces.   


%% End of Cur 2D and 1D plots   


% %% Curvature.
%     close all;
%     figure(1);
%     pts = VizSurfaceScalar(xcheb,ycheb,zcheb,tor,-0.2,'blue');
%     Npts = FindLargestCompTor(pts);
%     Sol_curveCoeff2= ExtractCurFromIsoSurface(6,Npts);
%     Plot3DCurve(Sol_curveCoeff2,20,'b');
%     Plot3DCurve(curveCoeff,20,'r'); 
%     title('Torsion,IsoValue = -0.2'); view(va,vb);% caxis([0,1]);
%    
%     figure(2);
%     pts = VizSurfaceScalar(xcheb,ycheb,zcheb,tor,0,'green');
%     Npts = FindLargestCompTor(pts);
%     Sol_curveCoeff1= ExtractCurFromIsoSurface(6,Npts);
%     Plot3DCurve(Sol_curveCoeff1,20,'g');
%     Plot3DCurve(curveCoeff,20,'r'); 
%     title('Torsion,IsoValue = 0.0'); view(va,vb);% caxis([0,1]);
%     
%     figure(3);
%     pts = VizSurfaceScalar(xcheb,ycheb,zcheb,tor,0.1,'cyan');
%     Npts = FindLargestCompTor(pts);
%     Sol_curveCoeff05= ExtractCurFromIsoSurface(6,Npts);
%     Plot3DCurve(Sol_curveCoeff05,20,'c');
%     Plot3DCurve(curveCoeff,20,'r');
%     title('Torsion,IsoValue = 0.1'); view(va,vb);% caxis([0,1]);
% 
%     figure(4);
%     pr = Plot3DCurve(curveCoeff,20,'r');
%     pb = Plot3DCurve(Sol_curveCoeff2,20,'b');
%     pg = Plot3DCurve(Sol_curveCoeff1,20,'g');
%     pc = Plot3DCurve(Sol_curveCoeff05,20,'c');
%     legend([pr,pb,pg,pc],'A-Core Line','Tor -0.2','Tor 0.0','Tor 0.1')
%     title('Plot of all T-Core lines in one graph (-0.2,0.0,0.1)');
%     
%     figure(5);
%     cdata = linspace(-1,1,20);
%     Acx = B_eval(curveCoeff(1,:),cdata); 
%     Acy = B_eval(curveCoeff(2,:),cdata);
%     Acz = B_eval(curveCoeff(3,:),cdata);
%     C2cx = B_eval(Sol_curveCoeff2(1,:),cdata); 
%     C2cy = B_eval(Sol_curveCoeff2(2,:),cdata);
%     C2cz = B_eval(Sol_curveCoeff2(3,:),cdata);
%     C1cx = B_eval(Sol_curveCoeff1(1,:),cdata); 
%     C1cy = B_eval(Sol_curveCoeff1(2,:),cdata);
%     C1cz = B_eval(Sol_curveCoeff1(3,:),cdata);
%     C05cx = B_eval(Sol_curveCoeff05(1,:),cdata); 
%     C05cy = B_eval(Sol_curveCoeff05(2,:),cdata);
%     C05cz = B_eval(Sol_curveCoeff05(3,:),cdata);
%     C2 = abs(Acx-C2cx) + abs(Acy-C2cy) + abs(Acz-C2cz);
%     C1 = abs(Acx-C1cx) + abs(Acy-C1cy) + abs(Acz-C1cz);
%     C05 = abs(Acx-C05cx) + abs(Acy-C05cy) + abs(Acz-C05cz);
%     plot(cdata,C2,'b',cdata,C1,'g',cdata,C05,'c');
%     legend('Tor -0.2','Tor 0.0','Tor 0.1');
%     ylim([0.0,0.2]);
%     title('Distance of T-Core from the A-core.');
%     ylabel('distance from A-coreline'); xlabel('t=[-1,1]');
% %  End of Cur


% %% TOR
% close all
%     v = [-0.2,-0.1,0,0.1,0.2];
%     count =1;
%     for yplot = -1:1:1
%         figure(count); 
%         curGrid = tor;
%         [X,Y,Z] = meshgrid(xcheb,ycheb,zcheb);
%         curGrid = ipermute(curGrid,[2,1,3]);
%         [Xs,Ys,Zs] = meshgrid((-0.4:.01:0.4),yplot,(-1:.01:1));
%         [Xp,Yp,Zp] = meshgrid((-0.4:.1:0.4),yplot,(-1:.1:1));
%         cur1 = interp3(X,Y,Z,curGrid,Xs,Ys,Zs);
%         contour( squeeze(Xs),squeeze(Zs),squeeze(cur1),v,'ShowText','on');
% %        surf(squeeze(Xs),squeeze(Zs),squeeze(cur1),'EdgeColor','none','LineStyle','none','FaceLighting','phong');hold on;
% %        surf(squeeze(Xp),squeeze(Zp),zeros(size(squeeze(Xp))),'EdgeColor','black','Color','none');
% %        xlabel('x');ylabel('y');zlabel('IsoCur'); view(va,vb)
%         title(strcat('Torsion at Y = ',num2str(yplot)));
%         count = count+1;
%         yplot
%     end    
% 
%     figure(count); zplot =0;
%     for yplot = [-1,-0.4,0,0.6,1.0]
%         [X,Y,Z] = meshgrid(xcheb,ycheb,zcheb);
%         [Xs,Ys,Zs] = meshgrid((-1:.01:1),yplot,zplot);
%  %       [Xs,Ys,Zs] = meshgrid(zplot,yplot,(-1:.01:1));
%         cur1 = interp3(X,Y,Z,curGrid,Xs,Ys,Zs);
%         plot(squeeze(Xs),squeeze(cur1)); hold on;
%         xlabel('x');ylabel('IsoTor'); 
%     end    
%     title(strcat('Torsion at line Z=0 Y=[-1,-0.4,0,0.6,1]',num2str(yplot)));
%     legend('Y=-1','Y=-0.4','Y=0','Y=0.6','Y=1.0');
%         plot(squeeze(Xs),zeros(size(squeeze(cur1))),'black'); hold off;
% % END of Tor


% %% BV
%     close all;
%     figure(1);
%     pts = VizSurfaceScalar(xcheb,ycheb,zcheb,BV,0.1,'blue');
%     Npts = FindLargestCompTor(pts);
%     Sol_curveCoeff2= ExtractCurFromIsoSurface(6,Npts);
%     Plot3DCurve(Sol_curveCoeff2,20,'b');
%     Plot3DCurve(curveCoeff,20,'r'); 
%     title('B parallel V,IsoValue = 0.2'); view(va,vb);% caxis([0,1]);
%    
%     figure(2);
%     pts = VizSurfaceScalar(xcheb,ycheb,zcheb,BV,0.05,'green');
%     Npts = FindLargestCompTor(pts);
%     Sol_curveCoeff1= ExtractCurFromIsoSurface(6,Npts);
%     Plot3DCurve(Sol_curveCoeff1,20,'g');
%     Plot3DCurve(curveCoeff,20,'r'); 
%     title('B parallel V,IsoValue = 0.1'); view(va,vb);% caxis([0,1]);
%     
%     figure(3);
%     pts = VizSurfaceScalar(xcheb,ycheb,zcheb,BV,0.01,'cyan');
%     Npts = FindLargestCompTor(pts);
%     Sol_curveCoeff05= ExtractCurFromIsoSurface(6,Npts);
%     Plot3DCurve(Sol_curveCoeff05,20,'c');
%     Plot3DCurve(curveCoeff,20,'r');
%     title('B parallel V ,IsoValue = 0.05'); view(va,vb);% caxis([0,1]);
% 
%     figure(4);
%     pr = Plot3DCurve(curveCoeff,20,'r');
%     pb = Plot3DCurve(Sol_curveCoeff2,20,'b');
%     pg = Plot3DCurve(Sol_curveCoeff1,20,'g');
%     pc = Plot3DCurve(Sol_curveCoeff05,20,'c');
%     legend([pr,pb,pg,pc],'A-Core Line','Tor -0.2','Tor 0.0','Tor 0.1')
%     title('Plot of all BV-Core lines in one graph (-0.2,0.0,0.1)');
%     
%     figure(5);
%     cdata = linspace(-1,1,20);
%     Acx = B_eval(curveCoeff(1,:),cdata); 
%     Acy = B_eval(curveCoeff(2,:),cdata);
%     Acz = B_eval(curveCoeff(3,:),cdata);
%     C2cx = B_eval(Sol_curveCoeff2(1,:),cdata); 
%     C2cy = B_eval(Sol_curveCoeff2(2,:),cdata);
%     C2cz = B_eval(Sol_curveCoeff2(3,:),cdata);
%     C1cx = B_eval(Sol_curveCoeff1(1,:),cdata); 
%     C1cy = B_eval(Sol_curveCoeff1(2,:),cdata);
%     C1cz = B_eval(Sol_curveCoeff1(3,:),cdata);
%     C05cx = B_eval(Sol_curveCoeff05(1,:),cdata); 
%     C05cy = B_eval(Sol_curveCoeff05(2,:),cdata);
%     C05cz = B_eval(Sol_curveCoeff05(3,:),cdata);
%     C2 = abs(Acx-C2cx) + abs(Acy-C2cy) + abs(Acz-C2cz);
%     C1 = abs(Acx-C1cx) + abs(Acy-C1cy) + abs(Acz-C1cz);
%     C05 = abs(Acx-C05cx) + abs(Acy-C05cy) + abs(Acz-C05cz);
%     plot(cdata,C2,'b',cdata,C1,'g',cdata,C05,'c');
%     legend('Tor -0.2','Tor 0.0','Tor 0.1');
%     ylim([0.0,0.2]);
%     title('Distance of T-Core from the A-core.');
%     ylabel('distance from A-coreline'); xlabel('t=[-1,1]');
% % END of BV    



% %%
%        plot3(squeeze(Xs),squeeze(Zs),squeeze(cur1));
%     f13 = figure(13);
%     plot3(T_Npts(:,1),T_Npts(:,2),T_Npts(:,3),'*r')
%    T_Pts = VizSurfaceScalar(xcheb,ycheb,zcheb,tor,-0.1); hold on;
%    T_Pts = VizSurfaceScalar(xcheb,ycheb,zcheb,tor,-0.15); hold on;
%    
    
%     savefig(f1,strcat('images/vel_',int2str(NN)));
%     savefig(f2,strcat('images/acc_',int2str(NN)));
%     savefig(f3,strcat('images/binor_',int2str(NN)));
%     savefig(f4,strcat('images/cur_',int2str(NN)));
%     savefig(f5,strcat('images/tor_',int2str(NN)));
%     close all;
%     save(strcat('testdata/data_mat',int2str(NN),'_.mat'));

%end

% print('-f6','images/0CurveOp_10xyz','-djpeg');
% print('-f1','images/1VelOp_10xyz','-djpeg');
% print('-f2','images/2AccOp_10xyz','-djpeg');
% print('-f3','images/3binorOp_10xyz','-djpeg');
% print('-f4','images/4curOp_10xyz','-djpeg');
% print('-f5','images/5torOp_10xyz','-djpeg');

