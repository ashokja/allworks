
 function y = ChebyshevPoly(degree,x)

 if degree == 0
   y = ones(size(x));
 elseif degree == 1
   y = x;
 else
   a0 = ones(size(x)); a1 = x;
   for i = 2:degree, 
     a2 = 2.0.*x.*a1 - a0;
     a0 = a1;
     a1 = a2;
   end
   y = a2;
 end
  
