import controlP5.*;
import papaya.*;
ControlP5 myGCP5;

ArrayList<ArrayList<Float>> KnotAs;
ArrayList<ArrayList<PVector>> CPtAs;
ArrayList<ArrayList<Draggable>> D_CPtAs;
ArrayList<Integer> NumCPts;
ArrayList<Integer> Degs;
ArrayList<Integer> CTypes;

//Surface
ArrayList<SurfaceCurves> scA;
SurfaceCurves sc,sdc_sc;
SurfaceDataCurves sdc;

//Unmodified
float ScreenXMin = -15;
float ScreenXMax = 15;
float ScreenYMin = -15;
float ScreenYMax = 15;
int ScreenX = 800;
int ScreenY = 800;
int CScreenX = 200;
int CScreenY = 0;
int C2ScreenX =200;
int C2ScreenY =0;
float CtetaX = 0;
float CtetaY = 0;
float CtetaZ = 0;
float T =1000.0;
//Modified
int myGCurC = -1;
// Used inside the function some where.
int myGNextLine =-1;
//Buttons
boolean myGCPVisible =true;
boolean myGLinesVisible =true;
boolean myGDeleteButton=false;
boolean GAddPoint = false;
color [] colors = new color[7];
int GCur_Num=1;
int GDegree=3;
Numberbox myGCN;
Numberbox myGCN_AP;
Numberbox myGD;
Numberbox myGDC;
RadioButton r;
Numberbox myGCN_AC;
Numberbox myGD_AC;
RadioButton r_AC;
Toggle T_ConvexHull;
Toggle T_KnotCurves;
Toggle T_NodalCurves;
Toggle T_IsoCurves;
Slider T_uSlider;
Slider T_vSlider;
boolean GUniform = true;
boolean GModified = false;
boolean recordingClicks = false;
boolean GACUni = false;
ArrayList<PVector> cptsA;
ArrayList<Draggable> D_cptsA;
int cpts = 0;
int aDeg =0;



void setup()
{
  // Intialize setup and variables.
  size(ScreenX + CScreenX + C2ScreenX, ScreenY +  CScreenY+C2ScreenY);
  Degs = new ArrayList<Integer> ();
  CTypes = new ArrayList<Integer> ();
  NumCPts = new ArrayList<Integer> ();
  CPtAs = new ArrayList<ArrayList<PVector>> ();
  KnotAs = new ArrayList<ArrayList<Float>> ();
  D_CPtAs = new ArrayList<ArrayList<Draggable>> ();
  scA = new ArrayList<SurfaceCurves> ();
  myGCP5Setup();
  //myLoadFile("heart.dat");
  //myLoadDataFile("heart.dat");
  //sc = myLoadSufFile("surf1.dat");
  sdc = myLoadSufDataFile("bicubic_horns_I.dat");
  //sdc.printData();
  //sc = myLoadSufFile("upanddown.dat");
  sdc.GetC(2);
  //print("\n\n\n");
  //sc.GetRowCpts(1);
  //print("\n\n\n");
  //sc.GetColCpts(1);
  //print("\n\n\n");
  smooth();
}


void draw()
{
  stroke(255, 102, 0);
  //stroke(0, 0, 0);
  background(255, 255, 255);
  // Loop through all curves.
  stroke(0, 102, 255);
  
  myDrawButtons();
  // Drawing all curves of curves.
  for ( int cs=0; cs <Degs.size(); cs++)
  {
    // In each curve
    ArrayList<Float> KnotA = KnotAs.get(cs);
    ArrayList<PVector> CPtA = CPtAs.get(cs);
    ArrayList<Draggable> D_CPtA = D_CPtAs.get(cs);
    int numCPt = NumCPts.get(cs);
    int Deg = Degs.get(cs);
    float t ; int J,I;
    DrawCurve( KnotA, CPtA, numCPt, Deg );
  }
    
  // Draw all surfaces
  for( int s =0; s < scA.size(); s++)
  {
    SurfaceCurves scc = scA.get(s);
    if(T_ConvexHull.getState()){
      stroke(200);
      scc.DrawControlMesh();    
    }
    if(T_IsoCurves.getState()){
      stroke(0,200,0);
      scc.DrawIsoSurMesh((int)T_uSlider.getValue(),(int)T_vSlider.getValue());
    }  
    if(T_KnotCurves.getState()) {
      stroke(0,0,200);
      scc.DrawSurKnotMesh();      
    }
    if(T_NodalCurves.getState()){
      stroke(200,0,0);
      scc.DrawSurNodalMesh();
    }

  }
  myDrawControlBox();
  myDrawLines();
//  sc.DrawControlMesh();
  //sc.DrawIsoSurMesh(10,10);
//  sc.DrawSurMesh();
//  stroke(0,0,128);
//  sdc.DrawGivenCurves();

}


void DrawCurve(ArrayList<Float> KnotA, ArrayList<PVector> CPtA, int numCPt, int Deg )
{
    float t ; int J,I;
    //Draw Control Polygon
    
    //stroke(240);
    myDrawLines(CPtA);
    //stroke(255, 102, 0);
    //Draw Buttons
    //Create and intialize PVectors
    PVector[] p_old = new PVector[Deg+1];
    PVector[] p_new = new PVector[Deg+1];
    PVector[] p_mid ;//= new PVector[Deg+1];
    for (int i =0; i <Deg+1; i++)
    {
      p_old[i] = new PVector(0.0,0.0,1.0);
      p_new[i] = new PVector(0.0,0.0,1.0);
    }
    //Actual Algorithm.
    
    for(int kt =Deg; kt< numCPt; kt++ )
    {
      if(KnotA.get(kt)<KnotA.get(kt+1))
      {
        J = kt;
       // Initialize p_old with Actual points;
        //print("\n KnotA["+kt+"]"); 
        for (int tt =1; tt< T; tt++)
        { // t loop
          t = KnotA.get(J) + (KnotA.get(J+1)-KnotA.get(J))*tt/T;
          for (int i=0; i<=Deg;i++)
          {
            PVector p = CPtA.get(i+J-Deg); 
            p_old[i].x = p.x; 
            p_old[i].y = p.y;
            p_old[i].z = p.z;
          } 
          //print("\n\n"+t+" ");
          for(int j =1 ; j<= Deg; j++)
          {
             for( int i =J-Deg+j; i<= J; i++)
             {
               I = i-J+Deg;
               float ul = t- KnotA.get(i);
               float b = KnotA.get(i+Deg+1-j) - KnotA.get(i);
               float ur = b-ul;

               p_new[I].x = p_old[I].x*ul/b + p_old[I-1].x*ur/b;
               p_new[I].y = p_old[I].y*ul/b + p_old[I-1].y*ur/b;
               p_new[I].z = p_old[I].z*ul/b + p_old[I-1].z*ur/b;
               //print("\n J = "+J+" i="+i+" I ="+I +" ul="+ul+" b"+ b+" ur="+ur);
             }
             //print("\np_old"+p_old[0]+"\t"+p_old[1]+"\t"+p_old[2]);
             //print("\np_new"+p_new[0]+"\t"+p_new[1]+"\t"+p_new[2]);               
             p_mid = p_old;
             p_old = p_new;
             p_new = p_mid;
          } 
          point(CToS(p_old[Deg],1) , CToS(p_old[Deg],2) );
        }
      }else{
        continue;
      }
    } 
}

void mousePressed() 
{
  for (int c =0; c< CPtAs.size(); c++)
  {
    ArrayList<Draggable> D_CPtA = D_CPtAs.get(c);
    for (int pb =0; pb < D_CPtA.size(); pb++)
    {
      D_CPtA.get(pb).clicked(mouseX,mouseY);
    }
  }
  
  if(myGDeleteButton)
  {
    print("\npressed\n");
    for (int c =0; c< CPtAs.size(); c++)
    {
      ArrayList<PVector> CPtA = CPtAs.get(c);
      for (int pb =0; pb < CPtA.size(); pb++)
      {
        // check if the point is clicked then delete it.
        PVector p = CPtA.get(pb);
        int x = CToS(p,1) - mouseX;
        int y = CToS(p,2) - mouseY;
        if ( (abs(x) <10) && (abs(y)<10))
        {
          CPtAs.get(c).remove(pb);
          D_CPtAs.get(c).remove(pb);
          KnotAs.get(c).remove(NumCPts.get(c)-1);
          NumCPts.set(c,NumCPts.get(c)-1);
        }
      }
    }    
    myGDeleteButton = false;
  }
  
  if(GAddPoint)
  {
    AddPoint();
    GAddPoint = false;
  }
  
  if(recordingClicks)
  {
    print ("\n into function mouse pressed recording clicks."+cpts+" \n");
    if( cpts > 0 )
    {
      int mx = mouseX;
      int my = mouseY;
      PVector p = new PVector(SToC(mx,my,1),SToC(mx,my,2),1.0);
      Draggable d = new Draggable(mx,my,10,10);
      cptsA.add(p);
      D_cptsA.add(d);
      cpts = cpts-1;
      print("\n This Add point "+cpts+" \n");
    }
    if( 0 == cpts)
    {
      //Add curve
      int numCpts = cptsA.size();
      ArrayList<Float> knot = new ArrayList<Float>();
      float count =0.0;
      for(int i =0; i < numCpts+aDeg+1; i++ )
      {
        if (GACUni)
        {
          count++;
        }else
        {
          if(i <= aDeg || i >= numCpts+1)
          {}
          else
          {
            count++;
          }
        }
        knot.add(count);
      }
      
      CPtAs.add(cptsA);
      D_CPtAs.add(D_cptsA);
      Degs.add(aDeg);
      KnotAs.add(knot);
      NumCPts.add(cptsA.size());
      myGCurC++;
      recordingClicks = false;
    }
  }
}

void AddCurve(ArrayList<PVector> cptsA,  ArrayList<Float> knotA,int aDeg)
{
  // create clickable points.
  ArrayList<Draggable> D_cptsA = CreateClickablePoints(cptsA,knotA,aDeg);  //= new ArrayList<Draggable> ();
  
  CPtAs.add(cptsA);
  D_CPtAs.add(D_cptsA);
  Degs.add(aDeg);
  KnotAs.add(knotA);
  NumCPts.add(cptsA.size());
  myGCurC++;
}

ArrayList<Draggable> CreateClickablePoints(ArrayList<PVector> cptsA,  ArrayList<Float> knotA,int aDeg)
{
  ArrayList<Draggable> D_cptsA = new ArrayList<Draggable>();
  for (int i =0 ;i < cptsA.size(); i++)
  {
    PVector p = cptsA.get(i);
    Draggable d = new Draggable(CToS(p,1),CToS(p,2),10,10);
    D_cptsA.add(d);
  }
  return D_cptsA;
}

void mouseReleased() 
{
  //d.stopDragging();
  for (int c =0; c< CPtAs.size(); c++)
  {
    ArrayList<Draggable> D_CPtA = D_CPtAs.get(c);
    for (int pb =0; pb < D_CPtA.size(); pb++)
    {
      D_CPtA.get(pb).stopDragging();
    }
  }
  MymouseWheel(+1);
  MymouseWheel(-1);
}
