#pragma once

//#define NEKTAR_USING_LAPACK
//#define NEKTAR_USING_BLAS
// Note TOLERENCE should always be smaller than TOLERENCE_MESH_COMP
#define TOLERENCE 1e-13
#define TOLERENCE_MESH_COMP 1e-14
//#define TOLERENCE 1e-13
//#define TOLERENCE_MESH_COMP 1e-12
   // TO test SpeedLSIAC V2
//#define TOLERENCE 1e-15
//#define TOLERENCE_MESH_COMP 1e-13

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include <LibUtilities/LinearAlgebra/Lapack.hpp>
#include <MultiRegions/ContField1D.h>
#include <LibUtilities/BasicUtils/VmathArray.hpp>
#include <LibUtilities/Communication/Comm.h>
#include <iostream>
#include <vector>

using namespace std;
using namespace Nektar;

/// This class help import all nektar base classes.
class NektarBaseClass{
	private:
	protected:
	public:
	void printNekArray(const Array<OneD,NekDouble> &ar,int del=0)const;
	void printNekArray(const vector<NekDouble> &ar,int del=0) const;
	void printNekArray(const vector<int> &ar,int del=0) const;
	void writeNekArray(vector<int> &ar,string filename) const;
	void writeNekArray(vector<NekDouble> &ar, string filename) const;
	void writeNekArray(Array<OneD,NekDouble> &ar, string filename) const;
	void writeNekArrayBin(Array<OneD,NekDouble> &ar, string filename) const;
//	void readNekArray(vector<NekDouble> &ar, string filename) const;
	void readNekArray(Array<OneD,NekDouble> &ar, string filename) const;
	void readNekArray(vector<NekDouble> &ar, string filename) const;
	void readNekArray(vector<int> &ar, string filename) const;
	
	void printGraphArray(const Array<OneD,NekDouble> &test,NekDouble down, NekDouble up,NekDouble increment=1.0)const;
};


