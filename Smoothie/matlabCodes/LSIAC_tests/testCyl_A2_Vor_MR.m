clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [81];

poly =2;
order = 2;
alpha =1;
count =1;

angle = 89;
fileName = 'Cy2l_300_';


for r = rates
    
    xfname = strcat('datatxt/',fileName,int2str(order),'_A2_',int2str(angle),'_pX_2D_OneSided2kp1',int2str(r),'.txt' );
    yfname = strcat('datatxt/',fileName,int2str(order),'_A2_',int2str(angle),'_pY_2D_OneSided2kp1',int2str(r),'.txt' );
    
    pI_pfname = strcat('datatxt/',fileName,int2str(order),'_A2_',int2str(angle),'_pI_p_2D_OneSided2kp1',int2str(r),'.txt' );
    pI_fname = strcat('datatxt/',fileName,int2str(order),'_A2_',int2str(angle),'_pI_2D_OneSided2kp1',int2str(r),'.txt' );
    
    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');
        
    pI_pval = textread(pI_pfname, '%f\t');     pI_val = textread(pI_fname, '%f\t');
    
    s = sqrt(max(size(xpos)));
    xVpos = reshape(xpos,s,s);    yVpos = reshape(ypos,s,s);    
    pI_pVal = reshape(pI_pval,s,s);     pI_Val = reshape(pI_val,s,s);
    
    
%Curvature,     

figure(18)
    surf(xVpos,yVpos, pI_pVal,'EdgeAlpha',0.1); 
    colorbar;
    title(strcat('Proj P',int2str(poly),' Vorticity'));
    %view([0,90]);
    saveas(18,strcat(fileName,'_',int2str(r),'_A_',int2str(angle),'_','p_C'),'jpg');

figure(19)
    surf(xVpos,yVpos, pI_Val,'EdgeAlpha',0.1); 
    colorbar;
    title(strcat('SIAC P',int2str(poly),' Vorticity'));
    %view([0,90]);    
    saveas(19,strcat(fileName,'_',int2str(r),'_A_',int2str(angle),'_','S_C'),'jpg');

figure(20)
    surf(xVpos,yVpos, pI_pVal,'EdgeAlpha',0.1); 
    colorbar;
    title(strcat('Proj P',int2str(poly),' Vorticity'));
    view([0,90]);
    saveas(20,strcat(fileName,'_',int2str(r),'_A_',int2str(angle),'_','p_C_v2'),'jpg');

figure(21)
    surf(xVpos,yVpos, pI_Val,'EdgeAlpha',0.1); 
    colorbar;
    title(strcat('SIAC P',int2str(poly),' Vorticity'));
    view([0,90]);    
    saveas(21,strcat(fileName,'_',int2str(r),'_A_',int2str(angle),'_','S_C_v2'),'jpg');
    
figure(22)
    surf(xVpos,yVpos, pI_pVal ,'EdgeAlpha',0.1); 
    colorbar;
    title(strcat('Proj P',int2str(poly),'_A_',int2str(angle),' C'));
    view([0,90]);
    
figure(23)
    contour(xVpos,yVpos, pI_Val,[min(pI_Val(:)) :.1: max(pI_Val(:))] ); 
    saveas(23,strcat(fileName,'_',int2str(r),'_A_',int2str(angle),'_','S_C_v3'),'jpg');
    
figure(24)
    contour(xVpos,yVpos, pI_pVal,[min(pI_pVal(:)) :.1: max(pI_pVal(:))] ); 
    saveas(24,strcat(fileName,'_',int2str(r),'_A_',int2str(angle),'_','P_C_v3'),'jpg');
    
    
end

