% calculates the lagrange polynomial values on x that are 
% defined on the points, xpts 



function y = LagrangePoly(x,pt,npts,xpts)

y = ones(size(x));

for i=1:(pt-1),
  y = y.*(x-xpts(i))./(xpts(pt)-xpts(i));
end

for i=(pt+1):npts,
  y = y.*(x-xpts(i))./(xpts(pt)-xpts(i));
end
