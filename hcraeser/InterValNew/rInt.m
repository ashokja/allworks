% intersection of real values code.
function res = rInt(X,Y)
    a = X(1); b = X(2);
    c = Y(1); d = Y(2);
    
    if (b <c)
        res = [nan,nan];
        return;
    elseif (d <a)
        res = [nan,nan];
        return;
    end
    
    if ( a<=c) & (d<=b)
        res = [c,d];
        return;
    end
    
    if ( c<=a) & (b<=d)
        res = [a,b];
        return;
    end
    
    if ( (a<=c) & (c<=b) & (b<=d) )
        res = [c,b];
        return;
    end
    
    if ( (c<=a) & (a<=d)& (d<=b))
        res = [a,d];
        return;
    end
    
    

end