clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [8000];

poly =2;
order = 2;
alpha =1;
count =1;

%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize
atX = '5p6';
fsw = 16;


fileName = 'Cy3l_Re500_';

for r = rates
    filenameFrPrint = strcat(fileName,int2str(order),'_5p6_',int2str(r));
    xfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pX_2D_Trail_Dyn_',int2str(r),'.txt' );
    yfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pY_2D_Trail_Dyn_',int2str(r),'.txt' );

    pVxfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx_2D_Trail_Dyn_',int2str(r),'.txt' );
    pUyfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy_2D_Trail_Dyn_',int2str(r),'.txt' );
    
    pVx1fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx1_2D_Trail_Dyn_',int2str(r),'.txt' );
    pUy1fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy1_2D_Trail_Dyn_',int2str(r),'.txt' );
    
    pVx2fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx2_2D_Trail_Dyn_',int2str(r),'.txt' );
    pUy2fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy2_2D_Trail_Dyn_',int2str(r),'.txt' );
    
    pVx3fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx3_2D_Trail_Dyn_',int2str(r),'.txt' );
    pUy3fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy3_2D_Trail_Dyn_',int2str(r),'.txt' );
    
    pVx4fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx4_2D_Trail_Dyn_',int2str(r),'.txt' );
    pUy4fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy4_2D_Trail_Dyn_',int2str(r),'.txt' );
    
    pVx_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx_p_2D_Trail_Dyn_',int2str(r),'.txt' );
    pUy_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy_p_2D_Trail_Dyn_',int2str(r),'.txt' );
    
    efname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pE_2D_Trail_Dyn_',int2str(r),'.txt' );

    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');

    pUyval = textread(pUyfname, '%f\t');
    pVxval = textread(pVxfname, '%f\t');
    
    pUy1val = textread(pUy1fname, '%f\t');
    pVx1val = textread(pVx1fname, '%f\t');
    pUy2val = textread(pUy2fname, '%f\t');
    pVx2val = textread(pVx2fname, '%f\t');
    pUy3val = textread(pUy3fname, '%f\t');
    pVx3val = textread(pVx3fname, '%f\t');
    pUy4val = textread(pUy4fname, '%f\t');
    pVx4val = textread(pVx4fname, '%f\t');
    
    pUy_pval = textread(pUy_pfname, '%f\t');
    pVx_pval = textread(pVx_pfname, '%f\t');

    elids = floor(textread(efname, '%f\t') +0.0001);
    
    % calculate vorticity
    pVortVal = pUyval-pVxval;
    pVort1Val = pUy1val-pVx1val;
    pVort2Val = pUy2val-pVx2val;
    pVort3Val = pUy3val-pVx3val;
    pVort4Val = pUy4val-pVx4val;
    pVort_pVal = pUy_pval-pVx_pval;
    
    figure(1)
    plot(ypos,pVortVal); hold on;
    plot(ypos,pVort4Val); hold on;
    plot(ypos,pVort_pVal); hold on;

    %
    [unique_ids,id_offset,id_tags] = unique(elids);
    graphHtMax = max(pVort_pVal)+0.01;
    graphHtMmin = min(pVort_pVal)-0.01;
    
    count =1;
     for tag = unique_ids'
        el_indices = find(elids== (zeros(size(elids))+tag) );
        el_x = xpos(el_indices);
        el_y = ypos(el_indices);
        
        el_s = pVortVal(el_indices);
        el_p = pVort_pVal(el_indices);
        el_snD = pVort4Val(el_indices);

        %remove first and last element in each element.
       % el_y = el_y(2:end-1);
       % el_s = el_s(2:end-1);
       % el_p = el_p(2:end-1);
        
        figure(3);
            hold on;
           plot(el_y,el_p,'LineWidth',lw-0.1,'MarkerSize',msz);
        %    title('DG');
           %plot([el_y(end), el_y(end)], [graphHtMmin,graphHtMax],'--k');
           xTTags(count) = el_y(end);
        figure(4); hold on;
           plot(el_y,el_s,'LineWidth',lw,'MarkerSize',msz);
         %  title('siac');
         %  plot([el_y(end), el_y(end)], [graphHtMmin,graphHtMax],'--k');
          
         figure(5); hold on;
         plot(el_y,el_snD,'LineWidth',lw,'MarkerSize',msz);
           
        figure(6); hold on;
            plot(el_y,el_p,'-b','LineWidth',lw,'MarkerSize',msz);
           plot(el_y,el_s,'-r','LineWidth',lw,'MarkerSize',msz);
           plot(el_y,el_snD,'-g','LineWidth',lw,'MarkerSize',msz);
           % plot([el_y(end), el_y(end)], [graphHtMmin,graphHtMax],'--k');
            
%         figure(3); hold on; caxis([-1,1])
%            plot(el_x,el_s,'LineWidth',lw,'MarkerSize',msz);
        count = count +1;
     end
     
     maxVor = max( [ max(pVort_pVal),max(pVortVal),max(pVort4Val)]);
     minVor = min( [ min(pVort_pVal),min(pVortVal),min(pVort4Val)]);

     
     figure(3)
     axis on; 
     fig = gcf;
     set(gca,'XTick',sort(xTTags))
     set(gca,'XTickLabel','')
     ylabel('Vorticity');
%     axis([min(ypos) max(ypos) min(pVort_pVal) max(pVort_pVal)])
     axis([min(ypos) max(ypos) minVor maxVor]);
ax = gca;
            ax.FontSize = fsw;
     print(strcat(filenameFrPrint,'_DG'), '-dpng','-r500');
      
     minVor = -0.5;
     
     figure(4)
     axis on; 
     fig = gcf;
     set(gca,'XTick',sort(xTTags))
     set(gca,'XTickLabel','')
     ylabel('Vorticity');
%     axis([min(ypos) max(ypos) min(pVortVal) max(pVortVal)]);
     axis([min(ypos) max(ypos) minVor maxVor]);
                 ax = gca;
            ax.FontSize = fsw;
     print(strcat(filenameFrPrint,'_LSIAC'), '-dpng','-r500');

     figure(5)
     axis on; 
     fig = gcf;
     set(gca,'XTick',sort(xTTags))
     set(gca,'XTickLabel','')
     ylabel('Vorticity');
%     axis([min(ypos) max(ypos) min(pVort4Val) max(pVort4Val)]);
                      ax = gca;
            ax.FontSize = fsw;
     axis([min(ypos) max(ypos) minVor maxVor]);
     print(strcat(filenameFrPrint,'_LSIAC_adap'), '-dpng','-r500');
     
    disp(max(pVortVal))
end

