#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include <boost/timer.hpp>


#include <iomanip> // std::setprecision
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
#define TOLERENCE_VIZ 1e-6

using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		cout << "4th no of quad Points for integration."<< endl;
		return 0;
	}


	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM2D->LoadMesh(var[0]);
	HNM2D->LoadMesh(var[1]);
	HNM2D->LoadMesh(var[2]);
	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);
//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);

	for (int i=0;  i< tNquadPts; i++)
	{
//		fce[i] = xc0[i];
//		fce[i] = std::cos(2.0*M_PI*(xc0[i]));
		fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]));
		//fce[i] = std::cos(2.0*M_PI*(xc0[i]))*std::cos(2.0*M_PI*(xc1[i]));
	}
	
	HNM2D->m_expansions[0]->FwdTrans(fce,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	ece = HNM2D->m_expansions[0]->GetPhys();
	HNM2D->m_Arrays.push_back(ece);
	HNM2D->LoadExpListIntoRTree();


	Array<OneD,NekDouble> ece_Coeffs =  HNM2D->m_expansions[0]->GetCoeffs();

	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]) );

	Array<OneD,NekDouble> directionL(3,0.0);
	directionL[0] = 1.0;
	vector<NekDouble> stPoint(3), endPoint(3);
	vector<int>  Gids,Eids;
	vector<NekDouble> HvalT; 
	stPoint[0] = 0.0; stPoint[1] = 0.454545; stPoint[2] = 0.0;
	//stPoint[0] = 0.0; stPoint[1] = 0.0; stPoint[2] = 0.0;
	Array<OneD,NekDouble> t_LineElm;
	
	sm.SetupLineForLSIAC(directionL, stPoint, 0.0,1.0, atoi(argv[4]) , HvalT, Gids, Eids, t_LineElm); 
	Array<OneD,NekDouble> tv_LineElm( t_LineElm.num_elements());
	//HNM2D-> m_Arrays.push_back(ece);
	sm.GetVLineForLSIAC(atoi(argv[4]), stPoint, directionL, HvalT, Gids,Eids, t_LineElm, tv_LineElm, 0);	

	vector<NekDouble> tparams, tvals;
	vector<NekDouble> tvals4, tvals7, tvals10;
	for ( NekDouble t =0.0; t<= 1.0001; t=t+0.001)
	{
		tparams.push_back(t);
	}


	boost::timer tim1;
	sm.EvaluateUsingLineAt( stPoint, directionL, 4, atof(argv[3]),tparams, tvals4, 0);
	double s1 = tim1.elapsed();
	std::cout << "speed-LSIAC time taken to evaluate at "<< tparams.size() << "is "<< s1 << " seconds"<< endl;

	boost::timer tim12;
//	sm.EvaluateUsingLineAt( stPoint, directionL, 7, atof(argv[3]),tparams, tvals7, 0);
	sm.EvaluateUsingLineAt_v3( stPoint, directionL, 4,10 , atof(argv[3]),tparams, tvals7, 0);
	double s2 = tim1.elapsed();
	std::cout << "speed-LSIAC time taken to evaluate at "<< tparams.size() << "is "<< s2 << " seconds"<< tvals7.size()<< endl;

	boost::timer tim3;
	sm.EvaluateUsingLineAt( stPoint, directionL, 10, atof(argv[3]),tparams, tvals10, 0);
	double s3 = tim1.elapsed();
	std::cout << "speed-LSIAC time taken to evaluate at "<< tparams.size() << "is "<< s3 << " seconds"<< endl;
	
	sm.EvaluateUsingLineAt( stPoint, directionL, atoi(argv[4]), atof(argv[3]),tparams, tvals, 0);

	vector<NekDouble> tparamsS(tparams.size());
	NekDouble valY, valZ;
// Need to print LSIAC solution also here.
	
	boost::timer tim;
	for(int t =0; t< tparams.size();t++)
	{
		sm.EvaluateAt(tparams[t],0.454545,0.0,tparamsS[t], valY,valZ, directionL, atof(argv[3]),0);
		//sm.EvaluateAt(tparams[t],0.0,0.0,tparamsS[t], valY,valZ, directionL, atof(argv[3]),0);
	}
	double sf = tim.elapsed();
	std::cout << "LSIAC time taken to evaluate at "<< tparams.size() << "is "<< sf << " seconds"<< endl;

	string fsname = "test";	
	NektarBaseClass k;
//	k.printNekArray(tv_LineElm,0.0);
	k.writeNekArray(t_LineElm,"test_LineX.txt");
	k.writeNekArray(tv_LineElm,"test_Line_ValTest.txt");
	k.writeNekArray(tparams,"test_params.txt");
	k.writeNekArray(tvals,fsname+"_vals"+argv[4]+".txt");
	k.writeNekArray(tvals4,fsname+"_vals4.txt");
	k.writeNekArray(tvals7,fsname+"_vals7.txt");
	k.writeNekArray(tvals10,fsname+"_vals10.txt");
	k.writeNekArray(tparamsS,fsname+"_paramsS.txt");





/*
	k.writeNekArray(pE,fname+"_"+argv[2]+"_pE_2D_OneSided2kp1_EL.txt");
	k.writeNekArray(pX,fname+"_"+argv[2]+"_pX_2D_OneSided2kp1_EL.txt");
	k.writeNekArray(pY,fname+"_"+argv[2]+"_pY_2D_OneSided2kp1_EL.txt");
	k.writeNekArray(pV,fname+"_"+argv[2]+"_pV_2D_OneSided2kp1_EL.txt");
	k.writeNekArray(pP,fname+"_"+argv[2]+"_pP_2D_OneSided2kp1_EL.txt");
	k.writeNekArray(pS,fname+"_"+argv[2]+"_pS_2D_OneSided2kp1_EL.txt");
*/
	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
