%function [y dy]=critfunctionEval(x)
function [val, dval,ret]=critSubEval(x,y,TR, Tri,xvSorted,yvSorted, uvSorted,uxvSorted,uyvSorted,vvSorted, vxvSorted, vyvSorted )

%Example function to try out Newton?s Method
    %
%     n=length(x);
%     y=zeros(size(x)); %Not necessary for a small vector
%     dy=zeros(n,n); %Not necessary for a small matrix
%     y(1)=-x(1)^3+x(2);
%     y(2)=x(1)^2+x(2)^2-1;
%     dy(1,1)=-3*x(1)^2; dy(1,2)=1;
%     dy(2,1)=2*x(1); dy(2,2)=2*x(2);
    
    dval = zeros(2,2);
    val = zeros(2,1);
    
    %check for out of bounds.
    
    minXlimit = min(xvSorted(:));
    maxXlimit = max(xvSorted(:));

    minYlimit = min(yvSorted(:));
    maxYlimit = max(yvSorted(:));
    ret = 1;
    if ( x < minXlimit || x>maxXlimit)
        disp(x);
        disp(y);
        disp('out x of bounds');
        ret =0;
        return;
    end
    
    if ( y < minYlimit || y>maxYlimit)
        disp(x);
        disp(y);
        disp('out y of bounds');
        ret =0;
        return
    end
    
    tid = pointLocation(TR,x,y);
    if( isnan(tid) )
        numTri = max(size(Tri));
        for t = 1:numTri
            in = inpolygon(x,y, xvSorted(Tri(t,:)),yvSorted(Tri(t,:)));
            if in
                tid =t;
            end
        end
    end
    
    P1 = [ xvSorted(Tri(tid,1)); yvSorted(Tri(tid,1)); uvSorted(Tri(tid,1)) ];
    P2 = [ xvSorted(Tri(tid,2)); yvSorted(Tri(tid,2)); uvSorted(Tri(tid,2)) ];
    P3 = [ xvSorted(Tri(tid,3)); yvSorted(Tri(tid,3)); uvSorted(Tri(tid,3)) ];
    Cp = cross(P3-P1,P2-P1);
    d = dot(Cp,P1);
    val(1) = ( d-x*Cp(1) -y*Cp(2) )/Cp(3);
    
    
    P1 = [ xvSorted(Tri(tid,1)); yvSorted(Tri(tid,1)); vvSorted(Tri(tid,1)) ];
    P2 = [ xvSorted(Tri(tid,2)); yvSorted(Tri(tid,2)); vvSorted(Tri(tid,2)) ];
    P3 = [ xvSorted(Tri(tid,3)); yvSorted(Tri(tid,3)); vvSorted(Tri(tid,3)) ];
    Cp = cross(P3-P1,P2-P1);
    d = dot(Cp,P1);
    val(2) = ( d-x*Cp(1) -y*Cp(2) )/Cp(3);

    
    
    P1 = [ xvSorted(Tri(tid,1)); yvSorted(Tri(tid,1)); uxvSorted(Tri(tid,1)) ];
    P2 = [ xvSorted(Tri(tid,2)); yvSorted(Tri(tid,2)); uxvSorted(Tri(tid,2)) ];
    P3 = [ xvSorted(Tri(tid,3)); yvSorted(Tri(tid,3)); uxvSorted(Tri(tid,3)) ];
    Cp = cross(P3-P1,P2-P1);
    d = dot(Cp,P1);
    dval(1,1) = ( d-x*Cp(1) -y*Cp(2) )/Cp(3);
    
    
    P1 = [ xvSorted(Tri(tid,1)); yvSorted(Tri(tid,1)); uyvSorted(Tri(tid,1)) ];
    P2 = [ xvSorted(Tri(tid,2)); yvSorted(Tri(tid,2)); uyvSorted(Tri(tid,2)) ];
    P3 = [ xvSorted(Tri(tid,3)); yvSorted(Tri(tid,3)); uyvSorted(Tri(tid,3)) ];
    Cp = cross(P3-P1,P2-P1);
    d = dot(Cp,P1);
    dval(1,2) = ( d-x*Cp(1) -y*Cp(2) )/Cp(3);
    
    P1 = [ xvSorted(Tri(tid,1)); yvSorted(Tri(tid,1)); vxvSorted(Tri(tid,1)) ];
    P2 = [ xvSorted(Tri(tid,2)); yvSorted(Tri(tid,2)); vxvSorted(Tri(tid,2)) ];
    P3 = [ xvSorted(Tri(tid,3)); yvSorted(Tri(tid,3)); vxvSorted(Tri(tid,3)) ];
    Cp = cross(P3-P1,P2-P1);
    d = dot(Cp,P1);
    dval(2,1) = ( d-x*Cp(1) -y*Cp(2) )/Cp(3);
    
    P1 = [ xvSorted(Tri(tid,1)); yvSorted(Tri(tid,1)); vyvSorted(Tri(tid,1)) ];
    P2 = [ xvSorted(Tri(tid,2)); yvSorted(Tri(tid,2)); vyvSorted(Tri(tid,2)) ];
    P3 = [ xvSorted(Tri(tid,3)); yvSorted(Tri(tid,3)); vyvSorted(Tri(tid,3)) ];
    Cp = cross(P3-P1,P2-P1);
    d = dot(Cp,P1);
    dval(2,2) = ( d-x*Cp(1) -y*Cp(2) )/Cp(3);
    
    
    
    
    
%    val(1) = interp2(xV,yV,uV,x,y);
%    val(2) = interp2(xV,yV,vV,x,y);

%    dval(1,1) = interp2(xV,yV,uxV,x,y);
%    dval(1,2) = interp2(xV,yV,uyV,x,y);
%    dval(2,1) = interp2(xV,yV,vxV,x,y);
%    dval(2,2) = interp2(xV,yV,vyV,x,y);

end