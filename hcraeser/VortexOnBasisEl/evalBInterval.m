% test Bernstein function.

function res = evalBInterval(coeff,X)

    switch nargin
        case 0,
    end
    
    [~,sc] = size(coeff);
    n = sc-1;
    res = B_eval(n,coeff,X);
    
    
    function rest = B_eval(n,coeff,X)
       count =1;
       rest = zeros(size(X));
       for k = 0:n
           r = coeff(count)*B(n,k,X);
           if ( (X(1) < (2*k/n-1)) && (X(2) > (2*k/n-1)) )
                   r(2) = coeff(count)*B(n,k,2*k/n-1); 
           end
           rest = rest  + r;
           count= count +1;          
       end 
    end

    function [res] = B(n,k,data)
        % assumed data varies from -1 to 1
        if n<k || k<0
            res = zeros(size(data));
            return
        end
        data = (data+1)/2;
        res = nchoosek(n,k)*((data.^k).*((1-data).^(n-k)));
    end

    function [res] = B_der(n,k,data)
        res = n*(B(n-1,k-1,data) - B(n-1,k,data))/2;
    end
    
    function [rest] = B_der_eval(n1,coeff,dataIn)
       count =1;
       [~,b] = size(dataIn);
       rest = zeros(a,b);
       for k1 = 0:n1
           rest(:) = rest(:)  + coeff(count)*B_der(n1,k1,dataIn);
           count= count +1;          
       end
    end

end

% test case to plot Berns and Berns Derivative.
%     data = (-1:.01:1);
%     n = 8;
%     figure(1);
%     for k = 0:n
%         y = B_der(n,k,data);
%         plot(data,y); hold on;
%     end
%     hold off;
%     
%     figure(2);
%     for k = 0:n
%         y = B(n,k,data);
%         plot(data,y); hold on;
%     end
%     hold on;
%     
%     data = (-1:2*(1/n):1);
%     y = 0.3*ones(size(data));
%     plot(data,y,'r*');
%     hold off.
