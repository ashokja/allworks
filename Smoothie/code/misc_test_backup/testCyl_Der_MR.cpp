#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 7)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		cout << "4th arg is derivative" << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();

	HNM2D->LoadData( fname+ ".chk",var);
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}

	// Loaded u,v,p from file. Work on them.
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
                                        HNM2D->m_expansions[0]->UpdatePhys());

    HNM2D->m_expansions[1]->BwdTrans( HNM2D->m_expansions[1]->GetCoeffs(),
                                        HNM2D->m_expansions[1]->UpdatePhys());
    
    HNM2D->m_expansions[2]->BwdTrans( HNM2D->m_expansions[2]->GetCoeffs(),
                                        HNM2D->m_expansions[2]->UpdatePhys());

	Array<OneD,NekDouble> u_phys, v_phys, p_phys;
	Array<OneD,NekDouble> udx_phys(tNquadPts), udy_phys(tNquadPts);
	Array<OneD,NekDouble> vdx_phys(tNquadPts), vdy_phys(tNquadPts);
	Array<OneD,NekDouble> pdx_phys(tNquadPts), pdy_phys(tNquadPts);
	
	u_phys = HNM2D->m_expansions[0]->GetPhys();
	v_phys = HNM2D->m_expansions[1]->GetPhys();
	p_phys = HNM2D->m_expansions[2]->GetPhys();

	//u deriv
	HNM2D->m_expansions[0]->PhysDeriv(0,u_phys,udx_phys);
	HNM2D->m_expansions[0]->PhysDeriv(1,u_phys,udy_phys);
	//v deriv
	HNM2D->m_expansions[1]->PhysDeriv(0,v_phys,vdx_phys);
	HNM2D->m_expansions[1]->PhysDeriv(1,v_phys,vdy_phys);
	//p deriv
	HNM2D->m_expansions[2]->PhysDeriv(0,p_phys,pdx_phys);
	HNM2D->m_expansions[2]->PhysDeriv(1,p_phys,pdy_phys);



// Evaluate on a new equal space grid mesh.
	int gPtsX = atoi(argv[5]);
	int gPtsY = atoi(argv[6]);
	int Nx = gPtsX, Ny=gPtsY;
	int totPts = Nx*Ny;
	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0);
	NekDouble StartX = 2.5, StartY = -4;	
	NekDouble EndX = 10.5, EndY = 4;	
	SmoothieSIAC2D sm(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]), atoi(argv[4])); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	Array<OneD,NekDouble> directionX(3,0.0), coord(3,0.0), directionY(3,0.0) ;
	directionX[0] = 1.0; directionY[1] =1.0;
	Array<OneD,NekDouble> pX(totPts), pY(totPts);
	Array<OneD,NekDouble> pU(totPts), pV(totPts), pP(totPts);
	Array<OneD,NekDouble> pUx(totPts), pVx(totPts), pPx(totPts);
	Array<OneD,NekDouble> pUy(totPts), pVy(totPts), pPy(totPts);
	Array<OneD,NekDouble> pU_p(totPts), pV_p(totPts), pP_p(totPts);
	Array<OneD,NekDouble> pUx_p(totPts), pVx_p(totPts), pPx_p(totPts);
	Array<OneD,NekDouble> pUy_p(totPts), pVy_p(totPts), pPy_p(totPts);

	for( int i =0; i < Nx; i++)
	{
		for(int j=0; j< Ny; j++)
		{
			int index = i*Nx+j;
			pX[index] = StartX + i*sx*(EndX-StartX);
			pY[index] = StartY + j*sy*(EndY-StartY);
			sm.EvaluateAt(pX[index],pY[index],0.0,pUx[index],valY,valZ,directionX, atof(argv[3]),0);
			sm.EvaluateAt(pX[index],pY[index],0.0,pUy[index],valY,valZ,directionY, atof(argv[3]),0);

			sm.EvaluateAt(pX[index],pY[index],0.0,pVx[index],valY,valZ,directionX, atof(argv[3]),1);
			sm.EvaluateAt(pX[index],pY[index],0.0,pVy[index],valY,valZ,directionY, atof(argv[3]),1);
			
			sm.EvaluateAt(pX[index],pY[index],0.0,pPx[index],valY,valZ,directionX, atof(argv[3]),2);
			sm.EvaluateAt(pX[index],pY[index],0.0,pPy[index],valY,valZ,directionY, atof(argv[3]),2);

			coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = 0.0;

			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(coord);
			int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elId);

			const Array<OneD,NekDouble> el_u_phys = u_phys.CreateWithOffset(u_phys,coeffOffset);
			const Array<OneD,NekDouble> el_v_phys = v_phys.CreateWithOffset(v_phys,coeffOffset);
			const Array<OneD,NekDouble> el_p_phys = p_phys.CreateWithOffset(p_phys,coeffOffset);

			const Array<OneD,NekDouble> el_udx_phys = udx_phys.CreateWithOffset(udx_phys,coeffOffset);
			const Array<OneD,NekDouble> el_vdx_phys = vdx_phys.CreateWithOffset(vdx_phys,coeffOffset);
			const Array<OneD,NekDouble> el_pdx_phys = pdx_phys.CreateWithOffset(pdx_phys,coeffOffset);

			const Array<OneD,NekDouble> el_udy_phys = udy_phys.CreateWithOffset(udy_phys,coeffOffset);
			const Array<OneD,NekDouble> el_vdy_phys = vdy_phys.CreateWithOffset(vdy_phys,coeffOffset);
			const Array<OneD,NekDouble> el_pdy_phys = pdy_phys.CreateWithOffset(pdy_phys,coeffOffset);

			pU_p[index] = lexp->PhysEvaluate(coord,el_u_phys);
			pV_p[index] = lexp->PhysEvaluate(coord,el_v_phys);
			pP_p[index] = lexp->PhysEvaluate(coord,el_p_phys);
			
			pUx_p[index] = lexp->PhysEvaluate(coord,el_udx_phys);
			pVx_p[index] = lexp->PhysEvaluate(coord,el_vdx_phys);
			pPx_p[index] = lexp->PhysEvaluate(coord,el_pdx_phys);

			pUy_p[index] = lexp->PhysEvaluate(coord,el_udy_phys);
			pVy_p[index] = lexp->PhysEvaluate(coord,el_vdy_phys);
			pPy_p[index] = lexp->PhysEvaluate(coord,el_pdy_phys);

			//cout << j << endl;
		}
		cout << i << endl;
	}

	NektarBaseClass k;
	k.writeNekArray(pX,fname+"_"+argv[2]+"_D_"+argv[4]+"_pX_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pY,fname+"_"+argv[2]+"_D_"+argv[4]+"_pY_2D_OneSided2kp1"+argv[6]+".txt");

	k.writeNekArray(pU,fname+"_"+argv[2]+"_D_"+argv[4]+"_pU_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pV,fname+"_"+argv[2]+"_D_"+argv[4]+"_pV_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pP,fname+"_"+argv[2]+"_D_"+argv[4]+"_pP_2D_OneSided2kp1"+argv[6]+".txt");

	k.writeNekArray(pUx,fname+"_"+argv[2]+"_D_"+argv[4]+"_pUx_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pVx,fname+"_"+argv[2]+"_D_"+argv[4]+"_pVx_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pPx,fname+"_"+argv[2]+"_D_"+argv[4]+"_pPx_2D_OneSided2kp1"+argv[6]+".txt");

	k.writeNekArray(pUy,fname+"_"+argv[2]+"_D_"+argv[4]+"_pUy_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pVy,fname+"_"+argv[2]+"_D_"+argv[4]+"_pVy_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pPy,fname+"_"+argv[2]+"_D_"+argv[4]+"_pPy_2D_OneSided2kp1"+argv[6]+".txt");

	k.writeNekArray(pU_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pU_p_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pV_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pV_p_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pP_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pP_p_2D_OneSided2kp1"+argv[6]+".txt");

	k.writeNekArray(pUx_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pUx_p_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pVx_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pVx_p_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pPx_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pPx_p_2D_OneSided2kp1"+argv[6]+".txt");

	k.writeNekArray(pUy_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pUy_p_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pVy_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pVy_p_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pPy_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pPy_p_2D_OneSided2kp1"+argv[6]+".txt");

	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
