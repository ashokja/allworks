function coeff = SIAC_coeff_XLi(k,T,lambda)

    switch nargin
         case 0
%            %T = [-2,-1,0;-1,0,1;0,1,2]-1;
%             T = [-4,-3,-2;-3,-2,-1;-2,-1,0;-1,0,0];
%             k =1;
            lambda =0.0;
            T = [-7:-4;-6:-3;-5:-2;-4:-1;-3:0;-1,0,0,0];
            k=2;
%             lambda=0.0;
        case 1
            k=1;
    end
    T
    addpath('../PolyLib');
    % r and w for integration.
    
    r = 2*k;
    [L,R] = size(T);
    % if L = 2*k+2 it is L/R filter.
    % if L = 2*k+1 it is a SYM filter.
    
    bSplKnots = T(1,1):1:T(end,end);
    
    rSize = r+2;
    
    linM = zeros(L,L);
    for row = 0:1:L-1
        for col = 0:1:L-1
%            gamma =col;
%            x_g = -r/2+gamma+lambda;
            for i = 0:1:max(size(bSplKnots))-2
                bGamma = [ bSplKnots(i+1),bSplKnots(i+1+1)];
                dI = k+1+row;
                no_s = ceil((dI+3)/2);
                [sPts,w] = JacobiGZW(no_s,0,0);
                xPts = bGamma(1) + (bGamma(2)-bGamma(1))*(sPts+1)/2;
                integral = sum(BSpl(xPts,k,0,T(col+1,:)).*((xPts).^row).*w.*abs(bGamma(2)-bGamma(1))/2);
                linM(row+1,col+1) = linM(row+1,col+1)+integral;
            end        
        end
    end
    linM
%     linM
%     rhs = zeros(1,rSize);rhs(1) =1;
%     coeff = linM\rhs'
%     
%     rSize = r+1;
%     cSize = r+1;
%     linM = zeros(rSize,cSize);
%     bSplKnots = -(k+1)/2:1:(k+1)/2;
%     for row = 0:1:rSize-1
%         for c = 0:1:cSize-1
%             gamma = c;
%             x_g = -r/2+gamma+lambda;
%             for i =0:1:max(size(bSplKnots))-2
%                 bGamma = [ bSplKnots(i+1),bSplKnots(i+1+1)];
%                 % s,w for integration.
%                 % degree for integration is k+r
%                 dI = k+1+row;
%                 no_s = ceil(((dI)+3)/2);
%                 [sPts,w] = JacobiGZW(no_s,0,0);
%                 xPts = bGamma(1) + (bGamma(2)-bGamma(1))*(sPts+1)/2;
%                 integral = sum(BSpl_central(xPts,k).*((xPts+x_g).^row).*w*abs(bGamma(2)-bGamma(1))/2);
%                 linM(row+1,c+1) = linM(row+1,c+1)+ integral;
%             end
%         end
%     end
    
    rhs = zeros(1,L);rhs(1) =1;
    coeff = linM\rhs';
    disp('right hand side coeff');
    disp(linM*coeff);
    disp('coeff');
    disp(coeff');
    disp('integral');

    %linM
    %rhs
    %coeff
    

end