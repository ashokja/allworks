% test to plot condition number of Bernstein function.
function rest =  plotCondNumber(i,j)

    switch nargin
        case 0,
            %n = 5;
            i = 3; j = 17; 
    end
    
    % Bernstein;
    rest = zeros(j-i+1,1);
    for it = i:j
        dataIn = JacobiGL(0,0,it);
        count = 1;
        mat = zeros(it+1,it+1);
        for k = 0:it
            mat(:,count) = Berns(it,k,dataIn);
            count = count +1;
        end
        rest(it-i+1) = cond(mat);
    end

    figure (1),
    plot((i:j),rest,'-*');
    title('Bernstein 1D');
    xlabel('polynomail order');
    ylabel('Condition number');

    % Monomials;
    rest = zeros(j-i+1,1);
    for it = i:j
        dataIn = JacobiGL(0,0,it);
        count = 1;
        mat = zeros(it+1,it+1);
        for k = 0:it
            mat(:,count) = Monom(it,k,dataIn);
            count = count +1;
        end
        rest(it-i+1) = cond(mat);
    end
    
    figure (2),
    plot((i:j),rest,'r-*');
    title('Monomail 1D');
    xlabel('polynomail order');
    ylabel('Condition number');

    
    
    
    % Bernstien at a data point.
    function [res] = Berns(n,k,data)
            % assumed data varies from -1 to 1
            if n<k || k<0
                res = zeros(size(data));
                return
            end
            data = (data+1)/2;
            res = nchoosek(n,k)*((data.^k).*((1-data).^(n-k)));
    end


    function [res] = Monom(n,k,data)
        data = (data+1)/2;
        res = data.^k;
    end

end
