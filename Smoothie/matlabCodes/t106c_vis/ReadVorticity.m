clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20];

%abc = '../../data/UnsTriMeshes/NM2_unsMeshvar1_0125_2_R_100_pX_2DDyn.txt';

R = 50;
poly =2;
order = poly;
count =1;
%prefixFilename = 'data/P4_triSplit6_L4_R1_';
%prefixFilename = 'data/P2_triSpli_L2_R1_';
%prefixName = '../../data/UnsTriMeshes_Ashok/';
%prefixName = '../../data/Diff_NPUTriMesh/';
%prefixName = '../../data/sQuadDyn/';

%prefixName = '../../data/NPUTriMesh_proj/';
%prefixName = '../../data/NUTriMesh_proj/';
%prefixName = '../../data/UTriMesh_proj/';
prefixName = '../../data/t106c/';

width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
fsw = 20;
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize

%for poly = [2,3,4]

            FileName = strcat('t106c_');
            prefixFilename = strcat( prefixName,FileName,int2str(poly),'_RSfld_',int2str(R),'_Param_0');

            xfname = strcat(prefixFilename,'_pX_2DDyn.txt' );
            yfname = strcat(prefixFilename,'_pY_2DDyn.txt' );

            Uypfname = strcat(prefixFilename,'_pUy_P_2DDyn.txt' );
            Uysfname = strcat(prefixFilename,'_pUy_S_2DDyn.txt' );
            UysDynfname = strcat(prefixFilename,'_pUy_SDyn_2DDyn.txt' );
            
            Vxpfname = strcat(prefixFilename,'_pVx_P_2DDyn.txt' );
            Vxsfname = strcat(prefixFilename,'_pVx_S_2DDyn.txt' );
            VxsDynfname = strcat(prefixFilename,'_pVx_SDyn_2DDyn.txt' );
            
%            ffname = strcat(prefixFilename,'_pP_2DDyn.txt' );
%            pfname = strcat(prefixFilename,'_pP_2DDyn.txt' );
%            sfname = strcat(prefixFilename,'_pS_2DDyn.txt' );
            

            sDynfname = strcat(prefixFilename,'_pSDyn_2DDyn.txt' );
            dynsfname = strcat(prefixFilename,'_pDyn_2DDyn.txt' );
            tfname = strcat(prefixFilename,'_pT_2DDyn.txt' );
            tDynfname = strcat(prefixFilename,'_pTDyn_2DDyn.txt' );
            
            
            

            xpos = textread(xfname, '%f\t');
            ypos = textread(yfname, '%f\t');
%            fpos = textread(ffname, '%f\t');

            Vxppos = textread(Vxpfname, '%f\t');
            Vxspos = textread(Vxsfname, '%f\t');
            VxsDynpos = textread(VxsDynfname, '%f\t');

            Uyppos = textread(Uypfname, '%f\t');
            Uyspos = textread(Uysfname, '%f\t');
            UysDynpos = textread(UysDynfname, '%f\t');

            
            dynspos = textread(dynsfname, '%f\t');
 %           tpos = textread(tfname, '%f\t');
 %           tDynpos = textread(tDynfname, '%f\t');
            
            
            xVpos = reshape(xpos,[R,R]);
            yVpos = reshape(ypos,[R,R]);
            
            VxpVpos = reshape(Vxppos,[R,R]);
            VxsVpos = reshape(Vxspos,[R,R]);
            VxsDynVpos = reshape(VxsDynpos,[R,R]);
            
            UypVpos = reshape(Uyppos,[R,R]);
            UysVpos = reshape(Uyspos,[R,R]);
            UysDynVpos = reshape(UysDynpos,[R,R]);
            
            dynsVpos = reshape(dynspos,[R,R]);
   %         tVpos = reshape(tpos,[R,R]);
    %        tDynVpos = reshape(tDynpos,[R,R]);

            %Vorticity
            pVpos = VxpVpos - UypVpos;
            sVpos = VxsVpos - UysVpos;
            sDynVpos = VxsDynVpos - UysDynVpos;
            
            figure(1),
            surf(xVpos,yVpos,pVpos);
            title('Simulation Result');
            
            figure(2),
            surf(xVpos,yVpos,sVpos);
            title('L-SIAC filter');
            
            figure(3),
            surf(xVpos,yVpos,sDynVpos);
            title('L-SIAC Dyn Filter');
            
            figure(4),
            subplot(1,3,1),
            contourf(xVpos,yVpos,pVpos);
            subplot(1,3,2),
            contourf(xVpos,yVpos,sVpos);
            subplot(1,3,3),
            contourf(xVpos,yVpos,sDynVpos);
            
                figure(5)
                surf(dynsVpos);
%end
