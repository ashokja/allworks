%Create image for mesh resolution.


close all
clear;

figure(1),
plotNekMesh()

%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 2;      % LineWidth
msz = 8;       % MarkerSize
atX = '5p8';



%%
axis equal

%% draw lines to indicate box for all our solutions.
xmin = 3.5;
xmax = 9.5;
ymin = -3;
ymax = 3;

line([xmin,xmin], [ymin,ymax],[10,10],'Color','r','LineStyle','-','LineWidth',lw); hold on; 

line([xmax,xmax], [ymin,ymax],[10,10],'Color','r','LineStyle','-','LineWidth',lw); hold on; 

line([xmin,xmax], [ymin,ymin],[10,10],'Color','r','LineStyle','-','LineWidth',lw); hold on; 

line([xmin,xmax], [ymax,ymax],[10,10],'Color','r','LineStyle','-','LineWidth',lw); hold on; 

grid off;
axis off;

%%
print(strcat('../paperUseImages/','NekMeshWithOverlap_2'), '-dpng','-r500');


%line([xmax,ymin], [xmax,ymax],[10,10],'Color','r','LineStyle','-','LineWidth',lw); hold on; 

%line([xmin,ymin], [xmax,ymin],[10,10],'Color','r','LineStyle','-','LineWidth',lw); hold on; 

%line([xmin,ymax], [xmax,ymax],[10,10],'Color','r','LineStyle','-','LineWidth',lw); hold on; 






