% Only create field files.

clear;
close all;
%field one;
a1 = 0.74+0.35i;
a2 = 0.68-0.59i;
a3 =-0.11-0.72i;
b1 =-0.58+0.64i;
b2 = 0.51-0.27i;
b3 =-0.12+0.84i;

%  %field two;
% a1 = 0.74+0.35i;
% a2 =-0.18-0.19i;
% a3 =-0.11-0.72i;
% b1 =-0.58+0.64i;
% b2 = 0.51-0.27i;
% %b3 =-0.12+0.84i;


%rate = 3;
%samplerate = 6;
%r = [2,3,4];
%samples= [6,11,21,41,81,161];
samples= [101];

for samplerate = samples
        s = linspace(0,1,samplerate);
        [xv,yv] = meshgrid(s);
        %yV = linspace(0,1,samplerate);
        xV = xv(:);
        yV = yv(:);

        xV = (2.0*xV-1).*2;
        yV = (2.0*yV-1).*2;
        
        zV = xV+yV*1i;
        zVb = xV-yV*1i;

        %
        Val = (zV-a1).*(zV-a2).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2;
        % Val = (zV-a1).*(zVb-a2).*(zV-a3).*(zVb-b1).*(zVb-b2);

        Val_x = (1).*(zV-a2).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2+ ...
                (zV-a1).*(1).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2+ ...
                (zV-a1).*(zV-a2).*(1).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2+...
                (zV-a1).*(zV-a2).*(zV-a3).*(1).*(zVb-b2).*(zVb-b3).^2+...
                (zV-a1).*(zV-a2).*(zV-a3).*(zVb-b1).*(1).*(zVb-b3).^2+...
                (2).*(zV-a1).*(zV-a2).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3);

        Val_y = (1i).*(zV-a2).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2+ ...
                (zV-a1).*(1i).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2+ ...
                (zV-a1).*(zV-a2).*(1i).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2+...
                (zV-a1).*(zV-a2).*(zV-a3).*(-1i).*(zVb-b2).*(zVb-b3).^2+...
                (zV-a1).*(zV-a2).*(zV-a3).*(zVb-b1).*(-1i).*(zVb-b3).^2+...
                (-2*1i).*(zV-a1).*(zV-a2).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3);
            
        ux = real(Val_x);
        vx = -1*imag(Val_x);
        uy = real(Val_y);
        vy = -1*imag(Val_y);
       u = real(Val); 
       v = -1*imag(Val);        
%         u = sin(2*pi*(xV+yV));
%         v = -1*cos(2*pi*(xV+yV));
%         ux = 2*pi*cos(2*pi*(xV+yV));
%         uy = 2*pi*cos(2*pi*(xV+yV));
%         vx = 2*pi*sin(2*pi*(xV+yV));
%         vy = 2*pi*sin(2*pi*(xV+yV));

        n_uV = sqrt(u.*u+v.*v);
        nu = u./n_uV;
        nv = v./n_uV;
        
        figure(1),
        quiver(xV,yV,nu,nv);

        
       dlmwrite(strcat('data/SampleRate_',int2str(samplerate),'_u.txt'),u','delimiter','\t','precision',25);
       dlmwrite(strcat('data/SampleRate_',int2str(samplerate),'_v.txt'),v','delimiter','\t','precision',25);
       dlmwrite(strcat('data/SampleRate_',int2str(samplerate),'_ux.txt'),ux','delimiter','\t','precision',25);
       dlmwrite(strcat('data/SampleRate_',int2str(samplerate),'_vx.txt'),vx','delimiter','\t','precision',25);
       dlmwrite(strcat('data/SampleRate_',int2str(samplerate),'_uy.txt'),uy','delimiter','\t','precision',25);
       dlmwrite(strcat('data/SampleRate_',int2str(samplerate),'_vy.txt'),vy','delimiter','\t','precision',25);


end
%%
% n_uv = sqrt(u.*u+v.*v);
% u = u./n_uv;
% v = v./n_uv;
% quiver(xV,yV,u,v,0.50);







%plot3(xV,yV,u);

