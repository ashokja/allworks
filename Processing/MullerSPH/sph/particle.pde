class particle {
  // Data variables.
  PVector mp;
  PVector m_ref ; //= new PVector(0.0,0.0,0.0);
  PVector m_vel = new PVector(0.0,0.0,0.0);
  PVector m_for = new PVector(0.0,0.0,0.0);
  
  // neigh
  ArrayList<Integer> neigh = new ArrayList<Integer> ();
  ArrayList<Float> neighDist = new ArrayList<Float> ();  
  
  float[][] m_Ainv = new float[3][3];
  
  float m_mas = 0.0;
  float m_den = 0.0;
  float m_vol = 0.0;
  float m_rad = 0.0;
  
  //float[][] m_Ainv = new float[3][3];
  
  void printc()
  {
    println("mp: "+mp);
    println("m_ref: "+m_ref);
    println("m_vel: "+ m_vel);
    println("m_for: "+ m_for);
    println("m_mas: "+m_mas);
    println("m_den: " + m_den);
    println("m_vol: "+ m_vol);
    println("m_rad: " + m_rad);
    println("neigh: " + neigh);
    println("neighDist "+ neighDist);
  }
  
  particle(PVector p)
  {
    mp    = new PVector(p.x,p.y,p.z);
    m_ref = new PVector(p.x,p.y,p.z);
    m_mas = MASS_PER_PARTICLE;
  }
  
  particle( float mx, float my, float mz)
  {
    mp    = new PVector(mx,my,mz);
    m_ref = new PVector(mx,my,mz);
    m_mas = MASS_PER_PARTICLE;
  }
  
  // init particle;
  // this function intialize
  void init()
  {
    // Call only after neigh calculated.
    calAvgRadius();
    calDensityVolume();
    computeAinv();
  }
  
  //average radius
  void calAvgRadius()
  {
     // loop through the particles and calc average mass.
     float distance = 0.0;
     for (int i =0 ; i < neighDist.size(); i ++)
     {
       distance += neighDist.get(i);
     }
     distance = distance/neighDist.size();
     m_rad = 3.0 *  distance;
  }
  
  void calDensityVolume()
  {
    m_den =0.0;
    for (int i =0; i < neighDist.size(); i++)
    {
      float w = Kernel.standardKernel(m_rad, neighDist.get(i) );
      m_den += pSys.pList.get(neigh.get(i)).m_mas*w;  
    }
    m_vol = m_mas/m_den; 
  }
  
  void draw()
  {
    pushMatrix();
    translate(mp.x,mp.y,mp.z);
    fill(0,0,255);
    box(0.2);
    popMatrix();
    //point( mp.x, mp.y, mp.z);
  }
  
  float getDist( particle pj)
  {
    PVector p = PVector.sub(m_ref,pj.m_ref);
    return p.mag();
  }
  
  void addToNeigh( int j_index, float dist, int maxNeigh)
  {
    // Find an index to add.
    // loop through neighDist to find spot to add.
    //println(j_index);
    for (int i =0; i < neighDist.size(); i++)
    {
      //println(j_index+ " "+ neighDist.size()+ "amx Neigh"+ maxNeigh);
      if(dist<neighDist.get(i)) //
      {
        // add to the neigh List  and Distance.
        neighDist.add(i,dist);
        neigh.add(i,j_index);
        break;
      } 
    }
    if(neighDist.size() > maxNeigh)
    {
        neighDist.remove(neighDist.size()-1);
        neigh.remove(neigh.size()-1);
    }
    // if Neigh list is less than maxNeigh
  }

  void computeAinv()
  {
    // initialize Ainv to zero.
    for (int i =0; i<3 ; i++)
    {
      for (int j =0; j < 3; j++)
      {
        m_Ainv[i][j] = 0.0;
      }
    } 
    for(int n =0 ;n < neigh.size(); n++)
    {
      PVector x_ij = PVector.sub(pSys.pList.get(neigh.get(n)).m_ref, m_ref); // sub thispoint ref - neigh reft;
      float w_ij = Kernel.standardKernel( m_rad, x_ij.mag());
      float[][] A_ij = matrixProduct(x_ij,x_ij,w_ij);
      matAddtoA_inv(A_ij);
    }
    
  }

  float[][] gradientAtPoint()
  {
    // Assume A_inv is already calculated. 
    PVector U = new PVector(0.0,0.0,0.0);
    PVector V = new PVector(0.0,0.0,0.0);
    PVector W = new PVector(0.0,0.0,0.0);
    PVector ui =  PVector.sub( mp, m_ref);
    for(int n =0 ;n < neigh.size(); n++)
    {
      PVector x_ij = PVector.sub(pSys.pList.get(neigh.get(n)).m_ref, m_ref); // sub thispoint ref - neigh reft;
      float w_ij = Kernel.standardKernel( m_rad, x_ij.mag());
      PVector uj = PVector.sub(pSys.pList.get(neigh.get(n)).mp , pSys.pList.get(neigh.get(n)).m_ref);
      
      PVector temp = PVector.mult(x_ij,w_ij);
      U.add(PVector.mult(temp,(uj.x -ui.x)));
      V.add(PVector.mult(temp,(uj.y -ui.y)));
      W.add(PVector.mult(temp,(uj.z -ui.z)));
    }
    
    // invert matrix A_inv
    QR qr = new QR(m_Ainv);
    double[] gradU = qr.solve(U.array());
    double[] gradV = qr.solve(V.array());
    double[] gradW = qr.solve(W.array());
    
    float[][] grad = new float[3][3];
    
    for (int i =0; i < 3; i++)
    {
      grad[0][i] = (float)gradU[i];
      grad[1][i] = (float)gradV[i];
      grad[2][i] = (float)gradW[i];
    }
    //matPrint(grad,3,3);    
    return grad;
  }
  
  /*void step()
  {
    //set forces to zero.
    float[][] I = Mat.identity(3);
    float[][] grad = gradientAtPoint();
    float[][] J = Mat.sum(grad, I);
    float[][] Jt = matTranspose(J,3,3); //Mat.sum(grad, I).transpose();
    float[][] strain = Mat.multiply(J,Jt); // Multiply this properly.
    strain = Mat.sum(strain,Mat.multiply(I,-1)); // right stress.
    
    // Compute Stress
    float trace_strain = strain[0][0] + strain[1][1] + strain[2][2];
    float diagValue = LAMBDA * trace_strain ;
    float[][] S = Mat.sum( Mat.multiply( Mat.identity(3), diagValue ), Mat.multiply(strain, 2.0*MU));
    float[][] P = Mat.multiply(J,S);
    
    ComputeForces(J, S);
  }*/
  
  void computeForces(float[][] J, float[][] S)
  {
    float kv = KV;
    float[][] Fe = Mat.multiply(Mat.multiply(J,S),-2.0*m_vol);
    float[][] JcrossJ = cross(J);
    float[][] Fv = Mat.multiply(JcrossJ, -m_vol*kv *(Mat.det(J)-1.0) );
    
    float[][] fullMatrix = Mat.multiply( Mat.sum(Fe,Fv), m_Ainv);
    
    for (int i =0; i < neigh.size(); i++)
    {
      PVector x_ij = PVector.sub(pSys.pList.get(neigh.get(i)).m_ref, m_ref);
      float w_ij = Kernel.standardKernel(m_rad, x_ij.mag());
      
      float[] force = Mat.multiply(fullMatrix, PVector.mult(x_ij,w_ij).array());
      PVector forcepv = new PVector(force[0], force[1], force[2] );
      pSys.pList.get(neigh.get(i)).m_for.add( forcepv);
      m_for.sub(forcepv);
    }
  }
  
  float[][] cross(float[][] J)
  {
    PVector Ju = new PVector(J[0][0],J[0][1],J[0][2]);
    PVector Jv = new PVector(J[1][0],J[1][1],J[1][2]);
    PVector Jw = new PVector(J[2][0],J[2][1],J[2][2]);
    
    
    PVector Ru = new PVector(0.0,0.0,0.0); Jv.cross(Jw,Ru);
    PVector Rv = new PVector(0.0,0.0,0.0); Jw.cross(Ju,Rv);
    PVector Rw = new PVector(0.0,0.0,0.0); Ju.cross(Jv,Rw);
    float[][] result = new float[3][3];
    result[0][0] = Ru.x; result[0][1] = Ru.y; result[0][2] = Ru.z; 
    result[1][0] = Rv.x; result[1][1] = Rv.y; result[1][2] = Rv.z;
    result[2][0] = Rw.x; result[2][1] = Rw.y; result[2][2] = Rw.z;
    return result;
  }
  
  void matPrint(float[][] f, int di, int dj)
  {
    for (int i =0 ; i < di; i++)
    {
      for (int j=0; j< dj; j++)
      {
         print(f[i][j]+" "); 
      }
      println();
    }
  }
  
  float[][] matrixProduct(PVector x_ij,PVector y_ij, float w)
  {
    float[][] A_ij = new float[3][3];
    float[] x_ijf = x_ij.array();
    float[] y_ijf = y_ij.array();
    for (int i =0; i <3; i++)
    {
      for (int j=0; j<3; j++)
      {
        A_ij[i][j] = x_ijf[i]*y_ijf[j]*w;
      }
    }
    return A_ij;
  }
  
  
  
  void matAddtoA_inv(float[][] A_ij)
  {
    for (int i =0; i< 3; i++)
    {
      for (int j=0; j<3; j++)
      {
        m_Ainv[i][j] += A_ij[i][j];
      }
    }
  }
  

}



