#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>

#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include <MultiRegions/DisContField2D.h>


#include <iomanip>

#define MAX_ITER 10000
#define MAX_ITER_NI 100
#define TOLERENCE_S 1e-16
#define TOLERENCE_F 1e-7
#define TOLERENCE_P 1e-14
#define TOLERENCE_A 1e-14
#define TOLERENCE_NN 1e-8
#define ALPHAMAX 1.0

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>

using namespace std;

int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2st arg xml file." << endl;
		cout << "3rd degree to outputfld." << endl;
		//cout << "3nd arg polynomial degree filter you want to apply" << endl;
		//cout << "4rd arg meshscaling you want to use." << endl;
	//	cout << "4th Angle of SIAC derivative in deg -90 to 90 allowed" << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();

    SpatialDomains::MeshGraphSharedPtr graph2D =
            SpatialDomains::MeshGraph::Read(vSession);

//	HNM2D->LoadData( fname+ ".chk",var);

	HNM2D->LoadMesh(var[0]);


    int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fPhys:" << tNquadPts<< endl;
	cout << "fCoeffs:" << HNM2D->m_expansions[0]->GetNcoeffs()<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);
	Array<OneD,NekDouble> u(tNquadPts);
	Array<OneD,NekDouble> v(tNquadPts);

    //  ----------------
    int poly = atoi(argv[3]);
    graph2D->SetExpansionsToPolyOrder( poly );
    MultiRegions::ExpListSharedPtr Exp_u;    
    Exp_u = MemoryManager<MultiRegions::DisContField2D>
            ::AllocateSharedPtr(vSession,graph2D,vSession->GetVariable(0));
	int New_tNquadPts = Exp_u->GetTotPoints();
    Array<OneD,NekDouble> U_raw(New_tNquadPts);
    Array<OneD,NekDouble> Ux_raw(New_tNquadPts);
    Array<OneD,NekDouble> Nxc0(New_tNquadPts), Nxc1(New_tNquadPts), Nxc2(New_tNquadPts);
//  -------------------

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Exp_u->GetCoords(Nxc0,Nxc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}

    /*
	NektarBaseClass k;
	if( 0 == atoi(argv[1+2]) )
	{	
		k.writeNekArray(xc0,fname+"_xc0."+"txt");
		k.writeNekArray(xc1,fname+"_xc1."+"txt");
		return 0;
	}
	else
	{
		k.readNekArray(u, fname+"_u.txt");
		k.readNekArray(v, fname+"_v.txt");
		//k.printNekArray(v,0);
	}
    */
        NekDouble x ,y; 
        NekDouble f1x,f2x,f3x;
        NekDouble f1y,f2y,f3y;
    for(int i =0; i < tNquadPts; i++)
    {
        x = xc0[i];
        y = xc1[i];
        f1x = cos(2.0*M_PI*x)/(2.0*M_PI);
        f2x = cos(4.0*M_PI*x)/(8.0*M_PI);
        f1y =sin(2*M_PI*y);
        f2y =sin(4*M_PI*y);
        u[i] = (f1x+ f2x)*(f1y+f2y);
    }


	// Now use Newton-Rapson to find the zero u and zero v.
	
	HNM2D->m_expansions[0]->FwdTrans_IterPerExp( u,HNM2D->m_expansions[0]->UpdateCoeffs());
	HNM2D->m_expansions[0]->BwdTrans_IterPerExp( HNM2D->m_expansions[0]->GetCoeffs(),
			HNM2D->m_expansions[0]->UpdatePhys());
	Array<OneD,NekDouble> u_DG = HNM2D->m_expansions[0]->GetPhys();	
	
    HNM2D->m_Arrays.push_back(u_DG);
    Array<OneD,NekDouble> ux_DG(tNquadPts),uy_DG(tNquadPts);	
    HNM2D->m_expansions[0]->PhysDeriv( u_DG, ux_DG, uy_DG);
	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();


	Array<OneD,NekDouble> coord(3,0.0);
    for(int index=0;index<New_tNquadPts;index++)
    {
			coord[0]= Nxc0[index]; 
			coord[1] = Nxc1[index];
            int elId = HNM2D->GetExpansionIndexUsingRTree(coord);
			int phys_offset = HNM2D->m_expansions[0]->GetPhys_Offset(elId);
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp( elId );
			Array<OneD,NekDouble> el_u_phys = u_DG.CreateWithOffset( u_DG, phys_offset );
			Array<OneD,NekDouble> el_ux_phys = ux_DG.CreateWithOffset( ux_DG, phys_offset );
			U_raw[index] = lexp->PhysEvaluate( coord, el_u_phys);
			Ux_raw[index] = lexp->PhysEvaluate( coord, el_ux_phys);
    }

	Exp_u->FwdTrans_IterPerExp( Ux_raw,Exp_u->UpdateCoeffs());
   
    // Also write a fld file for the dG projection.
	string out = vSession->GetSessionName() + "NewPoly"+argv[3]+".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (Exp_u->GetFieldDefinitions()).at(0);
    FieldDef[0]->m_fields.push_back("U");
 //   FieldDef[0]->m_fields.push_back("ux");
    Exp_u->AppendFieldData(FieldDef[0], FieldData[0]);
//	Exp_u->FwdTrans_IterPerExp( Ux_raw,Exp_u->UpdateCoeffs());
 //   Exp_u->AppendFieldData(FieldDef[0], FieldData[0]);
   HNM2D->m_fld->Write(out, FieldDef, FieldData);

	return 0;
}
