% Create a mass matrix of
% variables.
N = 10;

%Mu=f  
%find u;

[r,w] = JacobiGQ(0,0,N);
r = (r+1)/2;
% M = (N,N)
% for each (i,j) in M
%   temp =0.0
%  for each rv in r
%       temp += w(rv)* B_i(rv)* B_j(rv)
%   end rv
%  M(i,j) = temp;
% end M loop

M = zeros(N,N);

for i = 0:N-1
    for j = 0:N-1
        temp =0.0;
        for rv = 1:N
            temp = temp + w(rv)*B_i(i,N-1,r(rv))*B_i(j,N-1,r(rv));
        end
        M(i+1,j+1) = temp;
    end
end

% Calculated Mass matrix.

% Calculate f = \integral^1_0 f(x)Bi(x) dx
% 

f = zeros(N,1);
for i = 0:N-1;
   temp =0.0;
   for rv = 1:N
      temp = temp + w(rv)*B_i(i,N-1,r(rv))* sin(r(rv)*10);
   end
   f(i+1) = temp;
end

% soved coeffecients.
u = M\f;

test_r = (0:0.01:1)';
[s,del] = size(test_r);

% projected Function.
ProjF = funcEvalBerns(u,test_r);
ActF = sin(test_r*10);
plot(test_r, ProjF,test_r,ActF);
ActEr = sum(abs(ActF-ProjF))/s;


Deg_elevate = 20;
u_err = zeros(Deg_elevate,1);


% push the negative terms to zero and calculate error on ProjF.
u_cg = u;
for i = 1:Deg_elevate
    % change these to positive.
    u_use = u_cg.*(u_cg>0.0);
    ProjF = funcEvalBerns(u_use,test_r);
    ActF = sin(test_r*10);
    u_err(i) = sum(abs(ActF-ProjF))/s;
    u_cg = degElevation(u_cg);
end
u_err

plot(test_r, ProjF,test_r,ActF);

% degree elevate (1)

% push the negative terms to zero and calculate error on ProjF.


% degree elevate (1)

