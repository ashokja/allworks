clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20];

%abc = '../../data/UnsTriMeshes/NM2_unsMeshvar1_0125_2_R_100_pX_2DDyn.txt';

R = 100;
poly =2;
order = poly;
count =1;
%prefixFilename = 'data/P4_triSplit6_L4_R1_';
%prefixFilename = 'data/P2_triSpli_L2_R1_';
%prefixName = '../../data/UnsTriMeshes_Ashok/';
%prefixName = '../../data/Diff_NPUTriMesh/';
%prefixName = '../../data/sQuadDyn/';

%prefixName = '../../data/NPUTriMesh_proj/';
%prefixName = '../../data/NUTriMesh_proj/';
%prefixName = '../../data/UTriMesh_proj/';
prefixName = '../../data/t106c/';

width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
fsw = 20;
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize

%for poly = [2,3,4]

            FileName = strcat('t106c_');
            prefixFilename = strcat( prefixName,FileName,int2str(poly),'_RSfld_',int2str(R),'_Param_2');

            xfname = strcat(prefixFilename,'_pX_2DDyn.txt' );
            yfname = strcat(prefixFilename,'_pY_2DDyn.txt' );
            ffname = strcat(prefixFilename,'_pP_2DDyn.txt' );
            pfname = strcat(prefixFilename,'_pP_2DDyn.txt' );
            sfname = strcat(prefixFilename,'_pS_2DDyn.txt' );
            sDynfname = strcat(prefixFilename,'_pSDyn_2DDyn.txt' );
            dynsfname = strcat(prefixFilename,'_pDyn_2DDyn.txt' );
            tfname = strcat(prefixFilename,'_pT_2DDyn.txt' );
            tDynfname = strcat(prefixFilename,'_pTDyn_2DDyn.txt' );
            
            

            xpos = textread(xfname, '%f\t');
            ypos = textread(yfname, '%f\t');
            fpos = textread(ffname, '%f\t');
            ppos = textread(pfname, '%f\t');
            spos = textread(sfname, '%f\t');
            sDynpos = textread(sDynfname, '%f\t');
            dynspos = textread(dynsfname, '%f\t');
            tpos = textread(tfname, '%f\t');
            tDynpos = textread(tDynfname, '%f\t');
            
            
            xVpos = reshape(xpos,[R,R]);
            yVpos = reshape(ypos,[R,R]);
            fVpos = reshape(fpos,[R,R]);
            pVpos = reshape(ppos,[R,R]);
            sVpos = reshape(spos,[R,R]);
            sDynVpos = reshape(sDynpos,[R,R]);
            dynsVpos = reshape(dynspos,[R,R]);
            tVpos = reshape(tpos,[R,R]);
            tDynVpos = reshape(tDynpos,[R,R]);

            figure(1),
            surf(xVpos,yVpos,pVpos);
            title('Simulation Result');
          %  xlim([1.14,1.2]); zlim([0.4,0.6])
            
            figure(2),
            surf(xVpos,yVpos,sVpos);
            title('L-SIAC filter');
         %               xlim([1.14,1.2]); zlim([0.4,0.6])
            
            figure(3),
            surf(xVpos,yVpos,sDynVpos);
            title('L-SIAC Dyn Filter');
         %   xlim([1.14,1.2]); zlim([0.4,0.6])
            
            cvalues = linspace(0.4,0.6,10);
            figure(4),
            subplot(1,3,1),
            contourf(xVpos,yVpos,pVpos,cvalues);
            subplot(1,3,2),
            contourf(xVpos,yVpos,sVpos,cvalues);
            subplot(1,3,3),
            contourf(xVpos,yVpos,sDynVpos,cvalues);
%end
