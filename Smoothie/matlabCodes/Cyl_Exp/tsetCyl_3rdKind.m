%testCyl_3rdkind
% working with files form TestCyl_T2.
clear;
close all;

filePoly = 3;

addpath('../');

%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize
Cyl_Max = 6.3;
Cyl_Min = -5.2571;



r = filePoly;
filename = strcat( '../datatxt/Cy',int2str(r),'l_Re500');
filenameFrPrint = strcat( 'Cy',int2str(r),'l_Re500_');
%    filenameFrPrint = strcat( 'Cy',int2str(r),'l_Re500_','Vor2ndKind_');


elOfname = strcat( filename,'_elO.txt');
elTfname = strcat( filename,'_elT.txt');
xfname = strcat( filename,'_x.txt');
yfname = strcat( filename,'_y.txt');
ufname = strcat( filename,'_u.txt');
vfname = strcat( filename,'_v.txt');

Elxfname = strcat( filename,'_Elx.txt');
Elyfname = strcat( filename,'_Ely.txt');
Elidfname = strcat( filename,'_Elid.txt');


elOffsets = floor( textread(elOfname,'%f\t') + 1.0001);
elType = floor( textread(elTfname,'%f\t') + 0.0001);
xval = textread(xfname,'%f\t') ;
yval = textread(yfname,'%f\t') ;
uval = textread(ufname,'%f\t') ;
vval = textread(vfname,'%f\t') ;

Elx = textread(Elxfname, '%f\t');
Ely = textread(Elyfname, '%f\t');
Elid = textread(Elidfname, '%f\t');



 tri = [];
 tricount = 0; quadcount=0; 
 ptCloud = [xval,yval,zeros(size(xval))];
 for el = 1:max(size(elOffsets))
     if ( 3 == floor( elType(el) ) )
         tricount = tricount+1;
         startIndex = elOffsets(el);
         point = [Elx(tricount),Ely(tricount),0.0];
        [a,Special] = fNN(ptCloud,point);
         if ( abs(a) > 1e-6)
             disp('Something is wrong'); 
         end
         Atri = GenerateP3Triangles(elType(el), startIndex,Special);
         tri = [tri;Atri];
     else
         quadcount= quadcount+1;
         startIndex = elOffsets(el);
         Atri = GenerateP3Triangles(elType(el), startIndex,-1);
         tri = [tri;Atri];
     end      
 end

%%
ForUnique = floor(xval*1000000000+yval*1000);
[Val,elID, relID] = unique(ForUnique);
xvSorted = xval(elID);    
yvSorted = yval(elID);    

uvSorted = accumarray(relID,uval,[],@mean);
vvSorted = accumarray(relID,vval,[],@mean);

Tri = relID(tri(:,:));

figure(1)
subplot(1,2,1),
trisurf(Tri,xvSorted,yvSorted,uvSorted); hold on;
shading interp; axis([2.5 10 -4 4])
subplot(1,2,2),
trisurf(Tri,xvSorted,yvSorted,vvSorted); hold on;
shading interp; axis([2.5 10 -4 4])
%%

% for each triangle calculate constant.
uxTri = zeros(max(size(Tri)),1); uyTri = zeros(max(size(Tri)),1);
vxTri = zeros(max(size(Tri)),1); vyTri = zeros(max(size(Tri)),1);
for t = 1:max(size(Tri))
    P1 = [ xvSorted(Tri(t,1)); yvSorted(Tri(t,1)); uvSorted(Tri(t,1)) ];
    P2 = [ xvSorted(Tri(t,2)); yvSorted(Tri(t,2)); uvSorted(Tri(t,2)) ];
    P3 = [ xvSorted(Tri(t,3)); yvSorted(Tri(t,3)); uvSorted(Tri(t,3)) ];
    Cp = cross(P3-P1,P2-P1);
    uxTri(t) = -1.0*Cp(1)/Cp(3);
    uyTri(t) = -1.0*Cp(2)/Cp(3);
end

for t = 1:max(size(Tri))
    P1 = [ xvSorted(Tri(t,1)); yvSorted(Tri(t,1)); vvSorted(Tri(t,1)) ];
    P2 = [ xvSorted(Tri(t,2)); yvSorted(Tri(t,2)); vvSorted(Tri(t,2)) ];
    P3 = [ xvSorted(Tri(t,3)); yvSorted(Tri(t,3)); vvSorted(Tri(t,3)) ];
    Cp = cross(P3-P1,P2-P1);
    vxTri(t) = -1.0*Cp(1)/Cp(3);
    vyTri(t) = -1.0*Cp(2)/Cp(3);
end
%%

TriArea = zeros(max(size(Tri)),1);
%calcualte and store each area of triangle.
for t = 1:max(size(Tri))
    P1 = [ xvSorted(Tri(t,1)); yvSorted(Tri(t,1)); vvSorted(Tri(t,1)) ];
    P2 = [ xvSorted(Tri(t,2)); yvSorted(Tri(t,2)); vvSorted(Tri(t,2)) ];
    P3 = [ xvSorted(Tri(t,3)); yvSorted(Tri(t,3)); vvSorted(Tri(t,3)) ];
    xts = xvSorted(Tri(t,:));
    yts = yvSorted(Tri(t,:));
    TriArea(t) = polyarea(xts,yts);
end
%%
% for each vertex
% loop through all triangles calcuate list.
% weight based vertex value.
numV = max(size(xvSorted));
for v = 1:numV
%     listTids = [];
%     for t = 1:max(size(Tri))
%         ids = Tri(t,:);
%         %listTids(end+1) = find(ids==v);
%         %disp(find(ids==v));
%         if (find(ids==v))
%             listTids(end+1) = t;
%         end
%     end
    [listTids a2 a3] = find(Tri==v);
    disp(listTids);
%    vertexToTriMap(v,:) = listTids;
    sumArea = 0;
    uxV =0; uyV=0; vyV=0; vxV=0;
    for lt = 1:max(size(listTids))
        tid = listTids(lt);
        sumArea = sumArea + TriArea(tid);
        uxV = uxV+ TriArea(tid)*uxTri(tid);
        uyV = uyV+ TriArea(tid)*uyTri(tid);
        vxV = vxV+ TriArea(tid)*vxTri(tid);
        vyV = vyV+ TriArea(tid)*vyTri(tid);
    end
    uxvSorted(v) = uxV/sumArea;
    uyvSorted(v) = uyV/sumArea;
    vxvSorted(v) = vxV/sumArea;
    vyvSorted(v) = vyV/sumArea;
end

%%

save('ComputingTimehere');
%%
Cyl_Min = -6.3;
Cyl_Max = 5.2571;
VorvSorted = vxvSorted - uyvSorted;
figure(2)
trisurf(Tri,xvSorted,yvSorted,VorvSorted); hold on;
shading interp; axis([2.5 10 -4 4]);caxis( [Cyl_Min Cyl_Max] );


%%
% figure(3)
% trisurf(Tri,xvSorted,yvSorted,VorvSorted); hold on;
% shading interp; axis([2.5 10 -4 4]);caxis( [Cyl_Min Cyl_Max] );

figure(4)
[tcc tch] = tricontour( Tri,xvSorted,yvSorted,VorvSorted,(Cyl_Min:.3:Cyl_Max));
hold on;
line([3.7,3.7],[-4,4],'lineStyle','--','Color',[0.5 0.5 0.5],'linewidth',lw);
set (tch, 'LineWidth', lw);
xlim([2.5,10.5]);
ylim([-4,4]);
%axis box;
axis off;

print( strcat('../paperUseImagesF/',filenameFrPrint,'_Type4_contour'), '-dpng','-r500');
%plotNekMesh('../../data/CylExamples/Cy2l.xml');
   


% figure(5)
% tricontour( Tri,xvSorted,yvSorted,vxvSorted,(Cyl_Min:.3:Cyl_Max));
%    xlim([2.5,10.5]);
%     ylim([-4,4]);
% axis off;
% 
% 
% figure(6)
% tricontour( Tri,xvSorted,yvSorted,uyvSorted,(Cyl_Min:.3:Cyl_Max));
%    xlim([2.5,10.5]);
%     ylim([-4,4]);
% axis off;

%% The line working is hardest now :(

%% line graph.
% find all intersections of line with triangles.
xlp = [3.7, 3.7];
ylp = [-4, 4];

% Find all edges in mesh, note internal edges are repeated
F = Tri;
E = sort([F(:,1) F(:,2); F(:,2) F(:,3); F(:,3) F(:,1)]')';
% determine uniqueness of edges
[u,m,n] = unique(E,'rows');


count = 0;
for loop = 1:max(size(u))
    edg = u(loop,:);
    edgx = [xvSorted(edg(1)),xvSorted(edg(2))];
    edgy = [yvSorted(edg(1)),yvSorted(edg(2))];
    edgVals = [VorvSorted(edg(1)),VorvSorted(edg(1))];
    [xin,yin] = polyxpoly(xlp, ylp, edgx, edgy);
    if(~isempty(xin))
        disp(xin);
        disp(yin);
        count=count+1;
        ylstor(count) = yin;
        pt = sum((edgx(1)-edgx(2))^2 + (edgy(1)-edgy(2))^2).^(0.5);
        t = sum((edgx(1)-xin)^2 + (edgy(1)-yin)^2).^(0.5);
        r = t/pt;
        yvalstor(count) = edgVals(1) + ( edgVals(2) - edgVals(1) )*r;
    end
end
%%
load ('../xtigTagsforLabelUseat3p7.mat');
[tmp,ind]=sort(ylstor);
yvalstor_sort=yvalstor(ind);
figure()
plot(tmp,yvalstor_sort,'-k','LineWidth',lw,'MarkerSize',msz); hold on;
set(gca,'XTick',sort(unique(tmp)),'Xticklabel','');
ylabel('vorticity')
set(gca,'box','off')
axis([min(tmp) max(tmp) min(yvalstor) max(yvalstor)]);

ax1 = gca;
ax2 = axes('Position', get(ax1, 'Position'),'Color', 'none');
set(ax2, 'XAxisLocation', 'top','YAxisLocation','Right');
set(ax2, 'XLim', get(ax1, 'XLim'),'YLim', get(ax1, 'YLim'));
set(ax2, 'XTick',sort(sxTTags),'YTick',[]);
set(ax2, 'XTickLabel', [ ] ,'YTickLabel', [ ] );
%%

%%
print(strcat('../paperUseImagesF/',filenameFrPrint,'_Typ4_lineGraph'), '-dpng','-r500');





