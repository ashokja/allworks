function [] = plotQuiver(data,dataV)
    quiver3(data(:,1), data(:,2), data(:,3), ...
                dataV(:,1), dataV(:,2), dataV(:,3));
end
