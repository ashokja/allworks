% testing a tet with vert and velocity for vorticity.
clear
% assume a tet with vertices a,b,c,d;
a =[0;0;0];
b =[1;0;0];
c =[0;1;0];
d =[0;0;1];

%u_a = [0;0;0];
%u_b = [0;6;-5];
%u_c = [-6;2;20];
%u_d = [-1;-16;-10];

u_a = [1/2;-1/2;1/2];
u_b = [1/2;1/2;1/2];
u_c = [-1/2;-1/2;1/2];
u_d = [1/2;-1/2;-1/2];

% re-writing for coords and vel.
coords(1,:) = a;
coords(2,:) = b;
coords(3,:) = c;
coords(4,:) = d;

%coords= coords;
vel(1,:) = u_a;
vel(2,:) = u_b;
vel(3,:) = u_c;
vel(4,:) = u_d;

[c_f,V_f] = gradientTensorTet(coords,vel);

lambda = eig(V_f);

tet_complex =false;
if ~isreal(lambda)
    'tet contains complex number'
    tet_complex = true;
end

if (tet_complex)
    % find the real rool index. 
   real_index  = find(imag(lambda)==0);
end

if (tet_complex)
    [A,B] = eig(V_f);
    n = A(:,real_index)'/norm(A(:,real_index));
    w(1,:) = vel(1,:) - dot(vel(1,:),n)*n;
    w(2,:) = vel(2,:) - dot(vel(2,:),n)*n;
    w(3,:) = vel(3,:) - dot(vel(3,:),n)*n;
    w(4,:) = vel(4,:) - dot(vel(4,:),n)*n;
    [c_w,V_w] = gradientTensorTet(coords,w);
end

% Given plane equations and tet Coordinates.
% Find the line with intersects both planes.
% Find if any line segment passes through the tet 
%   and interset the faces of the tet planes.

retP = PlaneTetInter(V_w,c_w,coords)



