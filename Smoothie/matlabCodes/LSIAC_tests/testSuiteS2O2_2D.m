clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [10];

poly =2;
order = poly;
count =1;
for r = rates
    
    xfname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_xc0_OneSided2kp1.txt' );
    yfname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_xc1_OneSided2kp1.txt' );
    ffname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_fce_OneSided2kp1.txt' );
    efname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_ece_OneSided2kp1.txt' );
    sfname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_sce_OneSided2kp1.txt' );

    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');
    fval = textread(ffname, '%f\t');
    pval = textread(efname, '%f\t');
    sval = textread(sfname, '%f\t'); 
        
    s = sqrt(max(size(xpos)));
    xVpos = reshape(xpos,s,s);
    yVpos = reshape(ypos,s,s);
    fVval = reshape(fval,s,s);
    pVval = reshape(pval,s,s);
    sVval = reshape(sval,s,s);    
    
    figure(1)
   % surf(xVpos, yVpos, log10(abs(fVval-pVval))); hold on;
   % surf(xVpos, yVpos, log10(abs(fVval-sVval)) ); hold off;
    plot3(xpos,ypos, log10(abs(fval-pval)),'*',xpos,ypos,log10(abs(fval-sval)),'*');
    
    figure(2)
    plot3(xpos,ypos, log10(abs(fval-sval)),'*','DisplayName',strcat('No of ele:',int2str(r)));
    %surf(xVpos,yVpos, log10(abs(fVval-sVval)),'DisplayName',strcat('No of ele:',int2str(r)));
    
    hold on;

    figure(3)
    plot3(xpos,ypos, pval);
    %surf(xVpos,yVpos, pVval); hold on;
     %   surf(xVpos,yVpos, pVval); hold off;

    p_avg_err(count) = sum(abs(fval-pval))/max(size(xpos));
    s_avg_err(count) = sum(abs(fval-sval))/max(size(xpos));
    
    p_max_err(count) = max(abs(fval-pval));
    s_max_err(count) = max(abs(fval-sval));
    count = count+1;
end

figure(1),
legend('show');
figure(2),
legend('show');
title(strcat('poly-',int2str(poly),' order-',int2str(order)));
xlabel('domain');
ylabel('Cos(2*pi*x)');
saveas(2,strcat('S2O2poly_',int2str(poly),'_order_',int2str(order)),'jpg');

figure(3)
plot(log10(rates),log10(p_avg_err),log10(rates),log10(s_avg_err));
title(strcat('poly-',int2str(poly),' order-',int2str(order),'  AvgError'));
xlabel('Mesh Samping');
ylabel('Error');
legend('projected','SIAC Error');
saveas(3,strcat('S2O2poly_',int2str(poly),'_order_',int2str(order),'_AvgERplot'),'jpg');


figure(4)
plot(log10(rates),log10(p_max_err),log10(rates),log10(s_max_err));
title(strcat('poly-',int2str(poly),' order-',int2str(order),'  MaxError'));
xlabel('Mesh Samping');
ylabel('Error');
legend('projected','SIAC Error');
