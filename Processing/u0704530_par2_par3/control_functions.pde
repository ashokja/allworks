
void myGCP5Setup()
{
  myGCP5 = new ControlP5(this);
  myGCP5.addButton("Load_File",1,ScreenX+10,20,60,20);
  myGCP5.addToggle("Lines_ON_OFF",true,ScreenX+100,10,40,20);
  myGCP5.addToggle("Control_Pts_ON_OFF",true,ScreenX+100,50,40,20);
  myGCP5.addKnob("Rotate_ScreenZ",0,360,0,ScreenX+70,730,50);
    myGCP5.addKnob("Rotate_ScreenY",0,360,0,ScreenX+70,630,50);
      myGCP5.addKnob("Rotate_ScreenX",0,360,0,ScreenX+70,530,50);
  myGCN_AP = myGCP5.addNumberbox("Curve_Number_Add_Point",1,ScreenX+10,200,50,15);
  myGCP5.addButton("Add_Point",2,ScreenX+120,180,40,40);
  myGDC = myGCP5.addNumberbox("Delete_Curve",1,ScreenX+10,470,50,15);
  myGCP5.addButton("Delete_CurveB",2,ScreenX+120,470,40,40);
  myGCN = myGCP5.addNumberbox("Curve_Number",1,ScreenX+10,250,50,15);
  myGD = myGCP5.addNumberbox("Degree",3,ScreenX+130,250,50,15);
  r = myGCP5.addRadioButton("ChooseKnotType")
         .setPosition(ScreenX+10,290)
         .setSize(40,20)
         .setColorForeground(color(120))
         .setColorActive(color(255))
         .setColorLabel(color(255))
         .setItemsPerRow(3)
         .setSpacingColumn(50)
         .addItem("Uniform",1)
         .addItem("Modified",2);
  myGCP5.addButton("Modify_Curve",3,ScreenX+70,330,40,20);    
  myGCN_AC = myGCP5.addNumberbox("ControlPoints_AC",1,ScreenX+10,360,50,15);
  myGD_AC = myGCP5.addNumberbox("Degree_AC",3,ScreenX+130,360,50,15);
  r_AC = myGCP5.addRadioButton("ChooseKnotType_AC")
         .setPosition(ScreenX+10,400)
         .setSize(40,20)
         .setColorForeground(color(120))
         .setColorActive(color(255))
         .setColorLabel(color(255))
         .setItemsPerRow(3)
         .setSpacingColumn(50)
         .addItem("Uniform_AC",1)
         .addItem("Modified_AC",2);
  myGCP5.addButton("Add_Curve",3,ScreenX+70,430,40,20);
  myGCP5.addButton("Delete_Contol_Point",2,ScreenX+20,100,40,40);
  

// Surface controls
  myGCP5.addButton("Load_File_surface",1,ScreenX+CScreenX+10,20,110,20);
  // Toggle buttons
  T_ConvexHull = myGCP5.addToggle("Convex_Hull",true,ScreenX+CScreenX+10,60,60,15);
  T_IsoCurves = myGCP5.addToggle("Iso_Para_curves",true,ScreenX+CScreenX+10,90,60,15); 
  T_NodalCurves  = myGCP5.addToggle("Iso_Para_Nodal_curves",true,ScreenX+CScreenX+10,120,60,15); 
  T_KnotCurves = myGCP5.addToggle("Iso_Para_Knot_curves",true, ScreenX+CScreenX+10,150,60,15);
  // curves in udir
  T_uSlider = myGCP5.addSlider("uCurves")
     .setPosition(ScreenX+CScreenX+10,180)
     .setRange(2,25)
     .setValue(6);
  // curves in vdir
  T_vSlider = myGCP5.addSlider("vCurves")
     .setPosition(ScreenX+CScreenX+10,210)
     .setRange(2,25)
     .setValue(6);
     
  myGCP5.addButton("Load_FileData_Surface",1,ScreenX+CScreenX+10,380,110,20);  
  T_GivenData = myGCP5.addToggle("Given_Curves",true,ScreenX+CScreenX+10,410,110,15);
  T_CurveNum = myGCP5.addTextfield("CurveNum")
    .setPosition(ScreenX+CScreenX+10, 450)
      .setSize(70, 20)
          .setFocus(true)
            .setColor(color(255, 0, 0));
            
  T_FileName = myGCP5.addTextfield("FileName")
    .setPosition(ScreenX+CScreenX+10+80, 450)
      .setSize(120, 20)
          .setFocus(true)
            .setColor(color(255, 0, 0));
            
  myGCP5.addButton("SaveFile_surface",1,ScreenX+CScreenX+10,490,110,20);

  // Delete the Surface button.
  // Textfield
  T_Sur_Data_del = myGCP5.addTextfield("Sur_Data_To_Del")
     .setPosition(ScreenX+CScreenX+10,520)
     .setSize(70, 20)
     .setAutoClear(false);
     
  myGCP5.addButton("Del_Data_Sur",1,ScreenX+CScreenX+90,520,60,20);

  // Button
  T_Sur_del = myGCP5.addTextfield("Sur_To_Del")
     .setPosition(ScreenX+CScreenX+10,230)
     .setSize(70, 20)
     .setAutoClear(false);
     
  myGCP5.addButton("Del_Sur",1,ScreenX+CScreenX+90,230,60,20);


}

void controlEvent(ControlEvent theEvent) {

  if(theEvent.isController()) { 
    //print("control event from : "+theEvent.controller().name());
    //println(", value : "+theEvent.controller().value());
    
    if(theEvent.controller().name() == "Load_FileData_Surface"){
      selectInput("Select a file to process:", "fileDataSelectedSurface");
    }
    
    if(theEvent.controller().name() == "Del_Data_Sur" ) {
      int curNum = Integer.parseInt(trim(T_Sur_Data_del.getText() ));
      sdcA.remove(curNum-1); 
    }

    if(theEvent.controller().name() == "Del_Sur" ) {
      int curNum = Integer.parseInt(trim(T_Sur_del.getText() ));
      scA.remove(curNum-1); 
    }
      
    if( theEvent.controller().name()=="SaveFile_surface")
    {
      println(T_CurveNum.getText());
      println(T_FileName.getText());
      int curNum = Integer.parseInt(trim(T_CurveNum.getText()));
      SaveTofile(T_FileName.getText(), sdcA.get(curNum-1).scd);
      //T_CurveNum
      //T_FileName
      
    }
    
    if( theEvent.controller().name()== "Delete_CurveB")
    {
      int nCnum = (int)myGDC.getValue()-1;
      if (nCnum >= CPtAs.size())
      {
        print("Choose the right curve");
        return;
      }
      
      CPtAs.remove(nCnum);
      D_CPtAs.remove(nCnum);
      Degs.remove(nCnum);
      KnotAs.remove(nCnum);
      NumCPts.remove(nCnum);
      myGCurC--;
      
    }
    
    if(theEvent.controller().name()=="Add_Curve")
    {
      // check if controls points vs Degree are fine.
      // check if uniform or modified also
      
      cpts = (int)myGCN_AC.getValue();
      aDeg = (int)myGD_AC.getValue();
      
      if (1 == r_AC.getValue())
      {
        GACUni = true;
      } else if (2 == r_AC.getValue())
      {
        GACUni = false;
      } else
      {
        print("\nChoose uniform or Modified. \n");
        return;
      }
      
      if(cpts < aDeg+1)
      {
        print("\nControl points are less add more \n");
        return;
      }
      
      
      cptsA = new ArrayList<PVector>();
      D_cptsA = new ArrayList<Draggable>();
      
      print("\nRecroding true "+cpts+" \n");
      recordingClicks = true;
      
    }
    
    if(theEvent.controller().name()=="Rotate_ScreenZ")
    {
      CtetaZ = theEvent.controller().value();
      for (int c =0; c< CPtAs.size(); c++)
      {
        ArrayList<PVector> CPtA = CPtAs.get(c);
        for (int pb =0; pb < CPtA.size(); pb++)
        {
          // check if the point is clicked then delete it.
          PVector p = new PVector(CPtA.get(pb).x,CPtA.get(pb).y,CPtA.get(pb).z);
          Draggable d = D_CPtAs.get(c).get(pb);
          d.x = CToS(p,1);
          d.y = CToS(p,2);
        }
      }
    }
    
    
    if(theEvent.controller().name()=="Rotate_ScreenY")
    {
      CtetaY = theEvent.controller().value();
      for (int c =0; c< CPtAs.size(); c++)
      {
        ArrayList<PVector> CPtA = CPtAs.get(c);
        for (int pb =0; pb < CPtA.size(); pb++)
        {
          // check if the point is clicked then delete it.
          PVector p = new PVector(CPtA.get(pb).x,CPtA.get(pb).y,CPtA.get(pb).z);
          Draggable d = D_CPtAs.get(c).get(pb);
          d.x = CToS(p,1);
          d.y = CToS(p,2);
        }
      }
    }
    
    if(theEvent.controller().name()=="Add_Point")
    {
      GAddPoint = true;
      
    }
    
    if(theEvent.controller().name()=="Rotate_ScreenX")
    {
      CtetaX = theEvent.controller().value();
      for (int c =0; c< CPtAs.size(); c++)
      {
        ArrayList<PVector> CPtA = CPtAs.get(c);
        for (int pb =0; pb < CPtA.size(); pb++)
        {
          // check if the point is clicked then delete it.
          PVector p = new PVector(CPtA.get(pb).x,CPtA.get(pb).y,CPtA.get(pb).z);
          Draggable d = D_CPtAs.get(c).get(pb);
          d.x = CToS(p,1);
          d.y = CToS(p,2);
        }
      }
    }
    
    if(theEvent.controller().name()=="Modify_Curve")
    {
      //print(r.getValue());
      //print(myGCN.getValue());
      //print(myGD.getValue());
      int nDeg = (int)myGD.getValue();
      int nCnum = (int)myGCN.getValue();
      int radiob = (int)r.getValue();
      
      //Check if curve exists
      //Check if it has enough control points.
      if(nCnum > CPtAs.size())
      {
        print("Choose from existing curves.");
        return;
      }
      nCnum = nCnum-1;
      int nNumCPt = NumCPts.get(nCnum);
      
      if(nNumCPt < nDeg+1)
      {
        print("Not enough control points for choosen degree");
        return;
      }
      
      if(0 == radiob)
      {
        print("Choose uniform or modified knot vector.");
      }
      
      // All conditions satisfy. Go ahead and modify knot vector.
      ArrayList<Float> kv = new ArrayList<Float>();
      if( 1 == radiob )
      {
        // Uniform knot vector.
        for (int i =0; i < nNumCPt+nDeg+1; i++)
        {
          kv.add((float)i);
        }
      }
      else if ( 2==radiob )
      {
        int count=0;
        for(int i =0; i < nNumCPt+nDeg+1;i++)
        {
          if(i <= nDeg || i >= nNumCPt+1)
          {}
          else
          {
            count++;
          }
          float k = count;
          kv.add(k);
        }
        kv.set(nNumCPt+nDeg, kv.get(nNumCPt+nDeg)+1);
      }
      KnotAs.remove(nCnum);
      KnotAs.add(nCnum,kv);
    }
    
    
    if(theEvent.controller().name()=="Load_File") {
      selectInput("Select a file to process:", "fileSelected");
    }
    
    if(theEvent.controller().name() == "Load_File_surface"){
      selectInput("Select a file to process:", "fileSelectedSurface");
    }
    
    if(theEvent.controller().name()=="Delete_Contol_Point")
    {
      myGDeleteButton = true;
    }
    
    if(theEvent.controller().name()=="Lines_ON_OFF") 
    {
      // Turn On and Off lines.
      if (myGLinesVisible)
      {
        myGLinesVisible = false;
      }  
      else
      {
        myGLinesVisible = true;
      } 
    }
    
    if(theEvent.controller().name()=="Control_Pts_ON_OFF") 
    {
      // Turn On and Off lines.
      if (myGCPVisible)
      {
        myGCPVisible = false;
      }  
      else
      {
        myGCPVisible = true;
      }
    }
    
    
    if(theEvent.controller().name()=="toggle1") {
      if(theEvent.controller().value()==1) colors[2] = color(0,255,255);
      else                                 colors[2] = color(0,0,0);
    }
    
    if(theEvent.controller().name()=="slider1") {
      colors[3] = color(theEvent.controller().value(),0,0);
    }
    
    if(theEvent.controller().name()=="slider2") {
      colors[4] = color(0,theEvent.controller().value(),0);
    }
      
    if(theEvent.controller().name()=="knob1") {
      colors[5] = color(0,0,theEvent.controller().value());
    }
    
    if(theEvent.controller().name()=="ADD_POINT") {
      // write code to add point here.
      colors[6] = color(theEvent.controller().value());
    } 
    
  }  
  
}


