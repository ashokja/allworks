% read text file.

close all;
clear;

xpos = textread('Read_xpos.txt', '%f \t');
tval = textread('Read_tval.txt', '%f \t');
tvalS = textread('Read_tval2.txt', '%f \t');

figure(1)
plot(xpos,abs(tval-tvalS));
title ('Error');



figure(2)
plot(xpos,tval,xpos,tvalS);
title('Actual Functions')