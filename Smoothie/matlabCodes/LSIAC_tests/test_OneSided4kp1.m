clear;
close all;
% test 

xpos = textread('xc0_OneSided4kp1.txt', '%f\t');
fval = textread('fce_OneSided4kp1.txt', '%f\t');
pval = textread('ece_OneSided4kp1.txt', '%f\t');
sval = textread('sce_OneSided4kp1.txt', '%f\t');


xs = 1;
xe = 63;

plot(xpos, fval,xpos,pval,xpos,sval);
xlabel('1D mesh(x)')
ylabel('values')
legend('Actual function','projected','SIAC post-processed');
figure(2),
plot( xpos(xs:xe), abs(fval(xs:xe)-pval(xs:xe)), xpos(xs:xe), abs(fval(xs:xe)-sval(xs:xe)));
xlabel('1D mesh(x)')
ylabel('error')
legend('projection error','SIAC error');