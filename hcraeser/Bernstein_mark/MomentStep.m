function [C_out] = MomentStep(C_in,l,X,n,d,W)
    [de,q] = size(X);
    C_out=[] ;
    for i_l = 1:q
        abs = X(i_l); s = 1-abs; r = abs/s;
        % foreach (a1,a2 ...,al-1) \belogs I^n_{l-1}
        C_Alpha = CreateAlpha(l,n); % becoz of how CreateAlpha was defined.
        [C_Alpha_size,de] = size(C_Alpha);
        for ar = 1:C_Alpha_size 
           a_l_actual = C_Alpha(ar,l);
           w = s^(a_l_actual);
           % for a_l =0 to n-a1 -a2 ... a_{l-1} do
           for a_l = 0:a_l_actual
               % foreach(i_{l+1},....,i_d) belongs \{1,...q\}^{d-l}
               L = CreateDDimIndices(X,d-l);
               [numL,de]=size(L);
               ww = W[i_l]*nchoosek(a_l_actual,a_l)*abs^(a_l)*(1-abs)^(a_l_actual-a_l);
               for i_numL = 1:numL
                   % indices
                   ind_1st = C_Alpha(ar,1:end-1)+1;
                   ind_3rd = L(i_numL,:);
                   ind_out = [ind_1st,i_l,ind_3rd];
                   ind_in = [ind_1st,a_l+1,ind_3rd];
                   subsCell_out = num2cell(ind_out);
                   subsCell_in = num2cell(ind_in);
                   C_in(subsCell_in{:});
                   %C_out(subsCell_out{:}) = w*C_in(subsCell_in{:});
                   C_out_size = size(C_out);
                   if(1 == min(size(C_out_size) == size(ind_out)))
                   else
                       C_out_size=C_out_size(2);
                   end
                   %C_out_size;
                   %ind_out;
                   if( 1== min(C_out_size>=ind_out))
                       C_out(subsCell_out{:}) = C_out(subsCell_out{:})+ww*C_in(subsCell_in{:});
                   else
                       C_out(subsCell_out{:}) = ww*C_in(subsCell_in{:});
                   end
               end
               % special case
               if 0 == numL
                   ind_1st = C_Alpha(ar,1:end-1)+1;
                   %ind_3rd = L(i_numL,:); wont exist
                   ind_out = [ind_1st,i_l];
                   ind_in = [ind_1st,a_l+1];
                   subsCell_out = num2cell(ind_out);
                   subsCell_in = num2cell(ind_in);
                   C_in(subsCell_in{:});
                   %C_out(subsCell_out{:}) = w*C_in(subsCell_in{:});
                   C_out_size = size(C_out);
                   if(1 == min(size(C_out_size) == size(ind_out)))
                   else
                       C_out_size=C_out_size(2);
                   end
                   if( 1== min(C_out_size>=ind_out))
                       C_out(subsCell_out{:}) = C_out(subsCell_out{:})+ww*C_in(subsCell_in{:});
                   else
                       %size(ind_out);
                       %size(C_out_size);
                       C_out(subsCell_out{:}) = ww*C_in(subsCell_in{:});
                   end
               end
               w = w*r*(a_l_actual-a_l)/(1+a_l);
           end
        end
    end
end