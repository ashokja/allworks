function [] = VizIsoSurface(xcheb,ycheb,zcheb,UAtCheb,VAtCheb,WAtCheb,sx,sy,sz)
    switch nargin
        case 6
            sx = [-1];
            sy = [-1,0,1];
            sz = [-1];
    end
    [X,Y,Z] = meshgrid(xcheb,ycheb,zcheb);
    normAtCheb = sqrt(UAtCheb.^2 + VAtCheb.^2+ WAtCheb.^2);
    normAtCheb = permute(normAtCheb,[2,1,3]);

    hsurfaces = slice(X,Y,Z,normAtCheb,sx,sy,sz);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar;
end