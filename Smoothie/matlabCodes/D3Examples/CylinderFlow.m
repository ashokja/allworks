
function [U,V,W] = CylinderFlow(X,Y,Z)

    switch nargin
        case 0,
            x = linspace(-1,1,10);
            y = linspace(-1,1,10);
            z = linspace(-1,1,10);

            [X,Y,Z] = meshgrid(x,y,z);
            X = X(:); Y= Y(:); Z=Z(:);
    end

    R = (X.*X + Y.*Y).^(1/2);
    teta = atan(Y./X);
    U = Y.^2;
    V = -X.^2;
    W = ones(size(X));
        
end
