%clear;
% Load wind data
%load wind x y z u v w

% Compute speed
%spd = sqrt(u.*u + v.*v + w.*w);

figure

% Create isosurface patch
p = patch(isosurface(x, y, z, spd, 1));
isonormals(x, y, z, spd, p)
set(p, 'FaceColor', 'red', 'EdgeColor', 'none')

% Create isosurface end-caps
p2 = patch(isocaps(x, y, z, spd, 1));
set(p2, 'FaceColor', 'interp', 'EdgeColor', 'none')

% Adjust aspect ratio
daspect([1 1 1])

% Downsample patch
[f, verts] = reducepatch(isosurface(x, y, z, spd, 30), .2);

colormap(jet)
box on
axis tight
camproj perspective
camva(34)
campos([165 -20 65])
camtarget([100 40 -5])
camlight left
lighting gouraud