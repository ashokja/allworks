% Read xc0 and xc1 from file.
% Write u and v to file.

clear;
close all;
%rate = 3;
%samplerate = 6;
%r = [2,3,4];

%r = [7];
%samples= [20];

r = [2,3,4];
samples= [5,10,20];

W = 1.0;
R = 1.5;
G = 1;

for rate = r
    for samplerate = samples
        filename = strcat('P',int2str(rate),'_CubeGrid_',int2str(samplerate));
       
        xfname = strcat('../../data/NekMeshes/',filename,'_xc0','.txt' );
        yfname = strcat('../../data/NekMeshes/',filename,'_xc1','.txt' );
        zfname = strcat('../../data/NekMeshes/',filename,'_xc2','.txt' );
        
        xV = textread(xfname, '%f\t');
        yV = textread(yfname, '%f\t');
        zV = textread(zfname, '%f\t');

        xV = 3.0*xV+0.01;
        yV = 3.0*yV+0.01;
        zV = 3.0*zV-1.5;

%       xV = xV+0.1;
%       yV = yV+0.1;
%       zV = zV-0.5;

       [vel,u,v,w,Vor_x,Vor_y,Vor_z] = CalSpiralFlow_M2(xV,yV,zV,W,R,G);

       dlmwrite(strcat('../../data/NekMeshes/',filename,'_u.txt'),u','delimiter','\t','precision',25);
       dlmwrite(strcat('../../data/NekMeshes/',filename,'_v.txt'),v','delimiter','\t','precision',25);
       dlmwrite(strcat('../../data/NekMeshes/',filename,'_w.txt'),w','delimiter','\t','precision',25);

       dlmwrite(strcat('../../data/NekMeshes/',filename,'_VorX.txt'),Vor_x','delimiter','\t','precision',25);
       dlmwrite(strcat('../../data/NekMeshes/',filename,'_VorY.txt'),Vor_y','delimiter','\t','precision',25);
       dlmwrite(strcat('../../data/NekMeshes/',filename,'_VorZ.txt'),Vor_z','delimiter','\t','precision',25);

       disp(r);


% % % %         disp(sum(isnan(vel(:))));
% % % %         disp(sum(isnan(u(:))));
% % % %         disp(sum(isnan(v(:))));
% % % %         disp(sum(isnan(w(:))));
% % % %         
% % % %         disp(max((vel(:))));
% % % %         disp(max((u(:))));
% % % %         disp(max((v(:))));
% % % %         disp(max((w(:))));
% % % %         
% % % %         disp(min(abs(vel(:))));
% % % %         disp(min(abs(u(:))));
% % % %         disp(min(abs(v(:))));
% % % %         disp(min(abs(w(:))));
        
%         u = xV;
%         v = yV;
%         w = zV;

%         xV = 2.0*xV-1.0;
%         yV = 2.0*yV-1.0;
%         zV = 2.0*zV-1.0;
%         
%         [u,v,w] = CylinderFlow(xV,yV,zV);

        
%        dlmwrite(strcat('../../data/NekMeshes/',filename,'_u.txt'),u','delimiter','\t','precision',25);
%        dlmwrite(strcat('../../data/NekMeshes/',filename,'_v.txt'),v','delimiter','\t','precision',25);
%        dlmwrite(strcat('../../data/NekMeshes/',filename,'_w.txt'),w','delimiter','\t','precision',25);
 
    end
end
% n_uv = sqrt(u.*u+v.*v+w.*w);
% u = u./n_uv;
% v = v./n_uv;
% w = w./n_uv;
% % quiver(xV,yV,u,v);
% quiver3(xV,yV,zV,u,v,w);
% 
% %plot3(xV,yV,u);
% 
%    streamline 3D.
% sx =[0.75,0.7, 0.1];
% sy = [0.1,0.7,0.75];
% sz = [0];
% [startx,starty,startz] = meshgrid(sx,sy,sz);
% streamline(xV,yV,zV,u,v,w,startx,starty,startz);
