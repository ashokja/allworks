% Explanation: Polynomial projection error table along the analytical core
% line for voritices (Vector feild.)

clear;
close all;

N = [2:9];
%for NN = N
va = -70;
vb = 10;
addpath ../PolyLib
addpath ../createTestData
load('testData/jacX');
load('testData/jacWX');
sampleData = 50;
%    clearvars -except NN N
once = 5;
%Bring validation step here to increase the speed up;

    % % Data required for Validation step
     xvali = linspace(-1,1,6);
     yvali = linspace(-1,1,6);
     zvali = linspace(-1,1,6);
     [XV,YV,ZV] = meshgrid(xvali,yvali,zvali);
      XV = permute(XV,[2,1,3]); YV = permute(YV,[2,1,3]); ZV = permute(ZV,[2,1,3]);
     mapxyvali(:,1) = XV(:); mapxyvali(:,2) = YV(:); mapxyvali(:,3) = ZV(:);
     [xy,distance,t_a,mapxyVTrue,curveCoeff] = test_runXYZExt(1,mapxyvali);
     % curve location data.
     data = linspace(-1,1,sampleData);
     Cxdata = B_eval(curveCoeff(1,:),data);
     Cydata = B_eval(curveCoeff(2,:),data);
     Czdata = B_eval(curveCoeff(3,:),data);
     Cdata = [Cxdata;Cydata;Czdata]'; % Cdata is curve location data.
     [~,~,~,CVdata_Act,~] = test_runXYZExt(1,Cdata); %CVdata is value of Velcoity at Cdata.
     
for NN = N
    for nn = 2:5
        tic
        NX =NN; NY=NN; NZ=NN;
        nx=nn;ny=nn;nz=nn;
        clearvars mapxy mapxyV mapxyV_vali
        % call test_run to get the function defined at that location.
            XQ = XQN{NX+2+nx}; WXQ = WXQN{NX+2+nx};
            YQ = XQN{NY+2+ny}; WYQ = WXQN{NY+2+ny};
            ZQ = XQN{NZ+2+nz}; WZQ = WXQN{NZ+2+nz};

        [X,Y,Z] = meshgrid(XQ,YQ,ZQ);

        X = permute(X,[2,1,3]); Y = permute(Y,[2,1,3]); Z = permute(Z,[2,1,3]);
        mapxy(:,1) = X(:); mapxy(:,2) = Y(:); mapxy(:,3) = Z(:);
        [xy,distance,t_a,mapxyV,curveCoeff] = test_runXYZExt(1,mapxy);

        UAtGrid = reshape(mapxyV(:,1),[max(size(XQ)),max(size(YQ)),max(size(ZQ))]);
        VAtGrid = reshape(mapxyV(:,2),[max(size(XQ)),max(size(YQ)),max(size(ZQ))]);
        WAtGrid = reshape(mapxyV(:,3),[max(size(XQ)),max(size(YQ)),max(size(ZQ))]);

        % Vizualizing the actual core values.
        if (once ==1)
            figure(1)
    %        VizStreamLines(XQ,YQ,ZQ,UAtGrid,VAtGrid,WAtGrid);
            VizIsoSurface(XQ,YQ,ZQ,UAtGrid,VAtGrid,WAtGrid);
            once =3;
        end


        coeffU = CalLegendreCoeff(UAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);
        coeffV = CalLegendreCoeff(VAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);
        coeffW = CalLegendreCoeff(WAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);

    %      UAtVali = CalValueAt(coeffU,xvali,yvali,zvali);
    %      VAtVali = CalValueAt(coeffV,xvali,yvali,zvali);
    %      WAtVali = CalValueAt(coeffW,xvali,yvali,zvali);
    %      mapxyV_Vali(:,1) = UAtVali(:); mapxyV_Vali(:,2) = VAtVali(:); mapxyV_Vali(:,3) = WAtVali(:);

    % 
    %      er = abs(mapxyVTrue - mapxyV_Vali);
    %      erRatio = er./mapxyVTrue;
    %      MaxErAcrossEle(NN,nn) = max(erRatio(:));
    %      AvgErAcrossEle(NN,nn) = sum(erRatio(:))/max(size(erRatio(:)));
    %     er_cheb(NN,n) = max(abs(mapxyVTrue(:) - mapxyV_Vali(:)));

    %     Error Checks.
            UVAL = CalValueAtPts(coeffU,Cdata);
            VVAL = CalValueAtPts(coeffV,Cdata);
            WVAL = CalValueAtPts(coeffW,Cdata);
            CVdata_Proj = [UVAL,VVAL,WVAL];
            MaxErAcrossEle(NN,nn) = max(abs(CVdata_Proj(:) - CVdata_Act(:)));
            AvgErAcrossEle(NN,nn) = sum(abs(CVdata_Proj(:) - CVdata_Act(:)))/max(size(CVdata_Proj(:))) ;

    %

    %    calc_time(NN,n) = toc;
        toc
    end
end
    er_chebPLS = MaxErAcrossEle;
    er_cheb = er_chebPLS;
 %   UAtGrid = reshape(mapxyV(:,1),[max(size(XQ)),max(size(YQ)),max(size(ZQ))]);
 %   VAtGrid = reshape(mapxyV(:,2),[max(size(XQ)),max(size(YQ)),max(size(ZQ))]);
 %   WAtGrid = reshape(mapxyV(:,3),[max(size(XQ)),max(size(YQ)),max(size(ZQ))]);


 %   f1 = figure(1);
 %   Plot3DCurve(curveCoeff); title('Vortex axis curve'); view(va,vb); hold on;
 %   VizStreamLines(XQ,YQ,ZQ,UAtGrid,VAtGrid,WAtGrid); hold on;
 %   quiver3(X,Y,Z,UAtGrid,VAtGrid,WAtGrid);

%%

%     er_cheb = log10(er_chebPLS);
% 

% figure(1)
%     plot( 4+(1:10),er_cheb(4,:),'-c',...
%         5+(1:10),er_cheb(5,:),'-*r',...
%         6+(1:10),er_cheb(6,:),'-.g',...
%         7+(1:10),er_cheb(7,:),'-ob',...
%         8+(1:10),er_cheb(8,:),'-.c',...
%         9+(1:10), er_cheb(9,:),'-ks',...
%         10+(1:10),er_cheb(10,:),'k--'); hold on;
%     legend('D 4','D 5','D 6', 'D 7','D 8','D 9', 'D 10');
%     text(4,er_cheb(4,1),' D(4)');
%     text(5,er_cheb(5,1),' D(5)');
%     text(6,er_cheb(6,1),' D(6)');
%     text(7,er_cheb(7,1),' D(7)');
%     text(8,er_cheb(8,1),' D(8)');
%     text(9,er_cheb(9,1),' D(9)');
%     text(10,er_cheb(10,1),' D(10)');
%     xlabel('Number of Points')
%     ylabel('log10(Error)')
%     title('Max error at (6,6,6) Points for Bent Vortex')
    er_cheb = MaxErAcrossEle;
figure(1)
    plot( N,er_cheb(N,2),'*-c',...
          N,er_cheb(N,3),'*-r',...
          N,er_cheb(N,4),'o-g',...
          N,er_cheb(N,5),'o-r'); hold on;
    legend('k 2','k 3','k 4','k 5');
    text(2,er_cheb(2,2),' k-2');
    text(2,er_cheb(2,3),' k-3');
    text(2,er_cheb(2,4),' k-4');
    text(2,er_cheb(2,5),' k-5');
    xlabel('Polynomial Degree(N)')
    ylabel('Max Error among 50 points')
    title('Maximum Error Bent Vortex (Qpts=N+2+k)')
    %Each curve/line has quadraturepoints(N+2+k)
    %The aim of this graph is to show, as projection basis functions
    %change. Increasing quadrature points will decrease the error in
    %consistent way.
    
    
er_cheb =AvgErAcrossEle;
figure(2)
    plot( N,er_cheb(N,2),'*-c',...
          N,er_cheb(N,3),'*-r',...
          N,er_cheb(N,4),'o-g',...
          N,er_cheb(N,5),'o-r'); hold on;
    legend('k 2','k 3','k 4','k 5');
    text(2,er_cheb(2,2),' k-2');
    text(2,er_cheb(2,3),' k-3');
    text(2,er_cheb(2,4),' k-4');
    text(2,er_cheb(2,5),' k-5');
    xlabel('Polynomial Degree(N)')
    ylabel('Avg Error at (50) points')
    title('Average Error Bent Vortex (Qpts=N+2+k)')
    %Each curve/line has quadraturepoints(N+2+k)
    %The aim of this graph is to show, as projection basis functions
    %change. Increasing quadrature points will decrease the error in
    %consistent way.

