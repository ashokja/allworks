% testSuite 202.m Checking for Idempodent.

clear;
close all;
% test 
%rates = [10,20,40,60,80];
rates = [21];

poly =2;
order = 2;
count =1;
for r = rates
    
    xfname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_xc0_2D_OneSided2kp1.txt' );
    yfname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_xc1_2D_OneSided2kp1.txt' );
    ffname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_fce_2D_OneSided2kp1.txt' );
    efname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_ece_2D_OneSided2kp1.txt' );
    sfname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_sce_2D_OneSided2kp1.txt' );

    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');
    fval = textread(ffname, '%f\t');
    pval = textread(efname, '%f\t');
    sval = textread(sfname, '%f\t');

    figure(1)
    plot3(xpos,ypos,abs(log10(abs(fval-pval))),'*'); hold on;
    figure(2) 
    plot3(xpos,ypos,abs(log10(abs(fval-sval))),'*'); hold on;
    
%    figure(2)
%   scatter(xpos,ypos,log10(abs(fval-sval)),'DisplayName',strcat('No of ele:',int2str(r)));
%    hold on;
    
end
