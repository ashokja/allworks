#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include <MultiRegions/DisContField2D.h>
#include <iomanip>

#define MAX_ITER 10000
#define MAX_ITER_NI 100
#define TOLERENCE_S 1e-16
#define TOLERENCE_F 1e-7
#define TOLERENCE_P 1e-14
#define TOLERENCE_A 1e-14
#define TOLERENCE_NN 1e-8
#define ALPHAMAX 1.0

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>

using namespace std;

void calVorticity(int nq, Array<OneD,NekDouble> &udy_phys, Array<OneD,NekDouble> &vdx_phys,
    Array<OneD,NekDouble> &Vz )
{
    Vmath::Vsub(nq, udy_phys,1, vdx_phys,1,Vz ,1);
    return;
}


int main(int argc, char* argv[])
{
	if (argc != 6)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg 0/1"<<endl<<"\t 0 for writing data"<< endl<<"\t 1 for reading data." << endl;
		cout << "3rd Polynomial order of SIAC filter" << endl;
		cout << "4th Scaling of SICA filter" << endl;
		cout << "5nd arg Mesh resolution you want to write to." << endl;
		
		//cout << "3nd arg polynomial degree filter you want to apply" << endl;
		//cout << "4rd arg meshscaling you want to use." << endl;
	//	cout << "4th Angle of SIAC derivative in deg -90 to 90 allowed" << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();

//	HNM2D->LoadData( fname+ ".chk",var);

	HNM2D->LoadMesh(var[0]);
	HNM2D->LoadMesh(var[1]);

	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
//	cout << "fPhys:" << tNquadPts<< endl;
//	cout << "fCoeffs:" << HNM2D->m_expansions[0]->GetNcoeffs()<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);
	SpatialDomains::MeshGraphSharedPtr graph2D =
            SpatialDomains::MeshGraph::Read(vSession);

	const SpatialDomains::ExpansionMap &expansions = graph2D->GetExpansions();
	 LibUtilities::BasisKey              bkey0      =   
           expansions.begin()->second->m_basisKeyVector[0];
	cout <<"OldNumModes"<<bkey0.GetNumModes() << endl;
	 int poly = bkey0.GetNumModes()-1;
	graph2D->SetExpansionsToPolyOrder( 2*poly+1+1 );

	MultiRegions::ExpListSharedPtr Exp_u, Exp_v;	
	Exp_u = MemoryManager<MultiRegions::DisContField2D>
            ::AllocateSharedPtr(vSession,graph2D,vSession->GetVariable(0));
    //Exp_tor = MemoryManager<MultiRegions::DisContField2D>
      //      ::AllocateSharedPtr(vSession,graph2D,vSession->GetVariable(1));

	//cout << "NewNummodes"<<Exp_cur.begin()->second->m_basisKeyVector[0].GetNumModes() << endl;
	
	int New_tNquadPts = Exp_u->GetTotPoints();

	Array<OneD,NekDouble> uy(New_tNquadPts);
	Array<OneD,NekDouble> u(tNquadPts);
	Array<OneD,NekDouble> ux(New_tNquadPts);
	Array<OneD,NekDouble> v(tNquadPts);
	Array<OneD,NekDouble> vx(New_tNquadPts);
	Array<OneD,NekDouble> vy(New_tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}

	NektarBaseClass k;
	if( 0 == atoi(argv[2]) )
	{	
		k.writeNekArray(xc0,fname+"_xc0."+"txt");
		k.writeNekArray(xc1,fname+"_xc1."+"txt");
		return 0;
	}
	else
	{
		k.readNekArray(u, fname+"_u.txt");
		k.readNekArray(ux, fname+"_ux.txt");
		k.readNekArray(uy, fname+"_uy.txt");
		k.readNekArray(v, fname+"_v.txt");
		k.readNekArray(vx, fname+"_vx.txt");
		k.readNekArray(vy, fname+"_vy.txt");
		//k.printNekArray(v,0);
	}



	// Map the sample points to coeffeicents.
	HNM2D->m_expansions[0]->FwdTrans_IterPerExp( u,HNM2D->m_expansions[0]->UpdateCoeffs());
	HNM2D->m_expansions[0]->BwdTrans_IterPerExp( HNM2D->m_expansions[0]->GetCoeffs(),
			HNM2D->m_expansions[0]->UpdatePhys());
	
	HNM2D->m_expansions[1]->FwdTrans( v,HNM2D->m_expansions[1]->UpdateCoeffs());
	HNM2D->m_expansions[1]->BwdTrans( HNM2D->m_expansions[1]->GetCoeffs(),
			HNM2D->m_expansions[1]->UpdatePhys());

	// Map the sample Get physical points.
	Array<OneD,NekDouble> u_DG = HNM2D->m_expansions[0]->GetPhys();	
	Array<OneD,NekDouble> v_DG = HNM2D->m_expansions[1]->GetPhys();

	// load data in to m_Arrays.
	HNM2D-> m_Arrays.push_back(u_DG);
	HNM2D-> m_Arrays.push_back(v_DG);

	HNM2D->LoadExpListIntoRTree();
	//TODO : set the parameters.
	SmoothieSIAC2D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_4kp1, HNM2D, atoi(argv[3]), atof(argv[4]), 1);
	SmoothieSIAC2D sm(SIACUtilities::eSYM_DER_2kp1_1SIDED_4kp1, HNM2D, atoi(argv[3]), atof(argv[4]));
	
	Array<OneD,NekDouble> Nxc0(New_tNquadPts), Nxc1(New_tNquadPts);
	Exp_u->GetCoords(Nxc0,Nxc1);

	// Derivatives of points.
	Array<OneD,NekDouble> ux_DG(tNquadPts), uy_DG(tNquadPts), vx_DG(tNquadPts), vy_DG(tNquadPts);
	HNM2D->m_expansions[0]->PhysDeriv( u_DG, ux_DG, uy_DG);
	HNM2D->m_expansions[1]->PhysDeriv( v_DG, vx_DG, vy_DG);

	Array<OneD,NekDouble> directionX(3,0.0), directionY(3,0.0);
	directionX[0] = 1.0;
	directionY[1] =1.0;

// The code below is sampling the data field at various points.
//	int Nres = atoi(argv[5]);
//	int Nx = Nres, Ny = Nres;
//	int totPts = Nx*Ny;
//	NekDouble sx = 1.0/(Nx-1.0), sy= 1.0/(Ny-1.0);
//	vector< NekDouble> Px(tNquadPts), Py(tNquadPts);
//	vector< NekDouble> UX(tNquadPts), UY(tNquadPts), U(tNquadPts);
//	vector< NekDouble> VX(tNquadPts), VY(tNquadPts), V(tNquadPts);	
	Array<OneD, NekDouble> sUX(New_tNquadPts,0.0), sUY(New_tNquadPts,0.0), sU(New_tNquadPts,0.0);
	Array<OneD, NekDouble> sVX(New_tNquadPts,0.0), sVY(New_tNquadPts,0.0), sV(New_tNquadPts,0.0);
	Array<OneD,NekDouble> uy_DGN(New_tNquadPts), vx_DGN(New_tNquadPts);
	Array<OneD,NekDouble> coord(3,0.0);
	NekDouble valY=0.0, valZ=0.0;
	/*
	for(int index =0; index< tNquadPts;index++)
	{
		sm.EvaluateAt(xc0[index],xc1[index], 0.0, sU[index], valY,valZ,directionX,atof(argv[4]),0);
		smD.EvaluateAt(xc0[index],xc1[index], 0.0, sUX[index], valY,valZ,directionX,atof(argv[4]),0);
		smD.EvaluateAt(xc0[index],xc1[index], 0.0, sUY[index], valY,valZ,directionY,atof(argv[4]),0);

		sm.EvaluateAt(xc0[index],xc1[index], 0.0, sV[index], valY,valZ,directionY,atof(argv[4]),1);
		smD.EvaluateAt(xc0[index],xc1[index], 0.0, sVX[index], valY,valZ,directionX,atof(argv[4]),1);
		smD.EvaluateAt(xc0[index],xc1[index], 0.0, sVY[index], valY,valZ,directionY,atof(argv[4]),1);
		
	}*/
	vector<int> elIds,glIds;
	HNM2D->IntersectWithBoxUsingRTree( -1.0+0.000625, -1.0+0.000625, -0.01, 
										1.0-0.000625, 1.0-0.000625, 0.01 , elIds, glIds);
	//HNM2D->IntersectWithBoxUsingRTree( 0.25+0.000625, 0.25+0.000625, -0.01, 
	//									0.75-0.000625, 0.75-0.000625, 0.01 , elIds, glIds);
	cout << "Total Elem" << "Elements taken into consideration" << endl;
	cout << HNM2D->m_expansions[0]->GetExpSize()<< "\t" <<elIds.size() << endl;
	Array<OneD,NekDouble> lcoord(3,0.0);
	for(int i=0; i<elIds.size() ;i++)
	{
		int elmID = elIds[i];
		//cout << i <<" \t "<< elmID << endl;
		LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp( elmID );
		int phys_offset = Exp_u->GetPhys_Offset(elmID);
		Array<OneD,NekDouble> uy_phys_old = uy_DG.CreateWithOffset(uy_DG,
										HNM2D->m_expansions[0]->GetPhys_Offset(elmID)); 
		Array<OneD,NekDouble> vx_phys_old = vx_DG.CreateWithOffset(vx_DG, 
										HNM2D->m_expansions[0]->GetPhys_Offset(elmID)); 
		for (int j =0; j < Exp_u->GetExp(0)->GetTotPoints(); j++)
		{
			int index = j+phys_offset;
			//sm.EvaluateAt(Nxc0[index],Nxc1[index], 0.0, sU[index], valY,valZ,directionX,atof(argv[4]),0);
			//smD.EvaluateAt(Nxc0[index],Nxc1[index], 0.0, sUX[index], valY,valZ,directionX,atof(argv[4]),0);
			smD.EvaluateAt(Nxc0[index],Nxc1[index], 0.0, sUY[index], valY,valZ,directionY,atof(argv[4]),0);

			//sm.EvaluateAt(Nxc0[index],Nxc1[index], 0.0, sV[index], valY,valZ,directionY,atof(argv[4]),1);
			smD.EvaluateAt(Nxc0[index],Nxc1[index], 0.0, sVX[index], valY,valZ,directionX,atof(argv[4]),1);
			//smD.EvaluateAt(Nxc0[index],Nxc1[index], 0.0, sVY[index], valY,valZ,directionY,atof(argv[4]),1);
			lcoord[0] = Nxc0[index]; lcoord[1] = Nxc1[index];
			uy_DGN[index] = lexp->PhysEvaluate(lcoord,uy_phys_old);
			vx_DGN[index] = lexp->PhysEvaluate(lcoord,vx_phys_old);
		} 
	}
	Array<OneD,NekDouble> vort_DG(New_tNquadPts),vort_Raw(New_tNquadPts),vort_SIAC(New_tNquadPts);
	calVorticity( New_tNquadPts, uy_DGN, vx_DGN, vort_DG );
	calVorticity( New_tNquadPts, uy, vx, vort_Raw );
	calVorticity( New_tNquadPts, sUY, sVX, vort_SIAC );

//	cout << "L2 error DG:\t" << HNM2D->m_expansions[0]->L2(vort_Raw,vort_DG);
//	cout << "\t SIAC\t" << HNM2D->m_expansions[0]->L2(vort_Raw,vort_SIAC) << endl;
	
//	cout << "Linf error DG:\t" << HNM2D->m_expansions[0]->Linf(vort_Raw,vort_DG);
//	cout << "\t SIAC\t" << HNM2D->m_expansions[0]->Linf(vort_Raw,vort_SIAC) << endl;


	NekDouble L2_SIAC=0.0, L2_DG=0.0;
	NekDouble Linf_SIAC=0.0, Linf_DG=0.0;
	for(int i=0; i<elIds.size() ;i++)
	{
		int index = elIds[i];
		//LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp( index );
		LocalRegions::ExpansionSharedPtr lexp = Exp_u->GetExp( index );
		int phys_offset = Exp_u->GetPhys_Offset(index);
		Array<OneD,NekDouble> el_VR = vort_Raw.CreateWithOffset( vort_Raw, phys_offset);
		Array<OneD,NekDouble> el_VD = vort_DG.CreateWithOffset( vort_DG, phys_offset);
		Array<OneD,NekDouble> el_VS = vort_SIAC.CreateWithOffset( vort_SIAC, phys_offset);
		L2_DG += ( lexp->L2(el_VR,el_VD)*lexp->L2(el_VR,el_VD)) ; 
		L2_SIAC += ( lexp->L2(el_VR,el_VS)*lexp->L2(el_VR,el_VS));
		Linf_DG = std::max(Linf_DG,  lexp->Linf(el_VR,el_VD)); 
		Linf_SIAC =std::max(Linf_SIAC, lexp->Linf(el_VR,el_VS));
//		//cout << index <<"\t" << L2_DG <<"\t"<< L2_SIAC << endl;  
//		cout << index <<"\t" << lexp->L2(el_VR,el_VD) <<"\t"<< lexp->L2(el_VR,el_VS) << endl;  
	}

//	cout << "DG L2 SelArea " << std::sqrt(L2_DG) << endl;
//	cout << "SIAC L2 SelArea " << std::sqrt(L2_SIAC) << endl;

	cout << "Error at selective places" << endl;	
	cout << "L2 error DG:\t" << std::sqrt(L2_DG);  
	cout << "\t SIAC\t" <<  std::sqrt(L2_SIAC) << endl;
	
	cout << "Linf error DG:\t" << Linf_DG ;
	cout << "\t SIAC\t" <<Linf_SIAC << endl;

//	HNM2D->writeNekArray(Px, fname+"_"+argv[5]+"_SampleDeriv_x.txt");	
//	HNM2D->writeNekArray(Py, fname+"_"+argv[5]+"_SampleDeriv_y.txt");	
//	HNM2D->writeNekArray(U, fname+"_"+argv[5]+"_SampleDeriv_u.txt");	
//	HNM2D->writeNekArray(UX, fname+"_"+argv[5]+"_SampleDeriv_ux.txt");	
//	HNM2D->writeNekArray(UY, fname+"_"+argv[5]+"_SampleDeriv_uy.txt");	
//	HNM2D->writeNekArray(V, fname+"_"+argv[5]+"_SampleDeriv_v.txt");	
//	HNM2D->writeNekArray(VX, fname+"_"+argv[5]+"_SampleDeriv_vx.txt");	
//	HNM2D->writeNekArray(VY, fname+"_"+argv[5]+"_SampleDeriv_vy.txt");	
	
//	HNM2D->writeNekArray(sU, fname+"_"+argv[5]+"_SampleDeriv_su.txt");	
//	HNM2D->writeNekArray(sUX, fname+"_"+argv[5]+"_SampleDeriv_sux.txt");	
//	HNM2D->writeNekArray(sUY, fname+"_"+argv[5]+"_SampleDeriv_suy.txt");	
//	HNM2D->writeNekArray(sV, fname+"_"+argv[5]+"_SampleDeriv_sv.txt");	
//	HNM2D->writeNekArray(sVX, fname+"_"+argv[5]+"_SampleDeriv_svx.txt");	
//	HNM2D->writeNekArray(sVY, fname+"_"+argv[5]+"_SampleDeriv_svy.txt");	

	return 0;
}
