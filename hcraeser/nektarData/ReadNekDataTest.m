% open read new data output by nektar.

clear;
close all;
fileid = fopen('loadData/ForMatlabReading.txt');

%number of variables.
numVarC = textscan(fileid,'%f',1);
numVar = cell2mat(numVarC);

for nv = 1:numVar
    VarnameC = textscan(fileid,'%s',1);
    
    formatSpec = '%f,%f,%f';    
    C = textscan(fileid,formatSpec,1);
    sizes = cell2mat(C);
    N = prod(sizes);
    Ar_C = textscan(fileid,'%f',N,'Delimiter',',');
    Ar_M = cell2mat(Ar_C);
    Ar = reshape(Ar_M,sizes);
    %Ar = permute(Ar,[2,1,3]);
    v = char(VarnameC{1});
    varname = genvarname(v);
    eval([varname '=Ar;']);
end



figure(1)
VizNekSurface(msh_x,msh_y,msh_z,vel_u,vel_v,vel_w);

figure(2)
VizNekSurface(msh_x,msh_y,msh_z,a_x,a_y,a_z);

figure(3)
VizNekSurface(msh_x,msh_y,msh_z,b_x,b_y,b_z);

figure(4)
VizNekSurfaceScalar(msh_x,msh_y,msh_z,cur);
%%
figure(5)
VizNekSurfaceScalar(msh_x,msh_y,msh_z,tor);
%%
figure(6)
%  VizNekIsoSurfaceScalar(msh_x,msh_y,msh_z,tor,-10,'red'); hold on;
%  VizNekIsoSurfaceScalar(msh_x,msh_y,msh_z,tor,-0.2,'red'); hold on;
%  VizNekIsoSurfaceScalar(msh_x,msh_y,msh_z,tor,-0.3,'red'); hold on;
%  VizNekIsoSurfaceScalar(msh_x,msh_y,msh_z,tor, 10,'blue'); hold on;
%  VizNekIsoSurfaceScalar(msh_x,msh_y,msh_z,tor, 0.2,'blue'); hold on;
%  VizNekIsoSurfaceScalar(msh_x,msh_y,msh_z,tor, 0.3,'blue'); hold on;
[T_fvs,T_pts] = VizNekIsoSurfaceScalar(msh_x,msh_y,msh_z,tor, 0,'green'); hold on;
msh_xx = permute(msh_x, [2,1,3]);
msh_yy = permute(msh_y, [2,1,3]); 
msh_zz = permute(msh_z, [2,1,3]);

dotBV = permute(dotBV, [2,1,3]);
BV_Tor0 = interp3(msh_xx,msh_yy,msh_zz,dotBV,...
                            T_pts(:,1),T_pts(:,2),T_pts(:,3));

figure(7)
patch('Faces',T_fvs,'Vertices',T_pts,'FaceVertexCData', BV_Tor0, 'FaceColor','interp','edgecolor', 'interp'); hold on;

