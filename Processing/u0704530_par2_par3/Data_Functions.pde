// 
ArrayList<PVector> dataToControlPoints (ArrayList<PVector> data, ArrayList<Float> knotA, int k)
{
  // can check if data values are same as dof .i.e. data.size() = knotA.size() -k -1; 
  int coord =2;
  float[] datax = new float[data.size()];
  float[] datay = new float[data.size()];
  float[] dataz = new float[data.size()];
  for (int i =0; i < data.size(); i++)
  {
    PVector p = data.get(i);
    datax[i]=p.x; datay[i]=p.y;dataz[i]=p.z;
  }
  ArrayList<Float> s_t = KnotToInterParam(knotA,k);
  int m_1 = s_t.size();
  print("These two sizes should be same "+m_1+ " "+data.size()+ " "+knotA.size()+" "+k+"\n");
  float[][] A = new float[m_1][m_1] ;
  // Fill the matrix
  for (int j =0; j< m_1; j++)
  {
    for (int i=0; i< m_1; i++)
    {
      A[i][j] = b_splineATt( j, k, knotA,s_t.get(i));
    }
  }

  QR qr= new QR(A);
  double[] px = qr.solve(datax);
  double[] py = qr.solve(datay);
  double[] pz = qr.solve(dataz);
  
  ArrayList<PVector> points = new ArrayList<PVector> ();
  for(int i=0; i< data.size(); i++)
  {
    PVector p = new PVector((float)px[i],(float)py[i],(float)pz[i]);
    points.add(p);
  }
  
  return points;
}

//
float b_splineATt(int i , int k, ArrayList<Float> knotA, float t)
{
  // If t is out of support of B_i return 0 else contiue;
  float lt = knotA.get(i); float rt = knotA.get(i+k+1);
  //print(lt); print(rt);
  if (t < lt || t >=rt)
  {
        return 0.0;
  }
  // If k=0 return 1 else continue;
  if ( 0 == k)
      return 1.0;
      
  // If left Denominator is inf assign 0;
  float ld = knotA.get(i+k) - knotA.get(i);
  float lterm = 0.0;
  if (ld > 0.00001)
  {
   lterm = (t - knotA.get(i))/ld * b_splineATt(i,k-1,knotA,t); 
  }
  
  // If right Denominator is inf assing 0;
  float rd = knotA.get(i+k+1) - knotA.get(i+1);
  float rterm = 0.0;
  if (rd > 0.0001)
  {
    rterm = (knotA.get(i+k+1)-t)/rd * b_splineATt(i+1, k-1, knotA,t);
  }
  
  // Final computation
  
  return lterm+rterm;
}





// Given Knot Vectors returns interpolation parameter values.
// Input: knot Vector
// Output: data Interpolation values at nodes.
ArrayList<Float> KnotToInterParam(ArrayList<Float> knotA, int k )
{
  ArrayList<Float> intParam = new ArrayList<Float>();
  // determine number of control points.
  // number of interpolation points are also the same.
  int numKs = knotA.size();
  int numCpts = numKs-k-1;
  
  for (int i =0; i < numCpts; i++)
  {
    float d =0;
    for (int t=1; t<= k; t++)
    {
      d = d+ knotA.get(i+t);
    }
    intParam.add(d/(int)k);
  }
  return intParam;
}
