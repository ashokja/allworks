%CreatingCritical Point data from Mike's Previous papers.

%1st form.
clear;
close all;
Npts = 20;
N = [40]
count =1;
for Npts = N
NXpts = Npts; NYpts = Npts;
a1 = 0.74+0.35i;
a2 = 0.68-0.59i;
a3 =-0.11-0.72i;
b1 =-0.58+0.64i;
b2 = 0.51-0.27i;
b3 =-0.12+0.84i;

x = linspace(-1,1,NXpts);
y = linspace(-1,1,NYpts);

[xV,yV] = meshgrid(x,y);

zV = xV+yV*1i;
zVb = xV-yV*1i;


Val = (zV-a1).*(zV-a2).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2;

u = real(Val); 
v = -1*imag(Val);

n_uv = sqrt(u.*u+v.*v);
u = u./n_uv;
v = v./n_uv;

figure(count)
quiver(xV,yV,u,v,0.35);hold on;
axis([-1,1,-1,1])

% plot all ciritical points.
symbol = 'r*';
plot(0.74,0.35,symbol);
plot(0.68,-0.59,symbol);
plot(-0.11,-0.72,symbol);

plot(-0.58,-0.64,symbol);
plot(0.51,0.27,symbol);
plot(-0.12,-0.84,symbol);

count = count+1;

end

figure(1)
print(strcat('paperUseImagesF/','FieldVis'),'-dpng','-r500');

% startx = 0:0.05:1;
% starty = 0:0.05:1;
% [Sx,Sy]= meshgrid(startx,starty);
% %starty = ones(size(startx));
% 
% streamline(xV,yV,u,v,Sx,Sy)