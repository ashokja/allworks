clear;
%playing with laod wind optoins.
load wind;
quiver3(x,y,z,u,v,w);
% but want to sample at much lesser intervals.
X = (0:.1:1);
xmax = max(max(max(x)));
xmin = min(min(min(x)));
ymax = max(max(max(y)));
ymin = min(min(min(y)));
zmax = max(max(max(z)));
zmin = min(min(min(z)));

[xq,yq,zq] = meshgrid(X,X,X);
xq = xq.*(xmax-xmin)+xmin;
yq = yq.*(ymax-ymin)+ymin;
zq = zq.*(zmax-zmin)+zmin;

uq = interp3(x,y,z,u,xq,yq,zq);
vq = interp3(x,y,z,v,xq,yq,zq);
wq = interp3(x,y,z,w,xq,yq,zq);
quiver3(xq,yq,zq,uq,vq,wq);