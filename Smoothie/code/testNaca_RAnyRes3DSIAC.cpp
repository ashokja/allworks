/// This test file will simulate any of 9 single derivatives depending on input parameters.

#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"
//#include <ctime>



using namespace SIACUtilities;
using namespace std;

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		cout << "4th which parameter do you want to simulate select 1-9." << endl;
		return 0;
	}
	// This is so that vSession does not interpret extra parameters absurdly.
	argc = 2;

	clock_t startTime = clock();
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);

	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	//HNM3D->LoadMesh(var[0]);
	//HNM3D->LoadMesh(var[1]);
	//HNM3D->LoadMesh(var[2]);
	
	cout << "loading xml file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	string fname = vSession->GetSessionName();

	//string fldname = fname + ".fld/Info.xml";	
	//string fldname = "naca.fld";	
	string fldname = fname + ".fld";	
	cout << fname << endl;
	cout << fldname << endl;

	HNM3D->LoadData( fldname ,var);
	cout << "loading fld file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	cout << "expsize:\t"<< HNM3D->m_expansions[0]->GetExpSize() << endl;
	
	HNM3D->LoadExpListIntoRTree();
	cout << "loading into R-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	/*
	for (int i =0; i< HNM3D->m_expansions[0]->GetExpSize();i++)
	{
		int gid = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetGlobalID();
		cout << "i\t" << i << "\tg\t" << gid << endl;
		cout << HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetShapeType()<< endl;
	}
	*/
	// Find minx,miny,minz	
	// Find maxx,maxy,maxz	
	cout << HNM3D->m_expansions[0]->GetTotPoints() << endl;
	cout << HNM3D->m_expansions[0]->GetNcoeffs() << endl;
	
	int totPhys = HNM3D->m_expansions[0]->GetTotPoints();
	Array<OneD,NekDouble> xc0(totPhys),xc1(totPhys), xc2(totPhys);
	
	HNM3D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
	NekDouble minx = Vmath::Vmin(totPhys,xc0,1);	
	NekDouble miny = Vmath::Vmin(totPhys,xc1,1);	
	NekDouble minz = Vmath::Vmin(totPhys,xc2,1);	
	NekDouble maxx = Vmath::Vmax(totPhys,xc0,1);	
	NekDouble maxy = Vmath::Vmax(totPhys,xc1,1);	
	NekDouble maxz = Vmath::Vmax(totPhys,xc2,1);	

	cout << "min\t " << minx << "\t" << miny<<"\t"<< minz<< endl; 
	cout << "max\t " << maxx << "\t" << maxy<<"\t"<< maxz<< endl; 
	
	const Array<OneD,NekDouble> u_phys = HNM3D->m_expansions[0]->GetPhys();
	const Array<OneD,NekDouble> v_phys = HNM3D->m_expansions[1]->GetPhys();
	const Array<OneD,NekDouble> w_phys = HNM3D->m_expansions[2]->GetPhys();
	const Array<OneD,NekDouble> p_phys = HNM3D->m_expansions[3]->GetPhys();

	Array<OneD,NekDouble> u_sx_phys(totPhys), v_sx_phys(totPhys), w_sx_phys(totPhys);
	Array<OneD,NekDouble> u_sy_phys(totPhys), v_sy_phys(totPhys), w_sy_phys(totPhys);
	Array<OneD,NekDouble> u_sz_phys(totPhys), v_sz_phys(totPhys), w_sz_phys(totPhys);
	Array<OneD,NekDouble> ux_phys(totPhys), uy_phys(totPhys), uz_phys(totPhys);
	Array<OneD,NekDouble> vx_phys(totPhys), vy_phys(totPhys), vz_phys(totPhys);
	Array<OneD,NekDouble> wx_phys(totPhys), wy_phys(totPhys), wz_phys(totPhys);
	Array<OneD,NekDouble> au_phys(totPhys), av_phys(totPhys), aw_phys(totPhys);
	Array<OneD,NekDouble> Cu_phys(totPhys), Cv_phys(totPhys), Cw_phys(totPhys);
	Array<OneD,NekDouble> C2_phys(totPhys);

	HNM3D->m_expansions[0]->PhysDeriv(u_phys, ux_phys,uy_phys,uz_phys);
	HNM3D->m_expansions[1]->PhysDeriv(v_phys, vx_phys,vy_phys,vz_phys);
	HNM3D->m_expansions[2]->PhysDeriv(w_phys, wx_phys,wy_phys,wz_phys);
	
	cout << "Time taken to calculate 3 derivatives. "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
			
	//minx = 2.1; miny = 0.0; minz = 0.6;
	//maxx = 2.5; maxy = 0.5; maxz= 1.1;
	minx = 2.1; miny = 0.15; minz = 0.725;
	maxx = 2.5; maxy = 0.35; maxz= 0.925;
	//SmoothieSIAC3D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]),atof(argv[3]) ); 
	//SmoothieSIAC3D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]),atof(argv[3]),1); 
	
	NekDouble valY,valZ, scaling;
	scaling = atof(argv[3]);
	Array<OneD,NekDouble> directionX(3,0.0),directionY(3,0.0),directionZ(3,0.0);
	directionX[0] = 1.0;	directionY[1] = 1.0;	directionZ[2] = 1.0;
	
	vector<Array<OneD,NekDouble> > directionsXYZ,directionsYXZ, directionsXZY;
	std::shared_ptr<SmoothieSIAC> sm = std::make_shared<SmoothieSIAC3D> (SIACUtilities::eSYM_2kp1_1SIDED_2kp1,
				HNM3D, atoi(argv[2]), atof(argv[3]) );
	std::shared_ptr<SmoothieSIAC> smD = std::make_shared<SmoothieSIAC3D> (SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1,
				HNM3D, atoi(argv[2]), atof(argv[3]) ,1);

//  Get List of expansions in space.
// 	Loop through the expansions and 1DLSIAC first.
	startTime = clock();
	vector<int> eIds, gIds;
	HNM3D->IntersectWithBoxUsingRTree( 2.25-0.11 , miny-0.11, minz-0.11,
					2.25 + 0.11, maxy+0.11, maxz+0.11,
					eIds,gIds);
	//HNM3D->IntersectWithBoxUsingRTree( minx, miny, minz,
	//							maxx, maxy, maxz,
	//							eIds,gIds);
	
	cout << "Time taken to find the number of eIds."<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	cout << "Number of Eids " << eIds.size()<< endl;

	for( auto const& elId:eIds)
	{
		// get Phys offset.
		// loop through number of phys in each offset.
			// apply each lSIAC at each point in phys.
		int el_phys_offset = HNM3D->m_expansions[0]->GetPhys_Offset(elId);
		int el_num_phys = HNM3D->m_expansions[0]->GetExp(elId)->GetTotPoints(); 
		cout << elId << "\t" << el_phys_offset<< "\t"<< el_num_phys << endl;
		for(int i =0 ; i < el_num_phys ; i++)
		{
			int index = i+el_phys_offset;
					switch( atoi(argv[4]))
					{
						case 1:
							sm->EvaluateAt(xc0[index],xc1[index],xc2[index],u_sx_phys[index],valY,valZ, directionX, scaling ,0);
							break;
						case 2:
							sm->EvaluateAt(xc0[index],xc1[index],xc2[index],v_sx_phys[index],valY,valZ, directionX, scaling ,1);
							break;
						case 3:
							sm->EvaluateAt(xc0[index],xc1[index],xc2[index],w_sx_phys[index],valY,valZ, directionX, scaling ,2);
							break;
						case 4:
							sm->EvaluateAt(xc0[index],xc1[index],xc2[index],u_sy_phys[index],valY,valZ, directionY, scaling ,0);
							break;
						case 5:
							sm->EvaluateAt(xc0[index],xc1[index],xc2[index],v_sy_phys[index],valY,valZ, directionY, scaling ,1);
							break;
						case 6:
							sm->EvaluateAt(xc0[index],xc1[index],xc2[index],w_sy_phys[index],valY,valZ, directionY, scaling ,2);
							break;
						case 7:
							sm->EvaluateAt(xc0[index],xc1[index],xc2[index],u_sz_phys[index],valY,valZ, directionZ, scaling ,0);
							break;
						case 8:
							sm->EvaluateAt(xc0[index],xc1[index],xc2[index],v_sz_phys[index],valY,valZ, directionZ, scaling ,1);
							break;
						case 9:
							sm->EvaluateAt(xc0[index],xc1[index],xc2[index],w_sz_phys[index],valY,valZ, directionZ, scaling ,2);
							break;
					}	
		}
		//break;
	}
	cout << "Time taken to find the number of eIds."<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	NektarBaseClass k;
	
	switch( atoi(argv[4]))
	{
		case 1:
			k.writeNekArray(eIds,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pIds_phys.txt");
			k.writeNekArray(u_sx_phys,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pU_sx_phys.txt");
			break;
		case 2:
			k.writeNekArray(v_sx_phys,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pV_sx_phys.txt");
			break;
		case 3:
			k.writeNekArray(w_sx_phys,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pW_sx_phys.txt");
			break;
		case 4:
			k.writeNekArray(u_sy_phys,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pU_sy_phys.txt");
			break;
		case 5:
			k.writeNekArray(v_sy_phys,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pV_sy_phys.txt");
			break;
		case 6:
			k.writeNekArray(w_sy_phys,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pW_sy_phys.txt");
			break;
		case 7:
			k.writeNekArray(u_sz_phys,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pU_sz_phys.txt");
			break;
		case 8:
			k.writeNekArray(v_sz_phys,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pV_sz_phys.txt");
			break;
		case 9:
			k.writeNekArray(w_sz_phys,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pW_sz_phys.txt");
			break;
	}
	/*


	for (int i =0; i <Nx; i++)
	{
		for(int j=0; j<Ny; j++)
		{
			for(int k=0; k<Nz; k++)
			{
				int index = i*Ny*Nz+j*Nz+k;
				//cout << index << endl;
				//pX[index] = minx + i*sx*(maxx-minx);
				pX[index] = 2.25;
				pY[index] = miny + j*sy*(maxy-miny);
				pZ[index] = minz + k*sz*(maxz-minz);
				coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = pZ[index];
				//int elID = HNM3D->m_expansions[0]->GetExpIndex(coord);
				int elID = HNM3D->GetExpansionIndexUsingRTree(coord);
//				cout << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;	
//				cout << "eid: \t"<<elID<< "\t index \t"<< index << endl;
				pE[index] = elID;
				if (elID !=-1)
				{
					//sm->EvaluateAt(pX[index],pY[index],pZ[index],pU_s[index],valY,valZ, directionX, scaling ,0);
					//Sms[0]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pU_ss[index],valY,valZ,Sms, directions, scalings ,varsU,1);
					switch( atoi(argv[4]))
					{
						case 1:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pUx_ss[index],valY,valZ,SmD2s, directionsYXZ, scalings ,varsU,1);
							break;
						case 2:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pUy_ss[index],valY,valZ,SmD2s, directionsXYZ, scalings ,varsU,1);
							break;
						case 3:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pUz_ss[index],valY,valZ,SmD2s, directionsXZY, scalings ,varsU,1);
							break;
						case 4:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pVx_ss[index],valY,valZ,SmD2s, directionsYXZ, scalings ,varsV,1);
							break;
						case 5:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pVy_ss[index],valY,valZ,SmD2s, directionsXYZ, scalings ,varsV,1);
							break;
						case 6:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pVz_ss[index],valY,valZ,SmD2s, directionsXZY, scalings ,varsV,1);
							break;
						case 7:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pWx_ss[index],valY,valZ,SmD2s, directionsYXZ, scalings ,varsW,1);
							break;
						case 8:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pWy_ss[index],valY,valZ,SmD2s, directionsXYZ, scalings ,varsW,1);
							break;
						case 9:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pWz_ss[index],valY,valZ,SmD2s, directionsXZY, scalings ,varsW,1);
							break;
					}
				}else
				{	
					pU_p[index] = 0.0;
					pV_p[index] = 0.0;
					pW_p[index] = 0.0;
					pP_p[index] = 0.0;
					pP_s[index] = 0.0;
					cout << "out" << endl;
				}
	cout << "quering into R-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds for Nz = " << Nz<< endl;
	startTime = clock();
			}
			cout << j<< endl;
		}
		cout << i << endl;
	}
	
// Calculate acceleration,  b, torsion, Dbv for this data.


	switch ( atoi(argv[4]) )
	{
		case 1:
			k.writeNekArray(pX,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pX.txt");
			k.writeNekArray(pY,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pY.txt");
			k.writeNekArray(pZ,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pZ.txt");
			k.writeNekArray(pE,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pE.txt");
			k.writeNekArray(pUx_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pUx_ss.txt");
			break;
		case 2:
			k.writeNekArray(pUy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pUy_ss.txt");
			break;
		case 3:
			k.writeNekArray(pUz_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pUz_ss.txt");
			break;
		case 4:
			k.writeNekArray(pVx_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pVx_ss.txt");
			break;
		case 5:
			k.writeNekArray(pVy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pVy_ss.txt");
			break;
		case 6:
			k.writeNekArray(pVz_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pVz_ss.txt");
			break;
		case 7:
			k.writeNekArray(pWx_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pWx_ss.txt");
			break;
		case 8:
			k.writeNekArray(pWy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pWy_ss.txt");
			break;
		case 9:
			k.writeNekArray(pWz_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pWz_ss.txt");
			break;
	}
	
	//k.writeNekArray(pU_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pU_s.txt");
	//k.writeNekArray(pU_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pU_ss.txt");
	//k.writeNekArray(pUy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pUy_ss.txt");
	//k.writeNekArray(pUy_ssx,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pUy_ssx.txt");
	//k.writeNekArray(pUy_ssy,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pUy_ssy.txt");
	//k.writeNekArray(pUy_ssz,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pUy_ssz.txt");
	*/

	return 0;
}


