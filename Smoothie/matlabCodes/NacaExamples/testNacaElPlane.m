% testNaca ELPlane text file.

clear;
close all;
va = -51.5;
vb = 18;

%alpha =1;
%SIACFilterPoly = 2;
%scaling = '0.05'
ply = 100;

valueIso = 20;
UsebkupData = 'bkup_Submitted/'

filename = strcat('P0000009_SO_', int2str(ply),'_elPlane' );
filepath = strcat('data/',UsebkupData,filename);

xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );

vxfname = strcat(filepath,'_pVx_p.txt' );
wxfname = strcat(filepath,'_pWx_p.txt' );
uyfname = strcat(filepath,'_pUy_p.txt' );
wyfname = strcat(filepath,'_pWy_p.txt' );
uzfname = strcat(filepath,'_pUz_p.txt' );
vzfname = strcat(filepath,'_pVz_p.txt' );

xpos = textread(xfname,'%f\t');
ypos = textread(yfname,'%f\t');
zpos = textread(zfname,'%f\t');
epos = textread(efname, '%f\t');

vxpval = textread(vxfname,'%f\t');
wxpval = textread(wxfname,'%f\t');
uypval = textread(uyfname,'%f\t');
wypval = textread(wyfname,'%f\t');
uzpval = textread(uzfname,'%f\t');
vzpval = textread(vzfname,'%f\t');


Vort = ((uypval- vxpval).^(2) + (uzpval- wxpval).^(2) + (wypval- vzpval).^(2) ).^(1/2);

%%

% delyl = ypos;
% delzl = zpos;
% delvpl = Vort;
% delel = epos;
% 
% 
% [evals, elids,relids] = unique(delel);
% addpath('..');
% num_unique_ids = (max(size(evals)));
% for etag = evals'
%     disp(etag);
%     %disp('next');
%     el_indices = find(delel ==etag);
%     if (max(size(el_indices)) < 7)
%         continue;
%     end
%     %disp(delel(el_indices));
%     
%     ytripos = delyl(el_indices);
%     ztripos = delzl(el_indices);
%     vtripos = delvpl(el_indices);
%     tri = delaunay(ytripos,ztripos);
%     
%     
%     %figure(9), hold on;    
%     %trisurf(tri,ytripos,ztripos,vtripos); hold on;
%     
%     figure(10), hold on;
%     tricontour( [ytripos,ztripos], tri, vtripos,linspace(0,100,40)); hold on;
%     
%     [nouse hcs] = tricontour([ytripos,ztripos], tri, vtripos,[valueIso valueIso]); hold on;
%     %hcs = contourslice(xVpos, yVpos, zVpos, Vorticity_p, 2.25,[], [], ) ;
%      set(hcs,'EdgeColor',[1 0 0]);
%      set(hcs,'linewidth',2);
%      set(hcs,'linestyle','--');
%     
% %    tricontour(tri,ytripos,ztripos,vtripos,linspace(0,100,40)); hold on;
%     
% end

%%
% 
% for etag = evals'
%     disp(etag);
%     %disp('next');
%     el_indices = find(delel ==etag);
%     if (max(size(el_indices)) < 7)
%         continue;
%     end
%     %disp(delel(el_indices));
%     
%     ytripos = delyl(el_indices);
%     ztripos = delzl(el_indices);
%     vtripos = delvpl(el_indices);
%     tri = delaunay(ytripos,ztripos);
%     
%     
%     %figure(9), hold on;    
%     %trisurf(tri,ytripos,ztripos,vtripos); hold on;
%     
%     figure(11), hold on;
%     %tricontour( [ytripos,ztripos], tri, vtripos,linspace(0,100,40)); hold on;
%     
%     [nouse hcs] = tricontour([ytripos,ztripos], tri, vtripos,[valueIso valueIso]); hold on;
%     %hcs = contourslice(xVpos, yVpos, zVpos, Vorticity_p, 2.25,[], [], ) ;
%      set(hcs,'EdgeColor',[1 0 0]);
%      set(hcs,'linewidth',2);
%      set(hcs,'linestyle','--');
%     
% %    tricontour(tri,ytripos,ztripos,vtripos,linspace(0,100,40)); hold on;
%     
% end

%%
%enpos = RemoveBorders(epos);

delyl = ypos;
delzl = zpos;
delvpl = Vort;
delel = epos;


[evals, elids,relids] = unique(delel);
addpath('../LSIAC_tests');
num_unique_ids = (max(size(evals)));
for etag = evals'
    
    tri = ReturnTriangles(delel,etag);
    disp(etag);
    %figure(11), hold on;
    %trisurf(tri,delyl,delzl,delvpl);
	%shading interp, hold on;
    if (max(size(tri)) <7)
        continue;
    end
    figure(12), hold on;
    tricontour([delyl,delzl],tri,delvpl,linspace(0,100,40)); hold on;
    
    if (etag == 12343)
        continue;
    end
    [nouse hcs] = tricontour([delyl,delzl],tri,delvpl,[valueIso valueIso+800]); hold on;
    %[nouse hcs] = tricontour([delyl,delzl],tri,delvpl,linspace(0,20,1)); hold on;
     set(hcs,'EdgeColor',[1 0 0]);
     set(hcs,'linewidth',2);
     set(hcs,'linestyle','--');
    
%     disp(etag);
%     if (etag < 1)
%         continue;
%     end
%     %disp('next');
%     el_indices = find(delel ==etag);
%     if (max(size(el_indices)) < 7)
%         continue;
%     end
%     %disp(delel(el_indices));
%     
%     ytripos = delyl(el_indices);
%     ztripos = delzl(el_indices);
%     vtripos = delvpl(el_indices);
% %    tri = delaunay(ytripos,ztripos);
%     tri = ReturnTriangles(dele,etag)
%     
%     %figure(11), hold on;    
%     %trisurf(tri,ytripos,ztripos,vtripos);
%     %shading interp, hold on;
%     
%     figure(12), hold on;
%     tricontour( [ytripos,ztripos], tri, vtripos,linspace(0,100,40)); hold on;
%     
%     [nouse hcs] = tricontour([ytripos,ztripos], tri, vtripos,[valueIso valueIso]); hold on;
%     %hcs = contourslice(xVpos, yVpos, zVpos, Vorticity_p, 2.25,[], [], ) ;
%      set(hcs,'EdgeColor',[1 0 0]);
%      set(hcs,'linewidth',2);
%      set(hcs,'linestyle','--');
%     
% %    tricontour(tri,ytripos,ztripos,vtripos,linspace(0,100,40)); hold on;
%     
end
%%
figure(12), hold on;
axis off;
grid off;

%print('images/Vorticity_Contours_DG_P_2_FS','-dpng', '-r500')
