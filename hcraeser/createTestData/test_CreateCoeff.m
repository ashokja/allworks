
n = 7;
[xy,distance,t_a,mapxy,mapxyV,curveCoeff] = test_runNodes(r,n+1,1);

[coeff4] = buildCoeff(n,n,n,mapxy,mapxyV);
dataV4 = buildData(n,n,n,coeff4,mapxy);

[uRedCoeff,uRotCoeff,dataEig] = divideVelCreate(n,n,n,coeff4,mapxy,dataV4,distance);
%dRedCoeff = buildData(n,n,n,uRedCoeff,mapxy);
dRotCoeff = buildData(n,n,n,uRotCoeff,mapxy);
%get Rotational Velocity out of it.

%[xy,distance,t_a,mapxy,mapxyV,curveCoeff] = test_runNodes(r,n+1,2);

%[coeff4] = buildCoeff(n,n,n,mapxy,mapxyV);
%dataV4 = buildData(n,n,n,coeff4,mapxy);

%[uRedCoeff,~] = divideVel(n,n,n,coeff4,mapxy,dataV4);
dRedCoeff = buildData(n,n,n,uRedCoeff,mapxy);
%dRotCoeff = buildData(n,n,n,uRotCoeff,mapxy);

%drawing actual rotational Velocity added into field.
figure(12)
    drawStreamLine(mapxy,dRotCoeff,curveCoeff);
    hold off
    title(strcat('Rotational Velocity',int2str(n)));
    xlabel('x'); ylabel('y'); zlabel('z'); hold off;
    view(-110.5,16);
% drawing actual reduced velocity added into field.

figure(13)
    drawStreamLine(mapxy,dRedCoeff,curveCoeff);
%    drawStreamLine(mapxy,mapxyV,curveCoeff);
    hold off
    title(strcat('Reduced Velocity',int2str(n)));
    xlabel('x'); ylabel('y'); zlabel('z'); hold off;
    view(-110.5,16);
    %%
figure(14)
    plotBoundingBox();
    quiver3(mapxy(:,1),mapxy(:,2),mapxy(:,3),1-dataEig,1-dataEig,1-dataEig);
    hold off
        title(strcat('DataEig',int2str(n)));
    xlabel('x'); ylabel('y'); zlabel('z'); hold off;
    view(-110.5,16);
%%
uCoeff = uRedCoeff+uRotCoeff;
dData = dRedCoeff+dRotCoeff;
[uuRedCoeff,uuRotCoeff] = divideVel(n,n,n,uCoeff,mapxy,dData);
ddRedCoeff = buildData(n,n,n,uuRedCoeff,mapxy);
ddRotCoeff = buildData(n,n,n,uuRotCoeff,mapxy);

figure(15)
    drawStreamLine(mapxy,ddRotCoeff,curveCoeff);
    hold off
    title(strcat('Rotational Velocity',int2str(n)));
    xlabel('x'); ylabel('y'); zlabel('z'); hold off;
    view(-110.5,16);
% drawing actual reduced velocity added into field.

figure(16)
    drawStreamLine(mapxy,ddRedCoeff,curveCoeff);
%    drawStreamLine(mapxy,mapxyV,curveCoeff);
    hold off
    title(strcat('Reduced Velocity',int2str(n)));
    xlabel('x'); ylabel('y'); zlabel('z'); hold off;
    view(-110.5,16);

%%
figure(112);
[sol_seeds,goodSeeds,solAtSeeds] = zerosAtProjected(n,n,n,uuRotCoeff,7);
%%
t(:,1) = distance; t(:,3) = distance; t(:,3) = distance;

figure(1112);
    drawStreamLine(mapxy,t,curveCoeff);
    hold off
    title(strcat('Rotational Velocity',int2str(n)));
    xlabel('x'); ylabel('y'); zlabel('z'); hold off;
    view(-110.5,16);

