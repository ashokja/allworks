%test Vortex Projection
clear;
close all;
addpath ../VortexOnBasisEl;
addpath ../Bfuncs;
N = [6,8,10,11,14];
%N = [4,5];
coeff_poly = {}; dataV_poly = {};

count = 1;
%%
for n = N
    %n =5;
    [xy,distance,t_a,mapxy,mapxyV,curveCoeff] = test_runNodes(1,n+3,3,1);

    [coeff] = buildCoeff(n,n,n,mapxy,mapxyV);
    dataV = buildData(n,n,n,coeff,mapxy);
    % normal parallezation starts.
    %%
    x = (-1:0.1:1);
    y = (-1:0.1:1);
    z = (-1:0.1:1);
    [X,Y,Z] = meshgrid(x,y,z);
    dataIn(:,1) = reshape(X,[],1);
    dataIn(:,2) = reshape(Y,[],1);
    dataIn(:,3) = reshape(Z,[],1);
    %%
    dataOut = buildData(n,n,n,coeff,dataIn);
    drawStreamLine(dataIn,dataOut,curveCoeff);
    U = reshape(dataOut(:,1),size(X));
    V = reshape(dataOut(:,2),size(Y));
    W = reshape(dataOut(:,3),size(Z));
    %%
    Vbar = B3_der_eval(n,n,n,coeff,dataIn);
    V_bar = reshape(Vbar,[],3,3);
    [s_dIn,~] = size(dataIn);
    dataEigComp = zeros(s_dIn,1); 
    dataOutRv = zeros(s_dIn,3); dataOutSv = zeros(s_dIn,3);
    %%
    for i =1:s_dIn

        jj(:,:) = V_bar(i,:,:);
        jj = jj';
        lambda = eig(jj);
        tet_complex =false;
        if ~isreal(lambda)
            'tet contains complex number';
            tet_complex = true;
        end

        dataEigComp(i) = tet_complex;
        if (tet_complex)
            % find the real rool index. 
            real_index  = find(imag(lambda)==0);
        end

        if (tet_complex)
            [A,~] = eig(jj);

            nor = A(:,real_index)'/norm(A(:,real_index));
            nor_r = dot(dataOut(i,:),nor)*nor;
            nor_r = nor_r/norm(nor_r);
            dataOutRv(i,:) = nor_r;
            %dataOutRv(i,:) = dot(dataOut(i,:),nor)*nor;
            %dataOutRv(i,:) = lambda(real_index)*A(:,real_index)';
            dataOutSv(i,:) = dataOut(i,:) - dataOutRv(i,:);
        else
            dataOutRv(i,:) = dataOut(i,:);
            dataOutSv(i,:) = zeros(1,3);
        end

    end
    %%
    
    % smooth couple of times.
    RU = reshape(dataOutRv(:,1),size(X));
    RV = reshape(dataOutRv(:,2),size(Y));
    RW = reshape(dataOutRv(:,3),size(Z));
    
    %%
%     for smt = 1:1
%         RU = smooth3(RU,'box',9);
%         RV = smooth3(RV,'box',9);
%         RW = smooth3(RW,'box',9);
%     end
   [RUO,RVO,RWO]  = BuildLowPassApprox(RU,RV,RW,3,3);
    %%
    dataOutRv_s = [RUO(:),RVO(:),RWO(:)];
    %%
    dcomp = dot(dataOutRv_s,dataOut,2);
    dataOutRvF = [dcomp,dcomp,dcomp].*dataOutRv_s;
    dataOutSvF = dataOut - dataOutRvF;

%        coeff_poly(count) = {coeff};
%        dataV_poly(count) = {dataV};
%     %%
    figure (count*10+2);
    drawStreamLine(dataIn,dataOutSvF,curveCoeff);
    title(strcat('rotation Vel ',int2str(n)));
    view([-73.5,26]);
    figure (count*10+3);
    title(strcat('reduced Vel ',int2str(n)));
    drawStreamLine(dataIn,dataOutRvF,curveCoeff);
    view([-73.5,26]);
%     sum_eig = sum(dataEigComp(:));
%     [size_eig,~] = size(dataEigComp(:));
%     if (sum_eig < size_eig)
%         figure (count*10+4);
%         drawStreamLine(dataIn,[dataEigComp,dataEigComp,dataEigComp],curveCoeff);
%         title(strcat('eigComplexMap ',int2str(n)));
%         'fewer eig decomposition then expected'
%     else
%         'Right eig decompositions happened'
%     end
     dataOutSv_poly(count,:,:) = dataOutSvF;
     dataOutRv_poly(count,:,:) = dataOutRvF;
    count = count+1;
end


%%
    save('testdata/data_vary_poly_coeff_normal_21.m');
%%
  %  u_red = buildCoeff(n1,n2,n3,dataIn,dataOutRv);
  figure(54)
    u_rot = buildCoeff(n,n,n,dataIn,dataOutSv);
    %%
      [sol_seeds,goodSeeds,solAtSeeds] = zerosAtProjected(n,n,n,u_rot);