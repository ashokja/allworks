#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"
//#include <ctime>


using namespace SIACUtilities;
using namespace std;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>

int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		return 0;
	}
	argc = 2;
	clock_t startTime = clock();
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);

	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	//HNM3D->LoadMesh(var[0]);
	//HNM3D->LoadMesh(var[1]);
	//HNM3D->LoadMesh(var[2]);
	
	cout << "loading xml file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	string fname = vSession->GetSessionName();

	//string fldname = fname + ".fld/Info.xml";	
	//string fldname = "naca.fld";	
	string fldname = fname + ".fld";	
	cout << fname << endl;
	cout << fldname << endl;

	HNM3D->LoadData( fldname ,var);
	cout << "loading fld file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	cout << "expsize:\t"<< HNM3D->m_expansions[0]->GetExpSize() << endl;
	
	HNM3D->LoadExpListIntoRTree();
	cout << "loading into R-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	/*
	for (int i =0; i< HNM3D->m_expansions[0]->GetExpSize();i++)
	{
		int gid = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetGlobalID();
		cout << "i\t" << i << "\tg\t" << gid << endl;
		cout << HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetShapeType()<< endl;
	}
	*/
	// Find minx,miny,minz	
	// Find maxx,maxy,maxz	
	cout << HNM3D->m_expansions[0]->GetTotPoints() << endl;
	cout << HNM3D->m_expansions[0]->GetNcoeffs() << endl;
	
	int totPhys = HNM3D->m_expansions[0]->GetTotPoints();
	Array<OneD,NekDouble> xc0(totPhys),xc1(totPhys), xc2(totPhys);
	
	HNM3D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
	NekDouble minx = Vmath::Vmin(totPhys,xc0,1);	
	NekDouble miny = Vmath::Vmin(totPhys,xc1,1);	
	NekDouble minz = Vmath::Vmin(totPhys,xc2,1);	
	NekDouble maxx = Vmath::Vmax(totPhys,xc0,1);	
	NekDouble maxy = Vmath::Vmax(totPhys,xc1,1);	
	NekDouble maxz = Vmath::Vmax(totPhys,xc2,1);	

	cout << "min\t " << minx << "\t" << miny<<"\t"<< minz<< endl; 
	cout << "max\t " << maxx << "\t" << maxy<<"\t"<< maxz<< endl; 
	
	const Array<OneD,NekDouble> u_phys = HNM3D->m_expansions[0]->GetPhys();
	const Array<OneD,NekDouble> v_phys = HNM3D->m_expansions[1]->GetPhys();
	const Array<OneD,NekDouble> w_phys = HNM3D->m_expansions[2]->GetPhys();
	const Array<OneD,NekDouble> p_phys = HNM3D->m_expansions[3]->GetPhys();


	//Setting values to make sure inside element equalspaced.
//	minx = 2.5; miny = 0.0; minz = 0.6;
//	maxx = 3.0; maxy = 0.5; maxz= 1.1;

	minx = 4.2; miny = 0.0; minz = 0.6;
	maxx = 4.6; maxy = 0.5; maxz= 1.0;
  
	int gPtsX =  90;   //atoi(argv[2]);
	int gPtsY =  90;   //atoi(argv[2]);
	int gPtsZ =  90;   //atoi(argv[2]);
	int Nx = gPtsX, Ny = gPtsY, Nz = gPtsZ;
	int totPts = Nx*Ny*Nz;
	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0), sz=1.0/(Nz-1.0);
	Array<OneD,NekDouble> pX(totPts), pY(totPts), pZ(totPts), pE(totPts);	
	Array<OneD,NekDouble> pU_p(totPts), pV_p(totPts), pW_p(totPts), pP_p(totPts);
	Array<OneD,NekDouble> pU_s(totPts), pV_s(totPts), pW_s(totPts), pP_s(totPts);
	Array<OneD,NekDouble> pUx_s(totPts), pUy_s(totPts), pUz_s(totPts);
	Array<OneD,NekDouble> pVx_s(totPts), pVy_s(totPts), pVz_s(totPts);
	Array<OneD,NekDouble> pWx_s(totPts), pWy_s(totPts), pWz_s(totPts);

	Array<OneD,NekDouble> coord(3);
	SmoothieSIAC3D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]),atof(argv[3]) ); 
	SmoothieSIAC3D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]),atof(argv[3]),1); 
	
	NekDouble valY,valZ, scaling;
	Array<OneD,NekDouble> directionX(3,0.0),directionY(3,0.0),directionZ(3,0.0);
	
	scaling = atof(argv[3]);
	directionX[0] = 1.0;	directionY[1] = 1.0;	directionZ[2] = 1.0;

	for (int i =0; i <Nx; i++)
	{
		for(int j=0; j<Ny; j++)
		{
			for(int k=0; k<Nz; k++)
			{
				int index = i*Ny*Nz+j*Nz+k;
				//cout << index << endl;
				//pX[index] = minx + i*sx*(maxx-minx);
				pX[index] = minx + i*sx*(maxx-minx);
				pY[index] = miny + j*sy*(maxy-miny);
				pZ[index] = minz + k*sz*(maxz-minz);
				coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = pZ[index];
				//int elID = HNM3D->m_expansions[0]->GetExpIndex(coord);
				int elID = HNM3D->GetExpansionIndexUsingRTree(coord);
//				cout << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;	
//				cout << "eid: \t"<<elID<< "\t index \t"<< index << endl;
				pE[index] = elID;
				if (elID !=-1)
				{
/*
					LocalRegions::ExpansionSharedPtr lexp = HNM3D->m_expansions[0]->GetExp(elID);
		
					int physOffset = HNM3D->m_expansions[0]->GetPhys_Offset(elID);	
				
					const Array<OneD,NekDouble> el_u_phys = u_phys.CreateWithOffset(u_phys,physOffset);
					const Array<OneD,NekDouble> el_v_phys = v_phys.CreateWithOffset(v_phys,physOffset);
					const Array<OneD,NekDouble> el_w_phys = w_phys.CreateWithOffset(w_phys,physOffset);
					const Array<OneD,NekDouble> el_p_phys = p_phys.CreateWithOffset(p_phys,physOffset);
*/					
				//	sm.EvaluateAt(pX[index],pY[index],pZ[index],pU_s[index],valY,valZ, directionX, scaling ,0);
				//	sm.EvaluateAt(pX[index],pY[index],pZ[index],pV_s[index],valY,valZ, directionY, scaling ,1);
				//	sm.EvaluateAt(pX[index],pY[index],pZ[index],pW_s[index],valY,valZ, directionZ, scaling ,2);
				//	sm.EvaluateAt(pX[index],pY[index],pZ[index],pP_s[index],valY,valZ, directionY, scaling ,3);
//					cout << pU_s[index] << "\t" << pV_s[index] << "\t" << pW_s[index]<<"\t" << pP_s[index] << endl;
				//	smD.EvaluateAt(pX[index],pY[index],pZ[index],pUx_s[index],valY,valZ, directionX, scaling ,0);
					smD.EvaluateAt(pX[index],pY[index],pZ[index],pUy_s[index],valY,valZ, directionY, scaling ,0);
					smD.EvaluateAt(pX[index],pY[index],pZ[index],pUz_s[index],valY,valZ, directionZ, scaling ,0);
					
					smD.EvaluateAt(pX[index],pY[index],pZ[index],pVx_s[index],valY,valZ, directionX, scaling ,1);
				//	smD.EvaluateAt(pX[index],pY[index],pZ[index],pVy_s[index],valY,valZ, directionY, scaling ,1);
					smD.EvaluateAt(pX[index],pY[index],pZ[index],pVz_s[index],valY,valZ, directionZ, scaling ,1);
					
					smD.EvaluateAt(pX[index],pY[index],pZ[index],pWx_s[index],valY,valZ, directionX, scaling ,2);
					smD.EvaluateAt(pX[index],pY[index],pZ[index],pWy_s[index],valY,valZ, directionY, scaling ,2);
				//	smD.EvaluateAt(pX[index],pY[index],pZ[index],pWz_s[index],valY,valZ, directionZ, scaling ,2);

/*
					pU_p[index] = lexp->PhysEvaluate(coord,el_u_phys);
					pV_p[index] = lexp->PhysEvaluate(coord,el_v_phys);
					pW_p[index] = lexp->PhysEvaluate(coord,el_w_phys);
					pP_p[index] = lexp->PhysEvaluate(coord,el_p_phys);
//					cout << pU_p[index] << "\t" << pV_p[index] << "\t" << pW_p[index]<<"\t" << pP_p[index] << endl;
*/
				}else
				{	
					pU_p[index] = 0.0;
					pV_p[index] = 0.0;
					pW_p[index] = 0.0;
					pP_p[index] = 0.0;
					pP_s[index] = 0.0;
					cout << "out" << endl;
				}
			}
			cout << "\ti"<< i << "\tj "<< j<< endl;
	cout << "quering into R-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds for Nz = " << Nz<< endl;
	startTime = clock();
		}
		cout << i << endl;
	}
	

	NektarBaseClass k;
	k.writeNekArray(pX,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pX.txt");
	k.writeNekArray(pY,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pY.txt");
	k.writeNekArray(pZ,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pZ.txt");
	k.writeNekArray(pE,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pE.txt");

/*
	k.writeNekArray(pU_p,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pU_p.txt");
	k.writeNekArray(pV_p,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pV_p.txt");
	k.writeNekArray(pW_p,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pW_p.txt");
	k.writeNekArray(pP_p,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pP_p.txt");
	
	k.writeNekArray(pU_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pU_s.txt");
	k.writeNekArray(pV_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pV_s.txt");
	k.writeNekArray(pW_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pW_s.txt");
	k.writeNekArray(pP_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pP_s.txt");
*/	
//	k.writeNekArray(pUx_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pUx_s.txt");
	k.writeNekArray(pUy_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pUy_s.txt");
	k.writeNekArray(pUz_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pUz_s.txt");
	
	k.writeNekArray(pVx_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pVx_s.txt");
//	k.writeNekArray(pVy_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pVy_s.txt");
	k.writeNekArray(pVz_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pVz_s.txt");
	
	k.writeNekArray(pWx_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pWx_s.txt");
	k.writeNekArray(pWy_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pWy_s.txt");
//	k.writeNekArray(pWz_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_All3Dvar6_p4_pWz_s.txt");
	
	cout << "writing data out "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	return 0;
}


