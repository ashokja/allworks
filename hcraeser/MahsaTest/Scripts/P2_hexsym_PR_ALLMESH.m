% create file names.
clear;

MR = (2:2:16)*7+1;

addpath('..');

Deg=2;
ord = zeros(1,8);

ord(1) = 336;    ord(2) = 1344;    ord(3) = 3024;   ord(4) = 5376;
ord(5) = 8400;   ord(6) = 12096;   ord(7) = 16464;   ord(8) = 21504;

%use HL and VL
KF ='0';
P = '2';
KF ='0';
UF = '0';
pfilename = '../PostP_hexSym_debug/log_hex_sym_PR_MAXSIZE_MR';
count =1;
for mr = MR
    filename = strcat(pfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C(count,:) = ReadFile(filename);
    count= count+1;
end

L2B_ord = C(:,1);
L2_ord = C(:,2);
LinfB_ord = C(:,3);
Linf_ord = C(:,4);



%use MaxSize
KF ='0';
P = '2';
KF ='0';
UF = '0';
pfilename = '../PostP_hexSym_debug/log_hex_sym_PR_MAXSIZE_MR';
count =1;
for mr = MR
    filename = strcat(pfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C(count,:) = ReadFile(filename);
    count= count+1;
end
L2BH1_ord = C(:,1);
L2H1_ord = C(:,2);
LinfBH1_ord = C(:,3);
LinfH1_ord = C(:,4);
% 
%use Hex
KF ='0';
P = '2';
KF ='1';
UF = '0';
pfilename = '../PostP_hexSym_debug/log_hex_sym_PR_MAXSIZE_MR';
count =1;
for mr = MR
    filename = strcat(pfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C(count,:) = ReadFile(filename);
    count= count+1;
end
L2BH2_ord = C(:,1);
L2H2_ord = C(:,2);
LinfBH2_ord = C(:,3);
LinfH2_ord = C(:,4);


fileID = fopen('latex/P2_hexsym_PR_AllMESH_ALLERR.txt','w');

line0= '\caption{PR solution , Hex Sym, All SIAC filter, L2 Error.}';
fwrite(fileID,line0);

% write code to print L2
 line1= strcat('\label{table:4}',...
  '\begin{tabular}{||c| c| c| c| c ||}',...
 ' \hline ');
fwrite(fileID,line1);
line2 = ' Mesh Ele & Deg &L2(Before) & L2(Cart(M)) & L2(Hex) \\ [0.5ex] ';
fwrite(fileID,line2);
line3 = ' \hline\hline ';
fwrite(fileID,line3);
for i=1:8
    line4 = strcat('$',num2str(ord(i)),'$ & $',num2str(Deg),'$ & $',...
         num2str(L2B_ord(i),'%1.2e'), '$ & $',...
        num2str(L2H1_ord(i),'%1.2e'), '$ & $',...
        num2str(L2H2_ord(i),'%1.2e'), '$ ',...
        '\\');
    line5 = ' \hline ';
    fwrite(fileID,line4);
	fwrite(fileID,line5);
end
line11= ' \hline \end{tabular} ';
fwrite(fileID,line11);
fclose(fileID);

 

fileID = fopen('latex/P2_hexsym_PR_AllMESH_ALLERR_Linf.txt','w');

line0= '\caption{PR solution , Hex Sym, All SIAC filter, Linf Error.}';
fwrite(fileID,line0);

% write code to print L2
 line1= strcat('\label{table:4}',...
  '\begin{tabular}{||c| c| c| c| c ||}',...
 ' \hline ');
fwrite(fileID,line1);
line2 = ' Mesh Ele & Deg &Linf(Before) & Linf(Cart(M)) & Linf(Hex) \\ [0.5ex] ';
fwrite(fileID,line2);
line3 = ' \hline\hline ';
fwrite(fileID,line3);
for i=1:8
    line4 = strcat('$',num2str(ord(i)),'$ & $',num2str(Deg),'$ & $',...
         num2str(LinfB_ord(i),'%1.2e'), '$ & $',...
        num2str(LinfH1_ord(i),'%1.2e'), '$ & $',...
        num2str(LinfH2_ord(i),'%1.2e'), '$ ',...
        '\\');
    line5 = ' \hline ';
    fwrite(fileID,line4);
	fwrite(fileID,line5);
end
line11= ' \hline \end{tabular} ';
fwrite(fileID,line11);
fclose(fileID);