clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];

filePoly = 3;
poly = 2;

addpath('../')

%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize
Cyl_Max = 6.3;
Cyl_Min = -5.2571;

r = filePoly
filename = strcat( '../datatxt/Cy',int2str(r),'l_Re500_');
filenameFrPrint = strcat( 'Cy',int2str(r),'l_Re500_');
%    filenameFrPrint = strcat( 'Cy',int2str(r),'l_Re500_','Vor2ndKind_');
    
offsets = strcat( filename,'Vor2ndKind_Offset.txt');
elemts = strcat( filename,'Vor2ndKind_ElmT.txt');

xc0fname = strcat( filename,'Vor2ndKind_xc0.txt');
xc1fname = strcat( filename,'Vor2ndKind_xc1.txt');
dgVorfname = strcat( filename,'Vor2ndKind_DGVor.txt');
Vor2ndKindfname = strcat( filename,'Vor2ndKind_Vor2.txt');

Elxfname = strcat( filename,'Vor2ndKind_Elx.txt');
Elyfname = strcat( filename,'Vor2ndKind_Ely.txt');
Elvfname = strcat( filename,'Vor2ndKind_Elv.txt');
Elidfname = strcat( filename,'Vor2ndKind_Elid.txt');

%xc1fname = strcat( filename,'Vor2ndKind_xc0.txt');

 elOffsets = floor(textread(offsets,'%f\t') +1.0001);
 elType = textread(elemts,'%f\t');
 xval = textread(xc0fname, '%f\t');
 yval = textread(xc1fname, '%f\t');
 dgval = textread( dgVorfname, '%f\t');
 Vor2ndKindval = -1.0* textread(Vor2ndKindfname,'%f\t');
 
  Elx = textread(Elxfname, '%f\t');
  Ely = textread(Elyfname, '%f\t');
  Elv = textread(Elvfname, '%f\t');
  Elid = textread(Elidfname, '%f\t');
 
  tri = [];
 tricount = 0; quadcount=0; 
 ptCloud = [xval,yval,zeros(size(xval))];
 for el = 1:max(size(elOffsets))
     if ( 3 == floor( elType(el) ) )
         tricount = tricount+1;
         startIndex = elOffsets(el);
         point = [Elx(tricount),Ely(tricount),0.0];
        [a,Special] = fNN(ptCloud,point);
         if ( abs(a) > 1e-6)
             disp('Something is wrong'); 
         end
         Atri = GenerateP3Triangles(elType(el), startIndex,Special);
         tri = [tri;Atri];
     else
         quadcount= quadcount+1;
         startIndex = elOffsets(el);
         Atri = GenerateP3Triangles(elType(el), startIndex,-1);
         tri = [tri;Atri];
     end      
 end

% % % % % % % % % % % %  %%
% % % % % % % % % % % %  elOffsets(2)
% % % % % % % % % % % %  % indices for each elment are .
% % % % % % % % % % % %  tricount = 0; quadcount=0;
% % % % % % % % % % % %  for el = 1:max(size(elOffsets))-1
% % % % % % % % % % % %      if ( 3 == floor(elType(el)) )
% % % % % % % % % % % %          tricount = tricount+1;
% % % % % % % % % % % %         elIndices_Tri(tricount,:) = (elOffsets(el)+1:1:elOffsets(el+1));
% % % % % % % % % % % %      else
% % % % % % % % % % % %          quadcount= quadcount+1;
% % % % % % % % % % % %         elIndices_Quad(quadcount,:) = (elOffsets(el)+1:1:elOffsets(el+1));
% % % % % % % % % % % %         
% % % % % % % % % % % %      end
% % % % % % % % % % % %  end
% % % % % % % % % % % %      if ( 3 == floor(elType(end)) )
% % % % % % % % % % % %          tricount = tricount+1;
% % % % % % % % % % % %         elIndices_Tri(tricount,:) = (max(elOffsets)+1:1:max(size(xval)));
% % % % % % % % % % % %         
% % % % % % % % % % % %      else
% % % % % % % % % % % %          quadcount= quadcount+1;
% % % % % % % % % % % %         elIndices_Quad(quadcount,:) = (max(elOffsets)+1:1:max(size(xval)));
% % % % % % % % % % % %         
% % % % % % % % % % % %      end
% % % % % % % % % % % % %%
% % % % % % % % % % % %  % plot first triangle
% % % % % % % % % % % %  figure(1)
% % % % % % % % % % % %  plot3(xval(elIndices_Tri(1,:)),yval(elIndices_Tri(1,:)), ones(size(elIndices_Tri(1,:))),'-*');hold on;
% % % % % % % % % % % % %  plot3(xval(elIndices_Tri(2,:)),yval(elIndices_Tri(2,:)), ones(size(elIndices_Tri(2,:))),'g*')
% % % % % % % % % % % %  view(0,90);axis equal;
% % % % % % % % % % % % %%
% % % % % % % % % % % %  %plot first quad
% % % % % % % % % % % %  figure(2)
% % % % % % % % % % % %   plot3(xval(elIndices_Quad(1,:)),yval(elIndices_Quad(1,:)), elIndices_Quad(1,:),'*')
% % % % % % % % % % % %  view(0,90);
% % % % % % % % % % % %  
% % % % % % % % % % % %  % plot create triangles for each triangular element;
% % % % % % % % % % % %  figure(1), hold on;
% % % % % % % % % % % %  ptCloud = [xval,yval,zeros(size(xval))];
% % % % % % % % % % % %  for t = 1:tricount
% % % % % % % % % % % %     point = [Elx(t),Ely(t),0.0];
% % % % % % % % % % % %      [a,b] = fNN(ptCloud,point);
% % % % % % % % % % % %      disp(a);
% % % % % % % % % % % %      
% % % % % % % % % % % %  end
% % % % % % % % % % % %  
% % % % % % % % % % % %  tri[]
% % % % % % % % % % % %  =  GenerateP3Triangles(elType, startIndex,Special);
% % % % % % % % % % % %  
% % % % % % % % % % % %  
% % % % % % % % % % % %  
% % % % % % % % % % % %  
% % % % % % % % % % % %  
% % % % % % % % % % % %  
% % % % % % % % % % % %  
% % % % % % % % % % % %  %%
% % % % % % % % % % % %  
% % % % % % % % % % % %  
% % % % % % % % % % % %   tri = delaunay(xval,yval);
%plot(xval,yval,'.')
%  [X,Y] = meshgrid(linspace(2.5,10.5,801),linspace(-4,4,801));
%  dgVal = interp2(xval,yval,dgval,X,Y);
%LMM = Vor2ndKindfname(xval>=2.5&xval<=10.5);
el_indices = find(xval>=2.5&xval<=10.5);
dgVal = dgval(el_indices);

[r,c] = size(tri);
disp(r)

%%
figure(1)
h = trisurf(tri, xval, yval, dgval,'LineStyle','none','LineWidth',lw);
%axis vis3d
   xlim([2.5,10.5]);
    ylim([-4,4]);
%plotNekMesh('../../data/CylExamples/Cy2l.xml'); 
view(0,90); 
caxis( [Cyl_Min Cyl_Max] );



%%
figure(2)
h = trisurf(tri, xval, yval, Vor2ndKindval);
%axis vis3d
   xlim([2.5,10.5]);
    ylim([-4,4]);
%plotNekMesh('../../data/CylExamples/Cy2l.xml'); 
view(0,90); 
shading interp;
caxis( [Cyl_Min Cyl_Max] );
axis off;
%print(strcat('../paperUseImagesF/',filenameFrPrint,'_Vor2style_NCR_surf'), '-dpng','-r500');
%% 
hold on;
%trisurf(tri, xval, yval, Vor2ndKindval,'LineStyle','-','LineWidth',lw,'facealpha',0,'EdgeAlpha',0.3,'EdgeColor','c');
tricountN = 0; quadcountN=0;
for el = 1:max(size(elOffsets))
     if ( 3 == floor( elType(el) ) )
         tricountN = tricountN+1;
         startIndex = elOffsets(el);
         point = [Elx(tricountN),Ely(tricountN),0.0];
        [a,Special] = fNN(ptCloud,point);
         if ( abs(a) > 1e-6)
             disp('Something is wrong'); 
         end
         DrawP3Triangles(elType(el), startIndex,Special,ptCloud);
         %tri = [tri;Atri];
     else
         quadcountN= quadcountN+1;
         startIndex = elOffsets(el);
         DrawP3Triangles(elType(el), startIndex,-1,ptCloud);
     end      
 end


plotNekMesh('../../data/CylExamples/Cy2l.xml'); 
%print(strcat('../paperUseImagesF/',filenameFrPrint,'_Vor2style__NCR_surf_mesh'), '-dpng','-r500');

%%

multMatrix = (xval>2.5 & xval<10);
Vor2nd = Vor2ndKindval.*multMatrix;

%Vor2nd = Vor2ndKindval(xval>2.5 & xval<10);
%%
figure(3)
%tricontour( tri, xval,yval, Vor2nd,(Cyl_Min:.3:Cyl_Max));
   xlim([2.5,10.5]);
    ylim([-4,4]);
axis off;
%print(strcat('../paperUseImagesF/',filenameFrPrint,'_Vor2style_NCR_contours'), '-dpng','-r500');



tricountN = 0; quadcountN=0;
for el = 1:max(size(elOffsets))
     if ( 3 == floor( elType(el) ) )
         tricountN = tricountN+1;
         startIndex = elOffsets(el);
         point = [Elx(tricountN),Ely(tricountN),0.0];
        [a,Special] = fNN(ptCloud,point);
         if ( abs(a) > 1e-6)
             disp('Something is wrong'); 
         end
         DrawP3Triangles(elType(el), startIndex,Special,ptCloud);
         %tri = [tri;Atri];
     else
         quadcountN= quadcountN+1;
         startIndex = elOffsets(el);
         DrawP3Triangles(elType(el), startIndex,-1,ptCloud);
     end      
end
plotNekMesh('../../data/CylExamples/Cy2l.xml'); 
%print(strcat('../paperUseImagesF/',filenameFrPrint,'_Vor2style_NCR_contours_mesh'), '-dpng','-r500');
%print(strcat('../paperUseImagesF/',filenameFrPrint,'_Vor2style_NCR_only_mesh4'), '-dpng','-r500');
 disp('done rendering');
%%
figure(5)
[tcc tch] = tricontour( tri, xval, yval, Vor2nd, (Cyl_Min:.3:Cyl_Max));
hold on;
line([3.7,3.7],[-4,4],'lineStyle','--','Color',[0.5 0.5 0.5],'linewidth',lw);
set (tch, 'LineWidth', lw);
xlim([2.5,10.5]);
ylim([-4,4]);
%axis box;
axis off; 
print(strcat('../paperUseImagesF/',filenameFrPrint,'_Vor2style_NCR_contours_mesh'), '-dpng','-r500');

%  
%  figure(4)
% [tcc tch] = tricontour( Tri,xvSorted,yvSorted,VorvSorted,(Cyl_Min:.3:Cyl_Max));
% hold on;
% line([5.8,5.8],[-4,4],'lineStyle','--','Color',[0.5 0.5 0.5],'linewidth',lw);
% set (tch, 'LineWidth', lw);
% xlim([2.5,10.5]);
% ylim([-4,4]);
% %axis box;
% axis off;
% 
% print( strcat('../paperUseImagesF/',filenameFrPrint,'_Type4_contour'), '-dpng','-r500');

%% line graph.
% find all intersections of line with triangles.
xlp = [3.7 3.7];
ylp = [-4, 4];

% Find all edges in mesh, note internal edges are repeated
F = tri;
E = sort([F(:,1) F(:,2); F(:,2) F(:,3); F(:,3) F(:,1)]')';
% determine uniqueness of edges
[u,m,n] = unique(E,'rows');

%%


count = 0;
for loop = 1:max(size(u))
    edg = u(loop,:);
    edgx = [xval(edg(1)),xval(edg(2))];
    edgy = [yval(edg(1)),yval(edg(2))];
    edgVals = [Vor2ndKindval(edg(1)),Vor2ndKindval(edg(1))];
    [xin,yin] = polyxpoly(xlp, ylp, edgx, edgy);
    if(~isempty(xin))
        disp(xin);
        disp(yin);
        count=count+1;
        ylstor(count) = yin;
        pt = sum((edgx(1)-edgx(2))^2 + (edgy(1)-edgy(2))^2).^(0.5);
        t = sum((edgx(1)-xin)^2 + (edgy(1)-yin)^2).^(0.5);
        r = t/pt;
        yvalstor(count) = edgVals(1) + ( edgVals(2) - edgVals(1) )*r;
    end
end
%%
figure(4)
load ('../xtigTagsforLabelUseat3p7.mat');
[tmp,ind]=sort(ylstor);
yvalstor_sort=yvalstor(ind);
plot(tmp,yvalstor_sort,'-k','LineWidth',lw,'MarkerSize',msz);
set(gca,'XTick',sort(unique(tmp)),'Xticklabel','');
ylabel('vorticity')
set(gca,'box','off')
axis([min(tmp) max(tmp) min(yvalstor) max(yvalstor)]);

ax1 = gca;
ax2 = axes('Position', get(ax1, 'Position'),'Color', 'none');
set(ax2, 'XAxisLocation', 'top','YAxisLocation','Right');
set(ax2, 'XLim', get(ax1, 'XLim'),'YLim', get(ax1, 'YLim'));
set(ax2, 'XTick',sort(sxTTags),'YTick',[]);
set(ax2, 'XTickLabel', [ ] ,'YTickLabel', [ ] );
print(strcat('../paperUseImagesF/',filenameFrPrint,'_Vor2style_NCR_lineGraph'), '-dpng','-r500');


