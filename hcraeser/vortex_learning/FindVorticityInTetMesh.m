% test tet for a 3D cube i,j,k
%clear
function vor_lineSeg = FindVorticityInTetMesh(X,Y,Z,U,V,W)
    switch nargin
        case 0
            x = (-1:.2:1);
            [X,Y,Z] = meshgrid( x ,x ,x );
            U = zeros(size(X));
            V = zeros(size(Y));
            W = zeros(size(Z));

            [~,size_x] = size(x);
            r = rand(1,3);
            for i = 1:size_x
                for j = 1:size_x
                    for k = 1:size_x
                        rc = rand(1,3);
                        coords = [ X(i,j,k), Y(i,j,k), Z(i,j,k) ];
                        vel = cross(coords,[1+(1),1+r(2),1+r(3)]);
                       % vel = vel./norm(vel);
                        U(i,j,k) = vel(1);
                        V(i,j,k) = vel(2);
                        W(i,j,k) = vel(3);
                    end
                end
            end
    end

    %quiver3(X,Y,Z,U,V,W);hold on;
    vor_lineSeg = [];
    [size_x,size_y,size_z] = size(X);
    for i = 1:size_x-1
        for j = 1:size_y -1
            for k = 1:size_z -1
                % tet 1.
                coords(2,:) = [X(i,j,k),  Y(i,j,k),  Z(i,j,k)]; 
                vel(2,:)    = [U(i,j,k),  V(i,j,k),  W(i,j,k)];
                coords(1,:) = [X(i+1,j,k),Y(i+1,j,k),Z(i+1,j,k)];
                vel(1,:)    = [U(i+1,j,k),V(i+1,j,k),W(i+1,j,k)];
                coords(3,:) = [X(i,j+1,k),Y(i,j+1,k),Z(i,j+1,k)];
                vel(3,:)    = [U(i,j+1,k),V(i,j+1,k),W(i,j+1,k)];
                coords(4,:) = [X(i,j,k+1),Y(i,j,k+1),Z(i,j,k+1)];
                vel(4,:)    = [U(i,j,k+1),V(i,j,k+1),W(i,j,k+1)];
                pts = FindVorticy(coords,vel);
                if ~isempty(pts)
                    vor_lineSeg(end+1,:,:) = pts;
                end
                %dot( cross( coords(2,:)-coords(1,:),coords(3,:)-coords(1,:) ), coords(4,:)-coords(1,:) );

                % tet 2,
                coords(2,:) = [X(i+1,j,k),  Y(i+1,j,k),  Z(i+1,j,k)]; 
                vel(2,:)   =  [U(i+1,j,k),  V(i+1,j,k),  W(i+1,j,k)];
                coords(1,:) = [X(i+1,j+1,k),Y(i+1,j+1,k),Z(i+1,j+1,k)];
                vel(1,:)    = [U(i+1,j+1,k),V(i+1,j+1,k),W(i+1,j+1,k)];
                coords(3,:) = [X(i,j+1,k),Y(i,j+1,k),Z(i,j+1,k)];
                vel(3,:)    = [U(i,j+1,k),V(i,j+1,k),W(i,j+1,k)];
                coords(4,:) = [X(i+1,j+1,k+1),Y(i+1,j+1,k+1),Z(i+1,j+1,k+1)];
                vel(4,:)    = [U(i+1,j+1,k+1),V(i+1,j+1,k+1),W(i+1,j+1,k+1)];
                %dot( cross( coords(2,:)-coords(1,:),coords(3,:)-coords(1,:) ), coords(4,:)-coords(1,:) ); 
                pts = FindVorticy(coords,vel);
                if ~isempty(pts)
                    vor_lineSeg(end+1,:,:) = pts;
                end

                % tet 3,
                coords(2,:) = [X(i,j,k+1),  Y(i,j,k+1),  Z(i,j,k+1)]; 
                vel(2,:)   =  [U(i,j,k+1),  V(i,j,k+1),  W(i,j,k+1)];
                coords(1,:) = [X(i+1,j+1,k+1),Y(i+1,j+1,k+1),Z(i+1,j+1,k+1)];
                vel(1,:)    = [U(i+1,j+1,k+1),V(i+1,j+1,k+1),W(i+1,j+1,k+1)];
                coords(3,:) = [X(i+1,j,k+1),Y(i+1,j,k+1),Z(i+1,j,k+1)];
                vel(3,:)    = [U(i+1,j,k+1),V(i+1,j,k+1),W(i+1,j,k+1)];
                coords(4,:) = [X(i+1,j,k),Y(i+1,j,k),Z(i+1,j,k)];
                vel(4,:)    = [U(i+1,j,k),V(i+1,j,k),W(i+1,j,k)];
                %dot( cross( coords(2,:)-coords(1,:),coords(3,:)-coords(1,:) ), coords(4,:)-coords(1,:) );
                pts = FindVorticy(coords,vel);
                if ~isempty(pts)
                    vor_lineSeg(end+1,:,:) = pts;
                end

                %tet 4,
                coords(2,:) = [X(i,j,k+1),  Y(i,j,k+1),  Z(i,j,k+1)]; 
                vel(2,:)   =  [U(i,j,k+1),  V(i,j,k+1),  W(i,j,k+1)];
                coords(1,:) = [X(i,j+1,k+1),Y(i,j+1,k+1),Z(i,j+1,k+1)];
                vel(1,:)    = [U(i,j+1,k+1),V(i,j+1,k+1),W(i,j+1,k+1)];
                coords(3,:) = [X(i+1,j+1,k+1),Y(i+1,j+1,k+1),Z(i+1,j+1,k+1)];
                vel(3,:)    = [U(i+1,j+1,k+1),V(i+1,j+1,k+1),W(i+1,j+1,k+1)];
                coords(4,:) = [X(i,j+1,k),Y(i,j+1,k),Z(i,j+1,k)];
                vel(4,:)    = [U(i,j+1,k),V(i,j+1,k),W(i,j+1,k)];
                pts = FindVorticy(coords,vel);
                if ~isempty(pts)
                    vor_lineSeg(end+1,:,:) = pts;
                end
                %dot( cross( coords(2,:)-coords(1,:),coords(3,:)-coords(1,:) ), coords(4,:)-coords(1,:) );
            end
        end
    end
    %% Draw
    [num_att,~,~] = size(vor_lineSeg);
    for i = 1:num_att
        plot3( vor_lineSeg(i,:,1), vor_lineSeg(i,:,2),vor_lineSeg(i,:,3),'-g*');
        hold on;
    end
end

