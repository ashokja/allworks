% create file names.
clear;
T = 11;
MR = (1:T)*7+1;

addpath('..');

Deg=2;
ord = zeros(1,16);
ord(1) = 168;    ord(2) = 672;    ord(3) = 1512;   ord(4) = 2688;
ord(5) = 4200;   ord(6) = 6048;   ord(7) = 8232;   ord(8) = 10752;
ord(9) = 13608;  ord(10) = 16800; ord(11) = 20328; ord(12) = 24192;
ord(13) = 28392; ord(14) = 32928; ord(15) = 37800; ord(16) = 43008;

%use HL and VL
KF ='0';
P = '2';
KF ='0';
UF = '0';
pfilename = '../PostP2_2/Data_ALLMSH/log_hex_unionjack_DG_CART_MR';
count =1;
for mr = MR
    filename = strcat(pfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C(count,:) = ReadFile(filename);
    count= count+1;
end

L2B_ord = C(:,1);
L2_ord = C(:,2);
LinfB_ord = C(:,3);
Linf_ord = C(:,4);



%use MaxSize
KF ='0';
KF ='0';
UF = '0';
pfilename = '../PostP_hexUnion_MaxSize/Data_ALLMSH/log_hex_unionjack_DG_MAXSIZE_MR';
count =1;
for mr = MR
    filename = strcat(pfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C(count,:) = ReadFile(filename);
    count= count+1;
end
L2BH1_ord = C(:,1);
L2H1_ord = C(:,2);
LinfBH1_ord = C(:,3);
LinfH1_ord = C(:,4);
% 
%use Hex
KF ='0';

KF ='1';
UF = '0';
pfilename = '../PostP2_2/Data_ALLMSH/log_hex_unionjack_DG_CART_MR';
count =1;
for mr = MR
    filename = strcat(pfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C(count,:) = ReadFile(filename);
    count= count+1;
end
L2BH2_ord = C(:,1);
L2H2_ord = C(:,2);
LinfBH2_ord = C(:,3);
LinfH2_ord = C(:,4);


fileID = fopen('P2_hexUnionJack_DG_AllMESH_ALLERR.txt','w');

line0= '\caption{DG solution , Hex union, All SIAC filter, L2 Error.}';
fwrite(fileID,line0);

% write code to print L2
 line1= strcat('\label{table:4}',...
  '\begin{tabular}{||c| c|c| c| c| c ||}',...
 ' \hline ');
fwrite(fileID,line1);
line2 = ' Mesh Ele & Deg &L2(Before) & L2(Cart) & L2(Cart(M)) & L2(Hex) \\ [0.5ex] ';
fwrite(fileID,line2);
line3 = ' \hline\hline ';
fwrite(fileID,line3);
for i=1:T
    line4 = strcat('$',num2str(ord(i)),'$ & $',num2str(Deg),'$ & $',...
         num2str(L2B_ord(i),'%1.2e'), '$ & $',...
        num2str(L2_ord(i),'%1.2e'), '$ & $',...
        num2str(L2H1_ord(i),'%1.2e'), '$ & $',...
        num2str(L2H2_ord(i),'%1.2e'), '$ ',...
        '\\');
    line5 = ' \hline ';
    fwrite(fileID,line4);
	fwrite(fileID,line5);
end
line11= ' \hline \end{tabular} ';
fwrite(fileID,line11);
fclose(fileID);








fileID = fopen('P2_hexUnionJack_DG_AllMESH_ALLERR_Linf.txt','w');

line0= '\caption{DG solution , Hex union, All SIAC filter, Linf Error.}';
fwrite(fileID,line0);

% write code to print L2
 line1= strcat('\label{table:4}',...
  '\begin{tabular}{||c| c|c| c| c| c ||}',...
 ' \hline ');
fwrite(fileID,line1);
line2 = ' Mesh Ele & Deg &Linf(Before) & Linf(Cart) & L2(Cart(M)) & Linf(Hex) \\ [0.5ex] ';
fwrite(fileID,line2);
line3 = ' \hline\hline ';
fwrite(fileID,line3);
for i=1:T
    line4 = strcat('$',num2str(ord(i)),'$ & $',num2str(Deg),'$ & $',...
         num2str(LinfB_ord(i),'%1.2e'), '$ & $',...
        num2str(Linf_ord(i),'%1.2e'), '$ & $',...
        num2str(LinfH1_ord(i),'%1.2e'), '$ & $',...
        num2str(LinfH2_ord(i),'%1.2e'), '$ ',...
        '\\');
    line5 = ' \hline ';
    fwrite(fileID,line4);
	fwrite(fileID,line5);
end
line11= ' \hline \end{tabular} ';
fwrite(fileID,line11);
fclose(fileID);
 
 
 % % line 1.
% line4 = strcat('$',num2str(ord1),'$ & $',num2str(Deg),'$ & $',...
%      num2str(L2B_ord(1),'%1.2e'), '$ & $',...
%     num2str(orderB_L2(1)), '$ & $',...
%     num2str(L2_ord(1),'%1.2e'), '$ & $',...
%     num2str(order_L2(1)), '$ & $',...
%     num2str(L2H1_ord(1),'%1.2e'), '$ & $',...
%     num2str(orderH1_L2(1)), '$ & $',...
%     num2str(L2H2_ord(1),'%1.2e'), '$ & $',...
%     num2str(orderH2_L2(1)), '$ ',...
%     '\\');
% line5 = ' \hline ';
% % line 2.
% line6 = strcat('$',num2str(ord2),'$ & $',num2str(Deg),'$ & $',...
%      num2str(L2B_ord(2),'%1.2e'), '$ & $',...
%        num2str(orderB_L2(2)), '$ & $',...
%     num2str(L2_ord(2),'%1.2e'), '$ & $',...
%     num2str(order_L2(2)), '$ & $',...
%     num2str(L2H1_ord(2),'%1.2e'), '$ & $',...
%     num2str(orderH1_L2(2)), '$ & $',...
%     num2str(L2H2_ord(2),'%1.2e'), '$ & $',...
%     num2str(orderH2_L2(2)), '$ ',...
%     '\\');
% line7 = ' \hline ';
% 
% line8 = strcat('$',num2str(ord3),'$ & $',num2str(Deg),'$ & $',...
%      num2str(L2B_ord(3),'%1.2e'), '$ & $',...
%          num2str(orderB_L2(3)), '$ & $',...
%     num2str(L2_ord(3),'%1.2e'), '$ & $',...
%     num2str(order_L2(3)), '$ & $',...
%     num2str(L2H1_ord(3),'%1.2e'), '$ & $',...
%     num2str(orderH1_L2(3)), '$ & $',...
%     num2str(L2H2_ord(3),'%1.2e'), '$ & $',...
%     num2str(orderH2_L2(3)), '$ ',...
%     '\\');
% line9 = ' \hline ';
% 
% line10 = strcat('$',num2str(ord4),'$ & $',num2str(Deg),'$ & $',...
%      num2str(L2B_ord(4),'%1.2e'), '$ & $',...
%          num2str(orderB_L2(4)), '$ & $',...
%     num2str(L2_ord(4),'%1.2e'), '$ & $',...
%     num2str(order_L2(4)), '$ & $',...
%     num2str(L2H1_ord(4),'%1.2e'), '$ & $',...
%     num2str(orderH1_L2(4)), '$ & $',...
%     num2str(L2H2_ord(4),'%1.2e'), '$ & $',...
%     num2str(orderH2_L2(4)), '$ ',...
%     '\\');
% line11= ' \hline \end{tabular} ';
% 
% 
% fileID = fopen('P1_hexUnionJack_quad_m_1_DG_All_L2.txt','w');
% fwrite(fileID,line0);
% fwrite(fileID,line1);
% fwrite(fileID,line2);
% fwrite(fileID,line3);
% fwrite(fileID,line4);
% fwrite(fileID,line5);
% fwrite(fileID,line6);
% fwrite(fileID,line7);
% fwrite(fileID,line8);
% fwrite(fileID,line9);
% fwrite(fileID,line10);
% fwrite(fileID,line11);
% fclose(fileID);





















% 
% 
% orderB_L2(1) = 0;
% orderB_L2(2) = log(L2B_ord(2)/L2B_ord(1))/log(ord1/ord2)*2;
% orderB_L2(3) = log(L2B_ord(3)/L2B_ord(2))/log(ord2/ord3)*2;
% orderB_L2(4) = log(L2B_ord(4)/L2B_ord(3))/log(ord3/ord4)*2;
% 
% orderB_Linf(1) = 0;
% orderB_Linf(2) = log(LinfB_ord(2)/LinfB_ord(1))/log(ord1/ord2)*2;
% orderB_Linf(3) = log(LinfB_ord(3)/LinfB_ord(2))/log(ord2/ord3)*2;
% orderB_Linf(4) = log(LinfB_ord(4)/LinfB_ord(3))/log(ord3/ord4)*2;
% 
% order_L2(1) = 0;
% order_L2(2) = log(L2_ord(2)/L2_ord(1))/log(ord1/ord2)*2;
% order_L2(3) = log(L2_ord(3)/L2_ord(2))/log(ord2/ord3)*2;
% order_L2(4) = log(L2_ord(4)/L2_ord(3))/log(ord3/ord4)*2;
% 
% order_Linf(1) = 0;
% order_Linf(2) = log(Linf_ord(2)/Linf_ord(1))/log(ord1/ord2)*2;
% order_Linf(3) = log(Linf_ord(3)/Linf_ord(2))/log(ord2/ord3)*2;
% order_Linf(4) = log(Linf_ord(4)/Linf_ord(3))/log(ord3/ord4)*2;
% 
% orderH1_L2(1) = 0;
% orderH1_L2(2) = log(L2H1_ord(2)/L2H1_ord(1))/log(ord1/ord2)*2;
% orderH1_L2(3) = log(L2H1_ord(3)/L2H1_ord(2))/log(ord2/ord3)*2;
% orderH1_L2(4) = log(L2H1_ord(4)/L2H1_ord(3))/log(ord3/ord4)*2;
% 
% orderH1_Linf(1) = 0;
% orderH1_Linf(2) = log(LinfH1_ord(2)/LinfH1_ord(1))/log(ord1/ord2)*2;
% orderH1_Linf(3) = log(LinfH1_ord(3)/LinfH1_ord(2))/log(ord2/ord3)*2;
% orderH1_Linf(4) = log(LinfH1_ord(4)/LinfH1_ord(3))/log(ord3/ord4)*2;
% 
% orderH2_L2(1) = 0;
% orderH2_L2(2) = log(L2H2_ord(2)/L2H2_ord(1))/log(ord1/ord2)*2;
% orderH2_L2(3) = log(L2H2_ord(3)/L2H2_ord(2))/log(ord2/ord3)*2;
% orderH2_L2(4) = log(L2H2_ord(4)/L2H2_ord(3))/log(ord3/ord4)*2;
% 
% orderH2_Linf(1) = 0;
% orderH2_Linf(2) = log(LinfH2_ord(2)/LinfH2_ord(1))/log(ord1/ord2)*2;
% orderH2_Linf(3) = log(LinfH2_ord(3)/LinfH2_ord(2))/log(ord2/ord3)*2;
% orderH2_Linf(4) = log(LinfH2_ord(4)/LinfH2_ord(3))/log(ord3/ord4)*2;
% 
% orderB_L2
% orderB_Linf
% 
% order_L2
% order_Linf
% 
% orderH1_L2
% orderH1_Linf
% 
% orderH2_L2
% orderH2_Linf
% 
% 
% line0= '\caption{DG solution , Hex union, All SIAC filter, L2 Error.}';
% 
% % write code to print L2
% line1= strcat('\label{table:4}',...
%  '\begin{tabular}{||c| c|c| c| c| c| c| c | c|c ||}',...
% ' \hline ');
% line2 = ' Mesh Ele & Deg &L2(Before)& order & L2(Cart) & order& L2(Cart(M)) & order& L2(Hex) & order \\ [0.5ex] ';
% line3 = ' \hline\hline ';
% % line 1. 
% line4 = strcat('$',num2str(ord1),'$ & $',num2str(Deg),'$ & $',...
%      num2str(L2B_ord(1),'%1.2e'), '$ & $',...
%     num2str(orderB_L2(1)), '$ & $',...
%     num2str(L2_ord(1),'%1.2e'), '$ & $',...
%     num2str(order_L2(1)), '$ & $',...
%     num2str(L2H1_ord(1),'%1.2e'), '$ & $',...
%     num2str(orderH1_L2(1)), '$ & $',...
%     num2str(L2H2_ord(1),'%1.2e'), '$ & $',...
%     num2str(orderH2_L2(1)), '$ ',...
%     '\\');
% line5 = ' \hline ';
% % line 2.
% line6 = strcat('$',num2str(ord2),'$ & $',num2str(Deg),'$ & $',...
%      num2str(L2B_ord(2),'%1.2e'), '$ & $',...
%        num2str(orderB_L2(2)), '$ & $',...
%     num2str(L2_ord(2),'%1.2e'), '$ & $',...
%     num2str(order_L2(2)), '$ & $',...
%     num2str(L2H1_ord(2),'%1.2e'), '$ & $',...
%     num2str(orderH1_L2(2)), '$ & $',...
%     num2str(L2H2_ord(2),'%1.2e'), '$ & $',...
%     num2str(orderH2_L2(2)), '$ ',...
%     '\\');
% line7 = ' \hline ';
% 
% line8 = strcat('$',num2str(ord3),'$ & $',num2str(Deg),'$ & $',...
%      num2str(L2B_ord(3),'%1.2e'), '$ & $',...
%          num2str(orderB_L2(3)), '$ & $',...
%     num2str(L2_ord(3),'%1.2e'), '$ & $',...
%     num2str(order_L2(3)), '$ & $',...
%     num2str(L2H1_ord(3),'%1.2e'), '$ & $',...
%     num2str(orderH1_L2(3)), '$ & $',...
%     num2str(L2H2_ord(3),'%1.2e'), '$ & $',...
%     num2str(orderH2_L2(3)), '$ ',...
%     '\\');
% line9 = ' \hline ';
% 
% line10 = strcat('$',num2str(ord4),'$ & $',num2str(Deg),'$ & $',...
%      num2str(L2B_ord(4),'%1.2e'), '$ & $',...
%          num2str(orderB_L2(4)), '$ & $',...
%     num2str(L2_ord(4),'%1.2e'), '$ & $',...
%     num2str(order_L2(4)), '$ & $',...
%     num2str(L2H1_ord(4),'%1.2e'), '$ & $',...
%     num2str(orderH1_L2(4)), '$ & $',...
%     num2str(L2H2_ord(4),'%1.2e'), '$ & $',...
%     num2str(orderH2_L2(4)), '$ ',...
%     '\\');
% line11= ' \hline \end{tabular} ';
% 
% 
% fileID = fopen('P1_hexUnionJack_quad_m_1_DG_All_L2.txt','w');
% fwrite(fileID,line0);
% fwrite(fileID,line1);
% fwrite(fileID,line2);
% fwrite(fileID,line3);
% fwrite(fileID,line4);
% fwrite(fileID,line5);
% fwrite(fileID,line6);
% fwrite(fileID,line7);
% fwrite(fileID,line8);
% fwrite(fileID,line9);
% fwrite(fileID,line10);
% fwrite(fileID,line11);
% fclose(fileID);
% 
% 
% 
% % write code to print Linf
% line0= '\caption{DG solution, Hex union, All SIAC filter, Linf Error.}';
% 
% line1= strcat('\label{table:4}',...
%  '\begin{tabular}{||c| c| c| c| c| c| c| c | c|c ||}',...
% ' \hline ');
% line2 = ' Mesh Ele & Deg & Linf(Before)& order & Linf(Cart) & order& Linf(Cart(M)) & order& Linf(Hex) & order \\ [0.5ex] ';
% line3 = ' \hline\hline ';
% % line 1. 
% line4 = strcat('$',num2str(ord1),'$ & $',num2str(Deg),'$ & $',...
%      num2str(LinfB_ord(1),'%1.2e'), '$ & $',...
%     num2str(orderB_Linf(1)), '$ & $',...
%     num2str(Linf_ord(1),'%1.2e'), '$ & $',...
%     num2str(order_Linf(1)), '$ & $',...
%     num2str(LinfH1_ord(1),'%1.2e'), '$ & $',...
%     num2str(orderH1_Linf(1)), '$ & $',...
%     num2str(LinfH2_ord(1),'%1.2e'), '$ & $',...
%     num2str(orderH2_Linf(1)), '$ ',...
%     '\\');
% line5 = ' \hline ';
% % line 2.
% line6 = strcat('$',num2str(ord2),'$ & $',num2str(Deg),'$ & $',...
%      num2str(LinfB_ord(2),'%1.2e'), '$ & $',...
%      num2str(orderB_Linf(2)), '$ & $',...
%     num2str(Linf_ord(2),'%1.2e'), '$ & $',...
%     num2str(order_Linf(2)), '$ & $',...
%     num2str(LinfH1_ord(2),'%1.2e'), '$ & $',...
%     num2str(orderH1_Linf(2)), '$ & $',...
%     num2str(LinfH2_ord(2),'%1.2e'), '$ & $',...
%     num2str(orderH2_Linf(2)), '$ ',...
%     '\\');
% line7 = ' \hline ';
% 
% line8 = strcat('$',num2str(ord3),'$ & $',num2str(Deg),'$ & $',...
%      num2str(LinfB_ord(3),'%1.2e'), '$ & $',...
%      num2str(orderB_Linf(3)), '$ & $',...
%     num2str(Linf_ord(3),'%1.2e'), '$ & $',...
%     num2str(order_Linf(3)), '$ & $',...
%     num2str(LinfH1_ord(3),'%1.2e'), '$ & $',...
%     num2str(orderH1_Linf(3)), '$ & $',...
%     num2str(LinfH2_ord(3),'%1.2e'), '$ & $',...
%     num2str(orderH2_Linf(3)), '$ ',...
%     '\\');
% line9 = ' \hline ';
% 
% line10 = strcat('$',num2str(ord4),'$ & $',num2str(Deg),'$ & $',...
%      num2str(LinfB_ord(4),'%1.2e'), '$ & $',...
%      num2str(orderB_Linf(4)), '$ & $',...
%     num2str(Linf_ord(4),'%1.2e'), '$ & $',...
%     num2str(order_Linf(4)), '$ & $',...
%     num2str(LinfH1_ord(4),'%1.2e'), '$ & $',...
%     num2str(orderH1_Linf(4)), '$ & $',...
%     num2str(LinfH2_ord(4),'%1.2e'), '$ & $',...
%     num2str(orderH2_Linf(4)), '$ ',...
%     '\\');
% line11= ' \hline \end{tabular} ';
% 
% 
% fileID = fopen('P1_hexUnionJack_quad_m_1_DG_All_Linf.txt','w');
% fwrite(fileID,line0);
% fwrite(fileID,line1);
% fwrite(fileID,line2);
% fwrite(fileID,line3);
% fwrite(fileID,line4);
% fwrite(fileID,line5);
% fwrite(fileID,line6);
% fwrite(fileID,line7);
% fwrite(fileID,line8);
% fwrite(fileID,line9);
% fwrite(fileID,line10);
% fwrite(fileID,line11);
% fclose(fileID);
% 
