clear;
close all;
% test 

xpos = textread('xc0_Der4kp1.txt', '%f\t');
fval = textread('fce_Der4kp1.txt', '%f\t');
fvalD = textread('fceD_Der4kp1.txt', '%f\t');
pval = textread('ece_Der4kp1.txt', '%f\t');
pvalD = textread('eceD_Der4kp1.txt', '%f\t');
sval = textread('sce_Der4kp1.txt', '%f\t');


xs = 1;
xe = 63;

figure(1)
plot(xpos, fval,xpos,pval,xpos,sval);
xlabel('1D mesh(x)')
ylabel('values')
legend('Actual function','projected','SIAC post-processed');

figure(2)
plot(xpos, fvalD,xpos,pvalD,xpos,sval);
xlabel('1D mesh(x)')
ylabel('values')
legend('Actual function','projected','SIAC post-processed');

figure(3),
plot( xpos(xs:xe), abs(fvalD(xs:xe)-pvalD(xs:xe)), xpos(xs:xe), abs(fvalD(xs:xe)-sval(xs:xe)));
xlabel('1D mesh(x)')
ylabel('error')
legend('projection error','SIAC error');