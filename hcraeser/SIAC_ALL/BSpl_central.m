function v_eval = BSpl_central(x_eval,k)
    switch nargin
        case 0
            x_eval = -1:.1:1;
            k = 0;
    end
    
    if 0==k
        v_eval = (abs(x_eval)<=1/2);
    elseif 1==k % case k=2 creates an interesting problem at x =0.
        v_eval = zeros(size(x_eval));
        I = find(abs(x_eval)<=1);
        v_eval(I) = 1-abs(x_eval(I));
    else % for k >=1
        v_eval = 1/k.*(((k+1)/2+x_eval).*BSpl_central(x_eval+1/2,k-1) + ...
             ((k+1)/2-x_eval).*BSpl_central(x_eval-1/2,k-1) );
    end
    
end