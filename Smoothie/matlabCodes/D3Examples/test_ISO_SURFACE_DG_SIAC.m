% which c++ file creates these guys.
% How to Create them.
% 


clear;
close all;
va = -11.5; vb = 44;

rate = 2;
samplerate = 10;
Order = 2;
lc = 1;
wstr = '0d5A';
if lc ==1
%    letter = strcat('R41_O_',int2str(Order),'_w_',wstr) ;
    letter = strcat('_O_',int2str(Order),'_w_',wstr) ;
end
%filter = 1;

filename = strcat( 'P',int2str(rate),'_CubeGrid_',int2str(samplerate),letter );
xfname = strcat('../datatxt/VizData/',filename,'_x.txt' )
yfname = strcat('../datatxt/VizData/',filename,'_y.txt' );
zfname = strcat('../datatxt/VizData/',filename,'_z.txt' );

upfname = strcat('../datatxt/VizData/',filename,'_up.txt' );
vpfname = strcat('../datatxt/VizData/',filename,'_vp.txt' );
wpfname = strcat('../datatxt/VizData/',filename,'_wp.txt' );

Vxpfname = strcat('../datatxt/VizData/',filename,'_Vor_xp.txt' );
Vypfname = strcat('../datatxt/VizData/',filename,'_Vor_yp.txt' );
Vzpfname = strcat('../datatxt/VizData/',filename,'_Vor_zp.txt' );

ufname = strcat('../datatxt/VizData/',filename,'_u.txt' );
vfname = strcat('../datatxt/VizData/',filename,'_v.txt' );
wfname = strcat('../datatxt/VizData/',filename,'_w.txt' );

Vxfname = strcat('../datatxt/VizData/',filename,'_Vor_x.txt' );
Vyfname = strcat('../datatxt/VizData/',filename,'_Vor_y.txt' );
Vzfname = strcat('../datatxt/VizData/',filename,'_Vor_z.txt' );

xdata = textread(xfname, '%f\t');
ydata = textread(yfname, '%f\t');
zdata = textread(zfname, '%f\t');

updata = textread(upfname, '%f\t');     udata = textread(ufname, '%f\t');
vpdata = textread(vpfname, '%f\t');     vdata = textread(vfname, '%f\t');
wpdata = textread(wpfname, '%f\t');     wdata = textread(wfname, '%f\t');

Vxpdata = textread(Vxpfname,'%f\t');    Vxdata = textread(Vxfname,'%f\t');
Vypdata = textread(Vypfname,'%f\t');    Vydata = textread(Vyfname,'%f\t');
Vzpdata = textread(Vzpfname,'%f\t');    Vzdata = textread(Vzfname,'%f\t');



s = floor((max(size(xdata(:))))^(1/3)+0.0001);

xdataV = reshape(xdata,[s,s,s]); ydataV = reshape(ydata,[s,s,s]); zdataV = reshape(zdata,[s,s,s]);
updataV = reshape(updata,[s,s,s]); vpdataV = reshape(vpdata,[s,s,s]); wpdataV = reshape(wpdata,[s,s,s]);
udataV = reshape(udata,[s,s,s]); vdataV = reshape(vdata,[s,s,s]); wdataV = reshape(wdata,[s,s,s]);
VxpdataV = reshape(Vxpdata,[s,s,s]); VypdataV = reshape(Vypdata,[s,s,s]); VzpdataV = reshape(Vzpdata,[s,s,s]);
VxdataV = reshape(Vxdata,[s,s,s]); VydataV = reshape(Vydata,[s,s,s]); VzdataV = reshape(Vzdata,[s,s,s]);

xdataV = permute(xdataV,[2,3,1]);
ydataV = permute(ydataV,[2,3,1]);
zdataV = permute(zdataV,[2,3,1]);

udataV = permute(udataV,[2,3,1]);
vdataV = permute(vdataV,[2,3,1]);
wdataV = permute(wdataV,[2,3,1]);

updataV = permute(updataV,[2,3,1]);
vpdataV = permute(vpdataV,[2,3,1]);
wpdataV = permute(wpdataV,[2,3,1]);

VxpdataV = permute(VxpdataV,[2,3,1]);
VypdataV = permute(VypdataV,[2,3,1]);
VzpdataV = permute(VzpdataV,[2,3,1]);

VxdataV = permute(VxdataV,[2,3,1]);
VydataV = permute(VydataV,[2,3,1]);
VzdataV = permute(VzdataV,[2,3,1]);
%%
figure(1)
%plotBoundingBox(); hold on;
p = patch(isosurface(xdataV,ydataV,zdataV,VzpdataV,1.5));
isonormals(xdataV,ydataV,zdataV,VzpdataV,p)
p.FaceColor = 'red';
p.EdgeColor = 'none';
daspect([1,1,1])
view(va,vb); axis equal
camlight 
lighting gouraud
plotBoundingBox(); hold on;
hold off;
axis off;
% % %print(strcat('images/',filename,'_Vz_DGc' ), '-dpng', '-r500');

%title('VorZ (iso surface=1.5)vec using DG');
%saveas(gcf,'VORZ_ISO_SURFACE_1_5_DG.png')

%%
figure(2)
%plotBoundingBox(); hold on;
p = patch(isosurface(xdataV,ydataV,zdataV,VzdataV,1.5));
isonormals(xdataV,ydataV,zdataV,VzdataV,p)
p.FaceColor = 'red';
p.EdgeColor = 'none';
%daspect([1,1,1])
view(va,vb); axis equal;
grid off;
axis off;
camlight 
plotBoundingBox(); hold on;
lighting gouraud
% % % print(strcat('images/',filename,'_Vz_S' ), '-dpng', '-r500');


%%
% New assignment.
VmgpdataV = (VzpdataV.^2+ VxpdataV.^2+ VypdataV.^2).^(1/2);
VmgsdataV = (VzdataV.^2+ VxdataV.^2+ VydataV.^2).^(1/2);
figure(3)
    p = patch(isosurface(xdataV,ydataV,zdataV,VmgpdataV,150));
    isonormals(xdataV,ydataV,zdataV,VmgpdataV,p)
    p.FaceColor = 'red';
    p.EdgeColor = 'none';
    %daspect([1,1,1])
    view(va,vb); axis equal;
    grid off;
    axis off;
    camlight 
    plotBoundingBox(); hold on;
    lighting gouraud


figure(4)
    p = patch(isosurface(xdataV,ydataV,zdataV,VmgsdataV,200));
    isonormals(xdataV,ydataV,zdataV,VmgsdataV,p)
    p.FaceColor = 'red';
    p.EdgeColor = 'none';
    %daspect([1,1,1])
    view(va,vb); axis equal;
    grid off;
    axis off;
    camlight 
    plotBoundingBox(); hold on;
    lighting gouraud

%






%hold off;
%title('VorZ (isoSurface=1.5)vec using SIAC');
%saveas(gcf,'VORZ_ISO_SURFACE_1_5_SIAC.png')

% % % % % %%
% % % % % figure(3)
% % % % % plotBoundingBox(); hold on;
% % % % % p = patch(isosurface(xdataV,ydataV,zdataV,VxpdataV,0.5));
% % % % % isonormals(xdataV,ydataV,zdataV,VxpdataV,p)
% % % % % p.FaceColor = 'red';
% % % % % p.EdgeColor = 'none';
% % % % % daspect([1,1,1])
% % % % % view(va,vb); axis tight
% % % % % camlight 
% % % % % lighting gouraud
% % % % % hold off;
% % % % % title('VorX (iso surface=1.5)vec using DG');
% % % % % %saveas(gcf,'VORX_ISO_SURFACE_1_5_DG.png')
% % % % % 
% % % % % 
% % % % % figure(4)
% % % % % %plotBoundingBox(); hold on;
% % % % % p = patch(isosurface(xdataV,ydataV,zdataV,VxdataV,0.5));
% % % % % isonormals(xdataV,ydataV,zdataV,VxdataV,p)
% % % % % p.FaceColor = 'red';
% % % % % p.EdgeColor = 'none';
% % % % % %daspect([1,1,1])
% % % % % view(va,vb); axis equal;
% % % % % grid off;
% % % % % axis off;
% % % % % camlight 
% % % % % lighting gouraud
% % % % % %print(strcat('images/',filename,'_Vx_S' ), '-dpng', '-r500');
% % % % % %saveas(gcf,'VORX_ISO_SURFACE_1_5_SIAC.png')
% % % % % 
% % % % % %%
% % % % % 
% % % % % %Interesting relation. Iso surface of Vy ~ IsoSurface of -Vx
% % % % % 
% % % % % figure(5)
% % % % % plotBoundingBox(); hold on;
% % % % % p = patch(isosurface(xdataV,ydataV,zdataV,VypdataV,-0.3));
% % % % % isonormals(xdataV,ydataV,zdataV,VypdataV,p)
% % % % % p.FaceColor = 'red';
% % % % % p.EdgeColor = 'none';
% % % % % daspect([1,1,1])
% % % % % view(va,vb); axis tight
% % % % % camlight 
% % % % % lighting gouraud
% % % % % hold off;
% % % % % title('VorY (iso surface=1.5)vec using DG');
% % % % % %saveas(gcf,'VORY_ISO_SURFACE_1_5_DG.png')
% % % % % 
% % % % % 
% % % % % figure(6)
% % % % % %plotBoundingBox(); hold on;
% % % % % p = patch(isosurface(xdataV,ydataV,zdataV,VydataV,-0.3));
% % % % % isonormals(xdataV,ydataV,zdataV,VydataV,p)
% % % % % p.FaceColor = 'red';
% % % % % p.EdgeColor = 'none';
% % % % % %daspect([1,1,1])
% % % % % view(va,vb); axis equal;
% % % % % grid off;
% % % % % axis off;
% % % % % camlight 
% % % % % lighting gouraud
% % % % % %print(strcat('images/',filename,'_Vy_S' ), '-dpng', '-r500');
% % % % % %saveas(gcf,'VORY_ISO_SURFACE_1_5_SIAC.png')
% % % % % 
% % % % % %%
% % % % % %close all;
% % % % % figure(7)
% % % % % teta = linspace(-pi,pi,10);
% % % % % shift = 0.46666667;
% % % % % scale = 0.05
% % % % % startx = shift + (sin(teta))*scale;
% % % % % starty = zeros(size(teta));
% % % % % startz = shift + (cos(teta))*scale;
% % % % % % startx(end+1) = 0.5;
% % % % % % starty(end+1) = 0.0;
% % % % % % startz(end+1) = 0.5;
% % % % % 
% % % % % streamline(xdataV,ydataV,zdataV,udataV,vdataV,wdataV,startx,starty,startz);
% % % % % %streamline(xdataV,ydataV,zdataV,udataV,vdataV,wdataV,[0.25,0.5,0.75],[0,0,0],[0.25,0.5,0.75] );
% % % % % plotBoundingBox(); hold on;
% % % % % axis equal;
% % % % % xlabel('x'); ylabel('y'); zlabel('z');
% % % % % 
% % % % % 
% % % % % 
% % % % % %%
% % % % % %close all;
% % % % % figure(8)
% % % % % %h = streamribbon(x,y,z,u,v,w,sx,sy,sz);
% % % % % %h = streamtube(xdataV,ydataV,zdataV,udataV,vdataV,wdataV,startx,starty,startz);
% % % % % h = streamtube (stream3(xdataV,ydataV,zdataV,udataV,vdataV,wdataV,startx,starty,startz),0.005);hold on;
% % % % % %h.FaceColor = 'red';
% % % % % %h.EdgeColor = 'none';
% % % % % %plotBoundingBox(); hold on;
% % % % % set(h,'EdgeColor','none','FaceColor','r')%,...
% % % % %    %'AmbientStrength',.5)
% % % % % %view([9 8]);
% % % % % view([6 16]);
% % % % % axis equal;
% % % % % grid off;
% % % % % axis off;
% % % % % box off;
% % % % % camlight;
% % % % % lighting gouraud;
% % % % % %shading interp;
% % % % % %camlight('left');
% % % % % %print(strcat('images/',filename,'_Stream' ), '-dpng', '-r500');
% % % % % 
% % % % % 
% % % % % %%
% % % % % 
% % % % % figure(9)
% % % % % %h = streamribbon(x,y,z,u,v,w,sx,sy,sz);
% % % % % %h = streamtube(xdataV,ydataV,zdataV,udataV,vdataV,wdataV,startx,starty,startz);
% % % % % hp = streamtube (stream3(xdataV,ydataV,zdataV,updataV,vpdataV,wpdataV,startx,starty,startz),0.005);hold on;
% % % % % %h.FaceColor = 'red';
% % % % % %h.EdgeColor = 'none';
% % % % % %plotBoundingBox(); hold on;
% % % % % set(hp,'EdgeColor','none','FaceColor','r')%,...
% % % % %    %'AmbientStrength',.5)
% % % % % %view([9 8]);
% % % % % view([6 16]);
% % % % % 
% % % % % axis equal;
% % % % % grid off;
% % % % % box off;
% % % % % light;
% % % % % lighting gouraud;
% % % % % %shading interp;
% % % % % %camlight('left');
% % % % % hold on;
% % % % % h = streamtube (stream3(xdataV,ydataV,zdataV,updataV,vpdataV,wpdataV,0.46666667,0.0,0.4666667),0.15);hold on;
% % % % % %h.FaceColor = 'red';
% % % % % %h.EdgeColor = 'none';
% % % % % %plotBoundingBox(); hold on;
% % % % % set(h,'EdgeColor','none','FaceColor','black')%,...
% % % % %    %'AmbientStrength',.5)
% % % % % %view([9 8]);
% % % % % view([6 16]);
% % % % % axis equal;
% % % % % grid off;
% % % % % box off;
% % % % % axis off;
% % % % % alpha(h,0.25)
% % % % % light;
% % % % % lighting gouraud;
% % % % % %print(strcat('images/',filename,'_Stream2' ), '-dpng', '-r500');
% % % % % 
% % % % % 

