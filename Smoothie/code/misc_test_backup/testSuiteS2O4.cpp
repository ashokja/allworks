#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC1D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh1D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

 
int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM1D = new HandleNekMesh1D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM1D->LoadMesh(var[0]);
	string fname = vSession->GetSessionName();
	Array<OneD,NekDouble> direction(3,0.0);

	int tNquadPts = HNM1D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM1D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM1D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		default:
			cout << "opps did not plan for this" << endl;
			break;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);

	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*3.14*xc0[i]);
	}
	
	HNM1D->m_expansions[0]->FwdTrans(fce,HNM1D->m_expansions[0]->UpdateCoeffs());
	HNM1D->m_expansions[0]->BwdTrans( HNM1D->m_expansions[0]->GetCoeffs(),
										HNM1D->m_expansions[0]->UpdatePhys());
	ece = HNM1D->m_expansions[0]->GetPhys();
		

	SmoothieSIAC1D sm(SIACUtilities::eSYM_2kp1_1SIDED_4kp1, HNM1D, atoi(argv[2]), atof(argv[3]) ); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();

	for (int i =0; i <tNquadPts;i++)
	{
		sm.EvaluateAt(xc0[i],xc1[i],xc2[i],sce[i],valY,valZ);

		cout << "\tf:\t" << abs(fce[i]-ece[i]) ;
		cout << "\ts:\t:" << abs(fce[i] -sce[i]);
		cout << "\ts:\t:" << sce[i];
		cout << endl;
	}

	NektarBaseClass k;
	k.printNekArray(xc0,0);
	k.printNekArray(fce,0);
	k.printNekArray(ece,0);
	k.printNekArray(sce,0);
	cout << fname+"_"+argv[2]+"_xc0_OneSided2kp1.txt"<< endl;
	k.writeNekArray(xc0,fname+"_"+argv[2]+"_xc0_OneSided4kp1.txt");
	k.writeNekArray(fce,fname+"_"+argv[2]+"_fce_OneSided4kp1.txt");
	k.writeNekArray(ece,fname+"_"+argv[2]+"_ece_OneSided4kp1.txt");
	k.writeNekArray(sce,fname+"_"+argv[2]+"_sce_OneSided4kp1.txt");

	return 0;
}

void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
