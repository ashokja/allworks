#include "NonSymmetricSIAC.h"
#include <Eigen/Dense>

NonSymmetricSIAC::NonSymmetricSIAC(int order) :
							m_coeffCalculated(false),
							m_cenBSpline(order), m_splines(order-1)
{
	m_nthDer=0;
	// initialize number of coeffecients array using order.
	// If only order is specified. It is by default BASIC_SIAC_4kp1.
	m_filterType = SymFilterType::BASIC_SIAC_2kp1;

	switch (m_filterType)
	{
		case (SymFilterType::BASIC_SIAC_2kp1):
			m_order = order;
			m_nBSpl = 2*(m_order-1)+1;
			m_coeffs = Array<OneD,NekDouble>((2*(order-1)+1),0.0);
			m_genBSplinePtr = std::make_shared<GeneralBSplines> (order);
			break;
		case (SymFilterType::SIAC_GIVEN_R_SPLINES):
			m_order = order;
			m_nBSpl = 2*(m_order-1)+1;
			m_coeffs = Array<OneD,NekDouble>((2*(order-1)+1),0.0);
			m_genBSplinePtr = std::make_shared<GeneralBSplines> (order);
			break;
		default:
			assert(false && "something is wrong" );
			cout << "Should not come here assert here actually." << endl;
			break;	
	}
	//EvaluateCoefficients();
}

NonSymmetricSIAC::NonSymmetricSIAC(int order, SymFilterType filterType, int nthDer):
								m_cenBSpline(order), m_splines(order-1)
{
	m_nthDer=nthDer;
	// initialize number of coeffecients array using order.
	// If only order is specified. It is by default BASIC_SIAC_4kp1.
	m_filterType = SymFilterType::BASIC_DER_SIAC_2kp1;

	switch (m_filterType)
	{
		case (SymFilterType::BASIC_DER_SIAC_2kp1):
			m_order = order;
			m_nBSpl = 2*(m_order-1)+1;
			m_coeffs = Array<OneD,NekDouble>((2*(order-1)+1),0.0);
			m_genBSplinePtr = std::make_shared<GeneralBSplines> (order-nthDer);
			break;
		default:
			assert(false && "something is wrong" );
			cout << "Should not come here assert here actually." << endl;
			break;	
	}
}

bool NonSymmetricSIAC::v_EvaluateCoefficients_GivenNumSplines(const Array<OneD,NekDouble> &knotVec, const NekDouble kernelShift)
{
//	cout << "Into Evalute coefficietns" << endl; //???
    assert(false&&"Does it come here.");
	// Changing knot vector to knot Matrix.
	int t_nBSpl = knotVec.num_elements()-m_order;
	//m_coeffs = Array<OneD,NekDouble> (t_nBSpl);
	//m_nBSpl = t_nBSpl;
	vector<vector<NekDouble>> kv_mat;
	for (int nb=0; nb< t_nBSpl;nb++)
	{
		vector<NekDouble> kv(m_order+1);
		for(int i =0; i < m_order+1;i++)
		{
			kv[i] = knotVec[nb+i];
		}
		kv_mat.push_back(kv);
	}


	switch (m_filterType)
	{
		case(SymFilterType::BASIC_SIAC_2kp1):
		case(SymFilterType::BASIC_DER_SIAC_2kp1):
			CalCoeffForWideSymKernel( m_order-1,knotVec, m_coeffs);
			m_splines.Initialize( m_order-1, t_nBSpl,m_coeffs);
			//printNekArray(m_coeffs);
			CalCoeffForKnotMatrixVec_Hanieh(m_order-1,kv_mat,m_coeffs);
			//cout << "calculating coeff using knot matrix: "<< endl;
			//printNekArray(m_coeffs);
			break;
		default:
			cout << "Assert or add code for all the other cases Sig:ajiso876af" << endl;
	}
	return true;
}

bool NonSymmetricSIAC::v_EvaluateCoefficients(const Array<OneD,NekDouble> &knotVec, const NekDouble kernelShift)
{
//	cout << "Into Evalute coefficietns" << endl; //???

	// Changing knot vector to knot Matrix.
	        vector<vector<NekDouble>> kv_mat;
            int order,nBSpl;
	switch (m_filterType)
	{
		case(SymFilterType::BASIC_SIAC_2kp1):
            order = m_order;
            //nBSpl = 2*(m_order-1)+1;
            nBSpl = knotVec.num_elements()-order;
            //order = m_order -m_nthDer ;
            //nBSpl = 2*(m_order-1)+1 + m_nthDer;
            m_coeffs = Array<OneD,NekDouble>(nBSpl,0.0);
	        for (int nb=0; nb< nBSpl;nb++)
      	    {
		        vector<NekDouble> kv(order+1);
		        for(int i =0; i < order+1;i++)
		        {
			        kv[i] = knotVec[nb+i];
		        }
	        	kv_mat.push_back(kv);
	        }
			CalCoeffForKnotMatrixVec_Hanieh( m_order-1, kv_mat, m_coeffs);
			break;
		case(SymFilterType::BASIC_DER_SIAC_2kp1):
            order = m_order;
            //nBSpl = 2*(m_order-1)+1;
            nBSpl = knotVec.num_elements()-order;
            //order = m_order -m_nthDer ;
            //nBSpl = 2*(m_order-1)+1 + m_nthDer;
            m_coeffs = Array<OneD,NekDouble>(nBSpl,0.0);
	        for (int nb=0; nb< nBSpl;nb++)
      	    {
		        vector<NekDouble> kv(order+1);
		        for(int i =0; i < order+1;i++)
		        {
			        kv[i] = knotVec[nb+i];
		        }
	        	kv_mat.push_back(kv);
	        }
			CalCoeffForKnotMatrixVec_Hanieh( order-1, kv_mat, m_coeffs);
            break;
		default:
			cout << "Assert or add code for all the other cases Sig:ajiso876af" << endl;
	}
	return true;
}

bool NonSymmetricSIAC::v_EvaluateCoefficients(const NekDouble kernelShift)
{
//	cout << "Into Evalute coefficietns" << endl; //???
    assert(false&&"Does it come here.");

	switch (m_filterType)
	{
		case(SymFilterType::BASIC_SIAC_2kp1):
		case(SymFilterType::BASIC_DER_SIAC_2kp1):
			CalCoeffForWideSymKernel( m_order-1, m_nBSpl, m_coeffs);
			m_splines.Initialize( m_order-1, m_nBSpl,m_coeffs);
			break;
		default:
			cout << "Assert or add code for all the other cases Sig:ajiso876af" << endl;
	}
	return true;
}

bool NonSymmetricSIAC::EvaluateFilterUsingSplines( const Array<OneD,NekDouble> &x_pos, Array<OneD,NekDouble> &t_vals,
const NekDouble meshScaling, const NekDouble meshShift, const bool evalCoeff) 
//bool NonSymmetricSIAC::v_EvaluateFilter( const Array<OneD,NekDouble> &x_pos, Array<OneD,NekDouble> &t_vals,
//				 const NekDouble meshScaling, const NekDouble meshShift, const bool evalCoeff) 
{
	// Always check if coeffecients have already been calculated.
		//For symmetric case. They are always calculated during initilization.
    assert(false&&"Does it come here.");
	int nq;
	nq = x_pos.num_elements();
	Vmath::Fill(nq,0.0,t_vals,1);
	Array<OneD,NekDouble> t_valTemp(nq);
	for(int i = 0; i< m_nBSpl; i++)
	{
		//	NekDouble it = -m_R*0.5 + i;
		NekDouble it = -(m_nBSpl-1.0)/2.0+i;
		m_cenBSpline.EvaluateBSplines( x_pos,t_valTemp, ((NekDouble) it)*meshScaling , meshScaling );
		Vmath::Smul(nq, m_coeffs[m_nBSpl-1-i]/std::pow(meshScaling,1+m_nthDer) , t_valTemp,1,t_valTemp,1);
		Vmath::Vadd(nq, t_valTemp,1 ,t_vals,1,t_vals,1);
	}
	return true;
}

bool NonSymmetricSIAC::v_EvaluateFilter( const Array<OneD,NekDouble> &x_pos, Array<OneD,NekDouble> &t_vals,
	const NekDouble meshScaling, const NekDouble meshShift, const bool evalCoeff) 
//bool NonSymmetricSIAC::EvaluateFilterUsingSplines( const Array<OneD,NekDouble> &x_pos, Array<OneD,NekDouble> &t_vals,
//				const NekDouble meshScaling, const NekDouble meshShift, const bool evalCoeff) 
{
    assert(false&&"Does it come here.");
	vector<NekDouble> m_tempVector(10); // Need to fix this.
	vector<NekDouble> v_x_pos(x_pos.num_elements());
	memcpy( &v_x_pos[0], &x_pos[0], x_pos.num_elements()*sizeof(NekDouble));
	vector<NekDouble> v_t_vals(t_vals.num_elements());
// call the funtion here.
	m_splines.EvaluateUArr( v_x_pos, v_t_vals, m_tempVector, meshScaling,0.0, m_nthDer);
	memcpy( &t_vals[0], &v_t_vals[0], t_vals.num_elements()*sizeof(NekDouble));
	return true;
}


bool NonSymmetricSIAC::v_EvaluateFilterWknots( const Array<OneD,NekDouble> &x_pos, Array<OneD,NekDouble> &t_vals,
					const Array<OneD,NekDouble> &knotVec,const NekDouble meshScaling, const NekDouble meshShift,
					const bool evalCoeff) 
{
	int nq = x_pos.num_elements();
	Vmath::Fill(nq,0.0,t_vals,1);
	Array<OneD,NekDouble> t_valTemp(nq);	

	// Evaluate coefficients
    // To speedup up use the evalCoeff bool. (Currently not optimized))
	EvaluateCoefficients(knotVec, meshShift);
    
	vector<vector<NekDouble>> kv_mat;
    int deg,order, nBSpl;
    Array<OneD,NekDouble> coeffs,new_coeffs; 
	switch (m_filterType)
	{
		case (SymFilterType::BASIC_SIAC_2kp1):
            nBSpl = knotVec.num_elements()-m_order+m_nthDer;
	        for (int nb=0; nb< nBSpl;nb++)
	        {
		        vector<NekDouble> kv(m_order+1);
		        for(int i =0; i < m_order+1;i++)
		        {
			        kv[i] = knotVec[nb+i];
		        }
		        kv_mat.push_back(kv);
	        }

	        // Evaluate filter
            for(int i=0; i< nBSpl; i++)
            {   
     	        m_genBSplinePtr->EvaluateBSplines( x_pos, kv_mat[i], 0,t_valTemp,
                             0.0, meshScaling);
                Vmath::Smul(nq, m_coeffs[i]/meshScaling , t_valTemp,1,t_valTemp,1);
                Vmath::Vadd(nq, t_valTemp,1 ,t_vals,1,t_vals,1);
            }
            break;
		case (SymFilterType::BASIC_DER_SIAC_2kp1):
            //order = m_order ;
            //nBSpl = 2*(m_order-1)+1;
            order = m_order ;
            //nBSpl = 2*(m_order-1)+1 + m_nthDer;
            nBSpl = knotVec.num_elements()-m_order+m_nthDer;
            deg = order-1;
            coeffs = m_coeffs;
            for(int d =0;d<m_nthDer;d++)
            {
                new_coeffs = Array<OneD,NekDouble>(coeffs.num_elements()+1,0.0);
                for(int c=0; c<coeffs.num_elements();c++)
                {   // The negative sign is flipped to account for convolution of the L-SIAC filter.
                    new_coeffs[c]+= -1.0*deg*coeffs[c]/(knotVec[c+deg]-knotVec[c]); 
                    new_coeffs[c+1]+= deg*coeffs[c]/(knotVec[c+deg+1]-knotVec[c+1]); 
                }
                deg--;order--;
                coeffs = new_coeffs;
            }
            //m_coeffs = new_coeffs;
	        for (int nb=0; nb< nBSpl;nb++)
	        {
		        vector<NekDouble> kv(order+1);
		        for(int i =0; i < order+1;i++)
		        {
			        kv[i] = knotVec[nb+i];
		        }
		        kv_mat.push_back(kv);
	        }
            m_coeffs = new_coeffs;
	        // Evaluate filter
            for(int i=0; i< nBSpl; i++)
            {   
     	        m_genBSplinePtr->EvaluateBSplines( x_pos, kv_mat[i], 0,t_valTemp,
                             0.0, meshScaling);
                // Need to only change this line for derivative.
                    Vmath::Smul(nq, new_coeffs[i]/meshScaling , t_valTemp,1,t_valTemp,1);
                Vmath::Vadd(nq, t_valTemp,1 ,t_vals,1,t_vals,1);
            }
            break;
        default:
            assert(false&&"This should not happen");
    }   

	return true;
}

bool NonSymmetricSIAC::v_EvaluateFilterWknots_GivenNumSplines( const Array<OneD,NekDouble> &x_pos, Array<OneD,NekDouble> &t_vals,
					const Array<OneD,NekDouble> &knotVec,const NekDouble meshScaling, const NekDouble meshShift,
					const bool evalCoeff) 
{
    assert(false&&"Does it come here.");
	int nq = x_pos.num_elements();
	Vmath::Fill(nq,0.0,t_vals,1);
	Array<OneD,NekDouble> t_valTemp(nq);	

	// need to create knot vector or matrix
	// Changing knot vector to knot Matrix.
	int t_nBSpl = knotVec.num_elements()-m_order;
	m_coeffs = Array<OneD,NekDouble> (t_nBSpl);
	vector<vector<NekDouble>> kv_mat;
	for (int nb=0; nb< t_nBSpl;nb++)
	{
		vector<NekDouble> kv(m_order+1);
		for(int i =0; i < m_order+1;i++)
		{
			kv[i] = knotVec[nb+i];
		}
		kv_mat.push_back(kv);
	}
	// Evaluate coefficients
	EvaluateCoefficients_GivenNumSplines(knotVec, meshShift);

	// Evaluate filter
    for(int i=0; i< t_nBSpl; i++)
    {   
     	m_genBSplinePtr->EvaluateBSplines( x_pos, kv_mat[i], 0,t_valTemp,
                        0.0, meshScaling);
        Vmath::Smul(nq, m_coeffs[i]/meshScaling , t_valTemp,1,t_valTemp,1);
        Vmath::Vadd(nq, t_valTemp,1 ,t_vals,1,t_vals,1);
     }   

	return true;
}




/// This function return all break point of filter scaled appropriately.
// This function should not be calculating break points everytime.
// They should have been saved at initilization (That poses other problems :( )
bool NonSymmetricSIAC::v_GetBreakPts( const NekDouble scaling, vector<NekDouble> &valT,
							const NekDouble shift)
{
	NekDouble tmin, tmax;
	int t_nBSpl	=0;
	switch (m_filterType)
	{
		case(SymFilterType::BASIC_SIAC_2kp1):
		case(SymFilterType::BASIC_DER_SIAC_2kp1):
		case(SymFilterType::CUSTOM_SIAC):
		case(SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC):
		case(SymFilterType::CUSTOM_Derivative_SIAC):
		case(SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC_WOUT_DIVDIFF):
			tmin = -( (m_order)/2.0+(m_nBSpl-1.0)/2.0)*scaling;
			tmax = ( (m_order)/2.0+(m_nBSpl-1.0)/2.0)*scaling;
			break;
		case (SymFilterType::SIAC_GIVEN_R_SPLINES):
			t_nBSpl = 2*(m_order-1)+1;
			tmin = -( (m_order)/2.0+(t_nBSpl-1.0)/2.0)*scaling;
			tmax = ( (m_order)/2.0+(t_nBSpl-1.0)/2.0)*scaling;
			break;
		default:
			cout << "This case not coded yet. GetBreakPts" << endl;
	}

	valT.clear();
	for (NekDouble t = tmin; t <=tmax ; t += scaling)
	{
		valT.push_back(t);
	} 
	return true;
}

bool NonSymmetricSIAC::v_GetBreakPts_SPodd( const NekDouble scaling, vector<NekDouble> &valT,
							const NekDouble shift)
{
	NekDouble tmin, tmax;
	int t_nBSpl	=0;
	switch (m_filterType)
	{
		case(SymFilterType::BASIC_SIAC_2kp1):
		case(SymFilterType::BASIC_DER_SIAC_2kp1):
		case(SymFilterType::CUSTOM_SIAC):
		case(SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC):
		case(SymFilterType::CUSTOM_Derivative_SIAC):
		case(SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC_WOUT_DIVDIFF):
            if (m_order%2 ==0)
            {
			    tmin = -( (m_order)/2.0+(m_nBSpl-1.0)/2.0)*scaling;
			    tmax = ( (m_order)/2.0+(m_nBSpl-1.0)/2.0)*scaling;
            }else
            {
                //In case of odd order we shift the knots to match up mesh verts.
			    tmin = -( (m_order)/2.0+(m_nBSpl-1.0)/2.0+0.5)*scaling;
			    tmax = ( (m_order)/2.0+(m_nBSpl-1.0)/2.0+0.5)*scaling;
            }
			break;
		case (SymFilterType::SIAC_GIVEN_R_SPLINES):
			t_nBSpl = 2*(m_order-1)+1;
			    tmin = -( (m_order)/2.0+(t_nBSpl-1.0)/2.0)*scaling;
			    tmax = ( (m_order)/2.0+(t_nBSpl-1.0)/2.0)*scaling;
			break;
		default:
			cout << "This case not coded yet. GetBreakPts" << endl;
	}

	valT.clear();
	for (NekDouble t = tmin; t <=tmax ; t += scaling)
	{
		valT.push_back(t);
	} 
	return true;
}


bool NonSymmetricSIAC::v_GetFilterRange( NekDouble scaling, NekDouble &tmin, NekDouble &tmax,
					const NekDouble shift )
{
	int t_nBSpl	=0;
	switch (m_filterType)
	{
		case(SymFilterType::BASIC_SIAC_2kp1):
		case(SymFilterType::BASIC_DER_SIAC_2kp1):
		case(SymFilterType::CUSTOM_SIAC):
		case(SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC):
		case(SymFilterType::CUSTOM_Derivative_SIAC):
		case(SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC_WOUT_DIVDIFF):
			tmin = -( (m_order)/2.0+(m_nBSpl-1.0)/2.0)*scaling;
			tmax = ( (m_order)/2.0+(m_nBSpl-1.0)/2.0)*scaling;
			break;
		case (SymFilterType::SIAC_GIVEN_R_SPLINES):
			t_nBSpl = 2*(m_order-1)+1;
			tmin = -( (m_order)/2.0+(t_nBSpl-1.0)/2.0)*scaling;
			tmax = ( (m_order)/2.0+(t_nBSpl-1.0)/2.0)*scaling;
			break;
		default:
			cout << "This case not coded yet. GetFilterRange" << endl;
	}
	return true;
}

