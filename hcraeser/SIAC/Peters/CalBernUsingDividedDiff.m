function [v_list] = CalBernUsingDividedDiff(degree,x_list,tknots)

    switch (nargin)
        case 2
            tknots= -(degree+1)/2:1: (degree+1)/2;
    end
            

    h = @monomial;
    

    %tknots=ones(1,6)*2;
    %dividedDiff(1,3,tknots,0,h)
    i =1;
    j = max(size(tknots));
    count =1;

    v_list = zeros(size(x_list));
    for xx = x_list
        x_list(count) = xx;
        v_list(count) = (tknots(j)-tknots(i))*dividedDiff(i,j,tknots,xx,h,degree);
%         dividedDiff(1,4,tknots,xx,h,degree)
%         dividedDiff(2,4,tknots,xx,h,degree)
%         dividedDiff(1,3,tknots,xx,h,degree)
        count=count+1;
    end
    
    function result = monomial(t,x,der,D)
        y = max(t-x,0);
        if (y ~=0)
%            result = prod(D-der+1:D)*(y)^(D-der);
            result = y^(D-der)*prod(1:D)/prod(1:D-der);
        else
            result =0;
        end
    end

end