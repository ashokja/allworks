function [y dy]=myfunction(x)
%Example function to try out Newton's Method
%
    n=length(x);
    y=zeros(size(x)); %Not necessary for a small vector
    dy=zeros(n,n); %Not necessary for a small matrix
    y(1)=2*x(1);
    y(2)=-2*x(2);
    dy(1,1)=2; dy(1,2)=0;
    dy(2,1)=0; dy(2,2)=-2;
end
