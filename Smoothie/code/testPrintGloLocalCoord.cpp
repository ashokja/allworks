#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include <StdRegions/StdHexExp.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"
#include <boost/timer.hpp>
#include <iomanip>


using namespace SIACUtilities;
//using namespace Eigen;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;
						
int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		return 0;
	}

	argc = 2;
	
	double startTime = clock();
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();

	HNM3D->LoadMesh(var[0]);
	int tNqyadPts = HNM3D->m_expansions[0]->GetTotPoints();
	MultiRegions::ExpListSharedPtr expList = HNM3D->m_expansions[0];
	int expSize = expList->GetExpSize();
	int e=-1;
	NekDouble elX,elY,elZ;
	Array<OneD,NekDouble> Gcoord(3,0.0),Lcoord(3,0.0);
	cout << "GlobalCoord X:\t" ;
	cin >> Gcoord[0] ;
	cout << endl<< "GlobalCoord Y:\t" ;
	cin >> Gcoord[1] ;
	cout << endl<< "GlobalCoord Z:\t" ;
	cin >> Gcoord[2] ;
	cout << endl;

	for (int i =0; i < 100; i++)
	{
		cout << "Input ID (below "<< expSize<<"):\t" ;
		cin >> e ;
		// input element ID;
		SpatialDomains::GeometrySharedPtr geom = expList->GetExp(e)->GetGeom();
		int numPts = geom->GetNumVerts();
		for (int v=0; v<numPts;v++)
		{
			geom->GetVertex(v)->GetCoords(elX,elY,elZ);
			cout << elX << "\t" <<elY <<"\t"<<elZ << endl;
		}
		cout << "localCoordinates:\t" << endl;
		geom->GetLocCoords(Gcoord,Lcoord);
		cout << Lcoord[0] << "\t" <<Lcoord[1] << "\t" << Lcoord[2] << "\t";
		cout << geom->ContainsPoint(Gcoord,TOLERENCE) << endl;
	}

	return 0;
}

