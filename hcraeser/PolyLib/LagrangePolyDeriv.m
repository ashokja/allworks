
function y = LagrangePolyDeriv(x,pt,npts,xpts)

y = zeros(size(x));

for j=1:npts,
  if j~=pt,
    h = ones(size(x));
    for i=1:npts,
      if(i~=pt),
        if(i~=j)
          h = h.*(x-xpts(i));
        end
        h = h./(xpts(pt)-xpts(i));
	  end
    end
   	y = y + h;
  end
end