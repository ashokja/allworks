clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [10];

poly =2;
order = poly;
count =1;
for r = rates
    
    xfname = strcat('datatxt/','quad_simple_',int2str(r),'_',int2str(order),'_xc0_OneSided2kp1.txt' );
    yfname = strcat('datatxt/','quad_simple_',int2str(r),'_',int2str(order),'_xc1_OneSided2kp1.txt' );
    fufname = strcat('datatxt/','quad_simple_',int2str(r),'_',int2str(order),'_fceu_OneSided2kp1.txt' );
    eufname = strcat('datatxt/','quad_simple_',int2str(r),'_',int2str(order),'_eceu_OneSided2kp1.txt' );
    sufname = strcat('datatxt/','quad_simple_',int2str(r),'_',int2str(order),'_sceu_OneSided2kp1.txt' );

    fvfname = strcat('datatxt/','quad_simple_',int2str(r),'_',int2str(order),'_fcev_OneSided2kp1.txt' );
    evfname = strcat('datatxt/','quad_simple_',int2str(r),'_',int2str(order),'_ecev_OneSided2kp1.txt' );
    svfname = strcat('datatxt/','quad_simple_',int2str(r),'_',int2str(order),'_scev_OneSided2kp1.txt' );
    
    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');
    fuval = textread(fufname, '%f\t');
    puval = textread(eufname, '%f\t');
    suval = textread(sufname, '%f\t'); 
    
    fvval = textread(fvfname, '%f\t');
    pvval = textread(evfname, '%f\t');
    svval = textread(svfname, '%f\t'); 
    
    
    s = sqrt(max(size(xpos)));
    xVpos = reshape(xpos,s,s);
    yVpos = reshape(ypos,s,s);
    fVval = reshape(fuval,s,s);
    pVval = reshape(puval,s,s);
    sVval = reshape(suval,s,s);    
    
    %[X,Y] = meshgrid(0:0.05:1);
    %Fmg = interp2(xVpos,yVpos,fVval,X,Y);
    %surf(X,Y,Fmg);
    
    
    figure(1)
   % surf(xVpos, yVpos, log10(abs(fVval-pVval))); hold on;
   % surf(xVpos, yVpos, log10(abs(fVval-sVval)) ); hold off;
    plot3(xpos,ypos, log10(abs(fuval-puval)),xpos,ypos,log10(abs(fuval-suval)));
    
    figure(2)
    plot3(xpos,ypos, log10(abs(fuval-suval)),'DisplayName',strcat('No of ele:',int2str(r)));
    %surf(xVpos,yVpos, log10(abs(fVval-sVval)),'DisplayName',strcat('No of ele:',int2str(r)));
    
    hold on;

    figure(3)
    plot3(xpos,ypos, puval);
    %surf(xVpos,yVpos, pVval); hold on;
     %   surf(xVpos,yVpos, pVval); hold off;

    p_avg_err(count) = sum(abs(fuval-puval))/max(size(xpos));
    s_avg_err(count) = sum(abs(fuval-suval))/max(size(xpos));
    
    p_max_err(count) = max(abs(fuval-puval));
    s_max_err(count) = max(abs(fuval-suval));
    count = count+1;
end

figure(1),
legend('show');
figure(2),
legend('show');
title(strcat('poly-',int2str(poly),' order-',int2str(order)));
xlabel('domain');
ylabel('Cos(2*pi*x)');
%saveas(2,strcat('S2O2poly_',int2str(poly),'_order_',int2str(order)),'jpg');

% figure(3)
% plot(log10(rates),log10(p_avg_err),log10(rates),log10(s_avg_err));
% figure(4)
% plot(log10(rates),log10(p_max_err),log10(rates),log10(s_max_err));