clear;
%plot a curve and it coeff for itself and higher degree.
% can pick coeff
coeff = [1.0;-2.5;1.0;2.0;1.0;0];
% do a degree elevation upto r times.
r = 10;
coeff_old = coeff;
for i = 1:r
    coeff = degElevation(coeff);
end
coeff_new = coeff;
D_int = 0:.01:1;
subdivo = max(size(coeff_old))-1;
subdivn = max(size(coeff_new))-1;
yminlim = -1.5;
ymaxlim = 1.5;

figure (1),
subplot(4,2,1),plot(D_int,funcEvalBerns(coeff_old,D_int),0:1/subdivo:1,coeff_old,'-o');
ylim([yminlim ymaxlim])
title('Polynomial of degree 6')
grid on;
subplot(4,2,2),plot(D_int,funcEvalBerns(coeff_new,D_int),0:1/subdivn:1,coeff_new,'-o');
ylim([yminlim ymaxlim])
title('Polynomial of degree 16')
grid on;

%figure(2)
 %subplot(4,2,5),plot(D_int,funcEvalBerns(coeff_old,D_int),0:1/subdivo:1,coeff_old,'-o',0:1/subdivn:1,coeff_new,'-o');
 %ylim([yminlim ymaxlim])
 %grid on;

% make the convex hull positive.
coeff_oldp = coeff_old.*(coeff_old>0.0);
coeff_newp = coeff_new.*(coeff_new>0.0);

%figure (3),
subplot(4,2,3),plot(D_int,funcEvalBerns(coeff_oldp,D_int),0:1/subdivo:1,coeff_oldp,'-o');
ylim([yminlim ymaxlim])
title('Negative coeff are pushed to zero.')
grid on;
subplot(4,2,4),plot(D_int,funcEvalBerns(coeff_newp,D_int),0:1/subdivn:1,coeff_newp,'-o');
ylim([yminlim ymaxlim])
title('Negative coeff are pushed to zero.')
grid on;
%figure(4)
 %subplot(4,2,6),plot(D_int,funcEvalBerns(coeff_oldp,D_int),0:1/subdivo:1,coeff_oldp,0:1/subdivn:1,coeff_newp);
 %ylim([yminlim ymaxlim])
 %grid on;
% plot them together.

subplot(4,2,5),plot(D_int,funcEvalBerns(coeff_old,D_int),D_int,funcEvalBerns(coeff_oldp,D_int));%,0:1/subdivo:1,coeff_old);
ylim([yminlim ymaxlim])
title('comparision between new and old polynomail')
grid on;
subplot(4,2,6),plot(D_int,funcEvalBerns(coeff_new,D_int),D_int,funcEvalBerns(coeff_newp,D_int));%,0:1/subdivn:1,coeff_new);
title('comparision between new and old polynomial')
ylim([yminlim ymaxlim])
grid on;

subplot(4,2,7),plot(D_int,abs(funcEvalBerns(coeff_old,D_int)-funcEvalBerns(coeff_oldp,D_int)));%,0:1/subdivo:1,coeff_old);
ylim([yminlim ymaxlim])
title('Error for polynomial degree 6')
grid on;
subplot(4,2,8),plot(D_int,abs(funcEvalBerns(coeff_new,D_int)-funcEvalBerns(coeff_newp,D_int)));%,0:1/subdivn:1,coeff_new);
ylim([yminlim ymaxlim])
title('Error for polynomial degree 16')
grid on;