% P2
% quadrature cal error.
% cart siac filter.
% hex union jack.
% DG

clear;
% Test errors.
Deg=2;
ord1 = 15;
ord2 = 29;
ord3 = 57;
ord4 = 113;

L2B_ord1 = 6.75e-4;
L2B_ord2 = 8.20e-5;
L2B_ord3 = 1.02e-5;
L2B_ord4 = 1.27e-6;

LinfB_ord1 = 5.49e-3;
LinfB_ord2 = 7.08e-4;
LinfB_ord3 = 8.92e-5;
LinfB_ord4 = 1.12e-5;

L2_ord1 = 1.62e-3;
L2_ord2 = 2.03e-5;
L2_ord3 = 3.43e-7;
L2_ord4 = 5.94e-9;

Linf_ord1 = 4.98e-3;
Linf_ord2 = 2.89e-5;
Linf_ord3 = 4.88e-7;
Linf_ord4 = 8.44e-9;


order_L2(1) = 0;
order_L2(2) = log(L2_ord2/L2_ord1)/log(ord1/ord2);
order_L2(3) = log(L2_ord3/L2_ord2)/log(ord2/ord3);
order_L2(4) = log(L2_ord4/L2_ord3)/log(ord3/ord4);

order_Linf(1) = 0;
order_Linf(2) = log(Linf_ord2/Linf_ord1)/log(ord1/ord2);
order_Linf(3) = log(Linf_ord3/Linf_ord2)/log(ord2/ord3);
order_Linf(4) = log(Linf_ord4/Linf_ord3)/log(ord3/ord4);

order_L2
order_Linf

% Hex(1) filter.

L2BH1_ord1 = 6.75e-4;
L2BH1_ord2 = 8.20e-5;
L2BH1_ord3 = 1.02e-5;
L2BH1_ord4 = 1.27e-6;

LinfBH1_ord1 = 5.49e-3;
LinfBH1_ord2 = 7.08e-4;
LinfBH1_ord3 = 8.92e-5;
LinfBH1_ord4 = 1.12e-5;

L2H1_ord1 = 3.12e-2;
L2H1_ord2 = 2.07e-5;
L2H1_ord3 = 3.36e-7;
L2H1_ord4 = 0;

LinfH1_ord1 = 1.81e-1;
LinfH1_ord2 = 2.95e-5;
LinfH1_ord3 = 4.79e-7;
LinfH1_ord4 = 0;


orderH1_L2(1) = 0;
orderH1_L2(2) = log(L2H1_ord2/L2H1_ord1)/log(ord1/ord2);
orderH1_L2(3) = log(L2H1_ord3/L2H1_ord2)/log(ord2/ord3);
orderH1_L2(4) = log(L2H1_ord4/L2H1_ord3)/log(ord3/ord4);

orderH1_Linf(1) = 0;
orderH1_Linf(2) = log(LinfH1_ord2/LinfH1_ord1)/log(ord1/ord2);
orderH1_Linf(3) = log(LinfH1_ord3/LinfH1_ord2)/log(ord2/ord3);
orderH1_Linf(4) = log(LinfH1_ord4/LinfH1_ord3)/log(ord3/ord4);

orderH1_L2
orderH1_Linf

% Hex2 filter.

L2BH2_ord1 = 6.75e-4;
L2BH2_ord2 = 8.20e-5;
L2BH2_ord3 = 1.02e-5;
L2BH2_ord4 = 1.27e-6;

LinfBH2_ord1 = 5.49e-3;
LinfBH2_ord2 = 7.08e-4;
LinfBH2_ord3 = 8.92e-5;
LinfBH2_ord4 = 1.12e-5;

L2H2_ord1 = 1.59e-4;
L2H2_ord2 = 1.71e-5;
L2H2_ord3 = 2.07e-6;
L2H2_ord4 = 2.56e-7;

LinfH2_ord1 = 4.41e-4;
LinfH2_ord2 = 4.74e-5;
LinfH2_ord3 = 5.73e-6;
LinfH2_ord4 = 7.12e-7;


orderH2_L2(1) = 0;
orderH2_L2(2) = log(L2H2_ord2/L2H2_ord1)/log(ord1/ord2);
orderH2_L2(3) = log(L2H2_ord3/L2H2_ord2)/log(ord2/ord3);
orderH2_L2(4) = log(L2H2_ord4/L2H2_ord3)/log(ord3/ord4);

orderH2_Linf(1) = 0;
orderH2_Linf(2) = log(LinfH2_ord2/LinfH2_ord1)/log(ord1/ord2);
orderH2_Linf(3) = log(LinfH2_ord3/LinfH2_ord2)/log(ord2/ord3);
orderH2_Linf(4) = log(LinfH2_ord4/LinfH2_ord3)/log(ord3/ord4);

orderH2_L2
orderH2_Linf




line0= '\caption{proj solution, m = 1.0, HL = L, Hex union, All SIAC filter, L2 Error.}';

% write code to print L2
line1= strcat('\label{table:4}',...
 '\begin{tabular}{||c| c| c| c| c| c| c | c|c ||}',...
' \hline ');
line2 = ' Mesh Ele & Deg &L2(Before) & L2(Cart SIAC) & order& L2(Hex(1) SIAC) & order& L2(Hex(2) SIAC) & order \\ [0.5ex] ';
line3 = ' \hline\hline ';
% line 1. 
line4 = strcat('$',num2str(ord1),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord1,'%1.2e'), '$ & $',...
    num2str(L2_ord1,'%1.2e'), '$ & $',...
    num2str(order_L2(1)), '$ & $',...
    num2str(L2H1_ord1,'%1.2e'), '$ & $',...
    num2str(orderH1_L2(1)), '$ & $',...
    num2str(L2H2_ord1,'%1.2e'), '$ & $',...
    num2str(orderH2_L2(1)), '$ ',...
    '\\');
line5 = ' \hline ';
% line 2.
line6 = strcat('$',num2str(ord2),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord2,'%1.2e'), '$ & $',...
    num2str(L2_ord2,'%1.2e'), '$ & $',...
    num2str(order_L2(2)), '$ & $',...
    num2str(L2H1_ord2,'%1.2e'), '$ & $',...
    num2str(orderH1_L2(2)), '$ & $',...
    num2str(L2H2_ord2,'%1.2e'), '$ & $',...
    num2str(orderH2_L2(2)), '$ ',...
    '\\');
line7 = ' \hline ';

line8 = strcat('$',num2str(ord3),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord3,'%1.2e'), '$ & $',...
    num2str(L2_ord3,'%1.2e'), '$ & $',...
    num2str(order_L2(3)), '$ & $',...
    num2str(L2H1_ord3,'%1.2e'), '$ & $',...
    num2str(orderH1_L2(3)), '$ & $',...
    num2str(L2H2_ord3,'%1.2e'), '$ & $',...
    num2str(orderH2_L2(3)), '$ ',...
    '\\');
line9 = ' \hline ';

line10 = strcat('$',num2str(ord4),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord4,'%1.2e'), '$ & $',...
    num2str(L2_ord4,'%1.2e'), '$ & $',...
    num2str(order_L2(4)), '$ & $',...
    num2str(L2H1_ord4,'%1.2e'), '$ & $',...
    num2str(orderH1_L2(4)), '$ & $',...
    num2str(L2H2_ord4,'%1.2e'), '$ & $',...
    num2str(orderH2_L2(4)), '$ ',...
    '\\');
line11= ' \hline \end{tabular} ';


fileID = fopen('P2_hexUnionJack_quad_m_1_DG_All_L2.txt','w');
fwrite(fileID,line0);
fwrite(fileID,line1);
fwrite(fileID,line2);
fwrite(fileID,line3);
fwrite(fileID,line4);
fwrite(fileID,line5);
fwrite(fileID,line6);
fwrite(fileID,line7);
fwrite(fileID,line8);
fwrite(fileID,line9);
fwrite(fileID,line10);
fwrite(fileID,line11);
fclose(fileID);



% write code to print Linf
line0= '\caption{proj solution, m = 1.0, HL = L, Hex sym, All SIAC filter, Linf Error.}';

line1= strcat('\label{table:4}',...
 '\begin{tabular}{||c| c| c| c| c| c| c | c|c ||}',...
' \hline ');
line2 = ' Mesh Ele & Deg &Linf(Before) & Linf(Cart SIAC) & order& Linf(Hex(1) SIAC) & order& Linf(Hex(2) SIAC) & order \\ [0.5ex] ';
line3 = ' \hline\hline ';
% line 1. 
line4 = strcat('$',num2str(ord1),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(LinfB_ord1,'%1.2e'), '$ & $',...
    num2str(Linf_ord1,'%1.2e'), '$ & $',...
    num2str(order_Linf(1)), '$ & $',...
    num2str(LinfH1_ord1,'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(1)), '$ & $',...
    num2str(LinfH2_ord1,'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(1)), '$ ',...
    '\\');
line5 = ' \hline ';
% line 2.
line6 = strcat('$',num2str(ord2),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(LinfB_ord2,'%1.2e'), '$ & $',...
    num2str(Linf_ord2,'%1.2e'), '$ & $',...
    num2str(order_Linf(2)), '$ & $',...
    num2str(LinfH1_ord2,'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(2)), '$ & $',...
    num2str(LinfH2_ord2,'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(2)), '$ ',...
    '\\');
line7 = ' \hline ';

line8 = strcat('$',num2str(ord3),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(LinfB_ord3,'%1.2e'), '$ & $',...
    num2str(Linf_ord3,'%1.2e'), '$ & $',...
    num2str(order_Linf(3)), '$ & $',...
    num2str(LinfH1_ord3,'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(3)), '$ & $',...
    num2str(LinfH2_ord3,'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(3)), '$ ',...
    '\\');
line9 = ' \hline ';

line10 = strcat('$',num2str(ord4),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(LinfB_ord4,'%1.2e'), '$ & $',...
    num2str(Linf_ord4,'%1.2e'), '$ & $',...
    num2str(order_Linf(4)), '$ & $',...
    num2str(LinfH1_ord4,'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(4)), '$ & $',...
    num2str(LinfH2_ord4,'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(4)), '$ ',...
    '\\');
line11= ' \hline \end{tabular} ';


fileID = fopen('P2_hexUnionJack_quad_m_1_DG_All_Linf.txt','w');
fwrite(fileID,line0);
fwrite(fileID,line1);
fwrite(fileID,line2);
fwrite(fileID,line3);
fwrite(fileID,line4);
fwrite(fileID,line5);
fwrite(fileID,line6);
fwrite(fileID,line7);
fwrite(fileID,line8);
fwrite(fileID,line9);
fwrite(fileID,line10);
fwrite(fileID,line11);
fclose(fileID);

