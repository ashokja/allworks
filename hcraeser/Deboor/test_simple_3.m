% use Bspline to get Bernstein basis. (if possible.)

clear ;
close all;

deg = 2;

knots = size(2*deg,1);
for i  = 1:2*(deg+1)
    if ( i < deg+1+0.5)
        knots(i) = 0;
    else
        knots(i) = 1;        
    end    
end

%knots(end+1) = 2;
%knots = [0,0,0,0,0.25,0.5,0.75,1,1,1,1];
%cpts = [0,1,0,1,0,1,0];
cpts= [0,1,0,0,0]

% using Deboor algorithm for BSplines.
t_sel = linspace( 0.01,0.99,100 );
b_sel = zeros(size(t_sel));

for index = 1:size(t_sel(:))
    t = t_sel(index);
    
    %find k.
    for uk = 1:size(knots(:))
        if ( knots(uk) > t )
            k = uk-1;
            break;
        end
    end
    
    if  ( (k < deg) )
        disp('screwed up knot t value.');
        break;
    end
    if ( ( k > ( size(knots(:)) -deg +1) ) )
        disp('screwed up knot t value.');
        break;
    end
    
    %load the points required.
    pts_eval(:,1) = cpts(k-deg:k);
    disp(cpts);
    
    for l = 1:deg
        for lp = k-deg+1 + l -1 : k+1-1
            disp([l lp]);
            
            alpha = (t - knots(lp))/ (knots(lp+deg-l+1) - knots(lp));
            pt_index = lp -l+1;
%           pt_index = lp -l+1; -k+deg;
            pts_eval( pt_index, l+1) = (1-alpha)*pts_eval(pt_index ,l) + alpha*pts_eval( pt_index+1,l);
            
            
            %            disp ([l, lp]);
            
 %           alpha = (t - knots(lp) ) / (knots(lp-deg) - knots(lp) ) ;
 %           pts_eval( lp,l+1) = (1-alpha)* pts_eval(lp-1,l) + alpha* pts_eval(lp ,l);
        end
    end
    
    b_sel(index) = pts_eval(1,deg+1);
    %break;
end

plot(t_sel,b_sel ); hold on;
plot( t_sel, deg*((1-t_sel).^(deg-1)).*t_sel.^(1) )

