function [res] = DrawP3Triangles(elType, startIndex,Special,ptCloud)
    switch nargin
        case 0,
            elType = 3;
            startIndex = 20;
            Special = 89;
    end
    startIndex = startIndex-1;
%     Special = Special - startIndex;
%     if ( 3 == elType)
%         res = [1,2,5;
%               2,6,5;
%               2,3,6;
%               3,7,6;
%               3,4,7;
%               4,8,7;
%                5,6,9;
%               6,10,9;
%               6,7,10;
%               7,11,10;
%               7,8,11;
%               8,12,11;
%               9,10,Special;
%               10,11,Special;
%               11,12,Special];
%     else 
%         %quad
% %         res = [1,2,5;
% %               2,6,5;
% %               2,3,6;
% %               3,7,6;
% %               3,4,7;
% %               4,8,7;
% %               5,6,9;
% %               6,7,9;
% %               7,10,9;
% %               7,11,10;
% %               7,8,11;
% %               8,12,11;
% %               9,10,13;
% %               10,14,13;
% %               10,11,14;
% %               11,15,14;
% %               11,12,15;
% %               12,16,15;];
%         res = [1,2,5;
%               2,6,5;
%               2,3,6;
%               3,7,6;
%               3,4,7;
%               4,8,7;
%               5,6,9;
%               6,10,9;
%               6,7,10;
%               7,11,10;
%               7,8,11;
%               8,12,11;
%               9,10,13;
%               10,14,13;
%               10,11,14;
%               11,15,14;
%               11,12,15;
%               12,16,15;];
%     end
%     
%     res = res + startIndex;
    st = startIndex;
%    Edges to draw are 
%    for Triangle   
%     2,6
%     3,7
%     6,10
%     7,11
%     10,Sp
%     11, Sp
colorValue = [0.5 0.5 .5];
    if (3 == elType)
        line( [ptCloud(st+2,1);ptCloud(st+6,1) ], [ ptCloud(st+2,2);ptCloud(st+6,2) ],[ 30;30 ],'Color',colorValue);
        %patchline([vP1(1),vP2(1)],[vP1(2),vP2(2)],[9,9],'linestyle','-','edgecolor','k','linewidth',lw,'EdgeAlpha',0.2);
        line( [ptCloud(st+3,1);ptCloud(st+7,1) ], [ ptCloud(st+3,2);ptCloud(st+7,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+6,1);ptCloud(st+10,1) ], [ ptCloud(st+6,2);ptCloud(st+10,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+7,1);ptCloud(st+11,1) ], [ ptCloud(st+7,2);ptCloud(st+11,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+10,1);ptCloud(Special,1) ], [ ptCloud(st+10,2);ptCloud(Special,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+11,1);ptCloud(Special,1) ], [ ptCloud(st+11,2);ptCloud(Special,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+5,1);ptCloud(st+6,1) ], [ ptCloud(st+5,2);ptCloud(st+6,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+6,1);ptCloud(st+7,1) ], [ ptCloud(st+6,2);ptCloud(st+7,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+7,1);ptCloud(st+8,1) ], [ ptCloud(st+7,2);ptCloud(st+8,2) ],[ 30;30 ],'Color',colorValue);
        %line( [ptCloud(st+8,1);ptCloud(st+9,1) ], [ ptCloud(st+8,2);ptCloud(st+9,2) ],[ 30;30 ]);
        line( [ptCloud(st+9,1);ptCloud(st+10,1) ], [ ptCloud(st+9,2);ptCloud(st+10,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+10,1);ptCloud(st+11,1) ], [ ptCloud(st+10,2);ptCloud(st+11,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+11,1);ptCloud(st+12,1) ], [ ptCloud(st+11,2);ptCloud(st+12,2) ],[ 30;30 ],'Color',colorValue);        
    else
        line( [ptCloud(st+2,1);ptCloud(st+6,1) ], [ ptCloud(st+2,2);ptCloud(st+6,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+3,1);ptCloud(st+7,1) ], [ ptCloud(st+3,2);ptCloud(st+7,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+6,1);ptCloud(st+10,1) ], [ ptCloud(st+6,2);ptCloud(st+10,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+7,1);ptCloud(st+11,1) ], [ ptCloud(st+7,2);ptCloud(st+11,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+10,1);ptCloud(st+14,1) ], [ ptCloud(st+10,2);ptCloud(st+14,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+11,1);ptCloud(st+15,1) ], [ ptCloud(st+11,2);ptCloud(st+15,2) ],[ 30;30 ],'Color',colorValue);
        
        line( [ptCloud(st+5,1);ptCloud(st+6,1) ], [ ptCloud(st+5,2);ptCloud(st+6,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+6,1);ptCloud(st+7,1) ], [ ptCloud(st+6,2);ptCloud(st+7,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+7,1);ptCloud(st+8,1) ], [ ptCloud(st+7,2);ptCloud(st+8,2) ],[ 30;30 ],'Color',colorValue);
        %line( [ptCloud(st+8,1);ptCloud(st+9,1) ], [ ptCloud(st+8,2);ptCloud(st+9,2) ],[ 30;30 ]);
        line( [ptCloud(st+9,1);ptCloud(st+10,1) ], [ ptCloud(st+9,2);ptCloud(st+10,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+10,1);ptCloud(st+11,1) ], [ ptCloud(st+10,2);ptCloud(st+11,2) ],[ 30;30 ],'Color',colorValue);
        line( [ptCloud(st+11,1);ptCloud(st+12,1) ], [ ptCloud(st+11,2);ptCloud(st+12,2) ],[ 30;30 ],'Color',colorValue);        
        
    end
    

end