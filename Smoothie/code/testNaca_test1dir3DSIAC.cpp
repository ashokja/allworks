/// This test file will simulate any of 9 single derivatives depending on input parameters.

#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"
//#include <ctime>



using namespace SIACUtilities;
using namespace std;

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		cout << "4th which parameter do you want to simulate select 1-9." << endl;
		return 0;
	}
	// This is so that vSession does not interpret extra parameters absurdly.
	argc = 2;

	clock_t startTime = clock();
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);

	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	//HNM3D->LoadMesh(var[0]);
	//HNM3D->LoadMesh(var[1]);
	//HNM3D->LoadMesh(var[2]);
	
	cout << "loading xml file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	string fname = vSession->GetSessionName();

	//string fldname = fname + ".fld/Info.xml";	
	//string fldname = "naca.fld";	
	string fldname = fname + ".fld";	
	cout << fname << endl;
	cout << fldname << endl;
	cout << fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pU_sx_phys.txt" << endl;

	HNM3D->LoadData( fldname ,var);
	cout << "loading fld file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	cout << "expsize:\t"<< HNM3D->m_expansions[0]->GetExpSize() << endl;
	
	HNM3D->LoadExpListIntoRTree();
	cout << "loading into R-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	/*
	for (int i =0; i< HNM3D->m_expansions[0]->GetExpSize();i++)
	{
		int gid = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetGlobalID();
		cout << "i\t" << i << "\tg\t" << gid << endl;
		cout << HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetShapeType()<< endl;
	}
	*/
	// Find minx,miny,minz	
	// Find maxx,maxy,maxz	
	cout << HNM3D->m_expansions[0]->GetTotPoints() << endl;
	cout << HNM3D->m_expansions[0]->GetNcoeffs() << endl;
	
	int totPhys = HNM3D->m_expansions[0]->GetTotPoints();
	Array<OneD,NekDouble> xc0(totPhys),xc1(totPhys), xc2(totPhys);
	
	HNM3D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
	NekDouble minx = Vmath::Vmin(totPhys,xc0,1);	
	NekDouble miny = Vmath::Vmin(totPhys,xc1,1);	
	NekDouble minz = Vmath::Vmin(totPhys,xc2,1);	
	NekDouble maxx = Vmath::Vmax(totPhys,xc0,1);	
	NekDouble maxy = Vmath::Vmax(totPhys,xc1,1);	
	NekDouble maxz = Vmath::Vmax(totPhys,xc2,1);	

	cout << "min\t " << minx << "\t" << miny<<"\t"<< minz<< endl; 
	cout << "max\t " << maxx << "\t" << maxy<<"\t"<< maxz<< endl; 
	
	const Array<OneD,NekDouble> u_phys = HNM3D->m_expansions[0]->GetPhys();
	const Array<OneD,NekDouble> v_phys = HNM3D->m_expansions[1]->GetPhys();
	const Array<OneD,NekDouble> w_phys = HNM3D->m_expansions[2]->GetPhys();
	const Array<OneD,NekDouble> p_phys = HNM3D->m_expansions[3]->GetPhys();

	Array<OneD,NekDouble> u_sx_phys(totPhys), v_sx_phys(totPhys), w_sx_phys(totPhys);
	Array<OneD,NekDouble> u_sy_phys(totPhys), v_sy_phys(totPhys), w_sy_phys(totPhys);
	Array<OneD,NekDouble> u_sz_phys(totPhys), v_sz_phys(totPhys), w_sz_phys(totPhys);

	
	cout << "Time taken to calculate 3 derivatives. "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
			
	//SmoothieSIAC3D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]),atof(argv[3]) ); 
	//SmoothieSIAC3D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]),atof(argv[3]),1); 
	
	NekDouble valY,valZ, scaling;
	scaling = atof(argv[3]);
	Array<OneD,NekDouble> directionX(3,0.0),directionY(3,0.0),directionZ(3,0.0);
	directionX[0] = 1.0;	directionY[1] = 1.0;	directionZ[2] = 1.0;
	
	vector<Array<OneD,NekDouble> > directionsXYZ,directionsYXZ, directionsXZY;
	std::shared_ptr<SmoothieSIAC> sm = std::make_shared<SmoothieSIAC3D> (SIACUtilities::eSYM_2kp1_1SIDED_2kp1,
				HNM3D, atoi(argv[2]), atof(argv[3]) );
	std::shared_ptr<SmoothieSIAC> smD = std::make_shared<SmoothieSIAC3D> (SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1,
				HNM3D, atoi(argv[2]), atof(argv[3]) ,1);


	//Declare variables to be used for storing.
	//minx = 2.1; miny = 0.0; minz = 0.6;
	//maxx = 2.5; maxy = 0.5; maxz= 1.1;
	minx = 2.1; miny = 0.15; minz = 0.725;
	maxx = 2.5; maxy = 0.35; maxz= 0.925;
    
	int gPtsX =  1;   //atoi(argv[2]);
    int gPtsY =  100;   //atoi(argv[2]);
    int gPtsZ =  100;   //atoi(argv[2]);
    int Nx = gPtsX, Ny = gPtsY, Nz = gPtsZ;
    int totPts = Nx*Ny*Nz;
    NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0), sz=1.0/(Nz-1.0);
    Array<OneD,NekDouble> pX(totPts), pY(totPts), pZ(totPts), pE(totPts);   
    Array<OneD,NekDouble> pU_p(totPts), pU_sx(totPts), pU_sy(totPts), pU_sz(totPts);
    Array<OneD,NekDouble> pV_p(totPts), pV_sx(totPts), pV_sy(totPts), pV_sz(totPts);
    Array<OneD,NekDouble> pW_p(totPts), pW_sx(totPts), pW_sy(totPts), pW_sz(totPts);
	Array<OneD,NekDouble> coord(3,0.0);
	//  Get List of expansions in space.
	// 	Loop through the expansions and 1DLSIAC first.
		// Load data back into u_sx_phys and other similar variables.
		// sample data at the resolution needed and compare the result to 1-dir LSIAC output.

	HNM3D->readNekArray( u_sx_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pU_sx_phys.txt");
	HNM3D->readNekArray( u_sy_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pU_sy_phys.txt");
	HNM3D->readNekArray( u_sz_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pU_sz_phys.txt");

	HNM3D->readNekArray( v_sx_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pV_sx_phys.txt");
	HNM3D->readNekArray( v_sy_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pV_sy_phys.txt");
	HNM3D->readNekArray( v_sz_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pV_sz_phys.txt");
	
	HNM3D->readNekArray( w_sx_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pW_sx_phys.txt");
	HNM3D->readNekArray( w_sy_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pW_sy_phys.txt");
	HNM3D->readNekArray( w_sz_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pW_sz_phys.txt");

	for (int i =0; i <Nx; i++)
	{
		for(int j=0; j<Ny; j++)
		{
			for(int k=0; k<Nz; k++)
			{
				int index = i*Ny*Nz+j*Nz+k;
				//cout << index << endl;
				//pX[index] = minx + i*sx*(maxx-minx);
				pX[index] = 2.25;
				pY[index] = miny + j*sy*(maxy-miny);
				pZ[index] = minz + k*sz*(maxz-minz);
				coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = pZ[index];
				//int elID = HNM3D->m_expansions[0]->GetExpIndex(coord);
				int elID = HNM3D->GetExpansionIndexUsingRTree(coord);
//				cout << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;	
//				cout << "eid: \t"<<elID<< "\t index \t"<< index << endl;
				pE[index] = elID;
				if (elID !=-1)
				{
					//sm->EvaluateAt(pX[index],pY[index],pZ[index],pU_s[index],valY,valZ, directionX, scaling ,0);
					LocalRegions::ExpansionSharedPtr lexp = HNM3D->m_expansions[0]->GetExp(elID);
    
					int physOffset = HNM3D->m_expansions[0]->GetPhys_Offset(elID);  
    
					const Array<OneD,NekDouble> el_u_phys = u_phys.CreateWithOffset(u_phys,physOffset);
					pU_p[index] = lexp->PhysEvaluate(coord,el_u_phys);
					const Array<OneD,NekDouble> el_v_phys = v_phys.CreateWithOffset(v_phys,physOffset);
					pV_p[index] = lexp->PhysEvaluate(coord,el_v_phys);
					const Array<OneD,NekDouble> el_w_phys = w_phys.CreateWithOffset(w_phys,physOffset);
					pW_p[index] = lexp->PhysEvaluate(coord,el_w_phys);
					
					const Array<OneD,NekDouble> el_u_sx_phys = u_phys.CreateWithOffset(u_sx_phys,physOffset);
					pU_sx[index] = lexp->PhysEvaluate(coord,el_u_sx_phys);
					const Array<OneD,NekDouble> el_u_sy_phys = u_phys.CreateWithOffset(u_sy_phys,physOffset);
					pU_sy[index] = lexp->PhysEvaluate(coord,el_u_sy_phys);
					const Array<OneD,NekDouble> el_u_sz_phys = u_phys.CreateWithOffset(u_sz_phys,physOffset);
					pU_sz[index] = lexp->PhysEvaluate(coord,el_u_sz_phys);

					const Array<OneD,NekDouble> el_v_sx_phys = v_phys.CreateWithOffset(v_sx_phys,physOffset);
					pV_sx[index] = lexp->PhysEvaluate(coord,el_v_sx_phys);
					const Array<OneD,NekDouble> el_v_sy_phys = v_phys.CreateWithOffset(v_sy_phys,physOffset);
					pV_sy[index] = lexp->PhysEvaluate(coord,el_v_sy_phys);
					const Array<OneD,NekDouble> el_v_sz_phys = v_phys.CreateWithOffset(v_sz_phys,physOffset);
					pV_sz[index] = lexp->PhysEvaluate(coord,el_v_sz_phys);

					const Array<OneD,NekDouble> el_w_sx_phys = w_phys.CreateWithOffset(w_sx_phys,physOffset);
					pW_sx[index] = lexp->PhysEvaluate(coord,el_w_sx_phys);
					const Array<OneD,NekDouble> el_w_sy_phys = w_phys.CreateWithOffset(w_sy_phys,physOffset);
					pW_sy[index] = lexp->PhysEvaluate(coord,el_w_sy_phys);
					const Array<OneD,NekDouble> el_w_sz_phys = w_phys.CreateWithOffset(w_sz_phys,physOffset);
					pW_sz[index] = lexp->PhysEvaluate(coord,el_w_sz_phys);

				}else{
					cout << "out of box. Somehting is wrong." << endl;
					assert( false&&"out of box. Something is wrong" );
				}
			}
			cout << j<< endl;
			cout << "quering into R-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds for Nz = " << Nz<< endl;
			startTime = clock();
		}
		cout << i << endl;
	}
	
	HNM3D->writeNekArray(pX,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pX.txt");
	HNM3D->writeNekArray(pY,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pY.txt");
	HNM3D->writeNekArray(pZ,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pZ.txt");
	HNM3D->writeNekArray(pE,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pE.txt");
	
	HNM3D->writeNekArray(pU_p,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pU_p.txt");
	HNM3D->writeNekArray(pU_sx,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pU_sx.txt");
	HNM3D->writeNekArray(pU_sy,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pU_sy.txt");
	HNM3D->writeNekArray(pU_sz,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pU_sz.txt");
	
	HNM3D->writeNekArray(pV_p,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pV_p.txt");
	HNM3D->writeNekArray(pV_sx,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pV_sx.txt");
	HNM3D->writeNekArray(pV_sy,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pV_sy.txt");
	HNM3D->writeNekArray(pV_sz,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pV_sz.txt");
	
	HNM3D->writeNekArray(pW_p,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pW_p.txt");
	HNM3D->writeNekArray(pW_sx,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pW_sx.txt");
	HNM3D->writeNekArray(pW_sy,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pW_sy.txt");
	HNM3D->writeNekArray(pW_sz,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pW_sz.txt");


	/*


	for (int i =0; i <Nx; i++)
	{
		for(int j=0; j<Ny; j++)
		{
			for(int k=0; k<Nz; k++)
			{
				int index = i*Ny*Nz+j*Nz+k;
				//cout << index << endl;
				//pX[index] = minx + i*sx*(maxx-minx);
				pX[index] = 2.25;
				pY[index] = miny + j*sy*(maxy-miny);
				pZ[index] = minz + k*sz*(maxz-minz);
				coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = pZ[index];
				//int elID = HNM3D->m_expansions[0]->GetExpIndex(coord);
				int elID = HNM3D->GetExpansionIndexUsingRTree(coord);
//				cout << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;	
//				cout << "eid: \t"<<elID<< "\t index \t"<< index << endl;
				pE[index] = elID;
				if (elID !=-1)
				{
					//sm->EvaluateAt(pX[index],pY[index],pZ[index],pU_s[index],valY,valZ, directionX, scaling ,0);
					//Sms[0]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pU_ss[index],valY,valZ,Sms, directions, scalings ,varsU,1);
					switch( atoi(argv[4]))
					{
						case 1:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pUx_ss[index],valY,valZ,SmD2s, directionsYXZ, scalings ,varsU,1);
							break;
						case 2:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pUy_ss[index],valY,valZ,SmD2s, directionsXYZ, scalings ,varsU,1);
							break;
						case 3:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pUz_ss[index],valY,valZ,SmD2s, directionsXZY, scalings ,varsU,1);
							break;
						case 4:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pVx_ss[index],valY,valZ,SmD2s, directionsYXZ, scalings ,varsV,1);
							break;
						case 5:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pVy_ss[index],valY,valZ,SmD2s, directionsXYZ, scalings ,varsV,1);
							break;
						case 6:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pVz_ss[index],valY,valZ,SmD2s, directionsXZY, scalings ,varsV,1);
							break;
						case 7:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pWx_ss[index],valY,valZ,SmD2s, directionsYXZ, scalings ,varsW,1);
							break;
						case 8:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pWy_ss[index],valY,valZ,SmD2s, directionsXYZ, scalings ,varsW,1);
							break;
						case 9:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pWz_ss[index],valY,valZ,SmD2s, directionsXZY, scalings ,varsW,1);
							break;
					}
				}else
				{	
					pU_p[index] = 0.0;
					pV_p[index] = 0.0;
					pW_p[index] = 0.0;
					pP_p[index] = 0.0;
					pP_s[index] = 0.0;
					cout << "out" << endl;
				}
	cout << "quering into R-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds for Nz = " << Nz<< endl;
	startTime = clock();
			}
			cout << j<< endl;
		}
		cout << i << endl;
	}
	
// Calculate acceleration,  b, torsion, Dbv for this data.


	switch ( atoi(argv[4]) )
	{
		case 1:
			k.writeNekArray(pX,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pX.txt");
			k.writeNekArray(pY,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pY.txt");
			k.writeNekArray(pZ,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pZ.txt");
			k.writeNekArray(pE,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pE.txt");
			k.writeNekArray(pUx_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pUx_ss.txt");
			break;
		case 2:
			k.writeNekArray(pUy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pUy_ss.txt");
			break;
		case 3:
			k.writeNekArray(pUz_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pUz_ss.txt");
			break;
		case 4:
			k.writeNekArray(pVx_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pVx_ss.txt");
			break;
		case 5:
			k.writeNekArray(pVy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pVy_ss.txt");
			break;
		case 6:
			k.writeNekArray(pVz_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pVz_ss.txt");
			break;
		case 7:
			k.writeNekArray(pWx_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pWx_ss.txt");
			break;
		case 8:
			k.writeNekArray(pWy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pWy_ss.txt");
			break;
		case 9:
			k.writeNekArray(pWz_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pWz_ss.txt");
			break;
	}
	
	//k.writeNekArray(pU_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pU_s.txt");
	//k.writeNekArray(pU_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pU_ss.txt");
	//k.writeNekArray(pUy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pUy_ss.txt");
	//k.writeNekArray(pUy_ssx,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pUy_ssx.txt");
	//k.writeNekArray(pUy_ssy,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pUy_ssy.txt");
	//k.writeNekArray(pUy_ssz,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_test1dir3DSIAC_pUy_ssz.txt");
	*/

	return 0;
}


