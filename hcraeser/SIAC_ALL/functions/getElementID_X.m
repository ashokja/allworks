function [el] = getElementID_X( x_p, v_x)
	addpath('../PolyLib');
	% find which element x_p is present in ?
	if (x_p < v_x(1) || x_p >v_x(end))
		('invalid Input')
		el = NaN;
		return;
	end
	Nv = max(size(v_x));
	Ne = Nv-1;
	el = 0;
	for e=1:Ne
		if ( v_x(e+1) >= x_p && v_x(e) <= x_p)
			el =e;
			break;
		end
	end
		
end
