% testAngles_twice_D2

clear;
close all;
% test 
rates = [20];
%rates = [20,40,60,80];

poly =2;
order = poly;
count =1;
fname = '_quad_2DMesh_';

% Angle1 = 45;
% scaling1 = '0.05';
% Angle2 = 135;
% scaling2 = '0.05';

Angle1 = 0;
Angle2 = 90;
scaling2 = '0.05';
scaling1 = '0.05';
%scaling1 = '0.07071067812';
sam_R = 81

for r = rates
    fname = strcat('P',int2str(poly),'_quad_2DMesh_',int2str(r),'_Pp_',int2str(order),'_S1_',scaling1,'_A1_',int2str(Angle1),'_S2_',scaling2,'_A2_',int2str(Angle2),'_R_',int2str(sam_R));
    xfname = strcat('data/',fname,'_pX.txt' );
    yfname = strcat('data/',fname,'_pY.txt' );
    ffname = strcat('data/',fname,'_pV.txt' );
    efname = strcat('data/',fname,'_pP.txt' );
    sfname = strcat('data/',fname,'_pS.txt' );

    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');
    fval = textread(ffname, '%f\t');
    pval = textread(efname, '%f\t');
    sval = textread(sfname, '%f\t');

    s = floor( (max(size(xpos)))^(1/2) +0.1);
    xVpos = reshape(xpos,[s s]);
    yVpos = reshape(ypos,[s s]);
    fVval = reshape(fval,[s s]);
    pVval = reshape(pval,[s s]);
    sVval = reshape(sval,[s s]);
    
    
    figure(1)
    surf(xVpos,yVpos, fVval);

    figure(2)
    surf(xVpos,yVpos, abs(fVval-pVval)); title('projection error')

    figure(3)
    surf(xVpos,yVpos, abs(fVval-sVval)); title('LSIAC error')

    disp('Projection Error');
    disp(sum(abs(fval-pval))./max(size(xpos)));
    disp(max(abs(fval-pval)));
    disp('LSIAC Error');
    disp(sum(abs(fval-sval))./max(size(xpos)));
    disp(max(abs(fval-sval)));
    
%     figure(1)
%     plot(xpos,log10(abs(fval-pval)),xpos,log10(abs(fval-sval)));
%     
%     figure(2)
%     plot(xpos,log10(abs(fval-sval)),'DisplayName',strcat('No of ele:',int2str(r)));
%     hold on;
% 
%     figure(3)
%     plot(xpos,pval);
    
%     p_avg_err(count) = sum(abs(fval-pval))/max(size(xpos));
%     s_avg_err(count) = sum(abs(fval-sval))/max(size(xpos));
%     
%     p_max_err(count) = max(abs(fval-pval));
%     s_max_err(count) = max(abs(fval-sval));
    count = count+1;
end
