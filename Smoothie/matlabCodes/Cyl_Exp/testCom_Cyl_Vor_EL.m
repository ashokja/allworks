clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];

filePoly = 3;
poly = 3;

for r = filePoly
    filename = strcat( '../datatxt/Cyl_',int2str(poly),'_');
    efname = strcat( filename,'pE_EL.txt');
    xfname = strcat( filename,'pX_EL.txt');
    yfname = strcat( filename,'pY_EL.txt');
    
    upfname = strcat( filename,'pU_p_EL.txt');
    vpfname = strcat( filename,'pV_p_EL.txt');
    usfname = strcat( filename,'pU_s_EL.txt');
    vsfname = strcat( filename,'pV_s_EL.txt');
    
    uxpfname = strcat( filename,'pUx_p_EL.txt');
    vxpfname = strcat( filename,'pVx_p_EL.txt');
    uxsfname = strcat( filename,'pUx_s_EL.txt');
    vxsfname = strcat( filename,'pVx_s_EL.txt');
    
    uypfname = strcat( filename,'pUy_p_EL.txt');
    vypfname = strcat( filename,'pVy_p_EL.txt');
    uysfname = strcat( filename,'pUy_s_EL.txt');
    vysfname = strcat( filename,'pVy_s_EL.txt');
    
    
    Vorpfname = strcat( filename,'pVor_p_EL.txt');
    Vorsfname = strcat( filename,'pVor_s_EL.txt');

    disp(xfname);

    elval = floor( textread(efname, '%f\t') + 0.0001);
    xval = textread(xfname, '%f\t');
    yval = textread(yfname, '%f\t');
    
    upval = textread(upfname, '%f\t');
    vpval = textread(vpfname, '%f\t');
    usval = textread(usfname, '%f\t');
    vsval = textread(vsfname, '%f\t');
    
    uxpval = textread(uxpfname, '%f\t');
    vxpval = textread(vxpfname, '%f\t');
    uxsval = textread(uxsfname, '%f\t');
    vxsval = textread(vxsfname, '%f\t');
    
    uypval = textread(uypfname, '%f\t');
    vypval = textread(vypfname, '%f\t');
    uysval = textread(uysfname, '%f\t');
    vysval = textread(vysfname, '%f\t');
    
    vorpval = textread(Vorpfname, '%f\t');
    vorsval = textread(Vorsfname, '%f\t');
    
    [unique_ids,id_offset,id_tags] = unique(elval);
 %%   
    % for each tag
        % get the data.
        % vis the data
    %end
    num_unique_ids = (max(size(unique_ids)));
    for tag = unique_ids'
        
        el_indices = find(elval== (zeros(size(elval))+tag) );
%         if (tag ~= num_unique_ids)
%             el_indices = (id_offset(tag) :1: (id_offset(tag+1))-1);
%         else
%             el_indices = (id_offset(tag) :1: max(size(xval)));
%         end
        
        el_x = xval(el_indices);
        el_y = yval(el_indices);
%        el_p = vorpval(el_indices);
%        el_s = vorsval(el_indices);
        el_p = upval(el_indices);
        el_s = usval(el_indices);
        s = sqrt(max(size(el_x)));
        
        el_x = reshape(el_x,[s,s]);
        el_y = reshape(el_y,[s,s]);
        el_p = reshape(el_p,[s,s]);
        el_s = reshape(el_s,[s,s]);        
 
        figure(2),
        surf(el_x,el_y,el_p); hold on;
        shading interp;
        xlabel('x'); ylabel('y');
        figure(3),
        surf(el_x,el_y,el_s); hold on;
        shading interp;
        xlabel('x'); ylabel('y');
        
        figure(4),
        contour(el_x,el_y,el_p, (min(usval(:)) :.1: max(usval(:))) ); hold on;
        figure(5),
        contour(el_x,el_y,el_s, (min(usval(:)) :.1: max(usval(:))) ); hold on;
        
    end
end

%%
figure(2),
xlim([2.5,10.5]);
ylim([-4,4]);
view([0,90])
grid off;
%saveas(2,'../images/2DImages/vor_DG1.png');

figure(3)
xlim([2.5,10.5]);
ylim([-4,4]);
%view([-20.5,62])
view([0,90])
grid off;
%saveas(3,'../images/2DImages/vor_SI1.png');

figure(4),
xlim([2.5,10.5]);
ylim([-4,4]);
%saveas(4,'../images/2DImages/vor_DG_C.png');

figure(5)
xlim([2.5,10.5]);
ylim([-4,4]);
%saveas(4,'../images/2DImages/vor_SI_C.png');



%     s = sqrt(max(size(xpos)));
%     xVpos = reshape(xpos,s,s);
%     yVpos = reshape(ypos,s,s);
%     fVval = reshape(fval,s,s);
%     pVval = reshape(pval,s,s);
%     sVval = reshape(sval,s,s);    