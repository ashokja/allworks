function [] = VizNekSurfaceScalar(X,Y,Z,U,sx,sy,sz)
    switch nargin
        case 4
            sx = [-1];
            sy = [-1,0,1];
            sz = [-1];
    end
 %   normAtCheb = permute(normAtCheb,[2,1,3]);
    X = permute(X,[2,1,3]); U = permute(U,[2,1,3]);
    Y = permute(Y,[2,1,3]); 
    Z = permute(Z,[2,1,3]); 
    
    if (min(U(:)) == max(U(:)))
        v = 'All value same'
        return
    end
    hsurfaces = slice(X,Y,Z,U,sx,sy,sz);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none');
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar;
end