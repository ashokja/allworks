#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField2D.h>
//#include <MultiRegions/DisContField2D.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"

#define MAXSCALING 2.0
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();

	string fname = vSession->GetSessionName();
	HNM2D->LoadData(fname+".fld", var);
	//HNM2D->LoadMesh(var[0]);
	//HNM2D->LoadMesh(var[1]);
	//HNM2D->LoadMesh(var[2]);
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	int tNCoeffs = HNM2D->m_expansions[0]->GetNcoeffs();
	cout << "Totpts:" << tNquadPts<< endl;
	cout << "NCoefs:" << tNCoeffs<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}

	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());

	HNM2D->m_expansions[1]->BwdTrans( HNM2D->m_expansions[1]->GetCoeffs(),
										HNM2D->m_expansions[1]->UpdatePhys());
	
	HNM2D->m_expansions[2]->BwdTrans( HNM2D->m_expansions[2]->GetCoeffs(),
										HNM2D->m_expansions[2]->UpdatePhys());

	Array<OneD,NekDouble> u_Phys, v_Phys, p_Phys;
	u_Phys = HNM2D->m_expansions[0]->GetPhys();
	v_Phys = HNM2D->m_expansions[1]->GetPhys();
	p_Phys = HNM2D->m_expansions[2]->GetPhys();

	vector<NekDouble> elidOffsets;
	vector<NekDouble> ElType;
	for ( int i=0; i< HNM2D->m_expansions[0]->GetExpSize(); i++)
	{
		elidOffsets.push_back( HNM2D->m_expansions[0]->GetPhys_Offset(i)+0.001);
		ElType.push_back( HNM2D->m_expansions[0]->GetExp(i)->GetGeom()->GetShapeType() +0.001);
	}


    Array<OneD,NekDouble> offsets(HNM2D->m_expansions[0]->GetExpSize());
    Array<OneD,NekDouble> elmTypes(HNM2D->m_expansions[0]->GetExpSize());
    for (int i =0; i<HNM2D->m_expansions[0]->GetExpSize();i++)
    {   
        offsets[i] = HNM2D->m_expansions[0]->GetPhys_Offset(i);
        elmTypes[i] = HNM2D->m_expansions[0]->GetExp(i)->GetGeom()->GetShapeType();
    }   

    cout << HNM2D->m_expansions[0]->GetExp(0)->GetStdExp()->GetTotPoints() << endl;


    vector<NekDouble> xposEl, yposEl, VposEl, IdposEl;
    Array<OneD,NekDouble> lcoord(3),gcoord(3);
    lcoord[0] = -1.0; lcoord[1] = 1.0;
    for (int i =0; i<HNM2D->m_expansions[0]->GetExpSize();i++)
    {   
        if (HNM2D->m_expansions[0]->GetExp(i)->GetGeom()->GetShapeType() == LibUtilities::eTriangle )
        {   
            cout << "found a triangle" << endl;
        HNM2D->m_expansions[0]->GetExp(i)->GetCoord(lcoord,gcoord);
        xposEl.push_back(gcoord[0]);
        yposEl.push_back(gcoord[1]);
        IdposEl.push_back(i);
        }   
    }   


	HNM2D->writeNekArray( xc0	, fname+"_x.txt");	
	HNM2D->writeNekArray( xc1	, fname+"_y.txt");	
	HNM2D->writeNekArray(u_Phys , fname+"_u.txt");	
	HNM2D->writeNekArray(v_Phys , fname+"_v.txt");	
	HNM2D->writeNekArray(elidOffsets, fname+"_elO.txt");	
	HNM2D->writeNekArray(ElType, fname+"_elT.txt");	
	HNM2D->writeNekArray(xposEl,fname+"_Elx.txt");
    HNM2D->writeNekArray(yposEl,fname+"_Ely.txt");
    HNM2D->writeNekArray(IdposEl,fname+"_Elid.txt");

	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
