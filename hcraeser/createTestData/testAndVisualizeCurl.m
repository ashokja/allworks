% take a field mapxy and mapxyV.
% build coeffients for it.
% build gradient for it.
% calculate the curl at every point.
clear;
N = [7];
addpath ../VortexOnBasisEl;

count = 1;
for n = N
[xy,distance,t_a,mapxy,mapxyV,curveCoeff] = test_runNodes(1,n+3);
coeff = buildCoeff(n,n,n,mapxy,mapxyV);
[curlCoeff,curlData] = calculateCurl(n,n,n,coeff,mapxy,mapxyV);
figure(1),
drawStreamLine(mapxy,curlData,curveCoeff);
figure(2),
quiver3(mapxy(:,1),mapxy(:,2),mapxy(:,3),curlData(:,1),curlData(:,2),curlData(:,3));
figure(n*10+3),
            ywidth = 1;
            startx = (-0.2:0.05:0);
            startz = (-0.8:0.05:-0.6);
            starty = -1;
    [sx,sy,sz] = meshgrid(startx,starty,startz);
curveData_poly(count,:,:) = curlData;
drawOnlyStreamLine(mapxy,curlData,ywidth,sx,sy,sz); hold on;
drawOnlyCurve(curveCoeff);
%drawStreamLineOnlyBullzye(mapxy,curlData,curveCoeff); hold off;
%build coeffient at everypoint.
count = count +1;
end
%save('testdata/testDataSimple_1_05_curl.mat');
%%
[rotCoeff,rotData] = removeCurlComponent(n,n,n,mapxy,mapxyV,curlData);
figure(116)
[sol_seeds,goodSeeds,solAtSeeds] = zerosAtProjected(n,n,n,rotCoeff);
%%
drawStreamLine(mapxy,rotData,curveCoeff);

