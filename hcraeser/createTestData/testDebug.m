% testDebug rotational value errors.

clear;
addpath ../VortexOnBasisEl
%variables
tic
a = -66.5; b = 12;
n = 11;
[xy,distance,t_a,mapxy,mapxyV,curveCoeff] = test_run_dist(1,41);
toc

tic
coeff = buildCoeff(n,n,n,mapxy,mapxyV);
dataOut = buildData(n,n,n,coeff,mapxy);

[coeffRed,coeffRot] = divideVel(n,n,n,coeff,mapxy,dataOut);

figure(1)
drawStreamLine(mapxy,dataOut,curveCoeff);
title(strcat('Input interpolated velocity n = ',int2str(n))); colorbar; hold off; view(a,b);
dataORot = buildData(n,n,n,coeffRot,mapxy);
figure(2)
drawStreamLine(mapxy,dataORot,curveCoeff);
title(strcat('Rotational Velocity',int2str(n))); colorbar; hold off; view(a,b);

dataORed = buildData(n,n,n,coeffRed,mapxy);
figure(3)
drawStreamLine(mapxy,dataORed,curveCoeff);
title(strcat('Reduced Velocity',int2str(n))); colorbar; hold off; view(a,b);


figure(4),
    [sol_seeds,goodSeeds,solAtSeeds] = zerosAtProjected(n,n,n,coeffRot);
    title(strcat('zeros On faces',int2str(n))); 
% Cleaning the seeds i.e. Removing Redundancy seeds points.
[seedNum,dim] = size(goodSeeds);
% write a function to reduce duplicates.
for i = 1:seedNum
  for j = 1:dim
    if (goodSeeds(i,j) ==1)
        for ii = i+1:seedNum
            if (goodSeeds(ii,j) ==1)
                if ( norm(squeeze(abs(sol_seeds(i,j,:) - sol_seeds(ii,j,:)))) < 10^(-2))
                    goodSeeds(ii,j) = 0;
                    gseeds(count,:) = sol_seeds(i,j,:);
                    count = count+1;
                end
            end
        end
    end
  end
end
count =1;
for i = 1:seedNum
  for j = 1:dim
      if goodSeeds(i,j) ==1
        gseeds(count,:) = sol_seeds(i,j,:);
        count = count+1;
      end
  end
end

figure(6)
%drawStreamLine(mapxy,dataORot,curveCoeff); hold on;
plotBoundingBox(); hold on;
drawOnlyCurve(curveCoeff);
drawOnlyStreamLine(mapxy,dataORed,curveCoeff,gseeds(:,1),gseeds(:,2),gseeds(:,3)); hold on;
drawOnlyStreamLine(mapxy,-1*dataORed,curveCoeff,gseeds(:,1),gseeds(:,2),gseeds(:,3)); hold off;
title(strcat('sol Axis',int2str(n))); view(a,b);
toc
%% 
tps_poly = {};
tpsVel_poly = {};
    DT = [2];
    for dt = DT
        count =1; tps = {}; tpsVel = {};
        for i = 1:seedNum
            for j = 1:dim
                if (goodSeeds(i,j) ==1)
                    [trace,traceVel] = traceThePoint(n,n,n,coeff, sol_seeds(i,j,:),10^(-dt) );
                    tps(count) = {trace};
                    tpsVel(count) = {traceVel};
                    count = count +1;
                end
            end
        end
        tps_poly(n,dt)= {tps};
        tpsVel_poly(n,dt) = {tpsVel};
    end    
%%
% [~,b] = size(tps);
% figure(5);
% plotBoundingBox(); hold on;
% for i =1:b
%     trace = cell2mat(tps(i));
%     plot3(trace(:,1),trace(:,2),trace(:,3),'g');
%     hold on;
% end
% drawStreamLine(mapxy,dataORot,curveCoeff);
% %       hold off;


