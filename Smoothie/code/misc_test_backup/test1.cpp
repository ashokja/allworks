#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC1D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh1D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

 
int main(int argc, char* argv[])
{
	cout << "This is test case 1. Test and help build EvaluateAt function. " << endl;
	//step 1. create a session.
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	cout << "**** Step1 ********" << endl;
	//step 2. HandleNekMesh1D (sessionPtr);
	HandleNekMesh* HNM1D = new HandleNekMesh1D(vSession);
	cout << "**** Step2 ********" << endl;
	//step 3. SmoothieSIAC(HNB);
	Array<OneD,NekDouble> direction(3,0.0);
//	SmoothieSIAC1D sm(SIACUtilities::eSYM_2kp1, HNM1D, 2 ); 
	SmoothieSIAC1D sm(SIACUtilities::eSYM_2kp1, HNM1D, 2, 0.5); 
	cout << "**** Step3 ********" << endl;
	//step 4. SmoothieEvalute();
	NekDouble valX,valY,valZ;
	sm.EvaluateAt(2.3,1.0,0.0,valX,valY,valZ);
	
	//step 5. Finalize();
	

 
//	HandleNekMesh* mshHandle; 
//	SmoothieSIAC1D( eSYM_2kp1,mshHandle, 2);
	return 0;
}

void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
