    function [rest] = B3_der_eval(n1,n2,n3,coeff,dataIn)
        count = 1;
       [a,b] = size(dataIn);
       rest = zeros(a,b+6);
       for k1 = 0:n1
           for k2 = 0:n2
               for k3 = 0:n3
                    rest(:,1:3) = rest(:,1:3)  + coeff(count,1)*B3_der(n1,k1,n2,k2,n3,k3,dataIn);
                    rest(:,4:6) = rest(:,4:6)  + coeff(count,2)*B3_der(n1,k1,n2,k2,n3,k3,dataIn);
                    rest(:,7:9) = rest(:,7:9)  + coeff(count,3)*B3_der(n1,k1,n2,k2,n3,k3,dataIn);
                    count= count +1;
               end
           end
       end        
    end
