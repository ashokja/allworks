function [] = VizStreamtubes(xcheb,ycheb,zcheb,U,V,W,sx,sy,sz)
    switch nargin
        case 6,
            [sx, sy, sz] = meshgrid([-0.3:.1:0.3],[-1,1],[0]);
%            [sx, sy, sz] = meshgrid((-.2:.2:.2),[-1,0,1],(-.2:.2:.2));
    end
    [X,Y,Z] = meshgrid(xcheb,ycheb,zcheb);
    U = permute(U,[2,1,3]);
    V = permute(V,[2,1,3]);
    W = permute(W,[2,1,3]);

    htubes = streamtube(X,Y,Z,U,V,W,sx,sy,sz,[1.25 30]);
        xlabel('x'); ylabel('y'); zlabel('z');
set(htubes,'EdgeColor','none','FaceColor','r',...
   'AmbientStrength',.5)
end