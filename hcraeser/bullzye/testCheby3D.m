%testCheby3D.m
function [] = testCheby3D(NX,NY,NZ,nx,ny,nz,px,py,pz)
% we can do test for 1D first    
    switch nargin
        case 0,
            NX =5; NY=6; NZ = 7;
            nx =1; ny =1; nz =1;
            px =0;py =0; pz =0 ;
    end
    addpath ../PolyLib
    %WXQ,WYQ,XQ,YQ,NX,NY
    [XQ,WXQ] = JacobiGLZW(NX+nx,0,0);
    [YQ,WYQ] = JacobiGLZW(NY+ny,0,0);
    [ZQ,WZQ] = JacobiGLZW(NZ+nz,0,0);
    %Cal Lagrance before itself.
    for i =0:NX
         PhiXQ(i+1,:) = LegendrePoly(i,XQ);
    end
    for j = 0:NY
        PhiYQ(j+1,:) = LegendrePoly(j,YQ);
    end
    for k = 0:NZ
        PhiZQ(k+1,:) = LegendrePoly(k,ZQ);
    end
    for i = 0:NX
        for j = 0:NY
            for k = 0:NZ
                sol = 0;
                for x = 1:max(size(XQ))
                    for y = 1:max(size(YQ))
                        for z  = 1:max(size(ZQ))
                            sol = sol + func(XQ(x),YQ(y),ZQ(z))*WXQ(x)*WYQ(y)*WZQ(z)*...
                               outer(outer(PhiXQ(i+1,x),PhiYQ(j+1,y),2),PhiZQ(k+1,z),3);
                        end
                    end
                end
                u(i+1,j+1,k+1) = (2*i+1)*(2*j+1)*(2*k+1)/8*sol;
            end
        end
    end
    %u(1,1) = 2.6;
    solPts = zeros( max(size(XQ)), max(size(YQ)),max(size(ZQ)) );
    for i = 0:NX
        for j = 0:NY
            for k = 0:NZ
                solPts = solPts+u(i+1,j+1,k+1)* ...
                    outer(outer(PhiXQ(i+1,:),PhiYQ(j+1,:),2),PhiZQ(k+1,:),3);
            end
        end
    end
    
    [X,Y,Z] = meshgrid(XQ,YQ,ZQ);
    figure(1);
    hsurfaces = slice(X,Y,Z,func(X,Y,Z),[-1,0,1],1,-1);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold off;
    %isosurface(X,Y,Z,func(X,Y,Z),1.4); 
    
    figure(2)
    solPts = permute(solPts,[2,1,3]);
    hsurfaces = slice(X,Y,Z,solPts,[-1,0,1],1,-1);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
        xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold off;
%    isosurface(X,Y,Z,solPts,1.5); 
    ActEr = func(X,Y,Z) - solPts;
    Er = sum(abs(ActEr(:)))
    
    [DChebX,xcheb] = cheb(NX+nx+px);
    [DChebY,ycheb] = cheb(NY+ny+py);
    [DChebZ,zcheb] = cheb(NZ+nz+pz);
    
    clear Phi*;
    for i =0:NX
         PhiXQ(i+1,:) = LegendrePoly(i,xcheb);
    end
    for j = 0:NY
        PhiYQ(j+1,:) = LegendrePoly(j,ycheb);
    end
    for k = 0:NZ
        PhiZQ(k+1,:) = LegendrePoly(k,zcheb);
    end

    solChebPts = zeros( max(size(xcheb)), max(size(ycheb)),max(size(zcheb)) );
    for i = 0:NX
        for j = 0:NY
            for k = 0:NZ
                solChebPts = solChebPts+u(i+1,j+1,k+1)* ...
                    outer(outer(LegendrePoly(i,xcheb),LegendrePoly(j,ycheb)...
                    ,2),LegendrePoly(k,zcheb),3);
%                    outer(outer(PhiXQ(i+1,:),PhiYQ(j+1,:),2),PhiZQ(k+1,:),3)                    
            end
        end
    end

    
    [XCheb,YCheb,ZCheb] = meshgrid(xcheb,ycheb,zcheb);
    figure(10)
    solChebPts = permute(solChebPts,[2,1,3]);
    hsurfaces = slice(XCheb,YCheb,ZCheb,solChebPts,[-1,0,1],1,-1);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold off;
    ActEr = func(XCheb,YCheb,ZCheb) - solChebPts;
    Er = sum(abs(ActEr(:)))
    
    solChebPts = ipermute(solChebPts,[2,1,3]);
    % X partial Derivative
    for k = 1:max(size(zcheb))
        solChebPtsDX(:,:,k) = DChebX * solChebPts(:,:,k); 
    end
    % Y partial Derivative
    for k = 1:max(size(zcheb))
        solChebPtsDY(:,:,k) = (DChebY * solChebPts(:,:,k)')';
    end
    % Z partial Derivative
    for i = 1:max(size(xcheb))
        solChebPtsDZ(i,:,:) = (DChebZ * squeeze(solChebPts(i,:,:))')';
    end
    
    
    figure(3)
    hsurfaces = slice(XCheb,YCheb,ZCheb,funcDX(XCheb,YCheb,ZCheb),[-1,0,1],1,-1);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold off;
    figure(4)
    solChebPtsDX = permute(solChebPtsDX,[2,1,3]);
    hsurfaces = slice(XCheb,YCheb,ZCheb,solChebPtsDX,[-1,0,1],1,-1);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold off;
    
    ActEr = funcDX(XCheb,YCheb,ZCheb) - solChebPtsDX;
    Er = sum(abs(ActEr(:)))

    
    figure(5)
    hsurfaces = slice(XCheb,YCheb,ZCheb,funcDY(XCheb,YCheb,ZCheb),[-1,0,1],1,-1);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold off;
    figure(6)
    solChebPtsDY = permute(solChebPtsDY,[2,1,3]);
    hsurfaces = slice(XCheb,YCheb,ZCheb,solChebPtsDY,[-1,0,1],1,-1);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold off;

    ActEr = funcDY(XCheb,YCheb,ZCheb) - solChebPtsDY;
    Er = sum(abs(ActEr(:)))
    
    
    figure(7)
    hsurfaces = slice(XCheb,YCheb,ZCheb,funcDZ(XCheb,YCheb,ZCheb),[-1,0,1],1,-1);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold off;
    figure(8)
    solChebPtsDZ = permute(solChebPtsDZ,[2,1,3]);
    hsurfaces = slice(XCheb,YCheb,ZCheb,solChebPtsDZ,[-1,0,1],1,-1);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold off;
    
    ActEr = funcDZ(XCheb,YCheb,ZCheb) - solChebPtsDZ;
    Er = sum(abs(ActEr(:)))

    
    function r = func(x,y,z)
%        r = x.^3+y.^3+ z.^3;
        r = x.*sin((0.2)*pi*x)+y.*sin((0.7)*pi*y)+ z.*sin(pi*z);
        %r = x+y.^5+z;
    end
    
    function r = funcDX(x,y,z)
        %r = 3*x.^2;
        r = sin(0.2*pi*x) + 0.2*pi*x.*cos(0.2*pi*x);
        %r = x+y.^5+z;
    end
    
    function r = funcDY(x,y,z)
%        r = 3*y.^2;
        r = sin(0.7*pi*y) + 0.7*pi*y.*cos(0.7*pi*y);
        %r = x+y.^5+z;
    end
    
    function r = funcDZ(x,y,z)
        %r = 3*z.^2;
        r = sin(pi*z) + pi*z.*cos(pi*z);
        %r = x+y.^5+z;
    end
end