% Driver script for solving the 1D advection equations
clear;
close all;
Globals1D;

% Order of polymomials used for approximation 
N = 10;

% Generate simple mesh
[Nv, VX, K, EToV] = MeshGen1D(0,1,1);

% Initialize solver and construct grid and metric
StartUp1D;

% Set initial conditions
u = x;
plot(x,u);


[rp,wp] = JacobiGQ(0,0,N);
[rp] = JacobiGL(0,0,N);
[V1D] = Vandermonde1D(N,rp);
x = rp;
u = (rp+1)/2;


for n = 0:N
    rhs(n+1) = 1/2*sum(JacobiP(rp,0,0,n).*wp.*u);
end

%Mass matrix
for ni = 0:N
    for nj = 0:N
        M(ni+1,nj+1) = 1/2*sum(wp.*JacobiP(rp,0,0,ni).*JacobiP(rp,0,0,nj));
    end
end

uhat = inv(M)*rhs';

uf = V1D*uhat

uf_D = 2*GradVandermonde1D(N,rp)*uhat



% 
% % Solve Problem
% FinalTime = 10;
% [u] = Advec1D(u,FinalTime);
% [a1, a2] = size(u);
% u_p = reshape(u,[a1*a2,1]);
% x_p = reshape(x,[a1*a2,1]);
% figure(1),
% plot(x,u); hold on;
% plot(x,sin(x-2*pi*10));
% %figure(2), 
% %plot(x_p,u_p);



% close all;
% clear;
% r =linspace(-1,1,100);
% for N =0:6
%     plot(r,JacobiP(r,0,0,N)); hold on;
% end
% 
% [rp,wp] = JacobiGQ(0,0,N)
% 
% [rpL] = JacobiGL(0,0,N)
% 
% for N = 0:6
%     sum(wp.*JacobiP(rp,0,0,N).*JacobiP(rp,0,0,N))
% end
