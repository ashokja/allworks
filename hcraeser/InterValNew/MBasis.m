function X1 = MBasis(i,coeff,X,Y)

        n = max(size(coeff))-1;
        if i ==1
           X1 = M_evalX(n,coeff,X);
        else
            X1 =DM_evalX(n,coeff,X);
        end

    switch nargin
        case 4,
            data = [-1:.1:1];
            n = max(size(coeff))-1;
            for k = 0:n
                Y = M(n,k,data);
                plot(data,Y);hold on;
            end
            hold off;
            plot(data,M_eval(n,coeff,data));
    end
    function X1 = DM_evalX(n,coeff,X)
        X1 = zeros(size(X));
        for k = 1:n
            r = op('*',[k/2,k/2],op('*',[coeff(k+1),coeff(k+1)],MX(n,k-1,X)));
            X1 = X1+r;
        end
    end
    
    function X1 = M_evalX(n,coeff,X)
        X1 = zeros(size(X));
        for k = 0:n
            r = op('*',[coeff(k+1),coeff(k+1)],MX(n,k,X) );
            X1 = X1+r;
        end
    end
    
    function rest = M_eval(n,coeff,dataIn)
        rest = zeros(size(dataIn));
        for k = 0:n
            r = coeff(k+1)*M(n,k,dataIn);
            rest = rest+r;
        end
    end

    function rest = B_eval(n,coeff,dataIn)
       count =1;
       rest = zeros(size(dataIn));
       for k = 0:n
           r = coeff(count)*B(n,k,dataIn);
           rest = rest  + r;
           count= count +1;          
       end
    end
    
    function X1 = MX(n,k,data)
        X1 = M(n,k,data);
    end

    function [res] = M(n,k,data)
        if k<0
            res = zeros(size(data));
            return;
        end
        data = (data+1)/2;
        res = data.^k;
    end

    function [res] = B(n,k,data)
        % assumed data varies from -1 to 1
        if n<k || k<0
            res = zeros(size(data));
            return
        end
        data = (data+1)/2;
        res = nchoosek(n,k)*((data.^k).*((1-data).^(n-k)));
    end

   
    function [res] = B_der(n,k,data)
        res = n*(B(n-1,k-1,data) - B(n-1,k,data))/2;
    end
    
    function [rest] = B_der_eval(n1,coeff,dataIn)
       count =1;
       [a,b] = size(dataIn);
       rest = zeros(a,b);
       for k1 = 0:n1
           rest = rest + coeff(count)*B_der(n1,k1,dataIn);
           count= count +1;          
       end
    end
end