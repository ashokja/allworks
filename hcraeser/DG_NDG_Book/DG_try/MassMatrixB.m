function [M] = MassMatrixB(N)
    M = zeros(N,N);
%    [r,w,del] = lglnodes(N-1);
     [r,w] = JacobiGQ(0,0,N-1);
    r = (r+1)/2;
    for i = 0:N-1
        for j = 0:N-1
            temp =0.0;
            for rv = 1:N
                temp = temp + w(rv)*B_i(i,N-1,r(rv))*B_i(j,N-1,r(rv))/2;
            end
            M(i+1,j+1) = temp;
        end
    end
end
