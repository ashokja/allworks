% test naca for calculating Curvature across a plane.
clear;
close all;

%UsebkupData = 'bkup_SI_TOR/'
UsebkupData = ''
%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05'
filename = strcat('P0000009_So_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ACRVar' );
filepath = strcat('data/',UsebkupData,filename);
disp(filepath);


xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );
%torPfname =  strcat(filepath,'_pTor_p.txt' );
curSfname =  strcat(filepath,'_pC2_s.txt' );
curSSfname =  strcat(filepath,'_pC2_ss.txt' );
testSfname =  strcat(filepath,'_pVz_s.txt' );




xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');
%torPval = textread(torPfname, '%f\t');
curSval = textread(curSfname, '%f\t');
curSSval = textread(curSSfname, '%f\t');
testSval = textread(testSfname, '%f\t');

epos = floor(epos +0.0001);

s = floor( (max(size(xpos)))^(1/2) +0.1);

%upfname = strcat(filepath,'_pU_p.txt' );

xVpos = reshape(xpos,s,s);
yVpos = reshape(ypos,s,s);
zVpos = reshape(zpos,s,s);

%torPVval = reshape(torPval,s,s);
curSVval = reshape(curSval,s,s);
curSSVval = reshape(curSSval,s,s);
testSVval = reshape(testSval,s,s);

figure(2),
surf(yVpos,zVpos,(abs(curSVval)).^(1/2))
shading interp;
grid on;
grid minor

figure(3),
contourf(yVpos,zVpos,(abs(curSVval)).^(1/2),10)
contourcbar
title('Curvature Contour (1)');
print('Curvature_contour_t1','-dpng','-r300');


figure(4);
surf(yVpos,zVpos,(abs(curSSVval)).^(1/2))
shading interp;
colorbar;
grid on;
grid minor

figure(5);
contourf(yVpos,zVpos,(abs(curSSVval)).^(1/2),10)
contourcbar
title('Curvature Contour (2)');
print('Curvature_contour_t2','-dpng','-r300');


% figure(6);
% surf(yVpos,zVpos,testSVval)
% shading interp;
% colorbar;
% grid on;
% grid minor

%figure(7);
%contour(yVpos,zVpos,testSVval,[-1.1,-1.0,-0.8])
%contourcbar



