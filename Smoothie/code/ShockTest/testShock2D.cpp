#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"

#include <iomanip>

#define MAX_ITER 10000
#define MAX_ITER_NI 100
#define TOLERENCE_S 1e-16
#define TOLERENCE_F 1e-7
#define TOLERENCE_P 1e-14
#define TOLERENCE_A 1e-14
#define TOLERENCE_NN 1e-8
#define ALPHAMAX 1.0

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>

using namespace std;

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2st arg xml file." << endl;
		cout << "3nd arg 0/1"<<endl<<"\t 0 for writing data"<< endl<<"\t 1 for reading data." << endl;
		cout << "4nd arg Mesh resolution you want to write to." << endl;
		//cout << "3nd arg polynomial degree filter you want to apply" << endl;
		//cout << "4rd arg meshscaling you want to use." << endl;
	//	cout << "4th Angle of SIAC derivative in deg -90 to 90 allowed" << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();
	
	vector<string> var1;
	var1.push_back("ArtificialVisc");
	HNM2D->LoadData( fname+ "_1_small.fld",var1);


	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fPhys:" << tNquadPts<< endl;
	cout << "fCoeffs:" << HNM2D->m_expansions[0]->GetNcoeffs()<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}

	Array<OneD,NekDouble> u_DG = HNM2D->m_expansions[0]->GetPhys();	
    HNM2D->m_Arrays.push_back(u_DG);
	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();

    NekDouble valX,valY,valZ; 

	int Nres = atoi(argv[1+3]);
	int Nx = Nres, Ny = Nres;
	int totPts = Nx*Ny;
	NekDouble sx = 1.0/(Nx-1.0), sy= 1.0/(Ny-1.0);
	vector< NekDouble> Px(totPts), Py(totPts), U(totPts), V(totPts);	
	vector< NekDouble> SU(totPts), SV(totPts);	
	vector< NekDouble> SUDXY(totPts), SUXYD(totPts);	
	Array<OneD,NekDouble> coord(3,0.0);
    Array<OneD,NekDouble> direction(3,0.0);
    Array<OneD,NekDouble> directionX(3,0.0);  directionX[0] = 1.0;
    Array<OneD,NekDouble> directionY(3,0.0);  directionY[1]=1.0;
    Array<OneD,NekDouble> directionD(3,0.0); directionD[0] = 1/sqrt(2);directionD[1]= 1/sqrt(2);
	vector<Array<OneD,NekDouble>> directionXYD,directionDXY;
	directionXYD.push_back(directionX);
	directionXYD.push_back(directionY);
	directionXYD.push_back(directionD);
	directionDXY.push_back(directionD);
	directionDXY.push_back(directionY);
	directionDXY.push_back(directionY);
    direction[1]=1.0;
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp2, HNM2D, 2,0.1 );
	NekDouble startX = 0.024,startY=0.0;
	NekDouble endX = 0.16,endY=0.06;
    for(int i =0; i <Nx; i++)
	{
		for(int j=0; j< Ny; j++)
		{
			int index = i*Nx+j;
			Px[index] = startX + i*sx*(endX-startX); 
			Py[index] = startY + j*sy*(endY-startY); 
			// find the index for the point.
			// el_phys_array
			// Form the local expansion
			// at i,j
			coord[0] = Px[index];
			coord[1] = Py[index];
			//int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
            int elId = HNM2D->GetExpansionIndexUsingRTree(coord);
			int phys_offset = HNM2D->m_expansions[0]->GetPhys_Offset(elId);
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp( elId );
			Array<OneD,NekDouble> el_u_phys = u_DG.CreateWithOffset( u_DG, phys_offset );

			U[index] = lexp->PhysEvaluate( coord, el_u_phys);

            NekDouble dynScaling = HNM2D->GetDynamicScaling(coord);
    //        pDynLy[index] = dynScaling;
            //sm.EvaluateAt(Px[index],Py[index],0.0,SU[index],valY,valZ,direction, dynScaling ,0);
            sm.EvaluateAt(Px[index],Py[index],0.0,SUDXY[index],valY,valZ,directionDXY, dynScaling ,0);
            sm.EvaluateAt(Px[index],Py[index],0.0,SUXYD[index],valY,valZ,directionXYD, dynScaling ,0);
		}
	}

	HNM2D->writeNekArray(Px, fname+"_"+argv[1+3]+"_Sample_x.txt");	
	HNM2D->writeNekArray(Py, fname+"_"+argv[1+3]+"_Sample_y.txt");	
	HNM2D->writeNekArray(U, fname+"_"+argv[1+3]+"_Sample_u.txt");	
    HNM2D->writeNekArray(SUDXY, fname+"_"+argv[1+3]+"_Sample_suDXY.txt");	
    HNM2D->writeNekArray(SUXYD, fname+"_"+argv[1+3]+"_Sample_suXYD.txt");	
/*

    // Also write a fld file for the dG projection.
	string out = vSession->GetSessionName() + "TestDel.fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (HNM2D->m_expansions[0]->GetFieldDefinitions()).at(0);
    FieldDef[0]->m_fields.push_back("u");
    HNM2D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_fld->Write(out, FieldDef, FieldData);
*/
	return 0;
}
