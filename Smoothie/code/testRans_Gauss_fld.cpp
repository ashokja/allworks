#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include "OneSidedSIAC.h"
#include <omp.h>
#include <boost/foreach.hpp>
#include <iostream>
#include <iomanip> // std::setprecision
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
#define TOLERENCE_VIZ 1e-6

using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg conditions file." << endl;
		cout << "prefname to be used by all files." << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	
	BOOST_FOREACH(string &s, var)
	{
		HNM2D->LoadMesh(s);
	}
	//HNM2D->LoadMesh(var[4]);
	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	
	cout << "total Qpoints:\t"  << tNquadPts << endl;
	cout << "total Exp size:\t" << HNM2D->m_expansions[0]->GetExpSize()<< endl;

	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);

	HNM2D->m_Arrays.push_back(fce);
	HNM2D->LoadExpListIntoRTree();
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();


	for (int i =0; i < 1; i++)
	{
		string ptdFieldFile;
		string prefname = argv[3];
		switch (i){
			case 0:
				ptdFieldFile = prefname+"_u.dat";
				break;
			case 1:
				ptdFieldFile = prefname+"_v.dat";
				break;
			case 2:
				ptdFieldFile = prefname+"_p.dat";
				break;
			case 3:
				ptdFieldFile = prefname+"_rho.dat";
				break;
			case 4:
				ptdFieldFile = prefname+"_nut.dat";
				break;
			
		}
		cout << "Inputfile name:\t" << ptdFieldFile << endl;
		vector<NekDouble> ptArray;
		HNM2D->readNekArray(ptArray,ptdFieldFile );
		//HNM2D->printNekArray(ptArray);
		cout << "total Qpoints"  << tNquadPts << endl;
        cout << "Loaded ptArray" << ptArray.size() << endl;

        assert(tNquadPts+3 == ptArray.size() && "These arrays should match");

		for (int ii=0;  ii< tNquadPts; ii++)
		{
			fce[ii] = ptArray[3+ii];
		}
		HNM2D->m_expansions[i]->FwdTrans(fce,HNM2D->m_expansions[i]->UpdateCoeffs() );
		HNM2D->m_expansions[i]->BwdTrans( HNM2D->m_expansions[i]->GetCoeffs(),
											HNM2D->m_expansions[i]->UpdatePhys());
		Array<OneD,NekDouble> projPhys = HNM2D->m_expansions[i]->GetPhys();
		
		for (int i=0;  i< tNquadPts; i++)
		{
			if(std::abs( projPhys[i] - fce[i])>1e-13)
			{
				cout << "E :\t i = " << i ;
				cout << "\t projPhys = " << projPhys[i];
				cout << "\t fcePhys= " <<fce[i];
				cout << "\t Er= " <<fce[i]-projPhys[i]<< endl;
			}
		}
		cout << "Loaded sucessfully" << endl;
	}


  	string out = vSession->GetSessionName() + ".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (HNM2D->m_expansions[0]->GetFieldDefinitions()).at(0);
	BOOST_FOREACH(string &s, var)
	{
    	FieldDef[0]->m_fields.push_back(s);
	}
    HNM2D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0]);
   // HNM2D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0]);
   // HNM2D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0]);
   // HNM2D->m_expansions[3]->AppendFieldData(FieldDef[0], FieldData[0]);
   // HNM2D->m_expansions[4]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_fld->Write(out, FieldDef, FieldData);
	return 0;
}

