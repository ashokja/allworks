function TestBernDividedDiff
    degree = 4;
    h = @monomial;
    tknots= -(degree+1)/2:1: (degree+1)/2

    %tknots=ones(1,6)*2;
    %dividedDiff(1,3,tknots,0,h)
    i =1;
    j = max(size(tknots));
    count =1;
    x_list= zeros(1,61);
    v_list = zeros(1,61);
    for xx = (0:.1:6)-3
        x_list(count) = xx;
        v_list(count) = dividedDiff(i,j,tknots,xx,h,degree);
        count=count+1;
    end
    
    
    plot(x_list,v_list);
    function result = monomial(t,x,der,D)
        y = max(t-x,0);
        result = prod(D-der+1:D)*(y)^(D-der);
    end


end