close all;
clear;

v = VideoWriter('images/frames/SIAC_3D_Com.avi');
for i = -60:1:60
    open(v);
    A = imread(strcat('images/frames/vorticity_3D_SIAC_',int2str(i+10000 +60),'.png'));
%     imshow(A);
    writeVideo(v,A);
end
close(v);


v = VideoWriter('images/frames/LSIAC_3D_com.avi','Compressed AVI');
for i = -60:1:60
    open(v);
    A = imread(strcat('images/frames/vorticity_3D_LSIAC_',int2str(i+10000 +60),'.png'));
%     imshow(A);
    writeVideo(v,A);
end
close(v);


v = VideoWriter('images/frames1/SIAC_3D_Com.avi','Compressed AVI');
for i = -120:1:0
    open(v);
    A = imread(strcat('images/frames1/vorticity_3D_SIAC_',int2str(i+10000 +120),'.png'));
%     imshow(A);
    writeVideo(v,A);
end
close(v);


v = VideoWriter('images/frames1/LSIAC_3D_Com.avi','Compressed AVI');
for i = -120:1:0
    open(v);
    A = imread(strcat('images/frames1/vorticity_3D_LSIAC_',int2str(i+10000 +120),'.png'));
%     imshow(A);
    writeVideo(v,A);
end
close(v);



% figure(5)
% for i = -60:10:60
%     view(i,20)
%     disp(strcat('images/frames/vorticity_3D_LSIAC_',int2str(i+10000 +60)));
%     print(strcat('images/frames/vorticity_3D_LSIAC_',int2str(i+10000 +60)),'-dpng', '-r100');
% end