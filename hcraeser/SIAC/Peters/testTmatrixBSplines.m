% test position dependent and with General B-spline.
function testTmatrixBSplines()
%clear all;
%close all;
degree = 1;
j =degree;
% knots are given with one general B-Spline.

% Get a knot matrix.

% knot matrix for sym filter of given degree and j
tknots = -(degree+1)/2 - (degree+j)/2:1: (degree+1)/2+(degree+j)/2;
tk_size = max(size(tknots));
tmatrix = zeros(degree+j+1,degree+2);

for loop = 1:tk_size-degree-1
    tmatrix(loop,:) = tknots(loop:loop+degree+1);
end

tmatrix= tmatrix-max(max(tmatrix));
tmatrix(end+1,:) = zeros(1,degree+2);
tmatrix(end,1) = tmatrix(end-1,end-1);
% %tmatrix(end,3) = 0.1;
 disp(tmatrix);

[M0,c] = bulidM_0GiventMatrix(degree,tmatrix);

c(end) = 2*c(end);
%x_list for solution to be evaluated.
x_list = (min(min(tmatrix)):.001:max(max(tmatrix)));
v_list = zeros(size(x_list));

%points to calculate for central Bsplines.
for loop = 1:max(size(c))
%    delta = loop-degree-1;
    v_list = v_list+ c(loop)*CalBernUsingDividedDiff( degree,x_list,tmatrix(loop,:) );
end
plot(x_list,v_list); hold on;
disp('integral');
disp(sum(sum(v_list(:)))/max(size(v_list(:)))*abs(max(max(tknots))-min(min(tknots))));

disp(c')

end



