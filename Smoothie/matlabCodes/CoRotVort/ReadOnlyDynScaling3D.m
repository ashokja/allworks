% purpose to read dynamic scaling on vort5_mesh.

clear;
close all;
% test 
 % rates = [10,20,40,60,80];
 % rates = [20];

%abc = '../../data/UnsTriMeshes/NM2_unsMeshvar1_0125_2_R_100_pX_2DDyn.txt';

R = 100;
poly =2;
order = poly;
count =1;

prefixName = '../../data/bentVort_2/';
%prefixName = '../../data/vort5_mesh/';

width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
fsw = 20;
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize

Rp = R;

Dyn3D_s = zeros(R,R,R);

for fileIndex = 0:1:99
    i = fileIndex+1;
    FileName = strcat('vort5_Mesh');
    prefixFilename = strcat( prefixName,FileName,'_P_',int2str(poly),'_scaling_0.034_3D_i_',int2str(fileIndex),'_R_',int2str(R),'_DLISAC');    
   
    SsDynfname = strcat(prefixFilename,'_pS_sDyn.txt');
    SsDynpos = textread(SsDynfname, '%f\t');
   % SsDynpos = textread(SsDynfname,'%f\t',10000);
    SsDynVpos = reshape(SsDynpos,[R,R]);
    Dyn3D_s(i,:,:) = SsDynVpos;
    
    surf(SsDynVpos);
end
    
Dyn3D = Dyn3D_s;
va = 47.7;
vb = 37.2;
%%
Dyn3D_s = permute(Dyn3D,[1,3,2]);
%isovalue = 25;
isovalue = 10
figure(13)
 hz = slice(Dyn3D_s, [],80, []); hold on;
% hz.FaceColor = 'interp';
% hz.EdgeColor = 'none';
view(va,vb);
 hy = slice(Dyn3D_s, 40,[], []); hold on;
% hy.FaceColor = 'interp';
% hy.EdgeColor = 'none';

%slice(Dyn3D_s, [],100, []); hold on;

%slice(Dyn3D_s, [],[80,100], 1); hold on;


%slice(Dyn3D_s, [],[], 1); hold on;
%hz = slice(Dyn3D_s, [],1, []); hold on;
%slice(Dyn3D_s, [],(1:20),1); hold on;
%slice(Dyn3D_s, 1,(1:20),[]); hold on;
shading interp;
%h.DiffuseStrength = 0.8;

%colormap default
colormap (parula(15))
axis off;
colormap default;
colorbar;
            ax = gca;
            ax.FontSize = fsw;
print(strcat('images/',FileName,'_P_',int2str(poly),'_3D_Scaling_2'),'-dpng','-r300');

%colormap default;

%h.DiffuseStrength = 0.8;
%colorbar();
%slice(Dyn3D_s, [],[], 50); hold on;
%shading interp; colorbar();
%axis off;
%[c,h] = contourf(squeeze(Dyn3D_s(45,:,:))); hold on;
% contourslice(Dyn3D_s, [],50, [] ) ; hold on;
% hcs = contourslice(Dyn3D_s, [],50, [], [isovalue isovalue]) ; hold on;
% set(hcs,'EdgeColor','r');
% set(hcs,'linewidth',2);
% axis off;
% view([0,0]);
% caxis([0,50]);
