function [M0,c] = bulidM_0Sym_NonStandarad(deg,tknots)
    switch nargin
        case 0
            deg=2;
            tknots= -(deg+1)/2 - deg:1: (deg+1)/2+deg;
        case 1
            tknots= -(deg+1)/2 - deg:1: (deg+1)/2+deg;
    end

    %calculate J
    j = max(size(tknots))-2*deg-2;
    
    
    M0 = zeros(deg+j+1,deg+j+1);
    h = @monomial;
    %degree is given as deg
    for r = 1:deg+j+1
        for l = 1:deg+j+1
            delta = r-1;
            gamma = l;
            M0(r,l) = dividedDiff(gamma,gamma+deg+1,tknots,0.0,h,deg+delta+1);
        end
    end
    
    function result = monomial(t,x,der,D)
        y = t-x;
        result = prod(D-der+1:D)*(y)^(D-der);
    end

    e1 = zeros(deg+1+j,1);
    e1(1) =1;
    c = M0\e1;
end