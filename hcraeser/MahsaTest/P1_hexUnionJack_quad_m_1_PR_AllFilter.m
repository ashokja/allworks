% P1
% quadrature cal error.
% cart siac filter.
% hex union jack.
% PR

clear;
% Test errors.
Deg=1;
ord1 = 672;
ord2 = 2688;
ord3 = 10752;
ord4 = 43008;

L2B_ord(1) = 6.14e-3;
L2B_ord(2) = 1.54e-3;
L2B_ord(3) = 3.85e-4;
L2B_ord(4) = 9.63e-5;

LinfB_ord(1) = 4.72e-2;
LinfB_ord(2) = 1.19e-2;
LinfB_ord(3) = 2.97e-3;
LinfB_ord(4) = 7.44e-4;

orderB_L2(1) = 0;
orderB_L2(2) = log(L2B_ord(2)/L2B_ord(1))/log(ord1/ord2)*2;
orderB_L2(3) = log(L2B_ord(3)/L2B_ord(2))/log(ord2/ord3)*2;
orderB_L2(4) = log(L2B_ord(4)/L2B_ord(3))/log(ord3/ord4)*2;

orderB_Linf(1) = 0;
orderB_Linf(2) = log(LinfB_ord(2)/LinfB_ord(1))/log(ord1/ord2)*2;
orderB_Linf(3) = log(LinfB_ord(3)/LinfB_ord(2))/log(ord2/ord3)*2;
orderB_Linf(4) = log(LinfB_ord(4)/LinfB_ord(3))/log(ord3/ord4)*2;

orderB_L2
orderB_Linf

L2_ord(1) = 5.60e-3;
L2_ord(2) = 3.54e-4;
L2_ord(3) = 2.25e-5;
L2_ord(4) = 1.44e-6;

Linf_ord(1) = 1.22e-2;
Linf_ord(2) = 5.06e-4;
Linf_ord(3) = 3.22e-5;
Linf_ord(4) = 2.06e-6;


order_L2(1) = 0;
order_L2(2) = log(L2_ord(2)/L2_ord(1))/log(ord1/ord2)*2;
order_L2(3) = log(L2_ord(3)/L2_ord(2))/log(ord2/ord3)*2;
order_L2(4) = log(L2_ord(4)/L2_ord(3))/log(ord3/ord4)*2;

order_Linf(1) = 0;
order_Linf(2) = log(Linf_ord(2)/Linf_ord(1))/log(ord1/ord2)*2;
order_Linf(3) = log(Linf_ord(3)/Linf_ord(2))/log(ord2/ord3)*2;
order_Linf(4) = log(Linf_ord(4)/Linf_ord(3))/log(ord3/ord4)*2;

order_L2
order_Linf

% Hex(1) filter.

L2BH1_ord(1) = 6.14e-3;
L2BH1_ord(2) = 1.54e-3;
L2BH1_ord(3) = 3.85e-4;
L2BH1_ord(4) = 9.63e-5;

LinfBH1_ord(1) = 4.72e-2;
LinfBH1_ord(2) = 1.19e-2;
LinfBH1_ord(3) = 2.97e-3;
LinfBH1_ord(4) = 7.44e-4;



L2H1_ord(1) = 1.99e-3;
L2H1_ord(2) = 4.61e-5;
L2H1_ord(3) = 3.50e-6;
L2H1_ord(4) = 5.98e-7;

LinfH1_ord(1) = 2.99e-3;
LinfH1_ord(2) = 9.17e-5;
LinfH1_ord(3) = 9.48e-6;
LinfH1_ord(4) = 1.98e-6;


orderH1_L2(1) = 0;
orderH1_L2(2) = log(L2H1_ord(2)/L2H1_ord(1))/log(ord1/ord2)*2;
orderH1_L2(3) = log(L2H1_ord(3)/L2H1_ord(2))/log(ord2/ord3)*2;
orderH1_L2(4) = log(L2H1_ord(4)/L2H1_ord(3))/log(ord3/ord4)*2;

orderH1_Linf(1) = 0;
orderH1_Linf(2) = log(LinfH1_ord(2)/LinfH1_ord(1))/log(ord1/ord2)*2;
orderH1_Linf(3) = log(LinfH1_ord(3)/LinfH1_ord(2))/log(ord2/ord3)*2;
orderH1_Linf(4) = log(LinfH1_ord(4)/LinfH1_ord(3))/log(ord3/ord4)*2;

orderH1_L2
orderH1_Linf

% Hex2 filter.

L2BH2_ord(1) = 6.14e-3;
L2BH2_ord(2) = 1.54e-3;
L2BH2_ord(3) = 3.85e-4;
L2BH2_ord(4) = 9.63e-5;

LinfBH2_ord(1) = 4.72e-2;
LinfBH2_ord(2) = 1.19e-2;
LinfBH2_ord(3) = 2.97e-3;
LinfBH2_ord(4) = 7.44e-4;

L2H2_ord(1) = 2.57e-3;
L2H2_ord(2) = 6.60e-4;
L2H2_ord(3) = 1.66e-4;
L2H2_ord(4) = 4.16e-5;

LinfH2_ord(1) = 7.50e-3;
LinfH2_ord(2) = 1.92e-3;
LinfH2_ord(3) = 4.86e-4;
LinfH2_ord(4) = 1.22e-4;


orderH2_L2(1) = 0;
orderH2_L2(2) = log(L2H2_ord(2)/L2H2_ord(1))/log(ord1/ord2)*2;
orderH2_L2(3) = log(L2H2_ord(3)/L2H2_ord(2))/log(ord2/ord3)*2;
orderH2_L2(4) = log(L2H2_ord(4)/L2H2_ord(3))/log(ord3/ord4)*2;

orderH2_Linf(1) = 0;
orderH2_Linf(2) = log(LinfH2_ord(2)/LinfH2_ord(1))/log(ord1/ord2)*2;
orderH2_Linf(3) = log(LinfH2_ord(3)/LinfH2_ord(2))/log(ord2/ord3)*2;
orderH2_Linf(4) = log(LinfH2_ord(4)/LinfH2_ord(3))/log(ord3/ord4)*2;

orderH2_L2
orderH2_Linf




line0= '\caption{proj solution, m = 1.0, HL = 2*L, Hex union, All SIAC filter, L2 Error.}';

% write code to print L2
line1= strcat('\label{table:4}',...
 '\begin{tabular}{||c| c|c| c| c| c| c| c | c|c ||}',...
' \hline ');
line2 = ' Mesh Ele & Deg &L2(Before)& order & L2(Cart) & order& L2(Hex(1)) & order& L2(Hex(2)) & order \\ [0.5ex] ';
line3 = ' \hline\hline ';
% line 1. 
line4 = strcat('$',num2str(ord1),'$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord(1),'%1.2e'), '$ & $',...
    num2str(orderB_L2(1)), '$ & $',...
    num2str(L2_ord(1),'%1.2e'), '$ & $',...
    num2str(order_L2(1)), '$ & $',...
    num2str(L2H1_ord(1),'%1.2e'), '$ & $',...
    num2str(orderH1_L2(1)), '$ & $',...
    num2str(L2H2_ord(1),'%1.2e'), '$ & $',...
    num2str(orderH2_L2(1)), '$ ',...
    '\\');
line5 = ' \hline ';
% line 2.
line6 = strcat('$',num2str(ord2),'$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord(2),'%1.2e'), '$ & $',...
       num2str(orderB_L2(2)), '$ & $',...
    num2str(L2_ord(2),'%1.2e'), '$ & $',...
    num2str(order_L2(2)), '$ & $',...
    num2str(L2H1_ord(2),'%1.2e'), '$ & $',...
    num2str(orderH1_L2(2)), '$ & $',...
    num2str(L2H2_ord(2),'%1.2e'), '$ & $',...
    num2str(orderH2_L2(2)), '$ ',...
    '\\');
line7 = ' \hline ';

line8 = strcat('$',num2str(ord3),'$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord(3),'%1.2e'), '$ & $',...
         num2str(orderB_L2(3)), '$ & $',...
    num2str(L2_ord(3),'%1.2e'), '$ & $',...
    num2str(order_L2(3)), '$ & $',...
    num2str(L2H1_ord(3),'%1.2e'), '$ & $',...
    num2str(orderH1_L2(3)), '$ & $',...
    num2str(L2H2_ord(3),'%1.2e'), '$ & $',...
    num2str(orderH2_L2(3)), '$ ',...
    '\\');
line9 = ' \hline ';

line10 = strcat('$',num2str(ord4),'$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord(4),'%1.2e'), '$ & $',...
         num2str(orderB_L2(4)), '$ & $',...
    num2str(L2_ord(4),'%1.2e'), '$ & $',...
    num2str(order_L2(4)), '$ & $',...
    num2str(L2H1_ord(4),'%1.2e'), '$ & $',...
    num2str(orderH1_L2(4)), '$ & $',...
    num2str(L2H2_ord(4),'%1.2e'), '$ & $',...
    num2str(orderH2_L2(4)), '$ ',...
    '\\');
line11= ' \hline \end{tabular} ';


fileID = fopen('P1_hexUnionJack_quad_m_1_PR_All_L2.txt','w');
fwrite(fileID,line0);
fwrite(fileID,line1);
fwrite(fileID,line2);
fwrite(fileID,line3);
fwrite(fileID,line4);
fwrite(fileID,line5);
fwrite(fileID,line6);
fwrite(fileID,line7);
fwrite(fileID,line8);
fwrite(fileID,line9);
fwrite(fileID,line10);
fwrite(fileID,line11);
fclose(fileID);



% write code to print Linf
line0= '\caption{proj solution, m = 1.0, HL = 2*L, Hex union, All SIAC filter, Linf Error.}';

line1= strcat('\label{table:4}',...
 '\begin{tabular}{||c| c| c| c| c| c| c| c | c|c ||}',...
' \hline ');
line2 = ' Mesh Ele & Deg & Linf(Before)& order & Linf(Cart) & order& Linf(Hex(1)) & order& Linf(Hex(2)) & order \\ [0.5ex] ';
line3 = ' \hline\hline ';
% line 1. 
line4 = strcat('$',num2str(ord1),'$ & $',num2str(Deg),'$ & $',...
     num2str(LinfB_ord(1),'%1.2e'), '$ & $',...
    num2str(orderB_Linf(1)), '$ & $',...
    num2str(Linf_ord(1),'%1.2e'), '$ & $',...
    num2str(order_Linf(1)), '$ & $',...
    num2str(LinfH1_ord(1),'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(1)), '$ & $',...
    num2str(LinfH2_ord(1),'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(1)), '$ ',...
    '\\');
line5 = ' \hline ';
% line 2.
line6 = strcat('$',num2str(ord2),'$ & $',num2str(Deg),'$ & $',...
     num2str(LinfB_ord(2),'%1.2e'), '$ & $',...
     num2str(orderB_Linf(2)), '$ & $',...
    num2str(Linf_ord(2),'%1.2e'), '$ & $',...
    num2str(order_Linf(2)), '$ & $',...
    num2str(LinfH1_ord(2),'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(2)), '$ & $',...
    num2str(LinfH2_ord(2),'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(2)), '$ ',...
    '\\');
line7 = ' \hline ';

line8 = strcat('$',num2str(ord3),'$ & $',num2str(Deg),'$ & $',...
     num2str(LinfB_ord(3),'%1.2e'), '$ & $',...
     num2str(orderB_Linf(3)), '$ & $',...
    num2str(Linf_ord(3),'%1.2e'), '$ & $',...
    num2str(order_Linf(3)), '$ & $',...
    num2str(LinfH1_ord(3),'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(3)), '$ & $',...
    num2str(LinfH2_ord(3),'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(3)), '$ ',...
    '\\');
line9 = ' \hline ';

line10 = strcat('$',num2str(ord4),'$ & $',num2str(Deg),'$ & $',...
     num2str(LinfB_ord(4),'%1.2e'), '$ & $',...
     num2str(orderB_Linf(4)), '$ & $',...
    num2str(Linf_ord(4),'%1.2e'), '$ & $',...
    num2str(order_Linf(4)), '$ & $',...
    num2str(LinfH1_ord(4),'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(4)), '$ & $',...
    num2str(LinfH2_ord(4),'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(4)), '$ ',...
    '\\');
line11= ' \hline \end{tabular} ';


fileID = fopen('P1_hexUnionJack_quad_m_1_PR_All_Linf.txt','w');
fwrite(fileID,line0);
fwrite(fileID,line1);
fwrite(fileID,line2);
fwrite(fileID,line3);
fwrite(fileID,line4);
fwrite(fileID,line5);
fwrite(fileID,line6);
fwrite(fileID,line7);
fwrite(fileID,line8);
fwrite(fileID,line9);
fwrite(fileID,line10);
fwrite(fileID,line11);
fclose(fileID);

