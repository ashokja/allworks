function rest = B_eval(coeff,dataIn)
    count =1;
    rest = zeros(size(dataIn));
    n = max(size(coeff))-1;
    for k = 0:n
        r = coeff(count)*B(n,k,dataIn);
        rest = rest  + r;
        count= count +1;
    end
end
