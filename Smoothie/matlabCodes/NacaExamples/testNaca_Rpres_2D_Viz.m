% testNaca_Rpre.cpp evaluate.


clear;
close all;


UsebkupData = 'cur_feb12/' % has only data Ux_ss so remember to comment below also.

%UsebkupData = 'cur_feb10/'

%UsebkupData = ''
%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05'
filename = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ACRRVar' );
filepath = strcat('data/',UsebkupData,filename);

xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );

presfname = strcat(filepath,'_Pre_s.txt' );
pressfname = strcat(filepath,'_Pre_ss.txt' );

% Uy_sfname = strcat(filepath,'_pUy_s.txt' );
% Uy_ssfname = strcat(filepath,'_pUy_ss.txt' );
Uy_ssfname = strcat(filepath,'_pUx_ss.txt' );


% Uy_ssxfname = strcat(filepath,'_pUy_ssx.txt' );
% Uy_ssyfname = strcat(filepath,'_pUy_ssy.txt' );
% Uy_sszfname = strcat(filepath,'_pUy_ssz.txt' );


xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');

prespos = textread(presfname, '%f\t');
presspos = textread(pressfname, '%f\t');

% Uy_spos = textread(Uy_sfname, '%f\t');
Uy_sspos = textread(Uy_ssfname, '%f\t');
% Uy_ssxpos = textread(Uy_ssxfname, '%f\t');
% Uy_ssypos = textread(Uy_ssyfname, '%f\t');
% Uy_sszpos = textread(Uy_sszfname, '%f\t');



epos = floor(epos +0.0001);
s = floor( (max(size(xpos)))^(1/2) +0.1);
    
%xVpos = reshape(xpos,s,s,s);
%yVpos = reshape(ypos,s,s,s);
%zVpos = reshape(zpos,s,s,s);
%eVpos = reshape(epos,s,s,s);

    xVpos = reshape(xpos,s,s);
%    xVpos = permute(xVpos,[2,3,1]);
    yVpos = reshape(ypos,s,s);
%    yVpos = permute(yVpos,[2,3,1]);
    zVpos = reshape(zpos,s,s);
%    zVpos = permute(zVpos,[2,3,1]);


presVpos = reshape (prespos,s,s);
pressVpos = reshape (presspos,s,s);

% Uy_sVpos = reshape (Uy_spos,s,s);
Uy_ssVpos = reshape (Uy_sspos,s,s);
% Uy_ssxVpos = reshape (Uy_ssxpos,s,s);
% Uy_ssyVpos = reshape (Uy_ssypos,s,s);
% Uy_sszVpos = reshape (Uy_sszpos,s,s);

% 
% figure(1);
% surf(yVpos,zVpos,presVpos)
% shading interp;
% title('presssure s')
% figure(2);
% surf(yVpos,zVpos,pressVpos)
% shading interp;
% title('presssure ss')
% %%
% figure(3);
% contour(yVpos,zVpos,presVpos);
% title('presssure s')
% 
% figure(4);
% contour(yVpos,zVpos,pressVpos);
% title('presssure ss')
% 
% figure(5);
% surf(yVpos,zVpos,presVpos- pressVpos);
% 
% Uy_ssVpos = Uy_ssyVpos;
% 
% figure(1);
% surf(yVpos,zVpos,Uy_sVpos)
% shading interp;
% title('Uy')
% figure(2);
% surf(yVpos,zVpos,Uy_ssVpos)
% shading interp;
% title('Uy ss')
% %%
% figure(3);
% contour(yVpos,zVpos,Uy_sVpos,linspace(-6,6,10));
% title('Uy_ s')

figure(4);
contour(yVpos,zVpos,Uy_ssVpos,10);
title('Uy ss')

figure(5);
surf(yVpos,zVpos,Uy_ssVpos);
shading interp;
title('Uy ss')

