function [] = drawStreamLine(mapxy,mapxyV)
    [a,~] = size(mapxy);
    b = int64(a^(1/3));
    x = reshape(mapxy(:,1),b,b,b);
    y = reshape(mapxy(:,2),b,b,b);
    z = reshape(mapxy(:,3),b,b,b);
    u = reshape(mapxyV(:,1),b,b,b);
    v = reshape(mapxyV(:,2),b,b,b);
    w = reshape(mapxyV(:,3),b,b,b);
    xmin = min(x(:)); xmax = max(x(:));
    ymin = min(y(:)); ymax = max(y(:));
    zmin = min(z(:)); zmax = max(z(:));
    
    zmin = min(z(:));
    wind_speed = sqrt(u.^2 + v.^2 + w.^2);
    hsurfaces = slice(x,y,z,wind_speed,[xmin,0,xmax],[ymin,0,ymax],[zmin,0,zmax]);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    colormap jet
    
    hcont = ...
    contourslice(x,y,z,wind_speed,[xmin,0,xmax],[ymin,0,ymax],[zmin,0,zmax]);
    set(hcont,'EdgeColor',[0.2 0.2 0.2],'LineWidth',0.5)
    
    startx = (-1:.5:1);
    startz = (-1:.5:1);
    starty = -1;
    [sx,sy,sz] = meshgrid(startx,starty,startz);
    colorbar;

    hlines = streamline(x,y,z,u,v,w,sx(:),sy(:),sz(:));
    set(hlines,'LineWidth',2,'Color','cyan');
    hold on;
    plot3(sx(:),sy(:),sz(:),'r*'); hold on;
    xlabel('x');
    ylabel('y');
    zlabel('z');
end