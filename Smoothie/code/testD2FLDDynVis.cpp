#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg conditions xml file." << endl;
		cout << "3rd arg polynomial degree filter you want to apply" << endl;
		cout << "4th arg Resolution of output." << endl;
		return 0;
	}

	// load conditions file also. CHANGE 2
	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();
	string fldname = fname+".fld";
	
	HNM2D->LoadData(fldname,var);
	
	Array<OneD,NekDouble> ece;
//	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	LibUtilities::EquationSharedPtr ffunc
			= vSession->GetFunction("ExactSolution", 0); 
	//ffunc->Evaluate(, fce);
	ece = HNM2D->m_expansions[0]->GetPhys();
	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();
	NekDouble meshScaling = HNM2D->GetMeshLargestEdgeLength();

	cout << "Till Get mesh largest edge length" << endl;
	int gPts = atoi(argv[4])+1;
	int totPts = gPts*gPts;
	int Nx = gPts, Ny=gPts;
	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0);
	
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[3]), meshScaling ); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	Array<OneD,NekDouble> direction(3,0.0), coord(3,0.0);
	direction[0] = 1.0;
	Array<OneD,NekDouble> pX(totPts), pY(totPts), pV(totPts), pP(totPts), pS(totPts),pDyn(totPts),pSDyn(totPts),pE(totPts);
	Array<OneD,NekDouble> glCoords(3,0.0);
	for( int i =0; i < Nx; i++)
	{
		for(int j=0; j< Ny; j++)
		{
			int index = i*Nx+j;
			pX[index] = i*sx;
			pY[index] = j*sy;
			pV[index] = ffunc->Evaluate(pX[index],pY[index],0.0,0.5); 
			//pV[index] = std::cos(2.0*2.0*M_PI*(pX[index]))*std::cos(2.0*2.0*M_PI*(pY[index]));
			glCoords[0] = pX[index]; glCoords[1] = pY[index];
			NekDouble dynScaling = HNM2D->GetDynamicScaling(glCoords);
			pDyn[index] = dynScaling;
			sm.EvaluateAt(pX[index],pY[index],0.0,pS[index],valY,valZ,direction, meshScaling,0);
			sm.EvaluateAt(pX[index],pY[index],0.0,pSDyn[index],valY,valZ,direction, dynScaling ,0);
			coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = 0.0;
//			cout << setprecision(19) << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;
			int elid = HNM2D->GetExpansionIndexUsingRTree(coord);
//			cout << elid << endl;
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
			//int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
			const Array<OneD,NekDouble> el_phys = ece.CreateWithOffset(
										ece, coeffOffset);
			pP[index] = lexp->PhysEvaluate(coord,el_phys);
			pE[index] = elid;
//			cout << j << endl;
		}
		cout << i << endl;
	}
	NektarBaseClass k;
	k.writeNekArray(pX,fname+"_R_"+argv[4]+"_pX_2DDyn.txt");
	k.writeNekArray(pY,fname+"_R_"+argv[4]+"_pY_2DDyn.txt");
	k.writeNekArray(pE,fname+"_R_"+argv[4]+"_pE_2DDyn.txt");
	k.writeNekArray(pV,fname+"_R_"+argv[4]+"_pV_2DDyn.txt");
	k.writeNekArray(pP,fname+"_R_"+argv[4]+"_pP_2DDyn.txt");
	k.writeNekArray(pS,fname+"_R_"+argv[4]+"_pS_2DDyn.txt");
	k.writeNekArray(pSDyn,fname+"_R_"+argv[4]+"_pSDyn_2DDyn.txt");
	k.writeNekArray(pDyn,fname+"_R_"+argv[4]+"_pDyn_2DDyn.txt");

	cout << "Done writing plane" << endl;
	
	//plane completed.
	// Evaluating on line y =0;
	int gPtsLy = atoi(argv[4])*10+1;
	int totPtsLy = gPtsLy;
	sx = sx/10.0;
	Array<OneD,NekDouble> pLy(totPtsLy),pLNx(totPtsLy), pLPx(totPtsLy), pELy(totPtsLy);
	Array<OneD,NekDouble> pXLy(totPtsLy), pYLy(totPtsLy), pVLy(totPtsLy), pPLy(totPtsLy), pSLy(totPtsLy),pDynLy(totPtsLy),pSDynLy(totPtsLy);
	for (int i =0 ; i< gPtsLy; i++)
	{
		int index = i;
		pXLy[index] = i*sx;
		pYLy[index] = 0.5;
			//pVLy[index] = std::cos(2.0*(pXLy[index] + pYLy[index]) );
			pVLy[index] = ffunc->Evaluate(pXLy[index],pYLy[index],0.0,0.5); 
			//pV[index] = std::cos(2.0*2.0*M_PI*(pX[index]))*std::cos(2.0*2.0*M_PI*(pY[index]));
			glCoords[0] = pXLy[index]; glCoords[1] = pYLy[index];
			NekDouble dynScaling = HNM2D->GetDynamicScaling(glCoords);
			pDynLy[index] = dynScaling;
			sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSLy[index],valY,valZ,direction, meshScaling,0);
			sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSDynLy[index],valY,valZ,direction, dynScaling ,0);
			coord[0] = pXLy[index]; coord[1] = pYLy[index]; coord[2] = 0.0;
//			cout << setprecision(19) << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;
			int elid = HNM2D->GetExpansionIndexUsingRTree(coord);
//			cout << elid << endl;
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
			//int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
			const Array<OneD,NekDouble> el_phys = ece.CreateWithOffset(
										ece, coeffOffset);
			pPLy[index] = lexp->PhysEvaluate(coord,el_phys);
			pELy[index] = elid;
//			cout << j << endl;
	}



	cout << "Done writing Line" << endl;
	k.writeNekArray(pXLy,fname+"_R_"+argv[4]+"_pXLy_2DDyn.txt");
	k.writeNekArray(pYLy,fname+"_R_"+argv[4]+"_pYLy_2DDyn.txt");
	k.writeNekArray(pELy,fname+"_R_"+argv[4]+"_pELy_2DDyn.txt");
	k.writeNekArray(pVLy,fname+"_R_"+argv[4]+"_pVLy_2DDyn.txt");
	k.writeNekArray(pPLy,fname+"_R_"+argv[4]+"_pPLy_2DDyn.txt");
	k.writeNekArray(pSLy,fname+"_R_"+argv[4]+"_pSLy_2DDyn.txt");
	k.writeNekArray(pSDynLy,fname+"_R_"+argv[4]+"_pSDynLy_2DDyn.txt");
	k.writeNekArray(pDynLy,fname+"_R_"+argv[4]+"_pDynLy_2DDyn.txt");

	// Evaluate on line x =0.5; x = -0.5;
	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
