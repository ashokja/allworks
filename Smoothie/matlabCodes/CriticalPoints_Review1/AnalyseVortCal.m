% Make plot and legend.

%Calcualte V
clear all;
close all;

Er_L2 = zeros(3,5,2);
Er_Linf = zeros(3,5,2);
% 3 = res 20,40,80; 5 scalings, 0.5,0.75,1.0,1.5,2.0; 2 types DG and
% SIAC.
%scaling 1.0(3)
Er_L2(1,3,1) = 5.00929;
Er_Linf(1,3,1) = 31.049;
Er_L2(1,3,2) = 0.152808;
Er_Linf(1,3,2) = 0.700531;

Er_L2(2,3,1) = 2.50611;
Er_Linf(2,3,1) = 15.9363;
Er_L2(2,3,2) = 0.0391236;
Er_Linf(2,3,2) = 0.18678;

Er_L2(3,3,1) = 1.25324;
Er_Linf(3,3,1) = 8.07275;
Er_L2(3,3,2) = 0.00991044;
Er_Linf(3,3,2) = 0.0482843;

%scaling 2.0(5)
Er_L2(1,5,1) = 5.00929;
Er_Linf(1,5,1) = 31.049;
Er_L2(1,5,2) = 0.165848;
Er_Linf(1,5,2) = 0.779571;

Er_L2(2,5,1) = 2.50611;
Er_Linf(2,5,1) = 15.9363;
Er_L2(2,5,2) = 0.0392732;
Er_Linf(2,5,2) = 0.191746;

Er_L2(3,5,1) = 1.25324;
Er_Linf(3,5,1) = 8.07275;
Er_L2(3,5,2) = 0.00991006;
Er_Linf(3,5,2) = 0.0485955;

%scaling 0.75 (2)
Er_L2(1,2,1) = 5.00929;
Er_Linf(1,2,1) = 31.049;
Er_L2(1,2,2) = 0.154251;
Er_Linf(1,2,2) = 0.755885;

Er_L2(2,2,1) = 2.50611;
Er_Linf(2,2,1) = 15.9363;
Er_L2(2,2,2) = 0.0392238;
Er_Linf(2,2,2) = 0.202809;

Er_L2(3,2,1) = 1.25324;
Er_Linf(3,2,1) = 8.07275;
Er_L2(3,2,2) = 0.00990902;
Er_Linf(3,2,2) = 0.052531;

%scaling 0.5 (1)
Er_L2(1,1,1) = 5.00929;
Er_Linf(1,1,1) = 31.049;
Er_L2(1,1,2) = 0.156586;
Er_Linf(1,1,2) = 0.780249;

Er_L2(2,1,1) = 2.50611;
Er_Linf(2,1,1) = 15.9363;
Er_L2(2,1,2) = 0.0398268;
Er_Linf(2,1,2) = 0.20932;

Er_L2(3,1,1) = 1.25324;
Er_Linf(3,1,1) = 8.07275;
Er_L2(3,1,2) = 0.0100638;
Er_Linf(3,1,2) =0.0542007;

%scaling 1.5 (4)
Er_L2(1,4,1) = 5.00929;
Er_Linf(1,4,1) = 31.049;
Er_L2(1,4,2) = 0.15404;
Er_Linf(1,4,2) = 0.718863;

Er_L2(2,4,1) = 2.50611;
Er_Linf(2,4,1) = 15.9363;
Er_L2(2,4,2) = 0.0392667;
Er_Linf(2,4,2) = 0.187282;

Er_L2(3,4,1) = 1.25324;
Er_Linf(3,4,1) = 8.07275;
Er_L2(3,4,2) = 0.00994757;
Er_Linf(3,4,2) =0.048149;

figure(1)
plot([0.5,0.75,1.0,1.5,2.0], (Er_L2(1,:,1)),'--' ); hold on;
plot([0.5,0.75,1.0,1.5,2.0], (Er_L2(2,:,1)),'--' ); hold on;
plot([0.5,0.75,1.0,1.5,2.0], (Er_L2(3,:,1)),'--' ); hold on;
plot( [0.5,0.75,1.0,1.5,2.0], (Er_L2(1,:,2)),'-*' ); hold on;
plot( [0.5,0.75,1.0,1.5,2.0], (Er_L2(2,:,2)),'-*' ); hold on;
plot([0.5,0.75,1.0,1.5,2.0], (Er_L2(3,:,2)),'-*' ); hold on;

resol_text = [0,20,40,80];
resol_data = [0.5,0.75,1.0,1.5,2.0];
DG_ERR = [0,Er_L2(:,1,1)'];
dataL2 = [resol_text;DG_ERR;resol_data',Er_L2(:,:,2)'];
input.data = dataL2;
%%
latex = latexTable(input);
%disp(latex);
DG_ERR_Linf = [0,Er_Linf(:,1,1)'];
dataLinf = [resol_text;DG_ERR_Linf;resol_data',Er_Linf(:,:,2)'];
input.data = dataLinf;
%%
latexTable(input);


% inputVal.data = [saveValErNm];
% % given a seed point
% 
% latex = latexTable(inputVal);


