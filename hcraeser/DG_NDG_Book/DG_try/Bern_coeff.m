function [u_coeff] = Bern_coeff(data)
    [N,del] = size(data');

    M = zeros(N,N);
    [r,w,del] = lglnodes(N-1);
    
    r = (r+1)/2;
    r = flipud(r);
    for i = 0:N-1
        for j = 0:N-1
            temp =0.0;
            for rv = 1:N
                temp = temp + w(rv)*B_i(i,N-1,r(rv))*B_i(j,N-1,r(rv));
            end
            M(i+1,j+1) = temp;
        end
    end
    
    % Calculated Mass matrix.
    f = zeros(N,1);
    for i = 0:N-1;
        temp =0.0;
        for rv = 1:N
            temp = temp + w(rv)*B_i(i,N-1,r(rv))* data(rv);
        end
        f(i+1) = temp;
    end
    u_coeff = M\f;

end