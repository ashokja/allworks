% test Pyramid algorithm B-SPline Curves algorithm.
clear;
close all;

deg = 3;
seg = 1;
TOLERENCE = 1e-10;
MC = 1;


%knots = [1:2*deg+seg-1];
%cpts = [1:deg+seg];
knots = [0,0,0,0,1,1,1,1]
%knots = [0,0,0,1,1,1];
cpts = [1,2,1.5,0];

% pick an u.

u_sel = (knots(deg)+0.01:.01:knots(end-deg+1)-0.01);

u = 3.5; % will be run for all properly.
for index = 1:size(u_sel(:))
    u = u_sel(index);
% find k
for uk = 1:size(knots(:))
    if ( knots(uk) > u )
        k = uk-1;
        break;
    end
end

% add a check to see if k is in the right range.



% finding multiplicity
% find multiplicity.
count = 0;
if ( abs(knots(k) - u) < TOLERENCE)
    for kc = k-1:-1:1
        if ( abs(knots(kc) - u) < TOLERENCE )
            count = count +1;
        else
            break;
        end
    end
end
s = count;

% now actual calculations.
h = deg-s;
Ef_cps(k-deg+MC:k-s+MC,1) = cpts( k-deg+MC:k-s+MC);


for r = 1:h
    for i = k-deg+r:k-s
%        disp([r,i]);
        a(r,i) = ( u - knots(i) )/(knots(i+deg-r+1) - knots(i) );
        Ef_cps(i+MC,r+1) = (1-a(r,i))* Ef_cps(i-1+MC,r) + a(r,i)* Ef_cps(i+MC,r);
    end
end

%disp(a');
%disp(Ef_cps);
V_sel(index) = Ef_cps(deg+1,deg+1);
end
plot(u_sel,V_sel);
%plot(





