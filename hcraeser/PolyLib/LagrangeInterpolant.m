
function y = LagrangeInterpolant(x,npts,xpts,funcvals)

sum = zeros(size(x));

for i=1:npts,
  sum = sum + funcvals(i)*LagrangePoly(x,i,npts,xpts);
end

y=sum;