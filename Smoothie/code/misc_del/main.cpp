#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC1D.h"
#include "HandleNekMesh.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
int main()
{
	cout << "Ashok Jallepalli"<< endl;
	
	Array<OneD,NekDouble> knots(4);
	knots[0] = 0.0;
	knots[1] = 1.0;
	knots[2] = 2.0;
	knots[3] = 3.0;

	GeneralBSplines gbs(knots, 3); 
	Array<OneD,NekDouble> x_pos(31,0.0), t_val(31,0.0);
	for(int i =0; i < x_pos.num_elements();i++)
	{
		x_pos[i] = i/10.0;
	}
/*
	gbs.EvaluateBSplines(x_pos, 0,t_val);
//	printNekArray(x_pos,0);
//	printNekArray(t_val,0);
	
	CentralBSplines cbs(2);	
	cbs.EvaluateBSplines(x_pos, 0,t_val);
	//cbs.EvaluateBSplines(x_pos,0,t_val);
	printNekArray(x_pos,0);
	cout << t_val.num_elements()<<endl;
	cout << cbs.m_knotVector.num_elements()<<endl;
	cout << cbs.m_order<< endl;	
	printNekArray(cbs.m_knotVector,0);
	printNekArray(t_val,0);
	cout <<t_val[30]<< endl;
	SymmetricSIAC sym_siac(2);
	cout << "m_R\t:"<< sym_siac.m_R	 << endl;			
	cout << "M_order\t:"<< sym_siac.m_order <<endl;
	cout << "m_der\t"<<sym_siac.m_nthDer<<endl;
	cout << "m_coeffs"<<sym_siac.m_coeffs.num_elements()<< endl;
	printNekArray(sym_siac.m_coeffs,0);
	sym_siac.EvaluateFilter( x_pos, t_val);
	printNekArray(t_val,0);
*/
	HandleNekMesh* mshHandle; 
	SmoothieSIAC1D( eSYM_2kp1,mshHandle, 2);
}
