/// This test file will simulate any of 9 single derivatives depending on input parameters.

#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"
//#include <ctime>



using namespace SIACUtilities;
using namespace std;

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		cout << "4th which parameter do you want to simulate select 1-9." << endl;
		return 0;
	}
	// This is so that vSession does not interpret extra parameters absurdly.
	argc = 2;

	clock_t startTime = clock();
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);

	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	//HNM3D->LoadMesh(var[0]);
	//HNM3D->LoadMesh(var[1]);
	//HNM3D->LoadMesh(var[2]);
	
	cout << "loading xml file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	string fname = vSession->GetSessionName();

	//string fldname = fname + ".fld/Info.xml";	
	//string fldname = "naca.fld";	
	string fldname = fname + ".fld";	
	cout << fname << endl;
	cout << fldname << endl;
	cout << fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pU_sx_phys.txt" << endl;

	HNM3D->LoadData( fldname ,var);
	cout << "loading fld file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	cout << "expsize:\t"<< HNM3D->m_expansions[0]->GetExpSize() << endl;
	
	HNM3D->LoadExpListIntoRTree();
	cout << "loading into R-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	/*
	for (int i =0; i< HNM3D->m_expansions[0]->GetExpSize();i++)
	{
		int gid = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetGlobalID();
		cout << "i\t" << i << "\tg\t" << gid << endl;
		cout << HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetShapeType()<< endl;
	}
	*/
	// Find minx,miny,minz	
	// Find maxx,maxy,maxz	
	cout << HNM3D->m_expansions[0]->GetTotPoints() << endl;
	cout << HNM3D->m_expansions[0]->GetNcoeffs() << endl;
	
	int totPhys = HNM3D->m_expansions[0]->GetTotPoints();
	Array<OneD,NekDouble> xc0(totPhys),xc1(totPhys), xc2(totPhys);
	
	HNM3D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
	NekDouble minx = Vmath::Vmin(totPhys,xc0,1);	
	NekDouble miny = Vmath::Vmin(totPhys,xc1,1);	
	NekDouble minz = Vmath::Vmin(totPhys,xc2,1);	
	NekDouble maxx = Vmath::Vmax(totPhys,xc0,1);	
	NekDouble maxy = Vmath::Vmax(totPhys,xc1,1);	
	NekDouble maxz = Vmath::Vmax(totPhys,xc2,1);	

	cout << "min\t " << minx << "\t" << miny<<"\t"<< minz<< endl; 
	cout << "max\t " << maxx << "\t" << maxy<<"\t"<< maxz<< endl; 
	
	const Array<OneD,NekDouble> u_phys = HNM3D->m_expansions[0]->GetPhys();
	const Array<OneD,NekDouble> v_phys = HNM3D->m_expansions[1]->GetPhys();
	const Array<OneD,NekDouble> w_phys = HNM3D->m_expansions[2]->GetPhys();
	const Array<OneD,NekDouble> p_phys = HNM3D->m_expansions[3]->GetPhys();

	Array<OneD,NekDouble> u_sx_phys(totPhys), v_sx_phys(totPhys), w_sx_phys(totPhys);
	Array<OneD,NekDouble> u_sy_phys(totPhys), v_sy_phys(totPhys), w_sy_phys(totPhys);
	Array<OneD,NekDouble> u_sz_phys(totPhys), v_sz_phys(totPhys), w_sz_phys(totPhys);

	
	cout << "Time taken to calculate 3 derivatives. "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
			
	//SmoothieSIAC3D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]),atof(argv[3]) ); 
	//SmoothieSIAC3D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]),atof(argv[3]),1); 
	
	NekDouble valY,valZ, scaling;
	scaling = atof(argv[3]);
	Array<OneD,NekDouble> directionX(3,0.0),directionY(3,0.0),directionZ(3,0.0);
	directionX[0] = 1.0;	directionY[1] = 1.0;	directionZ[2] = 1.0;
	
	vector<Array<OneD,NekDouble> > directionsXYZ,directionsYXZ, directionsXZY,directionsZXY;
    directionsXYZ.push_back(directionX);
    directionsXYZ.push_back(directionY);    
    directionsXYZ.push_back(directionZ);    
    directionsYXZ.push_back(directionY);    
    directionsYXZ.push_back(directionX);    
    directionsYXZ.push_back(directionZ);    
    directionsXZY.push_back(directionX);    
    directionsXZY.push_back(directionZ);    
    directionsXZY.push_back(directionY); 
    directionsZXY.push_back(directionZ);    
    directionsZXY.push_back(directionX);    
    directionsZXY.push_back(directionY); 

   	vector< std::shared_ptr<SmoothieSIAC> > Sms, SmD1s, SmD2s, SmD3s;
    std::shared_ptr<SmoothieSIAC> sm = std::make_shared<SmoothieSIAC3D> (SIACUtilities::eSYM_2kp1_1SIDED_2kp1,
                HNM3D, atoi(argv[2]), atof(argv[3]) );
    std::shared_ptr<SmoothieSIAC> smD = std::make_shared<SmoothieSIAC3D> (SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1,
                HNM3D, atoi(argv[2]), atof(argv[3]) ,1);
    Sms.push_back(sm);  Sms.push_back(sm);  Sms.push_back(sm);
    SmD1s.push_back(smD);   SmD2s.push_back(sm);    SmD3s.push_back(sm);
    SmD1s.push_back(sm);    SmD2s.push_back(smD);   SmD3s.push_back(sm);
    SmD1s.push_back(sm);    SmD2s.push_back(sm);    SmD3s.push_back(smD);
    vector<NekDouble> scalings;
    scalings.push_back(scaling);    scalings.push_back(scaling);    scalings.push_back(scaling);

    vector<int> varsUsx, varsVsx, varsWsx;
    vector<int> varsUsy, varsVsy, varsWsy;
    vector<int> varsUsz, varsVsz, varsWsz;
	varsUsx.push_back(4);	varsUsx.push_back(4);	varsUsx.push_back(4);	
	varsUsy.push_back(5);	varsUsy.push_back(5);	varsUsy.push_back(5);	
	varsUsz.push_back(6);	varsUsz.push_back(6);	varsUsz.push_back(6);	
	varsVsx.push_back(7);	varsVsx.push_back(7);	varsVsx.push_back(7);	
	varsVsy.push_back(8);	varsVsy.push_back(8);	varsVsy.push_back(8);	
	varsVsz.push_back(9);	varsVsz.push_back(9);	varsVsz.push_back(9);	
	varsWsx.push_back(10);	varsWsx.push_back(10);	varsWsx.push_back(10);	
	varsWsy.push_back(11);	varsWsy.push_back(11);	varsWsy.push_back(11);	
	varsWsz.push_back(12);	varsWsz.push_back(12);	varsWsz.push_back(12);	

	//Declare variables to be used for storing.
	//minx = 2.1; miny = 0.0; minz = 0.6;
	//maxx = 2.5; maxy = 0.5; maxz= 1.1;
	minx = 2.1; miny = 0.15; minz = 0.725;
	maxx = 2.5; maxy = 0.35; maxz= 0.925;
    
	int gPtsX =  1;   //atoi(argv[2]);
    int gPtsY =  30;   //atoi(argv[2]);
    int gPtsZ =  30;   //atoi(argv[2]);
    int Nx = gPtsX, Ny = gPtsY, Nz = gPtsZ;
    int totPts = Nx*Ny*Nz;
    NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0), sz=1.0/(Nz-1.0);
    Array<OneD,NekDouble> pX(totPts), pY(totPts), pZ(totPts), pE(totPts);   
    Array<OneD,NekDouble> pU_p(totPts), pU_sx(totPts), pU_sy(totPts), pU_sz(totPts);
    Array<OneD,NekDouble> pV_p(totPts), pV_sx(totPts), pV_sy(totPts), pV_sz(totPts);
    Array<OneD,NekDouble> pW_p(totPts), pW_sx(totPts), pW_sy(totPts), pW_sz(totPts);
	Array<OneD,NekDouble> pUx_ss(totPts), pUy_ss(totPts), pUz_ss(totPts);
    Array<OneD,NekDouble> pVx_ss(totPts), pVy_ss(totPts), pVz_ss(totPts);
    Array<OneD,NekDouble> pWx_ss(totPts), pWy_ss(totPts), pWz_ss(totPts);
    Array<OneD,NekDouble> pU_ss(totPts), pV_ss(totPts), pW_ss(totPts);
	Array<OneD,NekDouble> coord(3,0.0);
	//  Get List of expansions in space.
	// 	Loop through the expansions and 1DLSIAC first.
		// Load data back into u_sx_phys and other similar variables.
		// sample data at the resolution needed and compare the result to 1-dir LSIAC output.
	vector<int> eIds;
	HNM3D->readNekArray( eIds, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pIds_phys.txt");
	cout << eIds.size()<< endl;
	HNM3D->printNekArray(eIds,0.0);

	HNM3D->readNekArray( u_sx_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pU_sx_phys.txt");
	HNM3D->readNekArray( u_sy_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pU_sy_phys.txt");
	HNM3D->readNekArray( u_sz_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pU_sz_phys.txt");

	HNM3D->readNekArray( v_sx_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pV_sx_phys.txt");
	HNM3D->readNekArray( v_sy_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pV_sy_phys.txt");
	HNM3D->readNekArray( v_sz_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pV_sz_phys.txt");
	
	HNM3D->readNekArray( w_sx_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pW_sx_phys.txt");
	HNM3D->readNekArray( w_sy_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pW_sy_phys.txt");
	HNM3D->readNekArray( w_sz_phys, fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_AnyRes3DSIAC1xs_pW_sz_phys.txt");
	
	Array<OneD,NekDouble> r_u_sx_phys = HNM3D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> r_v_sx_phys = HNM3D->m_expansions[1]->GetPhys();
	Array<OneD,NekDouble> r_w_sx_phys = HNM3D->m_expansions[2]->GetPhys();
	Array<OneD,NekDouble> r_u_sy_phys = HNM3D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> r_v_sy_phys = HNM3D->m_expansions[1]->GetPhys();
	Array<OneD,NekDouble> r_w_sy_phys = HNM3D->m_expansions[2]->GetPhys();
	Array<OneD,NekDouble> r_u_sz_phys = HNM3D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> r_v_sz_phys = HNM3D->m_expansions[1]->GetPhys();
	Array<OneD,NekDouble> r_w_sz_phys = HNM3D->m_expansions[2]->GetPhys();
	

//Fill remaining numbers with other data.
	for ( auto const& elId:eIds)
	{
		int el_phys_offset = HNM3D->m_expansions[0]->GetPhys_Offset(elId);
        int el_num_phys = HNM3D->m_expansions[0]->GetExp(elId)->GetTotPoints(); 
        cout << elId << "\t" << el_phys_offset<< "\t"<< el_num_phys << endl;
		for(int i =0 ; i < el_num_phys ; i++)
        {   
            int index = i+el_phys_offset;
			r_u_sx_phys[index] = u_sx_phys[index];
			r_u_sy_phys[index] = u_sy_phys[index];
			r_u_sz_phys[index] = u_sz_phys[index];
			r_v_sx_phys[index] = v_sx_phys[index];
			r_v_sy_phys[index] = v_sy_phys[index];
			r_v_sz_phys[index] = v_sz_phys[index];
			r_w_sx_phys[index] = w_sx_phys[index];
			r_w_sy_phys[index] = w_sy_phys[index];
			r_w_sz_phys[index] = w_sz_phys[index];
		}
	}

// add data into 
	HNM3D->m_Arrays.push_back(r_u_sx_phys); //4
	HNM3D->m_Arrays.push_back(r_u_sy_phys); //5
	HNM3D->m_Arrays.push_back(r_u_sz_phys); //6
	HNM3D->m_Arrays.push_back(r_v_sx_phys); 
	HNM3D->m_Arrays.push_back(r_v_sy_phys);
	HNM3D->m_Arrays.push_back(r_v_sz_phys);
	HNM3D->m_Arrays.push_back(r_w_sx_phys);
	HNM3D->m_Arrays.push_back(r_w_sy_phys);
	HNM3D->m_Arrays.push_back(r_w_sz_phys); //12


	for (int i =0; i <Nx; i++)
	{
		for(int j=0; j<Ny; j++)
		{
			for(int k=0; k<Nz; k++)
			{
				int index = i*Ny*Nz+j*Nz+k;
				//cout << index << endl;
				//pX[index] = minx + i*sx*(maxx-minx);
				pX[index] = 2.25;
				pY[index] = miny + j*sy*(maxy-miny);
				pZ[index] = minz + k*sz*(maxz-minz);
				coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = pZ[index];
				//int elID = HNM3D->m_expansions[0]->GetExpIndex(coord);
				int elID = HNM3D->GetExpansionIndexUsingRTree(coord);
//				cout << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;	
//				cout << "eid: \t"<<elID<< "\t index \t"<< index << endl;
				pE[index] = elID;
				if (elID !=-1)
				{
					//sm->EvaluateAt(pX[index],pY[index],pZ[index],pU_s[index],valY,valZ, directionX, scaling ,0);
					switch( atoi(argv[4]))
                    {   
                        case 1:
                            SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pUx_ss[index],valY,valZ,SmD2s, directionsZXY, scalings ,varsUsz,1);
                            break;
                        case 2:
                            SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pUy_ss[index],valY,valZ,SmD2s, directionsXYZ, scalings ,varsUsx,1);
                            break;
                        case 3:
                            SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pUz_ss[index],valY,valZ,SmD2s, directionsXZY, scalings ,varsUsx,1);
                            break;
                        case 4:
                            SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pVx_ss[index],valY,valZ,SmD2s, directionsZXY, scalings ,varsVsz,1);
                            break;
                        case 5:
                            SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pVy_ss[index],valY,valZ,SmD2s, directionsXYZ, scalings ,varsVsx,1);
                            break;
                        case 6:
                            SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pVz_ss[index],valY,valZ,SmD2s, directionsXZY, scalings ,varsVsx,1);
                            break;
                        case 7:
                            SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pWx_ss[index],valY,valZ,SmD2s, directionsZXY, scalings ,varsWsz,1);
                            break;
                        case 8:
                            SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pWy_ss[index],valY,valZ,SmD2s, directionsXYZ, scalings ,varsWsx,1);
                            break;
                        case 9:
                            SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pWz_ss[index],valY,valZ,SmD2s, directionsXZY, scalings ,varsWsx,1);
                            break;
                        case 10:
                            SmD1s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pU_ss[index],valY,valZ,SmD1s, directionsXYZ, scalings ,varsUsx,1);
                            break;
                        case 11:
                            SmD1s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pV_ss[index],valY,valZ,SmD1s, directionsXYZ, scalings ,varsVsx,1);
                            break;
                        case 12:
                            SmD1s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pW_ss[index],valY,valZ,SmD1s, directionsXYZ, scalings ,varsWsx,1);
                            break;
					}
				}else{
					cout << "out of box. Somehting is wrong." << endl;
					assert( false&&"out of box. Something is wrong" );
				}
			}
			cout << j<< endl;
			cout << "quering into R-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds for Nz = " << Nz<< endl;
			startTime = clock();
		}
		cout << i << endl;
	}

    NektarBaseClass k;

    switch ( atoi(argv[4]) )
    {
        case 1:
            k.writeNekArray(pX,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pX.txt");
            k.writeNekArray(pY,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pY.txt");
            k.writeNekArray(pZ,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pZ.txt");
            k.writeNekArray(pE,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pE.txt");
            k.writeNekArray(pUx_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pUx_ss.txt");
            break;
        case 2:
            k.writeNekArray(pUy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pUy_ss.txt");
            break;
        case 3:
            k.writeNekArray(pUz_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pUz_ss.txt");
            break;
        case 4:
            k.writeNekArray(pVx_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pVx_ss.txt");
            break;
        case 5:
            k.writeNekArray(pVy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pVy_ss.txt");
            break;
        case 6:
            k.writeNekArray(pVz_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pVz_ss.txt");
            break;
        case 7:
            k.writeNekArray(pWx_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pWx_ss.txt");
            break;
        case 8:
            k.writeNekArray(pWy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pWy_ss.txt");
            break;
        case 9:
            k.writeNekArray(pWz_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pWz_ss.txt");
            break;
        case 10:
            k.writeNekArray(pU_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_U_ss.txt");
            break;
        case 11:
            k.writeNekArray(pV_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_V_ss.txt");
            break;
        case 12:
            k.writeNekArray(pW_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_W_ss.txt");
            break;
    }

	
	//HNM3D->writeNekArray(pU_p,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pU_p.txt");
	//HNM3D->writeNekArray(pU_sx,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pU_sx.txt");
	
	/*


	for (int i =0; i <Nx; i++)
	{
		for(int j=0; j<Ny; j++)
		{
			for(int k=0; k<Nz; k++)
			{
				int index = i*Ny*Nz+j*Nz+k;
				//cout << index << endl;
				//pX[index] = minx + i*sx*(maxx-minx);
				pX[index] = 2.25;
				pY[index] = miny + j*sy*(maxy-miny);
				pZ[index] = minz + k*sz*(maxz-minz);
				coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = pZ[index];
				//int elID = HNM3D->m_expansions[0]->GetExpIndex(coord);
				int elID = HNM3D->GetExpansionIndexUsingRTree(coord);
//				cout << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;	
//				cout << "eid: \t"<<elID<< "\t index \t"<< index << endl;
				pE[index] = elID;
				if (elID !=-1)
				{
					//sm->EvaluateAt(pX[index],pY[index],pZ[index],pU_s[index],valY,valZ, directionX, scaling ,0);
					//Sms[0]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pU_ss[index],valY,valZ,Sms, directions, scalings ,varsU,1);
					switch( atoi(argv[4]))
					{
						case 1:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pUx_ss[index],valY,valZ,SmD2s, directionsYXZ, scalings ,varsU,1);
							break;
						case 2:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pUy_ss[index],valY,valZ,SmD2s, directionsXYZ, scalings ,varsU,1);
							break;
						case 3:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pUz_ss[index],valY,valZ,SmD2s, directionsXZY, scalings ,varsU,1);
							break;
						case 4:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pVx_ss[index],valY,valZ,SmD2s, directionsYXZ, scalings ,varsV,1);
							break;
						case 5:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pVy_ss[index],valY,valZ,SmD2s, directionsXYZ, scalings ,varsV,1);
							break;
						case 6:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pVz_ss[index],valY,valZ,SmD2s, directionsXZY, scalings ,varsV,1);
							break;
						case 7:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pWx_ss[index],valY,valZ,SmD2s, directionsYXZ, scalings ,varsW,1);
							break;
						case 8:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pWy_ss[index],valY,valZ,SmD2s, directionsXYZ, scalings ,varsW,1);
							break;
						case 9:
							SmD2s[1]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pWz_ss[index],valY,valZ,SmD2s, directionsXZY, scalings ,varsW,1);
							break;
					}
				}else
				{	
					pU_p[index] = 0.0;
					pV_p[index] = 0.0;
					pW_p[index] = 0.0;
					pP_p[index] = 0.0;
					pP_s[index] = 0.0;
					cout << "out" << endl;
				}
	cout << "quering into R-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds for Nz = " << Nz<< endl;
	startTime = clock();
			}
			cout << j<< endl;
		}
		cout << i << endl;
	}
	
// Calculate acceleration,  b, torsion, Dbv for this data.


	switch ( atoi(argv[4]) )
	{
		case 1:
			k.writeNekArray(pX,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pX.txt");
			k.writeNekArray(pY,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pY.txt");
			k.writeNekArray(pZ,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pZ.txt");
			k.writeNekArray(pE,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pE.txt");
			k.writeNekArray(pUx_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pUx_ss.txt");
			break;
		case 2:
			k.writeNekArray(pUy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pUy_ss.txt");
			break;
		case 3:
			k.writeNekArray(pUz_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pUz_ss.txt");
			break;
		case 4:
			k.writeNekArray(pVx_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pVx_ss.txt");
			break;
		case 5:
			k.writeNekArray(pVy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pVy_ss.txt");
			break;
		case 6:
			k.writeNekArray(pVz_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pVz_ss.txt");
			break;
		case 7:
			k.writeNekArray(pWx_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pWx_ss.txt");
			break;
		case 8:
			k.writeNekArray(pWy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pWy_ss.txt");
			break;
		case 9:
			k.writeNekArray(pWz_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pWz_ss.txt");
			break;
	}
	
	//k.writeNekArray(pU_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pU_s.txt");
	//k.writeNekArray(pU_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pU_ss.txt");
	//k.writeNekArray(pUy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pUy_ss.txt");
	//k.writeNekArray(pUy_ssx,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pUy_ssx.txt");
	//k.writeNekArray(pUy_ssy,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pUy_ssy.txt");
	//k.writeNekArray(pUy_ssz,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_DUP2use1dir3DSIAC_pUy_ssz.txt");
	*/

	return 0;
}


