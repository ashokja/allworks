#include <cstdio>
#include <cstdlib>
#include <stdlib.h>   

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include <LibUtilities/Communication/Comm.h>
#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
#include "SmoothieConfig.h"
#include "NektarBaseClass.h"
#include "NonSymmetricSIAC.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh1D.h"
#include "SmoothieSIAC1D.h"

using namespace std;
using namespace Nektar;

int main(int argc, char *argv[])
{
	cout << "SmoothieV: " << Smoothie_VERSION_MAJOR <<endl;
	
	argc = 2;
	//LibUtilities::SessionReaderSharedPtr vSession
	//			= LibUtilities::SessionReader::CreateInstance(argc,argv);

	//HandleNekMesh1D* HNM1D = new HandleNekMesh1D(vSession);

	NonSymmetricSIAC nsymSIAC(3);

	int k =1;
	vector< vector< NekDouble>> kmat;
//	vector<NekDouble> kvec = {-0.080007856944947123, -0.052529093512597401, -0.032275931016178004,
//  -0.012600852118420824, 0.015865857334837224, 0.030074555162219165, 0.056954721879551001, 0.063076327556725195,
//  0.068326026784193652};
	vector<NekDouble> kvec = {-2,-1,0,1,3};
	cout << kvec.size() << endl;
	for (int i =0; i< kvec.size()-k-1; i++)
	{
		vector<NekDouble> temp;
		for(int j=0;j<k+2;j++)
		{
			temp.push_back(kvec[i+j]);
		}
		kmat.push_back(temp);
	}
	Array<OneD,NekDouble> coeffs(kvec.size()-k-1);
	nsymSIAC.CalCoeffForKnotMatrixVec_Hanieh(1,kmat,coeffs);

	for(int i=0; i < coeffs.num_elements();i++)
	{
		cout << coeffs[i] << endl;
	} 

	return 0;
/*
	SmoothieSIAC1D sm_NONSYM(SIACUtilities::eSYM_UNEVEN_2kp1, HNM1D, 3, 0.1 ); 

	Array<OneD,NekDouble> knotVec(5,0.0);
	knotVec[0] = -2.0/2.0;
	knotVec[1] = -1.0/2.0;
	knotVec[2] =  1.0/2.0;
	knotVec[3] =  2.0/2.0;
	knotVec[4] =  3.0/2.0;
	nsymSIAC.EvaluateCoefficients(knotVec);
	nsymSIAC.EvaluateCoefficients(knotVec);

	Array<OneD,NekDouble> x_pos(101),t_vals(101,0.0);
	for(int i=0; i < 101; i++)
	{
		x_pos[i] = -2.0+(3.0-(-2.0))*i/(101.0-1.0);
	}

	nsymSIAC.EvaluateFilterWknots( x_pos, t_vals, knotVec, 1.0,0.0,true);

	k.writeNekArray(x_pos,"xpos.txt");
	k.writeNekArray(t_vals,"tvals.txt");


	Array<OneD,NekDouble> coord(3,0.0),direction(3,0.0),knotVec1(5,0.0);
	
	cout << "set xcoord:";
	cin >>coord[0]; 
	//coord[0]=0.5; 
	direction[0] = 1.0;
	NekDouble shift;
	HNM1D->GetKnotVec(1, coord, direction, knotVec1,shift);
	k.printNekArray(knotVec1);
	cout << shift<< endl;

	Array<OneD,NekDouble> knotVec2(11,0.0);
	HNM1D->GetKnotVec(3, coord, direction, knotVec2,shift);
	k.printNekArray(knotVec2);
	cout << shift<< endl;
	
	Array<OneD,NekDouble> knotVec3(8,0.0);
	HNM1D->GetKnotVec(2, coord, direction, knotVec3,shift);
	k.printNekArray(knotVec3);
	cout << shift<< endl;
	
	Array<OneD,NekDouble> knotVec4(14,0.0);
	HNM1D->GetKnotVec(4, coord, direction, knotVec4,shift);
	k.printNekArray(knotVec4);
	cout << shift<< endl;
	vector<NekDouble> HvalT;
	int Res = 1000;
	HvalT.push_back(0.0);
	for (int i =0; i < 10; i++)
	{
		int rnum = rand()% Res;
		HvalT.push_back(rnum/(Res+0.0));
	}
	HvalT.push_back(1.0);
	// sort HavlT
	std::sort(HvalT.begin(),HvalT.end());
	for (std::vector<NekDouble>::iterator it=HvalT.begin(); it!=HvalT.end(); ++it)
    	std::cout << ' ' << *it;
  	std::cout << '\n';
	
	Array<OneD,NekDouble> knotVec(8);
	NekDouble shift;

	Array<OneD,NekDouble> pX(Res), pL(Res),pR(Res);	
	Array<OneD,NekDouble> pK1(Res), pK2(Res),pK3(Res);	
	Array<OneD,NekDouble> pK4(Res), pK5(Res),pK6(Res);	

	for(int i =0; i < Res; i++)
	{
		NekDouble t = i/(Res+0.0);
		bool Applied = sm_NONSYM.CalculateKnotVec( t, HvalT, knotVec, shift);
		cout << knotVec[0] <<"\t" <<
			 knotVec[1] <<"\t" <<
			 knotVec[2] <<"\t" <<
 			knotVec[3] <<"\t" <<
 			knotVec[4] <<"\t" << 
			 knotVec[5] <<"\t" <<
 			knotVec[6] <<"\t" <<
 			knotVec[7] << endl;
		pX[i] = t;
		if (Applied)
		{
			pL[i] = knotVec[0];
			pK1[i] = knotVec[1];
			pK2[i] = knotVec[2];
			pK3[i] = knotVec[3];
			pK4[i] = knotVec[4];
			pK5[i] = knotVec[5];
			pK6[i] = knotVec[6];
			pR[i] = knotVec[7];
		}else{
			pL[i] = 0;
			pK1[i] = 0;
			pK2[i] =0;
			pK3[i] = 0;
			pK4[i] = 0;
			pK5[i] =0;
			pK6[i] = 0;
			pR[i] = 0; 
		}
	}

	k.writeNekArray(HvalT, "Bvalue.txt");
	k.writeNekArray(pX, "Xvalue.txt");
	k.writeNekArray(pL, "Lvalue.txt");
	k.writeNekArray(pK1, "K1value.txt");
	k.writeNekArray(pK2, "K2value.txt");
	k.writeNekArray(pK3, "K3value.txt");
	k.writeNekArray(pK4, "K4value.txt");
	k.writeNekArray(pK5, "K5value.txt");
	k.writeNekArray(pK6, "K6value.txt");
	k.writeNekArray(pR, "Rvalue.txt");

	k.printNekArray(HvalT);	
	for(int i =0; i < HvalT.size()-1; i++)
	{
		for ( int j=0; j< 3; j++)
		{
			NekDouble t = (HvalT[i]+HvalT[i+1])/2.0 +(j-1)*0.001;
			bool Applied = sm_NONSYM.CalculateKnotVec( t, HvalT, knotVec, shift);
			//cout << HvalT[i] << endl;
			cout << t<< endl;
			if (Applied)
			{
			cout << knotVec[0] <<"\t" <<
				 knotVec[1] <<"\t" <<
				 knotVec[2] <<"\t" <<
 				knotVec[3] <<"\t" <<
 				knotVec[4] << "\t" <<
				 knotVec[5] <<"\t" <<
				 knotVec[6] <<"\t" <<
				 knotVec[7] << endl;
			}else{
			cout << "Dint\t" << endl;
			}
		}
	}
*/	
  return 0;
}

