function [Vel] = testRothSerialData(Cdata)
switch nargin
    case 0,    
        x = (-1:0.1:1);
        y = (-1:.1:1);
        z = (-1:.1:1);
end

X = Cdata(1,:);
Y = Cdata(2,:);
Z = Cdata(3,:);

X = (X+1)*3/2;
Y = (Y+1)*3/2;
Z = Z/2*3;

w = 3; 
R = 1.5;
g = 1;

r_XY = ((X).^2+(Y).^2+0.01).^(1/2);
U = -g*Y./r_XY - w*Z.*X./r_XY.^2;
V = g*X./r_XY -w*Z.*Y./r_XY.^2;
W = w*(1-R./r_XY);

Vel(1,:) = U;
Vel(2,:) = V;
Vel(3,:) = w;

% 
% figure(1)
% VizIsoSurface(xold,yold,zold,U,V,W,[-1],[-1],[]); hold on;
% 
% 
% [sx, sy, sz] = meshgrid([0],[-1],[-1:0.05:1]);
% %figure(2)
% %VizStreamLines(x,y,z,U,V,W,1.73,1,0); hold on;
% %VizStreamLines(x,y,z,U,V,W,1.75,1,0); hold on;
% 
% VizStreamLines(xold,yold,zold,U,V,W,sx,sy,sz); hold on;

end












% % test for rothWorks
% clear;
% addpath ../createTestData/
% 
% t = (-10:.7:10);
% [X,Y,Z] = meshgrid(t,t,t);
% 
% data(:,1) = X(:);
% data(:,2) = Y(:);
% data(:,3) = Z(:);
% wa = 2; Ga = 7; R = 50;
% s_d = max(size(data));
% 
% for i = 1:s_d
%     pos = data(i,:);
%     x = pos(1); y = pos(2); z = pos(3);
%     r = sqrt(x^2+y^2)+0.5;
%     u = -Ga*y/r - wa*z*x/r^2;
%     v = Ga*x/r - wa*z*y/r^2;
%     w = wa*(1-R/r);
%     dataOut(i,:) = [u,v,w];
% end
% figure(1)
% quiver3(data(:,1),data(:,2),data(:,3),dataOut(:,1),dataOut(:,2),dataOut(:,3));
% view(-90,0);
% figure(2)
% drawStreamLine(data,dataOut,ones(6,3));
% view(-90,0);
% colorbar;

