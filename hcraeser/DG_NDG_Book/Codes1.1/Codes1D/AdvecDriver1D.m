% Driver script for solving the 1D advection equations
clear;
Globals1D;

% Order of polymomials used for approximation 
N = 3;

% Generate simple mesh
[Nv, VX, K, EToV] = MeshGen1D(0.0,2*pi,7);

% Initialize solver and construct grid and metric
StartUp1D;

% Set initial conditions
u = sin(x);

% Solve Problem
FinalTime = 10;
[u] = Advec1D(u,FinalTime);
[a1, a2] = size(u);
u_p = reshape(u,[a1*a2,1]);
x_p = reshape(x,[a1*a2,1]);
%figure(1),
plot(x,u);
%figure(2), 
plot(x_p,u_p);
