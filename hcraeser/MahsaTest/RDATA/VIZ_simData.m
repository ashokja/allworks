clear;
close all;
%t = '2016_15';
%t = '1_8064_29';
%t = '4032_15';
%t ='2_100800_71' ;
t = '2_197568_99';
subTxt = 'sym';
ustar_fn= strcat('data/projection_hex_',subTxt,'_periodic_uStar_P',t,'_cart.txt');
DG_fn= strcat('data/projection_hex_',subTxt,'_periodic_DG_P',t,'_cart.txt');
Er_fn= strcat('data/projection_hex_',subTxt,'_periodic_err_P',t,'_cart.dat');

%ustar_fn= strcat('data/projection_hex_unionjack_periodic_uStar_P1_',t,'_cart.txt');
%DG_fn= strcat('data/projection_hex_unionjack_periodic_DG_P1_',t,'_cart.txt');
%Er_fn= strcat('data/projection_hex_unionjack_periodic_err_P1_',t,'_cart.dat');

[Ustar,B,C] = textread(ustar_fn,'%f %f %f');
[DG,B2,C2] = textread(DG_fn,'%f %f %f');
[E] = textread(Er_fn,'%f');



figure(1)
plot3(C,B,E,'c*'); hold off;

E_C = E/(max(max(E)))*10;
figure(2)
scatter3(C,B,E,ones(size(E)),E_C);

figure(3)
plot3(C,B,Ustar,'r.');hold on;
plot3(C,B,DG,'b.'); hold on;

figure(4)
plot3(C,B,Ustar-DG,'c*'); hold off;