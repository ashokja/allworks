function [curveCoeff] = ExtractCurFromIsoSurface(deg, pts)
    addpath ../Bfuncs
    switch nargin
        case 0;
            deg = 3;
            % input will set of vertices.
            x = linspace(-1,1,10);
            [X,Y,Z] = meshgrid(x,x,x);
            V = X.^2+Z.^2;
            [~,pts] = isosurface(X,Y,Z,V,0.1);
    end

    if nargin == 0
        figure(1)
        isosurface(X,Y,Z,V,0.1);
        xlabel('x'); ylabel('y');zlabel('z');
    end
    [spts,~] = size(pts);

    % point close to plane y = -1
    [cpt,useful] = findPointCloseToPlane(2,-1,pts);
    if useful
        c_lpts = cpt;
    end

    [cpt,useful] = findPointCloseToPlane(2,+1,pts);
    if useful
        c_rpts = cpt;
    end

    t = findParamTCloseToLine(c_lpts,c_rpts,pts);


    IntMatrix = zeros(spts,deg+1);
    for k = 0:deg
        IntMatrix(:,k+1) = B(deg,k,t);
    end

    coeffx = IntMatrix\pts(:,1);
    coeffy = IntMatrix\pts(:,2);
    coeffz = IntMatrix\pts(:,3);
    curveCoeff = [coeffx,coeffy,coeffz]';

    vt = linspace(-1,1,10)';
    derPts = zeros(max(size(vt)),3);
    for k = 0:deg
        derPts(:,1) = derPts(:,1) + coeffx(k+1)*B(deg,k,vt); 
        derPts(:,2) = derPts(:,2) + coeffy(k+1)*B(deg,k,vt);
        derPts(:,3) = derPts(:,3) + coeffz(k+1)*B(deg,k,vt); 
    end
    if nargin == 0
        hold on;
        plot3(derPts(:,1),derPts(:,2),derPts(:,3),'-r*');
    end
end