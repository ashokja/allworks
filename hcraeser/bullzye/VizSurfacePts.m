function [pts] = VizSurfacePts(xcheb,ycheb,zcheb,UAtCheb,VAtCheb,WAtCheb,isoValue)

    [X,Y,Z] = meshgrid(xcheb,ycheb,zcheb);
    normAtCheb = sqrt(UAtCheb.^2 + VAtCheb.^2+ WAtCheb.^2);
    normAtCheb = permute(normAtCheb,[2,1,3]);
    
    [~,pts] = isosurface(X,Y,Z,normAtCheb,isoValue);
end