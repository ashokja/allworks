clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20];

abc = '../../data/UnsTriMeshes/NM3_unsMeshvar1_025_2_R_100_pX_2DDyn.txt';
%data/triangleMesh_O1_I2_2_S_0.1_R_100_pP_2D.txt ';

R = 100;
poly =4;
order = poly;
count =1;
prefixFilename = '../../data/UnsTriMeshes/NM4_unsMeshvar1_0125_';

xfname = strcat(prefixFilename,int2str(poly),'_R_',int2str(R),'_pX_2DDyn.txt' );
yfname = strcat(prefixFilename,int2str(poly),'_R_',int2str(R),'_pY_2DDyn.txt' );
ffname = strcat(prefixFilename,int2str(poly),'_R_',int2str(R),'_pV_2DDyn.txt' );
pfname = strcat(prefixFilename,int2str(poly),'_R_',int2str(R),'_pP_2DDyn.txt' );
sfname = strcat(prefixFilename,int2str(poly),'_R_',int2str(R),'_pS_2DDyn.txt' );
sDynfname = strcat(prefixFilename,int2str(poly),'_R_',int2str(R),'_pSDyn_2DDyn.txt' );
dynsfname = strcat(prefixFilename,int2str(poly),'_R_',int2str(R),'_pDyn_2DDyn.txt' );

xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
fpos = textread(ffname, '%f\t');
ppos = textread(pfname, '%f\t');
spos = textread(sfname, '%f\t');
sDynpos = textread(sDynfname, '%f\t');
dynspos = textread(dynsfname, '%f\t');

xVpos = reshape(xpos,[R,R]);
yVpos = reshape(ypos,[R,R]);
fVpos = reshape(fpos,[R,R]);
pVpos = reshape(ppos,[R,R]);
sVpos = reshape(spos,[R,R]);
 sDynVpos = reshape(sDynpos,[R,R]);
dynsVpos = reshape(dynspos,[R,R]);

figure(1),
surf(xVpos,yVpos,fVpos);

figure(2),
surf(xVpos,yVpos,pVpos);

figure(3),
surf(xVpos,yVpos,sVpos);


figure(4),
surf(xVpos,yVpos,abs(fVpos-pVpos));
title('projection errors');
figure(5),
surf(xVpos,yVpos,abs(fVpos-sVpos));
title('SIAC errors');

figure(6),
contourf(xVpos,yVpos,log10(abs(fVpos-pVpos)));
title('projection errors'); colorbar(); caxis([-10,0]);
view(0,90);

figure(7),
contourf(xVpos,yVpos,log10(abs(fVpos-sVpos)));
title('SIAC errors'); colorbar(); caxis([-10,0]);
view(0,90);

figure(8),
contourf(xVpos,yVpos,log10(abs(fVpos-sDynVpos)));
title('SIAC Dyn Errors'); colorbar(); caxis([-10,0]);
view(0,90);


disp('Proj');
disp(sum(sum((abs(fVpos-pVpos)))));
disp(max(max((abs(fVpos-pVpos)))));
disp('SIAC');
disp(sum(sum((abs(fVpos-sVpos)))));
disp(max(max((abs(fVpos-sVpos)))));