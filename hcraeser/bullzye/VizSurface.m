function [pts] = VizSurface(xcheb,ycheb,zcheb,UAtCheb,VAtCheb,WAtCheb,isoValue)

    [X,Y,Z] = meshgrid(xcheb,ycheb,zcheb);
    normAtCheb = sqrt(UAtCheb.^2 + VAtCheb.^2+ WAtCheb.^2);
    normAtCheb = permute(normAtCheb,[2,1,3]);
    
    [~,pts] = isosurface(X,Y,Z,normAtCheb,isoValue);
    p = patch(isosurface(X,Y,Z,normAtCheb,isoValue));
    isonormals(X,Y,Z,normAtCheb,p)
    p.FaceColor = 'red';
    p.EdgeColor = 'none';
%'FaceAlpha',.3
    p.FaceAlpha = .3;
    daspect([1,1,1])
    view(3); %axis tight
    camlight 
    lighting gouraud 
    hold on;
end