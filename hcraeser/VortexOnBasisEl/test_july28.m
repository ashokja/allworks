% test day with sin wave as starting point.
clear;

%initialize variables
n1 = 6; n2 = 6; n3 =6;
seedNum = 1;

%initialize Data
x = (-1:.1:1)';
[X,Y,Z] = meshgrid(x,x,x);
dataIn(:,1) = reshape(X,[],1);
dataIn(:,2) = reshape(Y,[],1);
dataIn(:,3) = reshape(Z,[],1);
[sx,~] = size(dataIn);
dataOut = zeros(size(dataIn));
% for i = 1:sx
%     coords = squeeze(dataIn(i,:));
%     x =  coords(1); y = coords(2); z = coords(3);
%     vel = [0.4*cos(4*z),-0.4*sin(4*z),1];
%     dataOut(i,:) = vel;
% end

for i = 1:sx
    coords = squeeze(dataIn(i,:));
    x =  coords(1)+1.001; y = coords(2)+1; z = coords(3);
    teta = atan(y/x);
    lambda = sqrt((x-1)^2+(y-1)^2+z^2); r =1;
    v1= [x,y,z]; v2 = [cos(teta),sin(teta),0];
    %rotational Velocity.
    %vel = v2;
    velh = 0.1*abs(sqrt(3)-lambda)*[sin(teta),-cos(teta),0];
    vel = cross(v1-v2,[-sin(teta),cos(teta),0])+velh;%+ 0.1*cross(v1,v2);% + [cos(teta),sin(teta),0]*.1;
    dataOut(i,:) = vel;
end

% acutal code
coeff = buildCoeff(n1,n2,n3,dataIn,dataOut);
figure(1)
vizualizeCoeff(n1,n2,n3,coeff);

[uCoeffRed,uCoeffRot] = divideVel(n1,n2,n3,coeff,dataIn,dataOut);

figure(2);
vizualizeCoeff(n1,n2,n3,uCoeffRed);

figure(3);
vizualizeCoeff(n1,n2,n3,uCoeffRot);

%%
%find zeros on each face.
[sol_seeds,goodSeeds,solAtSeeds] = zerosAtFaces(n1,n2,n3,uCoeffRot);
%%
%For all good seeds trace the seeds into the cube.
[seedNum,dim] = size(goodSeeds);

% using actual velocity.
count =1; tps = {}; tpsVel = {};
for i = 1:seedNum
    for j = 1:dim
        if (goodSeeds(i,j) ==1)
            [trace,traceVel] = traceThePoint(n1,n2,n3,coeff, sol_seeds(i,j,:) );
            tps(count) = {trace};
            tpsVel(count) = {traceVel};
            count = count +1;
        end
    end
end
%%

[~,b] = size(tps);
figure(4);
plotBoundingBox();
for i =1:b
    trace = cell2mat(tps(i));
    plot3(trace(:,1),trace(:,2),trace(:,3));
    plot3(trace(:,1),trace(:,2),trace(:,3),'*');
    hold on;
end
hold off;

% % using only reduced velocity.
% count =1; tpsr = {}; tpsVel= {}
% for i = 1:seedNum
%     for j = 1:dim
%         if (goodSeeds(i,j) ==1)
%             [trace,traceVel] = traceThePointUsingRedVel(n1,n2,n3,uCoeffRed, sol_seeds(i,j,:) );
%             tpsr(count) = {trace};
%             tpsVelr(count) = {traceVel};
%             count = count +1;
%         end
%     end
% end

