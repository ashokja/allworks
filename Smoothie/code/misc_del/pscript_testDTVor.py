#!/usr/bin/python
import sys
from decimal import Decimal 

#Exefile = "TestSuiteS2O2"
Exefile = sys.argv[1]
#filterOrder="2"
#filterOrder=sys.argv[2]

rates = [10,20]
f= [2,3,4]
for filterOrder in f:
	filename = "../data/NekMeshes/P"+ str(filterOrder)+"_CubeGrid_"
	for r in rates:
		print "./"+ Exefile +" "+ filename+str(r)+".xml 1 1 1 1"

fldexe = "~/gitlab/nektar/build/utilities/FieldConvert/FieldConvert"
mvdirTo = "~/VIZ/"
for filterOrder in f:
	for r in rates:
		filenamexml = "../data/NekMeshes/P"+ str(filterOrder)+"_CubeGrid_"+str(r)+".xml"
		filenamefld = "../data/NekMeshes/P"+ str(filterOrder)+"_CubeGrid_"+str(r)+".fld"
		filenamevtu = "../data/NekMeshes/P"+ str(filterOrder)+"_CubeGrid_"+str(r)+".vtu"
		print fldexe+" "+ filenamexml+" "+filenamefld+" "+filenamevtu;
		print "mv "+filenamevtu+ " " +mvdirTo;
#print "mv ../data/code/NekMeshes/*.txt ../matlabCodes/datatxt/"
