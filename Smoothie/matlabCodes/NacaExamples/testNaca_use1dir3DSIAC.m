% Read data from testNaca_use1dir3DSIAC
% s is used in variables but it ss data.

clear;
close all;


UsebkupData = 'cur_feb20/'; % has only data Ux_ss so remember to comment below also.
UsebkupData_alter_D = 'cur_feb13/'; % has only data Ux_ss so remember to comment below also.
UsebkupData_alter_C = 'cur_feb12/';

%UsebkupData = 'cur_feb10/'

%UsebkupData = ''
%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05';
filename = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_use1dir3DSIAC' );
filename_alter_D = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_Any2LSIACVAR' );
filename_alter_C = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ACRFDVar' );

filepath = strcat('data/',UsebkupData,filename);
filepathD = strcat('data/',UsebkupData_alter_D,filename_alter_D);
filepathC = strcat('data/',UsebkupData_alter_C,filename_alter_C);

xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );

ussfname = strcat(filepath,'_U_ss.txt' ); % raw data projection.
uxssfname = strcat(filepath,'_pUx_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
uyssfname = strcat(filepath,'_pUy_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
uzssfname = strcat(filepath,'_pUz_ss.txt' ); % this variable is u with LSIAC applied in X-direction.

%ussfname = strcat(filepathC,'_pU_ss.txt' ); % raw data projection.
% uxssfname = strcat(filepathD,'_pUx_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
% uyssfname = strcat(filepathD,'_pUy_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
% uzssfname = strcat(filepathD,'_pUz_ss.txt' ); % this variable is u with LSIAC applied in X-direction.


vssfname = strcat(filepath,'_V_ss.txt' ); % raw data projection.
vxssfname = strcat(filepath,'_pVx_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
vyssfname = strcat(filepath,'_pVy_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
vzssfname = strcat(filepath,'_pVz_ss.txt' ); % this variable is u with LSIAC applied in X-direction.

%vssfname = strcat(filepathC,'_pV_ss.txt' ); % raw data projection.
%vxssfname = strcat(filepathD,'_pVx_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
%vyssfname = strcat(filepathD,'_pVy_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
%vzssfname = strcat(filepathD,'_pVz_ss.txt' ); % this variable is u with LSIAC applied in X-direction.


wssfname = strcat(filepath,'_W_ss.txt' ); % raw data projection.
wxssfname = strcat(filepath,'_pWx_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
wyssfname = strcat(filepath,'_pWy_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
wzssfname = strcat(filepath,'_pWz_ss.txt' ); % this variable is u with LSIAC applied in X-direction.

% wssfname = strcat(filepathC,'_pW_ss.txt' ); % raw data projection.
% wxssfname = strcat(filepathD,'_pWx_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
% wyssfname = strcat(filepathD,'_pWy_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
% wzssfname = strcat(filepathD,'_pWz_ss.txt' ); % this variable is u with LSIAC applied in X-direction.


xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');

usspos = textread(ussfname, '%f\t');
uxsspos = textread(uxssfname, '%f\t');
uysspos = textread(uyssfname, '%f\t');
uzsspos = textread(uzssfname, '%f\t');

vsspos = textread(vssfname, '%f\t');
vxsspos = textread(vxssfname, '%f\t');
vysspos = textread(vyssfname, '%f\t');
vzsspos = textread(vzssfname, '%f\t');

wsspos = textread(wssfname, '%f\t');
wxsspos = textread(wxssfname, '%f\t');
wysspos = textread(wyssfname, '%f\t');
wzsspos = textread(wzssfname, '%f\t');


epos = floor(epos +0.0001);
s = floor( (max(size(xpos)))^(1/2) +0.1);

xVpos = reshape(xpos,s,s);
yVpos = reshape(ypos,s,s);
zVpos = reshape(zpos,s,s);

upVpos = reshape(usspos,s,s);  
usxVpos = reshape(uxsspos,s,s);  
usyVpos = reshape(uysspos,s,s);  
uszVpos = reshape(uzsspos,s,s);  

vpVpos = reshape(vsspos,s,s);  
vsxVpos = reshape(vxsspos,s,s);  
vsyVpos = reshape(vysspos,s,s);  
vszVpos = reshape(vzsspos,s,s);  

wpVpos = reshape(wsspos,s,s);  
wsxVpos = reshape(wxsspos,s,s);  
wsyVpos = reshape(wysspos,s,s);  
wszVpos = reshape(wzsspos,s,s);  


% Debug%

figure(1),
subplot(2,2,1), surf(yVpos,zVpos,upVpos); hold on; colorbar(); shading interp; view(0,90);
title('u_sss');
subplot(2,2,2), surf(yVpos,zVpos,usxVpos); hold on; colorbar(); shading interp; view(0,90);
title('ux_sss');
subplot(2,2,3), surf(yVpos,zVpos,usyVpos); hold on; colorbar(); shading interp; view(0,90);
title('uy_sss');
subplot(2,2,4), surf(yVpos,zVpos,uszVpos); hold on; colorbar(); shading interp; view(0,90);
title('uz_sss');


figure(2),
subplot(2,2,1), surf(yVpos,zVpos,vpVpos); hold on; colorbar(); shading interp; view(0,90);
title('v_sss');
subplot(2,2,2), surf(yVpos,zVpos,vsxVpos); hold on; colorbar(); shading interp; view(0,90);
title('vx_sss');
subplot(2,2,3), surf(yVpos,zVpos,vsyVpos); hold on; colorbar(); shading interp; view(0,90);
title('vy_sss');
subplot(2,2,4), surf(yVpos,zVpos,vszVpos); hold on; colorbar(); shading interp; view(0,90);
title('vz_sss');

figure(3),
subplot(2,2,1), surf(yVpos,zVpos,wpVpos); hold on; colorbar(); shading interp; view(0,90);
title('w_sss');
subplot(2,2,2), surf(yVpos,zVpos,wsxVpos); hold on; colorbar(); shading interp; view(0,90);
title('wx_sss');
subplot(2,2,3), surf(yVpos,zVpos,wsyVpos); hold on; colorbar(); shading interp; view(0,90);
title('wy_sss');
subplot(2,2,4), surf(yVpos,zVpos,wszVpos); hold on; colorbar(); shading interp; view(0,90);
title('wz_sss');
%%

figure(4),
subplot(2,2,1), contour(yVpos,zVpos,upVpos);
title('u_sss');
subplot(2,2,2), contour(yVpos,zVpos,usxVpos,linspace(min(usxVpos(:)),2.5,20)); 
title('ux_sss');
subplot(2,2,3), contour(yVpos,zVpos,usyVpos); 
title('uy_sss');
subplot(2,2,4), contour(yVpos,zVpos,uszVpos); 
title('uz_sss');


figure(5),
subplot(2,2,1), contour(yVpos,zVpos,vpVpos); 
title('v_sss');
subplot(2,2,2), contour(yVpos,zVpos,vsxVpos,20); 
title('vx_sss');
subplot(2,2,3), contour(yVpos,zVpos,vsyVpos); 
title('vy_sss');
subplot(2,2,4), contour(yVpos,zVpos,vszVpos); 
title('vz_sss');
%%
figure(6),
subplot(2,2,1), contour(yVpos,zVpos,wpVpos); 
title('w_sss');
subplot(2,2,2), contour(yVpos,zVpos,wsxVpos,20); 
title('wx_sss');
subplot(2,2,3), contour(yVpos,zVpos,wsyVpos);
title('wy_sss');
subplot(2,2,4), contour(yVpos,zVpos,wszVpos);
title('wz_sss');




% calculate acceleration.
auspos = uxsspos.*usspos + uysspos.*vsspos + uzsspos.*wsspos;
avspos = vxsspos.*usspos + vysspos.*vsspos + vzsspos.*wsspos;
awspos = wxsspos.*usspos + wysspos.*vsspos + wzsspos.*wsspos;
 %
% veclocity cross acceleration. 
cuspos = vsspos.*awspos - wsspos.*avspos;
cvspos = wsspos.*auspos - usspos.*awspos;
cwspos = usspos.*avspos - vsspos.*auspos;

curvature_pos = (cuspos.^(2) + cvspos.^(2) + cwspos.^(2)).^(1/2);
curvature_Vpos = reshape(curvature_pos,s,s);

figure(7)
surf(yVpos,zVpos,curvature_Vpos);
shading interp;

%%
figure(8)
contour3(yVpos,zVpos,curvature_Vpos,20,'LineColor','k'); hold on;
surf(yVpos,zVpos,curvature_Vpos), shading interp;
grid off;
axis off;
%colorbar();
view(0,90);
%print('images/Feb22/Curvature_3dirLSIAC_contour','-dpng', '-r500')
%view(100,-14);
%print('images/Feb22/Curvature_3dirLSIAC_contour_AngleView','-dpng', '-r500')


%%

figure(9)
subplot(4,3,1), contour(yVpos,zVpos,upVpos); 
title('u(3dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,2), contour(yVpos,zVpos,vpVpos); 
title('v(3dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,3), contour(yVpos,zVpos,wpVpos); 
title('w(3dir-LSIAC)'); 
grid off, axis off;

subplot(4,3,4), contour(yVpos,zVpos,usxVpos,linspace(min(usxVpos(:)),2.5,20)); 
title('u_x(3dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,5), contour(yVpos,zVpos,vsxVpos,20); 
title('v_x(3dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,6), contour(yVpos,zVpos,wsxVpos,20); 
title('w_x(3dir-LSIAC)'); 
grid off, axis off;

subplot(4,3,7), contour(yVpos,zVpos,usyVpos); 
title('u_y(3dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,8), contour(yVpos,zVpos,vsyVpos); 
title('v_y(3dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,9), contour(yVpos,zVpos,wsyVpos); 
title('w_y(3dir-LSIAC)'); 
grid off, axis off;

subplot(4,3,10), contour(yVpos,zVpos,uszVpos); 
title('u_z(3dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,11), contour(yVpos,zVpos,vszVpos); 
title('v_z(3dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,12), contour(yVpos,zVpos,wszVpos); 
title('w_z(3dir-LSIAC)'); 
grid off, axis off;
%print('images/Feb22/Contour_3dirLSIAC_Allvar','-dpng', '-r500');
%%

figure(10)
subplot(4,3,1), surf(yVpos,zVpos,upVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); 
title('u(3dir-LSIAC)');  
grid off, axis off;
subplot(4,3,2), surf(yVpos,zVpos,vpVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); 
title('v(3dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,3), surf(yVpos,zVpos,wpVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
title('w(3dir-LSIAC)'); 
grid off, axis off;

subplot(4,3,4), surf(yVpos,zVpos,usxVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); 
caxis([min(usxVpos(:)) max(usxVpos(:))-20 ]);
title('u_x(3dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,5), surf(yVpos,zVpos,vsxVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
caxis([min(vsxVpos(:))+4 max(vsxVpos(:)) ]);
title('v_x(3dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,6), surf(yVpos,zVpos,wsxVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
caxis([min(wsxVpos(:)) max(wsxVpos(:))-10 ]);
title('w_x(3dir-LSIAC)'); 
grid off, axis off;

subplot(4,3,7), surf(yVpos,zVpos,usyVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); 
title('u_y(3dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,8), surf(yVpos,zVpos,vsyVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
title('v_y(3dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,9), surf(yVpos,zVpos,wsyVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
title('w_y(3dir-LSIAC)'); 
grid off, axis off;

subplot(4,3,10), surf(yVpos,zVpos,uszVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
title('u_z(3dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,11), surf(yVpos,zVpos,vszVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
title('v_z(3dir-LSIAC)'); 
grid off, axis off;
subplot(4,3,12), surf(yVpos,zVpos,wszVpos); hold on; colorbar(); shading interp; view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar();
title('w_z(3dir-LSIAC)'); 
grid off, axis off;
%print('images/Feb22/Surf_3dirLSIAC_Allvar','-dpng', '-r500');