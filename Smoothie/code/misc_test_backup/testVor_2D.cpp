#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField2D.h>
//#include <MultiRegions/DisContField2D.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();

//	LibUtilities::CommSharedPtr vComm = vSession->GetComm();
//	LibUtilities::FieldIOSharedPtr fld =
  //            MemoryManager<LibUtilities::FieldIO>::AllocateSharedPtr(vComm);

    //LibUtilities::FieldIOSharedPtr fld =
      //      MemoryManager<LibUtilities::FieldIO>::AllocateSharedPtr(vComm,true);

	HNM2D->LoadMesh(var[0]);
	HNM2D->LoadMesh(var[1]);
	HNM2D->LoadMesh(var[2]);
	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce_u,fce_v,sce_u,sce_v,ece_u,ece_v, sce_w;
	fce_u = Array<OneD,NekDouble>(tNquadPts);	fce_v = Array<OneD,NekDouble>(tNquadPts);
	sce_u = Array<OneD,NekDouble>(tNquadPts);	sce_v = Array<OneD,NekDouble>(tNquadPts);
	sce_w = Array<OneD,NekDouble>(tNquadPts);	
//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);
	NekDouble R = 0.2;
	for (int i=0;  i< tNquadPts; i++)
	{
			fce_u[i] = xc1[i];
			fce_v[i] = -1.0*(xc0[i]);
/*		NekDouble r = std::sqrt( (xc1[i]-0.5)*(xc1[i]-0.5) + (xc0[i]-0.5)*(xc0[i]-0.5) );
		if (r <R)
		{ 
			fce_u[i] = (xc1[i] - 0.5)*r/R;
			fce_v[i] = -1.0*(xc0[i] - 0.5)*r/R;
		}
		else
		{
			fce_u[i] = (xc1[i] - 0.5)*R/r;
			fce_v[i] = -1.0*(xc0[i] - 0.5)*R/r;
		}
*/
	}
	
	HNM2D->m_expansions[0]->FwdTrans(fce_u,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	 ece_u = HNM2D->m_expansions[0]->GetPhys();

	HNM2D->m_expansions[1]->FwdTrans(fce_v,HNM2D->m_expansions[1]->UpdateCoeffs() );
	HNM2D->m_expansions[1]->BwdTrans( HNM2D->m_expansions[1]->GetCoeffs(),
										HNM2D->m_expansions[1]->UpdatePhys());
	 ece_v = HNM2D->m_expansions[1]->GetPhys();


	//SmoothieSIAC2D smu(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]) ); 
	//SmoothieSIAC2D smv(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]) ); 
	SmoothieSIAC2D smu(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]),1 ); 
	SmoothieSIAC2D smv(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]),1 ); 


	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	Array<OneD,NekDouble> direction_u(3,0.0),direction_v(3,0.0) ;
	direction_u[1] = 1.0;
	direction_v[0] = 1.0;
	for (int i =0; i <tNquadPts;i++)
	{
		smu.EvaluateAt(xc0[i],xc1[i],xc2[i],sce_u[i],valY,valZ,direction_u,0);
		smv.EvaluateAt(xc0[i],xc1[i],xc2[i],sce_v[i],valY,valZ,direction_v,1);
		sce_w[i] = sce_u[i] - sce_v[i];
	}

	HNM2D->m_expansions[2]->SetPhys(sce_w);
	HNM2D->m_expansions[2]->FwdTrans(sce_w,HNM2D->m_expansions[2]->UpdateCoeffs() );

	NektarBaseClass k;
	//k.printNekArray(xc0,0);
	//k.printNekArray(fce,0);
	//k.printNekArray(ece,0);
	//k.printNekArray(sce,0);
	cout << fname+"_"+argv[2]+"_xc0_OneSided2kp1.txt"<< endl;
	
	k.writeNekArray(xc0,fname+"_"+argv[2]+"_xc0_OneSided2kp1.txt");
	k.writeNekArray(xc1,fname+"_"+argv[2]+"_xc1_OneSided2kp1.txt");
	k.writeNekArray(fce_u,fname+"_"+argv[2]+"_fceu_OneSided2kp1.txt");
	k.writeNekArray(ece_u,fname+"_"+argv[2]+"_eceu_OneSided2kp1.txt");
	k.writeNekArray(sce_u,fname+"_"+argv[2]+"_sceu_OneSided2kp1.txt");
	k.writeNekArray(fce_v,fname+"_"+argv[2]+"_fcev_OneSided2kp1.txt");
	k.writeNekArray(ece_v,fname+"_"+argv[2]+"_ecev_OneSided2kp1.txt");
	k.writeNekArray(sce_v,fname+"_"+argv[2]+"_scev_OneSided2kp1.txt");
	k.writeNekArray(sce_w,fname+"_"+argv[2]+"_scew_OneSided2kp1.txt");

	string out = vSession->GetSessionName() + ".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (HNM2D->m_expansions[0]->GetFieldDefinitions()).at(0);
    FieldDef[0]->m_fields.push_back("u");
    FieldDef[0]->m_fields.push_back("v");
    FieldDef[0]->m_fields.push_back("w");
    FieldDef[0]->m_fields.push_back("test");
    HNM2D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0]);
	HNM2D->m_fld->Write(out, FieldDef, FieldData);

	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
