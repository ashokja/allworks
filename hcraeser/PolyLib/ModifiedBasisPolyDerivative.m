

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %
  %
  %
  %
  %
  %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  function y = ModifiedBasisPolyDerivative(index,maxdegree,x)

  if index == 0
    y = -0.5*ones(size(x));
  elseif index == maxdegree
    y = 0.5*ones(size(x));
  else
    y = -0.5.*x.*JacobiPoly(index-1,x,1,1) + 0.25*(1-x).*(1+x).*JacobiPolyDerivative(index-1,x,1,1);
  end
  
  