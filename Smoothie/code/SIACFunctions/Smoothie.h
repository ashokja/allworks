#pragma once

#include <iostream>
#include <vector>
#include "NektarBaseClass.h"
#include "FilterType.h"
using namespace std;

/// This subclasses of the class can be used to postprocess any point on a given mesh.
/** This class and its subclasses enable users to post process different points the mesh.
	These classes contain methods which are used by the end user of the tool.
*/
class Smoothie: public NektarBaseClass{
	private:
	protected:
	public:
};
