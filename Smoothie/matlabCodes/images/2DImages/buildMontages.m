%creating montage.

close all;
clear;

% figure(1),
% fcell={'AnalyticalFunc.png';'DGFunc.png';'SIACFUNC_X.png';'SIACFUNC_Y.png'};
% montage(fcell)
% 
% figure(2),
% scell={'Cy3l_Re120__81_P_C_v2.jpg';'Cy3l_Re120__81_S_C_v2.jpg';'Cy3l_Re120__81_P_C_v3.jpg';'Cy3l_Re120__81_S_C_v3.jpg'};
% montage(scell)


figure(3),
%SScell= {'vor_DG.png','vor_SI.png','vor_DG1.png','vor_SI1.png','vor_DG_C.png','Cy3l_Re120__81_S_C_v3.jpg'};
SScell= {'vor_DG.png','vor_DG1.png','vor_DG_C.png','vor_SI.png','vor_SI1.png','Cy3l_Re120__81_S_C_v3.jpg'};
img = montage(SScell,'Size', [2 3]);
imsave(img);
%saveas(3,'RE120_P3_SI2.png')
