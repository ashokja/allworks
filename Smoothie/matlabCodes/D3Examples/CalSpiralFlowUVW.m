function [vel] = CalSpiralFlowUVW(X,Y,Z)
    switch nargin
        case 0,
            x = linspace(.1,2,10);
            y = linspace(.1,2,10);
            z = linspace(.1,2,10);

            [X,Y,Z] = meshgrid(x,y,z);
            X = X(:); Y= Y(:); Z=Z(:);
    end

    teta = atan(Y./X);
    a = [X-cos(teta),Y-sin(teta),Z];
    b = [sin(teta),-cos(teta),zeros(size(teta))];
    vel = cross(a,b)+ [cos(teta),sin(teta),zeros(size(teta))]*.1;

    %disp(vel);
    %disp(size(vel));
    %disp(X(1)); disp(Y(1)); disp(Z(1));
    
end
