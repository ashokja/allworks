% saddle test.
% [X,Y] = meshgrid( [-1:.1:1],[-1:.1:1] );
% Z= X.^2 -Y.^2;
% [px,py] = gradient(Z,.2,.2);
% mag = sqrt(px.*px + py.*py);
% px = px./mag;
% py = py./mag;
% 
% quiver(X,Y,px,py);
function [attach_line, sep_line] = FindVorticityTri(coords,vel)

attach_line = [];
sep_line = [];

switch nargin
    case 0
        coords(1,:) = [3,1];    
        coords(2,:) = [1,3];
        coords(3,:) = [-3,2];
        coords= coords;
        vel(1,:) = [6,-2];
        vel(2,:) = [2,-6];
        vel(3,:) = [-6,-4];
end

[a,bc] = gradientTensorTri(coords,vel);

dis = (bc(1,1)+bc(2,2))^2 - 4*(bc(1,1)*bc(2,2) -bc(1,2)*bc(2,1)); %discriminant

if dis < 0
    return;
end

lambda = eig(bc);

if ~isempty(find(lambda==0, 1)) % check if eigen value has zero
 'proper node';
 cont = 0;
 return;
else
 cont =1;
end

    [evec eval] = eig(bc);
    %evec = [3,6;4,8]; % test case.
    tevec = evec.^2;
    magevec = sqrt(sum(tevec));
    evec(1,:) = evec(1,:)./magevec;
    evec(2,:) = evec(2,:)./magevec;

%calculate x_cp and y_cp critical point coords.

    c_1 = (a(2)*bc(1,2) - a(1)*bc(2,2))/(bc(2,2)*bc(1,1) - bc(1,2)*bc(2,1));
    c_2 = (a(2)*bc(1,1) - a(1)*bc(2,1))/(bc(1,2)*bc(2,1) - bc(1,1)*bc(2,2));
    cp = [c_1,c_2];
    coords_new(1,:) = (evec\(coords(1,:)-cp)');
    coords_new(2,:) = (evec\(coords(2,:)-cp)');
    coords_new(3,:) = (evec\(coords(3,:)-cp)');

%determine phase portrait using the eigenvalues tests.
%pha_por = 1(saddle)
    %    = 2(repelling improper node)
    %    = 3(attracting improper node)
    if (prod(lambda) <0)
        pha_por =1;
    elseif lambda(1) <0
        pha_por = 2;
    else
        pha_por = 3;
    end


% attachment lines. Saddle or repelling node.
    count =1;
    attach_line = zeros(0,0);
    if pha_por ==1 || pha_por ==2
        % tri side one.
        n = coords_new(2,:) - coords_new(1,:);
        t = -coords_new(1,1)/n(1);
        if t>=0 && t<=1
            attach_line(count,1) = 0;
            attach_line(count,2) = coords_new(1,2) + n(2)*t;
            count = count+1;
        end
        % tri side two.
        n = coords_new(3,:) - coords_new(2,:);
        t = -coords_new(2,1)/n(1);
        if t>=0 && t<=1
            attach_line(count,1) = 0;
            attach_line(count,2) = coords_new(2,2) + n(2)*t;
            count = count+1;
        end
        % tri side three.
        n = coords_new(1,:) - coords_new(3,:);
        t = -coords_new(3,1)/n(1);
        if t>=0 && t<=1
            attach_line(count,1) = 0;
            attach_line(count,2) = coords_new(3,2) + n(2)*t;
            %count = count+1;
        end
    end
    %map back to original coordinates.
    [num_pts,~] = size(attach_line);
    for i = 1:num_pts
        attach_line(i,:) = (evec*attach_line(i,:)'+cp')';
    end



    % seperation line. Saddle or attracting node.
    count =1;
    sep_line = zeros(0,0);
    if pha_por ==1 || pha_por ==3
        % tri side one.
        n = coords_new(2,:) - coords_new(1,:);
        t = -coords_new(1,2)/n(2);
        if t>=0 && t<=1
            sep_line(count,1) = coords_new(1,1)+ n(1)*t;
            sep_line(count,2) = 0;
            count = count+1;
        end
        % tri side two.
        n = coords_new(3,:) - coords_new(2,:);
        t = -coords_new(2,2)/n(2);
        if t>=0 && t<=1
            sep_line(count,1) = coords_new(2,1)+ n(1)*t;
            sep_line(count,2) = 0;
            count = count+1;
        end
        % tri side three.
        n = coords_new(1,:) - coords_new(3,:);
        t = -coords_new(3,2)/n(2);
        if t>=0 && t<=1
            sep_line(count,1) = coords_new(3,1) + n(1)*t;
            sep_line(count,2) = 0;
            %count = count+1;
        end
    end

    %map back to original coordinates.
    [num_pts,~] = size(sep_line);
    for i = 1:num_pts
        sep_line(i,:) = (evec*sep_line(i,:)'+cp')';
    end

end
%attach_line
%sep_line

    
    
    
    
    
    
    