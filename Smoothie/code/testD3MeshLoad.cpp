#include <cstdio>
#include <cstdlib>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include <LibUtilities/Communication/Comm.h>
#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
#include "SmoothieConfig.h"
#include "NektarBaseClass.h"
#include "NonSymmetricSIAC.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"
#include "HandleNekMesh2D.h"
#include "HandleNekMesh1D.h"


using namespace std;
using namespace Nektar;

int main(int argc, char *argv[])
{
	cout << "SmoothieV: " << Smoothie_VERSION_MAJOR <<endl;
	
	
	argc = 2;
	double startTime = clock();
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);

	
	HandleNekMesh* HNM3D;
	if( atoi(argv[2]) ==3)
		HNM3D = new HandleNekMesh3D(vSession);
	else if( atoi(argv[2]) ==2)
		HNM3D = new HandleNekMesh2D(vSession);
	else //if( atoi(argv[2]) ==1)
		HNM3D = new HandleNekMesh1D(vSession);

	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();

	
	cout << "loading xml file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC
		<< " seconds." << endl;
	startTime = clock();
	string fldname = fname+".fld";

	HNM3D->LoadMesh(var[0]);
	cout << "loading fld file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC
		<< " seconds." << endl;

	startTime = clock();
	//HNM3D->LoadExpListIntoRTree();

	cout << "loading into r-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC
		 << " seconds." << endl;
	HNM3D->CalculateDynamicScaling();

	NekDouble StartX, StartY, StartZ;	
	NekDouble EndX, EndY, EndZ;	


	// pick on arbitrary element.
	// get its local expansion
	// Loop through its vertices and calculate dynamic scaling at its vertices.
	
	Array<OneD,NekDouble> glCoord(3,0.0),lcoord(3,0.0);
	for (int elID = 0 ; elID <= HNM3D->m_expansions[0]->GetExpSize(); elID++)
	{
		SpatialDomains::GeometrySharedPtr geomSptr = HNM3D->m_expansions[0]->GetExp(elID)->GetGeom();
		cout << "shapeType:"<< geomSptr->GetShapeType() << endl;
		for (int i =0; i <geomSptr->GetNumVerts(); i++)
		{
			int Vid = geomSptr->GetVid(i);
			geomSptr->GetVertex(i)->GetCoords(glCoord);
			NekDouble dScaling = HNM3D->GetDynamicScaling(glCoord,elID,1.0);
			geomSptr->GetLocCoords(glCoord,lcoord);
			cout << "\t" << glCoord[0] << "\t" << glCoord[1] << "\t" << glCoord[2] << endl;
			cout << "\t" << (1.0+lcoord[0])/2.0 << (1.0+lcoord[1])/2.0 << "\t" << (1.0+lcoord[2])/2.0 << "\t" << endl;
			cout << "\t" << Vid << "\t" << dScaling << endl;
		}
		cout << endl; 
	}


/*	
	StartX = 0.11; StartY =0.01; StartZ=0.01;
	EndX = 0.6; EndY =0.35; EndZ=0.1;
	Array<OneD,NekDouble> pDynS(1000000,0.0);
	Array<OneD,NekDouble> pX(1000000,0.0),pY(1000000,0.0),pZ(1000000,0.0);
	int elID= -1;
	Array<OneD,NekDouble> coord(3,0.0);
	for (int i = 0; i<100;i++)
	{
		for (int j = 0; j<100;j++)
		{
			for (int k = 0; k<100;k++)
			{
				int index = i*100*100+j*100+k;
			 	coord[0] = StartX + (EndX-StartX)*i/100.0;
			 	coord[1] = StartY + (EndY-StartY)*j/100.0;
			 	coord[2] = StartZ + (EndZ-StartZ)*k/100.0;
			 	pDynS[index] = HNM3D->GetDynamicScaling(coord,elID,1.0);
				pX[index] = coord[0]; pY[index]=coord[1]; pZ[index]=coord[2];
			}
		}
	}
	
	cout << "loaded mesh" << endl;
	NektarBaseClass k;
	
	k.writeNekArray(pX,"SIACxpos.txt");
	k.writeNekArray(pY,"SIACypos.txt");
	k.writeNekArray(pZ,"SIACzpos.txt");
	k.writeNekArray(pDynS,"SIACDynSpos.txt");
*/
    return 0;
}

