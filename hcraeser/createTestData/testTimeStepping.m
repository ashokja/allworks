% test time stepping.
clear;
addpath ../VortexOnBasisEl
load('testdata/testDataSimple_1_05_All_poly_4_11.mat');
count = 1;
%N = (4:1:5);
%drawStreamLine(mapxy,mapxyV,curveCoeff);
hold off

% dataIn is mapxy
% original dataOut os mapxyV
%%
tps_poly = {};
tpsVel_poly = {};
for n = N
    %%
   dataIn = mapxy;
   uCoeffRot = cell2mat(uARot(n));
   uCoeffRed = cell2mat(uARed(n));
   coeff = cell2mat(coeff_poly(n));
   %Finding zeros on faces.
   [sol_seeds,goodSeeds,solAtSeeds] = zerosAtProjected(n,n,n,uCoeffRot);
   %%
   [seedNum,dim] = size(goodSeeds);
   % write a function to reduce duplicates.
    for i = 1:seedNum
      for j = 1:dim
        if (goodSeeds(i,j) ==1)
            for ii = i+1:seedNum
                if (goodSeeds(ii,j) ==1)
                    if ( norm(squeeze(abs(sol_seeds(i,j,:) - sol_seeds(ii,j,:)))) < 10^(-2))
                        goodSeeds(ii,j) = 0;
                    end
                end
            end
        end
      end
    end
   %%
    % using actual velocity.
    DT = (2:1:6);
    for dt = DT
        count =1; tps = {}; tpsVel = {};
        for i = 1:seedNum
            for j = 1:dim
                if (goodSeeds(i,j) ==1)
                    [trace,traceVel] = traceThePoint(n,n,n,coeff, sol_seeds(i,j,:),10^(-dt) );
                    tps(count) = {trace};
                    tpsVel(count) = {traceVel};
                    count = count +1;
                end
            end
        end
        tps_poly(n,dt)= {tps};
        tpsVel_poly(n,dt) = {tpsVel};
    end    
end
save('testdata/testDataSimple_1_05_TimeStepping_poly.mat');
%%
%draw curves,
    for n = N
        for dt = DT
            tps = tps_poly{n,dt};
            [~,b] = size(tps);
            for i =1:b
                trace = cell2mat(tps);
                plot3(trace(:,1),trace(:,2),trace(:,3));hold on;
                %trace = cell2mat(tps{i})
                %celldisp(tps(i))
                %cell2mat(trace)
            end
        end
    end
%     for dt = DT
%         count =1; %tps = {}; tpsVel = {};
%         for i = 1:seedNum
%             for j = 1:dim
%                 if (goodSeeds(i,j) ==1)
%                     [trace,traceVel] = traceThePoint(n,n,n,coeff, sol_seeds(i,j,:),10^(-dt) );
%                     tps(count) = {trace};
%                     tpsVel(count) = {traceVel};
%                     count = count +1;
%                 end
%             end
%         end
%     end    



