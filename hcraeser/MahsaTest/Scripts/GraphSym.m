%Graph union jack 8-113 for P1, P2, DG and PR.

clear;
close all;
%MR = (2:2:16)*7+1;
MR = (2:2:6)*7+1;
%mshEl = [168, 672, 2688, 10752, 43008];
% ord(1) = 336;    ord(2) = 1344;    ord(3) = 3024;   ord(4) = 5376;
% ord(5) = 8400;   ord(6) = 12096;   ord(7) = 16464;   ord(8) = 21504;

mshEl(1) = 168;    mshEl(2) = 672;    mshEl(3) = 1512;   
%mshEl(4) = 2688;
%mshEl(5) = 4200;   mshEl(6) = 6048;   mshEl(7) = 8232;   mshEl(8) = 10752;
mshEl = mshEl*2*2;

addpath('..');
%pPRCartfilename = '../PostP_hexSym_debug/log_hex_sym_PR_MAXSIZE_MR';
%pPRMaxfilename = '../PostP_hexSym_debug/log_hex_sym_PR_MAXSIZE_MR';
%pDGCartfilename = '../PostP7_2/log_hex_unionjack_DG_CART_MR';
%pDGMaxfilename = '../PostP7_1/log_hex_unionjack_DG_MAXSIZE_MR';

pPRCartfilename = '../hexSymTErrorAnalysis/PR_Rect/log_hexT_sym_PR_SIAC_MR';
pPRMaxfilename = '../hexSymTErrorAnalysis/PR_MAX/log_hexT_sym_PR_MAXSIZE_MR';
pDGCartfilename = '../PostP7_2/log_hex_unionjack_DG_CART_MR';
pDGMaxfilename = '../PostP7_1/log_hex_unionjack_DG_MAXSIZE_MR';

width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 0.4;      % LineWidth
msz = 8;       % MarkerSize
atX = '5p8';



P = '1';
KF ='0';
UF = '0';
count =1;
for mr = MR
    filename = strcat(pPRCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_PR_C(count,:) = ReadFile(filename);
    count= count+1;
end
figure(11)
plot(log(mshEl),log(C_P1_PR_C(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P1_PR_C(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_PR_C(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P1_PR_C(:,4)),'-g.'); hold off;
title('PR P1 Filter Cart');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='0';
count =1;
for mr = MR
    filename = strcat(pPRMaxfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_PR_M(count,:) = ReadFile(filename);
    count= count+1;
end
figure(12)
plot(log(mshEl),log(C_P1_PR_M(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P1_PR_M(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_PR_M(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P1_PR_M(:,4)),'-g.'); hold off;
title('PR P1 Filter MaxSize');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='1';
count =1;
for mr = MR
    filename = strcat(pPRCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_PR_H(count,:) = ReadFile(filename);
    count= count+1;
end
figure(13)
plot(log(mshEl),log(C_P1_PR_H(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P1_PR_H(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_PR_H(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P1_PR_H(:,4)),'-g.'); hold off;
title('PR P1 Filter Hex');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

P = '1';
KF ='0';
UF = '0';
count =1;
for mr = MR
    filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_DG_C(count,:) = ReadFile(filename);
    count= count+1;
end
figure(31)
plot(log(mshEl),log(C_P1_DG_C(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P1_DG_C(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_DG_C(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P1_DG_C(:,4)),'-g.'); hold off;
title('DG P1 Filter Cart');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='0';
count =1;
for mr = MR
    filename = strcat(pDGMaxfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_DG_M(count,:) = ReadFile(filename);
    count= count+1;
end
figure(32)
plot(log(mshEl),log(C_P1_DG_M(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P1_DG_M(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_DG_M(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P1_DG_M(:,4)),'-g.'); hold off;
title('DG P1 Filter MaxSize');

KF ='1';
count =1;
for mr = MR
    filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_DG_H(count,:) = ReadFile(filename);
    count= count+1;
end
figure(33)
plot(log(mshEl),log(C_P1_DG_H(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P1_DG_H(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_DG_H(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P1_DG_H(:,4)),'-g.'); hold off;
title('DG P1 Filter Hex');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;





% %use HL and VL
% KF ='0';
% P = '1';
% KF ='0';
% UF = '0';
% count =1;
% for mr = MR
%     filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
%     C_P1_PR_C(count,:) = ReadFile(filename);
%     count= count+1;
% end
% figure(3)
% plot(log(mshEl),log(C_P1_PR_C(:,1)),'-b*'); hold on;
% plot(log(mshEl),log(C_P1_PR_C(:,2)),'-g*'); hold on;
% plot(log(mshEl),log(C_P1_PR_C(:,3)),'-b.'); hold on;
% plot(log(mshEl),log(C_P1_PR_C(:,4)),'-g.'); hold off;
% title('PR P1 Filter EdgeSize');
% 
% %use HL and VL
% KF ='0';
% P = '2';
% KF ='0';
% UF = '0';
% count =1;
% for mr = MR
%     filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
%     C_P2_DG_C(count,:) = ReadFile(filename);
%     count= count+1;
% end
% figure(2)
% plot(log(mshEl(1:4)),log(C_P2_DG_C(1:4,1)),'-r*'); hold on;
% plot(log(mshEl(1:4)),log(C_P2_DG_C(1:4,2)),'-g*'); hold on;
% plot(log(mshEl(1:4)),log(C_P2_DG_C(1:4,3)),'-r.'); hold on;
% plot(log(mshEl(1:4)),log(C_P2_DG_C(1:4,4)),'-g.'); hold off;
% title('DG P2 Filter EdgeSize');
% 







P = '2';
KF ='0';
UF = '0';
count =1;
for mr = MR
    filename = strcat(pPRCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_PR_C(count,:) = ReadFile(filename);
    count= count+1;
end
figure(21)
plot(log(mshEl),log(C_P2_PR_C(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P2_PR_C(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_PR_C(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P2_PR_C(:,4)),'-g.'); hold off;
title('PR P2 Filter Cart');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='0';
count =1;
for mr = MR
    filename = strcat(pPRMaxfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_PR_M(count,:) = ReadFile(filename);
    count= count+1;
end
figure(22)
plot(log(mshEl),log(C_P2_PR_M(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P2_PR_M(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_PR_M(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P2_PR_M(:,4)),'-g.'); hold off;
title('PR P2 Filter MaxSize');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='1';
count =1;
for mr = MR
    filename = strcat(pPRCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_PR_H(count,:) = ReadFile(filename);
    disp(filename);
    count= count+1;
end
figure(23)
plot(log(mshEl),log(C_P2_PR_H(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P2_PR_H(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_PR_H(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P2_PR_H(:,4)),'-g.'); hold off;
title('PR P2 Filter Hex');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;





P = '2';
KF ='0';
UF = '0';
count =1;
for mr = MR
    filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_DG_C(count,:) = ReadFile(filename);
    count= count+1;
end
figure(41)
plot(log(mshEl),log(C_P2_DG_C(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P2_DG_C(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_DG_C(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P2_DG_C(:,4)),'-g.'); hold off;
title('DG P2 Filter Cart');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='0';
count =1;
for mr = MR
    filename = strcat(pDGMaxfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_DG_M(count,:) = ReadFile(filename);
    count= count+1;
end
figure(42)
plot(log(mshEl),log(C_P2_DG_M(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P2_DG_M(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_DG_M(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P2_DG_M(:,4)),'-g.'); hold off;
title('DG P2 Filter MaxSize');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='1';
count =1;
for mr = MR
    filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_DG_H(count,:) = ReadFile(filename);
    disp(filename);
    count= count+1;
end
figure(43)
plot(log(mshEl),log(C_P2_DG_H(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P2_DG_H(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_DG_H(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P2_DG_H(:,4)),'-g.'); hold off;
title('DG P2 Filter Hex');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;




figure(51)
plot(log(mshEl),log(C_P1_PR_C(:,1)),'-bo','LineWidth',lw,'MarkerSize',10); hold on;
%plot(log(mshEl),log(C_P1_PR_C(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_PR_M(:,2)),'-g^','LineWidth',lw,'MarkerSize',10); hold on;
plot(log(mshEl),log(C_P1_PR_H(:,2)),'-r*','LineWidth',lw,'MarkerSize',10); hold on;
set(gca,'fontsize',15)
%title('Projection P1 Filter L2');
xlabel('log(Mesh Size)');
ylabel('log(Error)')
%axis equal;
%legend('Before','Post(Max)','Post(Hex)')
legend('Projection','SIAC','HexSIAC')
X = log(mshEl);
Y = log(C_P1_PR_M(:,2));
k = 1;
m = -1.*(2*k+1)/2;
c = Y(2) - m*X(2);
NY = m.*X+c;
figure(51), hold on;
plot(X,NY,'--k');
print('P1_HexSym_Proj_L2_NQ','-dpng','-r500');

figure(52)
plot(log(mshEl),log(C_P1_PR_C(:,3)),'-bo','LineWidth',lw,'MarkerSize',10); hold on;
%plot(log(mshEl),log(C_P1_PR_C(:,4)),'-go'); hold on;
plot(log(mshEl),log(C_P1_PR_M(:,4)),'-g^','LineWidth',lw,'MarkerSize',10); hold on;
plot(log(mshEl),log(C_P1_PR_H(:,4)),'-r*','LineWidth',lw,'MarkerSize',10); hold on;
set(gca,'fontsize',15)
%title('Projection P1 Filter Linf');
xlabel('log(Mesh Size)');
ylabel('log(Error)')
%axis equal;
%legend('Before','Post(Max)','Post(Hex)')
legend('Projection','SIAC','HexSIAC')
X = log(mshEl);
Y = log(C_P1_PR_M(:,4));
k = 1;
m = -1.*(2*k+1)/2;
c = Y(2) - m*X(2);
NY = m.*X+c;
figure(52), hold on;
plot(X,NY,'--k');
print('P1_HexSym_Proj_Linf_NQ','-dpng','-r500');

% figure(53)
% plot(log(mshEl),log(C_P1_DG_C(:,1)),'-b*'); hold on;
% %plot(log(mshEl),log(C_P1_DG_C(:,2)),'-g*'); hold on;
% plot(log(mshEl),log(C_P1_DG_M(:,2)),'-c*'); hold on;
% plot(log(mshEl),log(C_P1_DG_H(:,2)),'-r*'); hold on;
% title('DG P1 Filter L2');
% xlabel('log(MshSize)');
% ylabel('log(Error)')
% axis equal;
% legend('Before','Post(Cart)','Post(Max)','Post(Hex)')
% 
% figure(54)
% plot(log(mshEl),log(C_P1_DG_C(:,3)),'-bo'); hold on;
% plot(log(mshEl),log(C_P1_DG_C(:,4)),'-go'); hold on;
% plot(log(mshEl),log(C_P1_DG_M(:,4)),'-co'); hold on;
% plot(log(mshEl),log(C_P1_DG_H(:,4)),'-ro'); hold on;
% title('DG P1 Filter Linf');
% xlabel('log(MshSize)');
% ylabel('log(Error)')
% axis equal;
% legend('Before','Post(Cart)','Post(Max)','Post(Hex)')

figure(61)
plot(log(mshEl),log(C_P2_PR_C(:,1)),'-bo','LineWidth',lw,'MarkerSize',10); hold on;
%plot(log(mshEl),log(C_P2_PR_C(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_PR_M(:,2)),'-g^','LineWidth',lw,'MarkerSize',10); hold on;
plot(log(mshEl),log(C_P2_PR_H(:,2)),'-r*','LineWidth',lw,'MarkerSize',10); hold on;
set(gca,'fontsize',15)
%title('Projection P2 Filter L2');
xlabel('log(Mesh Size)');
ylabel('log(Error)')
%axis equal;
%legend('Before','Post(Max)','Post(Hex)')
legend('Projection','SIAC','HexSIAC')
X = log(mshEl);
Y = log(C_P2_PR_M(:,2));
k = 2;
m = -1.*(2*k+1)/2;
c = Y(2) - m*X(2);
NY = m.*X+c;
figure(61), hold on;
plot(X,NY,'--k');
print('P2_HexSym_Proj_L2_NQ','-dpng','-r500');

%%
figure(62)
plot(log(mshEl),log(C_P2_PR_C(:,3)),'-bo','LineWidth',lw,'MarkerSize',10); hold on;
%plot(log(mshEl),log(C_P2_PR_C(:,4)),'-go'); hold on;
plot(log(mshEl),log(C_P2_PR_M(:,4)),'-g^','LineWidth',lw,'MarkerSize',10); hold on;
plot(log(mshEl),log(C_P2_PR_H(:,4)),'-r*','LineWidth',lw,'MarkerSize',10); hold on;
%title('Projection P2 Filter Linf');
set(gca,'fontsize',15)
xlabel('log(Mesh Size)');
ylabel('log(Error)')
%axis equal;
%legend('Before','Post(Max)','Post(Hex)')
legend('Projection','SIAC','HexSIAC')
X = log(mshEl);
Y = log(C_P2_PR_M(:,4));
k = 2;
m = -1.*(2*k+1)/2;
c = Y(2) - m*X(2);
NY = m.*X+c;
figure(62), hold on;
plot(X,NY,'--k');
print('P2_HexSym_Proj_Linf_NQ','-dpng','-r500');




% LC = 11;
% figure(63)
% plot(log(mshEl(1:LC)),log(C_P2_DG_C(1:LC,1)),'-b*'); hold on;
% %plot(log(mshEl(1:LC)),log(C_P2_DG_C(1:LC,2)),'-g*'); hold on;
% plot(log(mshEl(1:LC)),log(C_P2_DG_M(1:LC,2)),'-c*'); hold on;
% plot(log(mshEl(1:LC)),log(C_P2_DG_H(1:LC,2)),'-r*'); hold on;
% title('DG P2 Filter L2');
% xlabel('log(MshSize)');
% ylabel('log(Error)')
% axis equal;
% legend('Before','Post(Cart)','Post(Max)','Post(Hex)')
% 
% figure(64)
% plot(log(mshEl(1:LC)),log(C_P2_DG_C(1:LC,3)),'-bo'); hold on;
% plot(log(mshEl(1:LC)),log(C_P2_DG_C(1:LC,4)),'-go'); hold on;
% plot(log(mshEl(1:LC)),log(C_P2_DG_M(1:LC,4)),'-co'); hold on;
% plot(log(mshEl(1:LC)),log(C_P2_DG_H(1:LC,4)),'-ro'); hold on;
% title('DG P2 Filter Linf');
% xlabel('log(MshSize)');
% ylabel('log(Error)')
% axis equal;
% legend('Before','Post(Cart)','Post(Max)','Post(Hex)')


% % saveas(51,'images/PR_HS_P1_L2.jpg','jpg')
% % saveas(52,'images/PR_HS_P1_LI.jpg','jpg')
% % %saveas(53,'images/DG_P1_L2.jpg','jpg')
% % %saveas(54,'images/DG_P1_LI.jpg','jpg')
% % saveas(61,'images/PR_HS_P2_L2.jpg','jpg')
% % saveas(62,'images/PR_HS_P2_LI.jpg','jpg')
% % %saveas(63,'images/DG_P2_L2.jpg','jpg')
% % %saveas(64,'images/DG_P2_LI.jpg','jpg')

%%
% %%
% % Data tables for P1.
% %L2, DG, SIAC, HSIAC
% %DG
% Yresult(1,:) = mshEl;
% Yresult(2,:) = C_P1_PR_C(:,1);
% Yresult(3,:) = C_P1_PR_M(:,2);
% Yresult(4,:) = C_P1_PR_H(:,2);
% Yresult(5,:) = abs(C_P1_PR_H(:,2) - C_P1_PR_M(:,2));
% input.data = Yresult';
% input.tableColLabels = {'No Mesh El','Projection','SIAC','HSIAC','abs(HSIAC-SIAC)'};
% input.dataFormat = {'%i',1,'%2.3e',4};
% input.tableCaption = strcat('P1 L2 PR data');
% input.tableLabel = strcat('P1_L2_PR');
% latex = latexTable(input);
% 
% % save LaTex code as file
% fid=fopen(strcat('latex/MyLatex_P1_PR.tex'),'w');
% [nrows,ncols] = size(latex);
% for row = 1:nrows
%     fprintf(fid,'%s\n',latex{row,:});
% end
% fclose(fid);
% fprintf('\n... your LaTex code has been saved as ''MyLatex.tex'' in your working directory\n');
% 
% 
% 
% Yresult2(1,:) = mshEl;
% Yresult2(2,:) = C_P2_PR_C(:,1);
% Yresult2(3,:) = C_P2_PR_M(:,2);
% Yresult2(4,:) = C_P2_PR_H(:,2);
% Yresult2(5,:) = abs(C_P2_PR_H(:,2) - C_P2_PR_M(:,2));
% input2.data = Yresult2';
% input2.tableColLabels = {'No Mesh El','Projection','SIAC','HSIAC','abs(HSIAC-SIAC)'};
% input2.dataFormat = {'%i',1,'%2.3e',4};
% input2.tableCaption = strcat('P2 L2 PR data');
% input2.tableLabel = strcat('P2_L2_PR');
% latex = latexTable(input2);
% fid=fopen(strcat('latex/MyLatex_P2_PR.tex'),'w');
% [nrows,ncols] = size(latex);
% for row = 1:nrows
%     fprintf(fid,'%s\n',latex{row,:});
% end
% fclose(fid);
% fprintf('\n... your LaTex code has been saved as ''MyLatex.tex'' in your working directory\n');


%%
%%
% Data tables for P1.
%L2, DG, SIAC, HSIAC
%DG
Yresult(1,:) = mshEl;
Yresult(2,:) = C_P1_PR_C(:,1);
Yresult(3,:) = C_P1_PR_C(:,3);
Yresult(4,:) = C_P1_PR_M(:,2);
Yresult(5,:) = C_P1_PR_M(:,4);
Yresult(6,:) = C_P1_PR_H(:,2);
Yresult(7,:) = C_P1_PR_H(:,4);

%Yresult(4,:) = C_P1_dg_H(:,2);
%Yresult(5,:) = abs(C_P1_dg_H(:,2) - C_P1_dg_M(:,2));
input.data = Yresult';
input.tableColLabels = {'No Mesh El','PR(L2)','PR(Linf)','SIAC(L2)','SIAC(Linf)','HSIAC(L2)','HSIAC(Linf)'};
input.dataFormat = {'%i',1,'%2.3e',6};
input.tableCaption = strcat('P1 LAll PR ');
input.tableLabel = strcat('P1_LAll_PR');
latex = latexTable(input);

% save LaTex code as file
fid=fopen(strcat('latex/MyLatex_P1_LAll_PR.tex'),'w');
[nrows,ncols] = size(latex);
for row = 1:nrows
    fprintf(fid,'%s\n',latex{row,:});
end
fclose(fid);
fprintf('\n... your LaTex code has been saved as ''MyLatex.tex'' in your working directory\n');



Yresult2(1,:) = mshEl;
Yresult2(2,:) = C_P2_PR_C(:,1);
Yresult2(3,:) = C_P2_PR_C(:,3);
Yresult2(4,:) = C_P2_PR_M(:,2);
Yresult2(5,:) = C_P2_PR_M(:,4);
Yresult2(6,:) = C_P2_PR_H(:,2);
Yresult2(7,:) = C_P2_PR_H(:,4);
input2.data = Yresult2';
input2.tableColLabels = {'No Mesh El','PR(L2)','PR(Linf)','SIAC(L2)','SIAC(Linf)','HSIAC(L2)','HSIAC(Linf)'};
input2.dataFormat = {'%i',1,'%2.3e',6};
input2.tableCaption = strcat('P2 LAll PR ');
input2.tableLabel = strcat('P2_LAll_PR_');
latex = latexTable(input2);
fid=fopen(strcat('latex/MyLatex_P2_LAll_PR.tex'),'w');
[nrows,ncols] = size(latex);
for row = 1:nrows
    fprintf(fid,'%s\n',latex{row,:});
end
fclose(fid);
fprintf('\n... your LaTex code has been saved as ''MyLatex.tex'' in your working directory\n');



