% test curve for checking out tangent torsion and normal.
clear all;
close all;

rat = 0.5;
T = (0:.01:pi/2);
R = 50;
Ga = 7;
wa = 2;
c = 1;
for t = T
    data(c,1) = ( Ga + wa*cos(R*t) )*cos(t); 
    data(c,2) = ( Ga + wa*cos(R*t) )*sin(t);
    data(c,3) = wa*sin(R*t); 
    c = c+1;
end

plot3(data(:,1),data(:,2),data(:,3)); hold on;
axis([-10 10 -10 10 -10 10]);




% cr=9*pi;
% %[x_t,y_t,z_t] = meshgrid(t,t,t*2*pi);
% x = r*cos(t*cr);
% y = r*sin(t*cr);
% z = c*t*9;
% 
% xp = r*cos(tp*cr);
% yp = r*sin(tp*cr);
% zp = c*tp*9;
% 
% % velocity
% v_x = -1*r*sin(tp*cr)*cr;
% v_y = r*cr*cos(tp*cr);
% v_z = c*9*ones(size(tp));
% 
% %acceleration
% a_x = -1*r*cos(tp*cr)*cr*cr;
% a_y = -r*cr*sin(tp*cr)*cr;
% a_z = zeros(size(tp));
% 
% %bi normal
% b_x = 1*r*sin(tp*cr)*cr*cr*cr;
% b_y = -r*cr*cos(tp*cr)*cr*cr;
% b_z = zeros(size(tp));
% 
% figure(1),
% plot3(x,y,z); hold on;
% quiver3(xp,yp,zp,v_x,v_y,v_z); hold on;
% quiver3(xp,yp,zp,a_x,a_y,a_z); hold on;
% quiver3(xp,yp,zp,b_x,b_y,b_z); hold on;
% 
