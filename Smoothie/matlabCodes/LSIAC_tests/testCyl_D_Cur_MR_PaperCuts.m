%testCyl_D_Cur_MR_Paper_SubImages.
% what is the file name used here.

clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [81];

poly =2;
order = 2;
alpha =1;
count =1;

fileName = 'Cy3l_Re500_';

spacingStrgs={'0.3285';'0.657';'0.9855'};

%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize
atX = '5p8';

Cyl_Max = 6.3;
Cyl_Min = -5.2571;

for count = 1:max(size(spacingStrgs))
%for    spacingStrg = spacingStrgs
    spacingStrg = char(spacingStrgs(count));
    disp(spacingStrg);
    r = 81;
    filenameFrPrint = strcat(fileName,int2str(poly),'_');
    xfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pX_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    yfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pY_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    pUfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pU_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pVfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pV_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pPfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pP_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    pUxfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUx_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pVxfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pPxfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPx_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    pUyfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pVyfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVy_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pPyfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPy_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    pU_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pU_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pV_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pV_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pP_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pP_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    pUx_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUx_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pVx_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pPx_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPx_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    pUy_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pVy_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVy_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pPy_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPy_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );

    pAu_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pAu_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pAv_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pAv_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pC_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pC_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );

    pAu_fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pAu_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pAv_fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pAv_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pC_fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pC_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    pI_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pI_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pI_fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pI_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');
    
    
    pUval = textread(pUfname, '%f\t');  pUxval = textread(pUxfname, '%f\t');    pUyval = textread(pUyfname, '%f\t');
    pVval = textread(pVfname, '%f\t');  pVxval = textread(pVxfname, '%f\t');    pVyval = textread(pVyfname, '%f\t');
    pPval = textread(pPfname, '%f\t');  pPxval = textread(pPxfname, '%f\t');    pPyval = textread(pPyfname, '%f\t');

    pU_pval = textread(pU_pfname, '%f\t');  pUx_pval = textread(pUx_pfname, '%f\t');    pUy_pval = textread(pUy_pfname, '%f\t');
    pV_pval = textread(pV_pfname, '%f\t');  pVx_pval = textread(pVx_pfname, '%f\t');    pVy_pval = textread(pVy_pfname, '%f\t');
    pP_pval = textread(pP_pfname, '%f\t');  pPx_pval = textread(pPx_pfname, '%f\t');    pPy_pval = textread(pPy_pfname, '%f\t');

    pAu_pval = textread(pAu_pfname, '%f\t');   pAu_val = textread(pAu_fname, '%f\t');
    pAv_pval = textread(pAv_pfname, '%f\t');   pAv_val = textread(pAv_fname, '%f\t');
    pC_pval = textread(pC_pfname, '%f\t');     pC_val = textread(pC_fname, '%f\t');
    
    pI_pval = textread(pI_pfname, '%f\t');     pI_val = textread(pI_fname, '%f\t');
    
    s = sqrt(max(size(xpos)));
    xVpos = reshape(xpos,s,s);    yVpos = reshape(ypos,s,s);
    pUVal = reshape(pUval,s,s);   pVVal = reshape(pVval,s,s);   pPVal = reshape(pPval,s,s);
    pUxVal = reshape(pUxval,s,s);   pVxVal = reshape(pVxval,s,s);   pPxVal = reshape(pPxval,s,s);
    pUyVal = reshape(pUyval,s,s);   pVyVal = reshape(pVyval,s,s);   pPyVal = reshape(pPyval,s,s);
    
    pU_pVal = reshape(pU_pval,s,s);   pV_pVal = reshape(pV_pval,s,s);   pP_pVal = reshape(pP_pval,s,s);
    pUx_pVal = reshape(pUx_pval,s,s);   pVx_pVal = reshape(pVx_pval,s,s);   pPx_pVal = reshape(pPx_pval,s,s);
    pUy_pVal = reshape(pUy_pval,s,s);   pVy_pVal = reshape(pVy_pval,s,s);   pPy_pVal = reshape(pPy_pval,s,s);
    
    pAu_pVal = reshape(pAu_pval,s,s);   pAu_Val = reshape(pAu_val,s,s);
    pAv_pVal = reshape(pAv_pval,s,s);   pAv_Val = reshape(pAv_val,s,s);
    pC_pVal = reshape(pC_pval,s,s);     pC_Val = reshape(pC_val,s,s);
    
    pI_pVal = reshape(pI_pval,s,s);     pI_Val = reshape(pI_val,s,s);
    
    
   figure(23)
   contour(xVpos,yVpos, -1.0*pI_Val,(Cyl_Min :.3: Cyl_Max),'LineWidth',lw ); 
   axis off;
 
   result(count,:,:) = pI_Val;
end   


limits = [2.5 4.5];
figure(1)
contour(xVpos,yVpos, -1.0*squeeze(result(1,:,:)),(Cyl_Min :.3: Cyl_Max),'LineWidth',lw ); 
 axis equal;xlim(limits);
 axis off;
 grid off;
%print(strcat('paperUseImagesF/','Cy3l_2_Scaling_LSIAC_Contour_p1_c4'), '-dpng','-r500');

figure(2)
contour(xVpos,yVpos, -1.0*squeeze(result(2,:,:)),(Cyl_Min :.3: Cyl_Max),'LineWidth',lw ); 
 axis equal;xlim(limits);
 axis off;
 grid off;
%print(strcat('paperUseImagesF/','Cy3l_2_Scaling_LSIAC_Contour_p2_c4'), '-dpng','-r500');

  figure(3)
contour(xVpos,yVpos, -1.0*squeeze(result(3,:,:)),(Cyl_Min :.3: Cyl_Max),'LineWidth',lw ); 
 axis equal;xlim(limits);
 axis off;
 grid off;
%print(strcat('paperUseImagesF/','Cy3l_2_Scaling_LSIAC_Contour_p3_c4'), '-dpng','-r500');

 

