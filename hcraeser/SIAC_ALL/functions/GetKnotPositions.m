%GetKnotPositions.
function [a] = GetKnotPositions(order,scaling,offset)
    switch nargin
        case 0
            disp('order needs to specified. Assuming for debugging.');
            order =1.0;
            scaling =1.0;
            offset=0.0;
        case 1
            scaling =1.0;
            offset=0.0;
        case 2
            offset=0.0;
    end


    switch order
        case 1
            a = -2:1:2;
        case 2
            a = -3.5:1:3.5;
        otherwise
            a = 'null';
            disp('unknown order was requested');
    end
    a = a*scaling+offset;
end