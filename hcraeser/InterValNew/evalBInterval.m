% test Bernstein function.

function res = evalBInterval(i,coeff,X,Y)

    switch nargin
        case 0,
    end
    
    [~,sc] = size(coeff);
    n = sc-1;
    if (i ==1)
        % B value.
        res = B_evalX(n,coeff,X);
    elseif (i ==2)
        % B derivative Values.
        res = DB_evalX(n,coeff,X);
    end
    
    switch nargin
        case 4,
        dataIn = (-1:.1:1);
        y = B_eval(n,coeff,dataIn);
        plot(dataIn,y); grid on;hold on;
    
        dataIn = (-1:.1:1);
        y = B_der_eval(n,coeff,dataIn);
        plot(dataIn,y,'r'); hold off;
    end
%     dataIn = (-1:.1:1);
%     y = B_eval(n,coeff,dataIn);
%     plot(dataIn,y); grid on;hold on;
%     
%     dataIn = (-1:.1:1);
%     y = B_der_eval(n,coeff,dataIn);
%     plot(dataIn,y,'r'); hold off;
%     
%     %%% Experiment code.
%     figure(2)
%     y = B_der_eval(4,[0,0,1,0,0],dataIn);
%     plot(dataIn,y,'r*'); hold on;
% 
%     y1 = B_eval(3,[0,1,0,0],dataIn);
%     y2 = B_eval(3,[0,0,1,0],dataIn);
%     plot(dataIn,2*(y1-y2),'b'); hold off;
%     
%     figure(3)
%         y = B_eval(3,[0,1,0,0],dataIn);
%     plot(dataIn,y,'r*'); hold on;
%         y = B_eval(3,[0,0,1,0],dataIn);
%     plot(dataIn,-1*y,'r*'); hold off;
    %%% Remove after use.
    
    function rest = B_evalX(n,coeff,X)
       count =1;
       rest = zeros(size(X));
       for k = 0:n
           r = [];
           r(end+1) = B(n,k,X(1));
           r(end+1) = B(n,k,X(2));
           if ( (X(1) < (2*k/n-1)) && (X(2) > (2*k/n-1)) )
                   r(end+1) = B(n,k,2*k/n-1); 
           end
           %r
           rt(1) = min(r);
           rt(2) = max(r);
           rest = op('+',rest, op('*',[coeff(count),coeff(count)],rt));
           count= count +1;          
       end 
    end

    function rest = B_eval(n,coeff,dataIn)
       count =1;
       rest = zeros(size(dataIn));
       for k = 0:n
           r = coeff(count)*B(n,k,dataIn);
           rest = rest  + r;
           count= count +1;          
       end
    end

    function res = non_corrected_DB_evalX(n,coeff,X)
       count =1;
       res = zeros(1,2);
       for k = 0:n
           r = [];
           if( k>0 )
             if ( (X(1)<2*(k-1)/(n-1)-1) && (X(2)>2*(k-1)/(n-1)-1) )
                r(end+1) = B_der(n,k,2*(k-1)/(n-1)-1);
             end
           end
           if(k < n)
             if ( (X(1)<2*(k)/(n-1)-1) && (X(2)>2*(k)/(n-1)-1) )
                 r(end+1) = B_der(n,k,2*(k)/(n-1)-1);
             end
           end
           r(end+1) =  B_der(n,k,X(1) );
           r(end+1) =  B_der(n,k,X(2) );
           r
           rt(1) = min(r);
           rt(2) = max(r);

           res = op('+',res, op('*',[coeff(count),coeff(count)],rt));
           count= count +1;          
       end
    end
    
    function res = DB_evalX(n,coeff,X)
       count =1;
       res = zeros(1,2);
       for k = 0:n
           rt = op('*',[n/2,n/2],op('-',BX(n-1,k-1,X),BX(n-1,k,X)));
           res = op('+',res, op('*',[coeff(count),coeff(count)],rt));
           count= count +1;          
       end
    end

    function [res] = B(n,k,data)
        % assumed data varies from -1 to 1
        if n<k || k<0
            res = zeros(size(data));
            return
        end
        data = (data+1)/2;
        res = nchoosek(n,k)*((data.^k).*((1-data).^(n-k)));
    end

    function rt = BX(n,k,X)
        r = [];
        r(end+1) = B(n,k,X(1));
        r(end+1) = B(n,k,X(2));
        if ( (X(1) < (2*k/n-1)) && (X(2) > (2*k/n-1)) )
           r(end+1) = B(n,k,2*k/n-1); 
        end
        rt(1) = min(r);
        rt(2) = max(r);
    end

    function [res] = B_der(n,k,data)
        res = n*(B(n-1,k-1,data) - B(n-1,k,data))/2;
    end
    
    function [rest] = B_der_eval(n1,coeff,dataIn)
       count =1;
       [a,b] = size(dataIn);
       rest = zeros(a,b);
       for k1 = 0:n1
           rest = rest + coeff(count)*B_der(n1,k1,dataIn);
           count= count +1;          
       end
    end

end

% test case to plot Berns and Berns Derivative.
%     data = (-1:.01:1);
%     n = 8;
%     figure(1);
%     for k = 0:n
%         y = B_der(n,k,data);
%         plot(data,y); hold on;
%     end
%     hold off;
%     
%     figure(2);
%     for k = 0:n
%         y = B(n,k,data);
%         plot(data,y); hold on;
%     end
%     hold on;
%     
%     data = (-1:2*(1/n):1);
%     y = 0.3*ones(size(data));
%     plot(data,y,'r*');
%     hold off.
