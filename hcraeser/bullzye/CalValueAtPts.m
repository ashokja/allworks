function [solPts] = CalValueAtPts(coeff,Pts)
switch nargin
    case 0,
        n = 3
        xcoeff = zeros(1,n);
        ycoeff = linspace(-1,1,n);
        zcoeff = zeros(1,n);
        coeff = [xcoeff;ycoeff;zcoeff];
        Pts = [linspace(-1,1,10);linspace(-1,1,10);linspace(-1,1,10)]';
end
    solPts = zeros( max(size(Pts)),1);
    [NX,NY,NZ] = size(coeff);
    NX = NX-1; NY = NY-1; NZ = NZ-1; % since coeff have order and NX is degree.
    for i = 0:NX
        for j = 0:NY
            for k = 0:NZ
                solPts = solPts+coeff(i+1,j+1,k+1)*...
                        LegendrePoly(i,Pts(:,1)).*LegendrePoly(j,Pts(:,2)).*LegendrePoly(k,Pts(:,3));
%                    outer(outer(LegendrePoly(i,XQ),LegendrePoly(j,YQ),2),LegendrePoly(k,ZQ),3);
            end
        end
    end
end