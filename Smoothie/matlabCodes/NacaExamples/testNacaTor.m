% test naca for calculating torsion across a plane.
clear;
close all;

UsebkupData = 'bkup_SI_TOR/'
%UsebkupData = ''
%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05'
filename = strcat('P0000009_So_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ATR2Var' );
filepath = strcat('data/',UsebkupData,filename);
disp(filepath);


xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );
torSfname =  strcat(filepath,'_pTor_s.txt' );

xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');

torSval = textread(torSfname, '%f\t');

epos = floor(epos +0.0001);

s = floor( (max(size(xpos)))^(1/2) +0.1);

%upfname = strcat(filepath,'_pU_p.txt' );

xVpos = reshape(xpos,s,s);
yVpos = reshape(ypos,s,s);
zVpos = reshape(zpos,s,s);


torSVval = reshape(torSval,s,s);



figure(2);
surf(yVpos,zVpos,torSVval)
shading interp;
grid on;
grid minor


figure(3);
contourf(yVpos,zVpos,torSVval)
contourcbar
title('Torsion Contour (1)');
print('torsion_contour_t1','-dpng','-r300');

UsebkupData = 'bkup_SI_TOR/'
filename = strcat('P0000009_So_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ATRVar' );
filepath = strcat('data/',UsebkupData,filename);
disp(filepath);

torPfname =  strcat(filepath,'_pTor_p.txt' );
torPval = textread(torPfname, '%f\t');
torPVval = reshape(torPval,s,s);

figure(1);
surf(yVpos,zVpos,torPVval)
shading interp;

figure(4);
contourf(yVpos,zVpos,torPVval)
contourcbar
