%u_CartSIAC_1D_1Sided

% open SIAC test.

%% Test mass matrix mapping with more mapping points.
clear;
close all;
addpath('../PolyLib');
addpath('../SIAC');
addpath('functions');
Resols = [20]; 

Degrees =(2);
count=1;
for Deg = Degrees
   for Nv = Resols
       clearvars -except Deg Resols Nv count  Err_siac Err_proj
        Ne = Nv-1;
        a = 0; b = 2*pi;
        filter_H = (b-a)/Ne;
        filter_m = 0.5;
        v_x = a+ linspace(0,1,Nv)*(b-a);
        siacSamples = Deg+3;
        vizSamples= Deg+3;
        np = Deg+3;
        [r,w] = JacobiGLZW(np,0,0);

        x = zeros(np,Ne);
        h_e = zeros(Ne,1);
        for e = 1:Ne
            x(:,e) = v_x(e) + (r+1)/2*(v_x(e+1)-v_x(e));
            h_e(e) = v_x(e+1)-v_x(e);
        end
        % calculate u_act using function 
        u_act = sin(x);

        %Build and use mass matrix for coeffecints.
        M = zeros(Deg+1,Deg+1);
        for i = 0:Deg
            for j =0:Deg
                M(i+1,j+1) = sum( JacobiPoly(i,r,0,0) ...
                                    .*JacobiPoly(j,r,0,0) .* ...
                                    w);
            end
        end

        rhs = zeros(Deg+1,1);
        u_hat = zeros(Deg+1,Ne);
        for e =1:Ne
            for i=0:Deg
                rhs(i+1) = sum(JacobiPoly(i,r,0,0).*w.*u_act(:,e));
            end
            u_hat(:,e) = M\rhs;
        end

        %evaluate at only quadrature points.
        %
        [rviz,wviz] = JacobiGLZW(vizSamples,0,0);
        x_viz = zeros(vizSamples,Ne);
        for e = 1:Ne
            x_viz(:,e) = v_x(e) + (rviz+1)/2*(v_x(e+1)-v_x(e));
        end

        Vld_viz = zeros(vizSamples,Deg+1);
        for d = 0:Deg
           Vld_viz(:,d+1) = JacobiPoly(d,rviz,0,0); 
        end

        u_sim_viz = Vld_viz*u_hat;

        u_er_P_Act(Deg) = sum(sum(abs( (u_sim_viz-sin(x_viz))/max(size(x_viz(:))))));
        figure(1),
        subplot(2,2,1),plot(x_viz,u_sim_viz); 
        title('Function Projection');
        subplot(2,2,2),plot(x_viz,u_sim_viz-sin(x_viz)); 
        title('Projection Error');

        % let us try to evaluate at all the quass qudrature points.
        % 1st loop through all the points for evaluations.
        % Basing the filter at points. Find intersections and elments.
        % Break the elements using the overlap.
        % integrate on each element.

        % do siac filtering at these points. 
        [r_sEval,~] = JacobiGLZW(siacSamples,0,0);
        x_sEval = zeros(siacSamples,Ne);
        for e= 1:Ne
            x_sEval(:,e) = v_x(e) + (r_sEval+1)/2*(v_x(e+1)-v_x(e));
        end
        % Select which degree siac filter we will be using.
        Sorder =Deg;
        %here Sorder is degree not order, it represents degree.
        [sz_points,sz_elmts] = size(x_sEval);
        numIntPts =ceil((3*(Sorder)+3)/2);
        [p,w]= JacobiGZW(numIntPts,0,0);
        Debug =0;
         for elid = 1:sz_elmts
             for ptid = 1:sz_points
    %    for elid = 10
    %        for ptid = 1
                pt = x_sEval(ptid,elid);
                Sscaling = x_sEval( end,elid) - x_sEval(1,elid);
                knot_positions = GetKnotPositions_1Sided( Sorder, Sscaling, pt,a,b);
                % intersect 1D meshes with 1D filter here.
                AllElms = sort([v_x,knot_positions]);
                [~,b_ren,~] = find((AllElms >= knot_positions(1))& (AllElms <= knot_positions(end)) );
                nMshElm = AllElms(b_ren);
                if Debug
                    close
                    figure(3)
                    plot([v_x,v_x-max(max(v_x)),v_x+max(max(v_x))], ones(size([v_x,v_x-max(max(v_x)),v_x+max(max(v_x))]))+1.2,'-*r'); hold on;
                    plot(knot_positions, ones(size(knot_positions))+1.5,'-*b'); hold on;
                    plot(nMshElm, ones(size(nMshElm))+1.7,'-og'); hold off;
                    axis([-inf,inf,0,5])
                end
                % for each element in nMshElm
                % calculate integral.
                integral = 0.0;
                offset = pt;
                for Ielid = 1:max(size(nMshElm))-1
                    %check if each element has enough size.
                    v1 = nMshElm(Ielid); v2 = nMshElm(Ielid+1);
                    if (abs(v2-v1) > 1e-12)
                        % calculate integral.
                        intPts = v1+ (v2-v1)*(p+1)/2;
                        %sval = SIAC(intPts-pt,Sscaling,Sorder);
                        [lambda,sym] = calculateLambda(offset,a,b,Sscaling,2*Sorder,Sorder+1);
                        if sym
                            sval = SIAC(intPts-pt,Sscaling,Sorder);
                        else
                            sval = SIAC_filter(intPts-pt+lambda*Sscaling,Sscaling,Sorder,lambda);
                            %sval = SIAC_filter_VAN(intPts-pt+lambda*Sscaling,Sscaling,Sorder,lambda,4*Sorder);
                        end

                        elVal = GetFValPeriodic(v1,v2,p,v_x,u_hat);
                        integral = integral + sum(elVal.*sval.*w*abs(v2-v1)/2);
                        %integral = integral + sum(1.0.*sval.*w*abs(v2-v1))/2;
                        % debug for calculating the area of filter.
                        %integral = integral + abs(v2-v1);
                    end
                end
                integral;
                u_siac(ptid,elid) = integral;
            end
        end
        u_siac;

        subplot(2,2,3),plot(x_viz,u_siac); 
        title('Function Projection');
        figure(3)
        plot(x_viz,log10(abs(u_siac-u_act))); hold on;
        xlabel('x 0 to 2pi');ylabel('log10(error)');
        title('Projection Error');
    %     % 
    %     figure(2)
    %     plot(x_viz,u_sim_viz-sin(x_viz)); hold on;
    %     plot(x_viz,u_siac-u_act); 
        Err_siac(count) = L2(u_siac-u_act);
        Err_proj(count) =  L2(u_sim_viz-u_act);
        count = count+1;
   end
end
%legend('10','20','40','80');
%%
figure (2)
plot(log10(Resols),log10(Err_proj(:)),'-*'), hold on;
plot(log10(Resols),log10(Err_siac(:)),'-*'), hold off;
xlabel('log10(Mesh Resol)'); ylabel('log10(Error)');
legend('projection','SIAC')
title('1D projection SIAC DEG(2)')

