function [v_eval] = SIAC_filter_VAN(x_eval,h,k,lambda,r)

    switch nargin
        case 0
            x_eval = -1:.1:1;
            h = 1.0;
            k = 1;
            lambda = 0;
            r = 2*k;
            r = 4*k;
        case 3
            lambda = 0;
            r = 2*k;
    end
    
    coeff = SIAC_coeff(lambda, k,r);
    
    count =1;
    v_eval = zeros(size(x_eval));
    for it = -r/2:1:r/2
        v_eval= v_eval+ coeff(count)*BSpl_central(x_eval/h+it,k);
        count= count+1;
    end
    v_eval= v_eval/h;
    
    
end