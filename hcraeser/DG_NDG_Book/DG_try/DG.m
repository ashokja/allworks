% simulation start engine.
% 1-D DG simulation engine.
clear;
a = 0;
b = 4*pi; % pi
a_f = .25;
num_el = 20;
basis =9;
FinalTime =1.9;
TimeStamp = [0,.2,1];
% Periodic boundary conditions. node(1) = node(num_nodes)
% Low storage Runge-Kutta coefficients
rk4a = [            0.0 ...
        -567301805773.0/1357537059087.0 ...
        -2404267990393.0/2016746695238.0 ...
        -3550918686646.0/2091501179385.0  ...
        -1275806237668.0/842570457699.0];
rk4b = [ 1432997174477.0/9575080441755.0 ...
         5161836677717.0/13612068292357.0 ...
         1720146321549.0/2090206949498.0  ...
         3134564353537.0/4481467310338.0  ...
         2277821191437.0/14882151754819.0];
rk4c = [             0.0  ...
         1432997174477.0/9575080441755.0 ...
         2526269341429.0/6820363962896.0 ...
         2006345519317.0/3224310063776.0 ...
         2802321613138.0/2924317926251.0];

hx = (b-a)/(num_el);
     
% initialization.
%elements 

num_nodes = num_el+1;
x = (a:(b-a)/(num_el):b) ;

el = zeros(num_el,2);
el(:,1) = (1:num_nodes-1);
el(:,2) = (2:num_nodes);

u_start = sin(x);
%plot(x,u_start);
%grid on;

% each element should have basis number of coeff.
el_x_basis = zeros(num_el,basis);
r = flipud(lglnodes(basis-1));
r_t = (r+1)/2;

for i = 1:basis
    el_x_basis(:,i) = x(1:end-1)+ r_t(i)*(b-a)/(num_el) ;
end

u_basis_val = sin(el_x_basis*4); 
u_basis_coeff = zeros(size(el_x_basis));
% loop through each element and find its coeffcients.
% calculate u_basis_coeff.
for e_i  = 1: num_el
    u_basis_coeff(e_i,:) = Bern_coeff( u_basis_val(e_i,:) );
end

temp_basis_coeff = u_basis_coeff;

% calculate [[u]] and {{u}} assuming periodic boundary conditions.

u_sq_br = zeros(num_el,2);
u_cur_br = zeros(num_el,2);

% For simulation purposes. We have to calculate for each element.
%   We need mass Matrix and Stifness matrix for each element. Since basis are
% equal in all the elements

%Given 
%    el_x_basis
%    u_basis_val
%    u_basis_coeff
%   We need to calculate du(du/dt) = -a*(inv(M)*S)*u(coef) + flux (for each element.)
du = zeros(num_el,basis);
alpha = 1; %upwind flux. % 1 for central flux;
M = MassMatrixB(basis)*hx;
S = StiffMatB(basis);

%every time step. All terms in this need to be updated.
%Compute the time Step size.
time =0;
resu = zeros(size(u_basis_coeff));
xmin = min(min(abs(el_x_basis(:,1:end-1) - el_x_basis(:,2:end))));
%xmin = (b-a)/(num_el+1)/basis;
CFL = 0.75; dt = CFL/(a_f)*xmin; dt = 0.5*dt;
%dt = 0.0005;
Nsteps = ceil(FinalTime/dt);
dt = FinalTime/Nsteps;
TimeStamp = floor(TimeStamp*Nsteps);
TimeStamp(1) =1;
%dt = 0.0001;
% Nsteps =1;
% outer time step loop
t_sampler =1;
u_coeff_stamp = zeros(max(size(TimeStamp)),num_el,basis);
for tstep = 1: Nsteps
    if ( tstep == TimeStamp(t_sampler) )
        u_coeff_stamp(t_sampler,:,:) = u_basis_coeff;
        t_sampler =t_sampler +1;
    end
    for INTRK = 1:5
        timelocal = time + rk4c(INTRK)*dt;
        for e_i = 1:num_el
            % left border.
            if 1 == e_i
                u_sq_br(1,1) = u_basis_coeff(1,1) + u_basis_coeff(num_el,basis);
               u_cur_br(1,1) = -u_basis_coeff(1,1) + u_basis_coeff(num_el,basis); 
            else
                u_sq_br(e_i,1) = u_basis_coeff(e_i,1) + u_basis_coeff(e_i-1,basis);
               u_cur_br(e_i,1) = -u_basis_coeff(e_i,1) + u_basis_coeff(e_i-1,basis);
            end
            %right border
            if num_el == e_i
                u_sq_br(num_el,2) = u_basis_coeff(num_el,basis) + u_basis_coeff(1,1);
               u_cur_br(num_el,2) = u_basis_coeff(num_el,basis) - u_basis_coeff(1,1);
            else
                u_sq_br(e_i,2) = u_basis_coeff(e_i,basis) + u_basis_coeff(e_i+1,1);
               u_cur_br(e_i,2) = u_basis_coeff(e_i,basis) - u_basis_coeff(e_i+1,1);
            end
        end
        u_sq_br = u_sq_br/2;
        u_star = u_cur_br +(1-alpha)/2*u_sq_br;
        for e_i = 1: num_el
            %flux term
            u_flux = zeros(1,basis);
            %u_flux(1,basis) = a_f*( u_basis_coeff(e_i,basis) - u_star(e_i,2) );
            %u_flux(1,1) = - a_f * ( u_basis_coeff(e_i,1) - u_star(e_i,1) );
            u_flux(1,basis) = a_f*( - u_star(e_i,2) );
            u_flux(1,1) = -a_f*( - u_star(e_i,1) );
                % project back and forward. (tested and failed.)
                    %u_el_val = funcEvalBerns(S*a_f*u_basis_coeff(e_i,:)',r_t);
                    %u_el_val_fl = u_el_val+u_flux';
                    %u_el_coeff = Bern_coeff(u_el_val_fl');
                    %du(e_i,:) = M\( u_el_coeff );
			du(e_i,:) = M\( +a_f*S*u_basis_coeff(e_i,:)'+u_flux');
        end
        resu = rk4a(INTRK)*resu +dt*du;
        u_basis_coeff = u_basis_coeff + rk4b(INTRK)*resu;
    end
    time = time+dt;
end

sampling = (0:.2:1);
sampling(1) = 0.001;
sampling(end) = 1-0.001;
[del,num_samp] = size(sampling);
u_data = zeros(max(size(TimeStamp)),num_el * num_samp,1);
x_data = zeros(num_el * num_samp,1);
for ts = 1:max(size(TimeStamp))
    for e_i = 1: num_el
        u_data(ts,(e_i-1)*num_samp+1:e_i*num_samp) = funcEvalBerns( u_coeff_stamp(ts,e_i,:), sampling);
        x_data((e_i-1)*num_samp+1:e_i*num_samp) = el_x_basis(e_i,1)+ (hx)*sampling;
    end
end
%plot(x(1:end-1),u_basis_coeff(:,2),x(1:end-1),u_basis_coeff(:,3),x(1:end-1),u_basis_coeff(:,4))
%grid on;
%hold;
%plot(x(1:end-1),u_sq_br)
figure (1)
plot(x_data,u_data(3,:),'-+');
figure (2) 
%hold on;
plot(x_data,sin(x_data*4+1.9));
%hold off;
figure (3)
%hold on;
plot(x_data,u_data(3,:) - sin(x_data*4+1.9)');

%hold on;
%plot(el_x_basis(:,1),el_x_basis(:,1)*0,'o');