%create Citical data load for Finite differencing.

close all;
clear;

rate = 2;
DgGrid = 21;

%lattices = [11,21,31,51,61,71,81,91,101];
%lattices = [11,21,41,51,81,101,121,131];
lattices= 61;
%lattices = [11,21,41,61,81,121,141,161,201]; %p3,41

a(1,:) = [0.74;+0.35];
a(2,:) = [0.68;-0.59];
a(3,:) = [-0.11;-0.72];
a(4,:) = [-0.58;-0.64];
a(5,:) = [0.51;+0.27];
a(6,:) = [-0.12;-0.84];

for loop = 1:max(size(lattices))
    latticeGrid = lattices(loop);
    filename = strcat('P',int2str(rate),'_quad_simple_',int2str(DgGrid),'_',int2str(latticeGrid),'_Sample');

    xfname = strcat('../../data/NekMeshes/',filename,'_x.txt' );
    yfname = strcat('../../data/NekMeshes/',filename,'_y.txt' );

    ufname = strcat('../../data/NekMeshes/',filename,'_u.txt' );
    vfname = strcat('../../data/NekMeshes/',filename,'_v.txt' );


    xv = textread(xfname, '%f\t');
    yv = textread(yfname, '%f\t');

    uv = textread(ufname, '%f\t');
    vv = textread(vfname, '%f\t');

    xV = reshape(xv, [latticeGrid,latticeGrid]);
    yV = reshape(yv, [latticeGrid,latticeGrid]);

    % testcase
    %uV = xV-0.5;
    %vV = yV-0.5;

    uV = reshape(uv, [latticeGrid,latticeGrid]);
    vV = reshape(vv, [latticeGrid,latticeGrid]);

    uxV = zeros(size(uV));
    uyV = zeros(size(uV));
    vxV = zeros(size(uV));
    vyV = zeros(size(uV));

    figure(1)
      subplot(2,1,1)
    surf(xV,yV,uV); hold on;
           subplot(2,1,2)
    surf(xV,yV,vV);

    h = 1/(latticeGrid-1);
    for i = 2:latticeGrid-1
        for j = 2:latticeGrid-1
            uxV(i,j) = ( uV(i,j+1) - uV(i,j-1) )/(2*h);
            uyV(i,j) = ( uV(i+1,j) - uV(i-1,j) )/(2*h);
            vxV(i,j) = ( vV(i,j+1) - vV(i,j-1) )/(2*h);
            vyV(i,j) = ( vV(i+1,j) - vV(i-1,j) )/(2*h);
        end
    end

    figure(2)
    subplot(2,2,1)
    surf(xV(2:end-1,2:end-1),yV(2:end-1,2:end-1),uxV(2:end-1,2:end-1));
    subplot(2,2,2)
    surf(xV(2:end-1,2:end-1),yV(2:end-1,2:end-1),uyV(2:end-1,2:end-1));
    subplot(2,2,3)
    surf(xV(2:end-1,2:end-1),yV(2:end-1,2:end-1),vxV(2:end-1,2:end-1));
    subplot(2,2,4)
    surf(xV(2:end-1,2:end-1),yV(2:end-1,2:end-1),vyV(2:end-1,2:end-1));
    % figure(3),
    % surf(xV(2:end-1,2:end-1),yV(2:end-1,2:end-1),uxV(2:end-1,2:end-1));
    % 
    % 
    % figure(4),
    % surf(xV(2:end-1,2:end-1),yV(2:end-1,2:end-1),uyV(2:end-1,2:end-1));

%    [val, dval] = critfunctionEval(xin(1),xin(2),xV,yV,uV,vV,uxV,uyV,vxV,vyV,latticeGrid);
    %disp(val);
    %disp(dval);

    %Calculate derivatives at any point.
    %calculateinterp( x , y , uxV , latticeGrid );

    %check for every quass point

    for i = 1:6
        xin = ((a(i,:)+1)/2)';
        [pt,ret,debugX,debugY,ptVal]= critmultinewton(xin,100,xV,yV,uV,vV,uxV,uyV,vxV,vyV,latticeGrid );
        yout(i,:) = pt*2-1;
        yeval(i) = ret;
        ptValSave(i) = norm(ptVal);
    %disp(pt*2-1);
    %disp(ret);
    end
    disp(yout);
    disp(yeval);
    error = (yout-a);
    disp(error);

    for i =1 :6
        if ( 0 == yeval(i))
           errornorm(i) = inf; 
        else
           errornorm(i) = norm(error(i,:));
        end
    end
    disp(errornorm' );
    saveErNm(loop,:) = errornorm; 
    saveValErNm(loop,:) = ptValSave;
end
input.data = [lattices;saveErNm'];

%%
latex = latexTable(input);
%disp(latex);

inputVal.data = [saveValErNm];
% given a seed point

latex = latexTable(inputVal);


