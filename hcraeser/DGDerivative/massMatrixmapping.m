%% Test mass matrix mapping.
clear;
close all;
addpath('../PolyLib');

a = 0; b = 8*pi;
Nv = 4;
Ne = Nv-1;
v_x = a+ linspace(0,1,Nv)*(b-a);
Deg = 4;
vizSamples= 100;

[r,w] = JacobiGLZW(Deg+1,0,0);

x = zeros(Deg+1,Ne);
h_e = zeros(Ne,1);
for e = 1:Ne
    x(:,e) = v_x(e) + (r+1)/2*(v_x(e+1)-v_x(e));
    h_e(e) = v_x(e+1)-v_x(e);
end
% calculate u_act using function 
u_act = sin(x);

%Build and use mass matrix for coeffecints.
M = zeros(Deg+1,Deg+1);
for i = 0:Deg
    for j =0:Deg
        M(i+1,j+1) = sum( JacobiPoly(i,r,0,0) ...
                            .*JacobiPoly(j,r,0,0) .* ...
                            w);
    end
end
rhs = zeros(Deg+1,1);
u_hat = zeros(Deg+1,Ne);
for e =1:Ne
    for i=0:Deg
        rhs(i+1) = sum(JacobiPoly(i,r,0,0).*w.*u_act(:,e));
    end
    u_hat(:,e) = M\rhs;
end

%
rviz = linspace(-1,1,vizSamples);
x_viz = zeros(vizSamples,Ne);
for e = 1:Ne
    x_viz(:,e) = v_x(e) + (rviz+1)/2*(v_x(e+1)-v_x(e));
end

Vld = zeros(vizSamples,Deg+1);
for d = 0:Deg
   Vld(:,d+1) = JacobiPoly(d,rviz,0,0); 
end

u_sim = Vld*u_hat;
figure(1),
plot(x_viz,u_sim); 
hold on;
plot(x_viz,sin(x_viz),'r-');
title('Function Projection');
% Elementwise Local Derivative.

Vld_Der = zeros(vizSamples,Deg+1);
for d = 0:Deg
   Vld_Der(:,d+1) = JacobiPolyDerivative(d,rviz,0,0); 
end
u_simDer = Vld_Der*u_hat;
for e=1:Ne
    u_simDer(:,e) = u_simDer(:,e)/h_e(e)*2;
end

figure(2),
plot(x_viz,u_simDer); 
hold on;
plot(x_viz,cos(x_viz),'r.');
title('Function Derivative');
