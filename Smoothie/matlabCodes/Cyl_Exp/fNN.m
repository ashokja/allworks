function [V,I] =  fNN(ptCloud, pt)

 for i = 1:max(size(ptCloud))
      dist(i) =  (sum((ptCloud(i,:) - pt).^2)).^(1/2);
 end

 [V,I] = min(dist);

end
