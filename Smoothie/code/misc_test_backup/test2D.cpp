#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "OneSidedSIAC.h"
#include "SmoothieSIAC1D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh1D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

vector<NekDouble> cross_Math( const vector<NekDouble> &r,
				const vector<NekDouble> &s)
{
	vector<NekDouble> ans(3);
	ans[0] = r[1]*s[2] - r[2]*s[1];
	ans[1] = r[2]*s[0] - r[0]*s[2];
	ans[2] = r[0]*s[1] - r[1]*s[0];	
	return ans;
}

vector<NekDouble> sub_Math(
		vector<NekDouble> &p2, vector<NekDouble> &p1)
{
	vector<NekDouble> r(3);	
	r[0] = p2[0] - p1[0];	r[1] = p2[1] - p1[1];	r[2] = p2[2] - p1[2];
	return r;
}
NekDouble dot_Math(vector<NekDouble> &p,vector<NekDouble>&q)
{
	return p[0]*q[0] + p[1]*q[1] + p[2]*q[2] ;
}

NekDouble norm2_Math(vector<NekDouble> p)
{
	return p[0]*p[0] + p[1]*p[1] + p[2]*p[2] ;
}
bool intersect(
		vector<NekDouble> &p1, vector<NekDouble> &p2,
		vector<NekDouble> &q1, vector<NekDouble> &q2,
		vector<NekDouble> &i1, vector<NekDouble> &i2 )
{
	// Assuming all points are 3D.
	// 1. compute r and s ; r= P2-P1; s = Q2-Q1;
	i1.clear();i2.clear();
	vector<NekDouble> r, s, rCs, pMq, pMq_Cr, qMp, q2Mp, qMp_Cs ,qMp_Cr;
	NekDouble t0,t1,t;
	r = sub_Math(p2,p1); s = sub_Math(q2,q1);
	rCs = cross_Math(r,s);
	pMq = sub_Math(p1,q1);
	pMq_Cr = cross_Math(pMq,r);
	if( (norm2_Math(rCs) < TOLERENCE) && (norm2_Math(pMq_Cr) < TOLERENCE) )
	{
		// line segements are linear and coinside.
		// find i1 and i2;
		t0 = -1.0*dot_Math(pMq,r)/dot_Math(r,r);
		q2Mp = sub_Math(q2,p1);
		t1 =  dot_Math(q2Mp,r)/dot_Math(r,r);
		if (t0 > t1)
		{
			NekDouble temp = t0;
			t0 = t1;
			t1 = temp;	
		}
		if (t0 <0.0 && t1 <0.0)
		{ // ignore
			//cout << "p7" << endl;
			return false;
		}else if (t0<=0.0 && t1<=1.0)
		{ // p1 and t1
			i1.push_back( p1[0]); 
			i1.push_back( p1[1]); 
			i1.push_back( p1[2]); 
				i2.push_back( p1[0] + t1*r[0] );
				i2.push_back( p1[1] + t1*r[1] );
				i2.push_back( p1[2] + t1*r[2] );
			//cout << "p8" << endl;
			return true;
		}else if (t0<=0.0 && t1>=1.0)
		{
			i1.push_back( p1[0]); 
			i1.push_back( p1[1]); 
			i1.push_back( p1[2]); 
				i2.push_back( p2[0] );
				i2.push_back( p2[1] );
			//cout << "p11" << endl;
			return true;	
		}else if ( t0<=1.0 && t1 >=1.0)
		{ // t0 and P2
			i1.push_back( p1[0] + t0*r[0]);
			i1.push_back( p1[1] + t0*r[1]);
			i1.push_back( p1[2] + t0*r[2]);
				i2.push_back( p2[0] );
				i2.push_back( p2[1] );
				i2.push_back( p2[2] );
			//cout << "p9" << endl;
			return true;
		}else // one case left t0>1 and t1>1
		{ //ignore
			//cout << "p10" << endl;
			return false;
		}
	}
	if( (norm2_Math(rCs) < TOLERENCE) && (norm2_Math(pMq_Cr) > TOLERENCE) )
	{
		//cout << "p3" << endl;
		return false;
	}
	if( norm2_Math(rCs) > TOLERENCE)
	{
		qMp = sub_Math(q1,p1);
		qMp_Cs = cross_Math( qMp,s );
		qMp_Cr = cross_Math( qMp,r );
		rCs= cross_Math(r,s);
		NekDouble t = std::sqrt( norm2_Math(qMp_Cs)/norm2_Math(rCs) );
		NekDouble terr =  (std::abs(qMp_Cs[0] -t*rCs[0]) + 
				std::abs(qMp_Cs[1]-t*rCs[1]) + std::abs(qMp_Cs[2] - t*rCs[2]) ); 
		NekDouble u = std::sqrt( norm2_Math(qMp_Cr)/norm2_Math(rCs) );
		NekDouble uerr =  (std::abs(qMp_Cr[0] -u*rCs[0]) + 
				std::abs(qMp_Cr[1]-u*rCs[1]) + std::abs(qMp_Cr[2] - u*rCs[2]) );
		if ( (t>=0 && t<=1) &&(u>=0 && u<=1) &&
					(terr<TOLERENCE) && (uerr<TOLERENCE) )
		{
			i1.push_back( p1[0] + t*r[0]);
			i1.push_back( p1[1] + t*r[1]);
			i1.push_back( p1[2] + t*r[2]);
		//	cout << t << "\t" << u << "\t"<< r[0] << "\t" << r[1] << "\t" << r[2] << endl;
		//	cout << "p4" << endl;
			return true;
		}else{
		//	cout << "p5" << endl;
			return false;
		}
	}	
	cout << "p6" << endl;
	return true;
}

void IntersectWithEdges ( const SpatialDomains::SegGeomMap &segMap, const SpatialDomains::PointGeomMap &pointMap,
		const Array<OneD,NekDouble> &dir, const Array<OneD,NekDouble> &point, const NekDouble t1, const NekDouble t2, 
		vector<NekDouble> &tvalT)
{
	tvalT.clear();
	vector<NekDouble> p1(3), p2(3),i1(3),i2(3);
	int dirID = -1;
	p1[0] = point[0]+ t1*dir[0];	p1[1] = point[1]+ t1*dir[1];	p1[2] = point[2]+ t1*dir[2];
	p2[0] = point[0]+ t2*dir[0];	p2[1] = point[1]+ t2*dir[1];	p2[2] = point[2]+ t2*dir[2];
	// iterate through all the edges.
	// pick a direction not zero. 
	for (int i =0; i<3; i++)
	{
		if (std::abs(dir[i]) >TOLERENCE)
		{
			dirID = i;
			break;
		} 
	}
	assert( dirID >=0 && "Direction is not right something is up ");
	for (int s =0;s< segMap.size(); s++)
	{
		SpatialDomains::SegGeomSharedPtr segPtr =  segMap.find(s)->second; //->second;
		vector<NekDouble> q1(3), q2(3);
		pointMap.find( segPtr->GetVid(0) )->second->GetCoords(q1[0],q1[1],q1[2]);
		pointMap.find( segPtr->GetVid(1) )->second->GetCoords(q2[0],q2[1],q2[2]);
		bool b = intersect(p1,p2,q1,q2,i1,i2);
		if (b)
		{
			//nbc.printNekArray(i1,0);
			NekDouble t =0 ;
			t = (i1[dirID]-point[dirID])/dir[dirID];
			tvalT.push_back(t);	
			if(i2.size() > 0)
			{
			//	nbc.printNekArray(i2,0);
				NekDouble t =0 ;
				t = (i2[dirID]-point[dirID])/dir[dirID];
				tvalT.push_back(t);	
			}
		}
	}
} 


void FindElementIDForLineSegs(const vector<NekDouble> &tvalT,
							const Array<OneD,NekDouble> &point,
							const Array<OneD,NekDouble> &dir,
							const SpatialDomains::MeshGraphSharedPtr mesh_graph,
							vector<int> &EIDs )
{
	Array<OneD,NekDouble> temp(3);
	// for every point iteration
	for ( int i=0 ; i < tvalT.size()-1; i++ )
	{
		temp[0] = point[0] + 0.5*(tvalT[i]+ tvalT[i+1])*dir[0];	
		temp[1] = point[1] + 0.5*(tvalT[i]+ tvalT[i+1])*dir[1];	
		temp[2] = point[2] + 0.5*(tvalT[i]+ tvalT[i+1])*dir[2];		
	}
	
	
}

 
int main(int argc, char* argv[])
{
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	vector<string> var = vSession->GetVariables();
	
	for (int i=0; i < var.size(); i++)
	{
		cout << var[i] << endl;
	}
	// get mesh graph.
	SpatialDomains::MeshGraphSharedPtr mesh_graph = SpatialDomains::MeshGraph::Read(vSession);
	int meshDim = mesh_graph->GetMeshDimension();
	int meshSpa = mesh_graph->GetSpaceDimension();

	cout << "meshDim: " << meshDim << "\t meshSpa: " << meshSpa << endl;
		
	// composite map.	
	SpatialDomains::CompositeMap comMap = mesh_graph->GetComposites();
	SpatialDomains::PointGeomMap pointMap = mesh_graph->GetAllPointGeoms();
	SpatialDomains::SegGeomMap segMap = mesh_graph->GetAllSegGeoms();
	SpatialDomains::TriGeomMap triMap = mesh_graph->GetAllTriGeoms();
	SpatialDomains::QuadGeomMap quadMap = mesh_graph->GetAllQuadGeoms();
	SpatialDomains::QuadGeomMapIter it =  quadMap.begin();
	while (it != quadMap.end())
	{
		cout << it->first << "  :: " << it->second << endl;
		SpatialDomains::QuadGeomSharedPtr qspr = it->second;
		cout << "NumEdges: " << qspr->GetNumEdges() << endl;
		for (int i=0; i < qspr->GetNumEdges(); i++)
		{
			int eid = qspr->GetEid(i);
			SpatialDomains::SegGeomSharedPtr segPtr = segMap.find(eid)->second;
			int vid1 = segPtr->GetVid(0);
			int vid2 = segPtr->GetVid(1);
			cout << "\edge " << eid << "\t v1: " <<vid1 << "\t v2: " << vid2 << endl;
		}
		break;
		++it; 
	}
	
	NektarBaseClass nbc;
	Array<OneD,NekDouble> point(3,0.0),dir(3,0.0);
	NekDouble t1, t2;
	t1 = -0.25; t2 = 0.8; 
	point[0] = 0.0;point[1]=0.0; point[2]=0.0;
	dir[0] = 0.0;dir[1]=1.0; dir[2]=0.0;
	vector<NekDouble> tvalT;
	IntersectWithEdges( segMap, pointMap, dir, point, t1,t2,tvalT);
	nbc.printNekArray(tvalT,0);
	//FindElementIDForLineSegs(tvalT, tElIDT);

	
	/*
	SpatialDomains::SegGeomMapIter sit = segMap.begin();
	while (sit != segMap.end())
	{
		SpatialDomains::SegGeomSharedPtr segPtr =  sit->second;
		vector<NekDouble> q1(3), q2(3);
		pointMap.find( segPtr->GetVid(0) )->second->GetCoords(q1[0],q1[1],q1[2]);
		pointMap.find( segPtr->GetVid(1) )->second->GetCoords(q2[0],q2[1],q2[2]);
		nbc.printNekArray(q1,0);
		nbc.printNekArray(q2,0);
	}	
	*/
/*
	vector<NekDouble> p1(3),p2(3),q1(3),q2(3),i1(3),i2(3);
	p1[0] = atof(argv[1]);
	p1[1] = atof(argv[2]);
	p1[2] = atof(argv[3]);
	
	p2[0] = atof(argv[4]);
	p2[1] = atof(argv[5]);
	p2[2] = atof(argv[6]);

	q1[0] = atof(argv[7]);
	q1[1] = atof(argv[8]);
	q1[2] = atof(argv[9]);
	
	q2[0] = atof(argv[10]);
	q2[1] = atof(argv[11]);
	q2[2] = atof(argv[12]);

	NektarBaseClass k;
	k.printNekArray(p1,0);
	k.printNekArray(p2,0);
	k.printNekArray(q1,0);
	k.printNekArray(q2,0);
	
	bool b = intersect(p1,p2,q1,q2,i1,i2);
	
	cout << b << endl;
	k.printNekArray(i1,0);
	k.printNekArray(i2,0);
	

	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM1D = new HandleNekMesh1D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM1D->LoadData(vSession->GetSessionName()+".fld", var);
	Array<OneD,NekDouble> direction(3,0.0);

	int tNquadPts = HNM1D->m_expansions[0]->GetTotPoints();
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM1D->m_expansions[0]->GetCoordim(0) )
	{
		case 1:
			cout << "opps did not plan for this" << endl;
			break;
		case 2:
			HNM1D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, fceD, eceD, sceD;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	fceD = Array<OneD,NekDouble>(tNquadPts);
	sceD = Array<OneD,NekDouble>(tNquadPts);
	eceD = Array<OneD,NekDouble>(tNquadPts);
	LibUtilities::EquationSharedPtr ffunc
					= vSession->GetFunction("ExactSolution", 0); 
	ffunc->Evaluate(xc0,xc1,xc2, fce);

	LibUtilities::EquationSharedPtr ffuncD
					= vSession->GetFunction("ExactDerSolution", 0); 
	ffuncD->Evaluate(xc0,xc1,xc2, fceD);
	
	HNM1D->m_expansions[0]->FwdTrans(fce,HNM1D->m_expansions[0]->UpdateCoeffs());
	HNM1D->m_expansions[0]->BwdTrans( HNM1D->m_expansions[0]->GetCoeffs(),
										HNM1D->m_expansions[0]->UpdatePhys());
	ece = HNM1D->m_expansions[0]->GetPhys();
	HNM1D->m_expansions[0]->PhysDeriv( ece, eceD);	
*/

/*
	//Sym filter Test.
		cout << "second case" << endl;
	SymmetricSIAC symSIAC(2, SymmetricSIAC::SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC , 1);
		cout << "second case" << endl;
	cout << symSIAC.m_nBSpl << endl;	
	// -3:.1:3
	int parts = 61;
	Array<OneD,NekDouble> x_pos(parts), v_pos(parts);
	for (int i =0; i < parts; i++)
	{
		x_pos[i] =  -3.0+( (NekDouble)i/(NekDouble)(parts-1.0))*6.0;
	}

	symSIAC.EvaluateFilter(x_pos,v_pos,1.0,0.0,true );

	NektarBaseClass k;
	k.printNekArray( x_pos,0);	
	k.printNekArray( v_pos,0);	
	//k.printGraphArray(v_pos,-2,2,.01);	
	k.writeNekArray(x_pos,"test_xpos.txt");	
	k.writeNekArray(v_pos,"test_vpos.txt");	
*/
//	SymmetricSIAC symSIAC2(2, SymmetricSIAC::SymFilterType::CUSTOM_Derivative_SIAC , 1);

	//One SidedFilterTest.
//	OneSidedSIAC oneSIAC(2, OneSidedSIAC::OneSidedFilterType::Der_SMOOTH_BASIC_SIAC_2kp1 , 1);
//		cout << "second case" << endl;
//	OneSidedSIAC oneSIAC2(2, OneSidedSIAC::OneSidedFilterType::Der_BASIC_SIAC_2kp1 , 1);

//	NektarBaseClass k;
//	k.printNekArray( symSIAC.m_coeffs,0);	
/*	
	SmoothieSIAC1D sm(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM1D, 2, 1.0,1); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();

	for (int i =0; i <tNquadPts;i++)
	{
		sm.EvaluateAt(xc0[i],xc1[i],xc2[i],sce[i],valY,valZ);

		//cout << "\tf:\t" << abs(fceD[i]-eceD[i]) ;
		//cout << "\ts:\t:" << abs(fceD[i] -sce[i]);
		cout << "\tf:\t" << fceD[i] ;
		cout << "\te:\t:" << eceD[i];
		cout << "\ts:\t:" << sce[i];
		cout << endl;
	}

	NektarBaseClass k;
	k.printNekArray(xc0,0);
	k.printNekArray(fce,0);
	k.printNekArray(ece,0);
	k.printNekArray(eceD,0);
	k.printNekArray(sce,0);
	k.writeNekArray(xc0,"xc0_Der2kp1.txt");
	k.writeNekArray(fce,"fce_Der2kp1.txt");
	k.writeNekArray(fceD,"fceD_Der2kp1.txt");
	k.writeNekArray(ece,"ece_Der2kp1.txt");
	k.writeNekArray(eceD,"eceD_Der2kp1.txt");
	k.writeNekArray(sce,"sce_Der2kp1.txt");
	return 0;
*/	
}

void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
