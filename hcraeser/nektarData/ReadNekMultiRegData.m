% open read new data output by nektar.

clear;
close all;
fileid = fopen('loadData/ForMR_Nek_MultiRegions.txt');

%number of variables.
numVarC = textscan(fileid,'%f',1);
numVar = cell2mat(numVarC);

for nv = 1:numVar
    VarnameC = textscan(fileid,'%s',1);
    
    formatSpec = '%f,%f,%f';    
    C = textscan(fileid,formatSpec,1);
    sizes = cell2mat(C);
    N = prod(sizes);
    Ar_C = textscan(fileid,'%f',N,'Delimiter',',');
    Ar_M = cell2mat(Ar_C);
    Ar = reshape(Ar_M,sizes);
    %Ar = permute(Ar,[2,1,3]);
    v = char(VarnameC{1});
    varname = genvarname(v);
    eval([varname '=Ar;']);
end

figure(1)
VizNekSurfaceScalar(msh_x0*2-1,msh_y0*2-1,msh_z0*2-1,el_0); hold on;
VizNekSurfaceScalar(msh_x1*2-1,msh_y1*2-1,msh_z1*2-1,el_1); hold on;
VizNekSurfaceScalar(msh_x2*2-1,msh_y2*2-1,msh_z2*2-1,el_2); hold on;
VizNekSurfaceScalar(msh_x3*2-1,msh_y3*2-1,msh_z3*2-1,el_3); hold on;
VizNekSurfaceScalar(msh_x4*2-1,msh_y4*2-1,msh_z4*2-1,el_4); hold on;
VizNekSurfaceScalar(msh_x5*2-1,msh_y5*2-1,msh_z5*2-1,el_5); hold on;
VizNekSurfaceScalar(msh_x6*2-1,msh_y6*2-1,msh_z6*2-1,el_6); hold on;
VizNekSurfaceScalar(msh_x7*2-1,msh_y7*2-1,msh_z7*2-1,el_7); hold on;
hold off;

figure(2);
VizNekIsoSurfaceScalar(msh_x0*2-1,msh_y0*2-1,msh_z0*2-1,el_0,0,'green'); hold on;
VizNekIsoSurfaceScalar(msh_x1*2-1,msh_y1*2-1,msh_z1*2-1,el_1,0,'green'); hold on;
VizNekIsoSurfaceScalar(msh_x2*2-1,msh_y2*2-1,msh_z2*2-1,el_2,0,'green'); hold on;
VizNekIsoSurfaceScalar(msh_x3*2-1,msh_y3*2-1,msh_z3*2-1,el_3,0,'green'); hold on;
VizNekIsoSurfaceScalar(msh_x4*2-1,msh_y4*2-1,msh_z4*2-1,el_4,0,'green'); hold on;
VizNekIsoSurfaceScalar(msh_x5*2-1,msh_y5*2-1,msh_z5*2-1,el_5,0,'green'); hold on;
VizNekIsoSurfaceScalar(msh_x6*2-1,msh_y6*2-1,msh_z6*2-1,el_6,0,'green'); hold on;
VizNekIsoSurfaceScalar(msh_x7*2-1,msh_y7*2-1,msh_z7*2-1,el_7,0,'green'); hold on;
hold off;


figure(3)
VizNekSurfaceScalar(msh_x0*2-1,msh_y0*2-1,msh_z0*2-1,DBV_0); hold on;
VizNekSurfaceScalar(msh_x1*2-1,msh_y1*2-1,msh_z1*2-1,DBV_1); hold on;
VizNekSurfaceScalar(msh_x2*2-1,msh_y2*2-1,msh_z2*2-1,DBV_2); hold on;
VizNekSurfaceScalar(msh_x3*2-1,msh_y3*2-1,msh_z3*2-1,DBV_3); hold on;
VizNekSurfaceScalar(msh_x4*2-1,msh_y4*2-1,msh_z4*2-1,DBV_4); hold on;
VizNekSurfaceScalar(msh_x5*2-1,msh_y5*2-1,msh_z5*2-1,DBV_5); hold on;
VizNekSurfaceScalar(msh_x6*2-1,msh_y6*2-1,msh_z6*2-1,DBV_6); hold on;
VizNekSurfaceScalar(msh_x7*2-1,msh_y7*2-1,msh_z7*2-1,DBV_7); hold on;
hold off;
