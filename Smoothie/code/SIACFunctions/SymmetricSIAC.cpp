#include "SymmetricSIAC.h"
#include <Eigen/Dense>

SymmetricSIAC::SymmetricSIAC(int order) :
							m_coeffCalculated(false),
							m_cenBSpline(order), m_splines(order-1)
{
	m_nthDer=0;
	// initialize number of coeffecients array using order.
	// If only order is specified. It is by default BASIC_SIAC_4kp1.
	m_filterType = SymFilterType::BASIC_SIAC_2kp1;

	switch (m_filterType)
	{
		case (SymFilterType::BASIC_SIAC_2kp1):
			m_order = order;
			m_nBSpl = 2*(m_order-1)+1;
			m_coeffs = Array<OneD,NekDouble>((2*(order-1)+1),0.0);
			break;
		default:
			assert(false && "something is wrong" );
			cout << "Should not come here assert here actually." << endl;
			break;	
	}
/* 
	if (0 == m_nthDer){
		//m_coeffs(2*(order-1)+1,0.0);
		m_R = 2*(order-1) ; // intializeR
	}else{
		//m_coeffs(2*(order+nthDer-1)+1,0.0);	
		m_R = 2*(order+m_nthDer-1) ; // need to be re-checked ???
	}
*/
	EvaluateCoefficients();
}

SymmetricSIAC::SymmetricSIAC(int Order, int nBSpl ,int nthDerivative):
								m_cenBSpline(Order), m_splines(Order-1)
{
	m_nthDer = nthDerivative;
	if (m_nthDer ==0)
	{
		m_filterType = SymFilterType::CUSTOM_SIAC;
	}
	switch (m_filterType)
	{
		case(SymFilterType::CUSTOM_SIAC):
			m_order = Order;
			m_nBSpl = nBSpl;
			m_coeffs = Array<OneD,NekDouble>( nBSpl,0.0 );
			break;
		default:
			cout <<"Assert, Somthing is wrong. F: SymmetricSIAC" << endl;
	}
	EvaluateCoefficients();
}

SymmetricSIAC::SymmetricSIAC(int Order, SymFilterType filterType, int nthDer):
								m_filterType(filterType),
								m_cenBSpline(Order), m_splines(Order-1)
{
	m_nthDer = nthDer;
	switch (m_filterType)
	{
		case(SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC):
			m_order = Order;
			m_nBSpl = 2*(m_order-1)+1+ nthDer;
			m_coeffs = Array<OneD,NekDouble>( m_nBSpl,0.0 );
			//cout << "coming into this case" << endl;
			break;
		case(SymFilterType::CUSTOM_Derivative_SIAC):
			m_order = Order;
			m_nBSpl = 2*(m_order-1)+1;
			m_coeffs = Array<OneD,NekDouble>( m_nBSpl,0.0 );
			//cout << "came into 3rd case" << endl;
			break;
		case(SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC_WOUT_DIVDIFF):
			m_order = Order+nthDer;
			m_nthDer = 0;
			m_nBSpl = 2*(Order-1)+1;
			m_coeffs = Array<OneD,NekDouble>( m_nBSpl,0.0 );
			m_cenBSpline = CentralBSplines(m_order);
			//cout << "coming into this case" << endl;
			break;
		default:
			cout << "@#$Assert, Something is wrong. F: SymmetricSIAC" << endl;
	}
	EvaluateCoefficients();
}


bool SymmetricSIAC::v_EvaluateCoefficients(const NekDouble kernelShift)
{
//	cout << "Into Evalute coefficietns" << endl; //???

	switch (m_filterType)
	{
		case(SymFilterType::BASIC_SIAC_2kp1):
			CalCoeffForWideSymKernel( m_order-1, m_nBSpl, m_coeffs);
			m_splines.Initialize( m_order-1, m_nBSpl,m_coeffs);
			break;
		case(SymFilterType::CUSTOM_SIAC):
			CalCoeffForWideSymKernel( m_order-1, m_nBSpl, m_coeffs);
			m_splines.Initialize( m_order-1, m_nBSpl,m_coeffs);
			break;
		case(SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC):
		//case(SymFilterType::CUSTOM_Derivative_SIAC):
			CalCoeffForWideSymKernel( m_order-1+m_nthDer, m_nthDer, m_nBSpl, m_coeffs);
			//	cout << "n_BSpl "<< m_nBSpl << endl;
			//	printNekArray(m_coeffs,0);
			CalCoeffForCenBSplDerivatives( m_order-1+m_nthDer,m_nthDer,m_nBSpl, m_coeffs);
			//	cout << "n_BSpl "<< m_nBSpl << endl;
			//	printNekArray(m_coeffs,0);
			m_splines.Initialize( m_order-1, m_nBSpl,m_coeffs);
			break;
		case(SymFilterType::CUSTOM_Derivative_SIAC):
		//case(SymFilterType::CUSTOM_Derivative_SIAC):
			CalCoeffForWideSymKernel( m_order-1+m_nthDer, m_nthDer, m_nBSpl, m_coeffs);
			//	cout << "n_BSpl "<< m_nBSpl << endl;
			//	printNekArray(m_coeffs,0);
			CalCoeffForCenBSplDerivatives( m_order-1+m_nthDer,m_nthDer,m_nBSpl, m_coeffs);
			//	cout << "n_BSpl "<< m_nBSpl << endl;
			//	printNekArray(m_coeffs,0);
			m_splines.Initialize( m_order-1, m_nBSpl,m_coeffs);
			break;
		case(SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC_WOUT_DIVDIFF):
			CalCoeffForWideSymKernel( m_order-1, m_nBSpl, m_coeffs);
			m_splines.Initialize( m_order-1, m_nBSpl,m_coeffs);
			cout << "No of coeffs is"<<m_coeffs.num_elements() << endl;
				printNekArray(m_coeffs,0);
			break;
		default:
			cout << "Assert or add code for all the other cases Sig:ajiso876af" << endl;
	}


/*	if (0 == m_nthDer){
		switch(m_order){
			case 0:
				cout << "Assert needed here ???" << endl;
				break;
			case 1:
				cout << "Normally this order is not used. Are you sure about what you are doing."<< endl;
				break;
			case 2: // degree=1;
//				cout << "case 2" << endl; //???
				m_coeffs[0] = -1.0/12.0; m_coeffs[1] =14.0/12.0; m_coeffs[2]=-1.0/12.0;
				break;
			case 3:
				m_coeffs[0] = 37.0/1920.0; m_coeffs[1] =-388.0/1920.0; m_coeffs[2]=2622.0/1920.0;
				m_coeffs[4] = 37.0/1920.0; m_coeffs[3] =-388.0/1920.0; 
				break;
			default:
				cout << "Out of order, re-implement or hardcode values. ??? ";
		}
	}else
	{
		cout << "This stuff in not coded yet." << endl;
	}
*/
	return true;
}

bool SymmetricSIAC::EvaluateFilterUsingSplines( const Array<OneD,NekDouble> &x_pos, Array<OneD,NekDouble> &t_vals,
const NekDouble meshScaling, const NekDouble meshShift, const bool evalCoeff) 
//bool SymmetricSIAC::v_EvaluateFilter( const Array<OneD,NekDouble> &x_pos, Array<OneD,NekDouble> &t_vals,
//				 const NekDouble meshScaling, const NekDouble meshShift, const bool evalCoeff) 
{
	// Always check if coeffecients have already been calculated.
		//For symmetric case. They are always calculated during initilization.
	int nq;
	nq = x_pos.num_elements();
	Vmath::Fill(nq,0.0,t_vals,1);
	Array<OneD,NekDouble> t_valTemp(nq);
	for(int i = 0; i< m_nBSpl; i++)
	{
		//	NekDouble it = -m_R*0.5 + i;
		NekDouble it = -(m_nBSpl-1.0)/2.0+i;
		m_cenBSpline.EvaluateBSplines( x_pos,t_valTemp, ((NekDouble) it)*meshScaling , meshScaling );
		Vmath::Smul(nq, m_coeffs[m_nBSpl-1-i]/std::pow(meshScaling,1+m_nthDer) , t_valTemp,1,t_valTemp,1);
		Vmath::Vadd(nq, t_valTemp,1 ,t_vals,1,t_vals,1);
	}
	return true;
}

bool SymmetricSIAC::v_EvaluateFilter( const Array<OneD,NekDouble> &x_pos, Array<OneD,NekDouble> &t_vals,
	const NekDouble meshScaling, const NekDouble meshShift, const bool evalCoeff) 
//bool SymmetricSIAC::EvaluateFilterUsingSplines( const Array<OneD,NekDouble> &x_pos, Array<OneD,NekDouble> &t_vals,
//				const NekDouble meshScaling, const NekDouble meshShift, const bool evalCoeff) 
{
	vector<NekDouble> m_tempVector(10); // Need to fix this.
	vector<NekDouble> v_x_pos(x_pos.num_elements());
	memcpy( &v_x_pos[0], &x_pos[0], x_pos.num_elements()*sizeof(NekDouble));
	vector<NekDouble> v_t_vals(t_vals.num_elements());
// call the funtion here.
	m_splines.EvaluateUArr( v_x_pos, v_t_vals, m_tempVector, meshScaling,0.0, m_nthDer);
	memcpy( &t_vals[0], &v_t_vals[0], t_vals.num_elements()*sizeof(NekDouble));
	return true;
}






/// This function return all break point of filter scaled appropriately.
// This function should not be calculating break points everytime.
// They should have been saved at initilization (That poses other problems :( )
bool SymmetricSIAC::v_GetBreakPts( const NekDouble scaling, vector<NekDouble> &valT,
							const NekDouble shift)
{
	NekDouble tmin, tmax;
	switch (m_filterType)
	{
		case(SymFilterType::BASIC_SIAC_2kp1):
		case(SymFilterType::CUSTOM_SIAC):
		case(SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC):
		case(SymFilterType::CUSTOM_Derivative_SIAC):
		case(SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC_WOUT_DIVDIFF):
			tmin = -( (m_order)/2.0+(m_nBSpl-1.0)/2.0)*scaling;
			tmax = ( (m_order)/2.0+(m_nBSpl-1.0)/2.0)*scaling;
			break;
		default:
			cout << "This case not coded yet. GetBreakPts" << endl;
	}

	valT.clear();
	for (NekDouble t = tmin; t <=tmax ; t += scaling)
	{
		valT.push_back(t);
	} 
	return true;
}


bool SymmetricSIAC::v_GetFilterRange( NekDouble scaling, NekDouble &tmin, NekDouble &tmax,
					const NekDouble shift )
{
	switch (m_filterType)
	{
		case(SymFilterType::BASIC_SIAC_2kp1):
		case(SymFilterType::CUSTOM_SIAC):
		case(SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC):
		case(SymFilterType::CUSTOM_Derivative_SIAC):
		case(SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC_WOUT_DIVDIFF):
			tmin = -( (m_order)/2.0+(m_nBSpl-1.0)/2.0)*scaling;
			tmax = ( (m_order)/2.0+(m_nBSpl-1.0)/2.0)*scaling;
			break;
		default:
			cout << "This case not coded yet. GetFilterRange" << endl;
	}
	return true;
}

