% test3.m
clear;

x = (-1:.1:1);
y = (-1:.1:1);
[~,sx] = size(x);
[~,sy] = size(y);

%M = [3/5,4/5; 2/sqrt(13),3/sqrt(13) ];
M = [1,0;0,1];


lambda =-1;
mu =-1;

T = (-10:.2:1);
Alpha = (0:.2:1);
[~,sA] = size(Alpha);
Beta = (0:.2:1);
[~,sB] = size(Beta);
for alpha = 1:sA
    for beta = 1:sB
        dataOut = (M*[Alpha(alpha)*exp(lambda*T);Beta(beta)*exp(mu*T)])';
        plot(dataOut(:,1),dataOut(:,2),'*'); hold on
    end
end