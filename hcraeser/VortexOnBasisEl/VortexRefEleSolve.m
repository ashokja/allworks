% function [trace] = VortexRefEleSolve(n1,n2,n3,dataIn,dataOut,f)
%     
%     switch nargin
%         case 0
%             %initialize variables
%             n1 = 4; n2 = 4; n3 = 4;
%             f=1;
%             
%             %initialize Data
%             x = (-1:.2:1)';
%             [X,Y,Z] = meshgrid(x,x,x);
%             dataIn(:,1) = reshape(X,[],1);
%             dataIn(:,2) = reshape(Y,[],1);
%             dataIn(:,3) = reshape(Z,[],1);
%             [sx,~] = size(dataIn);
%             dataOut = zeros(size(dataIn));
%             for i = 1:sx
%                 coords = squeeze(dataIn(i,:));
%                 vel = cross(coords,[0,1,0])+ [0,1,0];
%                 dataOut(i,:) = vel;
%             end
%     end

    clear;
for kinder = 4:6
    n1 = 4; n2 = 4; n3 = 4;
    addpath ../createTestData/ ;
    [xy,distance,t_a,mapxy,mapxyV] = test_run(kinder); 
        hold off;
        saveas( gcf, strcat('testPlot',int2str(kinder*10+7)), 'jpg' );
    f=1; dataIn = mapxy; dataOut = mapxyV;
    
    coeff = buildCoeff(n1,n2,n3,dataIn,dataOut);
    if (f==1)
        figure(1)
        vizualizeCoeff(n1,n2,n3,coeff); 
    end
    [uCoeffRed,uCoeffRot] = divideVel(n1,n2,n3,coeff,dataIn);
    title('Original Velocity');
    xlabel('x'); ylabel('y'); zlabel('z'); hold off;
    saveas( gcf, strcat('testPlot',int2str(kinder*10+1)), 'jpg' );
    if f ==1
        figure(2);
        vizualizeCoeff(n1,n2,n3,uCoeffRed);
        title('Reduced Velocity');
        xlabel('x'); ylabel('y'); zlabel('z'); hold off;
            saveas( gcf, strcat('testPlot',int2str(kinder*10+2)), 'jpg' );
        figure(3);
        vizualizeCoeff(n1,n2,n3,uCoeffRot);
        title('Rotational Velocity');
        xlabel('x'); ylabel('y'); zlabel('z'); hold off;
            saveas( gcf, strcat('testPlot',int2str(kinder*10+3)), 'jpg' );
    end
    %find zeros on each face.
    %%
    figure(4),
    [sol_seeds,goodSeeds,solAtSeeds] = zerosAtProjected(n1,n2,n3,uCoeffRot);
    hold off;
        saveas( gcf, strcat('testPlot',int2str(kinder*10+4)), 'jpg' );
    %%
    %For all good seeds trace the seeds into the cube.
    [seedNum,dim] = size(goodSeeds);
    
    % using actual velocity.
    count =1; tps = {}; tpsVel = {};
    for i = 1:seedNum
        for j = 1:dim
            if (goodSeeds(i,j) ==1)
                [trace,traceVel] = traceThePoint(n1,n2,n3,coeff, sol_seeds(i,j,:),10^(-4) );
                tps(count) = {trace};
                tpsVel(count) = {traceVel};
                count = count +1;
            end
        end
    end
    
    
    %%
    if f==1
        [~,b] = size(tps);
        figure(5);
        plotBoundingBox(); hold on;
        for i =1:b
            trace = cell2mat(tps(i));
            plot3(trace(:,1),trace(:,2),trace(:,3),'g*');
            hold on;
        end
 %       hold off;
    end
    %%
    % using only reduced velocity.
    count =1; tpsr = {}; tpsVel= {};
    for i = 1:seedNum
        for j = 1:dim
            if (goodSeeds(i,j) ==1)
                [trace,traceVel] = traceThePointUsingRedVel(n1,n2,n3,uCoeffRed, sol_seeds(i,j,:) );
                tpsr(count) = {trace};
                tpsVelr(count) = {traceVel};
                count = count +1;
            end
        end
    end
    
    if(sum(sum(goodSeeds)) <1)
        continue;
    end
        %%
    if f==1
        [~,b] = size(tps);
       % figure(4);
       % plotBoundingBox();
        for i =1:b
            trace = cell2mat(tpsr(i));
            plot3(trace(:,1),trace(:,2),trace(:,3),'r*');
            hold on;
        end
%        hold off;
    end

    %%
    [~,b] = size(tps);
    for i = 1:b
        traceRed = cell2mat(tpsr(i));
        trace = cell2mat(tps(i));
        [sTraceRed,~] = size(traceRed);
        [sTrace,~] = size(trace);
        [c1,c2,c3] = parmetricCurves(traceRed,(0:1/(sTraceRed-1):1),10);
        curveCoeffRed(i,:,:) = [c1,c2,c3];
        [c1,c2,c3] = parmetricCurves(trace,(0:1/(sTrace-1):1),10);
        curveCoeff(i,:,:) = [c1,c2,c3];
    end
    %%
    if f==1
        [~,b] = size(tps);
        [~,sC,~] = size(curveCoeff);
       % figure(4);
       % plotBoundingBox();
        for i =1:b
            pts = DrawParametricCurve(curveCoeffRed(i,:,1),curveCoeffRed(i,:,2),curveCoeffRed(i,:,3),sC-1);
            plot3(pts(:,1),pts(:,2),pts(:,3),'-r'); hold on;
            pts = DrawParametricCurve(curveCoeff(i,:,1),curveCoeff(i,:,2),curveCoeff(i,:,3),sC-1);
            plot3(pts(:,1),pts(:,2),pts(:,3),'-g');
            hold on;
        end
       % hold off;
    end
    %%
    count =1;
   for i = 1:seedNum
        for j = 1:dim
            if (goodSeeds(i,j) ==1)
                gsvalue(count,:) = sol_seeds(i,j,:);
                count = count +1;
            end
        end
    end
    
    %%
    figure(5);
    plotBoundingBox();
    dataOutLast = buildData(n1,n2,n3,uCoeffRot,mapxy);
    dataOutRed = buildData(n1,n2,n3,uCoeffRed,mapxy);
	drawStreamLine(mapxy,dataOutLast,1,gsvalue(:,1),gsvalue(:,2),gsvalue(:,3));
	drawOnlyStreamLine(mapxy,dataOutRed,.5,gsvalue(:,1),gsvalue(:,2),gsvalue(:,3));
	drawOnlyStreamLine(mapxy,-1*dataOutRed,.5,gsvalue(:,1),gsvalue(:,2),gsvalue(:,3));
    %drawOnlyStreamLine(mapxy,dataOutLast,.5,gsvalue(:,1),gsvalue(:,2),gsvalue(:,3));
    title('Rotational Velocity');
    xlabel('x'); ylabel('y'); zlabel('z');
    %view(3)
    hold off;
    saveas( gcf, strcat('testPlot',int2str(kinder*10+5)), 'jpg' );
%%
close all;
end
    
    % Now use relxation method to draw on Bernstein polynomials as starting
    % points.
    %%
    
%     t = (0:.2:1);
%     i =1;
%     [~,sx] = size(t);
%     for i =2:2
%         pts = DrawParametricCurve(curveCoeffRed(i,:,1),curveCoeffRed(i,:,2),curveCoeffRed(i,:,3),sC-1,t);
%         for j = 1:sx
%             pointIn = pts(j,:);
%             pOut(j,:) = closestZeroCoeff(n1,n2,n3,coeff,uCoeffRed,pointIn,0.125);
%             if( sum(abs(pOut(j,:))>1) >1) 
%                 pOut(j,:) = [0,0,0];
%             end
%         end
%     end
%     %%
%     %plot3(pts(:,1),pts(:,2),pts(:,3),'c*'); hold on;
%     plot3(pOut(:,1),pOut(:,2),pOut(:,3),'c*'); hold on;
%     %%
%     
%     i =1;
%     pts = DrawParametricCurve(curveCoeffRed(i,:,1),curveCoeffRed(i,:,2),curveCoeffRed(i,:,3),sC-1);
%     ok = buildData(n1,n2,n3,uCoeffRot,pts)
%     
    
    
    
    
%end