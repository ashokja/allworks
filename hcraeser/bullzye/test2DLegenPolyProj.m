% Learn to interpolate Legendre Poly in 2D.
function [u] = test2DLegenPolyProj(NX,NY)
    switch nargin
        case 0,
            NX =5; NY=6;
    end
    addpath ../PolyLib
    %WXQ,WYQ,XQ,YQ,NX,NY
    [XQ,WXQ] = JacobiGLZW(NX+1,0,0);
    [YQ,WYQ] = JacobiGLZW(NY+1,0,0);
    %Cal Lagrance before itself.
    for i =0:NX
         PhiXQ(i+1,:) = LegendrePoly(i,XQ);
    end
    for j = 0:NY
        PhiYQ(j+1,:) = LegendrePoly(j,YQ);
    end
    for i = 0:NX
        for j = 0:NY
            sol = 0;
            for x = 1:max(size(XQ))
                for y = 1:max(size(YQ))
                    sol = sol + func(XQ(x),YQ(y))*WXQ(x)*WYQ(y)*...
                                   PhiXQ(i+1,x)*PhiYQ(j+1,y);
                end
            end
            u(i+1,j+1) = (2*i+1)*(2*j+1)/4*sol;
        end
    end
    %u(1,1) = 2.6;
    solPts = zeros( max(size(XQ)), max(size(YQ)) );
    for i = 0:NX
        for j = 0:NY
            solPts = solPts+u(i+1,j+1)* PhiXQ(i+1,:)'*PhiYQ(j+1,:);
        end
    end
    
    [X,Y] = meshgrid(XQ,YQ);
    data(:,1) = X(:);
    data(:,2) = Y(:);
    figure(1);
    surf(X,Y,func(X,Y)); hold on;
    surf(X,Y,solPts'); hold off;
    er = sum(sum(abs(func(X,Y)-solPts')))
%    plot3(data(:,1),data(:,2),func(data(:,1),data(:,2)),'g'); hold on;
%    plot3(data(:,1),data(:,2),solPts(:),'r');

    function z = func(x,y)
        z = x.*sin(3*pi*x)+y.*sin(3*pi*y);
    end
end
