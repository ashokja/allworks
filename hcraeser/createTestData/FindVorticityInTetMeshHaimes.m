function [] = FindVorticityInTetMeshHaimes(mapxy,mapxyV)
    [a,~] = size(mapxy);
    b = int64(a^(1/3));
    x = reshape(mapxy(:,1),b,b,b);
    y = reshape(mapxy(:,2),b,b,b);
    z = reshape(mapxy(:,3),b,b,b);
    u = reshape(mapxyV(:,1),b,b,b);
    v = reshape(mapxyV(:,2),b,b,b);
    w = reshape(mapxyV(:,3),b,b,b);
    addpath ../vortex_learning/

    
    
    FindVorticityInTetMesh(x,y,z,u,v,w);
    %hold on;
    plotBoundingBox();
    hold off;
end