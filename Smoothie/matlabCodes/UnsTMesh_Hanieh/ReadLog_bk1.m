% read log file.
close all;
clear;

prefix = '../../code/build_fsm3/';
LOGV = '2';

res ='025';
for Poly = 2:2
    filename = strcat('LOG',LOGV,'_NM',int2str(Poly),'_','Mesh',res,'.log');

    logFileName = strcat(prefix,filename);
    fid = fopen(logFileName,'r'); %# open csv file for reading
    line = fgets(fid);
    disp(line);
    line = fgets(fid);
    disp(line);
    line = fgets(fid);
    disp(line);
    % MuArray
    % DGL2
    % LSIACL2
    % DGLinf
    % LSIACLinf
    for i =1:4
        line = fgets(fid);
        line3 = fgets(fid);
        %disp(line3)
        A = sscanf(line3,'mu:\t%f');
        %disp(A);
        MuArray(i) = A;

        line4 = fgets(fid);
        disp(line4)
        A = textscan(line4,'%s %s %s %f %s %f %s %f');
        %A = sscanf(line3,'L2 error DG:%f SIAC %f SIACDyn %f');
    %    celldisp(A)
        cell2mat(A(4)); %DG
        cell2mat(A(6)); %SIACL2
        cell2mat(A(8)); %SIACDynL2
        LSIACDynL2(i) = cell2mat(A(8)); 

        line5 = fgets(fid);
    %    disp(line5)
        A = textscan(line5,'%s %s %s %f %s %f %s %f');
        %A = sscanf(line3,'L2 error DG:%f SIAC %f SIACDyn %f');
    %    celldisp(A)
        cell2mat(A(4));
        cell2mat(A(6));
        cell2mat(A(8));
        LSIACDynLinf(i) = cell2mat(A(8)); 
    end
	disp(LSIACDynL2);
%    figure(1)
%    plot(MuArray, log10(LSIACDynL2),'-*g'); hold on;
%    plot(MuArray, log10(LSIACDynLinf),'-or'); hold off;
    
%    figure(2),
%    plot(MuArray, log10(LSIACDynL2),'-*'); hold on;
    
    
 %   figure(3),
  %  plot(MuArray, log10(LSIACDynLinf),'-o'); hold on;
end

%figure(2),
%legend('P2','P3');
%title('LSIAC L2 R1(Figure 1) L^2 Error');
%xlabel('mu*(DynScaling)');
%ylabel('log_{10}(L^{2}Error)')
%print(strcat('Images/','Fig1_C1_L4_R1_L2'), '-dpng', '-r100');
%figure(3),
%legend('P2','P3');
%title('LSIAC L2 R1(Figure 1) L^{inf} Error');
%ylabel('log_{10}(L^{inf}Error)');
%xlabel('mu*(DynScaling)');
%print(strcat('Images/','Fig1_C1_L4_R1_Linf'), '-dpng', '-r100');

