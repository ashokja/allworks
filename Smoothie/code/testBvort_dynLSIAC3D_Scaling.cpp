// To be used for Bentvort_1.xml files.
// This test file will simulate any of 9 single derivatives depending on input parameters.

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"
//#include <ctime>



using namespace SIACUtilities;
using namespace std;

void calVorticity(int nq,
    const Array<OneD,NekDouble> &udx_phys, const Array<OneD,NekDouble> &udy_phys, const Array<OneD,NekDouble> &udz_phys,
    const Array<OneD,NekDouble> &vdx_phys, const Array<OneD,NekDouble> &vdy_phys, const Array<OneD,NekDouble> &vdz_phys,
    const Array<OneD,NekDouble> &wdx_phys, const Array<OneD,NekDouble> &wdy_phys, const Array<OneD,NekDouble> &wdz_phys,
    Array<OneD,NekDouble> &Vx,Array<OneD,NekDouble> &Vy,Array<OneD,NekDouble> &Vz )
{
    Vmath::Vsub(nq, wdy_phys,1, vdz_phys,1,Vx ,1);
    Vmath::Vsub(nq, udz_phys,1, wdx_phys,1,Vy ,1);
    Vmath::Vsub(nq, vdx_phys,1, udy_phys,1,Vz ,1);
    return;
}


int main(int argc, char* argv[])
{
	if (argc != 8)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		cout << "4th resolution of output" << endl;
		cout << "5th arg specifies which part of the simulation to run Select between 0-4" << endl;
		cout << "6th arg start of i index " << endl;
		cout << "7th arg end of  i index "<< endl;
		return 0;
	}
	// This is so that vSession does not interpret extra parameters absurdly.
	argc = 2;

	clock_t startTime = clock();
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);

	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	
	cout << "loading xml file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	string fname = vSession->GetSessionName();

	//string fldname = fname + ".fld/Info.xml";	
	//string fldname = "naca.fld";	
	string fldname = fname + ".fld";	
	cout << fname << endl;
	cout << fldname << endl;

	HNM3D->LoadData( fldname ,var);
	cout << "loading fld file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	cout << "expsize:\t"<< HNM3D->m_expansions[0]->GetExpSize() << endl;
	
	HNM3D->LoadExpListIntoRTree();
	cout << "loading into R-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	/*
	for (int i =0; i< HNM3D->m_expansions[0]->GetExpSize();i++)
	{
		int gid = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetGlobalID();
		cout << "i\t" << i << "\tg\t" << gid << endl;
		cout << HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetShapeType()<< endl;
	}
	*/
	cout << "Expansion Size\t " << endl;
	cout << "Expansion Size\t" << HNM3D->m_expansions[0]->GetExpSize() << endl;
	cout << "Total Number of Points \t" << HNM3D->m_expansions[0]->GetTotPoints() << endl;
	cout << "Total number of coefficients \t" << HNM3D->m_expansions[0]->GetNcoeffs() << endl;

	NekDouble minx, miny,minz,maxx,maxy,maxz;
	Array<OneD,NekDouble> coord(3,0.0);
    minx = 0.0; miny = -0.5; minz = -0.5;
    maxx = 1.0; maxy = 0.5; maxz= 0.5;
	int gPtsX =  atoi(argv[4]);
	int gPtsY =  atoi(argv[4]);
	int gPtsZ =  atoi(argv[4]);
	
	int Nx = gPtsX, Ny = gPtsY, Nz = gPtsZ;
	int totPts = Ny*Nz;
	NekDouble sx = 1.0/(Nx-1.0);
	NekDouble sy = 1.0/(Ny-1.0), sz=1.0/(Nz-1.0);

	Array<OneD,NekDouble> pX(totPts), pY(totPts), pZ(totPts), pE(totPts);	
	Array<OneD,NekDouble> pS_sDyn(totPts);
	HNM3D->CalculateDynamicScaling();	
	int index;
	NektarBaseClass k;
	int startOfiIndex = atoi(argv[6]);
	int endOfiIndex = atoi(argv[7]);
	for (int i =startOfiIndex; i <endOfiIndex; i++)
	{
		for(int j=0; j<Ny; j++)
		{
			for(int k=0; k<Nz; k++)
			{
//				j = 24; k=23;
				index = j*Nz+k;
				//cout << index << endl;
				pX[index] = minx + i*sx*(maxx-minx); 
				pY[index] = miny + j*sy*(maxy-miny);
				pZ[index] = minz + k*sz*(maxz-minz);
//				cout << "\t" << pX[index] << "\t" << pY[index] <<"\t"<< pZ[index] << endl;
				coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = pZ[index];
				//int elID = HNM3D->m_expansions[0]->GetExpIndex(coord);
				int elID = HNM3D->GetExpansionIndexUsingRTree(coord);
//				cout << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;	
//				cout << "eid: \t"<<elID<< "\t index \t"<< index << endl;
				pE[index] = elID;

				if (elID !=-1)
				{
					NekDouble dynScaling = HNM3D->GetDynamicScaling(coord,elID,1.0);
					pS_sDyn[index] = dynScaling;
				}else
				{	
					pS_sDyn[index] = 0.0;
					cout << "out" << endl;
				}
			}
			cout << "loop for each iteration "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds for Nz = " << Nz<< endl;
			startTime = clock();
			cout << "j = " << j<< endl;
		}
		cout << "i = " << i << endl;
		k.writeNekArray(pS_sDyn,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_3D_i_"+to_string(i)+"_R_"+argv[4]+"_DLISAC_pS_sDyn.txt");
	}	
	cout << "writing data out "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	return 0;
}


/*
	vector<NekDouble> elementSizes;
    vector<NekDouble> centersX,centersY, centersZ;
	NekDouble sumSizes = 0.0;
    for (int j =0; j< elementIds.size();j++)
    {   
		int i = elementIds[j];
        int NumVerts = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetNumVerts();
        assert("NumVerts!=3"&& "This logic is not built for elements not tetrahedrons.");
        Nektar::SpatialDomains::PointGeomSharedPtr v0 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(0);
        Nektar::SpatialDomains::PointGeomSharedPtr v1 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(1);
        Nektar::SpatialDomains::PointGeomSharedPtr v2 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(2);
        Nektar::SpatialDomains::PointGeomSharedPtr v3 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(3);
        NekDouble ab = v0->dist(*v1);
        NekDouble ac = v0->dist(*v2);
        NekDouble ad = v0->dist(*v3);
        NekDouble bc = v1->dist(*v2);
        NekDouble bd = v1->dist(*v3);
        NekDouble cd = v2->dist(*v3);
        NekDouble max = 0.0;
        max = std::max(max,ab);
        max = std::max(max,ac);
        max = std::max(max,ad);
        max = std::max(max,bc);
        max = std::max(max,bd);
        max = std::max(max,cd);
        elementSizes.push_back(max);
        NekDouble v0x,v0y,v0z;
        NekDouble v1x,v1y,v1z;
        NekDouble v2x,v2y,v2z;
        NekDouble v3x,v3y,v3z;
        v0->GetCoords(v0x,v0y,v0z);
        v1->GetCoords(v1x,v1y,v1z);
        v2->GetCoords(v2x,v2y,v2z);
        v3->GetCoords(v3x,v3y,v3z);
        NekDouble cx = (v0x+v1x+v2x+v3x)/4.0;
        NekDouble cy = (v0y+v1y+v2y+v3y)/4.0;
        NekDouble cz = (v0z+v1z+v2z+v3z)/4.0;
        centersX.push_back(cx), centersY.push_back(cy), centersZ.push_back(cz);
		sumSizes+=max;
    }   

	NekDouble AvgScaling = sumSizes/elementIds.size() ; 
	cout << "AverageSize" << sumSizes/elementIds.size() << endl;
	NekDouble maxSize = *(std::max_element(elementSizes.begin(),elementSizes.end()));
	cout << "Max Size" << maxSize << endl; 
	
	cout << endl;

*/


/*
	vector<NekDouble> elementSizes;
    vector<NekDouble> centersX,centersY, centersZ;
	NekDouble sumSizes = 0.0;
    for (int j =0; j< elementIds.size();j++)
    {   
		int i = elementIds[j];
        int NumVerts = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetNumVerts();
        assert("NumVerts!=3"&& "This logic is not built for elements not tetrahedrons.");
        Nektar::SpatialDomains::PointGeomSharedPtr v0 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(0);
        Nektar::SpatialDomains::PointGeomSharedPtr v1 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(1);
        Nektar::SpatialDomains::PointGeomSharedPtr v2 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(2);
        Nektar::SpatialDomains::PointGeomSharedPtr v3 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(3);
        NekDouble ab = v0->dist(*v1);
        NekDouble ac = v0->dist(*v2);
        NekDouble ad = v0->dist(*v3);
        NekDouble bc = v1->dist(*v2);
        NekDouble bd = v1->dist(*v3);
        NekDouble cd = v2->dist(*v3);
        NekDouble max = 0.0;
        max = std::max(max,ab);
        max = std::max(max,ac);
        max = std::max(max,ad);
        max = std::max(max,bc);
        max = std::max(max,bd);
        max = std::max(max,cd);
        elementSizes.push_back(max);
        NekDouble v0x,v0y,v0z;
        NekDouble v1x,v1y,v1z;
        NekDouble v2x,v2y,v2z;
        NekDouble v3x,v3y,v3z;
        v0->GetCoords(v0x,v0y,v0z);
        v1->GetCoords(v1x,v1y,v1z);
        v2->GetCoords(v2x,v2y,v2z);
        v3->GetCoords(v3x,v3y,v3z);
        NekDouble cx = (v0x+v1x+v2x+v3x)/4.0;
        NekDouble cy = (v0y+v1y+v2y+v3y)/4.0;
        NekDouble cz = (v0z+v1z+v2z+v3z)/4.0;
        centersX.push_back(cx), centersY.push_back(cy), centersZ.push_back(cz);
		sumSizes+=max;
    }   

	NekDouble AvgScaling = sumSizes/elementIds.size() ; 
	cout << "AverageSize" << sumSizes/elementIds.size() << endl;
	NekDouble maxSize = *(std::max_element(elementSizes.begin(),elementSizes.end()));
	cout << "Max Size" << maxSize << endl; 
	
	cout << endl;

*/


/*
	vector<NekDouble> elementSizes;
    vector<NekDouble> centersX,centersY, centersZ;
	NekDouble sumSizes = 0.0;
    for (int j =0; j< elementIds.size();j++)
    {   
		int i = elementIds[j];
        int NumVerts = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetNumVerts();
        assert("NumVerts!=3"&& "This logic is not built for elements not tetrahedrons.");
        Nektar::SpatialDomains::PointGeomSharedPtr v0 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(0);
        Nektar::SpatialDomains::PointGeomSharedPtr v1 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(1);
        Nektar::SpatialDomains::PointGeomSharedPtr v2 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(2);
        Nektar::SpatialDomains::PointGeomSharedPtr v3 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(3);
        NekDouble ab = v0->dist(*v1);
        NekDouble ac = v0->dist(*v2);
        NekDouble ad = v0->dist(*v3);
        NekDouble bc = v1->dist(*v2);
        NekDouble bd = v1->dist(*v3);
        NekDouble cd = v2->dist(*v3);
        NekDouble max = 0.0;
        max = std::max(max,ab);
        max = std::max(max,ac);
        max = std::max(max,ad);
        max = std::max(max,bc);
        max = std::max(max,bd);
        max = std::max(max,cd);
        elementSizes.push_back(max);
        NekDouble v0x,v0y,v0z;
        NekDouble v1x,v1y,v1z;
        NekDouble v2x,v2y,v2z;
        NekDouble v3x,v3y,v3z;
        v0->GetCoords(v0x,v0y,v0z);
        v1->GetCoords(v1x,v1y,v1z);
        v2->GetCoords(v2x,v2y,v2z);
        v3->GetCoords(v3x,v3y,v3z);
        NekDouble cx = (v0x+v1x+v2x+v3x)/4.0;
        NekDouble cy = (v0y+v1y+v2y+v3y)/4.0;
        NekDouble cz = (v0z+v1z+v2z+v3z)/4.0;
        centersX.push_back(cx), centersY.push_back(cy), centersZ.push_back(cz);
		sumSizes+=max;
    }   

	NekDouble AvgScaling = sumSizes/elementIds.size() ; 
	cout << "AverageSize" << sumSizes/elementIds.size() << endl;
	NekDouble maxSize = *(std::max_element(elementSizes.begin(),elementSizes.end()));
	cout << "Max Size" << maxSize << endl; 
	
	cout << endl;

*/


