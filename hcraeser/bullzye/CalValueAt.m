function [solPts] = CalValueAt(coeff,XQ,YQ,ZQ)
    solPts = zeros( max(size(XQ)), max(size(YQ)),max(size(ZQ)) );
    [NX,NY,NZ] = size(coeff);
    NX = NX-1; NY = NY-1; NZ = NZ-1; % since coeff have order and NX is degree.
    for i = 0:NX
        for j = 0:NY
            for k = 0:NZ
                solPts = solPts+coeff(i+1,j+1,k+1)*...
                    outer(outer(LegendrePoly(i,XQ),LegendrePoly(j,YQ),2),LegendrePoly(k,ZQ),3);
            end
        end
    end
    
end