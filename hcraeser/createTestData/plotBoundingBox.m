function plotBoundingBox(Xa,Xb,Xc,Xd,Xe,Xf,Xg,Xh)
    switch nargin
            case 0
                Xa = [-1,-1,-1];
                Xb = [1,-1,-1];
                Xc = [1,1,-1];
                Xd = [-1,1,-1];
                Xe = [-1,-1,1];
                Xf = [1,-1,1];
                Xg = [1,1,1];
                Xh = [-1,1,1];
                hold on;
    end
    X = [Xa(1),Xb(1),Xc(1),Xd(1),Xa(1),Xe(1),Xf(1),Xg(1),Xh(1),Xe(1),Xf(1),Xb(1),Xc(1),Xg(1),Xh(1),Xd(1) ];
    Y = [Xa(2),Xb(2),Xc(2),Xd(2),Xa(2),Xe(2),Xf(2),Xg(2),Xh(2),Xe(2),Xf(2),Xb(2),Xc(2),Xg(2),Xh(2),Xd(2) ];
    Z = [Xa(3),Xb(3),Xc(3),Xd(3),Xa(3),Xe(3),Xf(3),Xg(3),Xh(3),Xe(3),Xf(3),Xb(3),Xc(3),Xg(3),Xh(3),Xd(3) ];
    plot3(X,Y,Z);
    xlabel('x'); ylabel('y'); zlabel('z');
    hold on;

    return;
end