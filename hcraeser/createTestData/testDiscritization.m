% test polynomial interpolation error.
% assumed mapxy and mapxyV is already loaded.

clear;
addpath ../VortexOnBasisEl
r = 1;
%count = 1;
N = [7];
%drawStreamLine(mapxy,mapxyV,curveCoeff);
%hold off
%%
%[xy,distance,t_a,mapxy,mapxyV,coeff] = test_runNodes(r,nodes);
% this is for input test velocity.
% % title('Input test Velocity');
% xlabel('x'); ylabel('y'); zlabel('z'); hold off;
% view(-110.5,16);
% %caxis([0.9,1]);
% colorbar;
%saveas( gcf, strcat('images/testPlotSimple1',int2str(0)), 'jpg' );
%[a,b] = size(mapxy);
%data_poly = zeros(max(N),a,b);
% coeff_poly = {};
% data_poly = {}; data_polyD = {};
% for n = N
%     [xy,distance,t_a,mapxy,mapxyV,curveCoeff] = test_runNodes(r,n+2);
%     [a,b] = size(mapxy);
%     [coeff4] = buildCoeff(n,n,n,mapxy,mapxyV);
%     coeff_poly(n) = {coeff4};
%     data_poly(n) = {mapxyV};
%     dataV4 = buildData(n,n,n,coeff4,mapxy);
%     data_polyD(n) = {dataV4};
%     drawStreamLine(mapxy,dataV4,curveCoeff);
%     hold off
%     title('Input test Velocity');
%     xlabel('x'); ylabel('y'); zlabel('z'); hold off;
%     view(-110.5,16);
%  %   caxis([0.9,1]);
%     colorbar;
%     %saveas( gcf, strcat('images/testPlotSimple1',int2str(n)), 'jpg' );
% end

%%
% title('Rotation Velocity');
% xlabel('x'); ylabel('y'); zlabel('z'); hold off;
% view(-110.5,16);
% caxis([0.9,1]);
% colorbar;
% saveas( gcf, strcat('images/testPlotSimple1',int2str(0)), 'jpg' );
count = 1;
uARot = {}; uARed = {};
for n = N
    [xy,distance,t_a,mapxy,mapxyV,curveCoeff] = test_runNodes(r,n+1);
    [coeff4] = buildCoeff(n,n,n,mapxy,mapxyV);
    dataV4 = buildData(n,n,n,coeff4,mapxy);
   % [xy,distance,t_a,mapxy,mapxyV,curveCoeff] = test_runNodes(r,n+2);
    [uRedCoeff,uRotCoeff] = divideVel(n,n,n,coeff4,mapxy,dataV4);
    dRedCoeff = buildData(n,n,n,uRedCoeff,mapxy);
    dRotCoeff = buildData(n,n,n,uRotCoeff,mapxy);
    uARed(n) = {uRedCoeff};  
    uARot(n) = {uRotCoeff};
    dARed(n) = {dRedCoeff};
    dARot(n) = {dRotCoeff};
    figure(2)
    %Save Fig Rotational velocity
    drawStreamLine(mapxy,dRotCoeff,curveCoeff);
    hold off
    title(strcat('Rotational Velocity',int2str(n)));
    xlabel('x'); ylabel('y'); zlabel('z'); hold off;
    view(-110.5,16);
   % caxis([0,1]);
    colorbar;
    st = strcat('images/testPlotSimpleRot1_',int2str(n));
    st2 = strcat(st,'.fig');
    %savefig(st2);
    %saveas( gcf, strcat('images/testPlotSimpleRot1_',int2str(n)), 'jpg' );
    figure(3)
    %Save Fig Reduced Velocity
    drawStreamLine(mapxy,dRedCoeff,curveCoeff);
    hold off
    title(strcat('Reduced Velocity',int2str(n)));
    xlabel('x'); ylabel('y'); zlabel('z'); hold off;
    view(-110.5,16);
    %caxis([0,1]);
    %colorbar;
    st = strcat('images/testPlotSimpleRed1_',int2str(n));
    st2 = strcat(st,'.fig');
    %savefig(st2);
    %saveas( gcf, strcat('images/testPlotSimpleRed1_',int2str(n)), 'jpg' );
end

save('testdata/testDataSimple_1_05_All_poly_13_lgl_2.mat');

%%
figure(72);
[sol_seeds,goodSeeds,solAtSeeds] = zerosAtProjected(n,n,n,uRotCoeff,2);
%%plot(N,merr,'r',N,maxerr,'g');
