% which c++ file creates these guys. testDTVor_EL
% does element by elmenet.
% 


clear;
close all;
va = -11.5; vb = 44;

rate = 2; % poly
samplerate = 10;
Order = 2;
lc = 1;
Elres = 4;

wstr = '10';

if lc ==1
    letter = strcat('_R_',int2str(Elres),'_EL_O_',int2str(Order),'_w_',wstr) ;
%    letter = strcat('_EL_O_',int2str(Order),'_w_',wstr) ;
end
%filter = 1;

filename = strcat( 'P',int2str(rate),'_CubeGrid_',int2str(samplerate),letter );

efname = strcat('../datatxt/VizData/',filename,'_e.txt' )
xfname = strcat('../datatxt/VizData/',filename,'_x.txt' );
yfname = strcat('../datatxt/VizData/',filename,'_y.txt' );
zfname = strcat('../datatxt/VizData/',filename,'_z.txt' );

upfname = strcat('../datatxt/VizData/',filename,'_up.txt' );
vpfname = strcat('../datatxt/VizData/',filename,'_vp.txt' );
wpfname = strcat('../datatxt/VizData/',filename,'_wp.txt' );

Vxpfname = strcat('../datatxt/VizData/',filename,'_Vor_xp.txt' );
Vypfname = strcat('../datatxt/VizData/',filename,'_Vor_yp.txt' );
Vzpfname = strcat('../datatxt/VizData/',filename,'_Vor_zp.txt' );

ufname = strcat('../datatxt/VizData/',filename,'_u.txt' );
vfname = strcat('../datatxt/VizData/',filename,'_v.txt' );
wfname = strcat('../datatxt/VizData/',filename,'_w.txt' );

Vxfname = strcat('../datatxt/VizData/',filename,'_Vor_x.txt' );
Vyfname = strcat('../datatxt/VizData/',filename,'_Vor_y.txt' );
Vzfname = strcat('../datatxt/VizData/',filename,'_Vor_z.txt' );

elval = floor( textread(efname, '%f\t')+0.0001);
xdata = textread(xfname, '%f\t');
ydata = textread(yfname, '%f\t');
zdata = textread(zfname, '%f\t');

updata = textread(upfname, '%f\t');     udata = textread(ufname, '%f\t');
vpdata = textread(vpfname, '%f\t');     vdata = textread(vfname, '%f\t');
wpdata = textread(wpfname, '%f\t');     wdata = textread(wfname, '%f\t');

Vxpdata = textread(Vxpfname,'%f\t');    Vxsdata = textread(Vxfname,'%f\t');
Vypdata = textread(Vypfname,'%f\t');    Vysdata = textread(Vyfname,'%f\t');
Vzpdata = textread(Vzpfname,'%f\t');    Vzsdata = textread(Vzfname,'%f\t');

VorData = (Vxpdata.^(2) + Vypdata.^(2) + Vzpdata.^(2)).^(1/2) ;

[unique_ids,id_offset,id_tags] = unique(elval);

for tag = unique_ids'
    el_indices = find(elval == (zeros(size(elval))+tag) );
    
    el_x = xdata(el_indices);
    el_y = ydata(el_indices);
    el_z = zdata(el_indices);
    el_Vxp = Vxpdata(el_indices);
    el_Vyp = Vypdata(el_indices);
    el_Vzp = Vzpdata(el_indices);
%     el_Vzs = Vzsdata(el_indices);
    el_Vor = VorData(el_indices);
    s =  floor( (max(size(el_x)))^(1/3)+0.00001);

    el_x = reshape(el_x,[s,s,s]);
    el_y = reshape(el_y,[s,s,s]);
    el_z = reshape(el_z,[s,s,s]);
    el_Vxp = reshape(el_Vxp,[s,s,s]);
    el_Vyp = reshape(el_Vyp,[s,s,s]);
    el_Vzp = reshape(el_Vzp,[s,s,s]);
%     el_Vzs = reshape(el_Vzs,[s,s,s]);
    el_Vor = reshape(el_Vor,[s,s,s]);
   
    el_x = round(el_x,8);
    el_y = round(el_y,8);
    el_z = round(el_z,8);
    
%     
% %    el_Vormag = ( el_Vxp.^(2) + el_Vyp.^(2) + el_Vzp.^(2) ).^(1/2);
%     
    figure(1)
    p = patch(isosurface(el_x,el_y,el_z,el_Vxp,10));
    isonormals(el_x,el_y,el_z,el_Vxp, p)
    p.FaceColor = 'red';
    p.EdgeColor = 'none';
% 
%     
%    figure(2)
    p = patch(isosurface(el_x,el_y,el_z,el_Vyp,-10));
    isonormals(el_x,el_y,el_z,el_Vyp, p)
    p.FaceColor = 'blue';
    p.EdgeColor = 'none';
%     
%    figure(3)
    p = patch(isosurface(el_x,el_y,el_z,el_Vzp,1.5));
    isonormals(el_x,el_y,el_z,el_Vzp, p)
    p.FaceColor = 'green';
    p.EdgeColor = 'none';

    figure(4)
    p = patch(isosurface(el_x,el_y,el_z,el_Vor,20));
    isonormals(el_x,el_y,el_z,el_Vor, p)
    p.FaceColor = 'cyan';
    p.EdgeColor = 'none';
    %max(el_Vormag(:))
    %min(el_Vormag(:))
    
end
%%
figure(1)
   daspect([1,1,1])
   view(va,vb); 
   camlight 
   lighting gouraud
   plotBoundingBox();  
   axis off;
   axis equal;
   title('Red = V_x = -10, Blue= V_y = 10, Green V_z=1.5');
  % print(strcat('images/',filename,'_Vx_dg' ), '-dpng', '-r500');
%   print('example', '-dpng', '-r300');
   
   % title('VorX (iso surface=0.5)vec using DG');
% % 
% % % % % figure(2)
% % % % %    daspect([1,1,1])
% % % % %    view(va,vb); 
% % % % %    camlight 
% % % % %    lighting gouraud
% % % % %       plotBoundingBox();  
% % % % %    axis off;
% % % % %    axis equal;
% %  %  print(strcat('images/',filename,'_Vy_dg' ), '-dpng', '-r500');
% %    
% %     
% % % % %  figure(3)
% % % % %    daspect([1,1,1])
% % % % %    view(va,vb); axis equal
% % % % %    camlight 
% % % % %    lighting gouraud
% % % % %    axis off;
% % % % %    axis equal;
% %   % print(strcat('images/',filename,'_Vz_dg' ), '-dpng', '-r500');
% %    %plotBoundingBox();  
% %    %title('VorZ (iso surface=1.5)vec using DG');
   
 figure(4)
plotBoundingBox(); hold on;
   daspect([1,1,1])
   view(va,vb); axis equal
   camlight 
   lighting gouraud
   axis off;
   axis equal;
 title('Cyan V_mag=10');
% for w = 0.5
% IsoSurface values z=1.5, x = 0.5, y = -0.5
% fo w = 5 
% IsoSurface values z=1.5, x = 3.2, y = -3.2    

% fo w = 15 
% IsoSurface values z=1.5, x = 8.2, y = -8.2    