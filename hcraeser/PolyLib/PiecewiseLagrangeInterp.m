function yI = PiecewiseLagrangeInterp( xI, x, y, Nelmt );
% Evaluates a piecewise Lagrange Interpolating polynomial.
% 
%  [yI] = PiecewiseLagrangeInterp( xI, x, y, Nelmt );
%
%  Parameters:
%      xI    = points to interpolate to
%      x     = interpolation nodes
%      y     = value of interpolant at interpolation nodes
%      Nelmt = number of piecewise interpolation polynomials to use over
%              the points in x. (i.e. elements)
%
%  Returns: 
%      yI    = interpolated values at the xI
%
%  Notes:
%    Assumes that global interpolating polynomial is continuous, so nodes
%    at element boundaries are shared. Also assumes an equal number of 
%    points in each element.
%
%  Created on 01 Nov 2007
%          by Samuel Isaacson (isaacson@math.utah.edu)
%
%  Redistribution of this code is forbidden.
%
%

xILocIdxFirst = 1;
xILocIdxLast  = 1;
order         = (length(x) - 1) / Nelmt;
xIdxs         = 1:(order+1);
yI            = zeros( size(xI) );

for( n = 1:Nelmt )

  % indexes in xI for this element
  xILocIdxLast  = xILocIdxLast-1 + find( xI(xILocIdxLast:end) <= x(xIdxs(end)), 1, 'last' );
  xILocIdxs     = xILocIdxFirst:xILocIdxLast;
  
  yI(xILocIdxs) = LagrangeInterpolant( xI(xILocIdxs), order+1, x(xIdxs), y(xIdxs) );
  
  % indexes in x for next element
  xIdxs = xIdxs(end) + (0:order);

  % move xI window forward
  xILocIdxLast  = xILocIdxLast + 1;
  xILocIdxFirst = xILocIdxLast;
end


