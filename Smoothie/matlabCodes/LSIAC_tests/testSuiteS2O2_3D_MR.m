clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [20];

poly =2;
order = poly;
count =1;
%alpha =1;
for r = rates
    
    xfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_pX_3D_OneSided2kp1.txt' );
    yfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_pY_3D_OneSided2kp1.txt' );
    zfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_pZ_3D_OneSided2kp1.txt' );
    ffname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_pV_3D_OneSided2kp1.txt' );
    efname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_pP_3D_OneSided2kp1.txt' );
    sfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_pS_3D_OneSided2kp1.txt' );
    
    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');
    zpos = textread(zfname, '%f\t');
    
    fval = textread(ffname, '%f\t');
    pval = textread(efname, '%f\t');
    sval = textread(sfname, '%f\t'); 
    
    s = floor( (max(size(xpos)))^(1/3) +0.1);
    
    xVpos = reshape(xpos,s,s,s);
    xVpos = permute(xVpos,[2,3,1]);
    yVpos = reshape(ypos,s,s,s);
    yVpos = permute(yVpos,[2,3,1]);
    zVpos = reshape(zpos,s,s,s);
    zVpos = permute(zVpos,[2,3,1]);
    
    fVval = reshape(fval,s,s,s);
    pVval = reshape(pval,s,s,s);
    sVval = reshape(sval,s,s,s); 
    sVval = permute(sVval,[2,3,1]);
    
    figure,
    hsurfaces = slice(xVpos,yVpos,zVpos,fVval,[0,0.5,1],1,0);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold off; caxis([-1,1]);
    title('FUNC(ACT')

    figure,
    hsurfaces = slice(xVpos,yVpos,zVpos,pVval,[0,0.5,1],1,0);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold off; caxis([-1,1])
    title('Proj')

    figure,
    hsurfaces = slice(xVpos,yVpos,zVpos,sVval,[0,0.5,1],1,0);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold off; caxis([-1,1])
    title('SIAC')
    
%     
%     figure,
%     hsurfaces = slice(yVpos,zVpos,xVpos,fVval,[0,0.5,1],1,0);
%     set(hsurfaces,'FaceColor','interp','EdgeColor','none')
%     xlabel('x'); ylabel('y'); zlabel('z');
%     colormap jet; colorbar; hold off;
%     
%     
%     figure,
%     hsurfaces = slice(yVpos,zVpos,xVpos,pVval,[0,0.5,1],1,0);
%     set(hsurfaces,'FaceColor','interp','EdgeColor','none')
%     xlabel('x'); ylabel('y'); zlabel('z');
%     colormap jet; colorbar; hold off;
%     
%     figure,
%     hsurfaces = slice(yVpos,zVpos,xVpos,sVval,[0,0.5,1],1,0);
%     set(hsurfaces,'FaceColor','interp','EdgeColor','none')
%     xlabel('x'); ylabel('y'); zlabel('z');
%     colormap jet; colorbar; hold off;

end
