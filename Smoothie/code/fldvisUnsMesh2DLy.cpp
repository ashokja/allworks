#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include <boost/math/special_functions/sinc.hpp>
#include <boost/timer.hpp>
#include <chrono>

typedef std::chrono::high_resolution_clock Clock;


using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg conditions file." << endl;
		cout << "3rd arg polynomial degree filter you want to apply" << endl;
		cout << "4th arg Resolution of output." << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	//HNM2D->LoadMesh(var[0]);
	string fname = vSession->GetSessionName();
	HNM2D->LoadData(fname+".fld",var);

	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
    

	// Define forcing function for first variable defined in file
	LibUtilities::EquationSharedPtr ffunc
			= vSession->GetFunction("ExactSolution", 0); 
	Array<OneD,NekDouble> fce = Array<OneD,NekDouble>(tNquadPts); 
	fce = HNM2D->m_expansions[0]->GetPhys();
	//ffunc->Evaluate(xc0,xc1,xc2, fce);
/*	
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);

	for (int i=0;  i< tNquadPts; i++)
	{
//		fce[i] = std::cos(2.0*2.0*M_PI*(xc0[i]+xc1[i]));
//		fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]));
		fce[i] = boost::math::sinc_pi(4.0*M_PI*( xc0[i] - 0.5 )) * boost::math::sinc_pi(4.0*M_PI*( xc1[i] - 0.5 )) ;
		//fce[i] = std::cos(2.0*2.0*M_PI*(xc0[i]))*std::cos(2.0*2.0*M_PI*(xc1[i]));
	}
	HNM2D->m_expansions[0]->FwdTrans(fce,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	 ece = HNM2D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM2D->m_expansions[0]->GetCoeffs();

	HNM2D->m_Arrays.push_back(ece);
*/
	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();

	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1, HNM2D, atoi(argv[1+2]), scaling ); 
// Evaluate on a new equal space grid mesh.
	int gPts = atoi(argv[1+3]);
	int totPts = gPts*gPts;
	int Nx = gPts, Ny=gPts;
	//NekDouble sx = 0.5/(Nx-1.0), sy = 1.0/(Ny-1.0);
	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0);
	cout << "Line98" << endl;	
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	Array<OneD,NekDouble> direction(3,0.0), coord(3,0.0);
	direction[0] = 1.0;
	Array<OneD,NekDouble> pX(totPts), pY(totPts), pV(totPts), pP(totPts), pS(totPts),pDyn(totPts),pSDyn(totPts),pE(totPts);
	Array<OneD,NekDouble> pTDyn(totPts);
	Array<OneD,NekDouble> pT(totPts);
	Array<OneD,NekDouble> glCoords(3,0.0);
	NektarBaseClass k;
/*
	for( int i =0; i < Nx; i++)
	{
		for(int j=0; j< Ny; j++)
		{
			int index = i*Nx+j;
			//pX[index] = i*sx+0.25;
			pX[index] = i*sx;
			pY[index] = j*sy;
			pV[index] = ffunc->Evaluate(pX[index],pY[index]);
			//pV[index] = std::cos(2.0*M_PI*(pX[index] + pY[index]) );
			//pV[index] = std::cos(2.0*2.0*M_PI*(pX[index]))*std::cos(2.0*2.0*M_PI*(pY[index]));
			glCoords[0] = pX[index]; glCoords[1] = pY[index];
			
			boost::timer tim1;
			bool TDynRes, TRes;
			auto t1 = Clock::now();
				NekDouble dynScaling = HNM2D->GetDynamicScaling(glCoords);
				pDyn[index] = dynScaling;
				TDynRes = sm.EvaluateAt(pX[index],pY[index],0.0,pSDyn[index],valY,valZ,direction, dynScaling ,0);
			auto t2 = Clock::now();
		    std::chrono::duration<double> elapsed_seconds = t2-t1;
			pTDyn[index] = elapsed_seconds.count();
//std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();

			//pTDyn[index] = tim1.elapsed();
			//boost::timer tim2;
			auto tt1 = Clock::now();
				TRes = sm.EvaluateAt(pX[index],pY[index],0.0,pS[index],valY,valZ,direction, scaling,0);
			auto tt2 = Clock::now();
			//pT[index] = std::chrono::duration_cast<std::chrono::nanoseconds>(tt2 - tt1).count();
		    std::chrono::duration<double> elapsed_secondst = tt2-tt1;
			pT[index] = elapsed_secondst.count();
			//cout << pT[index] << "\t " <<pTDyn[index] << endl;
		//	pT[index] = tim2.elapsed();
			coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = 0.0;
//			cout << setprecision(19) << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;
			int elid = HNM2D->GetExpansionIndexUsingRTree(coord);
//			cout << elid << endl;
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
			//int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
			const Array<OneD,NekDouble> el_phys = fce.CreateWithOffset(
										fce, coeffOffset);
			pP[index] = lexp->PhysEvaluate(coord,el_phys);
			pE[index] = elid;
//			cout << j << endl;
		}
		cout << i << endl;
	}
	k.writeNekArray(pX,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pX_2DDyn.txt");
	k.writeNekArray(pY,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pY_2DDyn.txt");
	k.writeNekArray(pE,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pE_2DDyn.txt");
	k.writeNekArray(pV,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pV_2DDyn.txt");
	k.writeNekArray(pP,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pP_2DDyn.txt");
	k.writeNekArray(pS,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pS_2DDyn.txt");
	k.writeNekArray(pSDyn,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pSDyn_2DDyn.txt");
	k.writeNekArray(pDyn,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pDyn_2DDyn.txt");
	k.writeNekArray(pTDyn,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pTDyn_2DDyn.txt");
	k.writeNekArray(pT,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pT_2DDyn.txt");

	cout << "Done writing plane" << endl;
*/
 	//plane completed.
	// Evaluating on line y =0;
	int gPtsLy = atoi(argv[1+3])*10;
	int totPtsLy = gPtsLy;
	sy = 1.0/(gPtsLy-1.0);
	Array<OneD,NekDouble> pLy(totPtsLy),pLNx(totPtsLy), pLPx(totPtsLy), pELy(totPtsLy);
	Array<OneD,NekDouble> pXLy(totPtsLy), pYLy(totPtsLy), pVLy(totPtsLy), pPLy(totPtsLy), pSLy(totPtsLy),pDynLy(totPtsLy),pSDynLy(totPtsLy);
	Array<OneD,NekDouble> pTDynLy(totPtsLy), pTLy(totPtsLy);

	for (int i =0 ; i< gPtsLy; i++)
	{
		int index = i;
		pYLy[index] = 0.5;
		pXLy[index] = i*sy;
			//pVLy[index] = std::cos(2.0*M_PI*(pXLy[index] + pYLy[index]) );
		//	pVLy[index] = boost::math::sinc_pi(4.0*M_PI*( pXLy[index] - 0.5 )) * boost::math::sinc_pi(4.0*M_PI*( pYLy[index] - 0.5 )) ;
			glCoords[0] = pXLy[index]; glCoords[1] = pYLy[index];

			boost::timer tim1;
			NekDouble dynScaling = HNM2D->GetDynamicScaling(glCoords);
			pDynLy[index] = dynScaling;
			sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSDynLy[index],valY,valZ,direction, dynScaling ,0);
			pTDynLy[index] = tim1.elapsed();

			boost::timer tim2;
			sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSLy[index],valY,valZ,direction, scaling ,0);
			pTLy[index] = tim2.elapsed();

			coord[0] = pXLy[index]; coord[1] = pYLy[index]; coord[2] = 0.0;
//			cout << setprecision(19) << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;
			int elid = HNM2D->GetExpansionIndexUsingRTree(coord);
//			cout << elid << endl;
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
			//int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
			const Array<OneD,NekDouble> el_phys = fce.CreateWithOffset(
										fce, coeffOffset);
			pPLy[index] = lexp->PhysEvaluate(coord,el_phys);
			pELy[index] = elid;
//			cout << j << endl;
	}
	ffunc->Evaluate(pXLy,pYLy,pYLy,pVLy);
//	pVLy[index] = ffunc->Evaluate(pXLy[index],pYLy[index]);

	cout << "Done writing Line" << endl;
	k.writeNekArray(pXLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pXLy_2DDyn.txt");
	k.writeNekArray(pYLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pYLy_2DDyn.txt");
	k.writeNekArray(pELy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pELy_2DDyn.txt");
	k.writeNekArray(pVLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pVLy_2DDyn.txt");
	k.writeNekArray(pPLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pPLy_2DDyn.txt");
	k.writeNekArray(pSLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pSLy_2DDyn.txt");
	k.writeNekArray(pSDynLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pSDynLy_2DDyn.txt");
	k.writeNekArray(pDynLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pDynLy_2DDyn.txt");
	k.writeNekArray(pTDynLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pTDynLy_2DDyn.txt");
	k.writeNekArray(pTLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pTLy_2DDyn.txt");

	// Evaluate on line x =0.5; x = -0.5;
/*
	 string out = vSession->GetSessionName() + ".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (HNM2D->m_expansions[0]->GetFieldDefinitions()).at(0);
    FieldDef[0]->m_fields.push_back("u");
    FieldDef[0]->m_fields.push_back("v");
    FieldDef[0]->m_fields.push_back("w");
    HNM2D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_fld->Write(out, FieldDef, FieldData);
*/
	return 0;
}
