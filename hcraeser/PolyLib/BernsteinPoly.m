

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %
  %
  %
  %
  %
  %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  function y = BernsteinPoly(index,maxdegree,x)

  x = 0.5*(x+1.0);

  if index < 0
      y = 0;
  elseif index > maxdegree
      y = 0;
  else
    y = nchoosek(maxdegree,index)*x.^index.*(1-x).^(maxdegree-index);
  end
  
  