% solution for normal newton iteration.
function [listRes,splits] = solveBernNewton1D(evalB, coeff,X,count,ss)

    switch nargin
        case 0,
            evalB = @evalBInterval;
            coeff = [0,1,0,-1,0];
            X = [-1,1];
            count =1;
            ss =1;
    end
    switch nargin
        case 3,
            count =1;
            ss =1;
    end

%Evaluate F[X] DF[X].
    fhx = evalB(1,coeff,X);%fh(X);
    fhxz = hasZero(fhx);
    listRes = count+1;
    splits = ss;
    %if F[X] does not contain zero. then return.
    if ~fhxz
      %  fprintf('Ingnoring Interval'); X
        return;
    end
    
    % If X1 is very small interval. print into solution.
    if (X(2) - X(1)) < 10^(-6)
        fprintf('Found root:');
        X
        return;
    end
%if F[X] contains a zero and DF[X] does not contain zero
   % X1 = N(X) intersect X
   % If X1 is very small interval. print into solution.
   % Solve (X1);
    dfhx = evalB(2,coeff,X);
    dfhxz = hasZero(dfhx);

    if ~dfhxz
        X1 = IntBernNew1D(evalB,coeff,X);
        if isnan(X1)
    %    fprintf('Ingnoring Interval'); X
     %   X1
            return;
        end
        [listRes,splits] = solveBernNewton1D(evalB,coeff,X1,listRes,splits);
    end
%if F[X] contains a zero and DF[X] does not contain a zero.
    if dfhxz
        % X_1 = first half of X
        % Solve (X1);
        splits = splits+1;
        X1(1) = X(1);
        X1(2) = (X(1) + X(2))/2;
        [listRes,splits] = solveBernNewton1D(evalB,coeff,X1,listRes,splits);
        
        % X_2 = second half of X
        % Solve (X2);
        X2(1) = (X(1) + X(2))/2;
        X2(2) = X(2);
        [listRes,splits] = solveBernNewton1D(evalB,coeff,X2,listRes,splits);
    end
    
    function res = hasZero(Y)
        if Y(1)*Y(2) <=0
            res = true;
        else
            res = false;
        end
    end

end
