function[A_out] = CreateDDimIndices(X,d) % Create d Dim Data
   % d should be >= 0 .i.e. start from 1;
   % X = Vector of (1,q);
   if d == 0
       A_out = [];
       return;
       %error('ASH:Error Occured in CreateDDimIndices');
   end
   if 1 == d
      [de,q] = size(X);       
      A_out = [1:q]';
      return
   end
   A = CreateDDimIndices(X,d-1);
   [numA,de]=size(A);
   [de,q] = size(X);
   count =1;
   for i_l =1:q
       for i_numA = 1:numA
           A_out(count,:) = [i_l,A(i_numA,:)];
           count = count+1;
%           A_out(i_l*numA,:) = [X(i_l),A(i_numA,:)]
       end
   end

end