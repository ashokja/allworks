function val = GetFValPeriodic(v1,v2,p,v_x,u_hat)
    % given a point find element.
    % assuming periodic
    while v2-1e-13<v_x(1)
        v1 =v1+ v_x(end);
        v2 =v2+ v_x(end);
    end
    while v1+1e-13> v_x(end)
        v1 =v1- v_x(end);
        v2 =v2- v_x(end);
    end
    intPts = v1+ (p+1)/2*(v2-v1); % cyclic for elments.
    indices = find(v_x < (v1+v2)/2);
    IntElid = indices(end); %el id where v1 and v2 actually lie.
    
    vv1 = v_x(IntElid);
    vv2 = v_x(IntElid+1);
    
    x = (intPts-vv1)./(vv2-vv1);
    x = x*2-1;
    val = elemeval(u_hat(:,IntElid),x);
    %val = sin(intPts);
end