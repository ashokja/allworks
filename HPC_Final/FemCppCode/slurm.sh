#!/bin/csh
#SBATCH --time=hh:mm:ss
#SBATCH -N 2
#SBATCH -n 16
#SBATCH -J myjob
#SBATCH -o my_job_output
#SBATCH --mail-type=ALL
#SBATCH --mail-user=ash.nani@gmail.com
mpirun -np 4 ./a.out >>results.txt
