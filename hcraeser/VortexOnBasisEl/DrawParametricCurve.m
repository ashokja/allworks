function [points] = DrawParametricCurve(c1,c2,c3,poly,t)
    
    switch nargin
        case 4,
            t = (0:.1:1);
    end

    [~,s_t] = size(t);
    points = zeros(s_t,3);
    points(:,1) = B_eval(poly,c1,t);
    points(:,2) = B_eval(poly,c2,t);
    points(:,3) = B_eval(poly,c3,t);
    
    
    
    function [rest] = B_eval(n1,coeff,dataIn)
       count =1;
       rest = zeros(size(dataIn));
       for k1 = 0:n1
            rest = rest  + coeff(count)*B(n1,k1,dataIn);
            count= count +1;
       end
    end
    
    
    
    function [res] = B(n,k,data)
        % assumed data varies from -1 to 1
        if n<k || k<0
            res = zeros(size(data));
            return
        end
        %data = (data+1)/2;
        res = nchoosek(n,k)*((data.^k).*((1-data).^(n-k)));
    end

end