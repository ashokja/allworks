#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include <boost/math/special_functions/sinc.hpp>
#include <boost/timer.hpp>
#include <chrono>
#include "MetricTensor.h"
#include <math.h>

#define PI 3.14159265

typedef std::chrono::high_resolution_clock Clock;


using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 6)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2st arg conditions.xml file." << endl;
		cout << "3nd arg polynomial degree filter you want to apply" << endl;
		cout << "4th arg Resolution of output." << endl;
		cout << "5th arg theta between 0-180." << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();
	HNM2D->LoadData(fname+".fld",var);

	MetricTensor* metricT = new MetricTensor();
	metricT->LoadMetricTensor(HNM2D);
	metricT->CalculateMetricTensorAtNodes();

	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;

	Array<OneD,NekDouble> fce = HNM2D->m_expansions[0]->GetPhys();
	
        // Define forcing function for first variable defined in file
	LibUtilities::EquationSharedPtr ffunc
					= vSession->GetFunction("ExactSolution", 0); 


	NekDouble minx, miny,minz,maxx,maxy,maxz;
	minx = 0.0; miny= 0.0; minz=0.0;
	maxx= 1.0; maxy=1.0; maxz=1.0;

	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();
	vector<int> elementIds, gl_elementIds;
	HNM2D->IntersectWithBoxUsingRTree( minx-0.0001, miny, minz, maxx+0.0001,maxy,maxz,
		elementIds, gl_elementIds);
	cout << "Total Elem in bounds " << elementIds.size() << endl;
// Evaluate on a new equal space grid mesh.
	int gPts = atoi(argv[1+3]);
	int totPts = gPts*gPts;
	int Nx = gPts, Ny=gPts;
	//NekDouble sx = 0.5/(Nx-1.0), sy = 1.0/(Ny-1.0);
	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0);
	cout << "Line98" << endl;	
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1, HNM2D, atoi(argv[1+2]), scaling ); 
	SmoothieSIAC2D sm_NONSYM(SIACUtilities::eSYM_UNEVEN_2kp1, HNM2D, atoi(argv[1+2]), scaling ); 
	SmoothieSIAC2D sm_NONSYM_NSK2(SIACUtilities::eSYM_UNEVEN_2kp1, HNM2D, atoi(argv[1+2]), scaling ); 
	//SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1, HNM2D, atoi(argv[1+2]), scaling ); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	Array<OneD,NekDouble> direction(3,0.0);
	Array<OneD,NekDouble> coord(3,0.0);
	NekDouble theta = atof(argv[1+4]);
	//direction[0] = cos(theta*PI/180);
	//direction[1] = sin(theta*PI/180);
	direction[0] = 1.0;
	direction[1] = 0.0; 
	Array<OneD,NekDouble> pX(totPts), pY(totPts), pV(totPts), pP(totPts), pS(totPts),pDyn(totPts),pE(totPts);
	Array<OneD,NekDouble> pSDynX(totPts), pSDynY(totPts);
	Array<OneD,NekDouble> pTDyn(totPts), pT(totPts);
	Array<OneD,NekDouble> pSMet(totPts),pSMet2(totPts);
	Array<OneD,NekDouble> glCoords(3,0.0),eig(3,0.0),eig2(3,0.0);
	NekDouble lambda,lambda2;
/*
	for( int i =0; i < Nx; i++)
	{
		for(int j=0; j< Ny; j++)
		{
			int index = i*Nx+j;
			//pX[index] = i*sx+0.25;
			pX[index] = i*sx;
			pY[index] = j*sy;
			pV[index] = ffunc->Evaluate(pX[index],pY[index],0.0,0.0);
			glCoords[0] = pX[index]; glCoords[1] = pY[index];
			
			// L-SIAC Dynamic Scaling Direction 1.
			boost::timer tim1;
			bool TDynRes, TRes;
			auto t1 = Clock::now();
				NekDouble dynScaling = HNM2D->GetDynamicScaling(glCoords);
				pDyn[index] = dynScaling;
				TDynRes = sm.EvaluateAt(pX[index],pY[index],0.0,pSDynX[index],valY,valZ,directionX, dynScaling ,0);
				TDynRes = sm.EvaluateAt(pX[index],pY[index],0.0,pSDynY[index],valY,valZ,directionY, dynScaling ,0);
			auto t2 = Clock::now();
		    std::chrono::duration<double> elapsed_seconds = t2-t1;
			pTDyn[index] = elapsed_seconds.count();

			// L-SIAC Maximum Edge Scaling 
			auto tt1 = Clock::now();
				TRes = sm.EvaluateAt(pX[index],pY[index],0.0,pS[index],valY,valZ,directionY, scaling,0);
			auto tt2 = Clock::now();
		    std::chrono::duration<double> elapsed_secondst = tt2-tt1;
			pT[index] = elapsed_secondst.count();
		
			// dG Error	
			coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = 0.0;
			int elid = HNM2D->GetExpansionIndexUsingRTree(coord);
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
			int physOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
			const Array<OneD,NekDouble> el_phys = fce.CreateWithOffset(
										fce,physOffset);
			pP[index] = lexp->PhysEvaluate(coord,el_phys);
			pE[index] = elid;


			// L-SIAC Using Eig 1, Eig 2
			metricT->GetEigenPair(glCoords,elid,1,lambda,eig);
			sm.EvaluateAt(pX[index],pY[index],0.0,pSMet[index], valY, valZ, eig, lambda,0);
			metricT->GetEigenPair(glCoords,elid,2,lambda2,eig2);
			sm.EvaluateAt(pX[index],pY[index],0.0,pSMet2[index], valY, valZ, eig2, lambda2,0);
//			cout << j << endl;

		}
		cout << i << endl;
	}
	NektarBaseClass k;
	k.writeNekArray(pX,fname+"_"+argv[1+2]+"_METFLD_"+argv[1+3]+"_pX_2DDyn.txt");
	k.writeNekArray(pY,fname+"_"+argv[1+2]+"_METFLD_"+argv[1+3]+"_pY_2DDyn.txt");
	k.writeNekArray(pE,fname+"_"+argv[1+2]+"_METFLD_"+argv[1+3]+"_pE_2DDyn.txt");
	k.writeNekArray(pV,fname+"_"+argv[1+2]+"_METFLD_"+argv[1+3]+"_pV_2DDyn.txt");
	k.writeNekArray(pP,fname+"_"+argv[1+2]+"_METFLD_"+argv[1+3]+"_pP_2DDyn.txt");
	k.writeNekArray(pS,fname+"_"+argv[1+2]+"_METFLD_"+argv[1+3]+"_pS_2DDyn.txt");
	k.writeNekArray(pSDynX,fname+"_"+argv[1+2]+"_METFLD_"+argv[1+3]+"_pSDynX_2DDyn.txt");
	k.writeNekArray(pSDynY,fname+"_"+argv[1+2]+"_METFLD_"+argv[1+3]+"_pSDynY_2DDyn.txt");
	k.writeNekArray(pDyn,fname+"_"+argv[1+2]+"_METFLD_"+argv[1+3]+"_pDyn_2DDyn.txt");
	k.writeNekArray(pTDyn,fname+"_"+argv[1+2]+"_METFLD_"+argv[1+3]+"_pTDyn_2DDyn.txt");
	k.writeNekArray(pT,fname+"_"+argv[1+2]+"_METFLD_"+argv[1+3]+"_pT_2DDyn.txt");
	k.writeNekArray(pSMet,fname+"_"+argv[1+2]+"_METFLD_"+argv[1+3]+"_pSMet_2DDyn.txt");
	k.writeNekArray(pSMet2,fname+"_"+argv[1+2]+"_METFLD_"+argv[1+3]+"_pSMet2_2DDyn.txt");
*/
	cout << "Done writing plane" << endl;
	//plane completed.
	// Evaluating on line y =0;
	//int gPtsLy = atoi(argv[1+3])*10;
	int gPtsLy = atoi(argv[1+3]);
	int totPtsLy = gPtsLy;
	sy = 1.0/(gPtsLy-1.0);
	Array<OneD,NekDouble> pLy(totPtsLy),pLNx(totPtsLy), pLPx(totPtsLy), pELy(totPtsLy);
	Array<OneD,NekDouble> pXLy(totPtsLy), pYLy(totPtsLy), pVLy(totPtsLy), pPLy(totPtsLy), pSLy(totPtsLy),pDynLy(totPtsLy);
	//Array<OneD,NekDouble> pSDynLyX(totPtsLy),pSDynLyY(totPtsLy);
	Array<OneD,NekDouble> pSDynLy(totPtsLy);
	Array<OneD,NekDouble> pTDynLy(totPtsLy), pTLy(totPtsLy);
	Array<OneD,NekDouble> pSMetLy(totPtsLy);
	Array<OneD,NekDouble> pSMetEig1Ly(totPtsLy), pSMetEig2Ly(totPtsLy);
	Array<OneD,NekDouble> pSnskEig1Ly(totPtsLy);
	Array<OneD,NekDouble> pSnskEig2Ly(totPtsLy);
	Array<OneD,NekDouble> pSnskEig3Ly(totPtsLy);
	Array<OneD,NekDouble> pSnskEig4Ly(totPtsLy);
	Array<OneD,NekDouble> pSMetEigIP1Ly(totPtsLy), pSMetEigIP2Ly(totPtsLy);
	Array<OneD,NekDouble> pSnskEigIP1Ly(totPtsLy);
	Array<OneD,NekDouble> pSnskEigIP2Ly(totPtsLy);
	Array<OneD,NekDouble> pSnskEigIP3Ly(totPtsLy);
	Array<OneD,NekDouble> pSnskEigIP4Ly(totPtsLy);
	for (int i =0 ; i< gPtsLy; i++)
	{
		int index = i;
		pYLy[index] = 0.5;
		pXLy[index] = i*sy; 
		pVLy[index] = ffunc->Evaluate(pXLy[index],pYLy[index],0.0,0.0);
		glCoords[0] = pXLy[index]; glCoords[1] = pYLy[index];

		//L-SIAC dynamic Scaling
		boost::timer tim1;
		NekDouble dynScaling = HNM2D->GetDynamicScaling(glCoords);
		pDynLy[index] = dynScaling;
		sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSDynLy[index],valY,valZ,direction, dynScaling ,0);
		pTDynLy[index] = tim1.elapsed();

			//L-SIAC maximum Edge length Scaling
		//boost::timer tim2;
		//sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSLy[index],valY,valZ,directionY, scaling ,0);
		//pTLy[index] = tim2.elapsed();

		//dG Error 
		coord[0] = pXLy[index]; coord[1] = pYLy[index]; coord[2] = 0.0;
		int elid = HNM2D->GetExpansionIndexUsingRTree(coord);
		LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
		int physOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
		const Array<OneD,NekDouble> el_phys = fce.CreateWithOffset(
										fce,physOffset);
		pPLy[index] = lexp->PhysEvaluate(coord,el_phys);
		pELy[index] = elid;

		// L-SIAC Metric Error
		//	metricT->GetEigenPair(glCoords,elid,1,lambda,eig);
		//	sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMetLy[index], valY, valZ, eig, lambda,0);
		//	metricT->GetEigenPair(glCoords,elid,2,lambda2,eig2);
		//	sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMet2Ly[index], valY, valZ, eig2, lambda2,0);

		metricT->GetEigenPairAtTheta(glCoords,elid,theta,lambda,eig);
		sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMetLy[index], valY, valZ, eig, lambda,0);


		metricT->GetEigenPair(glCoords,elid,1,lambda,eig);
		sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMetEig1Ly[index], valY, valZ, eig, lambda,0);
		metricT->GetEigenPair(glCoords,elid,2,lambda2,eig2);
		sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMetEig2Ly[index], valY, valZ, eig2, lambda2,0);
		
		sm_NONSYM.EvaluateAt_NSK_FixedNumBSpl(pXLy[index],pYLy[index],0.0,pSnskEig1Ly[index],
									 valY, valZ, eig, lambda,0);
		sm_NONSYM.EvaluateAt_NSK_FixedNumBSpl(pXLy[index],pYLy[index],0.0,pSnskEig2Ly[index],
									 valY, valZ, eig2, lambda2,0);
		sm_NONSYM_NSK2.EvaluateAt_NSK_GivenFilterLength_v1(pXLy[index],pYLy[index],0.0,pSnskEig3Ly[index],
									 valY, valZ, eig, lambda,0);
		sm_NONSYM_NSK2.EvaluateAt_NSK_GivenFilterLength_v1(pXLy[index],pYLy[index],0.0,pSnskEig4Ly[index],
									 valY, valZ, eig2, lambda2,0);
		
		metricT->GetEigenPairUsingIP(glCoords,elid,1,lambda,eig);
		sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMetEigIP1Ly[index], valY, valZ, eig, lambda,0);
		metricT->GetEigenPairUsingIP(glCoords,elid,2,lambda2,eig2);
		sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMetEigIP2Ly[index], valY, valZ, eig2, lambda2,0);
		sm_NONSYM.EvaluateAt_NSK_FixedNumBSpl(pXLy[index],pYLy[index],0.0,pSnskEigIP1Ly[index],
									 valY, valZ, eig, lambda,0);
		sm_NONSYM.EvaluateAt_NSK_FixedNumBSpl(pXLy[index],pYLy[index],0.0,pSnskEigIP2Ly[index],
									 valY, valZ, eig2, lambda2,0);
		sm_NONSYM_NSK2.EvaluateAt_NSK_GivenFilterLength_v1(pXLy[index],pYLy[index],0.0,pSnskEigIP3Ly[index],
									 valY, valZ, eig, lambda,0);
		sm_NONSYM_NSK2.EvaluateAt_NSK_GivenFilterLength_v1(pXLy[index],pYLy[index],0.0,pSnskEigIP4Ly[index],
									 valY, valZ, eig2, lambda2,0);
		
	}

	cout << "Done writing Line" << endl;
	NektarBaseClass k;
	k.writeNekArray(pXLy,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pXLy_2DDyn.txt");
	k.writeNekArray(pYLy,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pYLy_2DDyn.txt");
	k.writeNekArray(pELy,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pELy_2DDyn.txt");
	k.writeNekArray(pVLy,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pVLy_2DDyn.txt");
	k.writeNekArray(pPLy,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pPLy_2DDyn.txt");
	k.writeNekArray(pSLy,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pSLy_2DDyn.txt");
	k.writeNekArray(pSDynLy,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pSDynLy_2DDyn.txt");
	k.writeNekArray(pDynLy,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pDynLy_2DDyn.txt");
	k.writeNekArray(pTDynLy,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pTDynLy_2DDyn.txt");
	k.writeNekArray(pTLy,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pTLy_2DDyn.txt");
	k.writeNekArray(pSMetLy,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pSMetLy_2DDyn.txt");
	k.writeNekArray(pSMetEig1Ly,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pSMetEig1Ly_2DDyn.txt");
	k.writeNekArray(pSMetEig2Ly,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pSMetEig2Ly_2DDyn.txt");
	k.writeNekArray(pSnskEig1Ly,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pSnskEig1Ly_2DDyn.txt");
	k.writeNekArray(pSnskEig2Ly,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pSnskEig2Ly_2DDyn.txt");
	k.writeNekArray(pSnskEig3Ly,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pSnskEig3Ly_2DDyn.txt");
	k.writeNekArray(pSnskEig4Ly,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pSnskEig4Ly_2DDyn.txt");
	k.writeNekArray(pSMetEigIP1Ly,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pSMetEigIP1Ly_2DDyn.txt");
	k.writeNekArray(pSMetEigIP2Ly,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pSMetEigIP2Ly_2DDyn.txt");
	k.writeNekArray(pSnskEigIP1Ly,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pSnskEigIP1Ly_2DDyn.txt");
	k.writeNekArray(pSnskEigIP2Ly,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pSnskEigIP2Ly_2DDyn.txt");
	k.writeNekArray(pSnskEigIP3Ly,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pSnskEigIP3Ly_2DDyn.txt");
	k.writeNekArray(pSnskEigIP4Ly,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R4_"+argv[1+3]+"_pSnskEigIP4Ly_2DDyn.txt");
// Calculate using Non-Symmetric Filter.
	vector<NekDouble> t_params;
	for (int i =0 ; i< gPtsLy; i++)
	{
		t_params.push_back(i*sy);
	}
	vector<NekDouble> stPoint(3,0.0),pSnskLy(totPtsLy);
	stPoint[1]=0.5;
	sm_NONSYM.EvaluateUsingLineAt_vNonSymKnots( stPoint, direction, 4, 1.0, t_params ,pSnskLy,0);
	k.writeNekArray(pSnskLy,fname+"_"+argv[1+2]+"_METFLD_T_"+argv[1+4]+"_R_"+argv[1+3]+"_pSnskLy_2DDyn.txt");

	// Evaluate on line x =0.5; x = -0.5;

	return 0;
}

