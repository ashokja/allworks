clear;
close all;

%alpha =1;

filename = 'P0000035';


xfname =  strcat('../datatxt/',filename,'_pX.txt' );
yfname =  strcat('../datatxt/',filename,'_pY.txt' );
zfname =  strcat('../datatxt/',filename,'_pZ.txt' );
efname =  strcat('../datatxt/',filename,'_pE.txt' );
upfname = strcat('../datatxt/',filename,'_pU_p.txt' );
vpfname = strcat('../datatxt/',filename,'_pV_p.txt' );
wpfname = strcat('../datatxt/',filename,'_pW_P.txt' );
ppfname = strcat('../datatxt/',filename,'_pP_p.txt' );

xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');

uppos = textread(upfname, '%f\t');
vppos = textread(vpfname, '%f\t');
wppos = textread(wpfname, '%f\t');
pppos = textread(ppfname, '%f\t');

epos = floor(epos +0.0001);

s = floor( (max(size(xpos)))^(1/3) +0.1);

    
%xVpos = reshape(xpos,s,s,s);
%yVpos = reshape(ypos,s,s,s);
%zVpos = reshape(zpos,s,s,s);
%eVpos = reshape(epos,s,s,s);

    xVpos = reshape(xpos,s,s,s);
    xVpos = permute(xVpos,[2,3,1]);
    yVpos = reshape(ypos,s,s,s);
    yVpos = permute(yVpos,[2,3,1]);
    zVpos = reshape(zpos,s,s,s);
    zVpos = permute(zVpos,[2,3,1]);

upVpos = reshape(uppos,s,s,s);
vpVpos = reshape(vppos,s,s,s);
wpVpos = reshape(wppos,s,s,s);
ppVpos = reshape(pppos,s,s,s);

upVpos = permute(upVpos,[2,3,1]);
vpVpos = permute(vpVpos,[2,3,1]);
wpVpos = permute(wpVpos,[2,3,1]);
ppVpos = permute(ppVpos,[2,3,1]);

%%
figure,
hsurfaces = slice(xVpos,yVpos,zVpos,ppVpos,[2.5,2.8333,3.0],0.25,0.6);
set(hsurfaces,'FaceColor','interp','EdgeColor','none')
xlabel('x'); ylabel('y'); zlabel('z');
colormap jet; colorbar; hold off; caxis([-1,1]);
title('FUNC(ACT'); hold on;

%x = linspace(2.5,3,10);
%[X,Y,Z] = meshgrid(x);
% 
% sx = linspace(2.5, 2.7,3);
% sy = linspace(min(yVpos(:)), max(yVpos(:)),10);
% sz = linspace(min(zVpos(:)), max(zVpos(:)),10);
% [startx,starty,startz] = meshgrid(sx,sy,sz);
% xlabel('x');ylabel('y'); zlabel('z');
% 
% streamline(xVpos,yVpos,zVpos,upVpos,vpVpos,wpVpos,startx,starty,startz); 

% %hold on;
%  figure(3)
% % fv = isosurface(xVpos,yVpos,zVpos,ppVpos,0.0);
% % isonormals(xVpos,yVpos,zVpos,ppVpos,fv)
% % fv.FaceColor = 'red';
% % fv.EdgeColor = 'none';
% % daspect([1,1,1])
% % view(3); axis tight
% % camlight 
% % lighting gouraud
% 
% 
% %[x,y,z,v] = flow;
% p = patch(isosurface(xVpos,yVpos,zVpos,ppVpos,-0.6));
% isonormals(xVpos,yVpos,zVpos,ppVpos,p)
% p.FaceColor = 'red';
% p.EdgeColor = 'none';
% daspect([1,1,1])
% view(3); axis tight
% camlight 
% lighting gouraud
