clear all;
close all;

t = linspace(-7, 7, 201);

S0 = SIAC(t, 1, 0);
S1 = SIAC(t, 1, 1);
S2 = SIAC(t, 1, 2);
S3 = SIAC(t, 1, 3);

figure(1),
plot( t, S1, t, S2, t, S3, 'LineWidth', 2)
legend('first order', 'second order', 'third order')

figure(2),
k = -2:.01:2;
plot(k,B1(k),k,B2(k),k,B3(k));
legend('B1', 'B2', 'B3')

figure(3)
plot( t, S1,  'LineWidth', 2)
legend('first order')
% 
% figure(4)
% plot( t, S0,  'LineWidth', 2)
% legend('Zeroth order')
% 
% figure(5)
% plot( t, S2,  'LineWidth', 2)
% legend('second order')

%knot positions
%figure(3),
