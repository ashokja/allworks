% Read data from testNaca_DUPuse1dir3DSIAC
% s is used in variables but it ss data.

clear;
close all;


UsebkupData = 'cur_feb20/'; % has only data Ux_ss so remember to comment below also.

%UsebkupData = 'cur_feb10/'

%UsebkupData = ''
%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05';
%filename = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_DUPuse1dir3DSIAC' );
filename = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_DUP2use1dir3DSIAC' );
filepath = strcat('data/',UsebkupData,filename);

xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );

ussfname = strcat(filepath,'_U_ss.txt' ); % raw data projection.
uxssfname = strcat(filepath,'_pUx_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
uyssfname = strcat(filepath,'_pUy_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
uzssfname = strcat(filepath,'_pUz_ss.txt' ); % this variable is u with LSIAC applied in X-direction.

vssfname = strcat(filepath,'_V_ss.txt' ); % raw data projection.
vxssfname = strcat(filepath,'_pVx_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
vyssfname = strcat(filepath,'_pVy_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
vzssfname = strcat(filepath,'_pVz_ss.txt' ); % this variable is u with LSIAC applied in X-direction.


wssfname = strcat(filepath,'_W_ss.txt' ); % raw data projection.
wxssfname = strcat(filepath,'_pWx_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
wyssfname = strcat(filepath,'_pWy_ss.txt' ); % this variable is u with LSIAC applied in X-direction.
wzssfname = strcat(filepath,'_pWz_ss.txt' ); % this variable is u with LSIAC applied in X-direction.


xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');

usspos = textread(ussfname, '%f\t');
uxsspos = textread(uxssfname, '%f\t');
uysspos = textread(uyssfname, '%f\t');
uzsspos = textread(uzssfname, '%f\t');

vsspos = textread(vssfname, '%f\t');
vxsspos = textread(vxssfname, '%f\t');
vysspos = textread(vyssfname, '%f\t');
vzsspos = textread(vzssfname, '%f\t');

wsspos = textread(wssfname, '%f\t');
wxsspos = textread(wxssfname, '%f\t');
wysspos = textread(wyssfname, '%f\t');
wzsspos = textread(wzssfname, '%f\t');


epos = floor(epos +0.0001);
s = floor( (max(size(xpos)))^(1/2) +0.1);

xVpos = reshape(xpos,s,s);
yVpos = reshape(ypos,s,s);
zVpos = reshape(zpos,s,s);

upVpos = reshape(usspos,s,s);  
usxVpos = reshape(uxsspos,s,s);  
usyVpos = reshape(uysspos,s,s);  
uszVpos = reshape(uzsspos,s,s);  

vpVpos = reshape(vsspos,s,s);  
vsxVpos = reshape(vxsspos,s,s);  
vsyVpos = reshape(vysspos,s,s);  
vszVpos = reshape(vzsspos,s,s);  

wpVpos = reshape(wsspos,s,s);  
wsxVpos = reshape(wxsspos,s,s);  
wsyVpos = reshape(wysspos,s,s);  
wszVpos = reshape(wzsspos,s,s);  


% Debug%

figure(1),
subplot(2,2,1), surf(yVpos,zVpos,usxVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir xLSIAC');
subplot(2,2,2), surf(yVpos,zVpos,upVpos); hold on; colorbar(); shading interp; view(0,90);
title('raw data');
subplot(2,2,3), surf(yVpos,zVpos,usyVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir yLSIAC');
subplot(2,2,4), surf(yVpos,zVpos,uszVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir zLSIAC');


figure(2),
subplot(2,2,1), surf(yVpos,zVpos,vsxVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir xLSIAC');
subplot(2,2,2), surf(yVpos,zVpos,vpVpos); hold on; colorbar(); shading interp; view(0,90);
title('raw data');
subplot(2,2,3), surf(yVpos,zVpos,vsyVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir yLSIAC');
subplot(2,2,4), surf(yVpos,zVpos,vszVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir zLSIAC');

figure(3),
subplot(2,2,1), surf(yVpos,zVpos,wsxVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir xLSIAC');
subplot(2,2,2), surf(yVpos,zVpos,wpVpos); hold on; colorbar(); shading interp; view(0,90);
title('raw data');
subplot(2,2,3), surf(yVpos,zVpos,wsyVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir yLSIAC');
subplot(2,2,4), surf(yVpos,zVpos,wszVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir zLSIAC');


figure(4),
subplot(2,2,1), contour(yVpos,zVpos,usxVpos);
title('1dir xLSIAC');
subplot(2,2,2), contour(yVpos,zVpos,upVpos); 
title('raw data');
subplot(2,2,3), contour(yVpos,zVpos,usyVpos); 
title('1dir yLSIAC');
subplot(2,2,4), contour(yVpos,zVpos,uszVpos); 
title('1dir zLSIAC');


figure(5),
subplot(2,2,1), contour(yVpos,zVpos,vsxVpos); 
title('1dir xLSIAC');
subplot(2,2,2), contour(yVpos,zVpos,vpVpos); 
title('raw data');
subplot(2,2,3), contour(yVpos,zVpos,vsyVpos); 
title('1dir yLSIAC');
subplot(2,2,4), contour(yVpos,zVpos,vszVpos); 
title('1dir zLSIAC');

figure(6),
subplot(2,2,1), contour(yVpos,zVpos,wsxVpos); 
title('1dir xLSIAC');
subplot(2,2,2), contour(yVpos,zVpos,wpVpos); 
title('raw data');
subplot(2,2,3), contour(yVpos,zVpos,wsyVpos);
title('1dir yLSIAC');
subplot(2,2,4), contour(yVpos,zVpos,wszVpos);
title('1dir zLSIAC');



% calculate acceleration.
auspos = uxsspos.*usspos + uysspos.*vsspos + uzsspos.*wsspos;
avspos = vxsspos.*usspos + vysspos.*vsspos + vzsspos.*wsspos;
awspos = wxsspos.*usspos + wysspos.*vsspos + wzsspos.*wsspos;
 %
% veclocity cross acceleration. 
cuspos = vsspos.*awspos - wsspos.*avspos;
cvspos = wsspos.*auspos - usspos.*awspos;
cwspos = usspos.*avspos - vsspos.*auspos;

curvature_pos = (cuspos.^(2) + cvspos.^(2) + cwspos.^(2)).^(1/2);
curvature_Vpos = reshape(curvature_pos,s,s);

figure(7)
surf(yVpos,zVpos,curvature_Vpos);
shading interp;
%%
figure(8)
contour3(yVpos,zVpos,curvature_Vpos,20,'LineColor','k'); hold on;
surf(yVpos,zVpos,curvature_Vpos), shading interp;
%grid off;
%axis off;
colorbar();
view(0,90);
%print('images/F3b13/Curvature_2dir_of_der','-dpng', '-r500')


