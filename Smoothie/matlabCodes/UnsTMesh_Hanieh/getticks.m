function res = getticks(eVpos,pXpos)
    
    %loop through eVpos and find when changes
    count = 0;
    var = eVpos(1);
    res = zeros(1);
    for i = 1:max(size(eVpos))
%        disp(i);
        if (abs( var- eVpos(i)) >0.5)
            count = count+1;
            res(count) = pXpos(i);            
            var = eVpos(i);
        end
    end
    
end