% Might be create better test Data.

%test Vortex Projection
clear;
n = 9;
addpath ../VortexOnBasisEl;
[xy,distance,t_a,mapxy,mapxyV,curveCoeff] = test_runNodes(1,n+4,3,1);

[coeff] = buildCoeff(n,n,n,mapxy,mapxyV);
dataV = buildData(n,n,n,coeff,mapxy);
% normal parallezation starts.
%%
x = (-1:0.1:1);
y = (-1:0.1:1);
z = (-1:0.1:1);

[X,Y,Z] = meshgrid(x,y,z);
dataIn(:,1) = reshape(X,[],1);
dataIn(:,2) = reshape(Y,[],1);
dataIn(:,3) = reshape(Z,[],1);
%%
dataOut = buildData(n,n,n,coeff,dataIn);
U = reshape(dataOut(:,1),size(X));
V = reshape(dataOut(:,2),size(Y));
W = reshape(dataOut(:,3),size(Z));

%%
figure(114)
addpath ../vortex_learning/    
vorLinSeg = FindVorticityInTetMesh(X,Y,Z,U,V,W); hold on;
plotBoundingBox();
drawOnlyCurve(curveCoeff);

