% vizBullzeye

function [] = VizBullzEye(xcheb,ycheb,zcheb,UAtCheb,VAtCheb,WAtCheb,coeff,sampledata)
    switch nargin
        case 7
            sampledata =6;
            curveSampledata = 20;
    end
        data = linspace(-1,1,sampledata);
            cdata = linspace(-1,1,curveSampledata);
    [X,Y,Z] = meshgrid(xcheb,ycheb,zcheb);
    normAtCheb = sqrt(UAtCheb.^2 + VAtCheb.^2+ WAtCheb.^2);
    normAtCheb = permute(normAtCheb,[2,1,3]);
    
    
    xdata = B_eval(coeff(1,:),data);    cxdata = B_eval(coeff(1,:),cdata); 
    ydata = B_eval(coeff(2,:),data);    cydata = B_eval(coeff(2,:),cdata);
    zdata = B_eval(coeff(3,:),data);    czdata = B_eval(coeff(3,:),cdata);
    xderdata = B_der_eval(coeff(1,:),data);
    yderdata = B_der_eval(coeff(2,:),data);
    zderdata = B_der_eval(coeff(3,:),data);

    
    
    for i = 1:sampledata
        point = [xdata(i),ydata(i), zdata(i)];
        nNormal = [xderdata(i),yderdata(i), zderdata(i)];
        pNormal= [0,0,1];
        nNormal = nNormal/norm(nNormal);
        theta = acos(dot(nNormal,pNormal)/(norm(pNormal)*norm(nNormal)) );
        axis = cross(nNormal,pNormal);
        axis = axis/norm(axis);
        
        hsp = surf(point(1)+linspace(-0.25,0.25,20),point(2)+linspace(-0.25,0.25,20),...
            point(3)+zeros(20) ); hold on;
        
        rotate(hsp,axis,-theta/pi*180,point);
        %rv = cross(v,[v(1),v(2),0])
        %rotate(hsp,rv,90-PHI*180/pi,[xls,yls,zls]);
        set(hsp,'FaceColor','interp','EdgeColor','none');
        xd = hsp.XData;
        yd = hsp.YData;
        zd = hsp.ZData;
        delete(hsp);
        hsurf = slice(X,Y,Z,normAtCheb,xd,yd,zd); hold on;
        set(hsurf,'FaceColor','interp','EdgeColor','none');

    end    

end

%     

