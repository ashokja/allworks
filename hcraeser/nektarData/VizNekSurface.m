function [] = VizNekSurface(X,Y,Z,U,V,W,sx,sy,sz)
    switch nargin
        case 6
            sx = [-1];
            sy = [-1,0,1];
            sz = [-1];
    end
 %   normAtCheb = permute(normAtCheb,[2,1,3]);
    X = permute(X,[2,1,3]); U = permute(U,[2,1,3]);
    Y = permute(Y,[2,1,3]); V = permute(V,[2,1,3]);
    Z = permute(Z,[2,1,3]); W = permute(W,[2,1,3]);

    %    [X,Y,Z] = meshgrid(xcheb,ycheb,zcheb);
    normAtCheb = sqrt(U.^2 + V.^2+ W.^2);

    if (min(normAtCheb(:)) == max(normAtCheb(:)))
        v = 'All value same'
        return
    end
    hsurfaces = slice(X,Y,Z,normAtCheb,sx,sy,sz);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none');
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar;
end