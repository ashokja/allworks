function [er] = testLearnPolyLib(N,n)
    addpath ../PolyLib
    %n = 7;
 %% Experiment 3
    [x,w] = JacobiGZW(N+n,0,0);
    Mass = zeros(N,N);
    for i= 0:N
        for j = 0:N
            if( i == j)
                Mass(i+1,j+1) = 2/(2*i+1);
            end
        end
    end
    rhs = zeros(1,N+1);
    for i = 0:N
        rhs(i+1) = dot(w,func(x).*LegendrePoly(i,x));
    end
    
    coeff = rhs/Mass;
    for i = 0:N
        coeffa(i+1) = (2*i+1)/2*dot(w,func(x).*LegendrePoly(i,x)); 
    end
    
    %validataon
    xnew = linspace(-1,1,50);
    sol = zeros(size(xnew));
    for i = 0:N
        sol = sol + coeffa(i+1)*LegendrePoly(i,xnew);
    end
    er = sum(abs(sol-func(xnew)));
    figure(1)
    plot(x,func(x),'*r',xnew,sol,'g'); hold on;
    
    
    function [res] = func(x)
        res = x.*sin(3*pi*x);
    end
    
%% Experiment 2
% 
%  % check if legendrePoly nomials are orthogonal polynomails.
%     %w,x,N
%     [x,w] = JacobiGZW(2*N+1,0,0);
%     for i =1:N
%         Phi(i,:) = LegendrePoly(i,x);
%     end
%     for i = 1:N
%         for j = 1:N
%            ortho(i,j) = dot(Phi(i,:).*Phi(j,:),w);
%            if( i == j)
%             cOrtho(i,j) = 2/(2*i+1);
%            end
%         end
%     end
%     res = trace(abs(((ortho)-cOrtho)));
%     sum(abs(res(:)))
    
    
    
    
%% Experiment 1    
%     for n = 1:N
%         [x,w] = JacobiGLZW(n,0,0);
%         res = dot(w,func(x,N));
%         er(n) =  res;
%     end
%     figure(1)
%     plot((1:N),er); hold on;
%     function [res] = func(x,N)
%         res = x.*sin(pi*x);
%     end
% %
end