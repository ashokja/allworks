class SurfaceDataCurves  //<>//
{
  int ku;
  ArrayList<Float> Knotu;
  int Tu;
  int numCptsu;
  int numCurves;
  ArrayList<PVector> CptsAu;

  SurfaceDataCurves(int tku, ArrayList<Float> tKnotu, int tnumCptsu, ArrayList<PVector> tCptsAu, int tnumCurves)
  {
     ku = tku;
     Knotu = tKnotu;
     Tu = Knotu.size();
     numCptsu = tnumCptsu;
     CptsAu = tCptsAu; 
     numCurves = tnumCurves;
  }
  
  
  void printData()
  {
    println(ku);
    println(Knotu);
    println(Tu);
    println(numCptsu);
    println(numCurves);
    println(CptsAu);
  }
  
}
