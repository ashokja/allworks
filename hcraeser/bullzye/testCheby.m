% test2D cheby D function using x points.
function [] = testCheby(N,n)
% we can do test for 1D first
  addpath ../PolyLib
  switch nargin
      case 0,     
          N = 5; n=1;
  end
%% Experiment 3
    [x,w] = JacobiGLZW(N+n,0,0);
    %Calculate Coeff
    for i = 0:N
        coeffa(i+1) = (2*i+1)/2*dot(w,func(x).*LegendrePoly(i,x)); 
    end
    
    
    %validataon
    % calcuate derivate at cheb points.
    [DChebX,xcheb] = cheb(N+n);
    xnew = linspace(-1,1,50);
    sol = zeros(size(xcheb));
    for i = 0:N
        sol = sol + coeffa(i+1)*LegendrePoly(i,xcheb);
    end
    er = sum(abs(sol-func(xcheb)))
    
    
    figure(1)
    plot(xnew,func(xnew),'g',xcheb,sol,'r*'); hold off;
    
    
    solCheb = zeros(size(xcheb));
    for i = 0:N
        solCheb = solCheb + coeffa(i+1)*LegendrePoly(i,xcheb);
    end
    
    solCheb_D = DChebX*solCheb;
    
    figure(2)
    plot(xnew,func_D(xnew),'g',xcheb,solCheb_D,'*r'); hold off;
    title('derivative')
    
    figure(3)
    plot(xnew,func_DD(xnew),'g',xcheb,DChebX*solCheb_D,'*r'); hold off;
    title('derivative')
    
    function [res] = func(x)
        %res = x.*sin(3*pi*x);
        res = sin(pi*x);
    end

    function [res] = func_D(x)
        %res = sin(3*pi*x) + 3*pi*(x.*cos(3*pi*x));
        res = pi*cos(pi*x);
    end

    function [res] = func_DD(x)
        %res = 3*pi*cos(3*pi*x) + 3*pi*(cos(3*pi*x)- 3*pi*x.*sin(3*pi*x));
        res = -pi*pi*sin(pi*x);
    end
end