%cpp file corres is testCyl_Vort_Line

clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [8001];

poly =2;
order = 2;
alpha =1;
count =1;

fileName = 'Cy3l_Re500_';

%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize
atX = '3p7';


for r = rates
    filenameFrPrint = strcat( fileName, int2str(order),'_',int2str(r),'_X_',atX);
    
    efname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pE_2D_OS2kp1_Line_',int2str(r),'.txt' )  
    xfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pX_2D_OS2kp1_Line_',int2str(r),'.txt' );
    yfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pY_2D_OS2kp1_Line_',int2str(r),'.txt' );
    
    pUfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pU_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pVfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pV_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pPfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pP_2D_OS2kp1_Line_',int2str(r),'.txt' );
    
    pUxfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUx_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pVxfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pPxfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPx_2D_OS2kp1_Line_',int2str(r),'.txt' );
    
    pUyfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pVyfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVy_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pPyfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPy_2D_OS2kp1_Line_',int2str(r),'.txt' );
    
    pU_pfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pU_p_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pV_pfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pV_p_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pP_pfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pP_p_2D_OS2kp1_Line_',int2str(r),'.txt' );
    
    pUx_pfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUx_p_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pVx_pfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx_p_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pPx_pfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPx_p_2D_OS2kp1_Line_',int2str(r),'.txt' );
    
    pUy_pfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy_p_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pVy_pfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVy_p_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pPy_pfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPy_p_2D_OS2kp1_Line_',int2str(r),'.txt' );

    pAu_pfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pAu_p_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pAv_pfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pAv_p_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pC_pfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pC_p_2D_OS2kp1_Line_',int2str(r),'.txt' );

    pAu_fname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pAu_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pAv_fname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pAv_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pC_fname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pC_2D_OS2kp1_Line_',int2str(r),'.txt' );
    
    pI_pfname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pI_p_2D_OS2kp1_Line_',int2str(r),'.txt' );
    pI_fname = strcat('../datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pI_2D_OS2kp1_Line_',int2str(r),'.txt' );
    
    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');
    elids = floor(textread(efname, '%f\t') +0.0001);
    pUval = textread(pUfname, '%f\t');  pUxval = textread(pUxfname, '%f\t');    pUyval = textread(pUyfname, '%f\t');
    pVval = textread(pVfname, '%f\t');  pVxval = textread(pVxfname, '%f\t');    pVyval = textread(pVyfname, '%f\t');
    pPval = textread(pPfname, '%f\t');  pPxval = textread(pPxfname, '%f\t');    pPyval = textread(pPyfname, '%f\t');

    pU_pval = textread(pU_pfname, '%f\t');  pUx_pval = textread(pUx_pfname, '%f\t');    pUy_pval = textread(pUy_pfname, '%f\t');
    pV_pval = textread(pV_pfname, '%f\t');  pVx_pval = textread(pVx_pfname, '%f\t');    pVy_pval = textread(pVy_pfname, '%f\t');
    pP_pval = textread(pP_pfname, '%f\t');  pPx_pval = textread(pPx_pfname, '%f\t');    pPy_pval = textread(pPy_pfname, '%f\t');

    pAu_pval = textread(pAu_pfname, '%f\t');   pAu_val = textread(pAu_fname, '%f\t');
    pAv_pval = textread(pAv_pfname, '%f\t');   pAv_val = textread(pAv_fname, '%f\t');
    pC_pval = textread(pC_pfname, '%f\t');     pC_val = textread(pC_fname, '%f\t');
    
    pI_pval = -1.0*textread(pI_pfname, '%f\t');     pI_val = -1.0*textread(pI_fname, '%f\t');
    
    %%
    figure(1),
    plot(ypos,pI_pval); hold on;
    plot(ypos,pI_val);
    
    
    figure(2),
    plot(ypos,elids,'*'); hold on;
    
    [unique_ids,id_offset,id_tags] = unique(elids);
    
    
    graphHtMax = max(pI_pval)+0.01;
    graphHtMmin = min(pI_pval)-0.01;
    count =1;
     for tag = unique_ids'
        el_indices = find(elids== (zeros(size(elids))+tag) );
        el_x = xpos(el_indices);
        el_y = ypos(el_indices);
        el_s = pI_val(el_indices);
        el_p = pI_pval(el_indices);

        %remove first and last element in each element.
        el_y = el_y(2:end-1);
        el_s = el_s(2:end-1);
        el_p = el_p(2:end-1);
        
        figure(3);
            hold on;
           plot(el_y,el_p,'LineWidth',lw-0.1,'MarkerSize',msz);
        %    title('DG');
           %plot([el_y(end), el_y(end)], [graphHtMmin,graphHtMax],'--k');
           xTTags(count) = el_y(end);
        figure(4); hold on;
           plot(el_y,el_s,'LineWidth',lw,'MarkerSize',msz);
         %  title('siac');
         %  plot([el_y(end), el_y(end)], [graphHtMmin,graphHtMax],'--k');
           
           
        figure(5); hold on;
            plot(el_y,el_p,'-b','LineWidth',lw,'MarkerSize',msz);
           plot(el_y,el_s,'-r','LineWidth',lw,'MarkerSize',msz);
           % plot([el_y(end), el_y(end)], [graphHtMmin,graphHtMax],'--k');
            
%         figure(3); hold on; caxis([-1,1])
%            plot(el_x,el_s,'LineWidth',lw,'MarkerSize',msz);
        count = count +1;
     end
     
     %%
     
     
     figure(3)
     axis on; 
     fig = gcf;
     set(gca,'XTick',sort(xTTags))
     set(gca,'XTickLabel','')
     ylabel('Vorticity');
     axis([min(ypos) max(ypos) min(pI_pval) max(pI_pval)])
     %print(strcat('paperUseImagesF/',filenameFrPrint,'_DG_R2'), '-dpng','-r500');
      
      
     figure(4)
     axis on; 
     fig = gcf;
     set(gca,'XTick',sort(xTTags))
     set(gca,'XTickLabel','')
     ylabel('Vorticity');
     axis([min(ypos) max(ypos) min(pI_val) max(pI_val)])
     print(strcat('paperUseImagesF/',filenameFrPrint,'_SIAC_R2'), '-dpng','-r500');
%      %%
%      
%      figure(5)
%      axis on; 
%      fig = gcf;
%      set(gca,'XTick',sort(xTTags))
%      set(gca,'XTickLabel','')
%      ylabel('Vorticity');
%      axis([min(ypos) max(ypos) min(pI_pval) max(pI_pval)])
%      print(strcat('paperUseImages/',filenameFrPrint,'_BTH_R2'), '-dpng','-r500');
    
    
%     s = sqrt(max(size(xpos)));
%     xVpos = reshape(xpos,s,s);    yVpos = reshape(ypos,s,s);
%     pUVal = reshape(pUval,s,s);   pVVal = reshape(pVval,s,s);   pPVal = reshape(pPval,s,s);
%     pUxVal = reshape(pUxval,s,s);   pVxVal = reshape(pVxval,s,s);   pPxVal = reshape(pPxval,s,s);
%     pUyVal = reshape(pUyval,s,s);   pVyVal = reshape(pVyval,s,s);   pPyVal = reshape(pPyval,s,s);
%     
%     pU_pVal = reshape(pU_pval,s,s);   pV_pVal = reshape(pV_pval,s,s);   pP_pVal = reshape(pP_pval,s,s);
%     pUx_pVal = reshape(pUx_pval,s,s);   pVx_pVal = reshape(pVx_pval,s,s);   pPx_pVal = reshape(pPx_pval,s,s);
%     pUy_pVal = reshape(pUy_pval,s,s);   pVy_pVal = reshape(pVy_pval,s,s);   pPy_pVal = reshape(pPy_pval,s,s);
%     
%     pAu_pVal = reshape(pAu_pval,s,s);   pAu_Val = reshape(pAu_val,s,s);
%     pAv_pVal = reshape(pAv_pval,s,s);   pAv_Val = reshape(pAv_val,s,s);
%     pC_pVal = reshape(pC_pval,s,s);     pC_Val = reshape(pC_val,s,s);
%     
%     pI_pVal = reshape(pI_pval,s,s);     pI_Val = reshape(pI_val,s,s);
    
    
% %Curvature,     
% 
% figure(18)
%     surf(xVpos,yVpos, pI_pVal,'EdgeAlpha',0.1); 
%     colorbar;
%     title(strcat('Proj P',int2str(poly),' Curvature'));
%     %view([0,90]);
%     saveas(18,strcat(fileName,'_',int2str(r),'_','p_C'),'jpg');
% 
% figure(19)
%     surf(xVpos,yVpos, pI_Val,'EdgeAlpha',0.1); 
%     colorbar;
%     title(strcat('SIAC P',int2str(poly),' Curvature'));
%     %view([0,90]);    
%     saveas(19,strcat(fileName,'_',int2str(r),'_','S_C'),'jpg');
% %%
% figure(20)
%     surf(xVpos,yVpos, pI_pVal,'EdgeAlpha',0); 
%     colorbar;
%     title(strcat('Proj P',int2str(poly),' Curvature'));
%     view([0,90]);
%     shading interp;
%     saveas(20,strcat('images/2DImages/',fileName,'_',int2str(r),'_','p_C_v2'),'jpg');
% 
% figure(21)
%     surf(xVpos,yVpos, pI_Val,'EdgeAlpha',0); 
%     colorbar;
%     title(strcat('SIAC P',int2str(poly),' Curvature'));
%     view([0,90]);  
%     shading interp;
%     saveas(21,strcat('images/2DImages/',fileName,'_',int2str(r),'_','S_C_v2'),'jpg');
%     
%     au = pUxVal.*pU_pVal + pUyVal.*pV_pVal;
%     av = pVxVal.*pU_pVal + pVyVal.*pV_pVal;
% %     normVel = sqrt(pU_pVal.*pU_pVal + pV_pVal.*pV_pVal);
% %     Unorm = pUx_pVal./normVel;
% %     Vnorm = pVx_pVal./normVel;
% %     normA = sqrt( au.*au + av.*av);
% %     Aunorm = au./normA;
% %     Avnorm = av./normA;
% %     C = Unorm.*Avnorm - Vnorm.*Aunorm;  
%     C = pU_pVal.*av- pV_pVal.*au;
% %     
% %     figure(22)
% %     surf(xVpos,yVpos, pI_pVal ,'EdgeAlpha',0); 
% %     colorbar;
% %     title(strcat('Proj P',int2str(poly),' C'));
% %     view([0,90]);
%     
%     figure(23)
%     contour(xVpos,yVpos, pI_Val,[min(pI_Val(:)) :.1: max(pI_pVal(:))] ); 
%     saveas(23,strcat('images/2DImages/',fileName,'_',int2str(r),'_','S_C_v3'),'jpg');
%     
%     figure(24)
%     contour(xVpos,yVpos, pI_pVal,[min(pI_pVal(:)) :.1: max(pI_pVal(:))] );
%     saveas(24,strcat('images/2DImages/',fileName,'_',int2str(r),'_','P_C_v3'),'jpg'); 
%     
     sxTTags = sort(xTTags);
     %save('xtigTagsforLabelUseat3p7','sxTTags');
end

