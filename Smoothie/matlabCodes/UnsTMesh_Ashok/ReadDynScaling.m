clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20];

%abc = '../../data/UnsTriMeshes/NM2_unsMeshvar1_0125_2_R_100_pX_2DDyn.txt';

R = 100;
poly =2;
order = poly;
count =1;

prefixName = '../../data/NPUTriMesh_proj/';
prefixName = '../../data/NUTriMesh_proj/';
prefixName = '../../data/UTriMesh_proj/';

width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
fsw = 26;
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize

for poly = [2,3,4]
          FileName = strcat('NM',int2str(poly),'_NPUniTriMesh05_d10_');
           FileName = strcat('NM',int2str(poly),'_NUTriMesh_05_');
           FileName = strcat('NM',int2str(poly),'_UniTriMesh_05_');

           prefixFilename = strcat( prefixName,FileName,int2str(poly),'_RS_',int2str(R));

            xfname = strcat(prefixFilename,'_pX_2DDyn.txt' );
            yfname = strcat(prefixFilename,'_pY_2DDyn.txt' );
            ffname = strcat(prefixFilename,'_pV_2DDyn.txt' );
            pfname = strcat(prefixFilename,'_pP_2DDyn.txt' );
            sfname = strcat(prefixFilename,'_pS_2DDyn.txt' );
            sDynfname = strcat(prefixFilename,'_pSDyn_2DDyn.txt' );
            dynsfname = strcat(prefixFilename,'_pDyn_2DDyn.txt' );
            tfname = strcat(prefixFilename,'_pT_2DDyn.txt' );
            tDynfname = strcat(prefixFilename,'_pTDyn_2DDyn.txt' );
            
            

            xpos = textread(xfname, '%f\t');
            ypos = textread(yfname, '%f\t');
            fpos = textread(ffname, '%f\t');
            ppos = textread(pfname, '%f\t');
            spos = textread(sfname, '%f\t');
            sDynpos = textread(sDynfname, '%f\t');
            dynspos = textread(dynsfname, '%f\t');
            tpos = textread(tfname, '%f\t');
            tDynpos = textread(tDynfname, '%f\t');
            
            
            xVpos = reshape(xpos,[R,R]);
            yVpos = reshape(ypos,[R,R]);
            fVpos = reshape(fpos,[R,R]);
            pVpos = reshape(ppos,[R,R]);
            sVpos = reshape(spos,[R,R]);
            sDynVpos = reshape(sDynpos,[R,R]);
            dynsVpos = reshape(dynspos,[R,R]);
            tVpos = reshape(tpos,[R,R]);
            tDynVpos = reshape(tDynpos,[R,R]);

            figure(1),
            surf(xVpos,yVpos,fVpos);

            figure(2),
            surf(xVpos,yVpos,pVpos);

            figure(3),
            surf(xVpos,yVpos,sVpos);

            %%
            figure(4),
            surf(xVpos,yVpos,abs(fVpos-pVpos));
            title('projection errors');
            figure(5),
            surf(xVpos,yVpos,abs(fVpos-sVpos));
            title('SIAC errors'); hold off;
            figure(51),
            surf(xVpos,yVpos,log10(abs(fVpos-sDynVpos)));
            shading interp; colorbar;
            title('SIAC Dyn errors');
            %%

            figure(6),
            contourf(xVpos,yVpos,log10(abs(fVpos-pVpos)));
            %title(strcat('$\mathcal{P}^',int2str(poly),'$  Before L- SIAC'),'fontsize',fsw,'Interpreter','latex'); %colorbar(); 
            %title('$\mathcal{P}$','Interpreter','latex')
            caxis([-16,0]);
            view(0,90); axis equal;
            ax = gca;
            ax.FontSize=fsw;
            line( [0.5;0.5], [0;1], 'color','k','LineWidth',6,'LineStyle','--')
            axis off;
            %print(strcat('ImagesF/',FileName,'proj'), '-dpng', '-r300');

            figure(7),
            contourf(xVpos,yVpos,log10(abs(fVpos-sVpos)));
            %surf(xVpos,yVpos,log10(abs(fVpos-sVpos)));
            title(strcat('\textbf{\textsf{$\mathcal{P}^',int2str(poly),'$ L- SIAC}}'),'fontsize',fsw,'Interpreter','latex'); %colorbar(); 
 %           title(strcat('$\mathcal{P}^',int2str(poly),'$ L-SIAC'),'fontsize',fsw); %colorbar(); 
            caxis([-16,0]);
            view(0,90); axis equal;axis tight;
            ax = gca;
            ax.FontSize=fsw;
            ax.FontName = 'ComicSans';
            ax.FontWeight='bold';
%            set(gca,'FontName','cmr12')
            line( [0;1], [0.5;0.5], 'color','y','LineWidth',6,'LineStyle','--')
            axis off;
            print(strcat('Images/',FileName,'LSIAC_BK'), '-dpng', '-r300');
            A = imread(strcat('Images/',FileName,'LSIAC_BK.png'));
            imwrite(A(10:1563,505:1910,:),strcat('ImagesF/',FileName,'LSIAC.png'))
            
            
            figure(8),
            contourf(xVpos,yVpos,log10(abs(fVpos-sDynVpos)));
            title(strcat('\textbf{\textsf{$\mathcal{P}^',int2str(poly),'$ Adaptive L- SIAC}}'),'fontsize',fsw,'Interpreter','latex'); %colorbar();
            caxis([-16,0]);
            view(0,90);axis equal; axis tight;
                        ax = gca;
            ax.FontSize=fsw;
            line( [0;1], [0.5;0.5], 'color','y','LineWidth',6,'LineStyle','--')
            axis off;
            print(strcat('Images/',FileName,'LSIAC_Dyn_BK'), '-dpng', '-r300');
            A = imread(strcat('Images/',FileName,'LSIAC_Dyn_BK.png'));
            imwrite(A(10:1563,505:1910,:),strcat('ImagesF/',FileName,'LSIAC_Dyn.png'))
            
             figure(9),
%             surf(xVpos,yVpos, dynsVpos); caxis([0,0.07]);
             contourf(xVpos, yVpos, dynsVpos,[0,0.01,0.02,0.03,0.04,0.05,0.06,0.07]); caxis([0,0.07])
%             contourf(xVpos, yVpos, dynsVpos);
             shading interp; %colorbar(); 
             view(0,90); hold off;
             axis off;
            ax = gca;
            ax.FontSize=16;
            axis equal;
%             print(strcat('Images/',FileName,'LSIAC_Scale_BK'), '-dpng', '-r300');
%             A = imread(strcat('Images/',FileName,'LSIAC_Scale_BK.png'));
%             imwrite(A(132:1558,495:1920,:),strcat('ImagesF/',FileName,'LSIAC_Scale.png'))


            figure(10),
            subplot(1,3,1),
            surf(xVpos,yVpos,log10(abs(fVpos-pVpos))); shading interp;
            title(strcat('Proj errors')); colorbar(); caxis([-16,0]);
            view(0,90); %axis equal;
            subplot(1,3,2),
            surf(xVpos,yVpos,log10(abs(fVpos-sVpos))); shading interp;
            title(strcat('LSIAC errors')); colorbar(); caxis([-16,0]);
            view(0,90); %axis equal;
            subplot(1,3,3),
            surf(xVpos,yVpos,log10(abs(fVpos-sDynVpos))); shading interp;
            title(strcat('LSIAC Dyn errors')); colorbar(); caxis([-16,0]);
            view(0,90); %axis equal;

            figure(11),
            subplot(1,2,1),
            surf(xVpos,yVpos, tVpos); shading interp;
            subplot(1,2,2);
            surf(xVpos,yVpos, tDynVpos); shading interp;
            
            figure(12),
            histogram (tVpos(:));
            figure(13),
            histogram (tDynVpos(:));
            
            disp('Proj');
            disp(sum(sum((abs(fVpos-pVpos)))));
            disp(max(max((abs(fVpos-pVpos)))));
            disp('SIAC');
            disp(sum(sum((abs(fVpos-sVpos)))));
            disp(max(max((abs(fVpos-sVpos)))));

            disp('SIAC Dyn');
            disp(sum(sum((abs(fVpos-sDynVpos)))));
            disp(max(max((abs(fVpos-sDynVpos)))));

end

%%
% figure (10);
%     addpath('../');
%     plotNekMesh('data/dataCos2/P2_triSplit6_L4_R1.xml');
%     xlim([-1,1]);
%     ylim([-1,1]);
%     plot3([-1,1],[0,0],[32,32],'--r')
%    print(strcat('Images/triSplit6_L4_R1_','meshFile'), '-dpng', '-r100');
%%    
    
% figure(9),
% surf(xVpos,yVpos,dynsVpos);
% title('dynscaling'); shading Interp;
% 
% figure(10),
% contourf(xVpos,yVpos,dynsVpos);
% title('dynscaling'); 


