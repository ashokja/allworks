% what is the file name used here.

clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [81];

poly =2;
order = 2;
alpha =1;
count =1;

fileName = 'Cy3l_Re500_';

spacingStrg='0.657';

%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize
atX = '5p8';

Cyl_Max = 6.3;
Cyl_Min = -5.2571;


for r = rates
    filenameFrPrint = strcat(fileName,int2str(poly),'_');
    xfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pX_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    yfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pY_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    pUfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pU_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pVfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pV_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pPfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pP_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    pUxfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUx_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pVxfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pPxfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPx_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    pUyfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pVyfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVy_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pPyfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPy_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    pU_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pU_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pV_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pV_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pP_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pP_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    pUx_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUx_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pVx_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pPx_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPx_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    pUy_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pVy_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVy_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pPy_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPy_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );

    pAu_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pAu_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pAv_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pAv_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pC_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pC_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );

    pAu_fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pAu_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pAv_fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pAv_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pC_fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pC_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    pI_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pI_p_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    pI_fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pI_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');
    
    
    pUval = textread(pUfname, '%f\t');  pUxval = textread(pUxfname, '%f\t');    pUyval = textread(pUyfname, '%f\t');
    pVval = textread(pVfname, '%f\t');  pVxval = textread(pVxfname, '%f\t');    pVyval = textread(pVyfname, '%f\t');
    pPval = textread(pPfname, '%f\t');  pPxval = textread(pPxfname, '%f\t');    pPyval = textread(pPyfname, '%f\t');

    pU_pval = textread(pU_pfname, '%f\t');  pUx_pval = textread(pUx_pfname, '%f\t');    pUy_pval = textread(pUy_pfname, '%f\t');
    pV_pval = textread(pV_pfname, '%f\t');  pVx_pval = textread(pVx_pfname, '%f\t');    pVy_pval = textread(pVy_pfname, '%f\t');
    pP_pval = textread(pP_pfname, '%f\t');  pPx_pval = textread(pPx_pfname, '%f\t');    pPy_pval = textread(pPy_pfname, '%f\t');

    pAu_pval = textread(pAu_pfname, '%f\t');   pAu_val = textread(pAu_fname, '%f\t');
    pAv_pval = textread(pAv_pfname, '%f\t');   pAv_val = textread(pAv_fname, '%f\t');
    pC_pval = textread(pC_pfname, '%f\t');     pC_val = textread(pC_fname, '%f\t');
    
    pI_pval = textread(pI_pfname, '%f\t');     pI_val = textread(pI_fname, '%f\t');
    
    s = sqrt(max(size(xpos)));
    xVpos = reshape(xpos,s,s);    yVpos = reshape(ypos,s,s);
    pUVal = reshape(pUval,s,s);   pVVal = reshape(pVval,s,s);   pPVal = reshape(pPval,s,s);
    pUxVal = reshape(pUxval,s,s);   pVxVal = reshape(pVxval,s,s);   pPxVal = reshape(pPxval,s,s);
    pUyVal = reshape(pUyval,s,s);   pVyVal = reshape(pVyval,s,s);   pPyVal = reshape(pPyval,s,s);
    
    pU_pVal = reshape(pU_pval,s,s);   pV_pVal = reshape(pV_pval,s,s);   pP_pVal = reshape(pP_pval,s,s);
    pUx_pVal = reshape(pUx_pval,s,s);   pVx_pVal = reshape(pVx_pval,s,s);   pPx_pVal = reshape(pPx_pval,s,s);
    pUy_pVal = reshape(pUy_pval,s,s);   pVy_pVal = reshape(pVy_pval,s,s);   pPy_pVal = reshape(pPy_pval,s,s);
    
    pAu_pVal = reshape(pAu_pval,s,s);   pAu_Val = reshape(pAu_val,s,s);
    pAv_pVal = reshape(pAv_pval,s,s);   pAv_Val = reshape(pAv_val,s,s);
    pC_pVal = reshape(pC_pval,s,s);     pC_Val = reshape(pC_val,s,s);
    
    pI_pVal = reshape(pI_pval,s,s);     pI_Val = reshape(pI_val,s,s);
    
    
%Curvature,     

% % figure(18)
% %     surf(xVpos,yVpos, pI_pVal,'EdgeAlpha',0.1); 
% %     colorbar;
% %     title(strcat('Proj P',int2str(poly),' Curvature'));
% %     %view([0,90]);
% %    % saveas(18,strcat(fileName,'_',int2str(r),'_','p_C'),'jpg');
% % 
% % figure(19)
% %     surf(xVpos,yVpos, pI_Val,'EdgeAlpha',0.1); 
% %     colorbar;
% %     title(strcat('SIAC P',int2str(poly),' Curvature'));
% %     %view([0,90]);    
% %    % saveas(19,strcat(fileName,'_',int2str(r),'_','S_C'),'jpg');
% % %%
% % figure(20)
% %     surf(xVpos,yVpos, pI_pVal,'EdgeAlpha',0); 
% %     colorbar;
% %     title(strcat('Proj P',int2str(poly),' Curvature'));
% %     view([0,90]);
% %     shading interp;
% %    % saveas(20,strcat('images/2DImages/',fileName,'_',int2str(r),'_','p_C_v2'),'jpg');
%%
figure(21)
   surf(xVpos,yVpos, -1.0*pI_Val,'EdgeAlpha',0); caxis([Cyl_Min,Cyl_Max])
   %title(strcat('SIAC P',int2str(poly),' Curvature'));
   view([0,90]);  
   shading interp;
   axis off; hold on;
  %print(strcat('paperUseImagesF/',filenameFrPrint,'Surf_SIAC'), '-dpng','-r500');
   %plotNekMesh();
   xlim([2.5,10.5]);
    ylim([-4,4]);
  %print(strcat('paperUseImagesF/',filenameFrPrint,'Surf_SIAC_Mesh'), '-dpng','-r500'); 
    
   
    %%
figure(23)
   contour(xVpos,yVpos, -1.0*pI_Val,[Cyl_Min :.3: Cyl_Max],'LineWidth',lw ); 
   axis off;
  %print(strcat('paperUseImagesF/',filenameFrPrint,'Surf_SIAC_Contour'), '-dpng','-r500');
   %plotNekMesh();
   line([3.7,3.7],[-4,4],'lineStyle','--','Color',[0.5 0.5 0.5],'linewidth',lw); hold on;
   xlim([2.5,10.5]);
   ylim([-4,4]);
   print(strcat('paperUseImagesF/',filenameFrPrint,'Surf_Linear_LSIAC_Contour',spacingStrg), '-dpng','-r500');
%print(strcat('paperUseImagesF/',filenameFrPrint,'Surf_SIAC_Contour_MESH'), '-dpng','-r500');

%%

% Vorticity type 1.
xspace = .1;
  Ux_s1 = diff(pU_pVal,1,2)./  diff(xVpos,1,2);
  Vx_s1 = diff(pV_pVal,1,2)./  diff(xVpos,1,2);
  
  Uy_s1 = diff(pU_pVal,1,1)./  diff(yVpos,1,1);
  Vy_s1 = diff(pV_pVal,1,1)./  diff(yVpos,1,1);
  
  [Ux_s2,Uy_s2] = gradient(pU_pVal,0.1,0.1);
  [Vx_s2,Vy_s2] = gradient(pV_pVal,0.1,0.1);
  
  Vor1 = Uy_s2 - Vx_s2;
  figure(25)
   surf(xVpos,yVpos, -1.0*Vor1,'EdgeAlpha',0); caxis([Cyl_Min,Cyl_Max])
   view([0,90]);  
   shading interp;
   axis off;
   %print(strcat('paperUseImagesF/',filenameFrPrint,'Surf_Bias1'), '-dpng','-r500');
   %plotNekMesh();
   xlim([2.5,10.5]);
   ylim([-4,4]);
   %print(strcat('paperUseImagesF/',filenameFrPrint,'Surf_Bias1_Mesh'), '-dpng','-r500');
   
   %%
   
  figure(26)
    contour(xVpos,yVpos, -1.0*Vor1,[Cyl_Min :.3: Cyl_Max],'LineWidth',lw-0.25 );
    %axis off;
    set(gca, 'XTick',linspace(2.5,10.5,80),'YTick',linspace(-4,4,80));
    set(gca, 'XTickLabel', [ ] ,'YTickLabel', [ ] );
    
    line([3.7,3.7],[-4,4],'lineStyle','--','Color',[0.5 0.5 0.5],'linewidth',lw); hold on;
%    print(strcat('paperUseImagesF/',filenameFrPrint,'Surf_Bias1_Contour'), '-dpng','-r500');
    %grid on;
    %plotNekMesh();
    %xlim([2.5,10.5]);
    %ylim([-4,4]);
%   print(strcat('paperUseImages/',filenameFrPrint,'Surf_Bias1_Mesh_Contour'), '-dpng','-r500');
    
  % shading interp;
   
  % for line graph
  
  %%
  [el_indices] = find(floor(xVpos*10) == ones(size(xVpos))*37);
  figure(27)
  yypos = yVpos(el_indices);
  vor11 = Vor1(el_indices);
  plot(yypos,-1.0*vor11,'-k','LineWidth',lw,'MarkerSize',msz);
  load ('xtigTagsforLabelUseat3p7.mat');
box off;
       axis on; 
     fig = gcf;
     set(gca,'XTick', linspace(-4,4,81))
     set(gca,'XTickLabel','')
     ylabel('Vorticity');
     axis([min(yypos) max(yypos) min(-1.0*vor11(:)) max(-1.0*vor11(:))])
     ax1 = gca;
     ax2 = axes('Position', get(ax1, 'Position'),'Color', 'none');
     set(ax2, 'XAxisLocation', 'top','YAxisLocation','Right');
     set(ax2, 'XLim', get(ax1, 'XLim'),'YLim', get(ax1, 'YLim'));
     set(ax2, 'XTick',sort(sxTTags),'YTick',[]);
     set(ax2, 'XTickLabel', [ ] ,'YTickLabel', [ ] );
     %print(strcat('paperUseImagesF/',filenameFrPrint,'Line_Vorticity_FineSample_Bias1'), '-dpng','-r500');
     
    %% 
%  diff(xVPos,1,2);
    
% %    saveas(21,strcat('images/2DImages/',fileName,'_',int2str(r),'_','S_C_v2'),'jpg');
% %     
% %     au = pUxVal.*pU_pVal + pUyVal.*pV_pVal;
% %     av = pVxVal.*pU_pVal + pVyVal.*pV_pVal;
% %     normVel = sqrt(pU_pVal.*pU_pVal + pV_pVal.*pV_pVal);
% %     Unorm = pUx_pVal./normVel;
% %     Vnorm = pVx_pVal./normVel;
% %     normA = sqrt( au.*au + av.*av);
% %     Aunorm = au./normA;
% %     Avnorm = av./normA;
% %     C = Unorm.*Avnorm - Vnorm.*Aunorm;  
% %     C = pU_pVal.*av- pV_pVal.*au;
% %     
% %     figure(22)
% %     surf(xVpos,yVpos, pI_pVal ,'EdgeAlpha',0); 
% %     colorbar;
% %     title(strcat('Proj P',int2str(poly),' C'));
% %     view([0,90]);
    

   % saveas(23,strcat('images/2DImages/',fileName,'_',int2str(r),'_','S_C_v3'),'jpg');
% %     
% %     figure(24)
% %     contour(xVpos,yVpos, pI_pVal,[min(pI_pVal(:)) :.1: max(pI_pVal(:))] );
% %    saveas(24,strcat('images/2DImages/',fileName,'_',int2str(r),'_','P_C_v3'),'jpg'); 
    
    
end

