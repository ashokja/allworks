% Graph sym jagged 8 -113 DG
% Results from PostP_hexSym_DG. Dated Dec 18th.


clear;
close all;
MR = (2:2:16)*7+1;
steps = 2000;
%mshEl = [168, 672, 2688, 10752, 43008];
% ord(1) = 336;    ord(2) = 1344;    ord(3) = 3024;   ord(4) = 5376;
% ord(5) = 8400;   ord(6) = 12096;   ord(7) = 16464;   ord(8) = 21504;

mshEl(1) = 168;    mshEl(2) = 672;    mshEl(3) = 1512;   mshEl(4) = 2688;
mshEl(5) = 4200;   mshEl(6) = 6048;   mshEl(7) = 8232;   mshEl(8) = 10752;
mshEl = mshEl*2;

addpath('..');
pdgCartfilename = '../PostP_hexSym_DG/log_hex_sym_DG_MAXSIZE_MR';
pdgMaxfilename = '../PostP_hexSym_DG/log_hex_sym_DG_MAXSIZE_MR';
%pDGCartfilename = '../PostP7_2/log_hex_unionjack_DG_CART_MR';
%pDGMaxfilename = '../PostP7_1/log_hex_unionjack_DG_MAXSIZE_MR';

width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 0.4;      % LineWidth
msz = 8;       % MarkerSize
atX = '5p8';



P = '1';
KF ='0';
UF = '0';
count =1;
for mr = MR
    filename = strcat(pdgCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_dg_C(count,:) = ReadFile(filename);
    count= count+1;
end
figure(11)
plot(log(mshEl),log(C_P1_dg_C(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P1_dg_C(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_dg_C(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P1_dg_C(:,4)),'-g.'); hold off;
title('PR P1 Filter Cart');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='0';
count =1;
for mr = MR
    filename = strcat(pdgMaxfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_dg_M(count,:) = ReadFile(filename);
    count= count+1;
end
figure(12)
plot(log(mshEl),log(C_P1_dg_M(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P1_dg_M(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_dg_M(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P1_dg_M(:,4)),'-g.'); hold off;
title('PR P1 Filter MaxSize');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='1';
count =1;
for mr = MR
    filename = strcat(pdgCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_dg_H(count,:) = ReadFile(filename);
    count= count+1;
end
figure(13)
plot(log(mshEl),log(C_P1_dg_H(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P1_dg_H(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_dg_H(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P1_dg_H(:,4)),'-g.'); hold off;
title('PR P1 Filter Hex');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;


P = '2';
KF ='0';
UF = '0';
count =1;
for mr = MR
    filename = strcat(pdgCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_dg_C(count,:) = ReadFile(filename);
    count= count+1;
end
figure(21)
plot(log(mshEl),log(C_P2_dg_C(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P2_dg_C(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_dg_C(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P2_dg_C(:,4)),'-g.'); hold off;
title('PR P2 Filter Cart');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='0';
count =1;
for mr = MR
    filename = strcat(pdgMaxfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_dg_M(count,:) = ReadFile(filename);
    count= count+1;
end
figure(22)
plot(log(mshEl),log(C_P2_dg_M(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P2_dg_M(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_dg_M(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P2_dg_M(:,4)),'-g.'); hold off;
title('PR P2 Filter MaxSize');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='1';
count =1;
for mr = MR
    filename = strcat(pdgCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_dg_H(count,:) = ReadFile(filename);
    disp(filename);
    count= count+1;
end
figure(23)
plot(log(mshEl),log(C_P2_dg_H(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P2_dg_H(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_dg_H(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P2_dg_H(:,4)),'-g.'); hold off;
title('PR P2 Filter Hex');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;





figure(51)
plot(log(mshEl),log(C_P1_dg_C(:,1)),'-bo','LineWidth',lw,'MarkerSize',10); hold on;
%plot(log(mshEl),log(C_P1_PR_C(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_dg_M(:,2)),'-g^','LineWidth',lw,'MarkerSize',10); hold on;
plot(log(mshEl),log(C_P1_dg_H(:,2)),'-r*','LineWidth',lw,'MarkerSize',10); hold on;
set(gca,'fontsize',15)
xlabel('log(Mesh Size)');
ylabel('log(Error)')
%legend('Before','Post(Max)','Post(Hex)')
legend('DG','SIAC','HexSIAC')
X = log(mshEl);
Y = log(C_P1_dg_M(:,2));
k = 1;
m = -1.*(2*k+1)/2;
c = Y(2) - m*X(2);
NY = m.*X+c;
figure(51), hold on;
plot(X,NY,'--k');
%axis equal;
%title('DG P1 Filter L2 Hex Sym');
print('P1_HexSym_dg_L2_NQ','-dpng','-r500');

figure(52)
plot(log(mshEl),log(C_P1_dg_C(:,3)),'-bo','LineWidth',lw,'MarkerSize',10); hold on;
%plot(log(mshEl),log(C_P1_PR_C(:,4)),'-go'); hold on;
plot(log(mshEl),log(C_P1_dg_M(:,4)),'-g^','LineWidth',lw,'MarkerSize',10); hold on;
plot(log(mshEl),log(C_P1_dg_H(:,4)),'-r*','LineWidth',lw,'MarkerSize',10); hold on;
set(gca,'fontsize',15)
xlabel('log(Mesh Size)');
ylabel('log(Error)')
%legend('Before','Post(Max)','Post(Hex)')
legend('DG','SIAC','HexSIAC')
X = log(mshEl);
Y = log(C_P1_dg_M(:,4));
k = 1;
m = -1.*(2*k+1)/2;
c = Y(2) - m*X(2);
NY = m.*X+c;
figure(52), hold on;
plot(X,NY,'--k');
%axis equal;
%title('DG P1 Filter Linf Hex Sym');
print('P1_HexSym_dg_Linf_NQ','-dpng','-r500');



figure(61)
plot(log(mshEl),log(C_P2_dg_C(:,1)),'-bo','LineWidth',lw,'MarkerSize',10); hold on;
%plot(log(mshEl),log(C_P2_PR_C(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_dg_M(:,2)),'-g^','LineWidth',lw,'MarkerSize',10); hold on;
plot(log(mshEl),log(C_P2_dg_H(:,2)),'-r*','LineWidth',lw,'MarkerSize',10); hold on;
set(gca,'fontsize',15)
xlabel('log(Mesh Size)');
ylabel('log(Error)')
%legend('Before','Post(Max)','Post(Hex)')
legend('DG','SIAC','HexSIAC')
X = log(mshEl);
Y = log(C_P2_dg_M(:,2));
k = 2;
m = -1.*(2*k+1)/2;
c = Y(2) - m*X(2);
NY = m.*X+c;
figure(61), hold on;
plot(X,NY,'--k');
%axis equal;
%title('DG P2 Filter L2 hex sym');
print('P2_HexSym_dg_L2_NQ','-dpng','-r500');

%%
figure(62)
plot(log(mshEl),log(C_P2_dg_C(:,3)),'-bo','LineWidth',lw,'MarkerSize',10); hold on;
%plot(log(mshEl),log(C_P2_PR_C(:,4)),'-go'); hold on;
plot(log(mshEl),log(C_P2_dg_M(:,4)),'-g^','LineWidth',lw,'MarkerSize',10); hold on;
plot(log(mshEl),log(C_P2_dg_H(:,4)),'-r*','LineWidth',lw,'MarkerSize',10); hold on;
set(gca,'fontsize',15)
xlabel('log(Mesh Size)');
ylabel('log(Error)')
%legend('Before','Post(Max)','Post(Hex)')
legend('DG','SIAC','HexSIAC')
X = log(mshEl);
Y = log(C_P2_dg_M(:,4));
k = 2;
m = -1.*(2*k+1)/2;
c = Y(2) - m*X(2);
NY = m.*X+c;
figure(62), hold on;
plot(X,NY,'--k');
%axis equal;
%title('DG P2 Filter Linf hex sym');
print('P2_HexSym_dg_Linf_NQ','-dpng','-r500');


% %%
% % Data tables for P1.
% %L2, DG, SIAC, HSIAC
% %DG
% Yresult(1,:) = mshEl;
% Yresult(2,:) = C_P1_dg_C(:,1);
% Yresult(3,:) = C_P1_dg_M(:,2);
% Yresult(4,:) = C_P1_dg_H(:,2);
% Yresult(5,:) = abs(C_P1_dg_H(:,2) - C_P1_dg_M(:,2));
% input.data = Yresult';
% input.tableColLabels = {'No Mesh El','DG','SIAC','HSIAC','abs(HSIAC-SIAC)'};
% input.dataFormat = {'%i',1,'%2.3e',4};
% input.tableCaption = strcat('P1 L2 DG data (',int2str(steps),' timesteps)');
% input.tableLabel = strcat('P1_L2_DG_',int2str(steps));
% latex = latexTable(input);
% 
% % save LaTex code as file
% fid=fopen(strcat('latex/MyLatex_P1_L2_DG_',int2str(steps),'.tex'),'w');
% [nrows,ncols] = size(latex);
% for row = 1:nrows
%     fprintf(fid,'%s\n',latex{row,:});
% end
% fclose(fid);
% fprintf('\n... your LaTex code has been saved as ''MyLatex.tex'' in your working directory\n');
% 
% 
% 
% Yresult2(1,:) = mshEl;
% Yresult2(2,:) = C_P2_dg_C(:,1);
% Yresult2(3,:) = C_P2_dg_M(:,2);
% Yresult2(4,:) = C_P2_dg_H(:,2);
% Yresult2(5,:) = abs(C_P2_dg_H(:,2) - C_P2_dg_M(:,2));
% input2.data = Yresult2';
% input2.tableColLabels = {'No Mesh El','DG','SIAC','HSIAC','abs(HSIAC-SIAC)'};
% input2.dataFormat = {'%i',1,'%2.3e',4};
% input2.tableCaption = strcat('P2 L2 DG data (',int2str(steps),' timesteps)');
% input2.tableLabel = strcat('P2_L2_DG_',int2str(steps));
% latex = latexTable(input2);
% fid=fopen(strcat('latex/MyLatex_P2_L2_DG_',int2str(steps),'.tex'),'w');
% [nrows,ncols] = size(latex);
% for row = 1:nrows
%     fprintf(fid,'%s\n',latex{row,:});
% end
% fclose(fid);
% fprintf('\n... your LaTex code has been saved as ''MyLatex.tex'' in your working directory\n');
% 
% 
% %%
% %%
% % Data tables for P1.
% %L2, DG, SIAC, HSIAC
% %DG
% Yresult(1,:) = mshEl;
% Yresult(2,:) = C_P1_dg_C(:,3);
% Yresult(3,:) = C_P1_dg_M(:,4);
% Yresult(4,:) = C_P1_dg_H(:,4);
% Yresult(5,:) = abs(C_P1_dg_H(:,4) - C_P1_dg_M(:,4));
% input.data = Yresult';
% input.tableColLabels = {'No Mesh El','DG','SIAC','HSIAC','abs(HSIAC-SIAC)'};
% input.dataFormat = {'%i',1,'%2.3e',4};
% input.tableCaption = strcat('P1 LInf DG data (',int2str(steps),' timesteps)');
% input.tableLabel = strcat('P1_LInf_DG_',int2str(steps));
% latex = latexTable(input);
% 
% % save LaTex code as file
% fid=fopen(strcat('latex/MyLatex_P1_Linf_DG_',int2str(steps),'.tex'),'w');
% [nrows,ncols] = size(latex);
% for row = 1:nrows
%     fprintf(fid,'%s\n',latex{row,:});
% end
% fclose(fid);
% fprintf('\n... your LaTex code has been saved as ''MyLatex.tex'' in your working directory\n');
% 
% 
% 
% Yresult2(1,:) = mshEl;
% Yresult2(2,:) = C_P2_dg_C(:,3);
% Yresult2(3,:) = C_P2_dg_M(:,4);
% Yresult2(4,:) = C_P2_dg_H(:,4);
% Yresult2(5,:) = abs(C_P2_dg_H(:,4) - C_P2_dg_M(:,4));
% input2.data = Yresult2';
% input2.tableColLabels = {'No Mesh El','DG','SIAC','HSIAC','abs(HSIAC-SIAC)'};
% input2.dataFormat = {'%i',1,'%2.3e',4};
% input2.tableCaption = strcat('P2 LInf DG data (',int2str(steps),' timesteps)');
% input2.tableLabel = strcat('P2_LInf_DG_',int2str(steps));
% latex = latexTable(input2);
% fid=fopen(strcat('latex/MyLatex_P2_Linf_DG_',int2str(steps),'.tex'),'w');
% [nrows,ncols] = size(latex);
% for row = 1:nrows
%     fprintf(fid,'%s\n',latex{row,:});
% end
% fclose(fid);
% fprintf('\n... your LaTex code has been saved as ''MyLatex.tex'' in your working directory\n');
% 

%%
% Data tables for P1.
%L2, DG, SIAC, HSIAC
%DG
Yresult(1,:) = mshEl;
Yresult(2,:) = C_P1_dg_C(:,1);
Yresult(3,:) = C_P1_dg_C(:,3);
Yresult(4,:) = C_P1_dg_M(:,2);
Yresult(5,:) = C_P1_dg_M(:,4);
Yresult(6,:) = C_P1_dg_H(:,2);
Yresult(7,:) = C_P1_dg_H(:,4);

%Yresult(4,:) = C_P1_dg_H(:,2);
%Yresult(5,:) = abs(C_P1_dg_H(:,2) - C_P1_dg_M(:,2));
input.data = Yresult';
input.tableColLabels = {'No Mesh El','DG(L2)','DG(Linf)','SIAC(L2)','SIAC(Linf)','HSIAC(L2)','HSIAC(Linf)'};
input.dataFormat = {'%i',1,'%2.3e',6};
input.tableCaption = strcat('P1 LAll DG data (',int2str(steps),' timesteps)');
input.tableLabel = strcat('P1_LAll_DG_',int2str(steps));
latex = latexTable(input);

% save LaTex code as file
fid=fopen(strcat('latex/MyLatex_P1_LAll_DG_',int2str(steps),'.tex'),'w');
[nrows,ncols] = size(latex);
for row = 1:nrows
    fprintf(fid,'%s\n',latex{row,:});
end
fclose(fid);
fprintf('\n... your LaTex code has been saved as ''MyLatex.tex'' in your working directory\n');



Yresult2(1,:) = mshEl;
Yresult2(2,:) = C_P2_dg_C(:,1);
Yresult2(3,:) = C_P2_dg_C(:,3);
Yresult2(4,:) = C_P2_dg_M(:,2);
Yresult2(5,:) = C_P2_dg_M(:,4);
Yresult2(6,:) = C_P2_dg_H(:,2);
Yresult2(7,:) = C_P2_dg_H(:,4);
input2.data = Yresult2';
input2.tableColLabels = {'No Mesh El','DG(L2)','DG(Linf)','SIAC(L2)','SIAC(Linf)','HSIAC(L2)','HSIAC(Linf)'};
input2.dataFormat = {'%i',1,'%2.3e',6};
input2.tableCaption = strcat('P2 LAll DG data (',int2str(steps),' timesteps)');
input2.tableLabel = strcat('P2_LAll_DG_',int2str(steps));
latex = latexTable(input2);
fid=fopen(strcat('latex/MyLatex_P2_LAll_DG_',int2str(steps),'.tex'),'w');
[nrows,ncols] = size(latex);
for row = 1:nrows
    fprintf(fid,'%s\n',latex{row,:});
end
fclose(fid);
fprintf('\n... your LaTex code has been saved as ''MyLatex.tex'' in your working directory\n');


