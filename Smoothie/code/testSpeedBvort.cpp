#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include <StdRegions/StdHexExp.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"
#include <boost/timer.hpp>
#include <iomanip>


using namespace SIACUtilities;
//using namespace Eigen;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;
						
NekDouble FindLambda2(NekDouble ux,NekDouble uy, NekDouble uz,
					 NekDouble vx, NekDouble vy, NekDouble vz,
					 NekDouble wx, NekDouble wy, NekDouble wz)
{
	Eigen::Matrix3d m1;
		//double tmp= pUx[index];
		//m1(1,1) = tmp;
	m1 << ux, uy, uz,
		  vx, vy, vz,
		  wx, wy, wz;
		//cout << m1<< endl;
			
	Eigen::Matrix3d mp = m1+ m1.transpose();
	Eigen::Matrix3d mn = m1- m1.transpose();
	Eigen::Matrix3d mE = mp*mp +mn*mn; 
				
	Eigen::EigenSolver<Eigen::Matrix3d>	eSolv(mE);
	eSolv.compute(mE, false);
	complex<double> lambda1 = eSolv.eigenvalues()[0];	
	complex<double> lambda2 = eSolv.eigenvalues()[1];	
	complex<double> lambda3 = eSolv.eigenvalues()[2];
	Array<OneD,NekDouble> eigsEach(3,0.0);	
	eigsEach[0] = lambda1.real();
	eigsEach[1] = lambda2.real();
	eigsEach[2] = lambda3.real();
	std::sort(eigsEach.begin(),eigsEach.end());
	return eigsEach[1] ;	
}

int main(int argc, char* argv[])
{
	int Nx = atoi(argv[1]);
	int Ny = atoi(argv[2]);
	int Nz = atoi(argv[3]);
	vector<NekDouble> tparams;
	NekDouble StartX = 0.25, StartY =0.05, StartZ=-0.04;
	NekDouble EndX = 0.35, EndY =0.15, EndZ=0.06;
	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0), sz = 1.0/(Nz-1.0);
	tparams.push_back(0.0);
	for (size_t i =0; i <Nx; i++)
	{
		//tparams.push_back(t);
		tparams.push_back(StartX+i*sx*(EndX-StartX));
	}
	tparams.push_back(0.7);
	cout << "X direction tparams size" << tparams.size();	
	//Z
	tparams.clear();
	tparams.push_back(-0.48);
	for ( NekDouble t = StartZ; t<=EndZ; t=t+sz*(EndZ-StartZ))
	{
		tparams.push_back(t);
	}
	tparams.push_back(0.48);
	cout << "Z direction tparams size" << tparams.size();	

	//Y
	tparams.clear();
	tparams.push_back(-0.48);
	for ( NekDouble t = StartY; t<=EndY; t=t+sy*(EndY-StartY))
	{
		tparams.push_back(t);
	}
	tparams.push_back(0.48);
	cout << "Y direction tparams size" << tparams.size();	
}
int main2(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		cout << "4th arg output resolution." << endl;
		return 0;
	}

	argc = 2;
	
	double startTime = clock();
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();

//	HNM3D->LoadData( fname+ ".chk",var);
	cout << "loading xml file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	string fldname = fname+".fld";
	
	HNM3D->LoadData(fldname,var);
	cout << "loading fld file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	HNM3D->LoadExpListIntoRTree();
	
	cout << "loading into r-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	
	int tNquadPts = HNM3D->m_expansions[0]->GetTotPoints();
	int tNCoeffs= HNM3D->m_expansions[0]->GetNcoeffs();
	cout << "fPhys:" << tNquadPts<< endl;
	cout << "fCoeffs:" << HNM3D->m_expansions[0]->GetNcoeffs()<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);
	Array<OneD,NekDouble> u(tNquadPts);
	Array<OneD,NekDouble> v(tNquadPts);
	Array<OneD,NekDouble> w(tNquadPts);

	HNM3D->CalculateDynamicScaling();
	
	switch( HNM3D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
/*	
	Array<OneD,NekDouble> u_DG = HNM3D->m_expansions[0]->GetPhys();	
	Array<OneD,NekDouble> v_DG = HNM3D->m_expansions[1]->GetPhys();
	Array<OneD,NekDouble> w_DG = HNM3D->m_expansions[2]->GetPhys();
	Array<OneD,NekDouble> ux_DG(tNquadPts), uy_DG(tNquadPts), uz_DG(tNquadPts);
	Array<OneD,NekDouble> vx_DG(tNquadPts), vy_DG(tNquadPts), vz_DG(tNquadPts);
	Array<OneD,NekDouble> wx_DG(tNquadPts), wy_DG(tNquadPts), wz_DG(tNquadPts);


	Array<OneD,NekDouble> ax(tNquadPts), ay(tNquadPts), az(tNquadPts);
	Array<OneD,NekDouble> curX(tNquadPts), curY(tNquadPts), curZ(tNquadPts);
	Array<OneD,NekDouble> curNorm2(tNquadPts) ;

	HNM3D->m_expansions[0]->PhysDeriv( u_DG, ux_DG, uy_DG, uz_DG);
	HNM3D->m_expansions[1]->PhysDeriv( v_DG, vx_DG, vy_DG, vz_DG);
	HNM3D->m_expansions[2]->PhysDeriv( w_DG, wx_DG, wy_DG, wz_DG);
*/
	
//  meshGrid.

	int gPtsX = atoi(argv[4]);
	int gPtsY = atoi(argv[4]);
	int gPtsZ = atoi(argv[4]);
	int Nx=gPtsX, Ny=gPtsY, Nz=gPtsZ;
	int totPts = Nx*Ny*Nz;
    //minx = 0.25; miny = 0.05; minz = -0.04;
	//maxx = 0.35; maxy = 0.15; maxz= 0.06;

	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0), sz = 1.0/(Nz-1.0);

	NekDouble StartX = 0.25, StartY =0.05, StartZ=-0.04;
	NekDouble EndX = 0.35, EndY =0.15, EndZ=0.06;

	Array<OneD,NekDouble> directionX(3,0.0), coord(3,0.0), directionY(3,0.0) ;
	Array<OneD,NekDouble> pX(totPts), pY(totPts),pZ(totPts);
    Array<OneD,NekDouble> pU(totPts), pV(totPts), pW(totPts);
    Array<OneD,NekDouble> pUx(totPts), pVx(totPts), pWx(totPts);
    Array<OneD,NekDouble> pUy(totPts), pVy(totPts), pWy(totPts);
    Array<OneD,NekDouble> pUz(totPts), pVz(totPts), pWz(totPts);
    Array<OneD,NekDouble> pU_p(totPts), pV_p(totPts), pW_p(totPts);
    Array<OneD,NekDouble> pVor_x(totPts), pVor_y(totPts), pVor_z(totPts);
    Array<OneD,NekDouble> pVor_xp(totPts), pVor_yp(totPts), pVor_zp(totPts);

//	stuff needed for SIAC FILTER	
	SmoothieSIAC3D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]), atof(argv[3]), 1);	
	SmoothieSIAC3D sm(SIACUtilities::eSYM_2kp1_1SIDED_4kp1, HNM3D, atoi(argv[2]), atof(argv[3]));
	NekDouble scaling = atof(argv[3]), valY,valZ;
	Array<OneD,NekDouble> dirX(3,0.0), dirY(3,0.0), dirZ(3,0.0);
	dirX[0] = 1.0; dirY[1] = 1.0; dirZ[2] = 1.0;

	vector<NekDouble> tparams,tvalsUx,tvalsUy,tvalsUz,tvalsVx,tvalsVy,tvalsVz,tvalsWx,tvalsWy,tvalsWz;
	vector<NekDouble> tvals_AllUx,tvals_AllUy,tvals_AllUz,tvals_AllVx,tvals_AllVy,tvals_AllVz,tvals_AllWx,tvals_AllWy,tvals_AllWz;
	tparams.push_back(0.0);
	for ( NekDouble t = StartX; t<=EndX; t=t+sx*(EndX-StartX))
	{
		tparams.push_back(t);
	}
	tparams.push_back(0.7);


	cout << tparams.size() << endl;	
	cout << "Starting calculating derivatives" << endl;
	boost::timer tim1;
	vector<NekDouble> stPoint(3);
	int NumQpts = 6;
	HNM3D->CalculateDynamicScaling();
	stPoint[0] = StartX; stPoint[1] = StartY; stPoint[2] = StartZ;
	stPoint[0]=0.0;
	for ( int i =0; i < Nx; i++)
	{
		for (int j=0; j< Ny; j++)
		{
			stPoint[1] = StartY +i*sy*(EndY-StartY); 
			stPoint[2] = StartZ +j*sz*(EndZ-StartZ); 
			//smD.EvaluateUsingLineAt(stPoint,dirX,NumQpts,scaling,tparams,tvalsUx,0);
			//smD.EvaluateUsingLineAt(stPoint,dirX,NumQpts,scaling,tparams,tvalsVx,1);
			//smD.EvaluateUsingLineAt(stPoint,dirX,NumQpts,scaling,tparams,tvalsWx,2);
			smD.EvaluateUsingLineAt_v3(stPoint,dirX,6,9,scaling,tparams,tvalsUx,0);
			smD.EvaluateUsingLineAt_v3(stPoint,dirX,6,9,scaling,tparams,tvalsVx,1);
			smD.EvaluateUsingLineAt_v3(stPoint,dirX,6,9,scaling,tparams,tvalsWx,2);
			copy(tvalsUx.begin()+1, tvalsUx.end()-1, back_inserter(tvals_AllUx));
			copy(tvalsVx.begin()+1, tvalsVx.end()-1, back_inserter(tvals_AllVx));
			copy(tvalsWx.begin()+1, tvalsWx.end()-1, back_inserter(tvals_AllWx));
			tvalsUx.clear(); tvalsVx.clear(); tvalsWx.clear();
		}
		//cout << i << endl;
	}
	cout << "Direction X done" << endl;
	cout << tvalsUx.size() << endl;
	cout << tvals_AllUx.size() << endl;

	//y-dervivative	
	tparams.clear();
	tparams.push_back(-0.48);
	for ( NekDouble t = StartY; t<=EndY; t=t+sy*(EndY-StartY))
	{
		tparams.push_back(t);
	}
	tparams.push_back(0.48);
	stPoint[0] = StartX; stPoint[1] = StartY; stPoint[2] = StartZ;
	stPoint[1]=0.0;
	for ( int i =0; i < Nx; i++)
	{
		for (int j=0; j< Ny; j++)
		{
			stPoint[0] = StartX +i*sy*(EndX-StartX); 
			stPoint[2] = StartZ +j*sz*(EndZ-StartZ); 
			//sm.EvaluateUsingLineAt(stPoint,dirY,NumQpts,scaling,tparams,tvalsUy,0);
			//sm.EvaluateUsingLineAt(stPoint,dirY,NumQpts,scaling,tparams,tvalsVy,1);
			//sm.EvaluateUsingLineAt(stPoint,dirY,NumQpts,scaling,tparams,tvalsWy,2);
			smD.EvaluateUsingLineAt_v3(stPoint,dirY,6,9,scaling,tparams,tvalsUy,0);
			smD.EvaluateUsingLineAt_v3(stPoint,dirY,6,9,scaling,tparams,tvalsVy,1);
			smD.EvaluateUsingLineAt_v3(stPoint,dirY,6,9,scaling,tparams,tvalsWy,2);
			copy(tvalsUy.begin()+1, tvalsUy.end()-1, back_inserter(tvals_AllUy));
			copy(tvalsVy.begin()+1, tvalsVy.end()-1, back_inserter(tvals_AllVy));
			copy(tvalsWy.begin()+1, tvalsWy.end()-1, back_inserter(tvals_AllWy));
			tvalsUy.clear(); tvalsVy.clear(); tvalsWy.clear();
		}
		//cout << i << endl;
	}
	cout << "Direction Y done" << endl;
	cout << tvalsUy.size() << endl;
	cout << tvals_AllUy.size() << endl;
	
	//z-derivative
	tparams.clear();
	tparams.push_back(-0.48);
	for ( NekDouble t = StartZ; t<=EndZ; t=t+sz*(EndZ-StartZ))
	{
		tparams.push_back(t);
	}
	tparams.push_back(0.48);
	stPoint[0] = StartX; stPoint[1] = StartY; stPoint[2] = StartZ;
	stPoint[2]=0.0;
	for ( int i =0; i < Nx; i++)
	{
		for (int j=0; j< Ny; j++)
		{
			stPoint[0] = StartX +i*sy*(EndX-StartX); 
			stPoint[1] = StartY +j*sz*(EndY-StartY); 
			//sm.EvaluateUsingLineAt(stPoint,dirZ,NumQpts,scaling,tparams,tvalsUz,0);
			//sm.EvaluateUsingLineAt(stPoint,dirZ,NumQpts,scaling,tparams,tvalsVz,1);
			//sm.EvaluateUsingLineAt(stPoint,dirZ,NumQpts,scaling,tparams,tvalsWz,2);
			smD.EvaluateUsingLineAt_v3(stPoint,dirZ,6,9,scaling,tparams,tvalsUz,0);
			smD.EvaluateUsingLineAt_v3(stPoint,dirZ,6,9,scaling,tparams,tvalsVz,1);
			smD.EvaluateUsingLineAt_v3(stPoint,dirZ,6,9,scaling,tparams,tvalsWz,2);
			copy(tvalsUz.begin()+1, tvalsUz.end()-1, back_inserter(tvals_AllUz));
			copy(tvalsVz.begin()+1, tvalsVz.end()-1, back_inserter(tvals_AllVz));
			copy(tvalsWz.begin()+1, tvalsWz.end()-1, back_inserter(tvals_AllWz));
			tvalsUz.clear(); tvalsVz.clear(); tvalsWz.clear();
		}
		//cout << i << endl;
	}
	cout << "Direction Z done" << endl;
	cout << tvalsUz.size() << endl;
	cout << tvals_AllUz.size() << endl;
	
	cout << "TimetakenDer =" << tim1.elapsed() << endl;
	boost::timer tim2;
	
	for(int i=0; i < Nx; i++)
	{
		for(int j=0; j< Ny;j++)
		{
			for(int k=0; k<Nz; k++)
			{
				int orgIndex = i*Nx*Nz+j*Nz+k;
				int xIndex = j*Nx*Nz +k*Nz+i;
				pUx[orgIndex]=tvals_AllUx[xIndex]; 
				pVx[orgIndex]=tvals_AllVx[xIndex]; 
				pWx[orgIndex]=tvals_AllWx[xIndex];
				int yIndex = i*Nx*Nz +k*Nz+j;
				pUy[orgIndex]=tvals_AllUy[yIndex]; 
				pVy[orgIndex]=tvals_AllVy[yIndex]; 
				pWy[orgIndex]=tvals_AllWy[yIndex];
				int zIndex = i*Nx*Nz +j*Nz+k;
				pUz[orgIndex]=tvals_AllUz[zIndex]; 
				pVz[orgIndex]=tvals_AllVz[zIndex]; 
				pWz[orgIndex]=tvals_AllWz[zIndex];
			}
		}
	}
	cout << "Dont know why this mapping" << endl;
	cout << "TimetakenMapping =" << tim2.elapsed() << endl;
	boost::timer tim3;
	
	std::vector<double> eigsEach(3);
	Eigen::EigenSolver<Eigen::Matrix3d>	eSolv;	
	Array<OneD,NekDouble> Eig1(totPts), Eig2(totPts),Eig3(totPts);
	for ( int k = 0 ; k<Nz;k++)
	{
		for ( int i =0; i < Nx; i++)
		{
			for (int j=0; j< Ny; j++)
			{
				int index = i*Nx*Nz+j*Nz+k;
				Eigen::Matrix3d m1;
				//double tmp= pUx[index];
				//m1(1,1) = tmp;
				m1 << pUx[index], pUy[index], pUz[index],
					  pVx[index], pVy[index], pVz[index],
					  pWx[index], pWy[index], pWz[index];
				//cout << m1<< endl;
			
				Eigen::Matrix3d mp = m1+ m1.transpose();
				Eigen::Matrix3d mn = m1- m1.transpose();
				Eigen::Matrix3d mE = mp*mp +mn*mn; 
				
				Eigen::EigenSolver<Eigen::Matrix3d>	eSolv(mE);
				eSolv.compute(mE, false);
				complex<double> lambda1 = eSolv.eigenvalues()[0];	
				complex<double> lambda2 = eSolv.eigenvalues()[1];	
				complex<double> lambda3 = eSolv.eigenvalues()[2];	
				//cout << lambda1 << "\t" << lambda2 << "\t" << lambda3 << endl;
		//		cout << lambda1.real() << "\t" << lambda2.real() << "\t" << lambda3.real() << endl;
		//		cout << lambda1.imag() << "\t" << lambda2.imag() << "\t" << lambda3.imag() << endl;
				eigsEach[0] = lambda1.real();
				eigsEach[1] = lambda2.real();
				eigsEach[2] = lambda3.real();
				std::sort(eigsEach.begin(),eigsEach.end());
				Eig1[index] = eigsEach[0];
				Eig2[index] = eigsEach[1];
				Eig3[index] = eigsEach[2];
			}
		}
	}
	cout << "TimetakenEig =" << tim3.elapsed() << endl;
	boost::timer tim4;
	

	NekDouble isovalue = -50;
	vector<int> HasIsoContour(Nx*Ny*Nz);
	for ( int i =0; i < Nx-1; i++)
	{
		for (int j=0; j< Ny-1; j++)
		{
			for ( int k = 0 ; k<Nz-1;k++)
			{
				// Tag indices(ijk) of element which potential have iso surface.
				int index = i*Ny*Nz+j*Nz+k;
				int id_ijk = i*Ny*Nz+j*Nz+k, id_i1jk = (i+1)*Ny*Nz+j*Nz+k, id_ij1k = i*Ny*Nz+(j+1)*Nz+k, id_i1j1k = (i+1)*Ny*Nz+(j+1)*Nz+k; 
				int id_ijk1 = i*Ny*Nz+j*Nz+k+1, id_i1jk1 = (i+1)*Ny*Nz+j*Nz+k+1, id_ij1k1 = i*Ny*Nz+(j+1)*Nz+k+1, id_i1j1k1 = (i+1)*Ny*Nz+(j+1)*Nz+k+1; 
				// all greater than iso value or all smaller than isovalue.
				if (  ( (Eig2[id_ijk]-isovalue >=0)  && (Eig2[id_i1jk]-isovalue >=0)  && (Eig2[id_ij1k]-isovalue >=0)  &&	(Eig2[id_i1j1k]-isovalue >=0) &&		
					   (Eig2[id_ijk1]-isovalue >=0) && (Eig2[id_i1jk1]-isovalue >=0) && (Eig2[id_ij1k1]-isovalue >=0) &&	(Eig2[id_i1j1k1]-isovalue >=0) ) ||		
					( (Eig2[id_ijk]-isovalue >=0)  && (Eig2[id_i1jk]-isovalue >=0)  && (Eig2[id_ij1k]-isovalue >=0)  &&	(Eig2[id_i1j1k]-isovalue >=0) &&		
					   (Eig2[id_ijk1]-isovalue >=0) && (Eig2[id_i1jk1]-isovalue >=0) && (Eig2[id_ij1k1]-isovalue >=0) &&	(Eig2[id_i1j1k1]-isovalue >=0) ) )	
				{
					HasIsoContour[index] = 0;
				}else{
					// tag the element for iso-surface.
					HasIsoContour[index] = 1;
				}		
			}
		}
	}
	cout << "TimetakenCon =" << tim4.elapsed() << endl;
/*
	int n_quadPts = 7;
	boost::timer tim5;
	NekDouble xscaling= sx*(EndX-StartX), yscaling= sy*(EndY-StartY), zscaling= sz*(EndZ-StartZ);
	Array<OneD,NekDouble> hStPoint(3,0.0);
	for ( int i =0; i < Nx-1; i++)
	{
		for (int j=0; j< Ny-1; j++)
		{
			for ( int k = 0 ; k<Nz-1;k++)
			{
				// Tag indices(ijk) of element which potential have iso surface.
				int index = i*Ny*Nz+j*Nz+k;
				// This element has Isocontour. Need to form High order element.
				if (HasIsoContour[index] ==1)
				{
					// Create a high order element.
					LibUtilities::PointsKey quadPointsKey(n_quadPts,Nektar::LibUtilities::eGaussGaussLegendre );
					LibUtilities::BasisKey bk = LibUtilities::BasisKey(Nektar::LibUtilities::eGauss_Lagrange ,
																							n_quadPts, quadPointsKey ) ;
					// Create a hex element.
					Nektar::StdRegions::StdExpansionSharedPtr hexExp_std= MemoryManager<StdRegions::StdHexExp>::AllocateSharedPtr(bk,bk,bk);	
					int hexNumQ = hexExp_std->GetTotPoints();
					Array<OneD,NekDouble> hex_xc0(hexNumQ),hex_xc1(hexNumQ),hex_xc2(hexNumQ);
					Array<OneD,NekDouble> ux(hexNumQ),uy(hexNumQ),uz(hexNumQ);
					Array<OneD,NekDouble> vx(hexNumQ),vy(hexNumQ),vz(hexNumQ);
					Array<OneD,NekDouble> wx(hexNumQ),wy(hexNumQ),wz(hexNumQ),lambda2(hexNumQ);
						
					for ( int q = 0; q< hexNumQ;q++)
					{
						hStPoint[0] = pX[index]+ hex_xc0[q]*xscaling;
						hStPoint[1] = pY[index]+ hex_xc1[q]*xscaling;
						hStPoint[2] = pZ[index]+ hex_xc2[q]*xscaling;
						
						smD.EvaluateAt(hStPoint[0],hStPoint[1],hStPoint[2],ux[q],valY,valZ,dirX,scaling,0);
						smD.EvaluateAt(hStPoint[0],hStPoint[1],hStPoint[2],uy[q],valY,valZ,dirY,scaling,0);
						smD.EvaluateAt(hStPoint[0],hStPoint[1],hStPoint[2],uz[q],valY,valZ,dirZ,scaling,0);
						
						smD.EvaluateAt(hStPoint[0],hStPoint[1],hStPoint[2],vx[q],valY,valZ,dirX,scaling,1);
						smD.EvaluateAt(hStPoint[0],hStPoint[1],hStPoint[2],vy[q],valY,valZ,dirY,scaling,1);
						smD.EvaluateAt(hStPoint[0],hStPoint[1],hStPoint[2],vz[q],valY,valZ,dirZ,scaling,1);
						
						smD.EvaluateAt(hStPoint[0],hStPoint[1],hStPoint[2],wx[q],valY,valZ,dirX,scaling,2);
						smD.EvaluateAt(hStPoint[0],hStPoint[1],hStPoint[2],wy[q],valY,valZ,dirY,scaling,2);
						smD.EvaluateAt(hStPoint[0],hStPoint[1],hStPoint[2],wz[q],valY,valZ,dirZ,scaling,2);
						
						lambda2[q] = FindLambda2( ux[q],uy[q],uz[q], vx[q],vy[q],vz[q], wx[q],wy[q],wz[q]);
					}
				}
			}
		}
	}	
*/	
	cout << "TimetakenCon =" << tim4.elapsed() << endl;
	
	cout << "Finding the iso values" << endl;

	cout << "totalTimetaken =" << tim1.elapsed() << endl;
	NektarBaseClass NBC;
		NBC.writeNekArray(pX,fname+"_P_"+argv[2]+"_R_"+argv[4]+"_OS_"+argv[3]+"_x.txt");
		NBC.writeNekArray(pY,fname+"_P_"+argv[2]+"_R_"+argv[4]+"_OS_"+argv[3]+"_y.txt");
		NBC.writeNekArray(pZ,fname+"_P_"+argv[2]+"_R_"+argv[4]+"_OS_"+argv[3]+"_z.txt");
	
		NBC.writeNekArray(pUx,fname+"_P_"+argv[2]+"_R_"+argv[4]+"_OS_"+argv[3]+"_ux.txt");
		NBC.writeNekArray(pUy,fname+"_P_"+argv[2]+"_R_"+argv[4]+"_OS_"+argv[3]+"_uy.txt");
		NBC.writeNekArray(pUz,fname+"_P_"+argv[2]+"_R_"+argv[4]+"_OS_"+argv[3]+"_uz.txt");
		
		NBC.writeNekArray(pVx,fname+"_P_"+argv[2]+"_R_"+argv[4]+"_OS_"+argv[3]+"_vx.txt");
		NBC.writeNekArray(pVy,fname+"_P_"+argv[2]+"_R_"+argv[4]+"_OS_"+argv[3]+"_vy.txt");
		NBC.writeNekArray(pVz,fname+"_P_"+argv[2]+"_R_"+argv[4]+"_OS_"+argv[3]+"_vz.txt");
		
		NBC.writeNekArray(pWx,fname+"_P_"+argv[2]+"_R_"+argv[4]+"_OS_"+argv[3]+"_wx.txt");
		NBC.writeNekArray(pWy,fname+"_P_"+argv[2]+"_R_"+argv[4]+"_OS_"+argv[3]+"_wy.txt");
		NBC.writeNekArray(pWz,fname+"_P_"+argv[2]+"_R_"+argv[4]+"_OS_"+argv[3]+"_wz.txt");
		
		NBC.writeNekArray(Eig1,fname+"_P_"+argv[2]+"_R_"+argv[4]+"_OS_"+argv[3]+"_eig1.txt");
		NBC.writeNekArray(Eig2,fname+"_P_"+argv[2]+"_R_"+argv[4]+"_OS_"+argv[3]+"_eig2.txt");
		NBC.writeNekArray(Eig3,fname+"_P_"+argv[2]+"_R_"+argv[4]+"_OS_"+argv[3]+"_eig3.txt");

		//NBC.writeNekArray(HasIsoContour,fname+"R"+argv[4]+"_OS_"+argv[3]+"_HasIsoContour.txt");

	return 0;
}

