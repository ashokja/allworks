function[coeff_h] = degElevation(coeff)
    n = max(size(coeff))-1;
    % first index special case.
    coeff_h = zeros(n+2,1);
    coeff_h(1) = coeff(1);
    for k = 1:n
        coeff_h(k+1) = coeff(k+1)*(1- k/(n+1)) + coeff(k)*(k/(n+1)); 
    end
    coeff_h(n+2) = coeff(n+1);