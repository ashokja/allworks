% test root finding algorithm using sign functions.
clear;
%create data for sign function.
K = (2:13);
P = (3:13);
x = (-1:.1:1);
for k = K
    data = sin(k*x);
    % create coeff and do root finding.
    plot(x,data,'r*');
    for p = P
        [a,b,c] =CompareNewtonBernstein('d',0,x,data,p);
        resultsB(k,p,:) = [a(1),b(1),c(1)];
        resultsM(k,p,:) = [a(2),b(2),c(2)];
    end
end
%%
% ploting k vs time for different p Bernstein
figure(2),
Xaxis = K;
for p = P
    Yaxis = resultsB(K,p,2);
    plot(Xaxis,log10(Yaxis),'-r'); hold on;
    text(Xaxis+0.2,log10(Yaxis)-0.2,int2str(p));
end
for p = P
    Yaxis = resultsM(K,p,2);
    plot(Xaxis,log10(Yaxis),'-b'); hold on;
    text(Xaxis+0.2,log10(Yaxis)-0.2,int2str(p));
end
hold off;

figure(3),
Xaxis = P;
for k = K
    Yaxis = resultsB(k,P,2);
    plot(Xaxis,log10(Yaxis),'-r'); hold on;
    text(Xaxis,log10(Yaxis),int2str(k));
end
for k = K
    Yaxis = resultsM(k,P,2);
    plot(Xaxis,log10(Yaxis),'-b'); hold on;
    text(Xaxis,log10(Yaxis),int2str(k));
end
hold off;

figure(4),
Xaxis = P;
clear Yaxis
for p = P
    YaxisT(p,:) =[resultsB(p,p,1),resultsM(p,p,1)];
    YaxisD(p,:) =[resultsB(p,p,2),resultsM(p,p,3)];
    YaxisS(p,:) =[resultsB(p,p,3),resultsM(p,p,3)];
end

figure(4),
subplot(3,1,1),
plot(Xaxis,log(YaxisT(3:end,1)),'-r*'); hold on;
plot(Xaxis,log(YaxisT(3:end,2)),'-b*'); hold off;
ylabel('log(Time)');
xlabel('polynomail(p=k)');
legend('Berns','Monomial');
title('Time plot ');

subplot(3,1,2),
plot(Xaxis,log(YaxisD(3:end,1)),'-r*'); hold on;
plot(Xaxis,log(YaxisD(3:end,2)),'-b*'); hold off;
ylabel('log(Leaves)');
xlabel('polynomail(p=k)');
legend('Berns','Monomial');
title('Tree Depth');

subplot(3,1,3),
plot(Xaxis,log(YaxisS(3:end,1)),'-r*'); hold on;
plot(Xaxis,log(YaxisS(3:end,2)),'-b*'); hold off;
ylabel('log(Splits)');
xlabel('polynomail(p=k)');
legend('Berns','Monomial');
title('Tree Splits');
