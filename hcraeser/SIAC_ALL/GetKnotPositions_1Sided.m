%GetKnotPositions.
function [pos] = GetKnotPositions_1Sided(order,scaling,offset,a,b)
    switch nargin
        case 0
            disp('order needs to specified. Assuming for debugging.');
            order =1.0;
            scaling =1.0;
            offset=0.0;
        case 1
            scaling =1.0;
            offset=0.0;
        case 2
            offset=0.0;
    end


    switch order
        case 1
            pos = -2:1:2;
        case 2
            pos = -3.5:1:3.5;
        otherwise
            pos = 'null';
            disp('unknown order was requested');
    end
    pos = pos*scaling+offset;
    
    if (offset < (a+b)/2)
        if pos(1) <a
            pos = pos- pos(1) +a;
        end
    else
        if pos(end)>b
            pos = pos-pos(end)+b;
        end
    end
    
end