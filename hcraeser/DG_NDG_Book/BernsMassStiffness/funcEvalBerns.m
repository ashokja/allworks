function[Data] = funcEvalBerns(coeff,D_int)
    %Evaluate a function given berns coeff.
    %D_int =0:.01:1;
    numD_int = max(size(D_int));
    Data = zeros(numD_int,1);
    n = max(size(coeff))-1;
    for d = 1:numD_int
        for k=0:n
            Data(d) = Data(d)+ coeff(k+1)*nchoosek(n,k)*D_int(d)^k*(1-D_int(d))^(n-k);
        end
    end
    
end