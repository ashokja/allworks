function res = B_i(i,n,rv)
    if(i <0)
        res = 0;
        return
    end
    if( i>n)
        res =0;
        return
    end
    res = nchoosek(n,i)*(rv).^i .* (1-rv).^(n-i);
end