% solution for normal newton iteration.
function listRes = solveNewton1D(fh, Dfh, X)

%Evaluate F[X] DF[X].
    fhx = fh(X); 
    fhxz = hasZero(fh(X));
    listRes = [0,0];
    %if F[X] does not contain zero. then return.
    if ~fhxz
      %  fprintf('Ingnoring Interval'); X
        return;
    end
    
    % If X1 is very small interval. print into solution.
    if (X(2) - X(1)) < 10^(-8)
        fprintf('Found root:');
        X
        return;
    end
%if F[X] contains a zero and DF[X] does not contain zero
   % X1 = N(X) intersect X
   % If X1 is very small interval. print into solution.
   % Solve (X1);
    dfhx = Dfh(X);
    dfhxz = hasZero(Dfh(X));

    if ~dfhxz
        X1 = IntNew1D(fh,Dfh,X);
        if isnan(X1)
    %    fprintf('Ingnoring Interval'); X
     %   X1
            return;
        end
        solveNewton1D(fh,Dfh,X1);
    end
%if F[X] contains a zero and DF[X] does not contain a zero.
    if dfhxz
        % X_1 = first half of X
        % Solve (X1);
        X1(1) = X(1);
        X1(2) = (X(1) + X(2))/2;
        solveNewton1D(fh,Dfh,X1);
        
        % X_2 = second half of X
        % Solve (X2);
        X2(1) = (X(1) + X(2))/2;
        X2(2) = X(2);
        solveNewton1D(fh,Dfh,X2);
    end
    
    function res = hasZero(Y)
        if Y(1)*Y(2) <=0
            res = true;
        else
            res = false;
        end
    end

end
