% run Bernstein vs Monomial test and plot graph.
clear;
% n = 6; X(1)  = n;
% coeff = [-1,2,0,-2.5,-1,5,-1];
% res(1,:) = CompareNewtonBernstein(coeff);
% 
% n = 8; X(2) = n;
% coeff = [-1,2,0,-2.5,-1,5,-1,-1,-1];
% res(2,:) = CompareNewtonBernstein(coeff);
% 
% n = 10; X(3)= n;
% coeff = [-1,2,0,-2.5,-1,5,-1,-1,-1,-1,-4];
% res(3,:) = CompareNewtonBernstein(coeff);
% 
% plot(X,res(:,1),X,res(:,2));


for j = 1:10
for i = 4:8
    coeff = (rand(1,i+1)*2-1)*8;
    [a,b,c] =CompareNewtonBernstein('c',coeff,0);
    res(j,i-3,:) = a;
    lcount(j,i-3,:) = b;
    splits(j,i-3,:) = c;
end
end
%%


X = (4:13);
figure(1)
plot(X,log10(res(:,1)),'-g*',X,log10(res(:,2)),'-r*');
ylabel('time(log10)')
xlabel('polynomail(p)')
legend('Berns','Monomial')
title('Time taken by Bernstein vs Monomail for root solving');

figure(2)
plot(X,log10(lcount(:,1)),'-g*',X,log10(lcount(:,2)),'-r*');
ylabel('depth(log10)')
xlabel('polynomail(p)')
legend('Berns','Monomial')
title('Depth of Interval Newton tree of Bernstein vs Monomail for root solving');

figure(3)
plot(X,log10(splits(:,1)),'-g*',X,log10(splits(:,2)),'-r*');
ylabel('splits(log10)')
xlabel('polynomail(p)')
legend('Berns','Monomial')
title('Splits Interval Newton tree of Bernstein vs Monomail for root solving');
