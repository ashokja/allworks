function J = jacobian2D(e1,e2,Xa,Xb,Xc,Xd)
    J = zeros(2,2);
    J(:,1) = -Xa*(1-e2)/4 + Xb*(1-e2)/4 +Xc*(1+e2)/4 - Xd*(1+e2)/4;
    %J(1,2) = -Xa*(1-e2)/4 + Xb*(1-e2)/4 +Xc(1+e2)/4 - Xd*(1+e2)/4;
    J(:,2) = -Xa*(1-e1)/4 - Xb*(1+e1)/4 +Xc*(1+e1)/4 + Xd*(1-e1)/4;
    %J(2,2) = -Xa*(1-e2)/4 + Xb*(1-e2)/4 +Xc(1+e2)/4 - Xd*(1+e2)/4;
end
