% test2D cheby D function using x points.
function [] = testCheby2D(NX,NY,nx,ny,px,py)
% we can do test for 1D first    
    switch nargin
        case 0,
            NX =5; NY=6;
            nx =1; ny =1;
            px =0;py =0;
    end
    addpath ../PolyLib
    %WXQ,WYQ,XQ,YQ,NX,NY
    [XQ,WXQ] = JacobiGLZW(NX+nx,0,0);
    [YQ,WYQ] = JacobiGLZW(NY+ny,0,0);
    %Cal Lagrance before itself.
    for i =0:NX
         PhiXQ(i+1,:) = LegendrePoly(i,XQ);
    end
    for j = 0:NY
        PhiYQ(j+1,:) = LegendrePoly(j,YQ);
    end
    for i = 0:NX
        for j = 0:NY
            sol = 0;
            for x = 1:max(size(XQ))
                for y = 1:max(size(YQ))
                    sol = sol + func(XQ(x),YQ(y))*WXQ(x)*WYQ(y)*...
                                   PhiXQ(i+1,x)*PhiYQ(j+1,y);
                end
            end
            u(i+1,j+1) = (2*i+1)*(2*j+1)/4*sol;
        end
    end
    %u(1,1) = 2.6;
    solPts = zeros( max(size(XQ)), max(size(YQ)) );
    for i = 0:NX
        for j = 0:NY
            solPts = solPts+u(i+1,j+1)* PhiXQ(i+1,:)'*PhiYQ(j+1,:);
        end
    end
    
    [X,Y] = meshgrid(XQ,YQ);
    data(:,1) = X(:);
    data(:,2) = Y(:);
    figure(1);
    surf(X,Y,func(X,Y)); hold on;
    surf(X,Y,solPts'); hold off;
    er = sum(sum(abs(func(X,Y)-solPts')))
%    plot3(data(:,1),data(:,2),func(data(:,1),data(:,2)),'g'); hold on;
%    plot3(data(:,1),data(:,2),solPts(:),'r');

    [DChebX,xcheb] = cheb(NX+nx+px);
    [DChebY,ycheb] = cheb(NY+ny+py);
%    [DChebZ,zcheb] = cheb(NZ+nz+pz);
     solChebPts = zeros( max(size(xcheb)), max(size(ycheb)) );
    for i = 0:NX
        for j = 0:NY
            solChebPts = solChebPts+u(i+1,j+1)* LegendrePoly(i,xcheb)*LegendrePoly(j,ycheb)';
        end
    end

    [XCheb,YCheb] = meshgrid(xcheb,ycheb);
    % X partial Derivative
    solChebPtsDX = DChebX * solChebPts ;
    %solChebPts*DChebY;
    % Y partial Derivative
    solChebPtsDY = (DChebY * solChebPts')';
    
    figure (2)
    surf(XCheb,YCheb,funcDX(XCheb,YCheb)); hold on;
    surf(XCheb,YCheb,solChebPtsDX'); hold off;
    title('SOl D_X');
    ActErr = funcDX(XCheb,YCheb) - solChebPtsDX';
    erX = sum(abs(ActErr(:)))
    
    figure (3)
    surf(XCheb,YCheb,funcDY(XCheb,YCheb)); hold on;
    surf(XCheb,YCheb,solChebPtsDY'); hold off;
    title('SOl D_Y');
    ActErr = funcDY(XCheb,YCheb) - solChebPtsDY';
    erX = sum(abs(ActErr(:)))
    
    function z = func(x,y)
        z = x.*sin(pi*x)+y.*sin(pi*y);
        
    end
  
    function z = funcDX(x,y)
        z = sin(pi*x) + pi*x.*cos(pi*x);
    end
    
    function z = funcDY(x,y)
        z = sin(pi*y) + pi*y.*cos(pi*y);
    end

end