% Draw all figure in one shot.
d =2;
n =3;

C_out = CreateAlpha(d+1,n);
[numCout,del] = size(C_out);

[XQ,WQ]

%X = [0:0.1:1];

X = (XQ'+1)/2;
[XX,YY] = meshgrid(X);

for f = 1:numCout
   subplot(numCout,1,f), mesh(XX,YY,EvalMomentBasis(X,d,n,f,WQ')); 
end