#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"

using namespace SIACUtilities;
using namespace std;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>

int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		return 0;
	}

	argc = 2;

	clock_t startTime = clock();
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);

	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	//HNM3D->LoadMesh(var[0]);
	//HNM3D->LoadMesh(var[1]);
	//HNM3D->LoadMesh(var[2]);
	
	cout << "loading xml file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	string fname = vSession->GetSessionName();

	//string fldname = fname + ".fld/Info.xml";	
	string fldname = fname + ".fld";	
	cout << fname << endl;
	cout << fldname << endl;

	HNM3D->LoadData( fldname ,var);
	cout << "loading fld file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	HNM3D->LoadExpListIntoRTree();
	cout << "no of exp" << HNM3D->m_expansions.size() << endl;
	cout << "expsize:\t"<< HNM3D->m_expansions[0]->GetExpSize() << endl;

//	Array<OneD,NekDouble> coeffs = HNM3D->m_expansions[0]->GetCoeffs();

	//HNM3D->printNekArray( coeffs, 0);

	//print at particular grid interval and check it is working.
	Array<OneD,NekDouble> xPos(10),yPos(10),zPos(10),val(10);
	Array<OneD,NekDouble> physCoord(3,0.0);
	for (int i=0; i < 10; i++)
	{
		xPos[i] = 0.55655555;
		yPos[i] = 0.55655555;
		zPos[i] = 0.55655555;
	}

	physCoord[0] = 0.55655555; 
	physCoord[1] = 0.55655555; 
	physCoord[2] = 0.55655555; 
	
	int elId = HNM3D->GetExpansionIndexUsingRTree(physCoord);

	Array<OneD,NekDouble> allPhys= HNM3D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> elPhys = allPhys.CreateWithOffset(allPhys, HNM3D->m_expansions[0]->GetPhys_Offset(elId));

	cout << elId << endl;	
	startTime = clock();
	for (int i = 0 ; i < 10;i++)
	{
		HNM3D->EvaluateAt(xPos,yPos,zPos, elId, elId, val,0);
	}
	cout << "Time Taken to evaluate at 10x10 points"<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	LocalRegions::ExpansionSharedPtr lexp = HNM3D->m_expansions[0]->GetExp(elId);
	// print local Coordinates.
	startTime = clock();
	for (int i = 0 ; i < 100;i++)
	{
		lexp->PhysEvaluate(physCoord, elPhys);
	}
	cout << "Time Taken to evaluate at 10x10 points"<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();


	Array<OneD,NekDouble> locCoord(3,0.0);
	
	startTime = clock();
	for (int i = 0 ; i < 100;i++)
	{
		HNM3D->m_expansions[0]->GetExp(elId)->GetGeom()->GetLocCoords(physCoord,locCoord);
	}
	cout << "Time Taken to evaluate at 10x10 points"<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	HNM3D->printNekArray( physCoord,0);
	HNM3D->printNekArray( locCoord,0);

	
	startTime = clock();
	for (int i = 0 ; i < 100;i++)
	{
		lexp->StdPhysEvaluate(locCoord, elPhys);
	}
	cout << "Time Taken to evaluate at 10x10 points"<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	int sampleRate = 101;
	Array<OneD,NekDouble> saveValues(sampleRate*sampleRate*sampleRate),sampleCoord(3,0.0);
	
	
	startTime = clock();
	for (int i =0; i <sampleRate;i++)
	{
		for(int j=0; j<sampleRate;j++)
		{
			for(int k=0; k<sampleRate;k++)
			{
				sampleCoord[0] = (NekDouble)i/( (NekDouble)sampleRate -1.0)*2.0-1.0;
				sampleCoord[1] = (NekDouble)j/( (NekDouble)sampleRate -1.0)*2.0-1.0;
				sampleCoord[2] = (NekDouble)k/( (NekDouble)sampleRate -1.0)*2.0-1.0;
				saveValues[i*sampleRate*sampleRate+j*sampleRate+k] = lexp->StdPhysEvaluate(sampleCoord, elPhys);
			}
		}
	}
	cout << "Time Taken set up the ensumble"<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;

	// To evaluate sample coord
//	saveValues
	NekDouble v_ijk, v_i1jk, v_i1j1k, v_ij1k;
	NekDouble v_ijk1, v_i1jk1, v_i1j1k1, v_ij1k1;
	NekDouble vy_floor, vy_top, vf;
	NekDouble vx_ijk_i1jk, vx_ij1k_i1j1k, vx_ijk1_i1jk1, vx_ij1k1_i1jk1; 
	int ii,jj,kk;
	startTime = clock();
	for (int i = 0 ; i < 100;i++)
	{
	ii = floor( (locCoord[0]+1.0)/2.0*(sampleRate-1.0) );
	jj = floor( (locCoord[1]+1.0)/2.0*(sampleRate-1.0) );
	kk = floor( (locCoord[2]+1.0)/2.0*(sampleRate-1.0) );
		v_ijk = saveValues[ii*sampleRate*sampleRate+jj*sampleRate+kk];
		v_i1jk = saveValues[(ii+1)*sampleRate*sampleRate+jj*sampleRate+kk];
		v_i1j1k = saveValues[(ii+1)*sampleRate*sampleRate+(jj+1)*sampleRate+kk];
		v_ij1k = saveValues[ii*sampleRate*sampleRate+(jj+1)*sampleRate+kk];
		v_ijk1 = saveValues[ii*sampleRate*sampleRate+jj*sampleRate+(kk+1)];
		v_i1jk1 = saveValues[(ii+1)*sampleRate*sampleRate+jj*sampleRate+(kk+1)];
		v_i1j1k1 = saveValues[(ii+1)*sampleRate*sampleRate+(jj+1)*sampleRate+(kk+1)];
		v_ij1k1 = saveValues[ii*sampleRate*sampleRate+(jj+1)*sampleRate+(kk+1)];

		vx_ijk_i1jk = v_ijk + ( (locCoord[0]+1.0)/2.0*(sampleRate-1.0) - ii )* (v_i1jk - v_ijk);
		vx_ij1k_i1j1k = v_ij1k + ( (locCoord[0]+1.0)/2.0*(sampleRate-1.0) -ii )* (v_i1j1k - v_ij1k);
		vy_floor = vx_ijk_i1jk + ( (locCoord[1]+1.0)/2.0*(sampleRate-1.0) -jj )* (vx_ij1k_i1j1k - vx_ijk_i1jk);
		
		vx_ijk1_i1jk1 = v_ijk1 + ( (locCoord[0]+1.0)/2.0*(sampleRate-1.0) -ii)* (v_i1jk1 - v_ijk1);
		vx_ij1k1_i1jk1 = v_ij1k1 + ( (locCoord[0]+1.0)/2.0*(sampleRate-1.0) -ii)* (v_i1j1k1 - v_ij1k1);
		vy_top= vx_ijk1_i1jk1 + ( (locCoord[1]+1.0)/2.0*(sampleRate-1.0) -jj)* (vx_ij1k1_i1jk1 - vx_ijk1_i1jk1);

		vf = vy_floor + ( (locCoord[2]+1.0)/2.0*(sampleRate-1.0)-kk) * (vy_top - vy_floor);
	}
	cout << "Time Taken to evaluate at 10x10 points"<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	cout << vf << endl;
	cout << "ijk \t" << v_ijk <<endl;
	cout << "i1jk \t" << v_i1jk <<endl;
	cout << "i1j1k \t" << v_i1j1k <<endl;
	cout << "ij1k \t" << v_ij1k <<endl;
	cout << "ijk1 \t" << v_ijk1 <<endl;
	cout << "i1jk1 \t" << v_i1jk1 <<endl;
	cout << "i1j1k1 \t" << v_i1j1k1 <<endl;
	cout << "ij1k1 \t" << v_ij1k1 <<endl;


	cout << val[0] << endl;
	//HNM3D->printNekArray(saveValues,0.0);
	cout << val[0] - vf << endl;	
	return 0;
}


