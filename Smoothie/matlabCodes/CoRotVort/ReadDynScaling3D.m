clear;
close all;
% test 
 % rates = [10,20,40,60,80];
 % rates = [20];

%abc = '../../data/UnsTriMeshes/NM2_unsMeshvar1_0125_2_R_100_pX_2DDyn.txt';

R = 100;
poly =2;
order = poly;
count =1;

prefixName = '../../data/bentVort_2/';

width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
fsw = 20;
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize
Rp = R;

vor3D_sDyn = zeros(50,R,R);
vor3D_s = zeros(50,R,R);
vor3D_p = zeros(50,R,R);

for fileIndex = 0:1:99
    i = fileIndex+1;
    FileName = strcat('bentvort_2');
    prefixFilename = strcat( prefixName,FileName,'_P_',int2str(poly),'_scaling_0.034_3D_i_',int2str(fileIndex),'_R_',int2str(R),'_DLISAC');    
   
    
   % SsDynfname = strcat(prefixFilename,'_pS_sDyn.txt');


   
    %Dyn LSIAC scaling Vorticity.
    %uxsDynfname = strcat(prefixFilename,'_pUx_sDyn.txt');
    uysDynfname = strcat(prefixFilename,'_pUy_sDyn.txt');
    uzsDynfname = strcat(prefixFilename,'_pUz_sDyn.txt');
    vxsDynfname = strcat(prefixFilename,'_pVx_sDyn.txt');
    %vysDynfname = strcat(prefixFilename,'_pVy_sDyn.txt');
    vzsDynfname = strcat(prefixFilename,'_pVz_sDyn.txt');
    wxsDynfname = strcat(prefixFilename,'_pWx_sDyn.txt');
    wysDynfname = strcat(prefixFilename,'_pWy_sDyn.txt');
    %wzsDynfname = strcat(prefixFilename,'_pWz_sDyn.txt');
    
    %LSIAC Scaling voriticty.
    %uxsfname = strcat(prefixFilename,'_pUx_s.txt');
    uysfname = strcat(prefixFilename,'_pUy_s.txt');
    uzsfname = strcat(prefixFilename,'_pUz_s.txt');
    vxsfname = strcat(prefixFilename,'_pVx_s.txt');
    %vysfname = strcat(prefixFilename,'_pVy_s.txt');
    vzsfname = strcat(prefixFilename,'_pVz_s.txt');
    wxsfname = strcat(prefixFilename,'_pWx_s.txt');
    wysfname = strcat(prefixFilename,'_pWy_s.txt');
    %wzsfname = strcat(prefixFilename,'_pWz_s.txt');
    
    %proj scaling Vorticity.
    %uxpfname = strcat(prefixFilename,'_pUx_p.txt');
    uypfname = strcat(prefixFilename,'_pUy_p.txt');
    uzpfname = strcat(prefixFilename,'_pUz_p.txt');
    vxpfname = strcat(prefixFilename,'_pVx_p.txt');
    %vypfname = strcat(prefixFilename,'_pVy_p.txt');
    vzpfname = strcat(prefixFilename,'_pVz_p.txt');
    wxpfname = strcat(prefixFilename,'_pWx_p.txt');
    wypfname = strcat(prefixFilename,'_pWy_p.txt');
    %wzpfname = strcat(prefixFilename,'_pWz_p.txt');
    
    
    
    
%    uxspos = textread(uxsDynfname, '%f\t');
    uysDynpos = textread(uysDynfname, '%f\t');
    uzsDynpos = textread(uzsDynfname, '%f\t');
    vxsDynpos = textread(vxsDynfname, '%f\t');
%   vysDynpos = textread(vysDynfname, '%f\t');
    vzsDynpos = textread(vzsDynfname, '%f\t');
    wxsDynpos = textread(wxsDynfname, '%f\t');
    wysDynpos = textread(wysDynfname, '%f\t');
%    wzsDynpos = textread(wzsDynfname, '%f\t');
%    disp(uxspos);
    uysDynVpos = reshape(uysDynpos,[R,R]);
    uzsDynVpos = reshape(uzsDynpos,[R,R]);   
    vxsDynVpos = reshape(vxsDynpos,[R,R]);
    vzsDynVpos = reshape(vzsDynpos,[R,R]);
    wxsDynVpos = reshape(wxsDynpos,[R,R]);
    wysDynVpos = reshape(wysDynpos,[R,R]);

    
%    uxspos = textread(uxsfname, '%f\t');
    uyspos = textread(uysfname, '%f\t');
    uzspos = textread(uzsfname, '%f\t');
    vxspos = textread(vxsfname, '%f\t');
%   vysDynpos = textread(vysfname, '%f\t');
    vzspos = textread(vzsfname, '%f\t');
    wxspos = textread(wxsfname, '%f\t');
    wyspos = textread(wysfname, '%f\t');
%    wzsDynpos = textread(wzsfname, '%f\t');

%    disp(uxspos);
    uysVpos = reshape(uyspos,[R,R]);
    uzsVpos = reshape(uzspos,[R,R]);   
    vxsVpos = reshape(vxspos,[R,R]);
    vzsVpos = reshape(vzspos,[R,R]);
    wxsVpos = reshape(wxspos,[R,R]);
    wysVpos = reshape(wyspos,[R,R]);    
    
%    uxppos = textread(uxpfname, '%f\t');
    uyppos = textread(uypfname, '%f\t');
    uzppos = textread(uzpfname, '%f\t');
    vxppos = textread(vxpfname, '%f\t');
%   vyppos = textread(vypfname, '%f\t');
    vzppos = textread(vzpfname, '%f\t');
    wxppos = textread(wxpfname, '%f\t');
    wyppos = textread(wypfname, '%f\t');
%    wzppos = textread(wzpfname, '%f\t');
    uypVpos = reshape(uyppos,[R,R]);
    uzpVpos = reshape(uzppos,[R,R]);   
    vxpVpos = reshape(vxppos,[R,R]);
    vzpVpos = reshape(vzppos,[R,R]);
    wxpVpos = reshape(wxppos,[R,R]);
    wypVpos = reshape(wyppos,[R,R]);
    
    %actual read
    disp(fileIndex);
    %SsDynpos = textread(SsDynfname,'%f\t',10000);
   % SsDynVpos = reshape(SsDynpos,[R,R]);
   
   VorVpos_sDyn = ((wysDynVpos-uzsDynVpos).^2 + (uzsDynVpos - wxsDynVpos).^2 + (vxsDynVpos - uysDynVpos).^2).^(1/2);
   VorVpos_s = ((wysVpos-uzsVpos).^2 + (uzsVpos - wxsVpos).^2 + (vxsVpos - uysVpos).^2).^(1/2);
   VorVpos_p = ((wypVpos-uzpVpos).^2 + (uzpVpos - wxpVpos).^2 + (vxpVpos - uypVpos).^2).^(1/2);
   
%    figure(1)
%    surf(VorVpos_sDyn);
%    
%    figure(2)
%    surf(VorVpos_s);
%    
%    figure(3)
%    surf(VorVpos_p); 
   
   vor3D_sDyn(i,:,:) = VorVpos_sDyn;
   vor3D_s(i,:,:) = VorVpos_s;
   vor3D_p(i,:,:) = VorVpos_p;
end
%%
isovalue = 14;
%isovalue = 10;
va = 34.1000;
vb = 6.8;

isocolour = [58/256,231/256,19/256];

figure(6)
p = patch(isosurface(vor3D_p,isovalue));
isonormals(vor3D_p,p)
p.FaceColor = isocolour;
p.EdgeColor = 'none';
daspect([1 1 1])
view([va,vb]); 
axis([1 100 1 100 1 100]);
camlight 
lighting gouraud
axis off;
line( [1;1], [50;50],[1,100], 'color','k','LineWidth',1.4)
line( [100;100],[50;50], [1,100], 'color','k','LineWidth',1.4)
line([1;100], [50;50], [1,1], 'color','k','LineWidth',1.4)
line( [1;100], [50;50],[100,100], 'color','k','LineWidth',1.4)
hcs = contourslice(vor3D_p, [],50, [], [isovalue isovalue]) ;
set(hcs,'EdgeColor','r');
set(hcs,'linewidth',2);
set(hcs,'linestyle','--');
print(strcat('images/',FileName,'_P_',int2str(poly),'_3D_dG'),'-dpng','-r200');


figure(5)
p = patch(isosurface(vor3D_s,isovalue));
isonormals(vor3D_s,p)
p.FaceColor = isocolour;
p.EdgeColor = 'none';
daspect([1 1 1])
view([va,vb]); 
axis([1 100 1 100 1 100]);
camlight 
lighting gouraud
axis off;
line( [1;1], [50;50],[1,100], 'color','k','LineWidth',1.4)
line( [100;100],[50;50], [1,100], 'color','k','LineWidth',1.4)
line([1;100], [50;50], [1,1], 'color','k','LineWidth',1.4)
line( [1;100], [50;50],[100,100], 'color','k','LineWidth',1.4)
hcs = contourslice(vor3D_s, [],50, [], [isovalue isovalue]) ;
set(hcs,'EdgeColor','r');
set(hcs,'linewidth',2);
set(hcs,'linestyle','--');
print(strcat('images/',FileName,'_P_',int2str(poly),'_3D_LSIAC'),'-dpng','-r200');

figure(4)
p = patch(isosurface(vor3D_sDyn,isovalue));
isonormals(vor3D_sDyn,p)
p.FaceColor = isocolour;
p.EdgeColor = 'none';
daspect([1 1 1])
view([va,vb]); 
axis([1 100 1 100 1 100]);
camlight 
lighting gouraud
axis off;
line( [1;1], [50;50],[1,100], 'color','k','LineWidth',1.4)
line( [100;100],[50;50], [1,100], 'color','k','LineWidth',1.4)
line([1;100], [50;50], [1,1], 'color','k','LineWidth',1.4)
line( [1;100], [50;50],[100,100], 'color','k','LineWidth',1.4)
hcs = contourslice(vor3D_sDyn, [],50, [], [isovalue isovalue]) ;
set(hcs,'EdgeColor','r');
set(hcs,'linewidth',2);
set(hcs,'linestyle','--');
print(strcat('images/',FileName,'_P_',int2str(poly),'_3D_A_LSIAC'),'-dpng','-r200');

%%
%isovalue = 25;
isovalue = 14
figure(10),
hcs = contourslice(vor3D_p, [],50, [], [isovalue isovalue]) ; hold on;
set(hcs,'EdgeColor','r');
hcs = contourslice(vor3D_s, [],50, [], [isovalue isovalue]) ; hold on;
set(hcs,'EdgeColor','g');
hcs = contourslice(vor3D_sDyn, [],50, [], [isovalue isovalue]) ; hold on;
set(hcs,'EdgeColor','b');
%%
figure(11);
%slice(vor3D_p, [],50, []); hold on;
shading interp;
contourslice(vor3D_p, [],50, [] ) ; hold on;
hcs = contourslice(vor3D_p, [],50, [], [isovalue isovalue]) ; hold on;
set(hcs,'EdgeColor','r');
set(hcs,'linewidth',2);
axis off;
view([0,0]);
caxis([0,50]);
%set(hcs,'linestyle','--');

figure(12)
slice(vor3D_s, [],50, []); hold on;
shading interp;
contourslice(vor3D_s, [],50, [] ) ; hold on;
hcs = contourslice(vor3D_s, [],50, [], [isovalue isovalue]) ; hold on;
caxis([0,50]);
set(hcs,'EdgeColor','r');
set(hcs,'linewidth',2);
axis off;
view([0,0]);



figure(13)
slice(vor3D_sDyn, [],50, []); hold on;
shading interp;
contourslice(vor3D_sDyn, [],50, [] ) ; hold on;
hcs = contourslice(vor3D_sDyn, [],50, [], [isovalue isovalue]) ; hold on;
set(hcs,'EdgeColor','r');
set(hcs,'linewidth',2);
axis off;
view([0,0]);
caxis([0,50]);
