% testNaca_RUre.cpp evaluate.
% testNaca_RVre.cpp evaluate.
% testNaca_RWre.cpp evaluate.

clear;
close all;

UsebkupData = 'cur_feb12/'

%UsebkupData = ''
%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05';
filenameB = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ACRFDBVar' );
filenameC = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ACRFDVar' );
filenameN = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ACRFDNVar' );

filepathB = strcat('data/',UsebkupData,filenameB);
filepathC = strcat('data/',UsebkupData,filenameC);
filepathN = strcat('data/',UsebkupData,filenameN);


xfname =  strcat(filepathB,'_pX.txt' );
yfname =  strcat(filepathB,'_pY.txt' );
zfname =  strcat(filepathB,'_pZ.txt' );
efname =  strcat(filepathB,'_pE.txt' );

UB_sfname = strcat(filepathB,'_pU_s.txt' );
UB_ssfname = strcat(filepathB,'_pU_ss.txt' );
VB_sfname = strcat(filepathB,'_pV_s.txt' );
VB_ssfname = strcat(filepathB,'_pV_ss.txt' );
WB_sfname = strcat(filepathB,'_pW_s.txt' );
WB_ssfname = strcat(filepathB,'_pW_ss.txt' );

UC_sfname = strcat(filepathC,'_pU_s.txt' );
UC_ssfname = strcat(filepathC,'_pU_ss.txt' );
VC_sfname = strcat(filepathC,'_pV_s.txt' );
VC_ssfname = strcat(filepathC,'_pV_ss.txt' );
WC_sfname = strcat(filepathC,'_pW_s.txt' );
WC_ssfname = strcat(filepathC,'_pW_ss.txt' );

UN_sfname = strcat(filepathN,'_pU_s.txt' );
UN_ssfname = strcat(filepathN,'_pU_ss.txt' );
VN_sfname = strcat(filepathN,'_pV_s.txt' );
VN_ssfname = strcat(filepathN,'_pV_ss.txt' );
WN_sfname = strcat(filepathN,'_pW_s.txt' );
WN_ssfname = strcat(filepathN,'_pW_ss.txt' );

xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');

%prespos = textread(presfname, '%f\t');
%presspos = textread(pressfname, '%f\t');

UB_spos = textread(UB_sfname, '%f\t');
UB_sspos = textread(UB_ssfname, '%f\t');
VB_spos = textread(VB_sfname, '%f\t');
VB_sspos = textread(VB_ssfname, '%f\t');
WB_spos = textread(WB_sfname, '%f\t');
WB_sspos = textread(WB_ssfname, '%f\t');

UC_spos = textread(UC_sfname, '%f\t');
UC_sspos = textread(UC_ssfname, '%f\t');
VC_spos = textread(VC_sfname, '%f\t');
VC_sspos = textread(VC_ssfname, '%f\t');
WC_spos = textread(WC_sfname, '%f\t');
WC_sspos = textread(WC_ssfname, '%f\t');

UN_spos = textread(UN_sfname, '%f\t');
UN_sspos = textread(UN_ssfname, '%f\t');
VN_spos = textread(VN_sfname, '%f\t');
VN_sspos = textread(VN_ssfname, '%f\t');
WN_spos = textread(WN_sfname, '%f\t');
WN_sspos = textread(WN_ssfname, '%f\t');




epos = floor(epos +0.0001);
s = floor( (max(size(xpos)))^(1/2) +0.1);
    
%xVpos = reshape(xpos,s,s,s);
%yVpos = reshape(ypos,s,s,s);
%zVpos = reshape(zpos,s,s,s);
%eVpos = reshape(epos,s,s,s);

    xVpos = reshape(xpos,s,s);
%    xVpos = permute(xVpos,[2,3,1]);
    yVpos = reshape(ypos,s,s);
%    yVpos = permute(yVpos,[2,3,1]);
    zVpos = reshape(zpos,s,s);
%    zVpos = permute(zVpos,[2,3,1]);


UB_sVpos = reshape (UB_spos,s,s);
UB_ssVpos = reshape (UB_sspos,s,s);
VB_sVpos = reshape (VB_spos,s,s);
VB_ssVpos = reshape (VB_sspos,s,s);
WB_sVpos = reshape (WB_spos,s,s);
WB_ssVpos = reshape (WB_sspos,s,s);

UC_sVpos = reshape (UC_spos,s,s);
UC_ssVpos = reshape (UC_sspos,s,s);
VC_sVpos = reshape (VC_spos,s,s);
VC_ssVpos = reshape (VC_sspos,s,s);
WC_sVpos = reshape (WC_spos,s,s);
WC_ssVpos = reshape (WC_sspos,s,s);

UN_sVpos = reshape (UN_spos,s,s);
UN_ssVpos = reshape (UN_sspos,s,s);
VN_sVpos = reshape (VN_spos,s,s);
VN_ssVpos = reshape (VN_sspos,s,s);
WN_sVpos = reshape (WN_spos,s,s);
WN_ssVpos = reshape (WN_sspos,s,s);


% figure(1);
% subplot(1,3,1),surf(yVpos,zVpos,U_sVpos);
% shading interp;
% subplot(1,3,2),surf(yVpos,zVpos,U_ssVpos);
% shading interp;
% subplot(1,3,3),surf(yVpos,zVpos,U_ssVpos-U_sVpos);
% shading interp;
% 
% figure(2);
% subplot(1,3,1),surf(yVpos,zVpos,V_sVpos);
% shading interp;
% subplot(1,3,2),surf(yVpos,zVpos,V_ssVpos);
% shading interp;
% subplot(1,3,3),surf(yVpos,zVpos,V_ssVpos-V_sVpos);
% shading interp;
% 
% figure(3);
% subplot(1,3,1),surf(yVpos,zVpos,W_sVpos);
% shading interp;
% subplot(1,3,2),surf(yVpos,zVpos,W_ssVpos);
% shading interp;
% subplot(1,3,3),surf(yVpos,zVpos,W_ssVpos-W_sVpos);
% shading interp;





figure(4);
subplot(1,3,1),contour(yVpos,zVpos,UN_sVpos);
subplot(1,3,2),contour(yVpos,zVpos,UN_ssVpos);
subplot(1,3,3),contour(yVpos,zVpos,UN_ssVpos-UN_sVpos);

figure(5);
subplot(1,3,1),contour(yVpos,zVpos,VN_sVpos);
subplot(1,3,2),contour(yVpos,zVpos,VN_ssVpos);
subplot(1,3,3),contour(yVpos,zVpos,VN_ssVpos-VN_sVpos);

figure(6);
subplot(1,3,1),contour(yVpos,zVpos,WN_sVpos);
subplot(1,3,2),contour(yVpos,zVpos,WN_ssVpos);
subplot(1,3,3),contour(yVpos,zVpos,WN_ssVpos-WN_sVpos);


%% single LSIAC Data
% UX_ssVpos = (UN_sVpos - UB_sVpos)./0.004;
% [UY_ssVpos,UZ_ssVpos] = gradient(UC_sVpos,yVpos(1,2) - yVpos(1,1),zVpos(2,1) - zVpos(1,1));
% 
% VX_ssVpos = (VN_sVpos - VB_sVpos)./0.004;
% [VY_ssVpos,VZ_ssVpos] = gradient(VC_sVpos,yVpos(1,2) - yVpos(1,1),zVpos(2,1) - zVpos(1,1));
% 
% 
% WX_ssVpos = (WN_sVpos - WB_sVpos)./0.004;
% [WY_ssVpos,WZ_ssVpos] = gradient(WC_sVpos,yVpos(1,2) - yVpos(1,1),zVpos(2,1) - zVpos(1,1));

%% Double LSIAC Data
UX_ssVpos = (UN_ssVpos - UC_ssVpos)./0.002;
[UY_ssVpos,UZ_ssVpos] = gradient(UC_ssVpos,yVpos(1,2) - yVpos(1,1),zVpos(2,1) - zVpos(1,1));

VX_ssVpos = (VN_ssVpos - VC_ssVpos)./0.002;
[VY_ssVpos,VZ_ssVpos] = gradient(VC_ssVpos,yVpos(1,2) - yVpos(1,1),zVpos(2,1) - zVpos(1,1));


WX_ssVpos = (WN_ssVpos - WC_ssVpos)./0.002;
[WY_ssVpos,WZ_ssVpos] = gradient(WC_ssVpos,yVpos(1,2) - yVpos(1,1),zVpos(2,1) - zVpos(1,1));


%% save variables after getting here.
%save('UVW_FiniteDiff')


%%
figure(7);
subplot(4,3,1),contour(yVpos,zVpos,UC_ssVpos); grid off; axis off;
title('u(2dir-LSIAC FD)');  
subplot(4,3,2),contour(yVpos,zVpos,VC_ssVpos);grid off; axis off;
title('v(2dir-LSIAC FD)');  
subplot(4,3,3),contour(yVpos,zVpos,WC_ssVpos);grid off; axis off;
title('w(2dir-LSIAC FD)');  

subplot(4,3,4),contour(yVpos,zVpos,UX_ssVpos);grid off; axis off;
title('u_x(2dir-LSIAC FD)');  
subplot(4,3,5),contour(yVpos,zVpos,VX_ssVpos);grid off; axis off;
title('v_x(2dir-LSIAC FD)');  
subplot(4,3,6),contour(yVpos,zVpos,WX_ssVpos);grid off; axis off;
title('w_x(2dir-LSIAC FD)');  

%figure(8);
subplot(4,3,7),contour(yVpos,zVpos,UY_ssVpos);grid off; axis off;
title('u_y(2dir-LSIAC FD)');  
subplot(4,3,8),contour(yVpos,zVpos,VY_ssVpos);grid off; axis off;
title('v_y(2dir-LSIAC FD)');  
subplot(4,3,9),contour(yVpos,zVpos,WY_ssVpos);grid off; axis off;
title('w_y(2dir-LSIAC FD)');  

%figure(9);
subplot(4,3,10),contour(yVpos,zVpos,UZ_ssVpos);grid off; axis off;
title('u_z(2dir-LSIAC FD)');  
subplot(4,3,11),contour(yVpos,zVpos,VZ_ssVpos);grid off; axis off;
title('v_z(2dir-LSIAC FD)');  
subplot(4,3,12),contour(yVpos,zVpos,WZ_ssVpos);grid off; axis off;
title('w_z(2dir-LSIAC FD)');  
%print('images/Feb22/Countour_2dir_FD_AllVar','-dpng', '-r500')
%%
figure(8);
subplot(4,3,1),surf(yVpos,zVpos,UC_ssVpos),shading interp,view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); axis off; grid off;
title('u(2dir-LSIAC FD)');  
subplot(4,3,2),surf(yVpos,zVpos,VC_ssVpos),shading interp,view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); axis off; grid off;
title('v(2dir-LSIAC FD)');  
subplot(4,3,3),surf(yVpos,zVpos,WC_ssVpos),shading interp,view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); axis off; grid off;
title('w(2dir-LSIAC FD)');  

subplot(4,3,4),surf(yVpos,zVpos,UX_ssVpos),shading interp,view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); axis off; grid off;
title('u_x(2dir-LSIAC FD)');  
subplot(4,3,5),surf(yVpos,zVpos,VX_ssVpos),shading interp,view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); axis off; grid off;
title('v_x(2dir-LSIAC FD)');  
subplot(4,3,6),surf(yVpos,zVpos,WX_ssVpos),shading interp,view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); axis off; grid off;
title('w_x(2dir-LSIAC FD)');  

%figure(11);
subplot(4,3,7),surf(yVpos,zVpos,UY_ssVpos),shading interp,view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); axis off; grid off;
title('u_y(2dir-LSIAC FD)');  
subplot(4,3,8),surf(yVpos,zVpos,VY_ssVpos),shading interp,view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); axis off; grid off;
title('v_y(2dir-LSIAC FD)');  
subplot(4,3,9),surf(yVpos,zVpos,WY_ssVpos),shading interp,view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); axis off; grid off;
title('w_y(2dir-LSIAC FD)');  

%figure(12);
subplot(4,3,10),surf(yVpos,zVpos,UZ_ssVpos),shading interp,view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); axis off; grid off;
title('u_z(2dir-LSIAC FD)');  
subplot(4,3,11),surf(yVpos,zVpos,VZ_ssVpos),shading interp,view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); axis off; grid off;
title('v_z(2dir-LSIAC FD)');  
subplot(4,3,12),surf(yVpos,zVpos,WZ_ssVpos),shading interp,view(0,90),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),colorbar(); axis off; grid off;
title('w_z(2dir-LSIAC FD)');  
%print('images/Feb22/Surf_2dir_FD_AllVar','-dpng', '-r500')
%%
% caculate curvature from  above values.


uspos = UC_ssVpos(:);   vspos = VC_ssVpos(:); wspos = WC_ssVpos(:);


uxspos = UX_ssVpos(:); uyspos = UY_ssVpos(:); uzspos = UZ_ssVpos(:);
vxspos = VX_ssVpos(:); vyspos = VY_ssVpos(:); vzspos = VZ_ssVpos(:);
wxspos = WX_ssVpos(:); wyspos = WY_ssVpos(:); wzspos = WZ_ssVpos(:);


% calculate acceleration.
auspos = uxspos.*uspos + uyspos.*vspos + uzspos.*wspos;
avspos = vxspos.*uspos + vyspos.*vspos + vzspos.*wspos;
awspos = wxspos.*uspos + wyspos.*vspos + wzspos.*wspos;
 %
% veclocity cross acceleration. 
cuspos = vspos.*awspos - wspos.*avspos;
cvspos = wspos.*auspos - uspos.*awspos;
cwspos = uspos.*avspos - vspos.*auspos;

curvature_pos = (cuspos.^(2) + cvspos.^(2) + cwspos.^(2)).^(1/2);
curvature_Vpos = reshape(curvature_pos,s,s);

figure(10)
surf(yVpos,zVpos,curvature_Vpos);
shading interp;
%%
figure(11)
contour3(yVpos,zVpos,curvature_Vpos,20,'LineColor','k'); hold on;
surf(yVpos,zVpos,curvature_Vpos), shading interp;
%grid off;
%axis off;
colorbar();
view(0,90);
%print('images/Feb22/Curvature_2dirLSIAC_FD_contour','-dpng', '-r500')
view(100,-14);
%print('images/Feb22/Curvature_2dirLSIAC_FD_contour_AngleView','-dpng', '-r500')

