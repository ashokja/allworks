function Val = elemeval(coeff,pts)
    switch nargin
        case 0
            coeff= [1,1,1];
            pts = (-1:.1:1);
    end  
%     addpath('../../PolyLib');
    Deg = max(size(coeff))-1;
    Val = zeros(size(pts));
    for d = 0:Deg
       Val = Val+coeff(d+1).*JacobiPoly(d,pts,0,0); 
    end

end