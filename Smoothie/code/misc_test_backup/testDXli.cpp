#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "OneSidedSIAC.h"
#include "SmoothieSIAC1D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh1D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

 
int main(int argc, char* argv[])
{

	cout << "OneSided filter debuging XLI filter" << endl;
	for (NekDouble shift = -2.0; shift <=+2.0 ; shift += 0.25)
	{
		OneSidedSIAC oneSIAC(2, OneSidedSIAC::OneSidedFilterType::Der_XLi_SIAC_2kp2,atoi(argv[1]));
		cout << "**********" << shift << "***********" << endl;
		oneSIAC.EvaluateCoefficients(shift);
		cout << "Matrix num rows() "<<oneSIAC.m_knotMatrix.size() << endl;
		for (int i = 0 ; i < oneSIAC.m_knotMatrix.size() ;i++)
		{
			oneSIAC.printNekArray(oneSIAC.m_knotMatrix[i],0);	
		}
		cout << "**********coeffs for: " << shift << "***********" << endl;
		oneSIAC.printNekArray(oneSIAC.m_coeffs,0);
	
	int parts = 1601;
	Array<OneD,NekDouble> x_pos(parts), v_pos(parts);
	for (int i =0; i < parts; i++)
	{
		x_pos[i] =  -8.0+ ( (NekDouble)i/(NekDouble)(parts-1.0))*16.0;
	}

	oneSIAC.EvaluateFilter(x_pos,v_pos,1,-2,true );
	
	oneSIAC.writeNekArray(x_pos,"xpos.txt");
	oneSIAC.writeNekArray(v_pos,"vpos.txt");
	}
	
/*
	int parts = 801;
	Array<OneD,NekDouble> x_pos(parts), v_pos(parts);
	for (int i =0; i < parts; i++)
	{
		x_pos[i] =  -4.0+ ( (NekDouble)i/(NekDouble)(parts-1.0))*8.0;
	}
	oneSIAC.EvaluateFilter(x_pos,v_pos,1,2.0,true );
	
	oneSIAC.writeNekArray(x_pos,"xpos.txt");
	oneSIAC.writeNekArray(v_pos,"vpos.txt");

	cout << "*****************Xlifilter***************" << endl;
	oneSIAC.printNekArray(v_pos,0);
	cout << "*****************GeneralBSplines***************" << endl;

	OneSidedSIAC oneSIAC2(3, OneSidedSIAC::OneSidedFilterType::XLi_SIAC_2kp2);
*/	
/*
	oneSIAC2.EvaluateCoefficients(+3.5);
	//oneSIAC2.EvaluateCoefficients(+0.1);

	cout << "Matrix num rows() "<<oneSIAC2.m_knotMatrix.size() << endl;
	for (int i = 0 ; i < oneSIAC2.m_knotMatrix.size() ;i++)
	{
		oneSIAC2.printNekArray(oneSIAC2.m_knotMatrix[i],0);	
	}
	oneSIAC2.printNekArray(oneSIAC2.m_coeffs,0);	
*/

/*	GeneralBSplines genbsp(2);
	NekDouble tmp[] = { -1.0, 0.0, 1.0 };
	std::vector<NekDouble> kvec( tmp, tmp+3 );	
	genbsp.EvaluateBSplines( x_pos, kvec,0, v_pos, 0.0,1.0);
	genbsp.printNekArray(v_pos,0);
*/
/*
	Array<OneD,NekDouble> karr(3 );
	karr[0] = -1.0; karr[1] =0.0; karr[2]=1.0;	
	genbsp.EvaluateBSplines( x_pos, karr,0, v_pos, 0.0,1.0);
	genbsp.printNekArray(v_pos,0);
*/
/*
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM1D = new HandleNekMesh1D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM1D->LoadData(vSession->GetSessionName()+".fld", var);
	Array<OneD,NekDouble> direction(3,0.0);

	int tNquadPts = HNM1D->m_expansions[0]->GetTotPoints();
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM1D->m_expansions[0]->GetCoordim(0) )
	{
		case 1:
			cout << "opps did not plan for this" << endl;
			break;
		case 2:
			HNM1D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, fceD, eceD, sceD;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	fceD = Array<OneD,NekDouble>(tNquadPts);
	sceD = Array<OneD,NekDouble>(tNquadPts);
	eceD = Array<OneD,NekDouble>(tNquadPts);
	LibUtilities::EquationSharedPtr ffunc
					= vSession->GetFunction("ExactSolution", 0); 
	ffunc->Evaluate(xc0,xc1,xc2, fce);

	LibUtilities::EquationSharedPtr ffuncD
					= vSession->GetFunction("ExactDerSolution", 0); 
	ffuncD->Evaluate(xc0,xc1,xc2, fceD);
	
	HNM1D->m_expansions[0]->FwdTrans(fce,HNM1D->m_expansions[0]->UpdateCoeffs());
	HNM1D->m_expansions[0]->BwdTrans( HNM1D->m_expansions[0]->GetCoeffs(),
										HNM1D->m_expansions[0]->UpdatePhys());
	ece = HNM1D->m_expansions[0]->GetPhys();
	HNM1D->m_expansions[0]->PhysDeriv( ece, eceD);	
*/

/*
	//Sym filter Test.
		cout << "second case" << endl;
	SymmetricSIAC symSIAC(2, SymmetricSIAC::SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC , 1);
		cout << "second case" << endl;
	cout << symSIAC.m_nBSpl << endl;	
	// -3:.1:3
	int parts = 61;
	Array<OneD,NekDouble> x_pos(parts), v_pos(parts);
	for (int i =0; i < parts; i++)
	{
		x_pos[i] =  -3.0+( (NekDouble)i/(NekDouble)(parts-1.0))*6.0;
	}

	symSIAC.EvaluateFilter(x_pos,v_pos,1.0,0.0,true );

	NektarBaseClass k;
	k.printNekArray( x_pos,0);	
	k.printNekArray( v_pos,0);	
	//k.printGraphArray(v_pos,-2,2,.01);	
	k.writeNekArray(x_pos,"test_xpos.txt");	
	k.writeNekArray(v_pos,"test_vpos.txt");	
*/
//	SymmetricSIAC symSIAC2(2, SymmetricSIAC::SymFilterType::CUSTOM_Derivative_SIAC , 1);

	//One SidedFilterTest.
//	OneSidedSIAC oneSIAC(2, OneSidedSIAC::OneSidedFilterType::Der_SMOOTH_BASIC_SIAC_2kp1 , 1);
//		cout << "second case" << endl;
//	OneSidedSIAC oneSIAC2(2, OneSidedSIAC::OneSidedFilterType::Der_BASIC_SIAC_2kp1 , 1);

//	NektarBaseClass k;
//	k.printNekArray( symSIAC.m_coeffs,0);	
/*	
	SmoothieSIAC1D sm(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM1D, 2, 1.0,1); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();

	for (int i =0; i <tNquadPts;i++)
	{
		sm.EvaluateAt(xc0[i],xc1[i],xc2[i],sce[i],valY,valZ);

		//cout << "\tf:\t" << abs(fceD[i]-eceD[i]) ;
		//cout << "\ts:\t:" << abs(fceD[i] -sce[i]);
		cout << "\tf:\t" << fceD[i] ;
		cout << "\te:\t:" << eceD[i];
		cout << "\ts:\t:" << sce[i];
		cout << endl;
	}

	NektarBaseClass k;
	k.printNekArray(xc0,0);
	k.printNekArray(fce,0);
	k.printNekArray(ece,0);
	k.printNekArray(eceD,0);
	k.printNekArray(sce,0);
	k.writeNekArray(xc0,"xc0_Der2kp1.txt");
	k.writeNekArray(fce,"fce_Der2kp1.txt");
	k.writeNekArray(fceD,"fceD_Der2kp1.txt");
	k.writeNekArray(ece,"ece_Der2kp1.txt");
	k.writeNekArray(eceD,"eceD_Der2kp1.txt");
	k.writeNekArray(sce,"sce_Der2kp1.txt");
	return 0;
*/	
}

void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
