clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20];

abc = 'data/triangleMesh_O1_I2_2_S_0.1_R_100_pP_2D.txt ';

R = 1000;
poly =3;
order = poly;
count =1;
scaling = '0.1';
prefixFilename = 'data/P3_tri_I1_O4_';

xfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pX_2D.txt' );
yfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pY_2D.txt' );
ffname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pV_2D.txt' );
pfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pP_2D.txt' );
sfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pS_2D.txt' );

xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
fpos = textread(ffname, '%f\t');
ppos = textread(pfname, '%f\t');
spos = textread(sfname, '%f\t');

xVpos = reshape(xpos,[R,R]);
yVpos = reshape(ypos,[R,R]);
fVpos = reshape(fpos,[R,R]);
pVpos = reshape(ppos,[R,R]);
sVpos = reshape(spos,[R,R]);


figure(1),
surf(xVpos,yVpos,fVpos);

figure(2),
surf(xVpos,yVpos,pVpos);

figure(3),
surf(xVpos,yVpos,sVpos);


figure(4),
surf(xVpos,yVpos,abs(fVpos-pVpos));
title('projection errors');
figure(5),
surf(xVpos,yVpos,abs(fVpos-sVpos));
title('SIAC errors');

figure(6),
contourf(xVpos,yVpos,log10(abs(fVpos-pVpos)));
title('projection errors'); colorbar(); caxis([-10,0]);
view(0,90);
figure(7),
contourf(xVpos,yVpos,log10(abs(fVpos-sVpos)));
title('SIAC errors'); colorbar(); caxis([-10,0]);
view(0,90);

disp('Proj');
disp(sum(sum((abs(fVpos-pVpos)))));
disp(max(max((abs(fVpos-pVpos)))));
disp('SIAC');
disp(sum(sum((abs(fVpos-sVpos)))));
disp(max(max((abs(fVpos-sVpos)))));

% ffname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_fce_OneSided2kp1.txt' );
% efname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_ece_OneSided2kp1.txt' );
% sfname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_sce_OneSided2kp1.txt' );
% 
% 
% for r = rates
%     
%     xfname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_xc0_OneSided2kp1.txt' );
%     ffname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_fce_OneSided2kp1.txt' );
%     efname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_ece_OneSided2kp1.txt' );
%     sfname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_sce_OneSided2kp1.txt' );
% 
%     disp(xfname);
%     xpos = textread(xfname, '%f\t');
%     fval = textread(ffname, '%f\t');
%     pval = textread(efname, '%f\t');
%     sval = textread(sfname, '%f\t');
% 
%     figure(1)
%     plot(xpos,log10(abs(fval-pval)),xpos,log10(abs(fval-sval)));
%     
%     figure(2)
%     plot(xpos,log10(abs(fval-sval)),'DisplayName',strcat('No of ele:',int2str(r)));
%     hold on;
% 
%     figure(3)
%     plot(xpos,pval);
%     
%     p_avg_err(count) = sum(abs(fval-pval))/max(size(xpos));
%     s_avg_err(count) = sum(abs(fval-sval))/max(size(xpos));
%     
%     p_max_err(count) = max(abs(fval-pval));
%     s_max_err(count) = max(abs(fval-sval));
%     count = count+1;
% end
% 
% figure(1),
% legend('show');
% figure(2),
% legend('show');
% title(strcat('poly-',int2str(poly),' order-',int2str(order)));
% xlabel('domain');
% ylabel('Cos(2*pi*x)');
% saveas(2,strcat('S2O2poly_',int2str(poly),'_order_',int2str(order)),'jpg');
% 
% % figure(3)
% % plot(log10(rates),log10(p_avg_err),log10(rates),log10(s_avg_err));
% % figure(4)
% plot(log10(rates),log10(p_max_err),log10(rates),log10(s_max_err));