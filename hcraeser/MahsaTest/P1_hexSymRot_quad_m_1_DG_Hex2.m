% P1
% quadrature cal error.
% Hex2 siac filter.
% hex union jack.
% DG

clear;
line0= '\caption{DG solution, m = 1.0, HL = L, Hex_sym, Hex(2) SIAC filter.}';
% Test errors.
Deg=1;
ord1 = 15;
ord2 = 29;
ord3 = 57;
ord4 = 113;

L2BH2_ord1 = 1.91e-2;
L2BH2_ord2 = 3.40e-3;
L2BH2_ord3 = 7.26e-4;
L2BH2_ord4 = 1.73e-4;

LinfBH2_ord1 = 5.53e-2;
LinfBH2_ord2 = 1.15e-2;
LinfBH2_ord3 = 2.56e-3;
LinfBH2_ord4 = 6.92e-4;

L2H2_ord1 = 1.81e-4;
L2H2_ord2 = 3.14e-3;
L2H2_ord3 = 6.72e-4;
L2H2_ord4 = 1.62e-4;

LinfH2_ord1 = 4.52e-2;
LinfH2_ord2 = 8.93e-3;
LinfH2_ord3 = 1.87e-3;
LinfH2_ord4 = 4.24e-4;

orderH2_L2(1) = 0;
orderH2_L2(2) = log(L2H2_ord2/L2H2_ord1)/log(ord1/ord2);
orderH2_L2(3) = log(L2H2_ord3/L2H2_ord2)/log(ord2/ord3);
orderH2_L2(4) = log(L2H2_ord4/L2H2_ord3)/log(ord3/ord4);

orderH2_Linf(1) = 0;
orderH2_Linf(2) = log(LinfH2_ord2/LinfH2_ord1)/log(ord1/ord2);
orderH2_Linf(3) = log(LinfH2_ord3/LinfH2_ord2)/log(ord2/ord3);
orderH2_Linf(4) = log(LinfH2_ord4/LinfH2_ord3)/log(ord3/ord4);

orderH2_L2
orderH2_Linf

% write code to print 

line0= '\caption{DG solution, m = 1.0, HL = 2L, HB= 2B, Hex Union Jack, Cartesian SIAC filter.}';
line1= strcat('\label{table:4}',...
 '\begin{tabular}{||c| c| c| c| c| c| c | c||}',...
' \hline ');
line2 = ' Mesh Ele & Deg &L2(Before) & L2(After SIAC) & order& Linf(Before) & Linf(After SIAC) & order \\ [0.5ex] ';
line3 = ' \hline\hline ';
% line 1. 
line4 = strcat('$',num2str(ord1),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2BH2_ord1,'%1.2e'), '$ & $',...
    num2str(L2H2_ord1,'%1.2e'), '$ & $',...
    num2str(orderH2_L2(1)), '$ & $',...
    num2str(LinfBH2_ord1,'%1.2e'), '$ & $',...
    num2str(LinfH2_ord1,'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(1)), '$ ',...
    '\\');
line5 = ' \hline ';
% line 2.
line6 = strcat('$',num2str(ord2),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2BH2_ord2,'%1.2e'), '$ & $',...
    num2str(L2H2_ord2,'%1.2e'), '$ & $',...
    num2str(orderH2_L2(2)), '$ & $',...
    num2str(LinfBH2_ord2,'%1.2e'), '$ & $',...
    num2str(LinfH2_ord2,'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(2)), '$ ',...
    '\\');
line7 = ' \hline ';

line8 = strcat('$',num2str(ord3),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2BH2_ord3,'%1.2e'), '$ & $',...
    num2str(L2H2_ord3,'%1.2e'), '$ & $',...
    num2str(orderH2_L2(3)), '$ & $',...
    num2str(LinfBH2_ord3,'%1.2e'), '$ & $',...
    num2str(LinfH2_ord3,'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(3)), '$ ',...
    '\\');
line9 = ' \hline ';

line10 = strcat('$',num2str(ord4),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2BH2_ord4,'%1.2e'), '$ & $',...
    num2str(L2H2_ord4,'%1.2e'), '$ & $',...
    num2str(orderH2_L2(4)), '$ & $',...
    num2str(LinfBH2_ord4,'%1.2e'), '$ & $',...
    num2str(LinfH2_ord4,'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(4)), '$ ',...
    '\\');


line11= ' \hline \end{tabular} ';


fileID = fopen('Del_P2.txt','w');
fwrite(fileID,line0);
fwrite(fileID,line1);
fwrite(fileID,line2);
fwrite(fileID,line3);
fwrite(fileID,line4);
fwrite(fileID,line5);
fwrite(fileID,line6);
fwrite(fileID,line7);
fwrite(fileID,line8);
fwrite(fileID,line9);
fwrite(fileID,line10);
fwrite(fileID,line11);
fclose(fileID);
