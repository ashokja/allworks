% demo to work with.
clear;
%close all;

% SIAC filter.
x_eval = linspace(-6,6,501);
h = 1.0;
k =1;
lambda = -0.0;

a =0;
b = 20;

x = 20;
h = 1;

boundary = 0; %0 -sym; -1 -left;1-Right.
%Determine if the left boundary or right boundary are necessary.
if (x-a)/h < (3*k+1)/2
    disp('considered to be in left boundary');
    l =-1;
elseif (b-x)/h < (3*k+1)/2
    disp('considered to be in right boundary');
    l =1;
else
    disp('inside symmetric boundary');
    l=0;
end

%Determine the coefficient of matrix.

L = k+1;
R = 2*k;
J = k+2;
I = 2*k+1;
if l<0 %left boudary
    I = I+1;
    T = zeros(I,J);
    lambda = (x-a)/h;
    for i = 1:I-1
        for j = 1:J
            T(i,j) = -L -R +i+j -2+lambda;
        end
    end
    T(I,1) = lambda-1;
    T(I,2:end) = lambda;
elseif l>0
    I = I+1;
    T = zeros(I,J);
    lambda = (x-b)/h;
    for i = 2:I
        for j = 1:J
            T(i,j) = i+j -3+lambda;
        end
    end
    T(1,1:end-1) = lambda;
    T(1,end) = lambda+1;
else
    T = zeros(I,J);
    for i = 1:I
        for j = 1:J
            T(i,j) = -L/2+i+j-k -2;
        end
    end
end


figure(1)
x_viz = -10:.01:10;
for i = 1:I
    knots = T(i,:);
    knots
    v_viz = BSpl(x_viz,k,0,knots);
    plot(x_viz,v_viz), hold on;
end
% hold off;


coeff = SIAC_coeff_XLi(k,T,lambda)

figure(2)
x_viz = -10:.01:10;
v_viz = zeros(size(x_viz));
for i = 1:I
    knots = T(i,:);
    v_viz = v_viz + coeff(i).*BSpl(x_viz,k,0,knots);
end
plot(x_viz,v_viz); hold on;
%hold off;









% 
% for i = 1:2*k+1
%     for j = 1:J
%         T(i,j) = -L/2+i+j-k -2;
%     end
% end

% figure(1)
% subplot(4,1,1),plot(x_eval,SIAC_filter(x_eval,h,k,lambda));
% 
% 
% x_eval = linspace(-6,6,501);
% h = 1;
% k =1;
% lambda = 0;
% 
% subplot(4,1,2),plot(x_eval,SIAC_filter(x_eval,h,k,lambda));
% 
% addpath ('../SIAC');
% subplot(4,1,3),plot(x_eval,SIAC(x_eval,h,k));
% 
% 
% subplot(4,1,4),plot(x_eval,SIAC_filter_VAN(x_eval,h,k,lambda,4*k));
% 
% figure(2)
% plot(x_eval,SIAC_filter_VAN(x_eval,h,k,lambda,4*k));
