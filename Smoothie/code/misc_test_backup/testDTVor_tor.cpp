#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"

#include <iomanip>


using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void calAcceleration(int nq, 
	const Array<OneD,NekDouble> u_phys, const Array<OneD,NekDouble> v_phys, const Array<OneD,NekDouble> w_phys,
    const Array<OneD,NekDouble> udx_phys, const Array<OneD,NekDouble> udy_phys, const Array<OneD,NekDouble> udz_phys,
    const Array<OneD,NekDouble> vdx_phys, const Array<OneD,NekDouble> vdy_phys, const Array<OneD,NekDouble> vdz_phys,
    const Array<OneD,NekDouble> wdx_phys, const Array<OneD,NekDouble> wdy_phys, const Array<OneD,NekDouble> wdz_phys,
    Array<OneD,NekDouble> au_phys, Array<OneD,NekDouble> av_phys,  Array<OneD,NekDouble> aw_phys)
{
    Array<OneD,NekDouble> temp1(nq), temp2(nq), temp3(nq);
    // a acceleration
    Vmath::Vmul(nq, udx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, udy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, udz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, au_phys,1);
    Vmath::Vadd(nq, temp3, 1,au_phys,1, au_phys,1);
   
    Vmath::Vmul(nq, vdx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, vdy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, vdz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, av_phys,1);
    Vmath::Vadd(nq, temp3, 1,av_phys,1, av_phys,1);
    
	Vmath::Vmul(nq, wdx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, wdy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, wdz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, aw_phys,1);
    Vmath::Vadd(nq, temp3, 1,aw_phys,1, aw_phys,1);
 /*       
		// jv_x component.
        Vmath::Vmul(Nspls,u_dx,1,phys_u,1,d1,1);
        Vmath::Vmul(Nspls,u_dy,1,phys_v,1,d2,1);
        Vmath::Vadd(Nspls,d1,1,d2,1,d3,1);
        Vmath::Vmul(Nspls,u_dz,1,phys_w,1,d1,1);
        Vmath::Vadd(Nspls,d3,1,d1,1,jv_x,1);
        // jv_y component.
        Vmath::Vmul(Nspls,v_dx,1,phys_u,1,d1,1);
        Vmath::Vmul(Nspls,v_dy,1,phys_v,1,d2,1);
        Vmath::Vadd(Nspls,d1,1,d2,1,d3,1);
        Vmath::Vmul(Nspls,v_dz,1,phys_w,1,d1,1);
        Vmath::Vadd(Nspls,d3,1,d1,1,jv_y,1);
        // jv_z component.
        Vmath::Vmul(Nspls,w_dx,1,phys_u,1,d1,1);
        Vmath::Vmul(Nspls,w_dy,1,phys_v,1,d2,1);
        Vmath::Vadd(Nspls,d1,1,d2,1,d3,1);
        Vmath::Vmul(Nspls,w_dz,1,phys_w,1,d1,1);
        Vmath::Vadd(Nspls,d3,1,d1,1,jv_z,1);
*/
}

void calculateB( int nq,
	const Array<OneD,NekDouble> u_phys, const Array<OneD,NekDouble> v_phys, const Array<OneD,NekDouble> w_phys,
    const Array<OneD,NekDouble> au_x, const Array<OneD,NekDouble> au_y, const Array<OneD,NekDouble> au_z,
    const Array<OneD,NekDouble> av_x, const Array<OneD,NekDouble> av_y, const Array<OneD,NekDouble> av_z,
    const Array<OneD,NekDouble> aw_x, const Array<OneD,NekDouble> aw_y, const Array<OneD,NekDouble> aw_z,
    Array<OneD,NekDouble> bu_phys, Array<OneD,NekDouble> bv_phys,  Array<OneD,NekDouble> bw_phys)
{
	calAcceleration( nq, u_phys, v_phys, w_phys,
					au_x, au_y, au_z,
					av_x, av_y, av_z,
					aw_x, aw_y, aw_z,
					bu_phys, bv_phys, bw_phys);
}

void calTorsion( int nq,
	const Array<OneD,NekDouble> u_phys, const Array<OneD,NekDouble> v_phys, const Array<OneD,NekDouble> w_phys,
    const Array<OneD,NekDouble> au, const Array<OneD,NekDouble> av, const Array<OneD,NekDouble> aw,
    const Array<OneD,NekDouble> bu, const Array<OneD,NekDouble> bv, const Array<OneD,NekDouble> bw,
    Array<OneD,NekDouble> tor)
{
    
	Array<OneD,NekDouble> temp1(nq), temp2(nq), temp3(nq);
	Array<OneD,NekDouble> vCa_x(nq), vCa_y(nq), vCa_z(nq);

	// vCa = (v x a)
    Vmath::Vmul(nq, v_phys,1,aw,1, temp1,1);
    Vmath::Vmul(nq, w_phys,1,av,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_x,1);

    Vmath::Vmul(nq, w_phys,1,au,1, temp1,1);
    Vmath::Vmul(nq, u_phys,1,aw,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_y,1);

    Vmath::Vmul(nq, u_phys,1,av,1, temp1,1);
    Vmath::Vmul(nq, v_phys,1,au,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_z,1);

	// vCadb = (v x a).b
	Vmath::Vmul(nq, vCa_x,1, bu, 1, temp1, 1);
	Vmath::Vmul(nq, vCa_y,1, bv, 1, temp2, 1);
	Vmath::Vmul(nq, vCa_z,1, bw, 1, temp3, 1);
	Vmath::Vadd(nq, temp1,1,temp2,1,temp1,1);
	Vmath::Vadd(nq, temp1,1,temp3,1,tor,1);
}


void DoVmathForBV( int Nspls,
                   const Array<OneD,NekDouble> phys_u,
                   const Array<OneD,NekDouble> phys_v,
                   const Array<OneD,NekDouble> phys_w,
                   const Array<OneD,NekDouble> bx,
                   const Array<OneD,NekDouble> by,
                   const Array<OneD,NekDouble> bz,
                   Array<OneD,NekDouble> Dbv )
{

   Array<OneD,NekDouble> d1(Nspls),d2(Nspls),d3(Nspls),nB(Nspls),nV(Nspls);;
        Vmath::Vmul(Nspls, phys_u,1,phys_u,1,d1,1);
        Vmath::Vmul(Nspls, phys_v,1,phys_v,1,d2,1);
    Vmath::Vadd(Nspls, d1,1,d2,1,d1,1);
        Vmath::Vmul(Nspls, phys_w,1,phys_w,1,d2,1);
    Vmath::Vadd(Nspls, d1,1,d2,1,nV,1);
        // Need a trick to make sure nV does not have any zero.
        Vmath::Vsqrt(Nspls,nV,1,nV,1);
        NekDouble vmin = Vmath::Vmin(Nspls,nV,1);
    if (vmin < 0.0000001)
    {
        //Vmath::Zero(Nspls,Dbv,1);
        cout<< "Some velocity are zero. "<< endl;

        for (int i =0 ; i < Nspls ; i++)
        {
            if (nV[i] < 0.0001)
            {
                nV[i] = 1.0;
            }
        }
    }
        Vmath::Vmul(Nspls, bx,1,bx,1,d1,1);
        Vmath::Vmul(Nspls, by,1,by,1,d2,1);
    Vmath::Vadd(Nspls, d1,1,d2,1,d1,1);
        Vmath::Vmul(Nspls, bz,1,bz,1,d2,1);
    Vmath::Vadd(Nspls, d1,1,d2,1,nB,1);
        Vmath::Vsqrt(Nspls,nB,1,nB,1);
    NekDouble bmin = Vmath::Vmin(Nspls,nB,1);
    if (bmin < 0.0000001)
    {
        //Vmath::Zero(Nspls,Dbv,1);
        cout<< "Some B's are zero. "<< endl;

        for (int i =0 ; i < Nspls ; i++)
        {
            if (nB[i] < 0.0001)
            {
                nB[i] = 1.0;
            }
        }
    }

    Vmath::Vdiv(Nspls, phys_u,1,nV,1,d1,1);
    Vmath::Vdiv(Nspls, bx,1,nB,1,d2,1);
    Vmath::Vmul(Nspls, d1,1,d2,1,Dbv,1);

    Vmath::Vdiv(Nspls, phys_v,1,nV,1,d1,1);
    Vmath::Vdiv(Nspls, by,1,nB,1,d2,1);
    Vmath::Vmul(Nspls, d1,1,d2,1,d3,1);
    Vmath::Vadd(Nspls, d3,1,Dbv,1,Dbv,1);

    Vmath::Vdiv(Nspls, phys_w,1,nV,1,d1,1);
    Vmath::Vdiv(Nspls, bz,1,nB,1,d2,1);
    Vmath::Vmul(Nspls, d1,1,d2,1,d3,1);
    Vmath::Vadd(Nspls, d3,1,Dbv,1,Dbv,1);

    int Num_nans = Vmath::Nnan(Nspls, Dbv,1);
    assert (0 == Num_nans && "DBV computed Nan. Check for errors");
}




void calCurvature(int nq, 
	Array<OneD,NekDouble> &u_phys, Array<OneD,NekDouble> &v_phys,Array<OneD,NekDouble> &w_phys,
    Array<OneD,NekDouble> &udx_phys, Array<OneD,NekDouble> &udy_phys, Array<OneD,NekDouble> &udz_phys,
    Array<OneD,NekDouble> &vdx_phys, Array<OneD,NekDouble> &vdy_phys, Array<OneD,NekDouble> &vdz_phys,
    Array<OneD,NekDouble> &wdx_phys, Array<OneD,NekDouble> &wdy_phys, Array<OneD,NekDouble> &wdz_phys,
    Array<OneD,NekDouble> &au_phys, Array<OneD,NekDouble> &av_phys,  Array<OneD,NekDouble> &aw_phys,
    Array<OneD,NekDouble> &vCa_x, Array<OneD,NekDouble> &vCa_y, Array<OneD,NekDouble> &vCa_z,
	Array<OneD,NekDouble> &curNorm2 )
{
    Array<OneD,NekDouble> temp1(nq), temp2(nq), temp3(nq);
    // a acceleration
    Vmath::Vmul(nq, udx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, udy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, udz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, au_phys,1);
    Vmath::Vadd(nq, temp3, 1,au_phys,1, au_phys,1);
   
    Vmath::Vmul(nq, vdx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, vdy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, vdz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, av_phys,1);
    Vmath::Vadd(nq, temp3, 1,av_phys,1, av_phys,1);

    Vmath::Vmul(nq, wdx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, wdy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, wdz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, aw_phys,1);
    Vmath::Vadd(nq, temp3, 1,av_phys,1, aw_phys,1);


    // cross v a  = curvature.
    Vmath::Vmul(nq, v_phys,1,aw_phys,1, temp1,1);
    Vmath::Vmul(nq, w_phys,1,av_phys,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_x,1);

    Vmath::Vmul(nq, w_phys,1,au_phys,1, temp1,1);
    Vmath::Vmul(nq, u_phys,1,aw_phys,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_y,1);

    Vmath::Vmul(nq, u_phys,1,av_phys,1, temp1,1);
    Vmath::Vmul(nq, v_phys,1,au_phys,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_z,1);

	//Curvature norm
	Vmath::Vmul(nq, vCa_x,1,vCa_x,1, temp1, 1);
	Vmath::Vmul(nq, vCa_y,1,vCa_y,1, temp2, 1);
	Vmath::Vmul(nq, vCa_z,1,vCa_z,1, temp3, 1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, curNorm2,1);
    Vmath::Vadd(nq, temp3, 1,curNorm2,1, curNorm2,1);

    return;
}





void calVorticity(int nq,
    Array<OneD,NekDouble> &udx_phys, Array<OneD,NekDouble> &udy_phys, Array<OneD,NekDouble> &udz_phys,
    Array<OneD,NekDouble> &vdx_phys, Array<OneD,NekDouble> &vdy_phys, Array<OneD,NekDouble> &vdz_phys,
    Array<OneD,NekDouble> &wdx_phys, Array<OneD,NekDouble> &wdy_phys, Array<OneD,NekDouble> &wdz_phys,
    Array<OneD,NekDouble> &Vx,Array<OneD,NekDouble> &Vy,Array<OneD,NekDouble> &Vz )
{
    Vmath::Vsub(nq, wdy_phys,1, vdz_phys,1,Vx ,1);
    Vmath::Vsub(nq, udz_phys,1, wdx_phys,1,Vy ,1);
    Vmath::Vsub(nq, vdx_phys,1, udy_phys,1,Vz ,1);
    return;
}

int main(int argc, char* argv[])
{
	if (argc != 7)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg 0/1"<<endl<<"\t 0 for writing data"<< endl<<"\t 1 for reading data." << endl;
		cout << "3nd arg polynomial degree filter you want to apply" << endl;
		cout << "4rd arg meshscaling you want to use." << endl;
		cout << "5th arg output resolution." << endl;
		cout << "6th arg value w to be attached to filename." << endl;
	//	cout << "4th Angle of SIAC derivative in deg -90 to 90 allowed" << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();

//	HNM3D->LoadData( fname+ ".chk",var);

	HNM3D->LoadMesh(var[0]);
	HNM3D->LoadMesh(var[1]);
	HNM3D->LoadMesh(var[2]);

	HNM3D->LoadExpListIntoRTree();
	
	int tNquadPts = HNM3D->m_expansions[0]->GetTotPoints();
	int tNCoeffs= HNM3D->m_expansions[0]->GetNcoeffs();
	cout << "fPhys:" << tNquadPts<< endl;
	cout << "fCoeffs:" << HNM3D->m_expansions[0]->GetNcoeffs()<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);
	Array<OneD,NekDouble> u(tNquadPts);
	Array<OneD,NekDouble> v(tNquadPts);
	Array<OneD,NekDouble> w(tNquadPts);

	
	switch( HNM3D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
	NektarBaseClass NBC;	

	if( 0 == atoi(argv[2]) )
	{
		NBC.writeNekArray(xc0,fname+"_xc0."+"txt");
		NBC.writeNekArray(xc1,fname+"_xc1."+"txt");
		NBC.writeNekArray(xc2,fname+"_xc2."+"txt");
		return 0;
	}
	else
	{
		NBC.readNekArray(u, fname+"_u.txt");
		NBC.readNekArray(v, fname+"_v.txt");
		NBC.readNekArray(w, fname+"_w.txt");
		//k.printNekArray(v,0);
	}

	// Now use Newton-Rapson to find the zero u and zero v.
	
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( u,HNM3D->m_expansions[0]->UpdateCoeffs());
	HNM3D->m_expansions[0]->BwdTrans_IterPerExp( HNM3D->m_expansions[0]->GetCoeffs(),
			HNM3D->m_expansions[0]->UpdatePhys());
	
	HNM3D->m_expansions[1]->FwdTrans_IterPerExp( v,HNM3D->m_expansions[1]->UpdateCoeffs());
	HNM3D->m_expansions[1]->BwdTrans_IterPerExp( HNM3D->m_expansions[1]->GetCoeffs(),
			HNM3D->m_expansions[1]->UpdatePhys());
	
	HNM3D->m_expansions[2]->FwdTrans_IterPerExp( w,HNM3D->m_expansions[2]->UpdateCoeffs());
	HNM3D->m_expansions[2]->BwdTrans_IterPerExp( HNM3D->m_expansions[2]->GetCoeffs(),
			HNM3D->m_expansions[2]->UpdatePhys());
/*
	Array<OneD,NekDouble> phys_u = HNM3D->m_expansions[0]->GetPhys();	
	Array<OneD,NekDouble> phys_v = HNM3D->m_expansions[1]->GetPhys();
	Array<OneD,NekDouble> phys_w = HNM3D->m_expansions[2]->GetPhys();
 	int Nspls = tNquadPts; 

        Array<OneD,NekDouble> curN_x( Nspls), curN_y( Nspls), curN_z( Nspls);
        Array<OneD,NekDouble> torN( Nspls), curN( Nspls);
        Array<OneD,NekDouble> jv_x( Nspls), jv_y( Nspls), jv_z( Nspls);
        Array<OneD,NekDouble> ax_dx( Nspls), ax_dy( Nspls), ax_dz( Nspls);
        Array<OneD,NekDouble> ay_dx( Nspls), ay_dy( Nspls), ay_dz( Nspls);
        Array<OneD,NekDouble> az_dx( Nspls), az_dy( Nspls), az_dz( Nspls);
        Array<OneD,NekDouble> u_dx( Nspls), u_dy( Nspls), u_dz( Nspls);
        Array<OneD,NekDouble> v_dx( Nspls), v_dy( Nspls), v_dz( Nspls);
        Array<OneD,NekDouble> w_dx( Nspls), w_dy( Nspls), w_dz( Nspls);
        Array<OneD,NekDouble> bN_x( Nspls), bN_y( Nspls), bN_z( Nspls);
        Array<OneD,NekDouble> d1( Nspls), d2( Nspls), d3( Nspls);
            // Physical derivative.
		HNM3D->m_expansions[0]->PhysDeriv( phys_u, u_dx, u_dy, u_dz);
        HNM3D->m_expansions[0]->PhysDeriv( phys_v, v_dx, v_dy, v_dz);
        HNM3D->m_expansions[0]->PhysDeriv( phys_w, w_dx, w_dy, w_dz);
        // jv_x component.
        Vmath::Vmul(Nspls,u_dx,1,phys_u,1,d1,1);
        Vmath::Vmul(Nspls,u_dy,1,phys_v,1,d2,1);
        Vmath::Vadd(Nspls,d1,1,d2,1,d3,1);
        Vmath::Vmul(Nspls,u_dz,1,phys_w,1,d1,1);
        Vmath::Vadd(Nspls,d3,1,d1,1,jv_x,1);
        // jv_y component.
        Vmath::Vmul(Nspls,v_dx,1,phys_u,1,d1,1);
        Vmath::Vmul(Nspls,v_dy,1,phys_v,1,d2,1);
        Vmath::Vadd(Nspls,d1,1,d2,1,d3,1);
        Vmath::Vmul(Nspls,v_dz,1,phys_w,1,d1,1);
        Vmath::Vadd(Nspls,d3,1,d1,1,jv_y,1);
        // jv_z component.
        Vmath::Vmul(Nspls,w_dx,1,phys_u,1,d1,1);
        Vmath::Vmul(Nspls,w_dy,1,phys_v,1,d2,1);
        Vmath::Vadd(Nspls,d1,1,d2,1,d3,1);
        Vmath::Vmul(Nspls,w_dz,1,phys_w,1,d1,1);
        Vmath::Vadd(Nspls,d3,1,d1,1,jv_z,1);

        HNM3D->m_expansions[0]->PhysDeriv( jv_x, ax_dx, ax_dy, ax_dz);
        HNM3D->m_expansions[0]->PhysDeriv( jv_y, ay_dx, ay_dy, ay_dz);
        HNM3D->m_expansions[0]->PhysDeriv( jv_z, az_dx, az_dy, az_dz);
        // bN_x component.
        Vmath::Vmul(Nspls,ax_dx,1,phys_u,1,d1,1);
        Vmath::Vmul(Nspls,ax_dy,1,phys_v,1,d2,1);
        Vmath::Vadd(Nspls,d1,1,d2,1,d3,1);
        Vmath::Vmul(Nspls,ax_dz,1,phys_w,1,d1,1);
        Vmath::Vadd(Nspls,d3,1,d1,1,bN_x,1);
        // bN_y component.
        Vmath::Vmul(Nspls,ay_dx,1,phys_u,1,d1,1);
        Vmath::Vmul(Nspls,ay_dy,1,phys_v,1,d2,1);
        Vmath::Vadd(Nspls,d1,1,d2,1,d3,1);
        Vmath::Vmul(Nspls,ay_dz,1,phys_w,1,d1,1);
        Vmath::Vadd(Nspls,d3,1,d1,1,bN_y,1);
        // bN_z component.
        Vmath::Vmul(Nspls,az_dx,1,phys_u,1,d1,1);
        Vmath::Vmul(Nspls,az_dy,1,phys_v,1,d2,1);
        Vmath::Vadd(Nspls,d1,1,d2,1,d3,1);
        Vmath::Vmul(Nspls,az_dz,1,phys_w,1,d1,1);
        Vmath::Vadd(Nspls,d3,1,d1,1,bN_z,1);
        // curNu = cross(v,a)
        Vmath::Vmul(Nspls, phys_v,1, jv_z,1, d1,1);
        Vmath::Vmul(Nspls, phys_w,1, jv_y,1, d2,1);
        Vmath::Vsub(Nspls, d1,1, d2,1, curN_x,1);
        Vmath::Vmul(Nspls, phys_w,1, jv_x,1, d1,1);
        Vmath::Vmul(Nspls, phys_u,1, jv_z,1, d2,1);
        Vmath::Vsub(Nspls, d1,1, d2,1, curN_y,1);
        Vmath::Vmul(Nspls, phys_u,1, jv_y,1, d1,1);
        Vmath::Vmul(Nspls, phys_v,1, jv_x,1, d2,1);
        Vmath::Vsub(Nspls, d1,1, d2,1, curN_z,1);
        // curN^2 = curN_x^2+curN_y^2+curN_z^2 
        Vmath::Vmul(Nspls, curN_x,1, curN_x,1, d1,1);
        Vmath::Vmul(Nspls, curN_y,1, curN_y,1, d2,1);
        Vmath::Vadd(Nspls, d1,1, d2,1, d3,1);
        Vmath::Vmul(Nspls, curN_z,1, curN_z,1, d1,1);
        Vmath::Vadd(Nspls, d1,1, d3,1, curN,1);
        // Tor = dot(curNu,bN);
        Vmath::Vmul(Nspls, curN_x,1, bN_x,1, d1,1);
        Vmath::Vmul(Nspls, curN_y,1, bN_y,1, d2,1);
        Vmath::Vadd(Nspls, d1,1, d2,1, d3,1);
        Vmath::Vmul(Nspls, curN_z,1, bN_z,1, d1,1);
        Vmath::Vadd(Nspls, d1,1, d3,1, torN,1);
        Array<OneD,NekDouble> Dbv(Nspls);
        DoVmathForBV( Nspls, phys_u, phys_v, phys_w, bN_x, bN_y, bN_z, Dbv );
	
	Array<OneD,NekDouble> Vor_xC(tNCoeffs), Vor_yC(tNCoeffs), Vor_zC(tNCoeffs), curnorm2C(tNCoeffs), DbvC(tNCoeffs);

	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( jv_x, Vor_xC);
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( jv_y, Vor_yC);
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( jv_z, Vor_zC);
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( torN, curnorm2C);
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( Dbv, DbvC);
*/

	Array<OneD,NekDouble> u_DG = HNM3D->m_expansions[0]->GetPhys();	
	Array<OneD,NekDouble> v_DG = HNM3D->m_expansions[1]->GetPhys();
	Array<OneD,NekDouble> w_DG = HNM3D->m_expansions[2]->GetPhys();

	Array<OneD,NekDouble> ux_DG(tNquadPts), uy_DG(tNquadPts), uz_DG(tNquadPts);
	Array<OneD,NekDouble> vx_DG(tNquadPts), vy_DG(tNquadPts), vz_DG(tNquadPts);
	Array<OneD,NekDouble> wx_DG(tNquadPts), wy_DG(tNquadPts), wz_DG(tNquadPts);

	Array<OneD,NekDouble> ax(tNquadPts), ay(tNquadPts), az(tNquadPts);
	Array<OneD,NekDouble> curX(tNquadPts), curY(tNquadPts), curZ(tNquadPts);
	Array<OneD,NekDouble> curNorm2(tNquadPts) ;

	HNM3D->m_expansions[0]->PhysDeriv( u_DG, ux_DG, uy_DG, uz_DG);
	HNM3D->m_expansions[1]->PhysDeriv( v_DG, vx_DG, vy_DG, vz_DG);
	HNM3D->m_expansions[2]->PhysDeriv( w_DG, wx_DG, wy_DG, wz_DG);

	Array<OneD,NekDouble> Vor_x(tNquadPts), Vor_y(tNquadPts), Vor_z(tNquadPts);
	Array<OneD,NekDouble> Vor_xC(tNCoeffs), Vor_yC(tNCoeffs), Vor_zC(tNCoeffs), curnorm2C(tNCoeffs), DbvC(tNCoeffs);
	
//	calVorticity(tNquadPts , ux_DG, uy_DG, uz_DG, vx_DG, vy_DG, vz_DG, wx_DG,wy_DG,wz_DG, Vor_x,Vor_y,Vor_z);

	Array<OneD,NekDouble> au_DG(tNquadPts),av_DG(tNquadPts),aw_DG(tNquadPts),
						vCa_xDG(tNquadPts), vCa_yDG(tNquadPts), vCa_zDG(tNquadPts),
						curNorm2_DG(tNquadPts);
//	calCurvature(tNquadPts, u_DG, v_DG, w_DG,
//							ux_DG, uy_DG, uz_DG,
//							vx_DG, vy_DG, vz_DG,
//							wx_DG, wy_DG, wz_DG,
//							au_DG, av_DG, aw_DG,
//							vCa_xDG,vCa_yDG,vCa_zDG,
//							curNorm2_DG);
	calAcceleration( tNquadPts, u_DG, v_DG, w_DG,
							ux_DG, uy_DG, uz_DG,
							vx_DG, vy_DG, vz_DG,
							wx_DG, wy_DG, wz_DG,
							au_DG, av_DG, aw_DG);						
	
	Array<OneD,NekDouble> au_dx_DG(tNquadPts), au_dy_DG(tNquadPts), au_dz_DG(tNquadPts);
	Array<OneD,NekDouble> av_dx_DG(tNquadPts), av_dy_DG(tNquadPts), av_dz_DG(tNquadPts);
	Array<OneD,NekDouble> aw_dx_DG(tNquadPts), aw_dy_DG(tNquadPts), aw_dz_DG(tNquadPts);
	Array<OneD,NekDouble> bu_DG(tNquadPts),bv_DG(tNquadPts),bw_DG(tNquadPts);
	Array<OneD,NekDouble> tor_DG(tNquadPts), Dbv(tNquadPts);
	// calcualte a , b
	HNM3D->m_expansions[0]->PhysDeriv( au_DG, au_dx_DG, au_dy_DG, au_dz_DG);
	HNM3D->m_expansions[1]->PhysDeriv( av_DG, av_dx_DG, av_dy_DG, av_dz_DG);
	HNM3D->m_expansions[2]->PhysDeriv( aw_DG, aw_dx_DG, aw_dy_DG, aw_dz_DG);

	calculateB( tNquadPts, u_DG, v_DG, w_DG, 
					au_dx_DG, au_dy_DG, au_dz_DG,
					av_dx_DG, av_dy_DG, av_dz_DG,
					aw_dx_DG, aw_dy_DG, aw_dz_DG,
					bu_DG, bv_DG, bw_DG);

	// calculate torsion
	calTorsion( tNquadPts,
					 u_DG, v_DG, w_DG,
					au_DG, av_DG, aw_DG,
					bu_DG, bv_DG, bw_DG,
					tor_DG);

	DoVmathForBV( tNquadPts,
					 u_DG, v_DG, w_DG,
					 bu_DG, bv_DG, bw_DG,
					 Dbv);

	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( au_DG, Vor_xC);
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( av_DG, Vor_yC);
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( aw_DG, Vor_zC);
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( tor_DG, curnorm2C);
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( Dbv, DbvC);
    

// Add more terms using SIAC filter.
	SmoothieSIAC3D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM3D, atoi( argv[3]), atof(argv[4]),1);
	NekDouble scaling = atof(argv[4]), valY, valZ;
	Array<OneD, NekDouble> dirX(3,0.0), dirY(3,0.0), dirZ(3,0.0);
	dirX[0] = 1.0; dirY[1]=1.0; dirZ[2]= 1.0;
	Array<OneD, NekDouble> u_Sx(tNquadPts), u_Sy(tNquadPts), u_Sz(tNquadPts);
	Array<OneD, NekDouble> v_Sx(tNquadPts), v_Sy(tNquadPts), v_Sz(tNquadPts);
	Array<OneD, NekDouble> w_Sx(tNquadPts), w_Sy(tNquadPts), w_Sz(tNquadPts);
	Array<OneD, NekDouble> au_S(tNquadPts), av_S(tNquadPts), aw_S(tNquadPts);
	Array<OneD, NekDouble> au_SC(tNCoeffs), av_SC(tNCoeffs), aw_SC(tNCoeffs);

	for (int i =0; i < tNquadPts; i++)
	{
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], u_Sx[i], valY, valZ, dirX, scaling, 0);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], u_Sy[i], valY, valZ, dirY, scaling, 0);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], u_Sz[i], valY, valZ, dirZ, scaling, 0);

		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], v_Sx[i], valY, valZ, dirX, scaling, 1);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], v_Sy[i], valY, valZ, dirY, scaling, 1);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], v_Sz[i], valY, valZ, dirZ, scaling, 1);
		
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], w_Sx[i], valY, valZ, dirX, scaling, 2);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], w_Sy[i], valY, valZ, dirY, scaling, 2);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], w_Sz[i], valY, valZ, dirZ, scaling, 2);
		cout << i << endl;
	}
	
	
	calAcceleration( tNquadPts, u_DG, v_DG, w_DG,
							u_Sx, u_Sy, u_Sz,
							v_Sx, v_Sy, v_Sz,
							w_Sx, w_Sy, w_Sz,
							au_S, av_S, aw_S);						

	Array<OneD, NekDouble> au_Sx(tNquadPts), au_Sy(tNquadPts), au_Sz(tNquadPts);
	Array<OneD, NekDouble> av_Sx(tNquadPts), av_Sy(tNquadPts), av_Sz(tNquadPts);
	Array<OneD, NekDouble> aw_Sx(tNquadPts), aw_Sy(tNquadPts), aw_Sz(tNquadPts);
	Array<OneD, NekDouble> bu_S(tNquadPts), bv_S(tNquadPts), bw_S(tNquadPts);
	Array<OneD, NekDouble> tor_S(tNquadPts), Dbv_S(tNquadPts);
	Array<OneD, NekDouble> tor_SC(tNCoeffs), Dbv_SC(tNCoeffs);

	HNM3D->m_expansions[0]->SetPhys(au_S);
	HNM3D->m_expansions[1]->SetPhys(av_S);
	HNM3D->m_expansions[2]->SetPhys(aw_S);
	
	// calculate SIAC acceleration derivatives.
	for ( int i =0; i < tNquadPts; i++)
	{
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], au_Sx[i], valY, valZ, dirX, scaling, 0);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], au_Sy[i], valY, valZ, dirY, scaling, 0);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], au_Sz[i], valY, valZ, dirZ, scaling, 0);

		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], av_Sx[i], valY, valZ, dirX, scaling, 1);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], av_Sy[i], valY, valZ, dirY, scaling, 1);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], av_Sz[i], valY, valZ, dirZ, scaling, 1);
		
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], aw_Sx[i], valY, valZ, dirX, scaling, 2);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], aw_Sy[i], valY, valZ, dirY, scaling, 2);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], aw_Sz[i], valY, valZ, dirZ, scaling, 2);
		cout << i << endl;
	}

	calculateB( tNquadPts, u_DG, v_DG, w_DG, 
					au_Sx, au_Sy, au_Sz,
					av_Sx, av_Sy, av_Sz,
					aw_Sx, aw_Sy, aw_Sz,
					bu_S, bv_S, bw_S);
	
	calTorsion( tNquadPts,
					 u_DG, v_DG, w_DG,
					au_S, av_S, aw_S,
					bu_S, bv_S, bw_S,
					tor_S);
	
	DoVmathForBV( tNquadPts,
					 u_DG, v_DG, w_DG,
					 bu_S, bv_S, bw_S,
					 Dbv_S);
	
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( au_S, au_SC);
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( av_S, av_SC);
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( aw_S, aw_SC);
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( tor_S, tor_SC);
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( Dbv_S, Dbv_SC);

string out = vSession->GetSessionName() +"R"+argv[5]+"_O_"+argv[3]+"_w_"+argv[6] +".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (HNM3D->m_expansions[0]->GetFieldDefinitions()).at(0);
    FieldDef[0]->m_fields.push_back("u");
    FieldDef[0]->m_fields.push_back("v");
    FieldDef[0]->m_fields.push_back("w");
	FieldDef[0]->m_fields.push_back("CurX");
	FieldDef[0]->m_fields.push_back("CurY");
	FieldDef[0]->m_fields.push_back("CurZ");
	FieldDef[0]->m_fields.push_back("au_S");
	FieldDef[0]->m_fields.push_back("av_S");
	FieldDef[0]->m_fields.push_back("aw_S");
	FieldDef[0]->m_fields.push_back("tor");
	FieldDef[0]->m_fields.push_back("DbvC");
	FieldDef[0]->m_fields.push_back("tor_S");
	FieldDef[0]->m_fields.push_back("Dbv_S");
	HNM3D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0]);
	HNM3D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0]);
	HNM3D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM3D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0], Vor_xC);
    HNM3D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0], Vor_yC);
    HNM3D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0], Vor_zC);
    HNM3D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0], au_SC);
    HNM3D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0], av_SC);
    HNM3D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0], aw_SC);
    HNM3D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0], curnorm2C);
    HNM3D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0], DbvC);
    HNM3D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0], tor_SC);
    HNM3D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0], Dbv_SC);
    HNM3D->m_fld->Write(out, FieldDef, FieldData);

	return 0;
}

