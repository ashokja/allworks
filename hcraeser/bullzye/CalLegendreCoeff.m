function [u] = CalLegendreCoeff(UAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ)
    switch nargin
        case 1,
            NX =5; NY=6; NZ = 7;
            nx=1;ny=1;nz=1;
            [XQ,WXQ] = JacobiGLZW(NX+nx,0,0);
            [YQ,WYQ] = JacobiGLZW(NY+ny,0,0);
            [ZQ,WZQ] = JacobiGLZW(NZ+nz,0,0);
    end
    addpath ../PolyLib
    
    %WXQ,WYQ,XQ,YQ,NX,NY

    
    %Cal Lagrance before itself.
    PhiXQ = zeros(NX+1,max(size(XQ)));
    PhiYQ = zeros(NY+1,max(size(YQ)));
    PhiZQ = zeros(NZ+1,max(size(ZQ)));
    for i =0:NX
         PhiXQ(i+1,:) = LegendrePoly(i,XQ);
    end
    for j = 0:NY
        PhiYQ(j+1,:) = LegendrePoly(j,YQ);
    end
    for k = 0:NZ
        PhiZQ(k+1,:) = LegendrePoly(k,ZQ);
    end
    
    u = zeros(NX+1,NY+1,NZ+1);
    for i = 0:NX
        for j = 0:NY
            for k = 0:NZ
                sol = 0;
                for x = 1:max(size(XQ))
                    for y = 1:max(size(YQ))
                        for z = 1:max(size(ZQ))
%                            sol = sol + func(XQ(x),YQ(y),ZQ(z))*WXQ(x)*WYQ(y)*WZQ(z)*...
                            sol = sol + UAtGrid(x,y,z)*WXQ(x)*WYQ(y)*WZQ(z)*...
                                       PhiXQ(i+1,x)*PhiYQ(j+1,y)*PhiZQ(k+1,z);
                        end
                    end
                end
                u(i+1,j+1,k+1) = (2*i+1)*(2*j+1)*(2*k+1)/8*sol;
            end
        end
    end

end