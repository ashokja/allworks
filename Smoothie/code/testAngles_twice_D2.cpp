#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;
void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

const double pi = std::acos(-1);
int main(int argc, char* argv[])
{
	if (argc != 8)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling1 you want to use." << endl;
		cout << "4th arg Angle1 " << endl;
		cout << "5th arg meshscaling2 you want to use." << endl;
		cout << "6th arg Angle2 " << endl;
		cout << "7th arg Resolution " << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM2D->LoadMesh(var[0]);
	HNM2D->LoadMesh(var[1]);
	HNM2D->LoadMesh(var[2]);
	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);
//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);

	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]));
		//fce[i] = std::cos(2.0*M_PI*(std::sqrt(std::pow((xc0[i]-0.5),2) + std::pow((xc1[i]-0.5),2))) );
		//fce[i] = std::cos(2.0*M_PI*(xc0[i]))*std::cos(2.0*M_PI*(xc1[i]));
	}
	
	HNM2D->m_expansions[0]->FwdTrans(fce,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	 ece = HNM2D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM2D->m_expansions[0]->GetCoeffs();

	HNM2D->m_Arrays.push_back(ece);
	HNM2D->LoadExpListIntoRTree();
// Evaluate on a new equal space grid mesh.
	int gPts = atoi(argv[7]);
	int totPts = gPts*gPts;
	int Nx = gPts, Ny=gPts;
	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0);
	
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]) ); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	Array<OneD,NekDouble> direction(3,0.0), coord(3,0.0);
	direction[1] = 1.0;
	Array<OneD,NekDouble> pX(totPts), pY(totPts), pV(totPts), pP(totPts), pS(totPts);
	
	
	Array<OneD,NekDouble> direction1(3,0.0), direction2(3,0.0);
	direction1[0] = std::cos(atof(argv[4])*pi/180.0);
	direction1[1] = std::sin(atof(argv[4])*pi/180.0);
	direction2[0] = std::cos(atof(argv[6])*pi/180.0);
	direction2[1] = std::sin(atof(argv[6])*pi/180.0);

	NekDouble scaling1, scaling2;
	scaling1 = atof(argv[3]);
	scaling2 = atof(argv[5]);

	std::shared_ptr<SmoothieSIAC> sm1 = std::make_shared<SmoothieSIAC2D> (SIACUtilities::eSYM_2kp1_1SIDED_2kp1,
					HNM2D, atoi(argv[2]), scaling1 );
	std::shared_ptr<SmoothieSIAC> sm2 = std::make_shared<SmoothieSIAC2D> (SIACUtilities::eSYM_2kp1_1SIDED_2kp1,
					HNM2D, atoi(argv[2]), scaling2 );
	vector< std::shared_ptr<SmoothieSIAC> > Sms;
	vector<Array<OneD,NekDouble> > directions;
	vector<NekDouble> scalings;
	vector<int> vars;
	Sms.push_back(sm2);
	Sms.push_back(sm1);
	directions.push_back(direction2);
	directions.push_back(direction1);
	vars.push_back(0);
	vars.push_back(0);
	scalings.push_back(scaling2);
	scalings.push_back(scaling1);

	for( int i =0; i < Nx; i++)
	{
		for(int j=0; j< Ny; j++)
		{
			int index = i*Nx+j;
			pX[index] = i*sx;
			pY[index] = j*sy;
			pV[index] = std::cos(2.0*M_PI*(pX[index]+ pY[index] ) ) ;
			//pV[index] = std::cos(2.0*M_PI*(std::sqrt(std::pow((pX[index]-0.5),2) + std::pow((pY[index]-0.5),2) )) );
			//pV[index] = std::cos(2.0*M_PI*(pX[index]))*std::cos(2.0*M_PI*(pY[index]));
//			sm.EvaluateAt(pX[index],pY[index],0.0,pS[index],valY,valZ,direction, atof(argv[3]),0);
			Sms[0]->EvaluateRecursiveAt(pX[index], pY[index], 0.0, pS[index], valY, valZ, Sms,
											directions, scalings, vars,0);

			coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = 0.0;
//			cout << setprecision(19) << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;
			int elid = HNM2D->GetExpansionIndexUsingRTree(coord);
//			cout << elid << endl;
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
			//int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
			const Array<OneD,NekDouble> el_phys = ece.CreateWithOffset(
										ece, coeffOffset);
			pP[index] = lexp->PhysEvaluate(coord,el_phys);
//			cout << j << endl;
		}
		cout << i << endl;
	}

/*
	for (int i =0; i <tNquadPts;i++)
	{
		sm.EvaluateAt(xc0[i],xc1[i],xc2[i],sce[i],valY,valZ,direction);

		//cout << "\tf:\t" << abs(fce[i]-ece[i]) ;
		//cout << "\ts:\t:" << abs(fce[i] -sce[i]);
		//cout << "\ts:\t:" << sce[i];
		//cout << endl;
	}
	Vmath::Vsub(tNquadPts, &ece[0],1, &fce[0],1,&temp0[0],1);
	Vmath::Vabs(tNquadPts, &temp0[0],1, &temp0[0],1); 
	
	HNM2D->m_expansions[1]->FwdTrans(temp0,HNM2D->m_expansions[1]->UpdateCoeffs() );
	HNM2D->m_expansions[1]->BwdTrans( HNM2D->m_expansions[1]->GetCoeffs(),
										HNM2D->m_expansions[1]->UpdatePhys());
	temp0 = HNM2D->m_expansions[1]->GetPhys();

	Vmath::Vsub(tNquadPts, &sce[0],1, &fce[0],1,&temp1[0],1);
	Vmath::Vabs(tNquadPts, &temp1[0],1, &temp1[0],1); 

	HNM2D->m_expansions[2]->FwdTrans(temp1,HNM2D->m_expansions[2]->UpdateCoeffs() );
	HNM2D->m_expansions[2]->BwdTrans( HNM2D->m_expansions[2]->GetCoeffs(),
										HNM2D->m_expansions[2]->UpdatePhys());
	 temp1 = HNM2D->m_expansions[2]->GetPhys();

	k.printNekArray(xc0,0);
	k.printNekArray(fce,0);
	k.printNekArray(ece,0);
	k.printNekArray(sce,0);
	cout << fname+"_"+argv[2]+"_xc0_OneSided2kp1.txt"<< endl;
*/	
	NektarBaseClass k;
	k.writeNekArray(pX,fname+"_P_"+argv[2]+"_S1_"+argv[3]+"_A1_"+argv[4]+"_S2_"+argv[5]+"_A2_"+argv[6]+"_R_"+argv[7]+"_pX.txt");
	k.writeNekArray(pY,fname+"_P_"+argv[2]+"_S1_"+argv[3]+"_A1_"+argv[4]+"_S2_"+argv[5]+"_A2_"+argv[6]+"_R_"+argv[7]+"_pY.txt");
	k.writeNekArray(pV,fname+"_P_"+argv[2]+"_S1_"+argv[3]+"_A1_"+argv[4]+"_S2_"+argv[5]+"_A2_"+argv[6]+"_R_"+argv[7]+"_pV.txt");
	k.writeNekArray(pP,fname+"_P_"+argv[2]+"_S1_"+argv[3]+"_A1_"+argv[4]+"_S2_"+argv[5]+"_A2_"+argv[6]+"_R_"+argv[7]+"_pP.txt");
	k.writeNekArray(pS,fname+"_P_"+argv[2]+"_S1_"+argv[3]+"_A1_"+argv[4]+"_S2_"+argv[5]+"_A2_"+argv[6]+"_R_"+argv[7]+"_pS.txt");

/*
	 string out = vSession->GetSessionName() + ".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (HNM2D->m_expansions[0]->GetFieldDefinitions()).at(0);
    FieldDef[0]->m_fields.push_back("u");
    FieldDef[0]->m_fields.push_back("v");
    FieldDef[0]->m_fields.push_back("w");
    HNM2D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_fld->Write(out, FieldDef, FieldData);
*/
	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
