#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"
#include <time.h>

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		cout << "4th arg Resolution you want to sample at." << endl;
		return 0;
	}
	clock_t timestamp;
	timestamp= clock();

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM3D->LoadMesh(var[0]);
	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM3D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);
	cout << "loading data file " << ((float)(clock()-timestamp))/CLOCKS_PER_SEC << endl;
 	timestamp = clock();

	HNM3D->LoadExpListIntoRTree();
	cout << "loading into rtree "<< ((float)(clock()-timestamp))/CLOCKS_PER_SEC << endl;
 	timestamp = clock();
	
	switch( HNM3D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);
//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);

	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]+xc2[i]));
		//fce[i] = std::cos(2.0*M_PI*(xc0[i]))*std::cos(2.0*M_PI*(xc1[i]));
	}
	
	HNM3D->m_expansions[0]->FwdTrans(fce,HNM3D->m_expansions[0]->UpdateCoeffs() );
	HNM3D->m_expansions[0]->BwdTrans( HNM3D->m_expansions[0]->GetCoeffs(),
										HNM3D->m_expansions[0]->UpdatePhys());
	 ece = HNM3D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM3D->m_expansions[0]->GetCoeffs();

	HNM3D->m_Arrays.push_back(ece);
	
	int gPts = atoi(argv[4]);
	
	SmoothieSIAC3D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]), atof(argv[3]) );
	NekDouble v1=0.0, v2=0.0, v3=0.0;
	// test EvalutaeRecursvieAT.

	int testLevels = 3;
	vector<Array<OneD,NekDouble> > directions;
	vector<NekDouble> scalings;
	vector<int> vars;
	vector<std::shared_ptr<SmoothieSIAC> > Sms;
	for (int i = 0; i < testLevels; i++)
	{
		Array<OneD,NekDouble> direction(3,0.0);
		direction[0] = 1.0; 
		directions.push_back(direction);
		NekDouble scaling = atof(argv[3]);
		scalings.push_back(scaling);
		int var = 0;
		vars.push_back(var);
	} 	

	std::shared_ptr<SmoothieSIAC> sm1 = std::make_shared<SmoothieSIAC3D> (SIACUtilities::eSYM_2kp1_1SIDED_2kp1, 
									HNM3D, atoi(argv[2]), atof(argv[3]) );
	std::shared_ptr<SmoothieSIAC> sm2 = std::make_shared<SmoothieSIAC3D> (SIACUtilities::eSYM_2kp1_1SIDED_2kp1, 
									HNM3D, atoi(argv[2]), atof(argv[3]) );
	std::shared_ptr<SmoothieSIAC> sm3 = std::make_shared<SmoothieSIAC3D> (SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, 
									HNM3D, atoi(argv[2]), atof(argv[3]) ,1);

	Sms.push_back(sm2);
	Sms.push_back(sm2);
	Sms.push_back(sm1);


		Array<OneD,NekDouble> direction(3,0.0);
		direction[0] = 1.0;
	
	NekDouble actVal = std::cos(2.0*M_PI*( 0.421+0.421+0.421 ));
	cout << "ACTU Eval: " << std::cos(2.0*M_PI*( 0.421+0.421+0.421 )) << endl ;
	Array<OneD,NekDouble> gloCoord(3,0.0);
	gloCoord[0] = 0.421;	gloCoord[1] = 0.421;	gloCoord[2] = 0.421;
	int expIndex = HNM3D->m_expansions[0]->GetExpIndex(gloCoord);
	int el_physOffset = HNM3D->m_expansions[0]->GetPhys_Offset(expIndex);
	Array<OneD,NekDouble> el_ece = ece.CreateWithOffset(ece, el_physOffset); 
	cout << "PhysEvaluate: " << HNM3D->m_expansions[0]->GetExp(expIndex)->PhysEvaluate(gloCoord, el_ece) << endl; 
	cout << "PhysEvaluate Er: " << HNM3D->m_expansions[0]->GetExp(expIndex)->PhysEvaluate(gloCoord, el_ece) -actVal<< endl; 

	sm.EvaluateAt(0.421,0.421,0.421,v1,v2,v3, direction, atof(argv[3]), 0); 
	cout << "SIAC Before: " << v1 << endl;
	// Test 1.
	// Single Level.

	//cout << "loading into rtree "<< ((float)(clock()-timestamp))/CLOCKS_PER_SEC << endl;
 	timestamp = clock();
	sm.EvaluateRecursiveAt( 0.421, 0.421, 0.421, v1, v2, v3,Sms, directions, scalings, vars,2); 

	cout << "SIAC Eval Er: " << v1 -actVal<< "\t";
	cout << "Time for Single Eval "<< ((float)(clock()-timestamp))/CLOCKS_PER_SEC << endl;
 	timestamp = clock();

	// Test 2.
	// Two Levels.
	sm.EvaluateRecursiveAt( 0.421, 0.421, 0.421, v1, v2, v3, Sms, directions, scalings, vars,1); 
	cout << "SIAC twice same direction Eval: " << v1 -actVal<< "\t";
	
	cout << "Time for Two Step Same Dir Eval "<< ((float)(clock()-timestamp))/CLOCKS_PER_SEC << endl;
 	timestamp = clock();

	//	different direction in X and Y
	directions[1][0] = 0.0;
	directions[1][1] = 1.0;
	sm.EvaluateRecursiveAt( 0.421, 0.421, 0.421, v1, v2, v3, Sms, directions, scalings, vars,1); 
	cout << "SIAC twice Ydir,Xdir Eval Er: " << v1 -actVal<< "\t";
	
	cout << "Time for Two Step Diff Dir Eval "<< ((float)(clock()-timestamp))/CLOCKS_PER_SEC << endl;
 	timestamp = clock();


	// Test 3.
	// Three Levels.
	directions[0][0] = 0.0;
	directions[0][2] = 1.0;
	sm.EvaluateRecursiveAt( 0.421, 0.421, 0.421, v1, v2, v3, Sms, directions, scalings, vars,0); 
	cout << "SIAC thrice same dir Eval: " << scientific << v1 -actVal << "\t";
	
	cout << "Time for Three Step Diff Dir Eval "<< ((float)(clock()-timestamp))/CLOCKS_PER_SEC << endl;
 	timestamp = clock();

/*
	for (int i =0; i <tNquadPts;i++)
	{
		sm.EvaluateAt(xc0[i],xc1[i],xc2[i],sce[i],valY,valZ,direction);

		//cout << "\tf:\t" << abs(fce[i]-ece[i]) ;
		//cout << "\ts:\t:" << abs(fce[i] -sce[i]);
		//cout << "\ts:\t:" << sce[i];
		//cout << endl;
	}
	Vmath::Vsub(tNquadPts, &ece[0],1, &fce[0],1,&temp0[0],1);
	Vmath::Vabs(tNquadPts, &temp0[0],1, &temp0[0],1); 
	
	HNM3D->m_expansions[1]->FwdTrans(temp0,HNM3D->m_expansions[1]->UpdateCoeffs() );
	HNM3D->m_expansions[1]->BwdTrans( HNM3D->m_expansions[1]->GetCoeffs(),
										HNM3D->m_expansions[1]->UpdatePhys());
	temp0 = HNM3D->m_expansions[1]->GetPhys();

	Vmath::Vsub(tNquadPts, &sce[0],1, &fce[0],1,&temp1[0],1);
	Vmath::Vabs(tNquadPts, &temp1[0],1, &temp1[0],1); 

	HNM3D->m_expansions[2]->FwdTrans(temp1,HNM3D->m_expansions[2]->UpdateCoeffs() );
	HNM3D->m_expansions[2]->BwdTrans( HNM3D->m_expansions[2]->GetCoeffs(),
										HNM3D->m_expansions[2]->UpdatePhys());
	 temp1 = HNM3D->m_expansions[2]->GetPhys();

	k.printNekArray(xc0,0);
	k.printNekArray(fce,0);
	k.printNekArray(ece,0);
	k.printNekArray(sce,0);
	cout << fname+"_"+argv[2]+"_xc0_OneSided2kp1.txt"<< endl;
*/	
/*
	 string out = vSession->GetSessionName() + ".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (HNM3D->m_expansions[0]->GetFieldDefinitions()).at(0);
    FieldDef[0]->m_fields.push_back("u");
    FieldDef[0]->m_fields.push_back("v");
    FieldDef[0]->m_fields.push_back("w");
    HNM3D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM3D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM3D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM3D->m_fld->Write(out, FieldDef, FieldData);
*/
	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
