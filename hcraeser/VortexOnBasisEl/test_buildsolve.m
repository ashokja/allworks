function [dataIn,px_dataIn,py_dataIn,coeff,dataOut,sol_seeds] = test_buildsolve(n1,n2,seedNum,d1,d2,f)
            % created a attachment and dettachment line which are not straight.
    switch nargin
        case 0,
            n1 = 3; n2 = 3;
            seedNum = 10;
            d1 =10
            d2 =10
            f=1
    end
    [x,w1] = lglnodes(d2);
    [y,w2] = lglnodes(d1);

    %x = (-1:2/d1:1);
    %y = (-1:2/d2:1);
    [X,Y] = meshgrid(x,y);

            %Complicated function.
    %px = sin(2*X+(2*Y).^2);
    %py = cos(2*X+(2*Y).^2);

            %Simple Saddle function.
    px = 2*X;
    py = -2*Y;

            % viz
    figure (1),
    subplot(2,2,1),quiver(X,Y,px,py);

            %Created dataIn
    dataIn(:,1) = reshape(X,[],1);
    dataIn(:,2) = reshape(Y,[],1);


            %Need to calcualte coeff.
    count=1;
    for k1 = 0:n1
        for k2 = 0:n2
            mat(:,count) = B2(n1,k1,n2,k2,dataIn);
            count = count +1;
        end
    end
    
    figure (f),
%    subplot(2,2,2),plot3(dataIn(:,1),dataIn(:,2),mat,'*');
%    subplot(2,2,2),mesh(dataIn(:,1),dataIn(:,2),mat);
            %Complicated Function.
    %px_dataIn = sin( 2*dataIn(:,1)+(2*dataIn(:,2)).^2 );
    %py_dataIn = cos( 2*dataIn(:,1)+(2*dataIn(:,2)).^2 );

            %Simple saddle Function
    px_dataIn = 2*dataIn(:,1);
    py_dataIn = -2*dataIn(:,2) ;


    figure (f),
    subplot(2,2,1),quiver(dataIn(:,1),dataIn(:,2),px_dataIn,py_dataIn);

    coeff(:,1) = mat\px_dataIn;
    coeff(:,2) = mat\py_dataIn;

    dataOut = B2_eval(n1,n2,coeff,dataIn);

    figure (f),
    subplot(2,2,4),quiver(dataIn(:,1),dataIn(:,2),dataOut(:,1),dataOut(:,2));


    %Newton solve using few seed points.

    rand('seed',407);
    seeds = (rand(seedNum,2)-0.5)/2;
    sol_seeds = zeros(seedNum,2);
    
    for i = 1:seedNum
        sol_seeds(i,:) = MultiNewtonBasis(n1,n2,coeff,seeds(i,:),30);
    end
    
    figure (f),
    subplot(2,2,3),plot(sol_seeds(:,1),sol_seeds(:,2),'*');
    
        %figure 1 done.

        % solve for zero using Newton method;
        % test statemetns.
            %[app] = B2_der(n1,0,n2,0,dataIn)
            %[appeval] = B2_der_eval(n1,n2,coeff,[0.1,0.1])

        % need to do the solve.

    function x=MultiNewtonBasis(n1,n2,coeff,x,NumIters)
    %Performs multidimensional Newton's method for the function defined in f
    %starting with x and running NumIters times.
        y = B2_eval(n1,n2,coeff,x);
        dy = reshape(B2_der_eval(n1,n2,coeff,x),2,2);
        %[res1,res2] = f_der(
        %dy = ;
        for j=1:NumIters
        s=dy\y';
        x=x-s';
        y = B2_eval(n1,n2,coeff,x);
        dy = reshape(B2_der_eval(n1,n2,coeff,x),2,2);
        end 

    end

    

    function [rest] =  B2(n1,k1,n2,k2,dataIn)
        v1 = B(n1,k1,dataIn(:,1));
        v2 = B(n2,k2,dataIn(:,2));
        rest = v1.*v2;
    end
    
    function [rest] = B2_der(n1,k1,n2,k2,dataIn)
        v1 =     B(n1,k1,dataIn(:,1));
        v1_der = B_der(n1,k1,dataIn(:,1));
        v2 =     B(n2,k2,dataIn(:,2));
        v2_der = B_der(n2,k2,dataIn(:,2));
        
        rest(:,1) = v1_der.*v2;
        rest(:,2) = v1.*v2_der;
    end

    function [rest] = B2_eval(n1,n2,coeff,dataIn)
       count =1;
       rest = zeros(size(dataIn));
       for k1 = 0:n1
           for k2 = 0:n2
               rest(:,1) = rest(:,1)  + coeff(count,1)*B2(n1,k1,n2,k2,dataIn);
               rest(:,2) = rest(:,2)  + coeff(count,2)*B2(n1,k1,n2,k2,dataIn);
               count= count +1;
           end
       end
    end

    function [rest] = B2_der_eval(n1,n2,coeff,dataIn)
       count =1;
       [a,b] = size(dataIn);
       rest = zeros(a,b+2);
       for k1 = 0:n1
           for k2 = 0:n2
               rest(:,1:2) = rest(:,1:2)  + coeff(count,1)*B2_der(n1,k1,n2,k2,dataIn);
               rest(:,3:4) = rest(:,3:4)  + coeff(count,2)*B2_der(n1,k1,n2,k2,dataIn);
               count= count +1;
           end
       end
    end

    function [res] = B(n,k,data)
        % assumed data varies from -1 to 1
        if n<k || k<0
            res = zeros(size(data));
            return
        end
        data = (data+1)/2;
        res = nchoosek(n,k)*((data.^k).*((1-data).^(n-k)));
    end

    function [res] = B_der(n,k,data)
        res = n*(B(n-1,k-1,data) - B(n-1,k,data));
    end
    

end