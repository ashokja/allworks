#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include <boost/math/special_functions/sinc.hpp>
#include <boost/timer.hpp>
#include "MetricTensor.h"
#include <chrono>

typedef std::chrono::high_resolution_clock Clock;


using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 6)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3th arg Resolution of output." << endl;
		cout << "4th arg minScaling" << endl;
		cout << "5th arg maxScaling" << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM2D->LoadMesh(var[0]);
	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);
//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);

	for (int i=0;  i< tNquadPts; i++)
	{
		//fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]));
		fce[i] = std::sin(2.0*M_PI*(xc0[i]+xc1[i]));
		//fce[i] = std::sin(2.0*M_PI*xc0[i])*std::cos(2*M_PI*xc1[i]);
		//fce[i] = std::sin(2.0*M_PI*xc0[i])*std::sin(2*M_PI*xc1[i]);
		//fce[i] = std::cos(2.0*M_PI*xc0[i])*std::cos(2*M_PI*xc1[i]);
	}
	
	HNM2D->m_expansions[0]->FwdTrans(fce,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	 ece = HNM2D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM2D->m_expansions[0]->GetCoeffs();
	HNM2D->m_Arrays.push_back(ece);
	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();
	
	MetricTensor* metricT = new MetricTensor();
	metricT->LoadMetricTensor(HNM2D);
	metricT->CalculateMetricTensorAtNodes();

	NekDouble minx, miny,minz,maxx,maxy,maxz;
	minx = 0.0; miny= 0.0; minz=0.0;
	maxx= 1.0; maxy=1.0; maxz=1.0;
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1, HNM2D, atoi(argv[2]), scaling ); 
	SmoothieSIAC2D sm_NONSYM(SIACUtilities::eSYM_UNEVEN_2kp1, HNM2D, atoi(argv[2]), scaling );
    SmoothieSIAC2D sm_NONSYM_NSK2(SIACUtilities::eSYM_UNEVEN_2kp1, HNM2D, atoi(argv[2]), scaling );
	NekDouble valX,valY,valZ;
    
	NekDouble minLength, maxLength;
    minLength = atof(argv[4]);
    maxLength = atof(argv[5]);
    int Res = atoi(argv[3]);

	int Ny=Res;
	Array<OneD,NekDouble> glCoords(3,0.0),eig1(3,0.0),eig2(3,0.0);
    Array<OneD,NekDouble> pSMetLy(Res,0.0), pScale(Res,0.0),pSMetEr(Res,0.0),pPLy(Res,0.0);
    Array<OneD,NekDouble> pSMetLy2(Res,0.0), pScale2(Res,0.0),pSMetEr2(Res,0.0);
    Array<OneD,NekDouble> pXLy(Res,0.0), pYLy(Res,0.0), pVLy(Res,0.0), pLamb1(Res,0.0), pLamb2(Res,0.0),pValue(Res,0.0);
    NekDouble lambda1, lambda2;

    NekDouble sy = 1.0/(Ny-1.0);
	NektarBaseClass k;
	for (int j=0;j<Res;j+=5)
    {
        int index =j;
        pYLy[index] = 0.25;
        pXLy[index] = j*sy;
		//pVLy[index] = std::cos(2.0*M_PI*(pXLy[index] + pYLy[index]) ); 
		//pVLy[index] = std::cos(2.0*M_PI*(pXLy[index] + pYLy[index]) ); 
		pVLy[index] = std::sin(2.0*M_PI*(pXLy[index] + pYLy[index]) ); 
		//pVLy[index] = std::sin(2.0*M_PI*pXLy[index])*std::sin(2*M_PI*pYLy[index]); 
		//pVLy[index] = std::cos(2.0*M_PI*pXLy[index])*std::cos(2*M_PI*pYLy[index]); 

        glCoords[0] = pXLy[index]; glCoords[1] = pYLy[index];
		
		int elid = HNM2D->GetExpansionIndexUsingRTree(glCoords);
		metricT->GetEigenPair(glCoords,elid,1,lambda1,eig1);
        metricT->GetEigenPair(glCoords,elid,2,lambda2,eig2);
        pLamb1[index] = lambda1;
        pLamb2[index] = lambda2;
	
		cout << j << endl;	

/*
		//	projected value.
       	LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
        int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
        const Array<OneD,NekDouble> el_phys = ece.CreateWithOffset(
                                        ece, coeffOffset);
        pPLy[index] = lexp->PhysEvaluate(glCoords,el_phys);

		//	L-SIAC value.
        sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMetLy[index], valY, valZ, eig1, lambda1,0);
        sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMetLy2[index], valY, valZ, eig2, lambda2,0);
*/

        for (int i=0;i < Res; i++)
        {
            lambda1 = minLength + (maxLength-minLength)*(i/(Res-1.0));
            lambda2 = minLength + (maxLength-minLength)*(i/(Res-1.0));
            pScale[i] = lambda1;
            pScale2[i] = lambda2;
            sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMetLy[i], valY, valZ, eig1, lambda1,0);
            sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMetLy2[i], valY, valZ, eig2, lambda2,0);
            pSMetEr[i] = pSMetLy[i]-pVLy[index];
            pSMetEr2[i] = pSMetLy2[i]-pVLy[index];
        }

        k.writeNekArray(pScale,fname+"_j_"+to_string(j)+"_"+argv[2]+"_Res1_"+argv[3]+"_pScale.txt");
        k.writeNekArray(pSMetLy,fname+"_j_"+to_string(j)+"_"+argv[2]+"_Res1_"+argv[3]+"_pMetLy.txt");
        k.writeNekArray(pSMetEr,fname+"_j_"+to_string(j)+"_"+argv[2]+"_Res1_"+argv[3]+"_pMetEr.txt");

        k.writeNekArray(pScale2,fname+"_j_"+to_string(j)+"_"+argv[2]+"_Res1_"+argv[3]+"_pScale2.txt");
        k.writeNekArray(pSMetLy2,fname+"_j_"+to_string(j)+"_"+argv[2]+"_Res1_"+argv[3]+"_pMetLy2.txt");
        k.writeNekArray(pSMetEr2,fname+"_j_"+to_string(j)+"_"+argv[2]+"_Res1_"+argv[3]+"_pMetEr2.txt");
    }
	// Res6 means cos(2PI(x+Y)) Also just Res
	// Res1 means sin(2PI(x+Y))
	// Res2 means sin(2PI(x))cos(2PIy)
	// Res3 means cos(2PI(x))sin(2PIy)
	// Res4 means sin(2PI(x))sin(2PIy)
	// Res5 means cos(2PI(x))cos(2PIy)

    k.writeNekArray(pXLy,fname+"_"+argv[2]+"_Res5_"+argv[3]+"_pX.txt");
    //k.writeNekArray(pVLy,fname+"_"+argv[2]+"_Res5_"+argv[3]+"_pV.txt");
    //k.writeNekArray(pPLy,fname+"_"+argv[2]+"_Res5_"+argv[3]+"_pP.txt");
    //k.writeNekArray(pSMetLy,fname+"_"+argv[2]+"_Res5_"+argv[3]+"_pSMet.txt");
    //k.writeNekArray(pSMetLy2,fname+"_"+argv[2]+"_Res5_"+argv[3]+"_pSMet2.txt");
    k.writeNekArray(pLamb1,fname+"_"+argv[2]+"_Res5_"+argv[3]+"_pLmab1.txt");
    k.writeNekArray(pLamb2,fname+"_"+argv[2]+"_Res5_"+argv[3]+"_pLamb2.txt");

	return 0;
}

