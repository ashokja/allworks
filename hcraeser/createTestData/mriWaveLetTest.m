function [] = mriWaveLetTest(X)
%clear;
close all;
dt = (0:.1:1);
map = [dt',dt',dt'];
%load wmri;
%mp = pink(90);
idxImages = 1:2:size(X,3);
figure('DefaultAxesXTick',[],'DefaultAxesYTick',[],...
    'DefaultAxesFontSize',8,'Color','w')
colormap(map)
for k = 1:9
    j = idxImages(k);
    subplot(3,3,k);
    imagesc(X(:,:,j));
    xlabel(['Z= ' int2str(j)]);
    if k ==2
        title('some slices along the Z- orentation of original brain data');
    end
%    caxis([0,1]);
end

perm = [1 3 2];
XP = permute(X,perm);
figure('DefaultAxesXTick',[],'DefaultAxesYTick',[],...
    'DefaultAxesFontSize',8,'Color','w')
colormap(map)
for k = 1:9
    j = idxImages(k);
    subplot(3,3,k);
    imagesc(XP(:,:,j));
    xlabel(['Y= ' int2str(j)]);
    if k ==2
        title('some slices along the Z- orentation of original brain data');
    end
end
clear XP;

n = 1;      %Decompostion level
w = 'sym4'; %Near symmetric wavelet
WT = wavedec3(X,n,w);

A = cell(1,n);
D = cell(1,n);
for k =1 :n
    A{k} = waverec3(WT,'a',k);
    D{k} = waverec3(WT,'d',k);
end

%check for perfect reconstruction.
err = zeros(1,n);
for k = 1:n
    E = double(X)-A{k}-D{k};
    err(k) = max(abs(E(:)));
end
disp(err);

%Display Low-Pass and High-Pass Components;
nbIMG = 4;
idxImages_New = [1 7 10 16 19 21];
for ik = 1:nbIMG
    j = idxImages_New(ik);
    figure('DefaultAxesXTick',[],'DefaultAxesYTick',[],...
    'DefaultAxesFontSize',8,'Color','w')
    colormap(map)
    for k = 1:n
        labstr = [int2str(k) ' - Z = ' int2str(j)];
        subplot(2,n,k);
        imagesc(A{k}(:,:,j));
        xlabel(['A' labstr]);
        if k ==2
            title(['Approximations and details at level 1 to 3 -slices = ' num2str(j)]);
        end
        subplot(2,n,k+n);
        imagesc(abs(D{k}(:,:,j)));
        xlabel(['D' labstr]);
    end
end

end


