
    function [rest] = B3_eval(n1,n2,n3,coeff,dataIn)
       count =1;
       rest = zeros(size(dataIn));
       for k1 = 0:n1
           for k2 = 0:n2
               for k3= 0:n3
                   rest(:,1) = rest(:,1)  + coeff(count,1)*B3(n1,k1,n2,k2,n3,k3,dataIn);
                   rest(:,2) = rest(:,2)  + coeff(count,2)*B3(n1,k1,n2,k2,n3,k3,dataIn);
                   rest(:,3) = rest(:,3)  + coeff(count,3)*B3(n1,k1,n2,k2,n3,k3,dataIn);
                   count= count +1;
               end
           end
       end
    end
