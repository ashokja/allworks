function [C_out] = Evaluate(C_0,X,d,n)
% assuming C_0 is a single vector.
% intialize C_0
%     C_alpha = CreateAlpha(d+1,n);
%     [num_C_alpha,de] = size(C_alpha);
%     num_C_alpha
% %    count1 =6;
%     count2 =2;
%     count3 =7;
%     for i = 1:num_C_alpha
%         subsCell = num2cell(C_alpha(i,1:end-1)+1);
%         if (i ==count1 )
%             C_out(subsCell{:}) = 1;
%         else
%             C_out(subsCell{:}) = 0;
%         end
%     end
%       C_out;
%       %
     % This is the actual function.
     C_out = C_0;
     for l = d:-1:1
         C_out = EvalStep(C_out,l,X,n,d);
     end

end