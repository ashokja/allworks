#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"
//#include <ctime>



using namespace SIACUtilities;
using namespace std;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
void calAcceleration(int nq, 
	const Array<OneD,NekDouble> u_phys, const Array<OneD,NekDouble> v_phys, const Array<OneD,NekDouble> w_phys,
    const Array<OneD,NekDouble> udx_phys, const Array<OneD,NekDouble> udy_phys, const Array<OneD,NekDouble> udz_phys,
    const Array<OneD,NekDouble> vdx_phys, const Array<OneD,NekDouble> vdy_phys, const Array<OneD,NekDouble> vdz_phys,
    const Array<OneD,NekDouble> wdx_phys, const Array<OneD,NekDouble> wdy_phys, const Array<OneD,NekDouble> wdz_phys,
    Array<OneD,NekDouble> au_phys, Array<OneD,NekDouble> av_phys,  Array<OneD,NekDouble> aw_phys)
{
    Array<OneD,NekDouble> temp1(nq), temp2(nq), temp3(nq);
    // a acceleration
    Vmath::Vmul(nq, udx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, udy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, udz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, au_phys,1);
    Vmath::Vadd(nq, temp3, 1,au_phys,1, au_phys,1);
   
    Vmath::Vmul(nq, vdx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, vdy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, vdz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, av_phys,1);
    Vmath::Vadd(nq, temp3, 1,av_phys,1, av_phys,1);
    
	Vmath::Vmul(nq, wdx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, wdy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, wdz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, aw_phys,1);
    Vmath::Vadd(nq, temp3, 1,aw_phys,1, aw_phys,1);
}

void calculateB( int nq,
	const Array<OneD,NekDouble> u_phys, const Array<OneD,NekDouble> v_phys, const Array<OneD,NekDouble> w_phys,
    const Array<OneD,NekDouble> au_x, const Array<OneD,NekDouble> au_y, const Array<OneD,NekDouble> au_z,
    const Array<OneD,NekDouble> av_x, const Array<OneD,NekDouble> av_y, const Array<OneD,NekDouble> av_z,
    const Array<OneD,NekDouble> aw_x, const Array<OneD,NekDouble> aw_y, const Array<OneD,NekDouble> aw_z,
    Array<OneD,NekDouble> bu_phys, Array<OneD,NekDouble> bv_phys,  Array<OneD,NekDouble> bw_phys)
{
	calAcceleration( nq, u_phys, v_phys, w_phys,
					au_x, au_y, au_z,
					av_x, av_y, av_z,
					aw_x, aw_y, aw_z,
					bu_phys, bv_phys, bw_phys);
}

void calTorsion( int nq,
	const Array<OneD,NekDouble> u_phys, const Array<OneD,NekDouble> v_phys, const Array<OneD,NekDouble> w_phys,
    const Array<OneD,NekDouble> au, const Array<OneD,NekDouble> av, const Array<OneD,NekDouble> aw,
    const Array<OneD,NekDouble> bu, const Array<OneD,NekDouble> bv, const Array<OneD,NekDouble> bw,
    Array<OneD,NekDouble> tor)
{
    
	Array<OneD,NekDouble> temp1(nq), temp2(nq), temp3(nq);
	Array<OneD,NekDouble> vCa_x(nq), vCa_y(nq), vCa_z(nq);

	// vCa = (v x a)
    Vmath::Vmul(nq, v_phys,1,aw,1, temp1,1);
    Vmath::Vmul(nq, w_phys,1,av,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_x,1);

    Vmath::Vmul(nq, w_phys,1,au,1, temp1,1);
    Vmath::Vmul(nq, u_phys,1,aw,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_y,1);

    Vmath::Vmul(nq, u_phys,1,av,1, temp1,1);
    Vmath::Vmul(nq, v_phys,1,au,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_z,1);

	// vCadb = (v x a).b
	Vmath::Vmul(nq, vCa_x,1, bu, 1, temp1, 1);
	Vmath::Vmul(nq, vCa_y,1, bv, 1, temp2, 1);
	Vmath::Vmul(nq, vCa_z,1, bw, 1, temp3, 1);
	Vmath::Vadd(nq, temp1,1,temp2,1,temp1,1);
	Vmath::Vadd(nq, temp1,1,temp3,1,tor,1);
}


void DoVmathForBV( int Nspls,
                   const Array<OneD,NekDouble> phys_u,
                   const Array<OneD,NekDouble> phys_v,
                   const Array<OneD,NekDouble> phys_w,
                   const Array<OneD,NekDouble> bx,
                   const Array<OneD,NekDouble> by,
                   const Array<OneD,NekDouble> bz,
                   Array<OneD,NekDouble> Dbv )
{

   Array<OneD,NekDouble> d1(Nspls),d2(Nspls),d3(Nspls),nB(Nspls),nV(Nspls);;
        Vmath::Vmul(Nspls, phys_u,1,phys_u,1,d1,1);
        Vmath::Vmul(Nspls, phys_v,1,phys_v,1,d2,1);
    Vmath::Vadd(Nspls, d1,1,d2,1,d1,1);
        Vmath::Vmul(Nspls, phys_w,1,phys_w,1,d2,1);
    Vmath::Vadd(Nspls, d1,1,d2,1,nV,1);
        // Need a trick to make sure nV does not have any zero.
        Vmath::Vsqrt(Nspls,nV,1,nV,1);
        NekDouble vmin = Vmath::Vmin(Nspls,nV,1);
    if (vmin < 0.0000001)
    {
        //Vmath::Zero(Nspls,Dbv,1);
        cout<< "Some velocity are zero. "<< endl;

        for (int i =0 ; i < Nspls ; i++)
        {
            if (nV[i] < 0.0001)
            {
                nV[i] = 1.0;
            }
        }
    }
        Vmath::Vmul(Nspls, bx,1,bx,1,d1,1);
        Vmath::Vmul(Nspls, by,1,by,1,d2,1);
    Vmath::Vadd(Nspls, d1,1,d2,1,d1,1);
        Vmath::Vmul(Nspls, bz,1,bz,1,d2,1);
    Vmath::Vadd(Nspls, d1,1,d2,1,nB,1);
        Vmath::Vsqrt(Nspls,nB,1,nB,1);
    NekDouble bmin = Vmath::Vmin(Nspls,nB,1);
    if (bmin < 0.0000001)
    {
        //Vmath::Zero(Nspls,Dbv,1);
        cout<< "Some B's are zero. "<< endl;

        for (int i =0 ; i < Nspls ; i++)
        {
            if (nB[i] < 0.0001)
            {
                nB[i] = 1.0;
            }
        }
    }

    Vmath::Vdiv(Nspls, phys_u,1,nV,1,d1,1);
    Vmath::Vdiv(Nspls, bx,1,nB,1,d2,1);
    Vmath::Vmul(Nspls, d1,1,d2,1,Dbv,1);

    Vmath::Vdiv(Nspls, phys_v,1,nV,1,d1,1);
    Vmath::Vdiv(Nspls, by,1,nB,1,d2,1);
    Vmath::Vmul(Nspls, d1,1,d2,1,d3,1);
    Vmath::Vadd(Nspls, d3,1,Dbv,1,Dbv,1);

    Vmath::Vdiv(Nspls, phys_w,1,nV,1,d1,1);
    Vmath::Vdiv(Nspls, bz,1,nB,1,d2,1);
    Vmath::Vmul(Nspls, d1,1,d2,1,d3,1);
    Vmath::Vadd(Nspls, d3,1,Dbv,1,Dbv,1);

    int Num_nans = Vmath::Nnan(Nspls, Dbv,1);
    assert (0 == Num_nans && "DBV computed Nan. Check for errors");
}




void calCurvature(int nq, 
	Array<OneD,NekDouble> u_phys, Array<OneD,NekDouble> v_phys,Array<OneD,NekDouble> w_phys,
    Array<OneD,NekDouble> udx_phys, Array<OneD,NekDouble> udy_phys, Array<OneD,NekDouble> udz_phys,
    Array<OneD,NekDouble> vdx_phys, Array<OneD,NekDouble> vdy_phys, Array<OneD,NekDouble> vdz_phys,
    Array<OneD,NekDouble> wdx_phys, Array<OneD,NekDouble> wdy_phys, Array<OneD,NekDouble> wdz_phys,
    Array<OneD,NekDouble> au_phys, Array<OneD,NekDouble> av_phys,  Array<OneD,NekDouble> aw_phys,
    Array<OneD,NekDouble> vCa_x, Array<OneD,NekDouble> vCa_y, Array<OneD,NekDouble> vCa_z,
	Array<OneD,NekDouble> curNorm2 )
{
    Array<OneD,NekDouble> temp1(nq), temp2(nq), temp3(nq);
    // a acceleration
    Vmath::Vmul(nq, udx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, udy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, udz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, au_phys,1);
    Vmath::Vadd(nq, temp3, 1,au_phys,1, au_phys,1);
   
    Vmath::Vmul(nq, vdx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, vdy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, vdz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, av_phys,1);
    Vmath::Vadd(nq, temp3, 1,av_phys,1, av_phys,1);

    Vmath::Vmul(nq, wdx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, wdy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, wdz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, aw_phys,1);
    Vmath::Vadd(nq, temp3, 1,aw_phys,1, aw_phys,1);


    // cross v a  = curvature.
    Vmath::Vmul(nq, v_phys,1,aw_phys,1, temp1,1);
    Vmath::Vmul(nq, w_phys,1,av_phys,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_x,1);

    Vmath::Vmul(nq, w_phys,1,au_phys,1, temp1,1);
    Vmath::Vmul(nq, u_phys,1,aw_phys,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_y,1);

    Vmath::Vmul(nq, u_phys,1,av_phys,1, temp1,1);
    Vmath::Vmul(nq, v_phys,1,au_phys,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_z,1);

	//Curvature norm
	Vmath::Vmul(nq, vCa_x,1,vCa_x,1, temp1, 1);
	Vmath::Vmul(nq, vCa_y,1,vCa_y,1, temp2, 1);
	Vmath::Vmul(nq, vCa_z,1,vCa_z,1, temp3, 1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, curNorm2,1);
    Vmath::Vadd(nq, temp3, 1,curNorm2,1, curNorm2,1);

    return;
}





void calVorticity(int nq,
    Array<OneD,NekDouble> &udx_phys, Array<OneD,NekDouble> &udy_phys, Array<OneD,NekDouble> &udz_phys,
    Array<OneD,NekDouble> &vdx_phys, Array<OneD,NekDouble> &vdy_phys, Array<OneD,NekDouble> &vdz_phys,
    Array<OneD,NekDouble> &wdx_phys, Array<OneD,NekDouble> &wdy_phys, Array<OneD,NekDouble> &wdz_phys,
    Array<OneD,NekDouble> &Vx,Array<OneD,NekDouble> &Vy,Array<OneD,NekDouble> &Vz )
{
    Vmath::Vsub(nq, wdy_phys,1, vdz_phys,1,Vx ,1);
    Vmath::Vsub(nq, udz_phys,1, wdx_phys,1,Vy ,1);
    Vmath::Vsub(nq, vdx_phys,1, udy_phys,1,Vz ,1);
    return;
}

int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		return 0;
	}
	argc = 2;

	clock_t startTime = clock();
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);

	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	//HNM3D->LoadMesh(var[0]);
	//HNM3D->LoadMesh(var[1]);
	//HNM3D->LoadMesh(var[2]);
	
	cout << "loading xml file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	string fname = vSession->GetSessionName();

	//string fldname = fname + ".fld/Info.xml";	
	//string fldname = "naca.fld";	
	string fldname = fname + ".fld";	
	cout << fname << endl;
	cout << fldname << endl;

	HNM3D->LoadData( fldname ,var);
	cout << "loading fld file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	cout << "expsize:\t"<< HNM3D->m_expansions[0]->GetExpSize() << endl;
	
	HNM3D->LoadExpListIntoRTree();
	cout << "loading into R-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	/*
	for (int i =0; i< HNM3D->m_expansions[0]->GetExpSize();i++)
	{
		int gid = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetGlobalID();
		cout << "i\t" << i << "\tg\t" << gid << endl;
		cout << HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetShapeType()<< endl;
	}
	*/
vector<NekDouble> elementSizes;
    vector<NekDouble> centersX,centersY, centersZ;
    for (int i =0; i< HNM3D->m_expansions[0]->GetExpSize();i++)
    {
        int NumVerts = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetNumVerts();
        assert("NumVerts!=3"&& "This logic is not built for elements not tetrahedrons.");
        Nektar::SpatialDomains::PointGeomSharedPtr v0 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(0);
        Nektar::SpatialDomains::PointGeomSharedPtr v1 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(1);
        Nektar::SpatialDomains::PointGeomSharedPtr v2 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(2);
        Nektar::SpatialDomains::PointGeomSharedPtr v3 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(3);
        NekDouble ab = v0->dist(*v1);
        NekDouble ac = v0->dist(*v2);
        NekDouble ad = v0->dist(*v3);
        NekDouble bc = v1->dist(*v2);
        NekDouble bd = v1->dist(*v3);
        NekDouble cd = v2->dist(*v3);
        NekDouble max = 0.0;
        max = std::max(max,ab);
        max = std::max(max,ac);
        max = std::max(max,ad);
        max = std::max(max,bc);
        max = std::max(max,bd);
        max = std::max(max,cd);
        elementSizes.push_back(max);
        NekDouble v0x,v0y,v0z;
        NekDouble v1x,v1y,v1z;
        NekDouble v2x,v2y,v2z;
        NekDouble v3x,v3y,v3z;
        v0->GetCoords(v0x,v0y,v0z);
        v1->GetCoords(v1x,v1y,v1z);
        v2->GetCoords(v2x,v2y,v2z);
        v3->GetCoords(v3x,v3y,v3z);
        NekDouble cx = (v0x+v1x+v2x+v3x)/4.0;
        NekDouble cy = (v0y+v1y+v2y+v3y)/4.0;
        NekDouble cz = (v0z+v1z+v2z+v3z)/4.0;
        centersX.push_back(cx), centersY.push_back(cy), centersZ.push_back(cz);
    }


	// Find minx,miny,minz	
	// Find maxx,maxy,maxz	
	cout << HNM3D->m_expansions[0]->GetTotPoints() << endl;
	cout << HNM3D->m_expansions[0]->GetNcoeffs() << endl;
	
	int totPhys = HNM3D->m_expansions[0]->GetTotPoints();
	Array<OneD,NekDouble> xc0(totPhys),xc1(totPhys), xc2(totPhys);
	
	HNM3D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
	NekDouble minx = Vmath::Vmin(totPhys,xc0,1);	
	NekDouble miny = Vmath::Vmin(totPhys,xc1,1);	
	NekDouble minz = Vmath::Vmin(totPhys,xc2,1);	
	NekDouble maxx = Vmath::Vmax(totPhys,xc0,1);	
	NekDouble maxy = Vmath::Vmax(totPhys,xc1,1);	
	NekDouble maxz = Vmath::Vmax(totPhys,xc2,1);	

	cout << "min\t " << minx << "\t" << miny<<"\t"<< minz<< endl; 
	cout << "max\t " << maxx << "\t" << maxy<<"\t"<< maxz<< endl; 
	
	const Array<OneD,NekDouble> u_phys = HNM3D->m_expansions[0]->GetPhys();
	const Array<OneD,NekDouble> v_phys = HNM3D->m_expansions[1]->GetPhys();
	const Array<OneD,NekDouble> w_phys = HNM3D->m_expansions[2]->GetPhys();
	const Array<OneD,NekDouble> p_phys = HNM3D->m_expansions[3]->GetPhys();

	Array<OneD,NekDouble> ux_phys(totPhys), uy_phys(totPhys), uz_phys(totPhys);
	Array<OneD,NekDouble> vx_phys(totPhys), vy_phys(totPhys), vz_phys(totPhys);
	Array<OneD,NekDouble> wx_phys(totPhys), wy_phys(totPhys), wz_phys(totPhys);
	Array<OneD,NekDouble> au_phys(totPhys), av_phys(totPhys), aw_phys(totPhys);
	Array<OneD,NekDouble> Cu_phys(totPhys), Cv_phys(totPhys), Cw_phys(totPhys);
	Array<OneD,NekDouble> C2_phys(totPhys);

	HNM3D->m_expansions[0]->PhysDeriv(u_phys, ux_phys,uy_phys,uz_phys);
	HNM3D->m_expansions[1]->PhysDeriv(v_phys, vx_phys,vy_phys,vz_phys);
	HNM3D->m_expansions[2]->PhysDeriv(w_phys, wx_phys,wy_phys,wz_phys);
	
	cout << "Time taken to calculate 3 derivatives. "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	calCurvature( totPhys, 
					u_phys, v_phys, w_phys,
					ux_phys, uy_phys, uz_phys,
					vx_phys, vy_phys, vz_phys,
					wx_phys, wy_phys, wz_phys,
					au_phys, av_phys, aw_phys,
					Cu_phys, Cv_phys, Cw_phys,
					C2_phys );

	HNM3D->m_Arrays.push_back(Cu_phys); //4
	HNM3D->m_Arrays.push_back(Cv_phys); //5
	HNM3D->m_Arrays.push_back(Cw_phys); //6
	HNM3D->m_Arrays.push_back(C2_phys); //7

	cout << "Time taken torsion and terms. "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
			
	//minx = 2.1; miny = 0.0; minz = 0.6;
	//maxx = 2.5; maxy = 0.5; maxz= 1.1;
	//minx = 2.1; miny = 0.15; minz = 0.725;
	//maxx = 2.5; maxy = 0.35; maxz= 0.925;
	
/*	
	minx = 2.25; miny = 0.2725; minz = 0.83;
	maxx = 5.5; maxy = 0.2775; maxz= 0.835;
	int gPtsX =  12;   //atoi(argv[2]);
	int gPtsY =  10;   //atoi(argv[2]);
	int gPtsZ =  10;   //atoi(argv[2]);
	minx = 2.1; miny = 0.2675; minz = 0.825;
	maxx = 2.5; maxy = 0.2825; maxz= 0.840;
	int gPtsX =  1;   //atoi(argv[2]);
	int gPtsY =  90;   //atoi(argv[2]);
	int gPtsZ =  90;   //atoi(argv[2]);
*/
	minx = 2.25; miny = 0.2675; minz = 0.825;
	maxx = 3.0; maxy = 0.2825; maxz= 0.840;
	int gPtsX =  12;   //atoi(argv[2]);
	int gPtsY =  30;   //atoi(argv[2]);
	int gPtsZ =  30;   //atoi(argv[2]);
	int Nx = gPtsX, Ny = gPtsY, Nz = gPtsZ;
	int totPts = Ny*Nz;
	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0), sz=1.0/(Nz-1.0);
	Array<OneD,NekDouble> pX(totPts), pY(totPts), pZ(totPts), pE(totPts);	
	Array<OneD,NekDouble> pPre_s(totPts), pPre_ss(totPts);
	Array<OneD,NekDouble> pU_p(totPts), pV_p(totPts), pW_p(totPts), pP_p(totPts);
	Array<OneD,NekDouble> pU_s(totPts), pV_s(totPts), pW_s(totPts), pP_s(totPts), pW_ss(totPts);
	Array<OneD,NekDouble> pUx_s(totPts), pUy_s(totPts), pUz_s(totPts), pUy_ss(totPts), pUy_ssx(totPts), pUy_ssy(totPts), pUy_ssz(totPts);
	Array<OneD,NekDouble> pVx_s(totPts), pVy_s(totPts), pVz_s(totPts);
	Array<OneD,NekDouble> pWx_s(totPts), pWy_s(totPts), pWz_s(totPts);
	Array<OneD,NekDouble> pAU_s(totPts), pAV_s(totPts), pAW_s(totPts);
	Array<OneD,NekDouble> pCU_s(totPts), pCV_s(totPts), pCW_s(totPts);
	Array<OneD,NekDouble> pC2_s(totPts), pC2_ss(totPts);
	Array<OneD,NekDouble> coord(3);
	//SmoothieSIAC3D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]),atof(argv[3]) ); 
	//SmoothieSIAC3D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]),atof(argv[3]),1); 
	
	NekDouble valY,valZ, scaling;
	scaling = atof(argv[3]);
	Array<OneD,NekDouble> directionX(3,0.0),directionY(3,0.0),directionZ(3,0.0);
	directionX[0] = 1.0;	directionY[1] = 1.0;	directionZ[2] = 1.0;
	
	vector<Array<OneD,NekDouble> > directions;
	directions.push_back(directionX);	
	directions.push_back(directionY);	
	directions.push_back(directionZ);	
	vector<int> varsU, varsV, varsW, varsC, varsP;
	varsU.push_back(0);	varsU.push_back(0);	varsU.push_back(0); 
	varsV.push_back(1);	varsV.push_back(1);	varsV.push_back(1);
	varsP.push_back(3);	varsP.push_back(3);	varsP.push_back(3);
	varsW.push_back(2);	varsW.push_back(2);	varsW.push_back(2);
	varsC.push_back(7);	varsC.push_back(7);	varsC.push_back(7);
	vector< std::shared_ptr<SmoothieSIAC> > Sms, SmD1s, SmD2s, SmD3s;
	std::shared_ptr<SmoothieSIAC> sm = std::make_shared<SmoothieSIAC3D> (SIACUtilities::eSYM_2kp1_1SIDED_2kp1,
				HNM3D, atoi(argv[2]), atof(argv[3]) );
	std::shared_ptr<SmoothieSIAC> smD = std::make_shared<SmoothieSIAC3D> (SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1,
				HNM3D, atoi(argv[2]), atof(argv[3]) ,1);
	Sms.push_back(sm);	Sms.push_back(sm);	Sms.push_back(sm);
	SmD1s.push_back(smD);	SmD2s.push_back(sm);	SmD3s.push_back(sm);
	SmD1s.push_back(sm);	SmD2s.push_back(smD);	SmD3s.push_back(sm);
	SmD1s.push_back(sm);	SmD2s.push_back(sm);	SmD3s.push_back(smD);
	vector<NekDouble> scalings;
	scalings.push_back(scaling);	scalings.push_back(scaling);	scalings.push_back(scaling);

	int index;
	for (int i =0; i <Nx; i++)
	{
		for(int j=0; j<Ny; j++)
		{
			for(int k=0; k<Nz; k++)
			{
				index = j*Nz+k;
				//cout << index << endl;
				//pX[index] = 2.25;
				pX[index] = minx + i*sx*(maxx-minx); 
				pY[index] = miny + j*sy*(maxy-miny);
				pZ[index] = minz + k*sz*(maxz-minz);
				coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = pZ[index];
				//int elID = HNM3D->m_expansions[0]->GetExpIndex(coord);
				int elID = HNM3D->GetExpansionIndexUsingRTree(coord);
//				cout << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;	
//				cout << "eid: \t"<<elID<< "\t index \t"<< index << endl;

                vector<int> PelIds,PglIds;
                HNM3D->IntersectWithBoxUsingRTree( centersX[elID] - elementSizes[elID]/2.0 ,
                  centersY[elID]- elementSizes[elID]/2.0 , centersZ[elID]- elementSizes[elID]/2.0 ,
                  centersX[elID]+ elementSizes[elID]/2.0 , centersY[elID]+ elementSizes[elID]/2.0,
                  centersZ[elID]+ elementSizes[elID]/2.0 , PelIds, PglIds );
                NekDouble maxScaling = 0.0;
                for( int pelIndex = 0; pelIndex < PelIds.size(); pelIndex++)
                {
                    maxScaling = std::max(maxScaling, elementSizes[PelIds[pelIndex]]);
                }
                scalings[0] = maxScaling; scalings[1]= maxScaling; scalings[2]=maxScaling;

				pE[index] = elID;
				if (elID !=-1)
				{
					sm->EvaluateAt(pX[index],pY[index],pZ[index],pW_s[index],valY,valZ, directionX, scaling ,2);
					Sms[0]->EvaluateRecursiveAt( pX[index], pY[index], pZ[index], pW_ss[index],valY,valZ,Sms, directions, scalings ,varsW,1);
				}else
				{	
					pU_p[index] = 0.0;
					pV_p[index] = 0.0;
					pW_p[index] = 0.0;
					pP_p[index] = 0.0;
					pP_s[index] = 0.0;
					cout << "out" << endl;
				}
	cout << "quering into R-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds for Nz = " << Nz<< endl;
	startTime = clock();
			}
			cout << j<< endl;
		}
		cout << i << endl;
	
// Calculate acceleration,  b, torsion, Dbv for this data.

	NektarBaseClass k;
	k.writeNekArray(pX,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_ACRFDzm5"+std::to_string(pX[index])+"Var_pX.txt");
	k.writeNekArray(pY,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_ACRFDzm5"+std::to_string(pX[index])+"Var_pY.txt");
	k.writeNekArray(pZ,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_ACRFDzm5"+std::to_string(pX[index])+"Var_pZ.txt");
	k.writeNekArray(pE,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_ACRFDzm5"+std::to_string(pX[index])+"Var_pE.txt");
	
	k.writeNekArray(pW_s,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_ACRFDzm5"+std::to_string(pX[index])+"Var_pW_s.txt");
	k.writeNekArray(pW_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_ACRFDzm5"+std::to_string(pX[index])+"Var_pW_ss.txt");
	//k.writeNekArray(pUy_ss,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_ACRFDzm5"+std::to_string(pX[index])+"Var_pUy_ss.txt");
	//k.writeNekArray(pUy_ssx,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_ACRFDzm5"+std::to_string(pX[index])+"Var_pUy_ssx.txt");
	//k.writeNekArray(pUy_ssy,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_ACRFDzm5"+std::to_string(pX[index])+"Var_pUy_ssy.txt");
	//k.writeNekArray(pUy_ssz,fname+"_SO_"+argv[2]+"_Sca_"+argv[3]+"_ACRFDzm5"+std::to_string(pX[index])+"Var_pUy_ssz.txt");
	}
	cout << "writing data out "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	return 0;
}


