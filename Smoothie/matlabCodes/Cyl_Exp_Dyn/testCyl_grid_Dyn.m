clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [100];

poly =2;
order = 2;
alpha =1;
count =1;

fileName = 'Cy3l_Re500_';
Cyl_Max = 6.3;
Cyl_Min = -5.2571;

%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.6;      % LineWidth
msz = 8;       % MarkerSize
Cyl_Max = 6.3;
Cyl_Min = -5.2571;
fsw = 22;

for r = rates
    filenameFrPrint = strcat(fileName,int2str(order),'_Plane_',int2str(r));
    xfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pX_2D_NewTrial_Dyn',int2str(r),'.txt' );
    yfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pY_2D_NewTrial_Dyn',int2str(r),'.txt' );

    pVxfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx_2D_NewTrial_Dyn',int2str(r),'.txt' );
    pUyfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy_2D_NewTrial_Dyn',int2str(r),'.txt' );
    
    pVx1fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx1_2D_NewTrial_Dyn',int2str(r),'.txt' );
    pUy1fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy1_2D_NewTrial_Dyn',int2str(r),'.txt' );
    
    pVx2fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx2_2D_NewTrial_Dyn',int2str(r),'.txt' );
    pUy2fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy2_2D_NewTrial_Dyn',int2str(r),'.txt' );
    
    pVx3fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx3_2D_NewTrial_Dyn',int2str(r),'.txt' );
    pUy3fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy3_2D_NewTrial_Dyn',int2str(r),'.txt' );
    
    pVx4fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx4_2D_NewTrial_Dyn',int2str(r),'.txt' );
    pUy4fname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy4_2D_NewTrial_Dyn',int2str(r),'.txt' );
    
    pVx_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx_p_2D_NewTrial_Dyn',int2str(r),'.txt' );
    pUy_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy_p_2D_NewTrial_Dyn',int2str(r),'.txt' );
    
    Dynfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pDyn_2D_NewTrial_Dyn',int2str(r),'.txt' );
    
%     pUfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pU_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     pVfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pV_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     pPfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pP_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     
%     pUxfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUx_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     pVxfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     pPxfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPx_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     
%     pUyfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     pVyfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVy_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     pPyfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPy_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     
%     pU_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pU_p_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     pV_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pV_p_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     pP_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pP_p_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     
%     pUx_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUx_p_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     pVx_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx_p_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     pPx_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPx_p_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     
%     pUy_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy_p_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     pVy_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVy_p_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
%     pPy_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPy_p_2D_OneSided2kp1_Dyn',int2str(r),'.txt' );
    
    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');

    pUyval = textread(pUyfname, '%f\t');
    pVxval = textread(pVxfname, '%f\t');
    
    pUy1val = textread(pUy1fname, '%f\t');
    pVx1val = textread(pVx1fname, '%f\t');
    pUy2val = textread(pUy2fname, '%f\t');
    pVx2val = textread(pVx2fname, '%f\t');
    pUy3val = textread(pUy3fname, '%f\t');
    pVx3val = textread(pVx3fname, '%f\t');
    pUy4val = textread(pUy4fname, '%f\t');
    pVx4val = textread(pVx4fname, '%f\t');
    
    pUy_pval = textread(pUy_pfname, '%f\t');
    pVx_pval = textread(pVx_pfname, '%f\t');
    pDyn_val = textread(Dynfname,'%f\t');
    
%     pUval = textread(pUfname, '%f\t');  pUxval = textread(pUxfname, '%f\t');    pUyval = textread(pUyfname, '%f\t');
%     pVval = textread(pVfname, '%f\t');  pVxval = textread(pVxfname, '%f\t');    pVyval = textread(pVyfname, '%f\t');
%     pPval = textread(pPfname, '%f\t');  pPxval = textread(pPxfname, '%f\t');    pPyval = textread(pPyfname, '%f\t');
% 
%     pU_pval = textread(pU_pfname, '%f\t');  pUx_pval = textread(pUx_pfname, '%f\t');    pUy_pval = textread(pUy_pfname, '%f\t');
%     pV_pval = textread(pV_pfname, '%f\t');  pVx_pval = textread(pVx_pfname, '%f\t');    pVy_pval = textread(pVy_pfname, '%f\t');
%     pP_pval = textread(pP_pfname, '%f\t');  pPx_pval = textread(pPx_pfname, '%f\t');    pPy_pval = textread(pPy_pfname, '%f\t');
    
    
    s = sqrt(max(size(xpos)));
    xVpos = reshape(xpos,s,s);    yVpos = reshape(ypos,s,s);
    pVxVal = reshape(pVxval,s,s);
    pUyVal = reshape(pUyval,s,s);

    pVx1Val = reshape(pVx1val,s,s);
    pUy1Val = reshape(pUy1val,s,s);
    
    pVx2Val = reshape(pVx2val,s,s);
    pUy2Val = reshape(pUy2val,s,s);
    
    pVx3Val = reshape(pVx3val,s,s);
    pUy3Val = reshape(pUy3val,s,s);
 
    pVx4Val = reshape(pVx4val,s,s);
    pUy4Val = reshape(pUy4val,s,s);
    
    pVx_pVal = reshape(pVx_pval,s,s);
    pUy_pVal = reshape(pUy_pval,s,s);
    pDyn_Val = reshape(pDyn_val,s,s);    

%     pUVal = reshape(pUval,s,s);   pVVal = reshape(pVval,s,s);   pPVal = reshape(pPval,s,s);
%     pUxVal = reshape(pUxval,s,s);   pVxVal = reshape(pVxval,s,s);   pPxVal = reshape(pPxval,s,s);
%     pUyVal = reshape(pUyval,s,s);   pVyVal = reshape(pVyval,s,s);   pPyVal = reshape(pPyval,s,s);
%     
%     pU_pVal = reshape(pU_pval,s,s);   pV_pVal = reshape(pV_pval,s,s);   pP_pVal = reshape(pP_pval,s,s);
%     pUx_pVal = reshape(pUx_pval,s,s);   pVx_pVal = reshape(pVx_pval,s,s);   pPx_pVal = reshape(pPx_pval,s,s);
%     pUy_pVal = reshape(pUy_pval,s,s);   pVy_pVal = reshape(pVy_pval,s,s);   pPy_pVal = reshape(pPy_pval,s,s);
    
    % calculate vorticity
    pVortVal = pUyVal-pVxVal;
    pVort1Val = pUy1Val-pVx1Val;
    pVort2Val = pUy2Val-pVx2Val;
    pVort3Val = pUy3Val-pVx3Val;
    pVort4Val = pUy4Val-pVx4Val;
    pVort_pVal = pUy_pVal-pVx_pVal;
    
    %%
    figure(1)
    surf(xVpos,yVpos, pVort_pVal,'EdgeAlpha',0); 
    title('projected');
    
    figure(2)
    surf(xVpos,yVpos, pVortVal,'EdgeAlpha',0); 
    title('Dynamic');
    
%     figure(3)
%     surf(xVpos,yVpos, pVort1Val,'EdgeAlpha',0); 
%     title('Scaling 0.45');
%     
%     figure(4)
%     surf(xVpos,yVpos, pVort2Val,'EdgeAlpha',0); 
%     title('Scaling 0.65');
%     
%     figure(5)
%     surf(xVpos,yVpos, pVort3Val,'EdgeAlpha',0); 
%     title('Scaling 0.85');

    figure(3)
    surf(xVpos,yVpos, pVort4Val,'EdgeAlpha',0); 
    title('Scaling 1.0');
%%
    values = linspace (-6,4,20);
    figure(4)
    contour(xVpos,yVpos,pVort_pVal,(Cyl_Min :.3: Cyl_Max),'LineWidth',lw);
    xlim([3.5,9.5]);
ylim([-2.5,2.5]);
axis off;
line([5.6,5.6],[-2.5,2.5],'lineStyle','--','Color',[0.3 0.3 0.3],'linewidth',lw); hold on;
%print(strcat(filenameFrPrint,'_DG'), '-dpng','-r500');

%title('projection');

    figure(5)
   contour(xVpos,yVpos,pVort4Val,(Cyl_Min :.3: Cyl_Max),'LineWidth',lw);
    xlim([3.5,9.5]);
ylim([-2.5,2.5]);
axis off;
line([5.6,5.6],[-2.5,2.5],'lineStyle','--','Color',[0.3 0.3 0.3],'linewidth',lw); hold on;
%axis equal;
print(strcat(filenameFrPrint,'_LSIAC'), '-dpng','-r500');
%title('L-SIAC');


    figure(6)
       contour(xVpos,yVpos,pVortVal,(Cyl_Min :.3: Cyl_Max),'LineWidth',lw);
    xlim([3.5,9.5]);
ylim([-2.5,2.5]);
axis off;
line([5.6,5.6],[-2.5,2.5],'lineStyle','--','Color',[0.3 0.3 0.3],'linewidth',lw); hold on;
print(strcat(filenameFrPrint,'_A_LSIAC'), '-dpng','-r500');
%title('adaptive L-SIAC')
%%
    figure(7)
    ccc = [linspace(0.55,0.7,10),linspace(0.7,1.8,10)];
    contourf(xVpos,yVpos,pDyn_Val,ccc);
    shading interp; colorbar();
    xlim([3.5,9.5]);
ylim([-2.5,2.5]);
axis off;
% 
%     axis off; view([0,90]);
                       ax = gca;
             ax.FontSize = fsw;
    print(strcat(filenameFrPrint,'_Dyn'), '-dpng','-r500');
    %%
%     up =10;
%     dwn = -10;
%     res = 30;
%     figure(2)
%     contourf(xVpos,yVpos,pVort_pVal)%,linspace(dwn,up,res)); 
%     %caxis([dwn,up]);
%     colorbar();
%     title('projection');
%   %  print(strcat('Images/',fileName,'proj'),'-dpng','-r100');
%     
%     figure(3)
%     contourf(xVpos,yVpos,pVort1Val)%,linspace(dwn,up,res)); caxis([dwn,up]);
%     colorbar();
%     title('Scaling 0.45');
%    % print(strcat('Images/',fileName,'Scal1'),'-dpng','-r100');
%     
%     figure(4)
%     contourf(xVpos,yVpos,pVort2Val)%,linspace(dwn,up,res)); caxis([dwn,up]);
%     colorbar();
%     title('Scaling 0.65');
%     %print(strcat('Images/',fileName,'Scal2'),'-dpng','-r100');
%     
%     figure(5)
%     contourf(xVpos,yVpos,pVort3Val)%,linspace(dwn,up,res)); caxis([dwn,up]);
%     colorbar();
%     title('Scaling 0.85');
%     %print(strcat('Images/',fileName,'Scal3'),'-dpng','-r100');
%     
%     figure(6)
%     contourf(xVpos,yVpos,pVort4Val)%,linspace(dwn,up,res)); caxis([dwn,up]);
%     colorbar();
%     title('Scaling 1.05');
%     %print(strcat('Images/',fileName,'Scal4'),'-dpng','-r100');
%     
%     figure(7)
%     contourf(xVpos,yVpos,pVortVal)%,linspace(dwn,up,res)); caxis([dwn,up]);
%     colorbar();
%     title('Dyn Scaling')
%     %print(strcat('Images/',fileName,'DynScal'),'-dpng','-r100');
%         
%     %%
    

    
end

