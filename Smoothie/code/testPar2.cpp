#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include "OneSidedSIAC.h"
#include <omp.h>

#include <iomanip> // std::setprecision
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
#define TOLERENCE_VIZ 1e-6

using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 7)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg conditions file" << endl;
		cout << "4rd arg Type of Filter to use." << endl;
		cout << "5th arg Res. " << endl;
		cout << "6th number of threads. " << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM2D->LoadMesh(var[0]);
	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	//cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);
//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);

	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]));
		//fce[i] = std::cos(2.0*M_PI*(xc0[i]))*std::cos(2.0*M_PI*(xc1[i]));
	}
	
	

	HNM2D->m_expansions[0]->FwdTrans(fce,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	 ece = HNM2D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM2D->m_expansions[0]->GetCoeffs();
	HNM2D->m_Arrays.push_back(ece);
	HNM2D->LoadExpListIntoRTree();
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();

// Evaluate on a new equal space grid mesh.
	int gPts = atoi(argv[5]);
	int totPts = gPts;
	int Nx = gPts, Ny=gPts;
	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0);

	SIACUtilities::FilterType filter = SIACUtilities::eSYM_2kp1_1SIDED_2kp1;
	if (atoi(argv[4]) ==1 ) // 2kp1
	{
		filter = SIACUtilities::eSYM_2kp1_1SIDED_2kp1;	
	}else if (atoi(argv[4]) ==2 ) // 4kp1
	{
		filter = SIACUtilities::eSYM_2kp1_1SIDED_4kp1;	
	}else if (atoi(argv[4]) ==3 ) // Xli
	{
		filter = SIACUtilities::eSYM_2kp1_1SIDED_2kp2;	
	}else if (atoi(argv[4]) ==4 ) // sym
	{
		filter = SIACUtilities::eSYM_2kp1;	
	}
	SmoothieSIAC2D sm(filter , HNM2D, atoi(argv[3]), scaling ); 

	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	Array<OneD,NekDouble> direction(3,0.0), coord(3,0.0);
	direction[1] = 1.0;
	Array<OneD,NekDouble> pX(totPts), pY(totPts), pV(totPts), pP(totPts), pS(totPts),pS_par(totPts);
	double startTime = omp_get_wtime();
	for(int j=0; j< Ny;j++)
	{
		int index = j;
		pX[index] = 0.5278;
		pY[index] = j*sy;
		pV[index] = std::cos(2.0*M_PI*(pX[index] + pY[index]) );
		sm.EvaluateAt(pX[index],pY[index],0.0,pS[index],valY,valZ,direction, scaling,0);
	}
	double timeTaken = omp_get_wtime()-startTime;
	cout << "Serial\t" << timeTaken << endl;
	
    int numThreds = atoi(argv[6]); 

	startTime = omp_get_wtime();
	#pragma omp parallel for num_threads(numThreds)
	for(int j=0; j< Ny;j++)
	{
			int index = j;
			pX[index] = 0.5278;
			pY[index] = j*sy;
			pV[index] = std::cos(2.0*M_PI*(pX[index] + pY[index]) );
            sm.EvaluateAt(pX[index],pY[index],0.0,pS_par[index],valY,valZ,direction, scaling,0);
			//sm.EvaluateAt(pX[Ny-index-1],pY[Ny-index-1],0.0,pS_par[Ny-index-1],valY,valZ,direction, scaling,0);
	}
	timeTaken = omp_get_wtime()-startTime;
	cout << "parallel \t "<< timeTaken <<endl;
	
    for(int j=0; j< Ny;j++)
    {   
           if(std::abs(pS[j]-pS_par[j]) > 1e-15)
           {   
               cout << "wrong index\t" << j << " error\t" << pS[j]-pS_par[j] << endl;
           }   
    }   


	NektarBaseClass k;
//	k.writeNekArray(pX,fname+"_"+argv[3]+"_"+argv[4]+"_pX_2D.txt");
//	k.writeNekArray(pY,fname+"_"+argv[3]+"_"+argv[4]+"_pY_2D.txt");
//	k.writeNekArray(pV,fname+"_"+argv[3]+"_"+argv[4]+"_pV_2D.txt");
//	k.writeNekArray(pP,fname+"_"+argv[3]+"_"+argv[4]+"_pP_2D.txt");
//	k.writeNekArray(pS,fname+"_"+argv[3]+"_"+argv[4]+"_pS_2D.txt");
	return 0;

}

