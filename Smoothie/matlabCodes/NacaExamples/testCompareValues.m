% test del


clear;
close all;

load('UVWDer_2dirLSIAC');
load('UVW_FiniteDiff');


figure(8);
subplot(3,3,1),surf(yVpos,zVpos,abs(uxsVpos-UX_ssVpos)),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
subplot(3,3,2),surf(yVpos,zVpos,abs(uysVpos-UY_ssVpos)),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
subplot(3,3,3),surf(yVpos,zVpos,abs(uzsVpos-UZ_ssVpos)),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();

subplot(3,3,4),surf(yVpos,zVpos,abs(vxsVpos-VX_ssVpos)),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
subplot(3,3,5),surf(yVpos,zVpos,abs(vysVpos-VY_ssVpos)),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
subplot(3,3,6),surf(yVpos,zVpos,abs(vzsVpos-VZ_ssVpos)),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();

subplot(3,3,7),surf(yVpos,zVpos,abs(wxsVpos-WX_ssVpos)),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
subplot(3,3,8),surf(yVpos,zVpos,abs(wysVpos-WY_ssVpos)),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
subplot(3,3,9),surf(yVpos,zVpos,abs(wzsVpos-WZ_ssVpos)),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
print('images/F3b13/Diff_Hires_UVW_Der','-dpng', '-r500')

