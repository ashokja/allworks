% read log file.
close all;
clear;

prefix = '../../code/build_fsm3/';
LOGV = '33';

rets =[{'05'},{'025'},{'0125'}];



for Poly = 2:4
strt = strcat( '\hline','&','&','&','$\mathcal{P}$=',num2str(Poly-1),'&','&','&',' \\ \hline');
disp(strt);
for ret = rets    
	res = cell2mat(ret);
    filename = strcat('LOG',LOGV,'_NM',int2str(Poly),'_','unsMeshvar1_',res,'.log');
    %filename = strcat('LOG',LOGV,'_NM',int2str(Poly),'_','Mesh',res,'.log');

    logFileName = strcat(prefix,filename);
    fid = fopen(logFileName,'r'); %# open csv file for reading
    line = fgets(fid);
    %disp(line);
    line = fgets(fid);
    %disp(line);
    line = fgets(fid);
    %disp(line);
    % MuArray
    % DGL2
    % LSIACL2
    % DGLinf
    % LSIACLinf
    for i =1:4
        line = fgets(fid);
        line3 = fgets(fid);
        %disp(line3)
		if ( (line3(1) == 'm') && (line3(2) =='u') )
        A = sscanf(line3,'mu:\t%f');
        %disp(A);
        MuArray(i) = A;
		mu = A;
        line4 = fgets(fid);
        %disp(line4)
        A = textscan(line4,'%s %s %s %f %s %f %s %f');
        %A = sscanf(line3,'L2 error DG:%f SIAC %f SIACDyn %f');
    %    celldisp(A)
        DGL2 = num2str(cell2mat(A(4)),'%1.3e'); %DGL2
        SIACL2 = num2str(cell2mat(A(6)),'%1.3e'); %SIACL2
        SIACDynL2 = num2str(cell2mat(A(8)),'%1.3e'); %SIACDynL2
        LSIACDynL2(i) = cell2mat(A(8)); 
		

        line5 = fgets(fid);
    %    disp(line5)
        A = textscan(line5,'%s %s %s %f %s %f %s %f');
        %A = sscanf(line3,'L2 error DG:%f SIAC %f SIACDyn %f');
    %    celldisp(A)
        DGLinf = num2str(cell2mat(A(4)),'%1.3e');
        SIACLinf = num2str(cell2mat(A(6)),'%1.3e');
        SIACDynLinf = num2str(cell2mat(A(8)),'%1.3e');
        LSIACDynLinf(i) = cell2mat(A(8)); 
        
        
        if ( abs(mu-1.0)< 0.01)
			strt = strcat( 'M',res,'&',DGL2,'&',DGLinf,'&',SIACL2,'&',SIACLinf,'&',SIACDynL2,'&',SIACDynLinf,' \\ \hline');
			disp(strt);
			%disp(Poly);
			%disp('DG');
		end

        
        
		else
			%break;
            %disp('none');
            break;
        end
        
    end 
end


	% disp(LSIACDynL2);
%    figure(1)
%    plot(MuArray, log10(LSIACDynL2),'-*g'); hold on;
%    plot(MuArray, log10(LSIACDynLinf),'-or'); hold off;
    
%    figure(2),
%    plot(MuArray, log10(LSIACDynL2),'-*'); hold on;
    
    
 %   figure(3),
  %  plot(MuArray, log10(LSIACDynLinf),'-o'); hold on;
end

%figure(2),
%legend('P2','P3');
%title('LSIAC L2 R1(Figure 1) L^2 Error');
%xlabel('mu*(DynScaling)');
%ylabel('log_{10}(L^{2}Error)')
%print(strcat('Images/','Fig1_C1_L4_R1_L2'), '-dpng', '-r100');
%figure(3),
%legend('P2','P3');
%title('LSIAC L2 R1(Figure 1) L^{inf} Error');
%ylabel('log_{10}(L^{inf}Error)');
%xlabel('mu*(DynScaling)');
%print(strcat('Images/','Fig1_C1_L4_R1_Linf'), '-dpng', '-r100');

