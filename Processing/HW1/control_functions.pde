
void myGCP5Setup()
{
  myGCP5 = new ControlP5(this);
  
  // description : a bang controller triggers an event when pressed. 
  // parameters  : name, x, y, width, height
//  myGCP5.addBang("Add_Point",ScreenX +10,50,20,20);
 
    
  // description : a button executes after release
  // parameters  : name, value (float), x, y, width, height
  myGCP5.addButton("Load_File",1,ScreenX+10,20,60,20);
  
  //myGCP5.addButton("Save_To_File",1,ScreenX+20,320,70,20);
  
  myGCP5.addTextfield("Save_To_File")
    .setPosition(ScreenX +20, 300)
      .setSize(60, 20)
          .setFocus(true)
            .setColor(color(255, 0, 0));
  
  // description : a toggle can have two states, true and false
  //               where true has the value 1 and false is 0.
  // parameters  : name, default value (boolean), x, y, width, height
  myGCP5.addToggle("Lines_ON_OFF",true,ScreenX+100,10,40,20);
  
  myGCP5.addToggle("Control_Pts_ON_OFF",true,ScreenX+100,50,40,20);
  
  // description : a slider is either used horizontally or vertically.
  //               width is bigger, you get a horizontal slider
  //               height is bigger, you get a vertical slider.  
  // parameters  : name, minimum, maximum, default value (float), x, y, width, height
//  myGCP5.addSlider("slider1",0,255,128,10,80,10,100);
//  myGCP5.addSlider("slider2",0,255,128,70,80,100,10);
  
  // description : round turning dial knob
  // parameters  : name, minimum, maximum, default value (float, x, y, diameter
//  myGCP5.addKnob("knob1",0,360,0,70, 120,50);
  
  // parameters : name, default value (float), x, y,  width, height
//  myGCP5.addNumberbox("Add_After",50,ScreenX+10,200,50,15);
  
  myGCP5.addTextfield("Add_Point_After")
    .setPosition(ScreenX +20, 100)
      .setSize(40, 40)
          .setFocus(true)
            .setColor(color(255, 0, 0));
            
            
    myGCP5.addTextfield("Delete_Point_At")
    .setPosition(ScreenX +20, 200)
      .setSize(40, 40)
          .setFocus(true)
            .setColor(color(255, 0, 0));
}

void controlEvent(ControlEvent theEvent) {
  /* events triggered by controllers are automatically forwarded to 
     the controlEvent method. by checking the name of a controller one can 
     distinguish which of the controllers has been changed.
  */ 
 
  /* check if the event is from a controller otherwise you'll get an error
     when clicking other interface elements like Radiobutton that don't support
     the controller() methods
  */
  
  if(theEvent.isController()) { 
    
    print("control event from : "+theEvent.controller().name());
    println(", value : "+theEvent.controller().value());
    
    if(theEvent.controller().name()=="Save_To_File") {
      String saveFileName = trim(" "+theEvent.controller().getStringValue());
      ArrayList<String> lines  = new ArrayList<String>();
      lines.add(curves.size()+" ");
      for (int c =0; c < curves.size(); c++)
      {
        ArrayList<PVector> points = curves.get(c);
        lines.add("P,"+ points.size());
        for (int p=0 ; p< curves.get(c).size(); p++)
        {
          PVector p0 = points.get(p);
          lines.add( p0.x+","+ p0.y+"," + p0.z);
        }
        
      }
      String lines_s[] = new String[lines.size()];
      for (int j =0; j < lines.size(); j++)
      {
        lines_s[j] = lines.get(j);
      }
      saveStrings(saveFileName+".dat",lines_s);
      println("File Saved");
    }
    
    if(theEvent.controller().name()=="Load_File") {
      selectInput("Select a file to process:", "fileSelected");
    }
    
    if(theEvent.controller().name()=="Add_Point_After")
    {
      String id_s = trim(" "+theEvent.controller().getStringValue());
      float id_f = Float.parseFloat(id_s);
      int id = (int)id_f;
      println(id_f);
      int c =0;
      // if id == 0; Add point betwen 0 and 1;
      // if id == size-1; Add point between size-1 and size-2;
      if (id >= curves.get(0).size()-1 )
      {
        println("came into loop" + id );
        id = curves.get(0).size() - 2 ;
      }
      PVector p0 = curves.get(c).get(id);
      PVector p1 = curves.get(c).get(id+1);
      PVector p = new PVector((p0.x+p1.x)/2.0, (p0.y+p1.y)/2.0, (p0.z+p1.z)/2.0);
      curves.get(c).add(id+1,p);
      Draggable d1 = new Draggable(CToS(p.x/p.z,1),CToS(p.y/p.z,2),10,10);
    //points_b.add(d1);
      curves_b.get(c).add(id+1,d1);
    }
    
    if(theEvent.controller().name()=="Delete_Point_At")
    {
     String id_s = trim(" "+theEvent.controller().getStringValue());
      float id_f = Float.parseFloat(id_s);
      int id = (int)id_f;
      int c =0;
      // if id == 0; Add point betwen 0 and 1;
      // if id == size-1; Add point between size-1 and size-2;
      if (id > 0 && id < curves.get(0).size()-1 )
      {
        println("came into loop" + id );
        id = curves.get(0).size() - 2 ;
        curves.get(c).remove(id);
        curves_b.get(c).remove(id);
      }

    }
    
    if(theEvent.controller().name()=="Lines_ON_OFF") 
    {
      // Turn On and Off lines.
      if (myGLinesVisible ==0)
      {
        myGLinesVisible = 1;
      }  
      else
      {
        myGLinesVisible = 0;
      } 
    }
    
    if(theEvent.controller().name()=="Control_Pts_ON_OFF") 
    {
      // Turn On and Off lines.
      if (myGCPVisible ==0)
      {
        myGCPVisible = 1;
      }  
      else
      {
        myGCPVisible = 0;
      }
    }
    
    
    if(theEvent.controller().name()=="toggle1") {
      if(theEvent.controller().value()==1) colors[2] = color(0,255,255);
      else                                 colors[2] = color(0,0,0);
    }
    
    if(theEvent.controller().name()=="slider1") {
      colors[3] = color(theEvent.controller().value(),0,0);
    }
    
    if(theEvent.controller().name()=="slider2") {
      colors[4] = color(0,theEvent.controller().value(),0);
    }
      
    if(theEvent.controller().name()=="knob1") {
      colors[5] = color(0,0,theEvent.controller().value());
    }
    
    if(theEvent.controller().name()=="ADD_POINT") {
      // write code to add point here.
      colors[6] = color(theEvent.controller().value());
    } 
    
  }  
}


