#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include "OneSidedSIAC.h"
#include <omp.h>
#include <boost/foreach.hpp>
#include <iostream>
#include <iomanip> // std::setprecision
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
#define TOLERENCE_VIZ 1e-6

using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg conditions file." << endl;
		cout << "Metric file information"<< endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	
	BOOST_FOREACH(string &s, var)
	{
		HNM2D->LoadMesh(s);
	}
	//HNM2D->LoadMesh(var[4]);
	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	
	cout << "total Qpoints:\t"  << tNquadPts << endl;
	cout << "total Exp size:\t" << HNM2D->m_expansions[0]->GetExpSize()<< endl;

	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);

	HNM2D->m_Arrays.push_back(fce);
	HNM2D->LoadExpListIntoRTree();
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();


	string metFieldFile = "metric_request_a0.bnmetric";
		vector<NekDouble> ptArray;
		HNM2D->readNekArray(ptArray,metFieldFile );
	cout << ptArray.size() << endl;


	return 0;

}

