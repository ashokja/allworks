clear;
close all;
% test 
rates = [20,40,60,80]+1;
%rates = [20,40,60,80];
poly =4;
order = poly;
der = 1;
count =1;
for r = rates
    
    xfname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_',int2str(der),'_xc0_OneSidedD2kp3.txt' );
    ffname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_',int2str(der),'_fce_OneSidedD2kp3.txt' );
    efname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_',int2str(der),'_ece_OneSidedD2kp3.txt' );
    fDfname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_',int2str(der),'_fceD_OneSidedD2kp3.txt' );
    eDfname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_',int2str(der),'_eceD_OneSidedD2kp3.txt' );
    sfname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_',int2str(der),'_sce_OneSidedD2kp3.txt' );
   
    disp(xfname);
    xpos = textread(xfname, '%f\t');
    fval = textread(ffname, '%f\t');
    pval = textread(efname, '%f\t');
    fvalD = textread(fDfname, '%f\t');
    pvalD = textread(eDfname, '%f\t');
    sval = textread(sfname, '%f\t');

     figure(1)
     plot(xpos,fvalD,xpos,pvalD,xpos,sval);
     %hold on;

    figure(2)
    plot(xpos,log10(abs(fvalD-sval)));
    hold on;
     
    p_avg_err(count) = sum(abs(fval-pval))/max(size(xpos));
    s_avg_err(count) = sum(abs(fvalD-sval))/max(size(xpos));
    
    p_max_err(count) = max(abs(fval-pval));
    s_max_err(count) = max(abs(fvalD-sval));
    count = count+1;
end

figure(1),
legend('show');
figure(2),
legend('show');
title(strcat('poly-',int2str(poly),' order-',int2str(order),' Der-',int2str(der)));
xlabel('domain');
if (1==der)
    ylabel('-2*pi*Sin(2*pi*x)');
else
    ylabel('-4*pi*pi*Cos(2*pi*x)');
end
saveas(2,strcat('S2O3Dpoly_',int2str(poly),'_order_',int2str(order),'_Der_',int2str(der)),'jpg');


% figure(1)
% plot(log10(rates),log10(p_avg_err),log10(rates),log10(s_avg_err));
% figure(2)
% plot(log10(rates),log10(p_max_err),log10(rates),log10(s_max_err));