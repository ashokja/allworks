% Learn to interpolate Legendre Poly in 2D.
function [err] = test3DLegenPolyProj(N,n)
    switch nargin
        case 0,
            NX =10; NY=10; NZ = 10;
            nx=1;ny=1;nz=1;
        case 2,
            NX =N; NY=N; NZ = N;
            nx=n;ny=n;nz=n;
    end
    addpath ../PolyLib
    %WXQ,WYQ,XQ,YQ,NX,NY
    [XQ,WXQ] = JacobiGLZW(NX+nx,0,0);
    [YQ,WYQ] = JacobiGLZW(NY+ny,0,0);
    [ZQ,WZQ] = JacobiGLZW(NZ+nz,0,0);
    %Cal Lagrance before itself.
    for i =0:NX
         PhiXQ(i+1,:) = LegendrePoly(i,XQ);
    end
    for j = 0:NY
        PhiYQ(j+1,:) = LegendrePoly(j,YQ);
    end
    for k = 0:NZ
        PhiZQ(k+1,:) = LegendrePoly(k,ZQ);
    end
    for i = 0:NX
        for j = 0:NY
            for k = 0:NZ
                sol = 0;
                for x = 1:max(size(XQ))
                    for y = 1:max(size(YQ))
                        for z = 1:max(size(ZQ))
                            sol = sol + func(XQ(x),YQ(y),ZQ(z))*WXQ(x)*WYQ(y)*WZQ(z)*...
                                       PhiXQ(i+1,x)*PhiYQ(j+1,y)*PhiZQ(k+1,z);
                        end
                    end
                end
                u(i+1,j+1,k+1) = (2*i+1)*(2*j+1)*(2*k+1)/8*sol;
            end
        end
    end

    solPts = zeros( max(size(XQ)), max(size(YQ)),max(size(ZQ)) );
    for i = 0:NX
        for j = 0:NY
            for k = 0:NZ
                solPts = solPts+u(i+1,j+1,k+1)*...
                    outer(outer(PhiXQ(i+1,:)',PhiYQ(j+1,:)',2),PhiZQ(k+1,:)',3);
            end
        end
    end
    
    %validation
    [X,Y,Z] = meshgrid(XQ,YQ,ZQ);
    
    figure(1);
    hsurfaces = slice(X,Y,Z,func(X,Y,Z),[-1,0,1],1,-1);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold on;
%    isosurface(X,Y,Z,func(X,Y,Z),1.4); 
    
    figure(2)
    solPts = permute(solPts,[2,1,3]);
    hsurfaces = slice(X,Y,Z,solPts,[-1,0,1],1,-1);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold on;
%    isosurface(X,Y,Z,solPts,1.5); 
    
    ActFun = func(X,Y,Z);
    err = sum(abs(ActFun(:)-solPts(:)));

    function r = func(x,y,z)
        r = x.*sin((0.2)*pi*x)+y.*sin((0.7)*pi*y)+ z.*sin(pi*z);
        %r = x+y.^5+z;
    end
end
