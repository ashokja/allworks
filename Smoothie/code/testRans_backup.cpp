#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include "OneSidedSIAC.h"
#include <omp.h>
#include <boost/foreach.hpp>
#include <iostream>
#include <iomanip> // std::setprecision
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
#define TOLERENCE_VIZ 1e-6

using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 7)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg variable file name: u" << endl;
		cout << "3nd arg variable file name: v" << endl;
		cout << "4nd arg variable file name: p" << endl;
		cout << "5nd arg variable file name: rho" << endl;
		cout << "6nd arg variable file name: nut" << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	
	BOOST_FOREACH(string &s, var)
	{
		HNM2D->LoadMesh(s);
	}
	//HNM2D->LoadMesh(var[4]);
	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	
	cout << "total Qpoints:\t"  << tNquadPts << endl;
	cout << "total Exp size:\t" << HNM2D->m_expansions[0]->GetExpSize();

	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);

	HNM2D->m_Arrays.push_back(fce);
	HNM2D->LoadExpListIntoRTree();
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();


	for (int i =0; i < 5; i++)
	{
		cout << "Inputfile name:\t" << argv[2+i] << endl;
		string ptdFieldFile = argv[2+i];
		vector<NekDouble> ptArray;
		HNM2D->readNekArray(ptArray,ptdFieldFile );
		//HNM2D->printNekArray(ptArray);
		cout << "total Qpoints"  << tNquadPts << endl;
	
		for (int ii=0;  ii< tNquadPts; ii++)
		{
			fce[ii] = ptArray[3+ii];
		}
		HNM2D->m_expansions[i]->FwdTrans(fce,HNM2D->m_expansions[i]->UpdateCoeffs() );
		HNM2D->m_expansions[i]->BwdTrans( HNM2D->m_expansions[i]->GetCoeffs(),
											HNM2D->m_expansions[i]->UpdatePhys());
		Array<OneD,NekDouble> projPhys = HNM2D->m_expansions[i]->GetPhys();
		
		for (int i=0;  i< tNquadPts; i++)
		{
			if(std::abs( projPhys[i] - fce[i])>1e-14)
			{
				cout << "E :\t i = " << i ;
				cout << "\t projPhys = " << projPhys[i];
				cout << "\t fcePhys= " <<fce[i]<< endl;
			}
		}
		cout << "Loaded sucessfully" << endl;
	}


  	string out = vSession->GetSessionName() + ".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (HNM2D->m_expansions[0]->GetFieldDefinitions()).at(0);
	BOOST_FOREACH(string &s, var)
	{
    	FieldDef[0]->m_fields.push_back(s);
	}
    HNM2D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[3]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[4]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_fld->Write(out, FieldDef, FieldData);
	
	return 0;

}

