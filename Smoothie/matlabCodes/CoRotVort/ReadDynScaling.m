clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20];

%abc = '../../data/UnsTriMeshes/NM2_unsMeshvar1_0125_2_R_100_pX_2DDyn.txt';

R = 100;
poly =2;
order = poly;
count =1;

prefixName = '../../data/CorotatingVort_3/';


width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
fsw = 20;
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize
Rp = R;

%for poly = [2,3,4]
%          FileName = strcat('NM',int2str(poly),'_NPUniTriMesh05_d10_');
%           FileName = strcat('NM',int2str(poly),'_NUTriMesh_05_');
%           FileName = strcat('NM',int2str(poly),'_UniTriMesh_05_');
            FileName = strcat('bentvort_1');
            FileName = strcat('vort5_mesh');
            
%           FileName = strcat('NM',int2str(poly),'_unsMeshvar1_025_');
%            FileName = strcat('P',int2str(poly-1),'_QuadCross_40_');
            %prefixFilename = strcat( prefixName,FileName,'_P_',int2str(poly),'_scaling_0.02_xVar_0p3_R_',int2str(R),'_DLISAC');
            %prefixFilenameP = strcat( prefixName,FileName,'_P_',int2str(2),'_p.aling_0.02_xVar_0p3_R_',int2str(Rp),'_DLISAC');
            %prefixFilenameDyn = strcat( prefixName,FileName,'_P_',int2str(poly),'_sDyncaling_0.06_xVar_0p3_R_',int2str(R),'_DLISAC');

            prefixFilename = strcat( prefixName,FileName,'_P_',int2str(poly),'_scaling_0.034_xVar_0p3_R_',int2str(R),'_DLISAC');
           % prefixFilenameP = strcat( prefixName,FileName,'_P_',int2str(poly),'_scaling_0.034_xVar_0p3_R_',int2str(R),'_DLISAC');
           % prefixFilenameDyn = strcat( prefixName,FileName,'_P_',int2str(poly),'_scaling_0.034_xVar_0p3_R_',int2str(R),'_DLISAC');
            prefixFilenameP = strcat( prefixName,FileName,'_P_',int2str(2),'_p.aling_0.034_xVar_0p3_R_',int2str(R),'_DLISAC');
            prefixFilenameDyn = strcat( prefixName,'bentvort_1','_P_',int2str(poly),'_sDyncaling_0.02_xVar_0p3_R_',int2str(R),'_DLISAC');
            prefixFilenameP = strcat( prefixName,FileName,'_P_',int2str(poly),'_scaling_0.034_xVar_0p3_R_',int2str(R),'_DLISAC');
            prefixFilenameDyn = strcat( prefixName,FileName,'_P_',int2str(poly),'_scaling_0.034_xVar_0p3_R_',int2str(R),'_DLISAC');
            xfname = strcat(prefixFilename,'_pX.txt' );
            yfname = strcat(prefixFilename,'_pY.txt' );
            zfname = strcat(prefixFilename,'_pZ.txt' );

            
            u_sfname = strcat(prefixFilename,'_pU_s.txt');
            v_sfname = strcat(prefixFilename,'_pV_s.txt');
            w_sfname = strcat(prefixFilename,'_pW_s.txt');
            p_sfname = strcat(prefixFilename,'_pP_s.txt');
            
            u_sDynfname = strcat(prefixFilename,'_pU_sDyn.txt');
            v_sDynfname = strcat(prefixFilename,'_pV_sDyn.txt');
            w_sDynfname = strcat(prefixFilename,'_pW_sDyn.txt');
            p_sDynfname = strcat(prefixFilename,'_pP_sDyn.txt');            
            s_sDynfname = strcat(prefixFilenameDyn,'_pS_sDyn.txt');
            
            ux_pfname = strcat(prefixFilenameP,'_pUx_p.txt');
            vx_pfname = strcat(prefixFilenameP,'_pVx_p.txt');
            wx_pfname = strcat(prefixFilenameP,'_pWx_p.txt');
            ux_sfname = strcat(prefixFilename,'_pUx_s.txt');
            vx_sfname = strcat(prefixFilename,'_pVx_s.txt');
            wx_sfname = strcat(prefixFilename,'_pWx_s.txt');
            ux_sDynfname = strcat(prefixFilename,'_pUx_sDyn.txt');
            vx_sDynfname = strcat(prefixFilename,'_pVx_sDyn.txt');
            wx_sDynfname = strcat(prefixFilename,'_pWx_sDyn.txt');

            uy_pfname = strcat(prefixFilenameP,'_pUy_p.txt');
            vy_pfname = strcat(prefixFilenameP,'_pVy_p.txt');
            wy_pfname = strcat(prefixFilenameP,'_pWy_p.txt');
            uy_sfname = strcat(prefixFilename,'_pUy_s.txt');
            vy_sfname = strcat(prefixFilename,'_pVy_s.txt');
            wy_sfname = strcat(prefixFilename,'_pWy_s.txt');
            uy_sDynfname = strcat(prefixFilename,'_pUy_sDyn.txt');
            vy_sDynfname = strcat(prefixFilename,'_pVy_sDyn.txt');
            wy_sDynfname = strcat(prefixFilename,'_pWy_sDyn.txt');

            uz_pfname = strcat(prefixFilenameP,'_pUz_p.txt');
            vz_pfname = strcat(prefixFilenameP,'_pVz_p.txt');
            wz_pfname = strcat(prefixFilenameP,'_pWz_p.txt');
            uz_sfname = strcat(prefixFilename,'_pUz_s.txt');
            vz_sfname = strcat(prefixFilename,'_pVz_s.txt');
            wz_sfname = strcat(prefixFilename,'_pWz_s.txt');
            uz_sDynfname = strcat(prefixFilename,'_pUz_sDyn.txt');
            vz_sDynfname = strcat(prefixFilename,'_pVz_sDyn.txt');
            wz_sDynfname = strcat(prefixFilename,'_pWz_sDyn.txt');

            

            xpos = textread(xfname, '%f\t');
            ypos = textread(yfname, '%f\t');
            zpos = textread(zfname, '%f\t');
            
            uspos = textread(u_sfname,'%f\t');
            vspos = textread(v_sfname,'%f\t');
            wspos = textread(w_sfname,'%f\t');
            pspos = textread(p_sfname,'%f\t');            
            usDynpos = textread(u_sDynfname,'%f\t');
            vsDynpos = textread(v_sDynfname,'%f\t');
            wsDynpos = textread(w_sDynfname,'%f\t');
            psDynpos = textread(p_sDynfname,'%f\t');            

            ssDynpos = textread(s_sDynfname,'%f\t');
            
            uxppos = textread(ux_pfname,'%f\t');
            vxppos = textread(vx_pfname,'%f\t');
            wxppos = textread(wx_pfname,'%f\t');
            uxspos = textread(ux_sfname,'%f\t');
            vxspos = textread(vx_sfname,'%f\t');
            wxspos = textread(wx_sfname,'%f\t');
            uxsDynpos = textread(ux_sDynfname,'%f\t');
            vxsDynpos = textread(vx_sDynfname,'%f\t');
            wxsDynpos = textread(wx_sDynfname,'%f\t');

            uyppos = textread(uy_pfname,'%f\t');
            vyppos = textread(vy_pfname,'%f\t');
            wyppos = textread(wy_pfname,'%f\t');
            uyspos = textread(uy_sfname,'%f\t');
            vyspos = textread(vy_sfname,'%f\t');
            wyspos = textread(wy_sfname,'%f\t');
            uysDynpos = textread(uy_sDynfname,'%f\t');
            vysDynpos = textread(vy_sDynfname,'%f\t');
            wysDynpos = textread(wy_sDynfname,'%f\t');
            
            uzspos = textread(uz_sfname,'%f\t');
            vzspos = textread(vz_sfname,'%f\t');
            wzspos = textread(wz_sfname,'%f\t');
            uzppos = textread(uz_pfname,'%f\t');
            vzppos = textread(vz_pfname,'%f\t');
            wzppos = textread(wz_pfname,'%f\t');
            uzsDynpos = textread(uz_sDynfname,'%f\t');
            vzsDynpos = textread(vz_sDynfname,'%f\t');
            wzsDynpos = textread(wz_sDynfname,'%f\t');
            
            xVpos = reshape(xpos,[R,R]);
            yVpos = reshape(ypos,[R,R]);
            zVpos = reshape(zpos,[R,R]);
            
            usVpos = reshape(uspos,[R,R]);
            vsVpos = reshape(vspos,[R,R]);
            wsVpos = reshape(wspos,[R,R]);
            psVpos = reshape(pspos,[R,R]);
            usDynVpos = reshape(usDynpos,[R,R]);
            vsDynVpos = reshape(vsDynpos,[R,R]);
            wsDynVpos = reshape(wsDynpos,[R,R]);
            psDynVpos = reshape(psDynpos,[R,R]);
            ssDynVpos = reshape(ssDynpos,[R,R]);
            
            uxsVpos = reshape(uxspos,[R,R]);
            vxsVpos = reshape(vxspos,[R,R]);
            wxsVpos = reshape(wxspos,[R,R]);
            uxpVpos = reshape(uxppos,[Rp,Rp]);
            vxpVpos = reshape(vxppos,[Rp,Rp]);
            wxpVpos = reshape(wxppos,[Rp,Rp]);
            uxsDynVpos = reshape(uxsDynpos,[R,R]);
            vxsDynVpos = reshape(vxsDynpos,[R,R]);
            wxsDynVpos = reshape(wxsDynpos,[R,R]);

            uypVpos = reshape(uyppos,[Rp,Rp]);
            vypVpos = reshape(vyppos,[Rp,Rp]);
            wypVpos = reshape(wyppos,[Rp,Rp]);
            uysVpos = reshape(uyspos,[R,R]);
            vysVpos = reshape(vyspos,[R,R]);
            wysVpos = reshape(wyspos,[R,R]);
            uysDynVpos = reshape(uysDynpos,[R,R]);
            vysDynVpos = reshape(vysDynpos,[R,R]);
            wysDynVpos = reshape(wysDynpos,[R,R]);
            
            uzpVpos = reshape(uzppos,[Rp,Rp]);
            vzpVpos = reshape(vzppos,[Rp,Rp]);
            wzpVpos = reshape(wzppos,[Rp,Rp]);
            uzsVpos = reshape(uzspos,[R,R]);
            vzsVpos = reshape(vzspos,[R,R]);
            wzsVpos = reshape(wzspos,[R,R]);
            uzsDynVpos = reshape(uzsDynpos,[R,R]);
            vzsDynVpos = reshape(vzsDynpos,[R,R]);
            wzsDynVpos = reshape(wzsDynpos,[R,R]);
            

            figure(1),
            subplot(1,2,1),
            contourf(yVpos,zVpos,usVpos);
            subplot(1,2,2)
            contourf(yVpos,zVpos,usDynVpos);
            
            figure(2),
            subplot(1,2,1),
            contourf(yVpos,zVpos,psVpos);
            subplot(1,2,2)
            contourf(yVpos,zVpos,psDynVpos);
            

            
            %calculate vorticity
%            VorVpos_dG = 
            VorVpos_p = ((wypVpos-uzpVpos).^2 + (uzpVpos - wxpVpos).^2 + (vxpVpos - uypVpos).^2).^(1/2);
            VorVpos_s = ((wysVpos-uzsVpos).^2 + (uzsVpos - wxsVpos).^2 + (vxsVpos - uysVpos).^2).^(1/2);
            VorVpos_sDyn = ((wysDynVpos-uzsDynVpos).^2 + (uzsDynVpos - wxsDynVpos).^2 + (vxsDynVpos - uysDynVpos).^2).^(1/2);
            
            minVor = min(min(VorVpos_s(:)),min(VorVpos_sDyn(:)));
            maxVor = max(max(VorVpos_s(:)),max(VorVpos_sDyn(:)))+25;
%%
            isovalue = 14;
            figure(3),
            subplot(1,3,1);
            contour(yVpos,zVpos,VorVpos_s,linspace(minVor,maxVor,10)); caxis([minVor,maxVor]); hold on;
            contour(yVpos,zVpos,VorVpos_s,[isovalue,1000],'--r');
            subplot(1,3,2);
            contour(yVpos,zVpos,VorVpos_sDyn,linspace(minVor,maxVor,10)); caxis([minVor,maxVor]);hold on;
            contour(yVpos,zVpos,VorVpos_sDyn,[isovalue,1000],'--r');
            subplot(1,3,3);
            contour(VorVpos_p); caxis([minVor,maxVor]); hold on;
            contour(VorVpos_p,[isovalue,1000],'--r');
%%
            numofcnts = 50;
            figure(5)
            contour(yVpos,zVpos,VorVpos_s',linspace(minVor,maxVor,numofcnts),'LineWidth',lw); caxis([minVor,maxVor]); hold on;
            contour(yVpos,zVpos,VorVpos_s',[isovalue,1000],'--r','LineWidth',lw); 
            colorbar();
            axis off;
            ax = gca;
            ax.FontSize = fsw;
 %           print(strcat('images/',FileName,'_P_',int2str(poly),'_scaling_0p034_xVar_0p3_R_',int2str(R),'_LISAC'),'-dpng','-r200');
            
            figure(6)
            contour(yVpos,zVpos,VorVpos_sDyn',linspace(minVor,maxVor,numofcnts),'LineWidth',lw); caxis([minVor,maxVor]); hold on;
            contour(yVpos,zVpos,VorVpos_sDyn',[isovalue,1000],'--r','LineWidth',lw); 
            colorbar();
            axis off;
            ax = gca;
            ax.FontSize = fsw;
  %          print(strcat('images/',FileName,'_P_',int2str(poly),'_scaling_0p034_xVar_0p3_R_',int2str(R),'_A_LISAC'),'-dpng','-r200');
            
            %maxVor = maxVor+10;
            figure(7)
            contour(VorVpos_p',linspace(minVor,maxVor,numofcnts),'LineWidth',lw); caxis([minVor,maxVor]); hold on;
            contour(VorVpos_p',[isovalue,1000],'--r','LineWidth',lw); 
            colorbar();
            axis off;
            ax = gca;
            ax.FontSize = fsw;
   %         print(strcat('images/',FileName,'_P_',int2str(poly),'_scaling_0p034_xVar_0p3_R_',int2str(R),'_dG'),'-dpng','-r200');
%%
            figure(4),
            contour(fliplr(rot90(ssDynVpos,-1)))
            view([0,90]);
%             subplot(1,3,1); 
%             surf(VorVpos_p); shading interp;
%             subplot(1,3,2);
%             surf(VorVpos_s); shading interp;
%             subplot(1,3,3);
%             surf(VorVpos_sDyn); shading interp;
            

%%
% figure (10);
%     addpath('../');
%     plotNekMesh('data/dataCos2/P2_triSplit6_L4_R1.xml');
%     xlim([-1,1]);
%     ylim([-1,1]);
%     plot3([-1,1],[0,0],[32,32],'--r')
%    print(strcat('Images/triSplit6_L4_R1_','meshFile'), '-dpng', '-r100');
%%    
    
% figure(9),
% surf(xVpos,yVpos,dynsVpos);
% title('dynscaling'); shading Interp;
% 
% figure(10),
% contourf(xVpos,yVpos,dynsVpos);
% title('dynscaling'); 

