#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 6)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		cout << "4th is derviative." << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM3D->LoadMesh(var[0]);
	HNM3D->LoadMesh(var[1]);
	HNM3D->LoadMesh(var[2]);
	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM3D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM3D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1, fceDx,fceDy,eceDx,eceDy,fceDz,eceDz;
	fce = Array<OneD,NekDouble>(tNquadPts);
	fceDx = Array<OneD,NekDouble>(tNquadPts);
	fceDy = Array<OneD,NekDouble>(tNquadPts);
	fceDz = Array<OneD,NekDouble>(tNquadPts);
	eceDx = Array<OneD,NekDouble>(tNquadPts);
	eceDy = Array<OneD,NekDouble>(tNquadPts);
	eceDz = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);

	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]+xc2[i]));
		//fce[i] = std::cos(2.0*M_PI*(xc0[i]))*std::cos(2.0*M_PI*(xc1[i]));
       if (2==atoi(argv[4]))
        {   
            fceDx[i] = -1.0*4.0*M_PI*M_PI*std::cos( 2.0*M_PI*(xc0[i]+ xc1[i]+xc2[i]) );
            fceDy[i] = -1.0*4.0*M_PI*M_PI*std::cos( 2.0*M_PI*(xc0[i]+ xc1[i]+xc2[i]) );
        }else
        {   
            fceDx[i] = -1.0*2.0*M_PI*std::sin( 2.0*M_PI*(xc0[i]+xc1[i]+xc2[i]) );
            fceDy[i] = -1.0*2.0*M_PI*std::sin( 2.0*M_PI*(xc0[i]+xc1[i]+xc2[i]) );
        }   

	}
	
	HNM3D->m_expansions[0]->FwdTrans(fce,HNM3D->m_expansions[0]->UpdateCoeffs() );
	HNM3D->m_expansions[0]->BwdTrans( HNM3D->m_expansions[0]->GetCoeffs(),
										HNM3D->m_expansions[0]->UpdatePhys());
	 ece = HNM3D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM3D->m_expansions[0]->GetCoeffs();

	HNM3D->m_expansions[0]->PhysDeriv(0,ece,eceDx);
    HNM3D->m_expansions[0]->PhysDeriv(1,ece,eceDy);
    HNM3D->m_expansions[0]->PhysDeriv(2,ece,eceDz);

// Evaluate on a new equal space grid mesh.
	int gPts = atoi(argv[5]);
	int totPts = gPts*gPts*gPts;
	int Nx = gPts, Ny=gPts, Nz=gPts;
	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0), sz=1.0/(Nz-1.0);
	
	SmoothieSIAC3D sm(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]), atof(argv[3]), atoi(argv[4]) ); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	Array<OneD,NekDouble> direction(3,0.0), coord(3,0.0), directionX(3,0.0),directionY(3,0.0),directionZ(3,0.0);
	direction[2] = 1.0; directionX[0] = 1.0; directionY[1] = 1.0; directionZ[2] = 1.0;

	Array<OneD,NekDouble> pX(totPts), pY(totPts), pZ(totPts), pV(totPts), pP(totPts), pS(totPts);
	Array<OneD,NekDouble> pVDx(totPts), pPDx(totPts), pSDx(totPts);
	Array<OneD,NekDouble> pVDy(totPts), pPDy(totPts), pSDy(totPts);
	Array<OneD,NekDouble> pVDz(totPts), pPDz(totPts), pSDz(totPts);
	for( int i =0; i < Nx; i++)
	{
		for(int j=0; j< Ny; j++)
		{
			for(int k =0; k<Nz;k++)
			{
				int index = i*Nx*Ny+j*Ny+k;
				pX[index] = i*sx;
				pY[index] = j*sy;
				pZ[index] = k*sz;
				pV[index] = std::cos(2.0*M_PI*(pX[index] + pY[index] + pZ[index]) );
				if( 2==atoi(argv[4]) )
				{
                	pVDx[index] = -1.0*4.0*M_PI*M_PI*std::cos( 2.0*M_PI*(pX[index]+ pY[index]+pZ[index]) );
                	pVDy[index] = -1.0*4.0*M_PI*M_PI*std::cos( 2.0*M_PI*(pX[index]+ pY[index]+pZ[index]) );
                	pVDz[index] = -1.0*4.0*M_PI*M_PI*std::cos( 2.0*M_PI*(pX[index]+ pY[index]+pZ[index]) );
				}
				else
				{
                	pVDx[index] = -2.0*M_PI*std::sin( 2.0*M_PI*(pX[index]+ pY[index]+pZ[index]) );
                	pVDy[index] = -2.0*M_PI*std::sin( 2.0*M_PI*(pX[index]+ pY[index]+pZ[index]) );
                	pVDz[index] = -2.0*M_PI*std::sin( 2.0*M_PI*(pX[index]+ pY[index]+pZ[index]) );
				}
				//pV[index] = std::cos(2.0*M_PI*(pX[index]))*std::cos(2.0*M_PI*(pY[index]));
				sm.EvaluateAt(pX[index],pY[index],pZ[index],pSDx[index],valY,valZ,directionX, atof(argv[3]),0);
				sm.EvaluateAt(pX[index],pY[index],pZ[index],pSDy[index],valY,valZ,directionY, atof(argv[3]),0);
				sm.EvaluateAt(pX[index],pY[index],pZ[index],pSDz[index],valY,valZ,directionZ, atof(argv[3]),0);

				coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = pZ[index];
				int elId = HNM3D->m_expansions[0]->GetExpIndex(coord,TOLERENCE);
				LocalRegions::ExpansionSharedPtr lexp = HNM3D->m_expansions[0]->GetExp(elId);
				int coeffOffset = HNM3D->m_expansions[0]->GetPhys_Offset(elId);

				const Array<OneD,NekDouble> el_phys = ece.CreateWithOffset(
                                        ece, coeffOffset);
            	pP[index] = lexp->PhysEvaluate(coord,el_phys);
            	const Array<OneD,NekDouble> el_physX = eceDx.CreateWithOffset(
                	                        eceDx, coeffOffset);
            	pPDx[index] = lexp->PhysEvaluate(coord,el_physX);
            	const Array<OneD,NekDouble> el_physY = eceDy.CreateWithOffset(
                                        eceDy, coeffOffset);
            	pPDy[index] = lexp->PhysEvaluate(coord,el_physY);
            	const Array<OneD,NekDouble> el_physZ = eceDz.CreateWithOffset(
                                        eceDz, coeffOffset);
            	pPDz[index] = lexp->PhysEvaluate(coord,el_physZ);
				
			}
			cout << "j:  "<<j << endl;
		}
		cout << "i:   "<<i << endl;
	}

	NektarBaseClass k;
	k.writeNekArray(pX,fname+"_"+argv[2]+"_D_"+argv[4]+"_pX_3D_OneSided2kp1.txt");
	k.writeNekArray(pY,fname+"_"+argv[2]+"_D_"+argv[4]+"_pY_3D_OneSided2kp1.txt");
	k.writeNekArray(pZ,fname+"_"+argv[2]+"_D_"+argv[4]+"_pZ_3D_OneSided2kp1.txt");
	k.writeNekArray(pV,fname+"_"+argv[2]+"_D_"+argv[4]+"_pV_3D_OneSided2kp1.txt");
	k.writeNekArray(pP,fname+"_"+argv[2]+"_D_"+argv[4]+"_pP_3D_OneSided2kp1.txt");
	k.writeNekArray(pS,fname+"_"+argv[2]+"_D_"+argv[4]+"_pS_3D_OneSided2kp1.txt");

	k.writeNekArray(pVDx,fname+"_"+argv[2]+"_D_"+argv[4]+"_pVDx_3D_OneSided2kp1.txt");
	k.writeNekArray(pPDx,fname+"_"+argv[2]+"_D_"+argv[4]+"_pPDx_3D_OneSided2kp1.txt");
	k.writeNekArray(pSDx,fname+"_"+argv[2]+"_D_"+argv[4]+"_pSDx_3D_OneSided2kp1.txt");
	
	k.writeNekArray(pVDy,fname+"_"+argv[2]+"_D_"+argv[4]+"_pVDy_3D_OneSided2kp1.txt");
	k.writeNekArray(pPDy,fname+"_"+argv[2]+"_D_"+argv[4]+"_pPDy_3D_OneSided2kp1.txt");
	k.writeNekArray(pSDy,fname+"_"+argv[2]+"_D_"+argv[4]+"_pSDy_3D_OneSided2kp1.txt");
	
	k.writeNekArray(pVDz,fname+"_"+argv[2]+"_D_"+argv[4]+"_pVDz_3D_OneSided2kp1.txt");
	k.writeNekArray(pPDz,fname+"_"+argv[2]+"_D_"+argv[4]+"_pPDz_3D_OneSided2kp1.txt");
	k.writeNekArray(pSDz,fname+"_"+argv[2]+"_D_"+argv[4]+"_pSDz_3D_OneSided2kp1.txt");

	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
