#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>
#include <limits>   //std::numeric_limits

#include <time.h>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
//#include "HandleNekMesh2D.h"
#include "HandleNekMesh3D.h"

#include <iomanip>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>

#include <boost/geometry/index/rtree.hpp>
#include <boost/timer.hpp>

//to store queries results.
#include <vector>

//just for output
#include <iostream>
#include <boost/foreach.hpp>

using namespace SIACUtilities;
using namespace std;

int main( int argc, char* argv[])
{

	int N = 800, deg = 5;
	NekDouble xmin = -20.0;
	NekDouble xmax = 20.0;
	NekDouble scaling = 1.5;
	NekDouble shift = 0.0;

	// Open a symmetric siac and evaluate at different points requried.
	// Test simple Symmetric SIAC Filter.

	int testCase = 0;
	SymmetricSIAC* symS;
	switch (testCase)
	{
		case 0:
		// code
			symS = new SymmetricSIAC( deg+1 );
			break;
		case 1:
			// code 
			symS = new SymmetricSIAC( deg+1,4*(deg+1) );
			break;
		case 2:
			symS = new SymmetricSIAC( deg+1, SymmetricSIAC::SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC,1);
			break;
		case 3:
			symS = new SymmetricSIAC( deg+1, SymmetricSIAC::SymFilterType::CUSTOM_SMOOTH_Derivative_SIAC,2);
			break;
		case 4:
			symS = new SymmetricSIAC( deg+1, SymmetricSIAC::SymFilterType::CUSTOM_Derivative_SIAC,1);
			break;
		case 5:
			symS = new SymmetricSIAC( deg+1, SymmetricSIAC::SymFilterType::CUSTOM_Derivative_SIAC,2);
			break;
	}

	Array<OneD,NekDouble> x_pos( N);
	Array<OneD,NekDouble> t_val(N);
	Array<OneD,NekDouble> t_val2( N);
	for ( int i = 0; i < x_pos.num_elements(); i++)
	{
		//x_pos[i] = (i - (deg+1.0)*N)/((NekDouble)N);
		//cout << x_pos[i] << endl;
		x_pos[i] = xmin + (NekDouble)i*(xmax-xmin)/((NekDouble)N); 
	}
	
	boost::timer t;
	for ( int i = 0; i < 1; i++)
		symS->EvaluateFilter( x_pos, t_val, scaling ,shift, true);
	double s = t.elapsed();
	std::cout << "time taken : "<< s << " sec"<< std::endl;

	boost::timer t2;
	for ( int i = 0; i < 1; i++)
		symS->EvaluateFilterUsingSplines( x_pos, t_val2, scaling,shift, true);
	double s2 = t2.elapsed();
	std::cout << "time taken : "<< s2 << " sec"<< std::endl;

	for ( int i = 0; i < x_pos.num_elements(); i++)
	{
		cout << x_pos[i] << "\t\t\t"<< t_val[i]<< "\t\t\t"<< t_val2[i]<< "\t\t\t" << abs(t_val[i]-t_val2[i])<<endl;
	}
	

	symS->writeNekArray( x_pos, "Read_xpos.txt");	
	symS->writeNekArray( t_val, "Read_tval.txt");	
	symS->writeNekArray( t_val2, "Read_tval2.txt");	

	symS->printNekArray( symS->m_coeffs, 0); 

}
