% create and save in cells.
    addpath ../PolyLib

for n = 1:22
    [XQ,WXQ] = JacobiGLZW(n,0,0);
    XQN{n} = XQ;
    WXQN{n} = WXQ;
end

save('testdata/jacX','XQN');
save('testdata/jacWX','WXQN');

clear;
load('testdata/jacX');
XQ = XQN{3}
