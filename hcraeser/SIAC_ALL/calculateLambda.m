function [lambda,sym] = calculateLambda(x,a,b,h,r,l)
switch nargin
    case 0
        x = 7;
        a =0;b=7;
        h = 0.5;
        k = 1;
        r = 2*k;
        l = k+1;
end
%    'in calculate lambda'
    if x < (a+b)/2
        lambda = min([0,-(r+l)/2+(x-a)/h]);
    else
        lambda = max([0,(r+l)/2+(x-b)/h]);
    end
%     x
%     a
%     b
%     h
%     r
%     l
    if (x >a+(r+l)/2*h) && (x <b-(r+l)/2*h)
        sym = true;
    else
        sym = false;
    end
end