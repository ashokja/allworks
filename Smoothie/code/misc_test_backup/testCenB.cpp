/*
 *	This test case is used to build General B-Splines and One-Sided Siac filters
*/
#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC1D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh1D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

 
int main(int argc, char* argv[])
{
	cout << "This is test case 6. To test error at arbitrary points." << endl;
	// test print graph array.
	
	NektarBaseClass k;
	//
	int order = atoi(argv[1]);
	CentralBSplines cbs(order);

	Array<OneD,NekDouble> t_pos(101,0.0),t_val(101,0.0);
	int i=0;
	for (NekDouble n= -5;n<=5;n+=.1)
	{
		t_pos[i] = n;
		i+=1;
	} 
	cbs.EvaluateBSplines(t_pos,t_val,atof(argv[2]),atof(argv[3]));
	k.printGraphArray(t_val,0,1,.1);
	
	return 0;
}

void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
