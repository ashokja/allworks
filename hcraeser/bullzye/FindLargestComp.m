function [Npts] = FindLargestComp(pts)
    switch nargin
        case 0,
            pts = rand(10,3)*2-1;
            resolution = 10;
        case 1,
            resolution = 10;
    end
    
    ptsKp = pts;
    spts = max(size(pts));
    pts = (pts +1)/2*resolution -0.0001;
    pts = floor(pts)+1;
    
    [val,idx] = min(pts(:));

    if(val == 0)
        pts(pts==0)=1;
    end
    
    Img = zeros(resolution,resolution,resolution);
    CImg = Img;
    
    for i = 1:spts
        p = pts(i,:);
        Img(p(1),p(2),p(3)) =1;
    end

    
    % Largest Component.
    CC = bwconncomp(Img);
    numPixels = cellfun(@numel,CC.PixelIdxList);
    
    [~,idx] = max(numPixels);
    CImg(CC.PixelIdxList{idx}) = 1;
    % End largest Component code.
    
    % Need to change this into finding center tube.
    
    
    count = 1;
    for i = 1:spts
        p = pts(i,:);
        if CImg(p(1),p(2),p(3))==1
            Npts(count,:) = ptsKp(i,:);
            count= count+1;
        end
    end

end
