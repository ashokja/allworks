function [cnew] = ApplyDerToCoeffs(c,degree,alpha, tknots)
    switch nargin
        case (0)
            degree = 2;
            c = ones(5,1);
            alpha =2;
            tknots = -(degree+1)/2 - degree:1: (degree+1)/2+degree;
    end
    
    cold = c;
    cnew = zeros(size(cold));
    for al = 1:alpha
        cnew = zeros(size(cold));
        cnew(end+1) =0;
        for i = 1:max(size(cnew))-1
            cnew(i) = cnew(i) + cold(i)* (degree-1)/( tknots(i+degree-1) - tknots(i));
            cnew(i+1) = cnew(i+1) - cold(i)* (degree-1)/( tknots(i+degree) - tknots(i+1));
        end
        cold = cnew;
    end
    
end


% function [cnew] = ApplyDerToCoeffs(c,degree,alpha, tknots)
%     switch nargin
%         case (0)
%             degree = 2;
%             c = ones(5,1);
%             alpha =2;
%             tknots = -(degree+1)/2 - degree:1: (degree+1)/2+degree;
%     end
%     
%     cold = c;
%     cnew = zeros(size(cold));
%     for al = 1:alpha
%         cnew = zeros(size(cold));
%         cnew(end+1) =0;
%         for i = 1:max(size(cnew))-1
%             cnew(i) = cnew(i) + cold(i)* (degree-1)/( tknots(i+degree-1) - tknots(i));
%             cnew(i+1) = cnew(i+1) - cold(i)* (degree-1)/( tknots(i+degree) - tknots(i+1));
%         end
%         cold = cnew;
%     end
%     
% end
