function [C] = ReadFile(filename)
%filename = '../PostP2_2/hex_test2_unionjack_periodic_quadrature_points_projection_inversematlablatticescaling_MR15_P1_KF1_UF0.txt';

fid = fopen(filename);

tline1 = fgetl(fid);
C = zeros(1,2);
tline2 = tline1;
tline3 = tline2;

while ischar(tline3)
    %disp(tline)
    tline1 = tline2;
    tline2 = tline3;
    tline3 = fgetl(fid);
end
        C(1) = cell2mat(textscan(tline1, 'L 2 error (variable u) : %f'));
        C(2) = cell2mat(textscan(tline2, 'L inf error (variable u) : %f'));

fclose(fid);
end
