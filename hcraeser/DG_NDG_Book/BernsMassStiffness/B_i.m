function res = B_i(i,n,rv)
    res = nchoosek(n,i)*rv.^i .* (1-rv).^(n-i);
end