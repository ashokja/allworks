clear;
close all;

xpos = textread('xpos_data.txt', '%f\t');
fval = textread('fval_data.txt', '%f\t');
pval = textread('pval_data.txt', '%f\t');
sval = textread('sval_data.txt', '%f\t');
sval2 = textread('sval_data2.txt', '%f\t');

xs = 20;
xe = 50;

plot(xpos, fval,xpos,pval,xpos,sval);
xlabel('1D mesh(x)')
ylabel('values')
legend('Actual function','projected','SIAC post-processed');
figure(2),
plot( xpos(xs:xe), abs(fval(xs:xe)-pval(xs:xe)), xpos(xs:xe), abs(fval(xs:xe)-sval(xs:xe)),'-o',xpos(xs:xe), abs(fval(xs:xe)-sval2(xs:xe)),'-*');
xlabel('1D mesh(x)')
ylabel('error')
legend('projection error','SIAC error');





