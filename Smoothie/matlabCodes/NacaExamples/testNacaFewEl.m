%data view testNacaFewEl

clear;
close all;
va = -51.5;
vb = 18;

%alpha =1;
%SIACFilterPoly = 2;
%scaling = '0.05'
ElSampling = 10;
scaling = '0.05';

filename = strcat('P0000009_SO_', int2str(ElSampling),'_Sca_' , scaling,'_FewEL' );
filepath = strcat('data/',filename);

xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' )

vxfname = strcat(filepath,'_pVx.txt' );
wxfname = strcat(filepath,'_pWx.txt' );
uyfname = strcat(filepath,'_pUy.txt' );
wyfname = strcat(filepath,'_pWy.txt' );
uzfname = strcat(filepath,'_pUz.txt' );
vzfname = strcat(filepath,'_pVz.txt' );

xpos = textread(xfname,'%f\t');
ypos = textread(yfname,'%f\t');
zpos = textread(zfname,'%f\t');
epos = textread(efname, '%f\t');

vxpval = textread(vxfname,'%f\t');
wxpval = textread(wxfname,'%f\t');
uypval = textread(uyfname,'%f\t');
wypval = textread(wyfname,'%f\t');
uzpval = textread(uzfname,'%f\t');
vzpval = textread(vzfname,'%f\t');

epos = floor(epos+0.0001);
%%
VortMag = ((uypval- vxpval).^(2) + (uzpval- wxpval).^(2) + (wypval- vzpval).^(2) ).^(1/2);


[unique_ids,id_offset,id_tags] = unique(epos);
num_unique_ids = (max(size(unique_ids)));
disp(num_unique_ids);

for tag = unique_ids'
    el_indices = find(epos== (zeros(size(epos))+tag) );
    
    el_x = xpos(el_indices);
    el_y = ypos(el_indices);
    el_z = zpos(el_indices);
    
    el_Vor = VortMag(el_indices);

    [X,Y,Z] = meshgrid(2.24:0.01:2.26, 0.2:0.001:0.35, 0.75:0.001:0.9);
    VorG = griddata(el_x,el_y,el_z,el_Vor,X,Y,Z);
    
    contourslice(X,Y,Z,VorG,2.25,[],[],linspace(0,100,40)); hold on;
%     el_x = round(el_x, 7);
% 	el_y = round(el_x, 7);
% 	el_z = round(el_x, 7);
%     
    %%
    %DT = delaunayTriangulation(el_x,el_y,el_z);
    %tetramesh(DT);
    %%
%     el_x = reshape(el_x,[ElSampling ElSampling ElSampling]);
%     el_y = reshape(el_y,[ElSampling ElSampling ElSampling]);
%     el_z = reshape(el_z,[ElSampling ElSampling ElSampling]);
%     el_Vor = reshape(el_Vor,[ElSampling ElSampling ElSampling]);
    
    
% 

    
%     
%     per = [2 3 1];
%     el_x = permute(el_x, per);
%     el_y = permute(el_y, per);
%     el_z = permute(el_z, per);
%     el_Vor = permute(el_Vor, per);
% %     
%      contourslice(el_x,el_y,el_z,el_Vor,2.25,[],[])
%     
    %break;
    
end
%%
% x = linspace(-1,1,10);
% y = linspace(-1,1,10);
% z = linspace(-1,1,10);
% r = rand (10,10,10);
% [X Y Z] = meshgrid(x,y,z);
% X = (2+ X-Z).*(1-Y)/4-1;
% Y = (1+Y).*(1-Z)/2-1;
% Z = Z;
% %X = (2-X.)
% contourslice(X,Y,Z,r,1.5,[],[])

