
clear;
close all;

rate = 2;
samplerate = 10;
lc = 1;
if lc ==1
    letter = '_f_';
end
filter = 1;

va = 30;
vb = 48;
filename = strcat( 'P',int2str(rate),'_CubeGrid_',int2str(samplerate) );
xfname = strcat('../../data/NekMeshes/',filename,letter,int2str(filter),'_x.txt' );
yfname = strcat('../../data/NekMeshes/',filename,letter,int2str(filter),'_y.txt' );
zfname = strcat('../../data/NekMeshes/',filename,letter,int2str(filter),'_z.txt' );

upfname = strcat('../../data/NekMeshes/',filename,letter,int2str(filter),'_up.txt' );
vpfname = strcat('../../data/NekMeshes/',filename,letter,int2str(filter),'_vp.txt' );
wpfname = strcat('../../data/NekMeshes/',filename,letter,int2str(filter),'_wp.txt' );

Vxpfname = strcat('../../data/NekMeshes/',filename,letter,int2str(filter),'_Vor_xp.txt' );
Vypfname = strcat('../../data/NekMeshes/',filename,letter,int2str(filter),'_Vor_yp.txt' );
Vzpfname = strcat('../../data/NekMeshes/',filename,letter,int2str(filter),'_Vor_zp.txt' );

ufname = strcat('../../data/NekMeshes/',filename,letter,int2str(filter),'_u.txt' );
vfname = strcat('../../data/NekMeshes/',filename,letter,int2str(filter),'_v.txt' );
wfname = strcat('../../data/NekMeshes/',filename,letter,int2str(filter),'_w.txt' );

Vxfname = strcat('../../data/NekMeshes/',filename,letter,int2str(filter),'_Vor_x.txt' );
Vyfname = strcat('../../data/NekMeshes/',filename,letter,int2str(filter),'_Vor_y.txt' );
Vzfname = strcat('../../data/NekMeshes/',filename,letter,int2str(filter),'_Vor_z.txt' );

xdata = textread(xfname, '%f\t');
ydata = textread(yfname, '%f\t');
zdata = textread(zfname, '%f\t');

updata = textread(upfname, '%f\t');     udata = textread(ufname, '%f\t');
vpdata = textread(vpfname, '%f\t');     vdata = textread(vfname, '%f\t');
wpdata = textread(wpfname, '%f\t');     wdata = textread(wfname, '%f\t');

Vxpdata = textread(Vxpfname,'%f\t');    Vxdata = textread(Vxfname,'%f\t');
Vypdata = textread(Vypfname,'%f\t');    Vydata = textread(Vyfname,'%f\t');
Vzpdata = textread(Vzpfname,'%f\t');    Vzdata = textread(Vzfname,'%f\t');



s = sqrt(max(size(xdata(:))));

xdataV = reshape(xdata,[s,s]); ydataV = reshape(ydata,[s,s]); zdataV = reshape(zdata,[s,s]);
updataV = reshape(updata,[s,s]); vpdataV = reshape(vpdata,[s,s]); wpdataV = reshape(wpdata,[s,s]);
udataV = reshape(udata,[s,s]); vdataV = reshape(vdata,[s,s]); wdataV = reshape(wdata,[s,s]);
VxpdataV = reshape(Vxpdata,[s,s]); VypdataV = reshape(Vypdata,[s,s]); VzpdataV = reshape(Vzpdata,[s,s]);
VxdataV = reshape(Vxdata,[s,s]); VydataV = reshape(Vydata,[s,s]); VzdataV = reshape(Vzdata,[s,s]);

%Vor_Norm = Vxpdata.*Vxpdata + Vypdata.*Vypdata+Vzpdata.*Vzpdata;
figure,
subplot(2,3,1),
surf(xdataV,ydataV,updataV,'EdgeAlpha',0.1);
subplot(2,3,2),
surf(xdataV,ydataV,vpdataV,'EdgeAlpha',0.1);
subplot(2,3,3),
surf(xdataV,ydataV,wpdataV,'EdgeAlpha',0.1);
% siac
subplot(2,3,4),
surf(xdataV,ydataV,udataV,'EdgeAlpha',0.1);
subplot(2,3,5),
surf(xdataV,ydataV,vdataV,'EdgeAlpha',0.1);
subplot(2,3,6),
surf(xdataV,ydataV,wdataV,'EdgeAlpha',0.1);

figure,
subplot(2,3,1),
surf(xdataV,ydataV,VxpdataV,'EdgeAlpha',0.1);
subplot(2,3,2),
surf(xdataV,ydataV,VypdataV,'EdgeAlpha',0.1);
subplot(2,3,3),
surf(xdataV,ydataV,VzpdataV,'EdgeAlpha',0.1);

% siac
subplot(2,3,4),
surf(xdataV,ydataV,VxdataV,'EdgeAlpha',0.1);
subplot(2,3,5),
surf(xdataV,ydataV,VydataV,'EdgeAlpha',0.1);
subplot(2,3,6),
surf(xdataV,ydataV,VzdataV,'EdgeAlpha',0.1);

figure(11),
surf(xdataV,ydataV,VzpdataV,'EdgeAlpha',0.1);
view([va,vb]);
saveas(11,strcat('isoCountour_','DG'),'jpg');

figure(12),
surf(xdataV,ydataV,VzdataV,'EdgeAlpha',0.1);
view([va,vb]);
saveas(12,strcat('isoCountour_','SIAC'),'jpg');
