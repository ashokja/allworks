% createdata for spiral flow.

% two ways to create it.

% 1) circle arround the line.
% 2) Coordinate transformation.

% doing 2) now.
clear;
close all;
% Create sample data. pass it to a function. 
spacing = 0.05;
w = 4;
R = 1.5;
G =2;
% x = (0.1:spacing:pi);
% y = (0.1:spacing:pi);
% z = (-1:spacing:1);
x = linspace(-1,1,30);
y = linspace(-1,1,30);;
z = linspace(-1,1,30);;
spacing = 2/30;


[X,Y,Z] = meshgrid(x,y,z);
X = X(:); Y= Y(:); Z=Z(:);

[vel] = CalSpiralFlowUVW(X,Y,Z);
%[vel,UUU,VVV,WWW] = CalSpiralFlow_M2(X,Y,Z,w,R,G);


  U = vel(:,1); V = vel(:,2); W = vel(:,3);
%XXX=X;YYY=Y;ZZZ=Z;
s = max(size(X))^(1/3);
s = floor(s+0.001);
XXX = reshape(X,[s,s,s]);
YYY = reshape(Y,[s,s,s]);
ZZZ = reshape(Z,[s,s,s]);
UUU = reshape(U,[s,s,s]);
VVV = reshape(V,[s,s,s]);
WWW = reshape(W,[s,s,s]);
[UX,UY,UZ] = gradient(UUU,spacing,spacing,spacing);
[VX,VY,VZ] = gradient(VVV,spacing,spacing,spacing);
[WX,WY,WZ] = gradient(WWW,spacing,spacing,spacing);

VorX= WY - VZ;
VorY= UZ - WX;
VorZ= VX - UY;

isoVor = VorX.*VorX + VorY.*VorY + VorZ.*VorZ;

% Visualize output.
%    Quiver
%quiver3(X,Y,Z,vel(:,1),vel(:,2),vel(:,3));
quiver3(XXX,YYY,ZZZ,UUU,VVV,WWW);
xlabel('x');ylabel('y');zlabel('z');

%    streamline 3D.
sx =linspace(-1,1,5);
sy =linspace(-1,1,5);
sz =linspace(-1,1,5);
[startx,starty,startz] = meshgrid(sx,sy,sz);
streamline(XXX,YYY,ZZZ,UUU,VVV,WWW,startx,starty,startz);


%isosurface.
%figure, 
hold on;
scalarvalues = linspace(5,456,8);
isoVar = UUU.*UUU+VVV.*VVV+WWW.*WWW;

disp(min(isoVor(:)));
disp(max(isoVor(:)));
%s = 0.05
for s = scalarvalues
    p = patch(isosurface(XXX,YYY,ZZZ,isoVor,s));
    isonormals(XXX,YYY,ZZZ,isoVor,p);

    p.FaceColor = 'red';
    p.EdgeColor = 'none';
    daspect([1,1,1])
    view(3); axis tight
    camlight 
    lighting gouraud
end


