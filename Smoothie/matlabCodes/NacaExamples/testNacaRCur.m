% test Naca RCurData.
% test naca for calculating Curvature across a plane.
clear;
close all;

UsebkupData = 'cur_jan24/'
%UsebkupData = 'bkup_Jan24/'
%UsebkupData = ''
%alpha =1;
SIACFilterPoly = 2;
scaling = '0.01'
filename = strcat('P0000009_So_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ACRRVar' );
filepath = strcat('data/',UsebkupData,filename);
disp(filepath);


xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );
%torPfname =  strcat(filepath,'_pTor_p.txt' );
curSfname =  strcat(filepath,'_pC2_s.txt' );
curSSfname =  strcat(filepath,'_pC2_ss.txt' );
USfname =  strcat(filepath,'_pU_s.txt' );
VSfname =  strcat(filepath,'_pV_s.txt' );
WSfname =  strcat(filepath,'_pW_s.txt' );

UxSfname =  strcat(filepath,'_pUx_s.txt' );
UySfname =  strcat(filepath,'_pUy_s.txt' );
UzSfname =  strcat(filepath,'_pUz_s.txt' );

VxSfname =  strcat(filepath,'_pVx_s.txt' );
VySfname =  strcat(filepath,'_pVy_s.txt' );
VzSfname =  strcat(filepath,'_pVz_s.txt' );

WxSfname =  strcat(filepath,'_pWx_s.txt' );
WySfname =  strcat(filepath,'_pWy_s.txt' );
WzSfname =  strcat(filepath,'_pWz_s.txt' );


xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');
USval = textread( USfname, '%f\t');
VSval = textread( VSfname, '%f\t');
WSval = textread( WSfname, '%f\t');

UxSval = textread(UxSfname, '%f\t');
UySval = textread(UySfname, '%f\t');
UzSval = textread(UzSfname, '%f\t');

VxSval = textread(VxSfname, '%f\t');
VySval = textread(VySfname, '%f\t');
VzSval = textread(VzSfname, '%f\t');

WxSval = textread(WxSfname, '%f\t');
WySval = textread(WySfname, '%f\t');
WzSval = textread(WzSfname, '%f\t');


%torPval = textread(torPfname, '%f\t');
curSval = textread(curSfname, '%f\t');
%curSSval = textread(curSSfname, '%f\t');


epos = floor(epos +0.0001);

s = floor( (max(size(xpos)))^(1/2) +0.1);

%upfname = strcat(filepath,'_pU_p.txt' );

xVpos = reshape(xpos,s,s);
yVpos = reshape(ypos,s,s);
zVpos = reshape(zpos,s,s);

USVval = reshape(USval,s,s);
VSVval = reshape(VSval,s,s);
WSVval = reshape(WSval,s,s);

UxSVval = reshape(UxSval,s,s);
UySVval = reshape(UySval,s,s);
UzSVval = reshape(UzSval,s,s);

VxSVval = reshape(VxSval,s,s);
VySVval = reshape(VySval,s,s);
VzSVval = reshape(VzSval,s,s);

WxSVval = reshape(WxSval,s,s);
WySVval = reshape(WySval,s,s);
WzSVval = reshape(WzSval,s,s);


figure(2),
surf(yVpos,zVpos,(abs(WzSVval)).^(1/2))
shading interp;


% Calcualte Vorticity.

VorX = WySVval-VzSVval;
VorY = UzSVval - WxSVval;
VorZ = VxSVval - UySVval;

Vor = (VorX.^2 +VorY.^2+ VorZ.^2).^(1/2);
figure(3)
surf(yVpos,zVpos, Vor);

figure(4)
contour(yVpos,zVpos,Vor,10);


%
% testSval = textread(testSfname, '%f\t');
% 
% epos = floor(epos +0.0001);
% 
% s = floor( (max(size(xpos)))^(1/2) +0.1);
% 
% %upfname = strcat(filepath,'_pU_p.txt' );
% 
% xVpos = reshape(xpos,s,s);
% yVpos = reshape(ypos,s,s);
% zVpos = reshape(zpos,s,s);
% 
% %torPVval = reshape(torPval,s,s);
% curSVval = reshape(curSval,s,s);
% curSSVval = reshape(curSSval,s,s);
% testSVval = reshape(testSval,s,s);
% 
% figure(2),
% surf(yVpos,zVpos,(abs(curSVval)).^(1/2))
% shading interp;
% grid on;
% grid minor
% 
% figure(3),
% contourf(yVpos,zVpos,(abs(curSVval)).^(1/2),10)
% contourcbar
% title('Curvature Contour (1)');
% print('Curvature_contour_t1','-dpng','-r300');
% 
% 
% figure(4);
% surf(yVpos,zVpos,(abs(curSSVval)).^(1/2))
% shading interp;
% colorbar;
% grid on;
% grid minor
% 
% figure(5);
% contourf(yVpos,zVpos,(abs(curSSVval)).^(1/2),10)
% contourcbar
% title('Curvature Contour (2)');
% print('Curvature_contour_t2','-dpng','-r300');
% 
% 
% % figure(6);
% % surf(yVpos,zVpos,testSVval)
% % shading interp;
% % colorbar;
% % grid on;
% % grid minor
% 
% %figure(7);
% %contour(yVpos,zVpos,testSVval,[-1.1,-1.0,-0.8])
% %contourcbar
% 
% 
% 
