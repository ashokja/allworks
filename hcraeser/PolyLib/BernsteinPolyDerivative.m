

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %
  %
  %
  %
  %
  %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  function y = BernsteinPolyDerivative(index,maxdegree,x)


  y = 0.5*maxdegree*(BernsteinPoly(index-1,maxdegree-1,x)-BernsteinPoly(index,maxdegree-1,x));
  
  
