function [data] = changeGridToArray(x)
   [X,Y,Z] = meshgrid(x,x,x);
   data(:,1) = reshape(X,[],1);
   data(:,2) = reshape(Y,[],1);
   data(:,3) = reshape(Z,[],1);
end