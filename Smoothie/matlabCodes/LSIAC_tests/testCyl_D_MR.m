clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [21];

poly =2;
order = 2;
alpha =1;
count =1;

fileName = 'Cy2l_300_';

for r = rates
    
    xfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pX_2D_OneSided2kp1',int2str(r),'.txt' );
    yfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pY_2D_OneSided2kp1',int2str(r),'.txt' );

    pUfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pU_2D_OneSided2kp1',int2str(r),'.txt' );
    pVfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pV_2D_OneSided2kp1',int2str(r),'.txt' );
    pPfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pP_2D_OneSided2kp1',int2str(r),'.txt' );
    
    pUxfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUx_2D_OneSided2kp1',int2str(r),'.txt' );
    pVxfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx_2D_OneSided2kp1',int2str(r),'.txt' );
    pPxfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPx_2D_OneSided2kp1',int2str(r),'.txt' );
    
    pUyfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy_2D_OneSided2kp1',int2str(r),'.txt' );
    pVyfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVy_2D_OneSided2kp1',int2str(r),'.txt' );
    pPyfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPy_2D_OneSided2kp1',int2str(r),'.txt' );
    
    pU_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pU_p_2D_OneSided2kp1',int2str(r),'.txt' );
    pV_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pV_p_2D_OneSided2kp1',int2str(r),'.txt' );
    pP_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pP_p_2D_OneSided2kp1',int2str(r),'.txt' );
    
    pUx_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUx_p_2D_OneSided2kp1',int2str(r),'.txt' );
    pVx_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVx_p_2D_OneSided2kp1',int2str(r),'.txt' );
    pPx_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPx_p_2D_OneSided2kp1',int2str(r),'.txt' );
    
    pUy_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pUy_p_2D_OneSided2kp1',int2str(r),'.txt' );
    pVy_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pVy_p_2D_OneSided2kp1',int2str(r),'.txt' );
    pPy_pfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pPy_p_2D_OneSided2kp1',int2str(r),'.txt' );
    
    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');
    
    pUval = textread(pUfname, '%f\t');  pUxval = textread(pUxfname, '%f\t');    pUyval = textread(pUyfname, '%f\t');
    pVval = textread(pVfname, '%f\t');  pVxval = textread(pVxfname, '%f\t');    pVyval = textread(pVyfname, '%f\t');
    pPval = textread(pPfname, '%f\t');  pPxval = textread(pPxfname, '%f\t');    pPyval = textread(pPyfname, '%f\t');

    pU_pval = textread(pU_pfname, '%f\t');  pUx_pval = textread(pUx_pfname, '%f\t');    pUy_pval = textread(pUy_pfname, '%f\t');
    pV_pval = textread(pV_pfname, '%f\t');  pVx_pval = textread(pVx_pfname, '%f\t');    pVy_pval = textread(pVy_pfname, '%f\t');
    pP_pval = textread(pP_pfname, '%f\t');  pPx_pval = textread(pPx_pfname, '%f\t');    pPy_pval = textread(pPy_pfname, '%f\t');
    
    
    s = sqrt(max(size(xpos)));
    xVpos = reshape(xpos,s,s);    yVpos = reshape(ypos,s,s);
    pUVal = reshape(pUval,s,s);   pVVal = reshape(pVval,s,s);   pPVal = reshape(pPval,s,s);
    pUxVal = reshape(pUxval,s,s);   pVxVal = reshape(pVxval,s,s);   pPxVal = reshape(pPxval,s,s);
    pUyVal = reshape(pUyval,s,s);   pVyVal = reshape(pVyval,s,s);   pPyVal = reshape(pPyval,s,s);
    
    pU_pVal = reshape(pU_pval,s,s);   pV_pVal = reshape(pV_pval,s,s);   pP_pVal = reshape(pP_pval,s,s);
    pUx_pVal = reshape(pUx_pval,s,s);   pVx_pVal = reshape(pVx_pval,s,s);   pPx_pVal = reshape(pPx_pval,s,s);
    pUy_pVal = reshape(pUy_pval,s,s);   pVy_pVal = reshape(pVy_pval,s,s);   pPy_pVal = reshape(pPy_pval,s,s);

figure,    
    subplot(2,2,1),
    surf(xVpos,yVpos, pPVal,'EdgeAlpha',0); 
     colorbar;
    title('SIAC U');
    
    subplot(2,2,2),
    surf(xVpos,yVpos, pP_pVal,'EdgeAlpha',0); 
    colorbar;
    title('Proj U_P');

    subplot(2,2,3),
    surf(xVpos,yVpos, pUVal,'EdgeAlpha',0); 
     colorbar;
    title('SIAC V');
    
    subplot(2,2,4),
    surf(xVpos,yVpos, pU_pVal,'EdgeAlpha',0); 
     colorbar;
    title('Proj V_p');
    
    
    
    
figure,    
    subplot(2,2,1),
    surf(xVpos,yVpos, pUxVal,'EdgeAlpha',0.1); 
     colorbar;
    title('SIAC U_DX');
    
    subplot(2,2,2),
    surf(xVpos,yVpos, pUx_pVal,'EdgeAlpha',0.1); 
    colorbar;
    title('Proj U_DX');

    subplot(2,2,3),
    surf(xVpos,yVpos, pUyVal,'EdgeAlpha',0.1); 
     colorbar;
    title('SIAC U_DY');
    
    subplot(2,2,4),
    surf(xVpos,yVpos, pUy_pVal,'EdgeAlpha',0.1); 
     colorbar;
    title('Proj U_DY');


figure,    
    subplot(2,2,1),
    surf(xVpos,yVpos, pVxVal,'EdgeAlpha',0.1); 
     colorbar;
    title('SIAC V_DX');
    
    subplot(2,2,2),
    surf(xVpos,yVpos, pVx_pVal,'EdgeAlpha',0.1); 
     colorbar;
    title('Proj V_DX');

    subplot(2,2,3),
    surf(xVpos,yVpos, pVyVal,'EdgeAlpha',0.1); 
     colorbar;
    title('SIAC V_DY');
    
    subplot(2,2,4),
    surf(xVpos,yVpos, pVy_pVal,'EdgeAlpha',0.1); 
    caxis([-1.3 1.3]); colorbar;
    title('Proj V_DY');

    
figure,    
    subplot(2,2,1),
    surf(xVpos,yVpos, pPxVal,'EdgeAlpha',0.1); 
     colorbar;
    title('SIAC P_DX');
    
    subplot(2,2,2),
    surf(xVpos,yVpos, pPx_pVal,'EdgeAlpha',0.1); 
     colorbar;
    title('Proj P_DX');

    subplot(2,2,3),
    surf(xVpos,yVpos, pPyVal,'EdgeAlpha',0.1); 
     colorbar;
    title('SIAC P_DY');
    
    subplot(2,2,4),
    surf(xVpos,yVpos, pPy_pVal,'EdgeAlpha',0.1); 
    colorbar;
    title('Proj P_DY');
    
% figures for saving.    
figure(11)
    surf(xVpos,yVpos, pUxVal,'EdgeAlpha',0.1); 
    colorbar;
    title(strcat('SIAC P',int2str(poly),' Udx') );
  %  saveas(11,strcat(fileName,'_',int2str(r),'_','S_Udx'),'jpg');

figure(12)
    surf(xVpos,yVpos, pVxVal,'EdgeAlpha',0.1); 
    colorbar;
    title(strcat('SIAC P',int2str(poly),' Vdx'));
 %   saveas(12,strcat(fileName,'_',int2str(r),'_','S_Vdx'),'jpg');

figure(13)
    surf(xVpos,yVpos, pUx_pVal,'EdgeAlpha',0.1); 
    colorbar;
    title(strcat('Proj P',int2str(poly),' Udx'));
  %  saveas(13,strcat(fileName,'_',int2str(r),'_','P_Udx'),'jpg');

figure(14)
    surf(xVpos,yVpos, pVx_pVal,'EdgeAlpha',0.1); 
    colorbar;
    title(strcat('Proj P',int2str(poly),' Vdx'));
  %  saveas(14,strcat(fileName,'_',int2str(r),'_','P_Vdx'),'jpg');
            

figure(15)
    surf(xVpos,yVpos, pUxVal,'EdgeAlpha',0.1); 
    colorbar;
    title(strcat('SIAC P',int2str(poly),' Udx') );
    view([0,90]);
  %  saveas(15,strcat(fileName,'_',int2str(r),'_','S_Udx_v2'),'jpg');

figure(16)
    surf(xVpos,yVpos, pVxVal,'EdgeAlpha',0.1); 
    colorbar;
    title(strcat('SIAC P',int2str(poly),' Vdx'));
    view([0,90]);
  %  saveas(16,strcat(fileName,'_',int2str(r),'_','S_Vdx_v2'),'jpg');

figure(17)
    surf(xVpos,yVpos, pUx_pVal,'EdgeAlpha',0.1); 
    colorbar;
    title(strcat('Proj P',int2str(poly),' Udx'));
    view([0,90]);
  %  saveas(17,strcat(fileName,'_',int2str(r),'_','P_Udx_v2'),'jpg');

figure(18)
    surf(xVpos,yVpos, pVx_pVal,'EdgeAlpha',0.1); 
    colorbar;
    title(strcat('Proj P',int2str(poly),' Vdx'));
    view([0,90]);
  %  saveas(18,strcat(fileName,'_',int2str(r),'_','P_Vdx_v2'),'jpg');    
    
    
    
end

