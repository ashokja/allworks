% Read xc0 and xc1 from file.
% Write u and v to file.

clear;
close all;
%field one;
a1 = 0.74+0.35i;
a2 = 0.68-0.59i;
a3 =-0.11-0.72i;
b1 =-0.58+0.64i;
b2 = 0.51-0.27i;
b3 =-0.12+0.84i;

%  %field two;
% a1 = 0.74+0.35i;
% a2 =-0.18-0.19i;
% a3 =-0.11-0.72i;
% b1 =-0.58+0.64i;
% b2 = 0.51-0.27i;
% %b3 =-0.12+0.84i;


%rate = 3;
%samplerate = 6;
%r = [2,3,4];
r = [4];
%samples= [6,11,21,41,81,161];
samples= [41];
for rate = r
    for samplerate = samples
        filename = strcat('P',int2str(rate),'_quad_simple_',int2str(samplerate));
        filenamem2 = strcat('P',int2str(rate)-2,'_quad_simple_',int2str(samplerate));
        %filename = strcat('P',int2str(rate),'_critquad_',int2str(samplerate));
        %filenamem2 = strcat('P',int2str(rate)-2,'_critquad_',int2str(samplerate));
        %xfname = strcat('../../data/NekMeshes/',filename,'_xc0','.txt' );
        %yfname = strcat('../../data/NekMeshes/',filename,'_xc1','.txt' );
        
        xfname = strcat('../../data/CritFldMeshes/',filename,'_xc0','.txt' );
        yfname = strcat('../../data/CritFldMeshes/',filename,'_xc1','.txt' );
        
        xV = textread(xfname, '%f\t');
        yV = textread(yfname, '%f\t');

       xV = (2.0.*xV-1)*2;
       yV = (2.0.*yV-1)*2;
        
        %xV = (xV-1);
        %yV = (yV-1+1);

        zV = xV+yV*1i;
        zVb = xV-yV*1i;


       Val = (zV-a1).*(zV-a2).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2;
        % Val = (zV-a1).*(zVb-a2).*(zV-a3).*(zVb-b1).*(zVb-b2);

        Val_x = (1).*(zV-a2).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2+ ...
                (zV-a1).*(1).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2+ ...
                (zV-a1).*(zV-a2).*(1).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2+...
                (zV-a1).*(zV-a2).*(zV-a3).*(1).*(zVb-b2).*(zVb-b3).^2+...
                (zV-a1).*(zV-a2).*(zV-a3).*(zVb-b1).*(1).*(zVb-b3).^2+...
                (2).*(zV-a1).*(zV-a2).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3);

        Val_y = (1i).*(zV-a2).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2+ ...
                (zV-a1).*(1i).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2+ ...
                (zV-a1).*(zV-a2).*(1i).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2+...
                (zV-a1).*(zV-a2).*(zV-a3).*(-1i).*(zVb-b2).*(zVb-b3).^2+...
                (zV-a1).*(zV-a2).*(zV-a3).*(zVb-b1).*(-1i).*(zVb-b3).^2+...
                (-2*1i).*(zV-a1).*(zV-a2).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3);
            
        ux = real(Val_x)*4;
        vx = -1*imag(Val_x)*4;
        uy = real(Val_y)*4;
        vy = -1*imag(Val_y)*4;
%         ux = real(Val_x);
%         vx = -1*imag(Val_x);
%         uy = real(Val_y);
%         vy = -1*imag(Val_y);
       u = real(Val); 
       v = -1*imag(Val); 
       
       
       
        %n_uV = sqrt(uy.*uy+vx.*vx);
        %nu = uy./n_uV;
       % nv = vx./n_uV;
        
       % figure(1),
       % quiver(xV,yV,nu,nv);

%         dlmwrite(strcat('../../data/CritFldMeshes/',filename,'_u.txt'),u','delimiter','\t','precision',25);
%         dlmwrite(strcat('../../data/CritFldMeshes/',filename,'_v.txt'),v','delimiter','\t','precision',25);
        
        dlmwrite(strcat('../../data/CritFldMeshes/',filenamem2,'_ux.txt'),ux','delimiter','\t','precision',25);
        dlmwrite(strcat('../../data/CritFldMeshes/',filenamem2,'_vx.txt'),vx','delimiter','\t','precision',25);
        
        dlmwrite(strcat('../../data/CritFldMeshes/',filenamem2,'_uy.txt'),uy','delimiter','\t','precision',25);
        dlmwrite(strcat('../../data/CritFldMeshes/',filenamem2,'_vy.txt'),vy','delimiter','\t','precision',25);
        
    end
end
%%
% n_uv = sqrt(u.*u+v.*v);
% u = u./n_uv;