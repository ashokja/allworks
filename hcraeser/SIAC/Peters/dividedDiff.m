% divided difference of a problem.
function [result] = dividedDiff(ii,jj,tknots,x,h,D)
    if ii==jj
        result = h(tknots(ii),x,0,D);
    else
        if tknots(ii) == tknots(jj)
%        result = h(tknots(ii),x,jj-ii,D)/prod(1:(jj-ii));
            result = h(tknots(ii),x,jj-ii,D)/prod(1:(jj-ii));
%            disp([ii, jj, result]);
        else
            result = ( dividedDiff(ii+1,jj,tknots,x,h,D) - dividedDiff(ii,jj-1,tknots,x,h,D) )/(tknots(jj)-tknots(ii));
        end
    end
end


