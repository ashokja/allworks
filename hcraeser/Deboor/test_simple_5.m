% test_simple_4
% use Bspline to get Bernstein basis. (if possible.)

clear ;
close all;
TOLERENCE = 1e-10;
deg = 3;

%knots = [0,0,0,0,1,1,1,1];
knots = [-0.1,-0.2,-0.1,0,1,1.2,1.3,1.4];
cpts = [0.1,0.3,0.2,1];
cptsX = [0,1,2,3,4,5,6];
cptsY = [0,1,2,3,3,2,1];

% using Deboor algorithm for BSplines.
t_sel = linspace( 0.0,1.0,100 );
b_sel = zeros(size(t_sel));

for index = 1:size(t_sel(:))
    t = t_sel(index);
%   t =0.4;

    % Assuming knots are not valid.
            


    %find k.
    k = -1;
    for uk = 1:size(knots(:))
        if ( t < knots(uk)  )
            k = uk-1;
            break;
        end
    end
    
      mul_count = 0 ;
    % check if multiplicity of knots is needed.
    if ( abs(knots(k)-t) < TOLERENCE )
        % loop thorough to find multiplicity.
      
        for kt = knots
            if (abs(kt-t) <= TOLERENCE)
               mul_count = mul_count +1; 
            end
        end
    end
    
    
    
    if  ( (k < deg) )
        disp('screwed up knot t value.');
        break;
    end
    if ( ( k > ( max(size(knots(:))) -deg ) ) )
        disp('screwed up knot t value.');
        break;
    end
    
    %load the points required.
    pts_eval = [];
    pts_eval(:,1) = cpts(k-deg:k-mul_count);
   % disp(pts_eval);
    
    for l = 1:deg-mul_count
        for lp = k-deg+1 + l -1 : k+1-1-mul_count
            %disp([l lp]);
            
            alpha = (t - knots(lp))/ (knots(lp+deg-l+1) - knots(lp));
           % disp([ knots(lp+deg-l+1) knots(lp)] );
%            pt_index = lp -l+1;
            pt_index = lp -l+1 -k+deg;
            pts_eval( pt_index, l+1) = (1-alpha)*pts_eval(pt_index ,l) + alpha*pts_eval( pt_index+1,l);
            %            disp ([l, lp]);
            
 %           alpha = (t - knots(lp) ) / (knots(lp-deg) - knots(lp) ) ;
 %           pts_eval( lp,l+1) = (1-alpha)* pts_eval(lp-1,l) + alpha* pts_eval(lp ,l);
        end
    end
    
    b_sel(index) = pts_eval(1,deg-mul_count+1);
%    disp(pts_eval);
    %break;
end

plot(t_sel,b_sel ); hold on;
plot(t_sel,zeros(size(t_sel)) ); hold on;
grid on;
grid minor;
%plot( t_sel, deg*((1-t_sel).^(deg-1)).*t_sel.^(1) )


