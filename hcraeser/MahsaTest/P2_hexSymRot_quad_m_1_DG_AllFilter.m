% P1
% quadrature cal error.
% cart siac filter.
% hex union jack.
% DG

line0= '\caption{DG solution, m = 1.0, HL = L, Hex_sym, Cartesian SIAC filter.}';

clear;
% Test errors.
Deg=2;
ord1 = 15;
ord2 = 29;
ord3 = 57;
ord4 = 113;

L2B_ord1 = 7.23e-4;
L2B_ord2 = 8.88e-5;
L2B_ord3 = 1.11e-5;
L2B_ord4 = 1.39e-6;

LinfB_ord1 = 3.65e-3;
LinfB_ord2 = 4.70e-4;
LinfB_ord3 = 5.91e-5;
LinfB_ord4 = 7.40e-6;

L2_ord1 = 1.15e-4;
L2_ord2 = 9.34e-6;
L2_ord3 = 1.12e-6;
L2_ord4 = 1.39e-7;

Linf_ord1 = 2.24e-4;
Linf_ord2 = 2.24e-5;
Linf_ord3 = 2.88e-6;
Linf_ord4 = 3.68e-7;


order_L2(1) = 0;
order_L2(2) = log(L2_ord2/L2_ord1)/log(ord1/ord2);
order_L2(3) = log(L2_ord3/L2_ord2)/log(ord2/ord3);
order_L2(4) = log(L2_ord4/L2_ord3)/log(ord3/ord4);

order_Linf(1) = 0;
order_Linf(2) = log(Linf_ord2/Linf_ord1)/log(ord1/ord2);
order_Linf(3) = log(Linf_ord3/Linf_ord2)/log(ord2/ord3);
order_Linf(4) = log(Linf_ord4/Linf_ord3)/log(ord3/ord4);

order_L2
order_Linf

% Hex(1) filter.

L2BH1_ord1 = 7.23e-4;
L2BH1_ord2 = 8.88e-5;
L2BH1_ord3 = 1.11e-5;
L2BH1_ord4 = 1.39e-6;

LinfBH1_ord1 = 3.65e-3;
LinfBH1_ord2 = 4.70e-4;
LinfBH1_ord3 = 5.91e-5;
LinfBH1_ord4 = 7.40e-6;

L2H1_ord1 = 1.13e-4;
L2H1_ord2 = 2.86e-6;
L2H1_ord3 = 8.23e-8;
L2H1_ord4 = 3.54e-9;

LinfH1_ord1 = 1.74e-4;
LinfH1_ord2 = 5.03e-6;
LinfH1_ord3 = 2.59e-7;
LinfH1_ord4 = 3.13e-8;

orderH1_L2(1) = 0;
orderH1_L2(2) = log(L2H1_ord2/L2H1_ord1)/log(ord1/ord2);
orderH1_L2(3) = log(L2H1_ord3/L2H1_ord2)/log(ord2/ord3);
orderH1_L2(4) = log(L2H1_ord4/L2H1_ord3)/log(ord3/ord4);

orderH1_Linf(1) = 0;
orderH1_Linf(2) = log(LinfH1_ord2/LinfH1_ord1)/log(ord1/ord2);
orderH1_Linf(3) = log(LinfH1_ord3/LinfH1_ord2)/log(ord2/ord3);
orderH1_Linf(4) = log(LinfH1_ord4/LinfH1_ord3)/log(ord3/ord4);

orderH1_L2
orderH1_Linf

% Hex2 filter.

L2BH2_ord1 = 7.23e-4;
L2BH2_ord2 = 8.88e-5;
L2BH2_ord3 = 1.11e-5;
L2BH2_ord4 = 1.39e-6;

LinfBH2_ord1 = 3.65e-3;
LinfBH2_ord2 = 4.70e-4;
LinfBH2_ord3 = 5.91e-5;
LinfBH2_ord4 = 7.40e-6;

L2H2_ord1 = 5.71e-4;
L2H2_ord2 = 6.92e-5;
L2H2_ord3 = 8.62e-6;
L2H2_ord4 = 1.08e-6;

LinfH2_ord1 = 1.50e-3;
LinfH2_ord2 = 1.79e-4;
LinfH2_ord3 = 2.26e-5;
LinfH2_ord4 = 2.83e-6;

orderH2_L2(1) = 0;
orderH2_L2(2) = log(L2H2_ord2/L2H2_ord1)/log(ord1/ord2);
orderH2_L2(3) = log(L2H2_ord3/L2H2_ord2)/log(ord2/ord3);
orderH2_L2(4) = log(L2H2_ord4/L2H2_ord3)/log(ord3/ord4);

orderH2_Linf(1) = 0;
orderH2_Linf(2) = log(LinfH2_ord2/LinfH2_ord1)/log(ord1/ord2);
orderH2_Linf(3) = log(LinfH2_ord3/LinfH2_ord2)/log(ord2/ord3);
orderH2_Linf(4) = log(LinfH2_ord4/LinfH2_ord3)/log(ord3/ord4);

orderH2_L2
orderH2_Linf




line0= '\caption{DG solution, m = 1.0, HL = L, Hex sym, All SIAC filter, L2 Error.}';

% write code to print L2
line1= strcat('\label{table:4}',...
 '\begin{tabular}{||c| c| c| c| c| c| c | c|c ||}',...
' \hline ');
line2 = ' Mesh Ele & Deg &L2(Before) & L2(Cart SIAC) & order& L2(Hex(1) SIAC) & order& L2(Hex(2) SIAC) & order \\ [0.5ex] ';
line3 = ' \hline\hline ';
% line 1. 
line4 = strcat('$',num2str(ord1),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord1,'%1.2e'), '$ & $',...
    num2str(L2_ord1,'%1.2e'), '$ & $',...
    num2str(order_L2(1)), '$ & $',...
    num2str(L2H1_ord1,'%1.2e'), '$ & $',...
    num2str(orderH1_L2(1)), '$ & $',...
    num2str(L2H2_ord1,'%1.2e'), '$ & $',...
    num2str(orderH2_L2(1)), '$ ',...
    '\\');
line5 = ' \hline ';
% line 2.
line6 = strcat('$',num2str(ord2),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord2,'%1.2e'), '$ & $',...
    num2str(L2_ord2,'%1.2e'), '$ & $',...
    num2str(order_L2(2)), '$ & $',...
    num2str(L2H1_ord2,'%1.2e'), '$ & $',...
    num2str(orderH1_L2(2)), '$ & $',...
    num2str(L2H2_ord2,'%1.2e'), '$ & $',...
    num2str(orderH2_L2(2)), '$ ',...
    '\\');
line7 = ' \hline ';

line8 = strcat('$',num2str(ord3),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord3,'%1.2e'), '$ & $',...
    num2str(L2_ord3,'%1.2e'), '$ & $',...
    num2str(order_L2(3)), '$ & $',...
    num2str(L2H1_ord3,'%1.2e'), '$ & $',...
    num2str(orderH1_L2(3)), '$ & $',...
    num2str(L2H2_ord3,'%1.2e'), '$ & $',...
    num2str(orderH2_L2(3)), '$ ',...
    '\\');
line9 = ' \hline ';

line10 = strcat('$',num2str(ord4),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord4,'%1.2e'), '$ & $',...
    num2str(L2_ord4,'%1.2e'), '$ & $',...
    num2str(order_L2(4)), '$ & $',...
    num2str(L2H1_ord4,'%1.2e'), '$ & $',...
    num2str(orderH1_L2(4)), '$ & $',...
    num2str(L2H2_ord4,'%1.2e'), '$ & $',...
    num2str(orderH2_L2(4)), '$ ',...
    '\\');
line11= ' \hline \end{tabular} ';


fileID = fopen('P2_hexSymRot_quad_m_1_DG_All_L2.txt','w');
fwrite(fileID,line0);
fwrite(fileID,line1);
fwrite(fileID,line2);
fwrite(fileID,line3);
fwrite(fileID,line4);
fwrite(fileID,line5);
fwrite(fileID,line6);
fwrite(fileID,line7);
fwrite(fileID,line8);
fwrite(fileID,line9);
fwrite(fileID,line10);
fwrite(fileID,line11);
fclose(fileID);



% write code to print Linf
line0= '\caption{DG solution, m = 1.0, HL = L, Hex sym, All SIAC filter, Linf Error.}';

line1= strcat('\label{table:4}',...
 '\begin{tabular}{||c| c| c| c| c| c| c | c|c ||}',...
' \hline ');
line2 = ' Mesh Ele & Deg &Linf(Before) & Linf(Cart SIAC) & order& Linf(Hex(1) SIAC) & order& Linf(Hex(2) SIAC) & order \\ [0.5ex] ';
line3 = ' \hline\hline ';
% line 1. 
line4 = strcat('$',num2str(ord1),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(LinfB_ord1,'%1.2e'), '$ & $',...
    num2str(Linf_ord1,'%1.2e'), '$ & $',...
    num2str(order_Linf(1)), '$ & $',...
    num2str(LinfH1_ord1,'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(1)), '$ & $',...
    num2str(LinfH2_ord1,'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(1)), '$ ',...
    '\\');
line5 = ' \hline ';
% line 2.
line6 = strcat('$',num2str(ord2),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(LinfB_ord2,'%1.2e'), '$ & $',...
    num2str(Linf_ord2,'%1.2e'), '$ & $',...
    num2str(order_Linf(2)), '$ & $',...
    num2str(LinfH1_ord2,'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(2)), '$ & $',...
    num2str(LinfH2_ord2,'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(2)), '$ ',...
    '\\');
line7 = ' \hline ';

line8 = strcat('$',num2str(ord3),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(LinfB_ord3,'%1.2e'), '$ & $',...
    num2str(Linf_ord3,'%1.2e'), '$ & $',...
    num2str(order_Linf(3)), '$ & $',...
    num2str(LinfH1_ord3,'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(3)), '$ & $',...
    num2str(LinfH2_ord3,'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(3)), '$ ',...
    '\\');
line9 = ' \hline ';

line10 = strcat('$',num2str(ord4),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(LinfB_ord4,'%1.2e'), '$ & $',...
    num2str(Linf_ord4,'%1.2e'), '$ & $',...
    num2str(order_Linf(4)), '$ & $',...
    num2str(LinfH1_ord4,'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(4)), '$ & $',...
    num2str(LinfH2_ord4,'%1.2e'), '$ & $',...
    num2str(orderH2_Linf(4)), '$ ',...
    '\\');
line11= ' \hline \end{tabular} ';


fileID = fopen('P2_hexSymRot_quad_m_1_DG_All_Linf.txt','w');
fwrite(fileID,line0);
fwrite(fileID,line1);
fwrite(fileID,line2);
fwrite(fileID,line3);
fwrite(fileID,line4);
fwrite(fileID,line5);
fwrite(fileID,line6);
fwrite(fileID,line7);
fwrite(fileID,line8);
fwrite(fileID,line9);
fwrite(fileID,line10);
fwrite(fileID,line11);
fclose(fileID);

