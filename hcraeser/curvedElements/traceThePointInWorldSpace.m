function [trace,traceVel] = traceThePointInWorldSpace(n1,n2,n3,coeff,tracePointW,Xa,Xb,Xc,Xd,Xe,Xf,Xg,Xh)
    switch nargin
        case 0,
        load('July26');
    end
    % From Face 1 (x = -1)

%    tracePoint = sol_seeds(1,1,:);
    dt = 3*10^(-2);
    % Part Def of function.
    tpR = T_worldToRef(tracePoint(1,1,1),tracePoint(1,1,2),tracePoint(1,1,3),Xa,Xb,Xc,Xd,Xe,Xf,Xg,Xh);
    tracePointR(1,1,:) = tpR;
    %tracePointW = [tracePointW(1,1,1),tracePointW(1,1,2),tracePointW(1,1,3)];
    trace = zeros(1,3);
    traceVel = zeros(1,3);
    for i = 1:1000
        vel = ReducedVelAt(n1,n2,n3,coeff,tracePointR);
        J = jacobian3D(tracePointR(1,1,1),tracePointR(1,1,2),tracePointR(1,1,3),Xa,Xb,Xc,Xd,Xe,Xf,Xg,Xh);
        traceVel(i,:) = J*vel';
        if ( norm(vel) ==0 ),break,end; % less than a norm not compared to zero
        tp = squeeze(tracePointW);
        tp = tp + J*vel'*dt;
        tracePointW(1,1,:) = tp;
        tracePointR(1,1,:) = T_worldToRef(tp(1),tp(2),tp(3),Xa,Xb,Xc,Xd,Xe,Xf,Xg,Xh);
        trace(i,:) = tp;
        if ( sum(tracePointR(1,1,:)'>1)>0 || sum(tracePointR(1,1,:)'<-1)>0)
            break;
        end
    end
    
    
    function vel = ReducedVelAt(n1,n2,n3,coeff,tracePoint)
        Vbar = B3_der_eval(n1,n2,n3,coeff,tracePoint);
        jj = reshape(Vbar,3,3);
        lambda = eig(jj);
        tet_complex =false;
        if ~isreal(lambda)
            'tet contains complex number';
            tet_complex = true;
            % find the real rool index. 
            real_index  = find(imag(lambda)==0);
        end
        if (tet_complex)
            [A,~] = eig(jj);            
            n = A(:,real_index)'/norm(A(:,real_index));
            do = squeeze(B3_eval(n1,n2,n3,coeff,tracePoint));
            vel = dot(do,n)*n;
            if ( norm (vel) < 10^(-2))
                vel = n;
            end
            %vel = A(:,real_index)';
        else
            vel = [0,0,0];
        end
    end
    
    function x=MultiNewtonBasis(n1,n2,n3,coeff,x,NumIters,val,id)
    %Performs multidimensional Newton's method for the function defined in f
    %starting with x and running NumIters times.
        y = B3_eval(n1,n2,n3,coeff,x);
        ko = B3_der_eval(n1,n2,n3,coeff,x);
        dy = reshape(ko,3,3);
        %[res1,res2] = f_der(
        %dy = ;
        for j=1:NumIters
            s=pinv(dy')*y';
            x=x-s';
            x(id) = val;
            y = B3_eval(n1,n2,n3,coeff,x);
            dy = reshape(B3_der_eval(n1,n2,n3,coeff,x),3,3);
        end 
    end

    

    function [rest] =  B2(n1,k1,n2,k2,dataIn)
        v1 = B(n1,k1,dataIn(:,1));
        v2 = B(n2,k2,dataIn(:,2));
        rest = v1.*v2;
    end
    
    function [rest] = B3(n1,k1,n2,k2,n3,k3,dataIn)
        v1 = B(n1,k1,dataIn(:,1));
        v2 = B(n2,k2,dataIn(:,2));
        v3 = B(n3,k3,dataIn(:,3));
        rest = v1.*v2.*v3;    
    end
    
    
    function [rest] = B2_der(n1,k1,n2,k2,dataIn)
        v1 =     B(n1,k1,dataIn(:,1));
        v1_der = B_der(n1,k1,dataIn(:,1));
        v2 =     B(n2,k2,dataIn(:,2));
        v2_der = B_der(n2,k2,dataIn(:,2));
        
        rest(:,1) = v1_der.*v2;
        rest(:,2) = v1.*v2_der;
    end

    function [rest] = B3_der(n1,k1,n2,k2,n3,k3,dataIn)
        v1 =     B(n1,k1,dataIn(:,1));
        v1_der = B_der(n1,k1,dataIn(:,1));
        v2 =     B(n2,k2,dataIn(:,2));
        v2_der = B_der(n2,k2,dataIn(:,2));
        v3 =     B(n3,k3,dataIn(:,3));
        v3_der = B_der(n3,k3,dataIn(:,3));
        rest(:,1) = v1_der.*v2.*v3;
        rest(:,2) = v1.*v2_der.*v3;
        rest(:,3) = v1.*v2.*v3_der;
    end

    function [rest] = B2_eval(n1,n2,coeff,dataIn)
       count =1;
       rest = zeros(size(dataIn));
       for k1 = 0:n1
           for k2 = 0:n2
               rest(:,1) = rest(:,1)  + coeff(count,1)*B2(n1,k1,n2,k2,dataIn);
               rest(:,2) = rest(:,2)  + coeff(count,2)*B2(n1,k1,n2,k2,dataIn);
               count= count +1;
           end
       end
    end

    function [rest] = B2_eval_3(n1,n2,coeff,dataIn)
       count =1;
       [a,b] = size(dataIn);
       rest = zeros(a,b+1);
       for k1 = 0:n1
           for k2 = 0:n2
               rest(:,1) = rest(:,1)  + coeff(count,1)*B2(n1,k1,n2,k2,dataIn);
               rest(:,2) = rest(:,2)  + coeff(count,2)*B2(n1,k1,n2,k2,dataIn);
               rest(:,3) = rest(:,3)  + coeff(count,3)*B2(n1,k1,n2,k2,dataIn);
               count= count +1;
           end
       end
    end

    function [rest] = B3_eval(n1,n2,n3,coeff,dataIn)
       count =1;
       rest = zeros(size(dataIn));
       for k1 = 0:n1
           for k2 = 0:n2
               for k3= 0:n3
                   rest(:,1) = rest(:,1)  + coeff(count,1)*B3(n1,k1,n2,k2,n3,k3,dataIn);
                   rest(:,2) = rest(:,2)  + coeff(count,2)*B3(n1,k1,n2,k2,n3,k3,dataIn);
                   rest(:,3) = rest(:,3)  + coeff(count,3)*B3(n1,k1,n2,k2,n3,k3,dataIn);
                   count= count +1;
               end
           end
       end
    end

    function [rest] = B3_der_eval(n1,n2,n3,coeff,dataIn)
        count = 1;
       [a,b] = size(dataIn);
       rest = zeros(a,b+6);
       for k1 = 0:n1
           for k2 = 0:n2
               for k3 = 0:n3
                    rest(:,1:3) = rest(:,1:3)  + coeff(count,1)*B3_der(n1,k1,n2,k2,n3,k3,dataIn);
                    rest(:,4:6) = rest(:,4:6)  + coeff(count,2)*B3_der(n1,k1,n2,k2,n3,k3,dataIn);
                    rest(:,7:9) = rest(:,7:9)  + coeff(count,3)*B3_der(n1,k1,n2,k2,n3,k3,dataIn);
                    count= count +1;
               end
           end
       end        
    end

    function [rest] = B2_der_eval(n1,n2,coeff,dataIn)
       count =1;
       [a,b] = size(dataIn);
       rest = zeros(a,b+2);
       for k1 = 0:n1
           for k2 = 0:n2
               rest(:,1:2) = rest(:,1:2)  + coeff(count,1)*B2_der(n1,k1,n2,k2,dataIn);
               rest(:,3:4) = rest(:,3:4)  + coeff(count,2)*B2_der(n1,k1,n2,k2,dataIn);
               count= count +1;
           end
       end
    end

    function [res] = B(n,k,data)
        % assumed data varies from -1 to 1
        if n<k || k<0
            res = zeros(size(data));
            return
        end
        data = (data+1)/2;
        res = nchoosek(n,k)*((data.^k).*((1-data).^(n-k)));
    end

    function [res] = B_der(n,k,data)
        res = n*(B(n-1,k-1,data) - B(n-1,k,data))/2;
    end
    
end
