%./TestCyl_Vort_EL ../data/CylExamples/Cy3l_Re500.xml 2 0.5 3


clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];

filePoly = 3;
poly = 2;


%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize

Cyl_Max = 6.3;
Cyl_Min = -5.2571;


for ijk = 115:900
    filename = strcat( 'data_EL/','Cy3l_Re500_',int2str(ijk),'_' );
    filenameFrPrint = strcat( 'frame_EL/','Cy3l_Re500_',sprintf('%05d',ijk) );
    
    efname = strcat( filename,'pE_EL.txt');
    xfname = strcat( filename,'pX_EL.txt');
    yfname = strcat( filename,'pY_EL.txt');
    
%     upfname = strcat( filename,'pU_p_EL.txt');
%     vpfname = strcat( filename,'pV_p_EL.txt');
%     usfname = strcat( filename,'pU_s_EL.txt');
%     vsfname = strcat( filename,'pV_s_EL.txt');
%     
%     uxpfname = strcat( filename,'pUx_p_EL.txt');
%     vxpfname = strcat( filename,'pVx_p_EL.txt');
%     uxsfname = strcat( filename,'pUx_s_EL.txt');
%     vxsfname = strcat( filename,'pVx_s_EL.txt');
%     
%     uypfname = strcat( filename,'pUy_p_EL.txt');
%     vypfname = strcat( filename,'pVy_p_EL.txt');
%     uysfname = strcat( filename,'pUy_s_EL.txt');
%     vysfname = strcat( filename,'pVy_s_EL.txt');
    
    
    Vorpfname = strcat( filename,'pVor_p_EL.txt');
 %   Vorsfname = strcat( filename,'pVor_s_EL.txt')

    disp(xfname);

    elval = floor( textread(efname, '%f\t') + 0.0001);
    xval = textread(xfname, '%f\t');
    yval = textread(yfname, '%f\t');
    
%     upval = textread(upfname, '%f\t');
%     vpval = textread(vpfname, '%f\t');
%     usval = textread(usfname, '%f\t');
%     vsval = textread(vsfname, '%f\t');
%     
%     uxpval = textread(uxpfname, '%f\t');
%     vxpval = textread(vxpfname, '%f\t');
%     uxsval = textread(uxsfname, '%f\t');
%     vxsval = textread(vxsfname, '%f\t');
%     
%     uypval = textread(uypfname, '%f\t');
%     vypval = textread(vypfname, '%f\t');
%     uysval = textread(uysfname, '%f\t');
%     vysval = textread(vysfname, '%f\t');
    
    vorpval = textread(Vorpfname, '%f\t');
%    vorsval = textread(Vorsfname, '%f\t');
    
    [unique_ids,id_offset,id_tags] = unique(elval);
 %%   
    % for each tag
        % get the data.
        % vis the data
    %end
    num_unique_ids = (max(size(unique_ids)));
    for tag = unique_ids'
        
        el_indices = find(elval== (zeros(size(elval))+tag) );
%         if (tag ~= num_unique_ids)
%             el_indices = (id_offset(tag) :1: (id_offset(tag+1))-1);
%         else
%             el_indices = (id_offset(tag) :1: max(size(xval)));
%         end
        
        el_x = xval(el_indices);
        el_y = yval(el_indices);
        el_p = vorpval(el_indices);
%         el_s = vorsval(el_indices);
%         el_sy = usval(el_indices);
%         el_sS = vsval(el_indices);
        s = sqrt(max(size(el_x)));
        
        el_x = reshape(el_x,[s,s]);
        el_y = reshape(el_y,[s,s]);
        el_p = reshape(el_p,[s,s]);
%         el_s = reshape(el_s,[s,s]);    
%         el_sy = reshape(el_sy,[s,s]); 
%         el_sS = reshape(el_sS,[s,s]); 
 
%         figure(2),
%         surf(el_x,el_y,el_p); hold on;
%         shading interp;
%         xlabel('x'); ylabel('y');
%         
%         
         figure(ijk),
         contour(el_x,el_y,el_p, (Cyl_Min :.3: Cyl_Max) ,'LineWidth',lw); hold on;
        
%         figure(5)
%         surf(el_x,el_y,el_s); hold on;
%         shading interp;
                
%         figure(6),
%         contour(el_x,el_y,el_s, (Cyl_Min :.3: Cyl_Max) ); hold on;
%         
%         figure(7)
%         surf(el_x,el_y,el_sy); hold on;
%         shading interp;
%                 
%         figure(8),
%         contour(el_x,el_y,el_sy, (min(vorpval(:)) :.3: max(vorpval(:))) ); hold on;
%         
%         figure(9)
%         surf(el_x,el_y,el_sS); hold on;
%         shading interp;
%                 
%         figure(10),
%         contour(el_x,el_y,el_sS, (min(vorpval(:)) :.3: max(vorpval(:))) ); hold on;
        
    end
    
    figure(ijk),
       xlim([2.5,10.5]);
    ylim([-4,4]);
    grid off; axis off;
   print(strcat(filenameFrPrint), '-djpeg','-r100');
    hold off;
    close(ijk);
end


%%
% % figure(2),
% % xlim([2.5,10.5]);
% % ylim([-4,4]);
% % view([0,90])
% % grid off;
% % axis off;
% % %print(strcat('../paperUseImages/',filenameFrPrint,'Surf_DG'), '-dpng','-r500');
% % plotNekMesh('../../data/CylExamples/Cy2l.xml');
% %    xlim([2.5,10.5]);
% %     ylim([-4,4]);
% % %print(strcat('../paperUseImages/',filenameFrPrint,'Surf_DG_MESH'), '-dpng','-r500');
% % 
% % %saveas(2,'../images/2DImages/vor_DG1.png');
% % 
% % % figure(3)
% % % xlim([2.5,10.5]);
% % % ylim([-4,4]);
% % % %view([-20.5,62])
% % % view([0,90])
% % % grid off;
% % % %saveas(3,'../images/2DImages/vor_SI1.png');
% % 
% % figure(4),
% % xlim([2.5,10.5]);
% % ylim([-4,4]);
% % axis off;
% % %print(strcat('../paperUseImages/',filenameFrPrint,'Surf_DG_Contour'), '-dpng','-r500');
% % plotNekMesh('../../data/CylExamples/Cy2l.xml');
% %    xlim([2.5,10.5]);
% %     ylim([-4,4]);
% % %print(strcat('../paperUseImages/',filenameFrPrint,'Surf_DG_Contour_MESH'), '-dpng','-r500');

%%

% 
% figure(5)
% view([0,90])
% xlim([2.5,10.5]);
% ylim([-4,4]);
% axis off;
% print(strcat('../paperUseImages/',filenameFrPrint,'Surf_SIACx_'), '-dpng','-r500');
% plotNekMesh('../../data/CylExamples/Cy2l.xml');
%    xlim([2.5,10.5]);
%     ylim([-4,4]);
% print(strcat('../paperUseImages/',filenameFrPrint,'Surf_SIACx_MESH'), '-dpng','-r500');
% 
% figure(6), hold on;
% xlim([2.5,10.5]);
% ylim([-4,4]);
% axis off;
% print(strcat('../paperUseImages/',filenameFrPrint,'Surf_SIACx_Contour'), '-dpng','-r500');
% plotNekMesh('../../data/CylExamples/Cy2l.xml');
%    xlim([2.5,10.5]);
%     ylim([-4,4]);
% print(strcat('../paperUseImages/',filenameFrPrint,'Surf_SIACx_Contour_MESH'), '-dpng','-r500');
%%

% figure(7)
% view([0,90])
% xlim([2.5,10.5]);
% ylim([-4,4]);
% axis off;
% %print(strcat('../paperUseImages/',filenameFrPrint,'Surf_SIACy_'), '-dpng','-r500');
% plotNekMesh('../../data/CylExamples/Cy2l.xml');
%    xlim([2.5,10.5]);
%     ylim([-4,4]);
% %print(strcat('../paperUseImages/',filenameFrPrint,'Surf_SIACy_MESH'), '-dpng','-r500');
% 
% figure(8), hold on;
% xlim([2.5,10.5]);
% ylim([-4,4]);
% axis off;
% %print(strcat('../paperUseImages/',filenameFrPrint,'Surf_SIACS_Contour'), '-dpng','-r500');
% plotNekMesh('../../data/CylExamples/Cy2l.xml');
%    xlim([2.5,10.5]);
%     ylim([-4,4]);
% %print(strcat('../paperUseImages/',filenameFrPrint,'Surf_SIACS_Contour_MESH'), '-dpng','-r500');
% 
% 
% 
% 
% figure(9)
% view([0,90])
% xlim([2.5,10.5]);
% ylim([-4,4]);
% axis off;
% %print(strcat('../paperUseImages/',filenameFrPrint,'Surf_SIACy_'), '-dpng','-r500');
% plotNekMesh('../../data/CylExamples/Cy2l.xml');
%    xlim([2.5,10.5]);
%     ylim([-4,4]);
% %print(strcat('../paperUseImages/',filenameFrPrint,'Surf_SIACy_MESH'), '-dpng','-r500');
% 
% figure(10), hold on;
% xlim([2.5,10.5]);
% ylim([-4,4]);
% axis off;
% %print(strcat('../paperUseImages/',filenameFrPrint,'Surf_SIACA45_Contour'), '-dpng','-r500');
% plotNekMesh('../../data/CylExamples/Cy2l.xml');
%    xlim([2.5,10.5]);
%     ylim([-4,4]);
% %print(strcat('../paperUseImages/',filenameFrPrint,'Surf_SIACA45_Contour_MESH'), '-dpng','-r500');



