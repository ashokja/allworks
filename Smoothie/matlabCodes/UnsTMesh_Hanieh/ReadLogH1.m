% read log file.
close all;
clear;

prefix = '../../code/build_fsm3/';
rets =[{'05'},{'025'},{'0125'}];



for Poly = 2:4
%strt = strcat( '\hline','&','&','&','$\mathcal{P}$=',num2str(Poly-1),'&','&','&',' \\ \hline');
strt = strcat( '\hline','&$\mathcal{P}^',num2str(Poly-1),'$ &','\\ \hline');
disp(strt);
for ret = rets
H1 = 0.0;
H2 = 0.0;
for H = 1:2    
	res = cell2mat(ret);
    %filename = strcat('LOGSH',int2str(H),'_NM',int2str(Poly),'_','unsMeshvar1_',res,'.log');
    filename = strcat('LOGH',int2str(H),'_NM',int2str(Poly),'_','Mesh',res,'.log');

    logFileName = strcat(prefix,filename);
    fid = fopen(logFileName,'r'); %# open csv file for reading
    line = fgets(fid);
	if (line(1) == 'H')
		if (H ==1)
        	H1 = sscanf(line,'H1 =\t%f');
		else
        	H2 = sscanf(line,'H2 =\t%f');
		end
	end 
end
	strt = strcat('M',res,'&',num2str(H1),' & ',num2str(H2), ' \\ \hline');
	disp(strt);
end 
end
