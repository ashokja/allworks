% striaght sided element 2D and 3D.
clear;

%% 2D Matrix elements 

% E1 = (-1:.1:1);
% E2 = (-1:.1:1);
% [X,Y] = meshgrid(E1,E2);
% Z = (((X+1)/2).*((Y+1)/2));
% 
% plot3(X,Y,Z,'*'); hold on;
% 
% Xa = [-1,-1];
% Xb = [4,-1];
% Xc = [4,1];
% Xd = [-1,1];
% 
% count =1;
% for e1 = E1
%     for e2 = E2
%         c = Xa*(1-e1)*(1-e2)/4 + ...
%                 Xb*(1+e1)*(1-e2)/4+...
%                     Xc*(1+e1)*(1+e2)/4+...
%                         Xd*(1-e1)*(1+e2)/4;
%          coords(count,:) = c;
%          Zout(count) = ((e1+1)/2).*((e2+1)/2)+1;
%          count= count +1;
%     end
% end
% %Z = coords(:,1).*coords(:,2)*0.3+1;
% plot3(coords(:,1), coords(:,2),Zout,'*');hold off;
% 
% 
% for e1 = E1
%     for e2 = E2
%         J = jacobian2D(e1,e2,Xa,Xb,Xc,Xd);
%         inv(J);
%     end
% end

% Velocity is at data points in world space.
% Map Velocity in reference space.
% Calcualte coeff for basis functions. 
% 


%% 3D T and J from here. 
E1 = (-1:.4:1);
E2 = (-1:.4:1);
E3 = (-1:.4:1);
[X,Y,Z] = meshgrid(E1,E2,E3);
%U = X; V = Y; W = Z;

%plot3(X,Y,Z,'*'); hold on;
% use quiver3 instead of plot;
%quiver3(X,Y,Z,U,V,W); 

Xa = [-1,-1,-1]+2;
Xb = [1,-1,-1]+2;
Xc = [1,1,-1]+2;
Xd = [-1,1,-1]+2;
Xe = [-1,-1,1]+3;
Xf = [1,-1,1]+3;
Xg = [1,1,1]+3;
Xh = [-1,1,1]+3;

count =1;
for e1 = E1
    for e2 = E2
        for e3 = E3
            c = Xa*(1-e1)*(1-e2)*(1-e3)/8 + ...
                    Xb*(1+e1)*(1-e2)*(1-e3)/8+...
                        Xc*(1+e1)*(1+e2)*(1-e3)/8+...
                            Xd*(1-e1)*(1+e2)*(1-e3)/8+...
                                Xe*(1-e1)*(1-e2)*(1+e3)/8 + ...
                                    Xf*(1+e1)*(1-e2)*(1+e3)/8+...
                                        Xg*(1+e1)*(1+e2)*(1+e3)/8+...
                                            Xh*(1-e1)*(1+e2)*(1+e3)/8;
         J = jacobian3D(e1,e2,e3,Xa,Xb,Xc,Xd,Xe,Xf,Xg,Xh);
         coords3d_ref(count,:) = [e1,e2,e3];
         coords3d(count,:) = c;
         vel = cross([e1,e2,e3],[0,1,0])+ [0,0.3,0];
         dataOutW(count,:) = J*vel';
         dataOutR(count,:) = vel;
         count= count +1;
        end
    end
end
%%

%Jacobian at each point
% for e1 = E1
%     for e2 = E2
%         for e3 = E3
%             jacobian3D(e1,e2,e3,Xa,Xb,Xc,Xd,Xe,Xf,Xg,Xh)
%         end
%     end
% end

% map data from world space to reference space.
figure(2);
quiver3(coords3d(:,1), coords3d(:,2),coords3d(:,3),...
                    dataOutR(:,1),dataOutR(:,2),dataOutR(:,3));
                
%build coeff for reference space.
n1 = 4; n2 = 4; n3 = 4;
coeff_ref = buildCoeff(n1,n2,n3,coords3d_ref,dataOutR);

%visualize velocity in reference space.
figure(3)
vizualizeCoeff(n1,n2,n3,coeff_ref);

% divide vel into rotation and reduced velocity.
[coeff_red_ref,coeff_rot_ref] = divideVel(n1,n2,n3,coeff_ref,coords3d_ref,dataOutR);

%visualize velocity in reference space.
figure(4);
vizualizeCoeff(n1,n2,n3,coeff_red_ref);

figure(5);
vizualizeCoeff(n1,n2,n3,coeff_rot_ref);
%%
%find zeros on each face.
tic
[sol_seeds,goodSeeds,solAtSeeds] = zerosAtFaces(n1,n2,n3,coeff_rot_ref);
toc
[seedNum,dim] = size(goodSeeds);

% print all good seeds
count =1; tps = {}; tpsVel = {};
for i = 1:seedNum
    for j = 1:dim
        if (goodSeeds(i,j) ==1)
             sol_seeds(i,j,:)
        end
    end
end
%
%% using actual velocity.
tic
count =1; tps = {}; tpsVel = {};
for i = 1:seedNum
    for j = 1:dim
        if (goodSeeds(i,j) ==1)
            [trace,traceVel] = traceThePoint(n1,n2,n3,coeff_ref, T_refToWorld(sol_seeds(i,j,1), ...
                                    sol_seeds(i,j,2),sol_seeds(i,j,3),Xa,Xb,Xc,Xd,Xe,Xf,Xg,Xh) );
            tps(count) = {trace};
            tpsVel(count) = {traceVel};
            count = count +1;
        end
    end
end
toc
%%
[~,b] = size(tps);
figure(6);
plotBoundingBox();
for i =1:b
    trace = cell2mat(tps(i));
    plot3(trace(:,1),trace(:,2),trace(:,3),'r*');
    hold on;
end
hold off;
%%
plotBoundingBox(Xa,Xb,Xc,Xd,Xe,Xf,Xg,Xh)
% transfrom points from ref to World.
%%

% label all good seeds on world space.
count =1; tps = {}; tpsVel = {};
for i = 1:seedNum
    for j = 1:dim
        if (goodSeeds(i,j) ==1)
             c_ref = sol_seeds(i,j,:);
             c_wor = T_refToWorld(c_ref(1),c_ref(2),c_ref(3),Xa,Xb,Xc,Xd,Xe,Xf,Xg,Xh);
             plot3(c_wor(1),c_wor(2),c_wor(3),'r*'); hold on;
        end
    end
end
hold off;

