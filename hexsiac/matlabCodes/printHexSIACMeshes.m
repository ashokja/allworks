
clear;
close all;
%idea is to print the meshes for visualization in hex siac paper.
%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.2;      % LineWidth
msz = 8;       % MarkerSize



%hex union jack

plotNekMesh('../data/P1_hex_sym_15.xml');
axis off;
%print()
%print('hex_UnionJack_mesh', '-dpng','-r500');


% figure(2)
% %hex sym.
% plotNekMeshSP('../data/hexSymout.xml',[12 6 19 9 27 33 41 55 47 24 38 52 1 15 29 43 56 3],'c','-',10);
% axis equal; hold on;
% axis off;
% %print('hex_mesh_1', '-dpng','-r500');
% yl = 0.08248
% yu = 0.989
% %line([0 1 1 0 0],[yl yl yu yu yl],'color','k');
% %xlim([0 1]);
% %ylim([0 1.4]);
% 
% %print('hex_Sym_mesh', '-dpng','-r500');
% 
% 
% figure(3)
% %hex sym.
% plotNekMeshSP('../data/hexSymout.xml',[1000],'k','-',10); hold on;
% plotNekMeshSP('../data/hexSymout.xml',[12 6 19 9 27 33 41 55 47 24 38 52 1 15 29 43 56 3],'k','-',12);
% axis equal; hold on;
% axis off;
% yl = 0.08248
% yu = 0.989
% %line([0 1 1 0 0],[yl yl yu yu yl],'color','k','LineStyle','-','LineWidth',lw);
% xlim([0 1]);
% ylim([0 1.2]);
% %print('hex_mesh_21', '-dpng','-r500');



