% parametric curves.  
function [c1,c2,c3] = parmetricCurves(p1,t1,poly)
    switch nargin
        case 0,
            p1 = rand(6,3);
            t1 = (0:.2:1);
            poly = 3;
    end
    size(p1);
    [~,s_t1] = size(t1);
    
    mat = zeros( s_t1, poly+1);
    %count =1;
    for i = 0:poly
        mat(:,i+1) = B(poly,i,t1);
        %count = count+1;
    end

    c1 = mat\p1(:,1);
    c2 = mat\p1(:,2);
    c3 = mat\p1(:,3);
    
    function [res] = B(n,k,data)
        % assumed data varies from -1 to 1
        if n<k || k<0
            res = zeros(size(data));
            return
        end
        %data = (data+1)/2;
        res = nchoosek(n,k)*((data.^k).*((1-data).^(n-k)));
    end

end