%Aim: To create a random curve in 3D.
%Sub Aim1: create a cuve in 2d first. 
%Sub Aim2: extend the curve to 3d.

% Let say the curve is dregree 3 in x and y and z.

%current problem is that random curve does not touch the faces.
function [xy,distance,t_a,mapxy,mapxyV,coeff] = test_run(r)
    switch nargin,
        case 0
            r =1;
            sampledata = 50;
        case 1
            sampledata = 50;
    end
    %s = RandStream('mt19937ar','Seed',r);
    %RandStream.setDefaultStream(s);
    rng('default');
    rng(r);
    data = linspace(-1,1,sampledata);
    n = 3;
    figure(7);
    %[xcoeff,ycoeff,zcoeff] = pickRand6twice(n);
    [xcoeff,ycoeff,zcoeff] = pickSimpleCase(n);
    coeff = [xcoeff;ycoeff;zcoeff];
    %coeff = [0,1,1,0];
    xdata = B_eval(xcoeff,data);
    ydata = B_eval(ycoeff,data);
    zdata = B_eval(zcoeff,data);
    xderdata = B_der_eval(xcoeff,data);
    yderdata = B_der_eval(ycoeff,data);
    zderdata = B_der_eval(zcoeff,data);

    % creating 
    curvexy(:,1) = xdata; curvexy(:,2) = ydata; curvexy(:,3) = zdata;
    curveDataIn = lglnodes(11);
    mapxy = changeGridToArray(curveDataIn);
    [xy,distance,t_a] = distance2curve(curvexy,mapxy,'spline');

    [X,Y,Z] = meshgrid(curveDataIn);
    [a,~] = size(mapxy); 
    b = int64(a^(1/3));
    DISTANCE = reshape(distance,b,b,b);
    TA = reshape(t_a,b,b,b);
    
    %contourslice(X,Y,Z,DISTANCE,[],curveDataIn,[]); hold on;
    %contourslice(X,Y,Z,TA,[],[],curveDataIn); hold on;
    t_a_basis = t_a*2-1;
    x_NonCurve = B_der_eval(xcoeff,t_a_basis);
    y_NonCurve = B_der_eval(ycoeff,t_a_basis);
    z_NonCurve = B_der_eval(zcoeff,t_a_basis);
    NonCurve = [x_NonCurve,y_NonCurve,z_NonCurve];
    %x_LofCurve = B_eval(xcoeff,xy);
    %y_LofCurve = B_eval(ycoeff,xy);
    %z_LofCurve = B_eval(zcoeff,xy);
    %LofCurve = [x_LofCurve;y_LofCurve;z_LofCurve];
    VecCurveToPoint = mapxy - xy;
    %cVec = zeros(size(x_NonCurve));
    mapxyV = cross(NonCurve,VecCurveToPoint,2) + ([1./(distance+1),1./(distance+1),1./(distance+1)].*NonCurve);
    %plotQuiver(mapxy,mapxyV); hold on;

     drawStreamLine(mapxy,mapxyV,coeff);
end

function [] = plotQuiver(data,dataV)
    quiver3(data(:,1), data(:,2), data(:,3), ...
                dataV(:,1), dataV(:,2), dataV(:,3));
end



function [data] = changeGridToArray(x)
   [X,Y,Z] = meshgrid(x,x,x);
   data(:,1) = reshape(X,[],1);
   data(:,2) = reshape(Y,[],1);
   data(:,3) = reshape(Z,[],1);
end

function [xcoeff,ycoeff,zcoeff] = pickSimpleCase(n)
    xcoeff = rand(1,n+1);
    ycoeff = (-1:2/n:1);
    zcoeff = zeros(1,n+1);
end

function [xcoeff,ycoeff,zcoeff] = pickRand6twice(n)
    xcoeff = rand(1,n+1)*2-1;
    ycoeff = rand(1,n+1)*2-1;
    zcoeff = rand(1,n+1)*2-1;
    r = floor(rand(1,1)*6)+1;
    switch r % for starting point
        case 1, %x = -1 plane
            xcoeff(1) = -1;
        case 2, %x = =1 place
            xcoeff(1) = 1;
        case 3, %y = -1 plane
            ycoeff(1) = -1;
        case 4, %y = =1 place
            ycoeff(1) = 1;
        case 5, %z = -1 plane
            zcoeff(1) = -1;
        case 6, %z = =1 place
            zcoeff(1) = 1;
    end
    
    r = floor(rand(1,1)*6)+1;
    switch r % for end point
        case 1, %x = -1 plane
            xcoeff(n+1) = -1;
        case 2, %x = =1 place
            xcoeff(n+1) = 1;
        case 3, %y = -1 plane
            ycoeff(n+1) = -1;
        case 4, %y = =1 place
            ycoeff(n+1) = 1;
        case 5, %z = -1 plane
            zcoeff(n+1) = -1;
        case 6, %z = =1 place
            zcoeff(n+1) = 1;
    end
    
end

