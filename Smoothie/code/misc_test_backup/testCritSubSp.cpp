#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"

#include <iomanip>

#define MAX_ITER 10000
#define MAX_ITER_NI 100
#define TOLERENCE_S 1e-16
#define TOLERENCE_F 1e-7
#define TOLERENCE_P 1e-14
#define TOLERENCE_A 1e-14
#define TOLERENCE_NN 1e-8
#define ALPHAMAX 1.0

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>

using namespace std;

int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg 0/1"<<endl<<"\t 0 for writing data"<< endl<<"\t 1 for reading data." << endl;
		//cout << "3nd arg Mesh resolution you want to write to." << endl;
		//cout << "3nd arg polynomial degree filter you want to apply" << endl;
		//cout << "4rd arg meshscaling you want to use." << endl;
	//	cout << "4th Angle of SIAC derivative in deg -90 to 90 allowed" << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();

//	HNM2D->LoadData( fname+ ".chk",var);

	HNM2D->LoadMesh(var[0]);
	HNM2D->LoadMesh(var[1]);

	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fPhys:" << tNquadPts<< endl;
	cout << "fCoeffs:" << HNM2D->m_expansions[0]->GetNcoeffs()<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);
	Array<OneD,NekDouble> u(tNquadPts);
	Array<OneD,NekDouble> v(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}

	NektarBaseClass k;
	if( 0 == atoi(argv[2]) )
	{	
		k.writeNekArray(xc0,fname+"_xc0."+"txt");
		k.writeNekArray(xc1,fname+"_xc1."+"txt");
		return 0;
	}
	else
	{
		k.readNekArray(u, fname+"_u.txt");
		k.readNekArray(v, fname+"_v.txt");
		//k.printNekArray(v,0);
	}

	// Now use Newton-Rapson to find the zero u and zero v.
	
	HNM2D->m_expansions[0]->FwdTrans_IterPerExp( u,HNM2D->m_expansions[0]->UpdateCoeffs());
	HNM2D->m_expansions[0]->BwdTrans_IterPerExp( HNM2D->m_expansions[0]->GetCoeffs(),
			HNM2D->m_expansions[0]->UpdatePhys());
	
	HNM2D->m_expansions[1]->FwdTrans( v,HNM2D->m_expansions[1]->UpdateCoeffs());
	HNM2D->m_expansions[1]->BwdTrans( HNM2D->m_expansions[1]->GetCoeffs(),
			HNM2D->m_expansions[1]->UpdatePhys());

	Array<OneD,NekDouble> u_DG = HNM2D->m_expansions[0]->GetPhys();	
	Array<OneD,NekDouble> v_DG = HNM2D->m_expansions[1]->GetPhys();

	Array<OneD,NekDouble> ux_DG(tNquadPts), uy_DG(tNquadPts), vx_DG(tNquadPts), vy_DG(tNquadPts);
	HNM2D->m_expansions[0]->PhysDeriv( u_DG, ux_DG, uy_DG);
	HNM2D->m_expansions[1]->PhysDeriv( v_DG, vx_DG, vy_DG);




	vector<NekDouble> elidOffsets;
	for ( int i=0; i< HNM2D->m_expansions[0]->GetExpSize();i++)
	{
		elidOffsets.push_back(HNM2D->m_expansions[0]->GetPhys_Offset(i));
	}
	HNM2D->writeNekArray(xc0, fname+"_SubSp_x.txt");	
	HNM2D->writeNekArray(xc1, fname+"_SubSp_y.txt");	
	HNM2D->writeNekArray(u_DG, fname+"_SubSp_u.txt");	
	HNM2D->writeNekArray(v_DG, fname+"_SubSp_v.txt");	
	HNM2D->writeNekArray(elidOffsets, fname+"_SubSp_eo.txt");	
	
	return 0;
}
