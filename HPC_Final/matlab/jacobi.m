function x = jacobi(A,b,n)
    [k,del] = size(b);
    x = b;
    d = diag(A);
    d
    for i =1:n
        r = b- A*x;
        x = x+ r/3.33;
        %del has to be in [n,1] dimension for this to work.
    end
end