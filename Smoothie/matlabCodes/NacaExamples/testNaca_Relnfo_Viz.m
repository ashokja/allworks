%testNaca_RelInfo
% this file visualizes Element Ids, max Length, min Length 

clear;
close all;


UsebkupData = 'cur_feb13/' % has only data Ux_ss so remember to comment below also.

%UsebkupData = 'cur_feb10/'

%UsebkupData = ''
%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05'
filename = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_IDLongShortEdge' );
filepath = strcat('data/',UsebkupData,filename);

xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );
unElIdsfname = strcat(filepath,'_UniqueIds.txt');
longEdfname = strcat(filepath,'_LongEdge.txt');
shortEdfname = strcat(filepath,'_ShortEdge.txt');

xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');
unElIdsval = textread (unElIdsfname, '%f\t');
longEdsval = textread (longEdfname, '%f\t');
shortEdsval = textread (shortEdfname, '%f\t');

epos = floor(epos +0.0001);
unElIdsval = floor(unElIdsval + 0.0001);

s = floor( (max(size(xpos)))^(1/2) +0.1);
xVpos = reshape(xpos,s,s);
yVpos = reshape(ypos,s,s);
zVpos = reshape(zpos,s,s);
eVpos = reshape(unElIdsval,s,s);


[a,b] = unique(unElIdsval);
uniqueIds = a;
longEds = longEdsval(b);
shortEds = shortEdsval(b);

%%
figure(1),
subplot(2,1,1), histogram(longEds,20); title('maximum Edge length histogram');
subplot(2,1,2), histogram(shortEds,20);title('minimum Edge length histogram');
%print('images/F3b13/ElementSideHistogram','-dpng', '-r500')

%%
figure(2),
surf(yVpos,zVpos, eVpos), shading interp,view(0,90);

figure(3),
contour(yVpos,zVpos, eVpos+0.001, 900);
title('Element boundaries (color and thickness have no meaning)')
%print('images/F3b13/ElementBoundaries','-dpng', '-r500')

figure(4),
%[px,py] = gradient(eVpos+0.001);
px = diff(eVpos,1,1);
py = diff(eVpos,1,2);
resl = abs(px(1:99,1:99)) + abs(py(1:99,1:99)) >0 ;
surf(resl+0.001), shading interp;
figure(5)
imshow(resl);






