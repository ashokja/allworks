#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include "OneSidedSIAC.h"
#include <omp.h>

#include <iomanip> // std::setprecision
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
#define TOLERENCE_VIZ 1e-6

using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 7)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg Type of Filter to use." << endl;
		cout << "4th arg Res. " << endl;
		cout << "5th arg Number to threds. " << endl;
		cout << "6th arg chunk size " << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM2D->LoadMesh(var[0]);
	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	//cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);
//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);

	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]));
		//fce[i] = std::cos(2.0*M_PI*(xc0[i]))*std::cos(2.0*M_PI*(xc1[i]));
	}
	
	

	HNM2D->m_expansions[0]->FwdTrans(fce,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	 ece = HNM2D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM2D->m_expansions[0]->GetCoeffs();
	HNM2D->m_Arrays.push_back(ece);
	HNM2D->LoadExpListIntoRTree();
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();

	int Ny = atoi(argv[4]);
	
	NekDouble PtsX=0.0,PtsY=0.0,PtsZ=0.0,tmin=0.0,tmax=scaling*4,meshTShift=0.0;
    NekDouble startT = 0.0;
   NekDouble endT = 1.0 - scaling*4; 
	Array<OneD,NekDouble> direction(3,0.0);
	direction[0] = 1.0;
	NekDouble startTime = omp_get_wtime();
	for(int i=0; i <Ny; i++)
	{
        tmin = startT+ (endT-startT)/Ny*i;
        tmax = tmin + scaling*4;
		vector<NekDouble> HvalX,HvalY,HvalZ,HvalT;
		HNM2D->GetBreakPts(PtsX, PtsY, PtsZ,direction, tmin,tmax, HvalX, HvalY, HvalZ, HvalT);
        HNM2D->CanTRangebeApplied(PtsX,PtsY,PtsZ,direction,tmin,tmax,meshTShift);
        vector<int> t_GIDs,t_EIDs;
        HNM2D->GetListOfGIDs(PtsX,PtsY,PtsZ,direction, HvalT,t_GIDs,t_EIDs);
		for(int j=0;j<HvalT.size()-1;j++)
		{
			NekDouble startT = HvalT[j];
			NekDouble endT = HvalT[j+1];
			Array<OneD,NekDouble> ptsX(11),ptsY(11),ptsZ(11),vals(11);
			for(int ii=0;ii<=10;ii++)
			{
				NekDouble ith = startT + (endT-startT)*ii/10.0;
				ptsX[ii] = PtsX+ith*direction[0];
				ptsY[ii]= PtsY+ith*direction[1];
				ptsZ[ii]= PtsZ+ith*direction[1];
			}
	  //      HNM2D->EvaluateAt(ptsX,ptsY,ptsZ,t_GIDs[j],t_EIDs[j],vals,0);
        }
    }
	NekDouble	timeTakenS = omp_get_wtime()-startTime;
  //  cout << "START" << endl;
//	cout << "Serial\t "<< timeTakenS <<endl;

    int chunk = atoi(argv[6]);
    
	startTime = omp_get_wtime();
	#pragma omp parallel for schedule(dynamic,chunk) num_threads(atoi(argv[5])) 
	for(int i=0; i <Ny; i++)
	{ 
        tmin = startT+ (endT-startT)/Ny*i;
        tmax = tmin + scaling*4;
		vector<NekDouble> HvalX,HvalY,HvalZ,HvalT;
		HNM2D->GetBreakPts(PtsX, PtsY, PtsZ,direction, tmin,tmax, HvalX, HvalY, HvalZ, HvalT);
        HNM2D->CanTRangebeApplied(PtsX,PtsY,PtsZ,direction,tmin,tmax,meshTShift);
        vector<int> t_GIDs,t_EIDs;
        HNM2D->GetListOfGIDs(PtsX,PtsY,PtsZ,direction, HvalT,t_GIDs,t_EIDs);
		for(int j=0;j<HvalT.size()-1;j++)
		{
			NekDouble startT = HvalT[j];
			NekDouble endT = HvalT[j+1];
			Array<OneD,NekDouble> ptsX(11),ptsY(11),ptsZ(11),vals(11);
			for(int ii=0;ii<=10;ii++)
			{
				NekDouble ith = startT + (endT-startT)*ii/10.0;
				ptsX[ii] = PtsX+ith*direction[0];
				ptsY[ii]= PtsY+ith*direction[1];
				ptsZ[ii]= PtsZ+ith*direction[1];
			}
	//        HNM2D->EvaluateAt(ptsX,ptsY,ptsZ,t_GIDs[j],t_EIDs[j],vals,0);
        }
    }
	NekDouble timeTakenP = omp_get_wtime()-startTime;
//	cout << "Parallel\t "<< timeTakenP <<endl;

    NekDouble speedup = timeTakenS/timeTakenP ;
    NekDouble Eff = speedup/atoi(argv[5])*100;
//    cout << "Tds:\tspup" <<"\t" << "Eff" << "\t" << endl;
    std::cout.precision(3); 
    cout << argv[5]<<"\t"<<speedup <<"\t" << Eff <<endl;
    return 0;

}

