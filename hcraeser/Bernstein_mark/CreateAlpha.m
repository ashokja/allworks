function [A_new] = CreateAlpha(d,n)
    if n ==0
        A_new = zeros(1,d);
        return;
    end
    A_p = CreateAlpha(d,n-1);
    [s,dum] = size(A_p);
    count =1;
    for si = 1:s
        new = A_p(si,:);
        for di = 1:d
            newd = new;
            newd(1,di) = new(1,di)+1;
            test  = true;
            for ci =1:count-1
                if A_new(ci,:) == newd
                    test = false;
                    break;
                end
            end
            if test
                A_new(count,:)=new;
                A_new(count,di) = A_new(count,di)+ 1;
                count = count+1;
            end
        end
    end
    return;
end
