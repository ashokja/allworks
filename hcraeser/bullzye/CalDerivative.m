function UAtCheb_D = CalDerivative(UAtCheb,DCheb,axis)
    
    [Xdim,~,Zdim] = size(UAtCheb);
    UAtCheb_D = zeros(size(UAtCheb));
        % X partial Derivative
    if axis == 'X' || axis == 'x'
        for k = 1:Zdim
            UAtCheb_D(:,:,k) = DCheb * UAtCheb(:,:,k); 
        end
    elseif axis == 'Y' || axis == 'y'
        for k = 1:Zdim
            UAtCheb_D(:,:,k) = (DCheb * UAtCheb(:,:,k)')';
        end
    elseif axis == 'Z' || axis == 'z'
        for i = 1:Xdim
            UAtCheb_D(i,:,:) = (DCheb * squeeze(UAtCheb(i,:,:))')';
        end
    else
        'Wrong Input'
    end
    
end
