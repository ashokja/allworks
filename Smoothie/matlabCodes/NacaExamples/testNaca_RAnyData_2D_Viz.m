% Read data from testNaca_RAnyRes
% s is used in variables but it ss data.

clear;
close all;


UsebkupData = 'cur_feb13/' % has only data Ux_ss so remember to comment below also.
UsebkupDataC = 'cur_feb12/'

%UsebkupData = 'cur_feb10/'

%UsebkupData = ''
%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05';
filename = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_Any2LSIACVAR' );
filenameC = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ACRFDVar' );

filepath = strcat('data/',UsebkupData,filename);
filepathC = strcat('data/',UsebkupDataC,filenameC);

xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );

usfname = strcat(filepathC,'_pU_ss.txt' );
vsfname = strcat(filepathC,'_pV_ss.txt' );
wsfname = strcat(filepathC,'_pW_ss.txt' );

uxsfname = strcat(filepath, '_pUx_ss.txt');
uysfname = strcat(filepath, '_pUy_ss.txt');
uzsfname = strcat(filepath, '_pUz_ss.txt');

vxsfname = strcat(filepath, '_pVx_ss.txt');
vysfname = strcat(filepath, '_pVy_ss.txt');
vzsfname = strcat(filepath, '_pVz_ss.txt');

wxsfname = strcat(filepath, '_pWx_ss.txt');
wysfname = strcat(filepath, '_pWy_ss.txt');
wzsfname = strcat(filepath, '_pWz_ss.txt');



xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');

uspos = textread(usfname, '%f\t');
vspos = textread(vsfname, '%f\t');
wspos = textread(wsfname, '%f\t');

uxspos = textread( uxsfname,'%f\t');
uyspos = textread( uysfname,'%f\t');
uzspos = textread( uzsfname,'%f\t');

vxspos = textread( vxsfname,'%f\t');
vyspos = textread( vysfname,'%f\t');
vzspos = textread( vzsfname,'%f\t');

wxspos = textread( wxsfname,'%f\t');
wyspos = textread( wysfname,'%f\t');
wzspos = textread( wzsfname,'%f\t');

epos = floor(epos +0.0001);
s = floor( (max(size(xpos)))^(1/2) +0.1);


xVpos = reshape(xpos,s,s);
yVpos = reshape(ypos,s,s);
zVpos = reshape(zpos,s,s);

usVpos = reshape(uspos,s,s);  vsVpos = reshape(vspos,s,s);  wsVpos = reshape(wspos,s,s);

uxsVpos = reshape(uxspos,s,s);  vxsVpos = reshape(vxspos,s,s);  wxsVpos = reshape(wxspos,s,s);
uysVpos = reshape(uyspos,s,s);  vysVpos = reshape(vyspos,s,s);  wysVpos = reshape(wyspos,s,s);
uzsVpos = reshape(uzspos,s,s);  vzsVpos = reshape(vzspos,s,s);  wzsVpos = reshape(wzspos,s,s);



figure(7);
subplot(4,3,1),contour(yVpos,zVpos,usVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
title('u(2dir-LSIAC)'); grid off; axis off;
subplot(4,3,2),contour(yVpos,zVpos,vsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
title('v(2dir-LSIAC)'); grid off; axis off; 
subplot(4,3,3),contour(yVpos,zVpos,wsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
title('w(2dir-LSIAC)'); grid off; axis off; 

subplot(4,3,4),contour(yVpos,zVpos,uxsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
title('u_x(2dir-LSIAC)'); grid off; axis off;  
subplot(4,3,5),contour(yVpos,zVpos,vxsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
title('v_x(2dir-LSIAC)');  grid off; axis off;
subplot(4,3,6),contour(yVpos,zVpos,wxsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
title('w_x(2dir-LSIAC)');  grid off; axis off;

subplot(4,3,7),contour(yVpos,zVpos,uysVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
title('u_y(2dir-LSIAC)');  grid off; axis off;
subplot(4,3,8),contour(yVpos,zVpos,vysVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
title('v_y(2dir-LSIAC)');  grid off; axis off;
subplot(4,3,9),contour(yVpos,zVpos,wysVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
title('w_y(2dir-LSIAC)');  grid off; axis off;


subplot(4,3,10),contour(yVpos,zVpos,uzsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
title('u_z(2dir-LSIAC)');  grid off; axis off;
subplot(4,3,11),contour(yVpos,zVpos,vzsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
title('v_z(2dir-LSIAC)');  grid off; axis off;
subplot(4,3,12),contour(yVpos,zVpos,wzsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]);
title('w_z(2dir-LSIAC)');  grid off; axis off;
%print('images/Feb22/Countour_2dir_AllVar','-dpng', '-r500')


figure(8);
subplot(4,3,1),surf(yVpos,zVpos,usVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
title('u(2dir-LSIAC)');  grid off; axis off;
subplot(4,3,2),surf(yVpos,zVpos,vsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
title('v(2dir-LSIAC)');  grid off; axis off;
subplot(4,3,3),surf(yVpos,zVpos,wsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
title('w(2dir-LSIAC)');  grid off; axis off;

subplot(4,3,4),surf(yVpos,zVpos,uxsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
title('u_x(2dir-LSIAC)');  grid off; axis off;
subplot(4,3,5),surf(yVpos,zVpos,vxsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
title('v_x(2dir-LSIAC)');  grid off; axis off;
subplot(4,3,6),surf(yVpos,zVpos,wxsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
title('w_x(2dir-LSIAC)');  grid off; axis off;

subplot(4,3,7),surf(yVpos,zVpos,uysVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
title('u_y(2dir-LSIAC)');  grid off; axis off;
subplot(4,3,8),surf(yVpos,zVpos,vysVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
title('v_y(2dir-LSIAC)');  grid off; axis off;
subplot(4,3,9),surf(yVpos,zVpos,wysVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
title('w_y(2dir-LSIAC)');  grid off; axis off;

subplot(4,3,10),surf(yVpos,zVpos,uzsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
title('u_z(2dir-LSIAC)');  grid off; axis off;
subplot(4,3,11),surf(yVpos,zVpos,vzsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
title('v_z(2dir-LSIAC)');  grid off; axis off;
subplot(4,3,12),surf(yVpos,zVpos,wzsVpos),xlim([ 0.15 0.35 ]), ylim([ 0.73 0.93 ]),shading interp,view(0,90),colorbar();
title('w_z(2dir-LSIAC)');  grid off; axis off;
%print('images/Feb22/Surf_2dir_AllVar','-dpng', '-r500')

% calculate acceleration.
auspos = uxspos.*uspos + uyspos.*vspos + uzspos.*wspos;
avspos = vxspos.*uspos + vyspos.*vspos + vzspos.*wspos;
awspos = wxspos.*uspos + wyspos.*vspos + wzspos.*wspos;
 %
% veclocity cross acceleration. 
cuspos = vspos.*awspos - wspos.*avspos;
cvspos = wspos.*auspos - uspos.*awspos;
cwspos = uspos.*avspos - vspos.*auspos;

curvature_pos = (cuspos.^(2) + cvspos.^(2) + cwspos.^(2)).^(1/2);
curvature_Vpos = reshape(curvature_pos,s,s);

figure(10)
surf(yVpos,zVpos,curvature_Vpos);
shading interp;
%%
figure(11)
contour3(yVpos,zVpos,curvature_Vpos,20,'LineColor','k'); hold on;
surf(yVpos,zVpos,curvature_Vpos), shading interp;
grid off;
axis off;
%colorbar();
view(0,90);
print('images/Feb22/Curvature_2dirLSIAC_contour','-dpng', '-r500')
%view(100,-14);
%print('images/Feb22/Curvature_2dirLSIAC_contour_AngleView','-dpng', '-r500')


%%
%save('UVWDer_2dirLSIAC');
save('curvatueAtPlane','curvature_Vpos');