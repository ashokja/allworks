

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %
  %
  %
  %
  %
  %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  function y = ModifiedBasisPoly(index,maxdegree,x)

  if index == 0
    y = 0.5*(1-x);
  elseif index == maxdegree
    y = 0.5*(1+x);
  else
    y = 0.25*(1-x).*(1+x).*JacobiPoly(index-1,x,1,1);
  end
  
  