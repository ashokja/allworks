%testNaca_3D.cpp


clear;
close all;
va = -51.5;
vb = 18;


%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05' ;
%filename = strcat('testData3_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_All3DVar3' );
%filepath = strcat('data_3D/DSset120_30_30_p6_2/',filename);
filename = strcat('testData3_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_All3DVar5' );
filepath = strcat('data_3D/WORK3/',filename);
s1 = 60;    s2 =60; s3 = 240;


xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );

vxsfname = strcat(filepath,'_pVx_s.txt' );
wxsfname = strcat(filepath,'_pWx_s.txt' );
uysfname = strcat(filepath,'_pUy_s.txt' );
wysfname = strcat(filepath,'_pWy_s.txt' );
uzsfname = strcat(filepath,'_pUz_s.txt' );
vzsfname = strcat(filepath,'_pVz_s.txt' );

uyspos = textread(uysfname,'%f\t');
uzspos = textread(uzsfname,'%f\t');
vxspos = textread(vxsfname,'%f\t');
vzspos = textread(vzsfname,'%f\t');
wxspos = textread(wxsfname,'%f\t');
wyspos = textread(wysfname,'%f\t');

xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');


epos = floor(epos +0.0001);
%%

%s = floor( (max(size(xpos)))^(1/3) +0.1);

    xVpos = reshape(xpos,s1,s2,s3);
    xVpos = permute(xVpos,[2,3,1]);
     yVpos = reshape(ypos,s1,s2,s3);
     yVpos = permute(yVpos,[2,3,1]);
     zVpos = reshape(zpos,s1,s2,s3);
     zVpos = permute(zVpos,[2,3,1]);
%     
% uypVpos = reshape(uyppos,s,s,s);    uypVpos = permute(uypVpos,[2,3,1]);
% uzpVpos = reshape(uzppos,s,s,s);    uzpVpos = permute(uzpVpos,[2,3,1]);
% 
% vxpVpos = reshape(vxppos,s,s,s);    vxpVpos = permute(vxpVpos,[2,3,1]);
% vzpVpos = reshape(vzppos,s,s,s);    vzpVpos = permute(vzpVpos,[2,3,1]);
% 
% wxpVpos = reshape(wxppos,s,s,s);    wxpVpos = permute(wxpVpos,[2,3,1]);
% wypVpos = reshape(wyppos,s,s,s);    wypVpos = permute(wypVpos,[2,3,1]);
% 
% 
 uysVpos = reshape(uyspos,s1,s2,s3);    uysVpos = permute(uysVpos,[2,3,1]);
 uzsVpos = reshape(uzspos,s1,s2,s3);    uzsVpos = permute(uzsVpos,[2,3,1]);
% 
 vxsVpos = reshape(vxspos,s1,s2,s3);    vxsVpos = permute(vxsVpos,[2,3,1]);
 vzsVpos = reshape(vzspos,s1,s2,s3);    vzsVpos = permute(vzsVpos,[2,3,1]);
% 
 wxsVpos = reshape(wxspos,s1,s2,s3);    wxsVpos = permute(wxsVpos,[2,3,1]);
 wysVpos = reshape(wyspos,s1,s2,s3);    wysVpos = permute(wysVpos,[2,3,1]);

%[SX,SY,SZ] = meshgrid(linspace(2.15,5.95,40), linspace(0.05,0.45,10),linspace(0.65,0.95,10))

%%
figure (1),
hsurfaces = slice(xVpos,yVpos,zVpos,uysVpos,[2.2,2.25,2.3,2.35],[0.2,0.25,0.3,0.35],[0.75,0.8,0.85,0.9]);
set(hsurfaces,'FaceColor','flat','EdgeColor','none')
xlabel('x'); ylabel('y'); zlabel('z');
colormap jet; colorbar; hold off; 
title('FUNC(ACT'); hold on;
axis equal;


%Vorticity_p = ((uypVpos- vxpVpos).^(2) + (uzpVpos- wxpVpos).^(2) + (wypVpos- vzpVpos).^(2) ).^(1/2);

Vorticity_s = ((uysVpos- vxsVpos).^(2) + (uzsVpos- wxsVpos).^(2) + (wysVpos- vzsVpos).^(2) ).^(1/2);

% 
% 
% 
% figure (2),
% hsurfaces = slice(xVpos,yVpos,zVpos,Vorticity_p,[2.2,2.25,2.3,2.35],[0.2,0.25,0.3,0.35],[0.75,0.8,0.85,0.9]);
% set(hsurfaces,'FaceColor','flat','EdgeColor','none')
% xlabel('x'); ylabel('y'); zlabel('z');
% colormap jet; colorbar; hold off; 
% title('FUNC(ACT'); hold on;
% axis equal;
% 
% 
% figure (3),
% hsurfaces = slice(xVpos,yVpos,zVpos,Vorticity_s,[2.2,2.25,2.3,2.35],[0.2,0.25,0.3,0.35],[0.75,0.8,0.85,0.9]);
% set(hsurfaces,'FaceColor','flat','EdgeColor','none')
% xlabel('x'); ylabel('y'); zlabel('z');
% colormap jet; colorbar; hold off; 
% title('FUNC(ACT'); hold on; caxis([0 60]);
% axis equal;
% %%
% valueIso = 20;
% %%
%%
figure(4),hold off
fv = patch(isosurface(xVpos,yVpos,zVpos,Vorticity_s,20)); hold on;
isonormals(xVpos,yVpos,zVpos,Vorticity_s,fv)
fv.FaceColor = [1 0.2 0.2];
fv.EdgeColor = 'none';
daspect([1,1,1])
view([va,vb])
camlight 
lighting gouraud
axis equal;
hold off;
axis off;
axis([2.4,4.25,0.0,0.5,0.6,1.0]); view([-0.05,45])
%print('~/Desktop/teaser/teaser_part7','-dpng', '-r700');



[f,v] = isosurface(xVpos,yVpos,zVpos,Vorticity_s,20);
exportTriangulation2VTK('teaser_isoSurface_H1',v,f);


%%
% factor = 3;
% mehtod = 'spline';
% xVpos2 = interp3(xVpos,factor,mehtod);
% yVpos2 = interp3(yVpos,factor,mehtod);
% zVpos2 = interp3(zVpos,factor,mehtod);
% VS2 = interp3(Vorticity_s,factor,mehtod);
% 
% [f,v] = isosurface(xVpos2,yVpos2,zVpos2,VS2,20);
% exportTriangulation2VTK('teaser_isoSurface_H3',v,f);

% 
% figure(5),hold off
% fv = patch(isosurface(xVpos2,yVpos2,zVpos2,VS2,20)); hold on;
% isonormals(xVpos2,yVpos2,zVpos2,VS2,fv)
% fv.FaceColor = 'red';
% fv.EdgeColor = 'none';
% daspect([1,1,1])
% view([va,vb])
% camlight 
% lighting gouraud
% axis equal;
% hold off;
% axis off;
% axis([2.4,4.25,0.0,0.5,0.6,1.0]); view([+0.05,45])
% print('~/Desktop/teaser/teaser_part7_H3','-dpng', '-r1000');



















% %axis([2.2,2.35,0.2,0.35,0.75,0.9])
% line([2.25;2.25], [0.2;0.2], [0.75,0.9], 'color','k','LineWidth',1.4)
% line([2.25;2.25], [0.35;0.35], [0.75,0.9], 'color','k','LineWidth',1.4)
% line([2.25;2.25], [0.2;0.35], [0.75,0.75], 'color','k','LineWidth',1.4)
% line([2.25;2.25], [0.2;0.35], [0.9,0.9], 'color','k','LineWidth',1.4)
% hcs = contourslice(xVpos, yVpos, zVpos, Vorticity_s, 2.25,[], [], [20 20]) ;
% set(hcs,'EdgeColor',[1 1 0]);
% set(hcs,'linewidth',2);
% set(hcs,'linestyle','--');
% %print('images/Vorticity_IsoSur_DG_ISo_20_P_2_outline_F','-dpng', '-r500');
% %%
% %[f,v] = isosurface(xVpos,yVpos,zVpos,Vorticity_S,valueIso);
% %exportTriangulation2VTK('test_isoSurface',v,f);

% 
% %%
% figure(5),hold off
% fv = patch(isosurface(xVpos,yVpos,zVpos,Vorticity_s,valueIso)); hold on;
% isonormals(xVpos,yVpos,zVpos,Vorticity_s,fv)
% fv.FaceColor = 'red';
% fv.EdgeColor = 'none';
% daspect([1,1,1])
% view([va,vb])
% camlight 
% lighting gouraud
% axis equal;
% hold off;
% axis off;
% axis([2.2,2.35,0.2,0.35,0.75,0.9])
% %print('images/Vorticity_IsoSur_SIAC_ISo_20_P_2','-dpng', '-r500');
% line([2.25;2.25], [0.2;0.2], [0.75,0.9], 'color','k','LineWidth',2)
% line([2.25;2.25], [0.35;0.35], [0.75,0.9], 'color','k','LineWidth',2)
% line([2.25;2.25], [0.2;0.35], [0.75,0.75], 'color','k','LineWidth',2)
% line([2.25;2.25], [0.2;0.35], [0.9,0.9], 'color','k','LineWidth',2)
% %print('images/Vorticity_IsoSur_SIAC_ISo_20_P_2_outline','-dpng', '-r500');
% hold on;
% hcs = contourslice(xVpos, yVpos, zVpos, Vorticity_s, 2.25,[], [], [valueIso valueIso]) ;
% set(hcs,'EdgeColor',[1 1 0]);
% set(hcs,'linewidth',2);
% set(hcs,'linestyle','--');
% %print('images/Vorticity_IsoSur_SIAC_ISo_20_P_2_outline_F','-dpng', '-r500');
% %view(90,0);
% %axis off;
% %axis([2.2,2.35,0.2,0.35,0.75,0.9])
% %%
% c_temp = 0
% figure(6), hold off
% contourslice(xVpos, yVpos, zVpos, Vorticity_s, 2.35,[], [], 90);
% %contourslice(xVpos, yVpos, zVpos, Vorticity_s, 2.25,[], [], linspace(0,100,40)) ;
% view(90,0);
% axis off;
% %axis([2.2,2.35,0.2,0.35,0.75,0.9])
% hcs = contourslice(xVpos, yVpos, zVpos, Vorticity_s, 2.35,[], [], [valueIso+c_temp valueIso+c_temp]) ;
% set(hcs,'EdgeColor',[1 0 0]);
% set(hcs,'linewidth',2);
% set(hcs,'linestyle','--');
% %print('images/Vorticity_Contours_SIAC_P_2_F','-dpng', '-r500');
% %%
% figure(7), hold off
% contourslice(xVpos, yVpos, zVpos, Vorticity_p, 2.25,[], [], linspace(0,100,40));
% view(90,0);
% axis off;
% axis([2.2,2.35,0.2,0.35,0.75,0.9])
% hcs = contourslice(xVpos, yVpos, zVpos, Vorticity_p, 2.25,[], [], [valueIso valueIso]) ;
% set(hcs,'EdgeColor',[1 0 0]);
% set(hcs,'linewidth',2);
% set(hcs,'linestyle','--');
% %print('images/Vorticity_Contours_DG_P_2_F','-dpng', '-r500');
% 
% %%
% 
% figure (8), hold on;
% eVpos = reshape(epos,s,s,s);
% eVpos = permute(eVpos,[2,3,1]);
% dely = squeeze(yVpos(:,31,:));
% delz = squeeze(zVpos(:,31,:));
% delvp = squeeze(Vorticity_p(:,31,:));
% dele = squeeze(eVpos(:,31,:));
% surf(dely,delz,dele);
% 
% delyl = dely(:);
% delzl = delz(:);
% delvpl = delvp(:);
% delel = dele(:);
% 
% 
% [evals, elids,relids] = unique(delel(:));
% addpath('..');
% num_unique_ids = (max(size(evals)));
% for etag = evals'
%     disp(etag);
%     %disp('next');
%     el_indices = find(dele(:)' ==etag);
%     if (max(size(el_indices)) < 7)
%         continue;
%     end
%     disp(delel(el_indices));
%     
%     ytripos = delyl(el_indices);
%     ztripos = delzl(el_indices);
%     vtripos = delvpl(el_indices);
%     tri = delaunay(ytripos,ztripos);
%     
%     
%     figure(9), hold on;    
%     trisurf(tri,ytripos,ztripos,vtripos); hold on;
%     
%     figure(10), hold on;
%     tricontour( [ytripos,ztripos], tri, vtripos,linspace(0,100,40)); hold on;
%     
%     tricontour([ytripos,ztripos], tri, vtripos,[valueIso valueIso]); hold on;
%     %hcs = contourslice(xVpos, yVpos, zVpos, Vorticity_p, 2.25,[], [], ) ;
%     set(hcs,'EdgeColor',[1 0 0]);
%     set(hcs,'linewidth',2);
%     set(hcs,'linestyle','--');
%     
% %    tricontour(tri,ytripos,ztripos,vtripos,linspace(0,100,40)); hold on;
%     
% end
% 
% % 
% % figure(3)
% % fv = isosurface(xVpos,yVpos,zVpos,Vorticity_s,30.0); hold on;
% % isonormals(xVpos,yVpos,zVpos,Vorticity_s,fv)
% % fv.FaceColor = 'red';
% % fv.EdgeColor = 'none';
% % daspect([1,1,1])
% % view(3); 
% % camlight 
% % lighting gouraud
% 
% 
% % % %%
% % % figure (2),
% % % hsurfaces = slice(xVpos,yVpos,zVpos,psVpos,[2.5,2.8333,3.0],0.25,0.6);
% % % set(hsurfaces,'FaceColor','flat','EdgeColor','none')
% % % xlabel('x'); ylabel('y'); zlabel('z');
% % % colormap jet; colorbar; hold off; caxis([-1,1]);
% % % title('FUNC(ACT'); hold on;
% 
% 
% %x = linspace(2.5,3,10);
% %[X,Y,Z] = meshgrid(x);
% % 
% % sx = linspace(2.5, 2.7,3);
% % sy = linspace(min(yVpos(:)), max(yVpos(:)),10);
% % sz = linspace(min(zVpos(:)), max(zVpos(:)),10);
% % [startx,starty,startz] = meshgrid(sx,sy,sz);
% % xlabel('x');ylabel('y'); zlabel('z');
% % 
% % streamline(xVpos,yVpos,zVpos,upVpos,vpVpos,wpVpos,startx,starty,startz); 
% 
% % %hold on;
% %  figure(3)
% % % fv = isosurface(xVpos,yVpos,zVpos,ppVpos,0.0);
% % % isonormals(xVpos,yVpos,zVpos,ppVpos,fv)
% % % fv.FaceColor = 'red';
% % % fv.EdgeColor = 'none';
% % % daspect([1,1,1])
% % % view(3); axis tight
% % % camlight 
% % % lighting gouraud
% % 
% % 
% % %[x,y,z,v] = flow;
% % p = patch(isosurface(xVpos,yVpos,zVpos,ppVpos,-0.6));
% % isonormals(xVpos,yVpos,zVpos,ppVpos,p)
% % p.FaceColor = 'red';
% % p.EdgeColor = 'none';
% % daspect([1,1,1])
% % view(3); axis tight
% % camlight 
% % lighting gouraud
