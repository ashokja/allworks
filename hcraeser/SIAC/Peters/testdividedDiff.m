function test
    degree = 3;
    h = @monomial;
    h(2,0,0)
    h(2,0,1)
    %tknots=(1:6);
    tknots=ones(1,6)*2;
    dividedDiff(1,3,tknots,0,h)

    function result = monomial(t,x,der)
        result = prod(degree-der+1:degree)*(t-x)^(degree-der);
    end


end