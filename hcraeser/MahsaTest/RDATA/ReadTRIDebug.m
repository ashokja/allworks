% read file name.
clear;
close all;
T = textread('triPts.txt','%f');
Tv = textread('tridata.txt','%f');
Tc = textread('tridataC.txt','%f');

Txcount =1;
Tycount =1;
for c =1:max(size(T))
    if 1 == mod(c,2)
        Tx(Txcount) = T(c);
        Txcount = Txcount+1;
    else
        Ty(Tycount) = T(c);
        Tycount = Tycount+1;
    end
end
%%
figure(1)
plot3(Tx,Ty,ones(size(Tx)),'*');

figure(2)
plot3(Tx,Ty,Tv);

figure(3)
plot3(Tx,Ty,Tc);

advx = 0.989743;
advy = 1.0;
FinTime = 1.0;
x = Tx;
y=Ty;
Tc_f = sin(2*pi/0.989743*(x-advx*FinTime)+2*pi*(y-advy*FinTime));

% figure(4)
% plot3(Tx,Ty,Tc_f); hold off;
% 
% figure(6)
% plot3(Tx,Ty,abs(Tc_f'-Tc)); hold off

[T1 T2 T3 T4 T5 T6] = textread('tri.txt',' %f %f %f %f %f %f');
TX = [T1, T3, T5];
TY = [T2, T4, T6];
patch(TX,TY,'red');
lp = max(size(T1));
figure(7)
for i = 1:lp
    patch([T1(i), T3(i), T5(i)],[T2(i), T4(i), T6(i)],'red');
end
