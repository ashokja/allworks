#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"

#include <iomanip>


using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void calCurvature(int nq, 
	Array<OneD,NekDouble> &u_phys, Array<OneD,NekDouble> &v_phys,Array<OneD,NekDouble> &w_phys,
    Array<OneD,NekDouble> &udx_phys, Array<OneD,NekDouble> &udy_phys, Array<OneD,NekDouble> &udz_phys,
    Array<OneD,NekDouble> &vdx_phys, Array<OneD,NekDouble> &vdy_phys, Array<OneD,NekDouble> &vdz_phys,
    Array<OneD,NekDouble> &wdx_phys, Array<OneD,NekDouble> &wdy_phys, Array<OneD,NekDouble> &wdz_phys,
    Array<OneD,NekDouble> &au_phys, Array<OneD,NekDouble> &av_phys,  Array<OneD,NekDouble> &aw_phys,
    Array<OneD,NekDouble> &vCa_x, Array<OneD,NekDouble> &vCa_y, Array<OneD,NekDouble> &vCa_z,
	Array<OneD,NekDouble> &curNorm2 )
{
    Array<OneD,NekDouble> temp1(nq), temp2(nq), temp3(nq);
    // a acceleration
    Vmath::Vmul(nq, udx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, udy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, udz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, au_phys,1);
    Vmath::Vadd(nq, temp3, 1,au_phys,1, au_phys,1);
   
    Vmath::Vmul(nq, vdx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, vdy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, vdz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, av_phys,1);
    Vmath::Vadd(nq, temp3, 1,av_phys,1, av_phys,1);

    Vmath::Vmul(nq, wdx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, wdy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, wdz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, aw_phys,1);
    Vmath::Vadd(nq, temp3, 1,aw_phys,1, aw_phys,1);


    // cross v a  = curvature.
    Vmath::Vmul(nq, v_phys,1,aw_phys,1, temp1,1);
    Vmath::Vmul(nq, w_phys,1,av_phys,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_x,1);

    Vmath::Vmul(nq, w_phys,1,au_phys,1, temp1,1);
    Vmath::Vmul(nq, u_phys,1,aw_phys,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_y,1);

    Vmath::Vmul(nq, u_phys,1,av_phys,1, temp1,1);
    Vmath::Vmul(nq, v_phys,1,au_phys,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_z,1);

	//Curvature norm
	Vmath::Vmul(nq, vCa_x,1,vCa_x,1, temp1, 1);
	Vmath::Vmul(nq, vCa_y,1,vCa_y,1, temp2, 1);
	Vmath::Vmul(nq, vCa_z,1,vCa_z,1, temp3, 1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, curNorm2,1);
    Vmath::Vadd(nq, temp3, 1,curNorm2,1, curNorm2,1);

    return;
}





void calVorticity(int nq,
    Array<OneD,NekDouble> &udx_phys, Array<OneD,NekDouble> &udy_phys, Array<OneD,NekDouble> &udz_phys,
    Array<OneD,NekDouble> &vdx_phys, Array<OneD,NekDouble> &vdy_phys, Array<OneD,NekDouble> &vdz_phys,
    Array<OneD,NekDouble> &wdx_phys, Array<OneD,NekDouble> &wdy_phys, Array<OneD,NekDouble> &wdz_phys,
    Array<OneD,NekDouble> &Vx,Array<OneD,NekDouble> &Vy,Array<OneD,NekDouble> &Vz )
{
    Vmath::Vsub(nq, wdy_phys,1, vdz_phys,1,Vx ,1);
    Vmath::Vsub(nq, udz_phys,1, wdx_phys,1,Vy ,1);
    Vmath::Vsub(nq, vdx_phys,1, udy_phys,1,Vz ,1);
    return;
}

int main(int argc, char* argv[])
{
	if (argc != 7)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg 0/1"<<endl<<"\t 0 for writing data"<< endl<<"\t 1 for reading data." << endl;
		cout << "3nd arg polynomial degree filter you want to apply" << endl;
		cout << "4rd arg meshscaling you want to use." << endl;
		cout << "5th arg output resolution." << endl;
		cout << "6th arg value w to be attached to filename." << endl;
	//	cout << "4th Angle of SIAC derivative in deg -90 to 90 allowed" << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();

//	HNM3D->LoadData( fname+ ".chk",var);

	HNM3D->LoadMesh(var[0]);
	HNM3D->LoadMesh(var[1]);
	HNM3D->LoadMesh(var[2]);

	HNM3D->LoadExpListIntoRTree();
	
	int tNquadPts = HNM3D->m_expansions[0]->GetTotPoints();
	int tNCoeffs= HNM3D->m_expansions[0]->GetNcoeffs();
	cout << "fPhys:" << tNquadPts<< endl;
	cout << "fCoeffs:" << HNM3D->m_expansions[0]->GetNcoeffs()<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);
	Array<OneD,NekDouble> u(tNquadPts);
	Array<OneD,NekDouble> v(tNquadPts);
	Array<OneD,NekDouble> w(tNquadPts);

	
	switch( HNM3D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
	NektarBaseClass NBC;	

	if( 0 == atoi(argv[2]) )
	{
		NBC.writeNekArray(xc0,fname+"_xc0."+"txt");
		NBC.writeNekArray(xc1,fname+"_xc1."+"txt");
		NBC.writeNekArray(xc2,fname+"_xc2."+"txt");
		return 0;
	}
	else
	{
		NBC.readNekArray(u, fname+"_u.txt");
		NBC.readNekArray(v, fname+"_v.txt");
		NBC.readNekArray(w, fname+"_w.txt");
		//k.printNekArray(v,0);
	}

	// Now use Newton-Rapson to find the zero u and zero v.
	
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( u,HNM3D->m_expansions[0]->UpdateCoeffs());
	HNM3D->m_expansions[0]->BwdTrans_IterPerExp( HNM3D->m_expansions[0]->GetCoeffs(),
			HNM3D->m_expansions[0]->UpdatePhys());
	
	HNM3D->m_expansions[1]->FwdTrans_IterPerExp( v,HNM3D->m_expansions[1]->UpdateCoeffs());
	HNM3D->m_expansions[1]->BwdTrans_IterPerExp( HNM3D->m_expansions[1]->GetCoeffs(),
			HNM3D->m_expansions[1]->UpdatePhys());
	
	HNM3D->m_expansions[2]->FwdTrans_IterPerExp( w,HNM3D->m_expansions[2]->UpdateCoeffs());
	HNM3D->m_expansions[2]->BwdTrans_IterPerExp( HNM3D->m_expansions[2]->GetCoeffs(),
			HNM3D->m_expansions[2]->UpdatePhys());


	Array<OneD,NekDouble> u_DG = HNM3D->m_expansions[0]->GetPhys();	
	Array<OneD,NekDouble> v_DG = HNM3D->m_expansions[1]->GetPhys();
	Array<OneD,NekDouble> w_DG = HNM3D->m_expansions[2]->GetPhys();

	Array<OneD,NekDouble> ux_DG(tNquadPts), uy_DG(tNquadPts), uz_DG(tNquadPts);
	Array<OneD,NekDouble> vx_DG(tNquadPts), vy_DG(tNquadPts), vz_DG(tNquadPts);
	Array<OneD,NekDouble> wx_DG(tNquadPts), wy_DG(tNquadPts), wz_DG(tNquadPts);

	Array<OneD,NekDouble> ax(tNquadPts), ay(tNquadPts), az(tNquadPts);
	Array<OneD,NekDouble> curX(tNquadPts), curY(tNquadPts), curZ(tNquadPts);
	Array<OneD,NekDouble> curNorm2(tNquadPts) ;

	HNM3D->m_expansions[0]->PhysDeriv( u_DG, ux_DG, uy_DG, uz_DG);
	HNM3D->m_expansions[1]->PhysDeriv( v_DG, vx_DG, vy_DG, vz_DG);
	HNM3D->m_expansions[2]->PhysDeriv( w_DG, wx_DG, wy_DG, wz_DG);

	Array<OneD,NekDouble> Vor_x(tNquadPts), Vor_y(tNquadPts), Vor_z(tNquadPts);
	Array<OneD,NekDouble> Vor_xC(tNCoeffs), Vor_yC(tNCoeffs), Vor_zC(tNCoeffs);
	
	calVorticity(tNquadPts , ux_DG, uy_DG, uz_DG, vx_DG, vy_DG, vz_DG, wx_DG,wy_DG,wz_DG, Vor_x,Vor_y,Vor_z);


	HNM3D->m_expansions[0]->FwdTrans( Vor_x, Vor_xC);
	HNM3D->m_expansions[1]->FwdTrans( Vor_y, Vor_yC);
	HNM3D->m_expansions[2]->FwdTrans( Vor_z, Vor_zC);



//  EL_meshGrid.

	int gPtsX = atoi(argv[5]);
	int gPtsY = atoi(argv[5]);
	int gPtsZ = atoi(argv[5]);
	int Nx=gPtsX, Ny=gPtsY, Nz=gPtsZ;
	int totNumExp = HNM3D->m_expansions[0]->GetExpSize();
	int totPts = Nx*Ny*Nz*totNumExp;

	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0), sz = 1.0/(Nz-1.0);
	NekDouble StartX = 0.0, StartY =0.0, StartZ=0.0;
	NekDouble EndX = 1.0, EndY =1.0, EndZ=1.0;

	Array<OneD,NekDouble> coord(3,0.0),locCoord(3), locCollapsed(3);
	Array<OneD,NekDouble> pX(totPts), pY(totPts),pZ(totPts),pE(totPts);
    Array<OneD,NekDouble> pU(totPts), pV(totPts), pW(totPts);
    Array<OneD,NekDouble> pUx(totPts), pVx(totPts), pWx(totPts);
    Array<OneD,NekDouble> pUy(totPts), pVy(totPts), pWy(totPts);
    Array<OneD,NekDouble> pUz(totPts), pVz(totPts), pWz(totPts);
    Array<OneD,NekDouble> pU_p(totPts), pV_p(totPts), pW_p(totPts);
    Array<OneD,NekDouble> pVor_x(totPts), pVor_y(totPts), pVor_z(totPts);
    Array<OneD,NekDouble> pVor_xp(totPts), pVor_yp(totPts), pVor_zp(totPts);

//	stuff needed for SIAC FILTER	
	SmoothieSIAC3D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[3]), atof(argv[4]), 1);	
	SmoothieSIAC3D sm(SIACUtilities::eSYM_2kp1_1SIDED_4kp1, HNM3D, atoi(argv[3]), atof(argv[4]));
	NekDouble scaling = atof(argv[4]), valY,valZ;
	Array<OneD,NekDouble> dirX(3,0.0), dirY(3,0.0), dirZ(3,0.0);
	dirX[0] = 1.0; dirY[1] = 1.0; dirZ[2] = 1.0;

	// Total 9 planes.
		// Plane Z = 0.0;
		// Plane Z = 0.5;
		// Plane Z = 1.0;
// loop through all the elements.
	int index = 0;
	for( int elId=0; elId< totNumExp; elId++)
	{
		cout << "elID: " << elId<<endl;
		int physOffset = HNM3D->m_expansions[0]->GetPhys_Offset(elId);
		
		const Array<OneD,NekDouble> el_u_phys = u_DG.CreateWithOffset(u_DG,physOffset);
	    const Array<OneD,NekDouble> el_v_phys = v_DG.CreateWithOffset(v_DG,physOffset);
    	const Array<OneD,NekDouble> el_w_phys = w_DG.CreateWithOffset(w_DG,physOffset);
	
    	const Array<OneD,NekDouble> el_Vor_x_phys = pVor_x.CreateWithOffset(Vor_x,physOffset);
    	const Array<OneD,NekDouble> el_Vor_y_phys = pVor_y.CreateWithOffset(Vor_y,physOffset);
    	const Array<OneD,NekDouble> el_Vor_z_phys = pVor_z.CreateWithOffset(Vor_z,physOffset);
		LocalRegions::ExpansionSharedPtr lexp = HNM3D->m_expansions[0]->GetExp(elId);

		for ( int k = 0 ; k<Nz;k++)
		{
			//Zmesh = k*0.5;
			for ( int i =0; i < Nx; i++)
			{
				for (int j=0; j< Ny; j++)
				{
					locCoord[0] = i*sx*2.0-1.0;
					locCoord[1] = j*sx*2.0-1.0;
					locCoord[2] = k*sx*2.0-1.0;
					if( Nektar::LibUtilities::eHexahedron == lexp->GetGeom()->GetShapeType())
					{
						locCollapsed[0] = locCoord[0];
						locCollapsed[1] = locCoord[1];
						locCollapsed[2] = locCoord[2];
					}else
					{
						assert( false && "only tetraDegron here.");
					}
					lexp->GetCoord(locCollapsed,coord);
					pE[index] = elId;
					pX[index] = coord[0];
					pY[index] = coord[1];
					pZ[index] = coord[2];
					/*
					// SIAC stuff;
					sm.EvaluateAt(pX[index],pY[index],pZ[index],pU[index],valY,valZ,dirX,scaling,0);
					sm.EvaluateAt(pX[index],pY[index],pZ[index],pV[index],valY,valZ,dirY,scaling,1);
					sm.EvaluateAt(pX[index],pY[index],pZ[index],pW[index],valY,valZ,dirZ,scaling,2);

					smD.EvaluateAt(pX[index],pY[index],pZ[index],pUx[index],valY,valZ,dirX,scaling,0);
					smD.EvaluateAt(pX[index],pY[index],pZ[index],pVx[index],valY,valZ,dirX,scaling,1);
					smD.EvaluateAt(pX[index],pY[index],pZ[index],pWx[index],valY,valZ,dirX,scaling,2);
				
					smD.EvaluateAt(pX[index],pY[index],pZ[index],pUy[index],valY,valZ,dirY,scaling,0);
					smD.EvaluateAt(pX[index],pY[index],pZ[index],pVy[index],valY,valZ,dirY,scaling,1);
					smD.EvaluateAt(pX[index],pY[index],pZ[index],pWy[index],valY,valZ,dirY,scaling,2);
				
					smD.EvaluateAt(pX[index],pY[index],pZ[index],pUz[index],valY,valZ,dirZ,scaling,0);
					smD.EvaluateAt(pX[index],pY[index],pZ[index],pVz[index],valY,valZ,dirZ,scaling,1);
					smD.EvaluateAt(pX[index],pY[index],pZ[index],pWz[index],valY,valZ,dirZ,scaling,2);
					*/
					pU_p[index] = lexp->PhysEvaluate(coord,el_u_phys);
        	    	pV_p[index] = lexp->PhysEvaluate(coord,el_v_phys);
            		pW_p[index] = lexp->PhysEvaluate(coord,el_w_phys);
				
					pVor_xp[index] = lexp->PhysEvaluate(coord,el_Vor_x_phys);
        	    	pVor_yp[index] = lexp->PhysEvaluate(coord,el_Vor_y_phys);
            		pVor_zp[index] = lexp->PhysEvaluate(coord,el_Vor_z_phys);

					index++;				
				}
			}
		}
	}

		
		calVorticity(totPts, pUx, pUy, pUz, pVx, pVy, pVz, pWx, pWy, pWz, pVor_x, pVor_y, pVor_z);
 
		NBC.writeNekArray(pE,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_e.txt");
		NBC.writeNekArray(pX,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_x.txt");
		NBC.writeNekArray(pY,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_y.txt");
		NBC.writeNekArray(pZ,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_z.txt");
	
		NBC.writeNekArray(pU_p,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_up.txt");
		NBC.writeNekArray(pV_p,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_vp.txt");
		NBC.writeNekArray(pW_p,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_wp.txt");
		
		NBC.writeNekArray(pU,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_u.txt");
		NBC.writeNekArray(pV,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_v.txt");
		NBC.writeNekArray(pW,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_w.txt");
	
		NBC.writeNekArray(pVor_xp,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_Vor_xp.txt");
		NBC.writeNekArray(pVor_yp,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_Vor_yp.txt");
		NBC.writeNekArray(pVor_zp,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_Vor_zp.txt");
		
		NBC.writeNekArray(pVor_x,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_Vor_x.txt");
		NBC.writeNekArray(pVor_y,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_Vor_y.txt");
		NBC.writeNekArray(pVor_z,fname+"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6]+"_Vor_z.txt");


	//NektarBaseClass k;
	


/*
	SmoothieSIAC2D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[3]), atof(argv[4]), 1);	
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_4kp1, HNM3D, atoi(argv[3]), atof(argv[4]));
	NekDouble scaling = atof(argv[4]);


	NekDouble sx,sy;
	cout << "reached till here" << endl;
	//input seed 
	vector<NekDouble> inPutSx, outPutSx;
	vector<NekDouble> inPutSy, outPutSy;
	
	}
//	SmoothieSIAC2D sm_sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]), atof(argv[3])); 

	k.writeNekArray(seedPtsX,fname+"_seedX_DG.txt");
	k.writeNekArray(seedPtsY,fname+"_seedY_DG.txt");
	k.writeNekArray(seedPtsU,fname+"_seedU_DG.txt");
	k.writeNekArray(seedPtsV,fname+"_seedV_DG.txt");


//	k.writeNekArray(u_DG, fname+"_u_DG.txt");
//	k.writeNekArray(v_DG, fname+"_v_DG.txt");
*/

    string out = vSession->GetSessionName() +"_R_"+argv[5]+"_EL_O_"+argv[3]+"_w_"+argv[6] +".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (HNM3D->m_expansions[0]->GetFieldDefinitions()).at(0);
    FieldDef[0]->m_fields.push_back("u");
    FieldDef[0]->m_fields.push_back("v");
    FieldDef[0]->m_fields.push_back("w");
	FieldDef[0]->m_fields.push_back("VorX");
	FieldDef[0]->m_fields.push_back("VorY");
	FieldDef[0]->m_fields.push_back("VorZ");
	HNM3D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0]);
	HNM3D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0]);
	HNM3D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM3D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0], Vor_xC);
    HNM3D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0], Vor_yC);
    HNM3D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0], Vor_zC);
    HNM3D->m_fld->Write(out, FieldDef, FieldData);


	return 0;
}

