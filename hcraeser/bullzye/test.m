% test.m to create bullzye plane viz through the curve.

% [x,y,z] = meshgrid(-2:.2:2,-2:.25:2,-2:.16:2);
% v = x.*exp(-x.^2-y.^2-z.^2);
% figure
% colormap hsv
% for k = -2:.05:2
%    hsp = surf(linspace(-1,1,20),linspace(-1,1,20),...
%       zeros(20) + k);
%    rotate(hsp,[1,-1,1],20)
%    xd = hsp.XData;
%    yd = hsp.YData;
%    zd = hsp.ZData;
%    delete(hsp)
%    
%    hsurfaces1 = slice(x,y,z,v,[-2,0,2],[0,2],-2); % Draw some volume boundaries
%    hold on
%    hsurfaces2 = slice(x,y,z,v,xd,yd,zd);
%    %set(hsurfaces1,'FaceColor','interp','EdgeColor','none');
%       %set(hsurfaces2,'FaceColor','interp','EdgeColor','none');
%    hold off
%    view(-5,50)
%    axis([-2.5 2.5 -2 2 -2 4])
%    drawnow
% end

addpath ../createTestData ;
plotBoundingBox();
xdata = [0.25,0.5,0];
xderdata = [1,1,-0.5];
xls = xdata(1); yls = xdata(2); zls = xdata(3);
        xdls = xderdata(1); ydls = xderdata(2); zdls = xderdata(3);
        quiver3(xdata(1),xdata(2),xdata(3),xdls,ydls,zdls);
        pNormal = [0,0,1];
        nNormal = [xdls,ydls,zdls];
        nNormal = nNormal/norm(nNormal);
        theta = acos(dot(nNormal,pNormal)/(norm(pNormal)*norm(nNormal)) );
        axis = cross(nNormal,pNormal);
        axis = axis/norm(axis);
        %[THETA,PHI] = cart2sph(xdls,ydls,zdls);
        hsp = surf(xls+linspace(-0.25,0.25,20),yls+linspace(-0.25,0.25,20),...
            zls+zeros(20) );
        
        rotate(hsp,axis,-theta/pi*180,[xls,yls,zls]);
        %rv = cross(v,[v(1),v(2),0])
        %rotate(hsp,rv,90-PHI*180/pi,[xls,yls,zls]);
        set(hsp,'FaceColor','interp','EdgeColor','none');
        hold off;