%ReadDynScaling for Line y =0;
clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20];

%abc = 'data/triangleMesh_O1_I2_2_S_0.1_R_100_pP_2D.txt ';

R = 100;
poly =4;
order = poly;
count =1;
scaling = '0.2';
datafile = 'dataCos2';

for poly = [2,3,4]
 %   for scaling = [{'0.1'},{'0.2'}]
    disp(poly);
    disp(scaling);
    prefixFilename = strcat('data/',datafile,'/P',int2str(poly),'_triSplit6_L2_R1_');
    %int2str(int16(str2double(scaling)*10))
    prefixImg = strcat('Images/',datafile,'_P',int2str(poly),'_triSplit6_L2_R1_','_S_',int2str(int16(str2double(scaling)*10)),'_R_',int2str(R));

    %prefixFilename = strcat('data/',datafile,'/P',int2str(poly),'_triSplit6_L4_R1_');
    %prefixImg = strcat('Images/',datafile,'_P',int2str(poly),'_triSplit6_L4_R1_','_S_',int2str(int16(str2double(scaling)*10)),'_R_',int2str(R));

    xfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pXLy_2DDyn.txt' );
    yfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pYLy_2DDyn.txt' );
    efname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pELy_2DDyn.txt' );
    ffname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pVLy_2DDyn.txt' );
    pfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pPLy_2DDyn.txt' );
    sfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pSLy_2DDyn.txt' );
    sDynfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pSDynLy_2DDyn.txt' );
    dynsfname = strcat(prefixFilename,int2str(poly),'_S_',scaling,'_R_',int2str(R),'_pDynLy_2DDyn.txt' );

    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');
    epos = textread(efname, '%f\t');
    fpos = textread(ffname, '%f\t');
    ppos = textread(pfname, '%f\t');
    spos = textread(sfname, '%f\t');
    sDynpos = textread(sDynfname, '%f\t');
    dynspos = textread(dynsfname, '%f\t');

    xVpos = xpos;%reshape(xpos,[R]);
    yVpos = ypos;%reshape(ypos,[R]);
    eVpos = epos;
    fVpos = fpos;%reshape(fpos,[R]);
    pVpos = ppos;%reshape(ppos,[R]);
    sVpos = spos;%reshape(spos,[R]);
    sDynVpos = sDynpos;%reshape(sDynpos,[R]);
    dynsVpos = dynspos;%reshape(dynspos,[R]);

    figure(1),
    plot(xVpos,fVpos);

    figure(2),
    plot(xVpos,pVpos);

    figure(3),
    plot(xVpos,sVpos);

    %%
    figure(4),
    plot(xVpos,abs(fVpos-pVpos));
    title('projection errors');
    figure(5),
    plot(xVpos,abs(fVpos-sVpos));
    title('SIAC errors'); hold off;
    figure(51),
    plot(xVpos,abs(fVpos-sDynVpos));
    title('SIAC Dyn errors');

    figure(6)
    plot(xVpos,log10(abs(fVpos-pVpos))); hold on;
    plot(xVpos,log10(abs(fVpos-sVpos))); hold on;
    plot(xVpos,log10(abs(fVpos-sDynVpos)),'--k'); hold off;
    legend('proj',strcat('LSIAC (',scaling,')'),'Dyn LSIAC')
    xlabel(' -1<x<1; y=0');
    ylabel('log_{10}(error)')
    xlim([-1, 1])
    xt = getticks(eVpos,xVpos);
    set(gca,'xtick',xt);
    set(gca,'XTickLabel',sprintf('%.1f\n',xt))
    %set(gca,'XTickAngle',45)
    axH=gca;
    axH.XTickLabelRotation = 60;
    %set(gca,'XTickformat','%.2f');
    % figure(7)
    % plot(xVpos,dynsVpos); hold on;
    %print(strcat(prefixImg,'LineGraph'), '-dpng', '-r100');
    
    figure(51),
    plot(xVpos,dynsVpos);
    title('Dyn Scaling');
    xlim([-1,1]);
    xt = getticks(eVpos,xVpos);
    set(gca,'xtick',xt);
    set(gca,'XTickLabel',sprintf('%.1f\n',xt))
    %set(gca,'XTickAngle',45)
    axH=gca;
    axH.XTickLabelRotation = 60;

    print('Images/triSplit6_L2_R1_LineScaling', '-dpng', '-r100');
    
    xlim([-1,1]);
%    end
end
%%

% figure(6),
% contourf(xVpos,yVpos,log10(abs(fVpos-pVpos)));
% title('projection errors'); colorbar(); caxis([-10,0]);
% view(0,90);
% figure(7),
% contourf(xVpos,yVpos,log10(abs(fVpos-sVpos)));
% title('SIAC errors'); colorbar(); caxis([-10,0]);
% view(0,90); hold on;
% figure(7),
% contourf(xVpos,yVpos,log10(abs(fVpos-sDynVpos)));
% title('SIAC Dyn errors'); colorbar(); caxis([-10,0]);
% view(0,90);
% 
% 
% disp('Proj');
% disp(sum(sum((abs(fVpos-pVpos)))));
% disp(max(max((abs(fVpos-pVpos)))));
% disp('SIAC');
% disp(sum(sum((abs(fVpos-sVpos)))));
% disp(max(max((abs(fVpos-sVpos)))));
% 
% disp('SIAC Dyn');
% disp(sum(sum((abs(fVpos-sDynVpos)))));
% disp(max(max((abs(fVpos-sDynVpos)))));
% 
% 
% figure(8),
% plot(xVpos,yVpos,dynsVpos);
% title('dynscaling'); shading Interp;
% 
% figure(9),
% contourf(xVpos,yVpos,dynsVpos);
% title('dynscaling'); 


