#pragma once

namespace SIACUtilities
{
        //! This enum specifies the kind of filter to be used by the object.
        /*!  The user can specify any type of smoothing or derivative filter.
         *  All the properties of memeber depends on this enum. This enum cannot be changed after the object has been initialized.
         *  A new enum needs to be added to the list to create any new type of filter.
         */
        enum FilterType
        {   
            eNONE,                   /**< enum value 1 */
            eSYM_2kp1,               /**< enum value 2, Will apply filter only when symmetric filter is possible. */
            eSYM_2kp1_1SIDED_2kp1,   /**< enum value 3, Simplest form of SIAC and oneSided filter. */
            eSYM_2kp1_1SIDED_2kp2,   /**< enum value 4, Xli 2k+1 CentralBspl and 1 GeneralBSlp only at borders appropriately*/
            eSYM_2kp1_1SIDED_4kp1,   /**< enum value 5, SRV's 4k+1 CentralBspl at boundaries.*/
            eSYM_DER_2kp1_1SIDED_2kp1,   /**< enum value 6, Simple Derivative filter*/
            eSYM_DER_2kp1_1SIDED_2kp2,   /**< enum value 7, SRV's Derivative filter */
            eSYM_DER_2kp1_1SIDED_4kp1,    /**< enum value 8, Xli's Derivative fitler */
			eSYM_4kp1,
			eSYM_NDER_2kp1_1SIDED_2kp1,  /**<enum value 10, This is for derivative Filter but not applying Divided Difference> */
			eSYM_UNEVEN_2kp1,      /**<enum value 11, This is for new type of filter with uneven knots> */
			eSYM_DER_UNEVEN_2kp1      /**<enum value 11, This is for new type of filter with uneven knots> */
        };  

	struct SMOOTHIE_Status {
		int count;
		int cancelled;
		int SOURCE;
		int TAG;
		int ERROR;
	};
}
