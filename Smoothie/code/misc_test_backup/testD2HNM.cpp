#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"
#include <ctime>



using namespace SIACUtilities;
using namespace std;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>

int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		return 0;
	}

	argc = 2;

	clock_t startTime = clock();
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);

	HandleNekMesh* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	//HNM3D->LoadMesh(var[0]);
	//HNM3D->LoadMesh(var[1]);
	//HNM3D->LoadMesh(var[2]);
	
	cout << "loading xml file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	string fname = vSession->GetSessionName();

	//string fldname = fname + ".fld/Info.xml";	
	string fldname = fname + "_one.fld";	
	cout << fname << endl;
	cout << fldname << endl;


	HNM3D->LoadData( fldname ,var);
	cout << "loading fld file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	cout << "expsize:\t"<< HNM3D->m_expansions[0]->GetExpSize() << endl;

	Array<OneD,NekDouble> coeffs = HNM3D->m_expansions[0]->GetCoeffs();

	//HNM3D->printNekArray( coeffs, 0);
	

// print at particular grid interval and check it is working.






/*

	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM3D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);
	
	switch( HNM3D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}

	cout << "Total Number of quadrture points: "<<tNquadPts << endl; 
	cout << argv[1] << endl;
// test one point first.
	NekDouble xc = atof(argv[2]); NekDouble yc= atof(argv[3]); NekDouble zc= atof(argv[4]);
	Array<OneD,NekDouble> dir(3,0.0);
	dir[2] = 1.0;
	//dir[0] = 1.0/sqrt(3.0);	dir[1] = 1.0/sqrt(3.0);dir[2] = 1.0/sqrt(3.0);
	//dir[1] = 1.0/sqrt(2.0);	dir[2] =1.0/sqrt(2.0) ;dir[0] = 0.0;
	//dir[0] = 1.0/sqrt(3.0);	dir[1] =1.0/sqrt(3.0) ;dir[2] = 1.0/sqrt(3.0);
	vector<NekDouble> tPos;
	HNM3D->GetBreakPts(xc,yc,zc, dir, -0.1, atof(argv[6]),tPos,tPos,tPos,tPos);
	//HNM3D->GetBreakPts(xc,yc,zc, dir, atof(argv[5]), atof(argv[6]),tPos,tPos,tPos,tPos);

	for (int t =0; t< tPos.size(); t++)
	{
		cout << xc+tPos[t]*dir[0] << "\t" ;
		cout << yc+tPos[t]*dir[1] << "\t" ;
		cout << zc+tPos[t]*dir[2] << endl ;
	}

	NektarBaseClass k;
	HNM3D->printNekArray(tPos,0);	
*/
	return 0;
}


