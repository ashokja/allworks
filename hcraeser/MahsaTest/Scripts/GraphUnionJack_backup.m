%Graph union jack 8-113 for P1, P2, DG and PR.

clear;
close all;
MR = (1:16)*7+1;
%mshEl = [168, 672, 2688, 10752, 43008];
mshEl(1) = 168;    mshEl(2) = 672;    mshEl(3) = 1512;   mshEl(4) = 2688;
mshEl(5) = 4200;   mshEl(6) = 6048;   mshEl(7) = 8232;   mshEl(8) = 10752;
mshEl(9) = 13608;  mshEl(10) = 16800; mshEl(11) = 20328; mshEl(12) = 24192;
mshEl(13) = 28392; mshEl(14) = 32928; mshEl(15) = 37800; mshEl(16) = 43008;

addpath('..');
pPRCartfilename = '../PostP7_2/log_hex_unionjack_PR_CART_MR';
pPRMaxfilename = '../PostP7_1/log_hex_unionjack_PR_MAXSIZE_MR';
pDGCartfilename = '../PostP7_2/log_hex_unionjack_DG_CART_MR';
pDGMaxfilename = '../PostP7_1/log_hex_unionjack_DG_MAXSIZE_MR';




P = '1';
KF ='0';
UF = '0';
count =1;
for mr = MR
    filename = strcat(pPRCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_PR_C(count,:) = ReadFile(filename);
    count= count+1;
end
figure(11)
plot(log(mshEl),log(C_P1_PR_C(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P1_PR_C(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_PR_C(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P1_PR_C(:,4)),'-g.'); hold off;
title('PR P1 Filter Cart');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='0';
count =1;
for mr = MR
    filename = strcat(pPRMaxfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_PR_M(count,:) = ReadFile(filename);
    count= count+1;
end
figure(12)
plot(log(mshEl),log(C_P1_PR_M(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P1_PR_M(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_PR_M(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P1_PR_M(:,4)),'-g.'); hold off;
title('PR P1 Filter MaxSize');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='1';
count =1;
for mr = MR
    filename = strcat(pPRCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_PR_H(count,:) = ReadFile(filename);
    count= count+1;
end
figure(13)
plot(log(mshEl),log(C_P1_PR_H(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P1_PR_H(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_PR_H(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P1_PR_H(:,4)),'-g.'); hold off;
title('PR P1 Filter Hex');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;


P = '2';
KF ='0';
UF = '0';
count =1;
for mr = MR
    filename = strcat(pPRCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_PR_C(count,:) = ReadFile(filename);
    count= count+1;
end
figure(21)
plot(log(mshEl),log(C_P2_PR_C(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P2_PR_C(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_PR_C(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P2_PR_C(:,4)),'-g.'); hold off;
title('PR P2 Filter Cart');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='0';
count =1;
for mr = MR
    filename = strcat(pPRMaxfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_PR_M(count,:) = ReadFile(filename);
    count= count+1;
end
figure(22)
plot(log(mshEl),log(C_P2_PR_M(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P2_PR_M(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_PR_M(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P2_PR_M(:,4)),'-g.'); hold off;
title('PR P2 Filter MaxSize');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='1';
count =1;
for mr = MR
    filename = strcat(pPRCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_PR_H(count,:) = ReadFile(filename);
    count= count+1;
end
figure(23)
plot(log(mshEl),log(C_P2_PR_H(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P2_PR_H(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_PR_H(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P2_PR_H(:,4)),'-g.'); hold off;
title('PR P2 Filter Hex');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;


% %use HL and VL
% KF ='0';
% P = '1';
% KF ='0';
% UF = '0';
% count =1;
% for mr = MR
%     filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
%     C_P1_PR_C(count,:) = ReadFile(filename);
%     count= count+1;
% end
% figure(3)
% plot(log(mshEl),log(C_P1_PR_C(:,1)),'-b*'); hold on;
% plot(log(mshEl),log(C_P1_PR_C(:,2)),'-g*'); hold on;
% plot(log(mshEl),log(C_P1_PR_C(:,3)),'-b.'); hold on;
% plot(log(mshEl),log(C_P1_PR_C(:,4)),'-g.'); hold off;
% title('PR P1 Filter EdgeSize');
% 
% %use HL and VL
% KF ='0';
% P = '2';
% KF ='0';
% UF = '0';
% count =1;
% for mr = MR
%     filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
%     C_P2_DG_C(count,:) = ReadFile(filename);
%     count= count+1;
% end
% figure(2)
% plot(log(mshEl(1:4)),log(C_P2_DG_C(1:4,1)),'-r*'); hold on;
% plot(log(mshEl(1:4)),log(C_P2_DG_C(1:4,2)),'-g*'); hold on;
% plot(log(mshEl(1:4)),log(C_P2_DG_C(1:4,3)),'-r.'); hold on;
% plot(log(mshEl(1:4)),log(C_P2_DG_C(1:4,4)),'-g.'); hold off;
% title('DG P2 Filter EdgeSize');
% 
P = '1';
KF ='0';
UF = '0';
count =1;
for mr = MR
    filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_DG_C(count,:) = ReadFile(filename);
    count= count+1;
end
figure(31)
plot(log(mshEl),log(C_P1_DG_C(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P1_DG_C(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_DG_C(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P1_DG_C(:,4)),'-g.'); hold off;
title('DG P1 Filter Cart');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='0';
count =1;
for mr = MR
    filename = strcat(pDGMaxfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_DG_M(count,:) = ReadFile(filename);
    count= count+1;
end
figure(32)
plot(log(mshEl),log(C_P1_DG_M(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P1_DG_M(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_DG_M(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P1_DG_M(:,4)),'-g.'); hold off;
title('DG P1 Filter MaxSize');

KF ='1';
count =1;
for mr = MR
    filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P1_DG_H(count,:) = ReadFile(filename);
    count= count+1;
end
figure(33)
plot(log(mshEl),log(C_P1_DG_H(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P1_DG_H(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P1_DG_H(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P1_DG_H(:,4)),'-g.'); hold off;
title('DG P1 Filter Hex');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;


P = '2';
KF ='0';
UF = '0';
count =1;
for mr = MR
    filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_DG_C(count,:) = ReadFile(filename);
    count= count+1;
end
figure(41)
plot(log(mshEl),log(C_P2_DG_C(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P2_DG_C(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_DG_C(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P2_DG_C(:,4)),'-g.'); hold off;
title('DG P2 Filter Cart');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='0';
count =1;
for mr = MR
    filename = strcat(pDGMaxfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_DG_M(count,:) = ReadFile(filename);
    count= count+1;
end
figure(42)
plot(log(mshEl),log(C_P2_DG_M(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P2_DG_M(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_DG_M(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P2_DG_M(:,4)),'-g.'); hold off;
title('DG P2 Filter MaxSize');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;

KF ='1';
count =1;
for mr = MR
    filename = strcat(pDGCartfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C_P2_DG_H(count,:) = ReadFile(filename);
    count= count+1;
end
figure(43)
plot(log(mshEl),log(C_P2_DG_H(:,1)),'-b*'); hold on;
plot(log(mshEl),log(C_P2_DG_H(:,2)),'-g*'); hold on;
plot(log(mshEl),log(C_P2_DG_H(:,3)),'-b.'); hold on;
plot(log(mshEl),log(C_P2_DG_H(:,4)),'-g.'); hold off;
title('DG P2 Filter Hex');
xlabel('log(MshSize)');
ylabel('log(Error)')
axis equal;