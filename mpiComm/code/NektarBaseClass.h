#pragma once

#define NEKTAR_USING_LAPACK
#define NEKTAR_USING_BLAS

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include <LibUtilities/Communication/Comm.h>
#include <MultiRegions/ContField1D.h>
#include <SpatialDomains/MeshGraph1D.h>

#include <iostream>
#include <vector>

using namespace std;
using namespace Nektar;

/// This class help import all nektar base classes.
class NektarBaseClass{
	private:
	protected:
	public:
	void printNekArray(Array<OneD,NekDouble> &ar,int del)const;
};


