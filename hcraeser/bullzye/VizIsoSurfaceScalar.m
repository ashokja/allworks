function [] = VizIsoSurfaceScalar(xcheb,ycheb,zcheb,UAtCheb,sx,sy,sz)
    switch nargin
        case 4
            sx = [-1];
            sy = [-1,0,1];
            sz = [-1];
    end
    [X,Y,Z] = meshgrid(xcheb,ycheb,zcheb);
    normAtCheb = UAtCheb;
    normAtCheb = permute(normAtCheb,[2,1,3]);

    hsurfaces = slice(X,Y,Z,normAtCheb,sx,sy,sz);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar;
end