clear;
close all;
% test 

xpos = textread('xc0_Der2kp1.txt', '%f\t');
fval = textread('fce_Der2kp1.txt', '%f\t');
fvalD = textread('fceD_Der2kp1.txt', '%f\t');
pval = textread('ece_Der2kp1.txt', '%f\t');
pvalD = textread('eceD_Der2kp1.txt', '%f\t');
sval = textread('sce_Der2kp1.txt', '%f\t');


xs = 1;
xe = 63;

figure(1)
plot(xpos, fval,xpos,pval,xpos,sval);
xlabel('1D mesh(x)')
ylabel('values')
legend('Actual function','projected','SIAC post-processed');

figure(2)
plot(xpos, fvalD,xpos,pvalD,xpos,sval);
xlabel('1D mesh(x)')
ylabel('values')
legend('Actual function','projected','SIAC post-processed');

figure(3),
plot( xpos(xs:xe), abs(fvalD(xs:xe)-pvalD(xs:xe)), xpos(xs:xe), abs(fvalD(xs:xe)-sval(xs:xe)));
xlabel('1D mesh(x)')
ylabel('error')
legend('projection error','SIAC error');

%%
% 
% figure(4)
% plot(xpos, fval,xpos, cos(pi*xpos/10));
% 
% figure(5)
% plot(xpos, pvalD,xpos, -1*pi/10*sin(pi*xpos/10));