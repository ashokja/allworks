% For testing polynomail approximation.

% For testing polynomail approximation.

clear;
close all;

N = (2:1:10)
%    clearvars -except NN N
load('testdata/jacX');
load('testdata/jacWX');

for NN = N
    for n = 0:10
        Deg = NN; % Deg = NN
    tic
        NX =NN; NY=NN; NZ=NN;
        nx=n;ny=n;nz=n;
        addpath ../createTestData
        addpath ../PolyLib
        px = 10;py = 10; pz = 10;
        % call test_run to get the function defined at that location.
        
        XQ = XQN{NX+2+nx}; WXQ = WXQN{NX+2+nx};
        YQ = XQN{NY+2+ny}; WYQ = WXQN{NY+2+ny};
        ZQ = XQN{NZ+2+nz}; WZQ = WXQN{NZ+2+nz};

        [X,Y,Z] = meshgrid(XQ,YQ,ZQ);
        X = permute(X,[2,1,3]); Y = permute(Y,[2,1,3]); Z = permute(Z,[2,1,3]);
 %      [xy,distance,t_a,mapxyV,curveCoeff] = test_runXYZ(1,mapxy);
        [UAtGrid,VAtGrid,WAtGrid] = testPolyDeg(XQ,YQ,ZQ,Deg);

        coeffU = CalLegendreCoeff(UAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);
%         coeffV = CalLegendreCoeff(VAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);
%         coeffW = CalLegendreCoeff(WAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);

%         [DChebX,xcheb] = cheb(px);
%         [DChebY,ycheb] = cheb(py);
%         [DChebZ,zcheb] = cheb(pz);
        xcheb = (-1:2/(px-1):1);
        ycheb = (-1:1/(px-1):1);
        zcheb = (-1:1/(px-1):1);
        

        UAtCheb = CalValueAt(coeffU,xcheb,ycheb,zcheb);
%         VAtCheb = CalValueAt(coeffV,xcheb,ycheb,zcheb);
%         WAtCheb = CalValueAt(coeffW,xcheb,ycheb,zcheb);

        [UCGrid,VCGrid,WCGrid] = testPolyDeg(xcheb,ycheb,zcheb,Deg);

        Uer = abs(UAtCheb - UCGrid);
%         Ver = abs(VAtCheb - VCGrid);
%         Wer = abs(WAtCheb - WCGrid);
        
        er_UMax(NN,n+1) = max(Uer(:));
%         er_VMax(NN,n+1) = max(Ver(:));
%         er_WMax(NN,n+1) = max(Wer(:));

        er_U(NN,n+1) = sum(Uer(:))/(px*py*pz);
%         er_V(NN,n+1) = sum(Ver(:))/(px*py*pz);
%         er_W(NN,n+1) = sum(Wer(:))/(px*py*pz);
        
    toc
    end
end
%save('testData/PloyAppoximation');


%%
% 
er_Max = er_UMax;
%er_L2 = (er_U+er_V+er_W)/3;

figure(1)
plot(3+(1:11),er_Max(2,:),...
        4+(1:11),er_Max(3,:),...
        5+(1:11),er_Max(4,:),...
        6+(1:11),er_Max(5,:),...
        7+(1:11),er_Max(6,:),...
        8+(1:11),er_Max(7,:),...
        9+(1:11),er_Max(8,:),...
        10+(1:11),er_Max(9,:),...
        11+(1:11),er_Max(10,:)...
    );
    xlabel('N+2+k = No quadrature in X,Y,Z direction ');
    ylabel('Max(ActualFunc - ProjectedFunc)')
    legend('Deg2','Deg3','Deg4','Deg5','Deg6', 'Deg7','Deg8','Deg9', 'Deg10');
    text(3,er_Max(2,1),' Deg2');
    text(4,er_Max(3,1),' Deg3');
    text(5,er_Max(4,1),' Deg4');
    text(6,er_Max(5,1),' Deg5');
    text(7,er_Max(6,1),' Deg6');
    text(8,er_Max(7,1),' Deg7');
    text(9,er_Max(8,1),' Deg8');
    text(10,er_Max(9,1),' Deg9');
    text(11,er_Max(10,1),' Deg10');
    title('Projection error of function fSin')
%     
%     
% figure(2)
% plot(1:10,er_L2(2,:),...
%         1:10,er_L2(2,:),...
%         1:10,er_L2(3,:),...
%         1:10,er_L2(4,:),...
%         1:10,er_L2(5,:),...
%         1:10,er_L2(6,:),...
%         1:10,er_L2(7,:),...
%         1:10,er_L2(8,:),...
%         1:10,er_L2(9,:),...
%         1:10,er_L2(10,:)...
%     );
% xlabel('x = Total Points - Polynomial Degree');
% ylabel('Average(L2) Error')
%     legend('Deg2','Deg3','Deg4','Deg5','Deg6', 'Deg7','Deg8','Deg9', 'Deg10');
%     text(1,er_L2(2,1),' Deg2');
%     text(1,er_L2(3,1),' Deg3');
%     text(1,er_L2(4,1),' Deg4');
%     text(1,er_L2(5,1),' Deg5');
%     text(1,er_L2(6,1),' Deg6');
%     text(1,er_L2(7,1),' Deg7');
%     text(1,er_L2(8,1),' Deg8');
%     text(1,er_L2(9,1),' Deg9');
%     text(1,er_L2(10,1),' Deg10');
%     title('Avg Error vs (x = Total Points - Polynomial Degree)')
