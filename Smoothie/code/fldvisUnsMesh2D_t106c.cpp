#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include <boost/math/special_functions/sinc.hpp>
#include <boost/timer.hpp>
#include <chrono>

typedef std::chrono::high_resolution_clock Clock;


using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 6)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd conditions file" << endl;
		cout << "3rd arg polynomial degree filter you want to apply" << endl;
		cout << "4th arg Resolution of output." << endl;
		cout << "5th arg for variable to postprocess" << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	for (int i=0; i < var.size(); i++)
	{
		cout << i << " : " << var[i] << endl;
	}
	//HNM2D->LoadMesh(var[0]);
	string fname = vSession->GetSessionName();
	HNM2D->LoadData(fname+".fld",var);

	// 0-rho; 1-rhou;2-rhov;3-E
	
	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();

	cout << "check1 " << endl;
	//NekDouble startX=1.14 , startY=-0.048 ;
	//NekDouble endX=1.2 , endY=-0.068 ;
	NekDouble startX=1.15 , startY=0.0 ;
	NekDouble endX=1.225 , endY=-0.03 ;


	int paramVar = atoi(argv[1+4]);
	// Getting rhou
	//Array<OneD,NekDouble> exp_rhoU = HNM2D->m_expansions[paramVar]->GetPhys();
	int totPhys = HNM2D->m_expansions[0]->GetTotPoints();
	const Array<OneD,NekDouble> u_phys = HNM2D->m_expansions[1]->GetPhys();
	const Array<OneD,NekDouble> v_phys = HNM2D->m_expansions[2]->GetPhys();
	Array<OneD,NekDouble> ux_phys(totPhys), vx_phys(totPhys);
	Array<OneD,NekDouble> uy_phys(totPhys), vy_phys(totPhys);

	HNM2D->m_expansions[0]->PhysDeriv(u_phys, ux_phys,uy_phys);
	HNM2D->m_expansions[0]->PhysDeriv(u_phys, ux_phys,uy_phys);

	cout << "check2 " << endl;

// Evaluate on a new equal space grid mesh.
	int gPts = atoi(argv[1+3]);
	int totPts = gPts*gPts;
	int Nx = gPts, Ny=gPts;
	//NekDouble sx = 0.5/(Nx-1.0), sy = 1.0/(Ny-1.0);
	NekDouble sx = 1.0/(Nx-1.0)*(endX-startX), sy = 1.0/(Ny-1.0)*(endY-startY);
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1, HNM2D, atoi(argv[1+2]), scaling ); 
	SmoothieSIAC2D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[1+2]),scaling,1);
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	Array<OneD,NekDouble> directionX(3,0.0), directionY(3,0.0),coord(3,0.0);
	directionX[0] = 1.0;
	directionY[1] = 1.0;
	
	Array<OneD,NekDouble> pX(totPts), pY(totPts),pDyn(totPts),pE(totPts);
	Array<OneD,NekDouble> pUy_S(totPts),pVx_S(totPts);
	Array<OneD,NekDouble> pUy_SDyn(totPts),pVx_SDyn(totPts);
	Array<OneD,NekDouble> pUy_P(totPts),pVx_P(totPts);
	Array<OneD,NekDouble> pTDyn(totPts);
	Array<OneD,NekDouble> pT(totPts);
	Array<OneD,NekDouble> glCoords(3,0.0);
	bool TDynRes, TRes;

	for( int i =0; i < Nx; i++)
	{
		for(int j=0; j< Ny; j++)
		{
			int index = i*Nx+j;
			//pX[index] = i*sx+0.25;
			pX[index] = startX + i*sx;
			pY[index] = startY + j*sy;
			//pV[index] = std::cos(2.0*M_PI*(pX[index] + pY[index]) );
			//pV[index] = std::cos(2.0*2.0*M_PI*(pX[index]))*std::cos(2.0*2.0*M_PI*(pY[index]));
			glCoords[0] = pX[index]; glCoords[1] = pY[index];
		
			// Calculate using dynamic L-SIAC	
				NekDouble dynScaling = HNM2D->GetDynamicScaling(glCoords);
				pDyn[index] = dynScaling;
			//	TDynRes = sm.EvaluateAt(pX[index],pY[index],0.0,pSDyn[index],valY,valZ,direction, dynScaling ,paramVar);
				TDynRes = smD.EvaluateAt_SymY(pX[index],pY[index],0.0,pUy_SDyn[index],valY,valZ,directionY, dynScaling ,1);
				TDynRes = smD.EvaluateAt_SymY(pX[index],pY[index],0.0,pVx_SDyn[index],valY,valZ,directionX, dynScaling ,2);
	
			// Calculating for L-SIAC
				TRes = smD.EvaluateAt_SymY(pX[index],pY[index],0.0,pUy_S[index],valY,valZ,directionX, scaling,1);
				TRes = smD.EvaluateAt_SymY(pX[index],pY[index],0.0,pVx_S[index],valY,valZ,directionY, scaling,2);

			// Calculate dG value.
			coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = 0.0;
//			cout << setprecision(19) << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;
			int elid = HNM2D->GetExpansionIndexUsingRTree(coord);
//			cout << elid << endl;
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
			//int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
			const Array<OneD,NekDouble> el_uy_phys = uy_phys.CreateWithOffset(
										uy_phys, coeffOffset);
			const Array<OneD,NekDouble> el_vx_phys = vx_phys.CreateWithOffset(
										vx_phys, coeffOffset);
			pUy_P[index] = lexp->PhysEvaluate(coord,el_uy_phys);
			pVx_P[index] = lexp->PhysEvaluate(coord,el_vx_phys);

			pE[index] = elid;

		}
		cout << i << endl;
	}

	NektarBaseClass k;
	k.writeNekArray(pX,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_Param_"+argv[1+4]+"_pX_2DDyn.txt");
	k.writeNekArray(pY,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_Param_"+argv[1+4]+"_pY_2DDyn.txt");
	k.writeNekArray(pE,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_Param_"+argv[1+4]+"_pE_2DDyn.txt");
	//k.writeNekArray(pV,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pV_2DDyn.txt");

	k.writeNekArray(pVx_P,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_Param_"+argv[1+4]+"_pVx_P_2DDyn.txt");
	k.writeNekArray(pUy_P,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_Param_"+argv[1+4]+"_pUy_P_2DDyn.txt");
	k.writeNekArray(pVx_SDyn,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_Param_"+argv[1+4]+"_pVx_SDyn_2DDyn.txt");
	k.writeNekArray(pUy_SDyn,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_Param_"+argv[1+4]+"_pUy_SDyn_2DDyn.txt");
	k.writeNekArray(pVx_S,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_Param_"+argv[1+4]+"_pVx_S_2DDyn.txt");
	k.writeNekArray(pUy_S,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_Param_"+argv[1+4]+"_pUy_S_2DDyn.txt");
	k.writeNekArray(pDyn,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_Param_"+argv[1+4]+"_pDyn_2DDyn.txt");
//	k.writeNekArray(pTDyn,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_Param_"+argv[1+4]+"_pTDyn_2DDyn.txt");
//	k.writeNekArray(pT,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_Param_"+argv[1+4]+"_pT_2DDyn.txt");

	cout << "Done writing plane" << endl;
/*
 	//plane completed.
	// Evaluating on line y =0;
	int gPtsLy = atoi(argv[1+3])*10;
	int totPtsLy = gPtsLy;
	sy = 1.0/(gPtsLy-1.0);
	Array<OneD,NekDouble> pLy(totPtsLy),pLNx(totPtsLy), pLPx(totPtsLy), pELy(totPtsLy);
	Array<OneD,NekDouble> pXLy(totPtsLy), pYLy(totPtsLy), pVLy(totPtsLy), pPLy(totPtsLy), pSLy(totPtsLy),pDynLy(totPtsLy),pSDynLy(totPtsLy);
	Array<OneD,NekDouble> pTDynLy(totPtsLy), pTLy(totPtsLy);

	for (int i =0 ; i< gPtsLy; i++)
	{
		int index = i;
		pXLy[index] = 0.5;
		pYLy[index] = i*sy;
			//pVLy[index] = std::cos(2.0*M_PI*(pXLy[index] + pYLy[index]) );
			pVLy[index] = boost::math::sinc_pi(4.0*M_PI*( pXLy[index] - 0.5 )) * boost::math::sinc_pi(4.0*M_PI*( pYLy[index] - 0.5 )) ;
			glCoords[0] = pXLy[index]; glCoords[1] = pYLy[index];
		//	cout << pYLy[index]<< endl;

			boost::timer tim1;
			NekDouble dynScaling = HNM2D->GetDynamicScaling(glCoords);
			pDynLy[index] = dynScaling;
			sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSDynLy[index],valY,valZ,direction, dynScaling ,0);
			pTDynLy[index] = tim1.elapsed();

			boost::timer tim2;
			sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSLy[index],valY,valZ,direction, scaling ,0);
			pTLy[index] = tim2.elapsed();

			coord[0] = pXLy[index]; coord[1] = pYLy[index]; coord[2] = 0.0;
//			cout << setprecision(19) << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;
			int elid = HNM2D->GetExpansionIndexUsingRTree(coord);
//			cout << elid << endl;
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
			//int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
			const Array<OneD,NekDouble> el_phys = ece.CreateWithOffset(
										ece, coeffOffset);
			pPLy[index] = lexp->PhysEvaluate(coord,el_phys);
			pELy[index] = elid;
//			cout << j << endl;
	}



	cout << "Done writing Line" << endl;
	k.writeNekArray(pXLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pXLy_2DDyn.txt");
	k.writeNekArray(pYLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pYLy_2DDyn.txt");
	k.writeNekArray(pELy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pELy_2DDyn.txt");
	k.writeNekArray(pVLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pVLy_2DDyn.txt");
	k.writeNekArray(pPLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pPLy_2DDyn.txt");
	k.writeNekArray(pSLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pSLy_2DDyn.txt");
	k.writeNekArray(pSDynLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pSDynLy_2DDyn.txt");
	k.writeNekArray(pDynLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pDynLy_2DDyn.txt");
	k.writeNekArray(pTDynLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pTDynLy_2DDyn.txt");
	k.writeNekArray(pTLy,fname+"_"+argv[1+2]+"_RSfld_"+argv[1+3]+"_pTLy_2DDyn.txt");

	// Evaluate on line x =0.5; x = -0.5;
*/
/*
	 string out = vSession->GetSessionName() + ".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (HNM2D->m_expansions[0]->GetFieldDefinitions()).at(0);
    FieldDef[0]->m_fields.push_back("u");
    FieldDef[0]->m_fields.push_back("v");
    FieldDef[0]->m_fields.push_back("w");
    HNM2D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_fld->Write(out, FieldDef, FieldData);
*/
	return 0;
}
/*	for( int i =0; i < Nx; i++)
	{
		for(int j=0; j< Ny; j++)
		{
			int index = i*Nx+j;
			//pX[index] = i*sx+0.25;
			pX[index] = startX + i*sx;
			pY[index] = startY + j*sy;
			//pV[index] = std::cos(2.0*M_PI*(pX[index] + pY[index]) );
			//pV[index] = std::cos(2.0*2.0*M_PI*(pX[index]))*std::cos(2.0*2.0*M_PI*(pY[index]));
			glCoords[0] = pX[index]; glCoords[1] = pY[index];
			
	// calculating for dynamic L-SIAC
			boost::timer tim1;
			bool TDynRes, TRes;
			auto t1 = Clock::now();
				NekDouble dynScaling = HNM2D->GetDynamicScaling(glCoords);
				pDyn[index] = dynScaling;
				TDynRes = sm.EvaluateAt(pX[index],pY[index],0.0,pSDyn[index],valY,valZ,direction, dynScaling ,paramVar);
			auto t2 = Clock::now();
		    std::chrono::duration<double> elapsed_seconds = t2-t1;
			pTDyn[index] = elapsed_seconds.count();
//std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();

	// Calculating for L-SIAC
			//pTDyn[index] = tim1.elapsed();
			//boost::timer tim2;
			auto tt1 = Clock::now();
				TRes = sm.EvaluateAt(pX[index],pY[index],0.0,pS[index],valY,valZ,direction, scaling,paramVar);
			auto tt2 = Clock::now();
			//pT[index] = std::chrono::duration_cast<std::chrono::nanoseconds>(tt2 - tt1).count();
		    std::chrono::duration<double> elapsed_secondst = tt2-tt1;
			pT[index] = elapsed_secondst.count();
			//cout << pT[index] << "\t " <<pTDyn[index] << endl;
		//	pT[index] = tim2.elapsed();
		//	cout << TRes << "\t " << TDynRes << endl;
		//		cout << pT[index] << "\t " <<pTDyn[index] << endl;
		//	if ((TDynRes && TRes))
		//	{
		//	}else{
		//	}

	// DG value at this point.
			coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = 0.0;
//			cout << setprecision(19) << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;
			int elid = HNM2D->GetExpansionIndexUsingRTree(coord);
//			cout << elid << endl;
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
			//int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
			const Array<OneD,NekDouble> el_phys = exp_rhoU.CreateWithOffset(
										exp_rhoU, coeffOffset);
			pP[index] = lexp->PhysEvaluate(coord,el_phys);
			pE[index] = elid;
//			cout << j << endl;
		}
		cout << i << endl;
	}
*/
