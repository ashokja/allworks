#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

int main(int argc, char* argv[])
{
	NektarBaseClass k;
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg conditions file." << endl;
		cout << "4rd arg meshscaling you want to use." << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();
	HNM2D->LoadData(fname+".fld",var);
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	MetricTensor* metricT = new MetricTensor();
	metricT->LoadMetricTensor(HNM2D);
	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);

 //	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);
	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*M_PI*(xc0[i])) *std::sin(2.0*M_PI*(xc1[i]));
	}

	HNM2D->m_expansions[0]->FwdTrans(HNM2D->m_expansions[0]->GetPhys(),HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	 ece = HNM2D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM2D->m_expansions[0]->GetCoeffs();

	HNM2D->m_Arrays.push_back(ece);
	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();
// Evaluate on a new equal space grid mesh.
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[3]), atof(argv[4]) ); 
	SmoothieSIAC2D sm_NONSYM(SIACUtilities::eSYM_UNEVEN_2kp1, HNM2D, atoi(argv[1+2]), atof(argv[4]) );

    Array<OneD,NekDouble> pX(tNquadPts),pY(tNquadPts),pE(tNquadPts),pP_p(tNquadPts),pV_p(tNquadPts),coord(3,0.0);
    Array<OneD,NekDouble> pS(tNquadPts), pSDyn(tNquadPts), pSNUK(tNquadPts),pDyn(tNquadPts);
    Array<OneD,NekDouble> pSY(tNquadPts), pSDynY(tNquadPts), pSNUKY(tNquadPts),pSNUK_Eig1(tNquadPts),pSNUK_Eig2(tNquadPts);
    NekDouble valY,valZ,lambda,lambda2;
    Array<OneD,NekDouble> direction(3,0.0),directionY(3,0.0),eig(3,0.0),eig2(3,0.0);
    direction[0] = 1.0;
    directionY[1] = 1.0;

    for(int elId = 0; elId < HNM2D->m_expansions[0]->GetExpSize(); elId++)
    {
        int physOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elId);
        const Array<OneD,NekDouble> el_u_phys = ece.CreateWithOffset(ece,physOffset);

        LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elId);
        
        int numPtsEl = HNM2D->m_expansions[0]->GetExp(elId)->GetTotPoints();
        for(int i=0; i< numPtsEl; i++)
        {
            int index = physOffset+i;
            pX[index] = xc0[index]; 
            pY[index] = xc1[index];
            pE[index] = elId;
            coord[0] = xc0[index]; 
            coord[1] = xc1[index]; 
            pP_p[index] = lexp->PhysEvaluate(coord,el_u_phys);
            pV_p[index] = std::cos(2.0*M_PI*(xc0[index]))*std::sin(2.0*M_PI*(xc1[index]));
            //pS[index] = 
            //pSDyn[index] = 
            //pSNUK[index] = 
	/*		
            NekDouble dynScaling = HNM2D->GetDynamicScaling(coord);
			pDyn[index] = dynScaling;
			sm.EvaluateAt(pX[index],pY[index],0.0,pS[index],valY,valZ,direction, atof(argv[4]) ,0);
			sm.EvaluateAt(pX[index],pY[index],0.0,pSDyn[index],valY,valZ,direction, dynScaling ,0);
			sm.EvaluateAt(pX[index],pY[index],0.0,pSDynY[index],valY,valZ,directionY, dynScaling ,0);
		
            sm_NONSYM.EvaluateAt_NUK_MetricTensor(pX[index],pY[index],0.0,pSNUK[index], 
																valY, valZ, direction, dynScaling,0);
            sm_NONSYM.EvaluateAt_NUK_MetricTensor(pX[index],pY[index],0.0,pSNUKY[index], 
																valY, valZ, directionY, dynScaling,0);
	*/	    
            
            metricT->GetEigenPair(coord,elId,1,lambda,eig);
            metricT->GetEigenPair(coord,elId,2,lambda2,eig2);
            sm_NONSYM.EvaluateAt_NUK_MetricTensor(pX[index],pY[index],0.0,pSNUK_Eig1[index], 
																valY, valZ, eig, lambda,0);
            sm_NONSYM.EvaluateAt_NUK_MetricTensor(pX[index],pY[index],0.0,pSNUK_Eig2[index], 
																valY, valZ, eig2, lambda2,0);


        }
    }

	k.writeNekArray(xc0,fname+"_xc0_EL.txt");
	k.writeNekArray(xc1,fname+"_xc1_EL.txt");
	k.writeNekArray(fce,fname+"_fce_EL.txt");
	k.writeNekArray(ece,fname+"_ece_EL.txt");
	
    k.writeNekArray(pX,fname+"_pX_EL.txt");
	k.writeNekArray(pY,fname+"_pY_EL.txt");
	k.writeNekArray(pE,fname+"_pE_EL.txt");
	k.writeNekArray(pP_p,fname+"_pP_p_EL.txt");
	k.writeNekArray(pV_p,fname+"_pV_p_EL.txt");
   /* 
    k.writeNekArray(pS,fname+"_pS_EL.txt");
    k.writeNekArray(pSDyn,fname+"_pSDyn_EL.txt");
    k.writeNekArray(pSNUK,fname+"_pSNUK_EL.txt");
    k.writeNekArray(pDyn,fname+"_pDyn_EL.txt");
    k.writeNekArray(pSY,fname+"_pSY_EL.txt");
    k.writeNekArray(pSDynY,fname+"_pSDynY_EL.txt");
    k.writeNekArray(pSNUKY,fname+"_pSNUKY_EL.txt");
    */
    k.writeNekArray(pSNUK_Eig1,fname+"_pSNUK_Eig1_EL.txt");
    k.writeNekArray(pSNUK_Eig2,fname+"_pSNUK_Eig2_EL.txt");
	
/*
	 string out = vSession->GetSessionName() + ".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (HNM2D->m_expansions[0]->GetFieldDefinitions()).at(0);
    FieldDef[0]->m_fields.push_back("u");
    FieldDef[0]->m_fields.push_back("v");
    FieldDef[0]->m_fields.push_back("w");
    HNM2D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_fld->Write(out, FieldDef, FieldData);
*/
	return 0;
}


