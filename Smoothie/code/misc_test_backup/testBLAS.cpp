#include <iostream>
#include <Eigen/Dense>
#include "NektarBaseClass.h"

//using Eigen::MatrixXd;

using namespace std;
//using namespace Eigen;


int factorial(int jj);
void CalCoeffForStandardKernel(int deg, Nektar::Array<OneD,NekDouble> &coeffs,
						const NekDouble shift =0.0);
NekDouble dividedDiff(int ii, int jj,const Nektar::Array<OneD,NekDouble> &tknots, NekDouble x, NekDouble D);
NekDouble Monomial(NekDouble t, NekDouble x, int der, int degree);
void CalCoeffForWideSymKernel( int deg, const Nektar::Array<OneD,NekDouble> &tknots, 
						Nektar::Array<OneD,NekDouble> &coeffs, const NekDouble shift=0.0);
void CalCoeffForWideSymKernel(const int deg, const int nSpl,
			Nektar::Array<OneD,NekDouble> &coeffs,const NekDouble shift=0.0);

int main(int argc, char* argv[])
{
	int degree = atoi(argv[1]);
	int nBSpl = atoi(argv[2]);
	Nektar::Array<OneD,NekDouble> coeffs(nBSpl);

	CalCoeffForWideSymKernel( degree, nBSpl, coeffs,0.0);
	
	NektarBaseClass nbc;
	nbc.printNekArray(coeffs,0);

/*
	CalCoeffForStandardKernel(degree, coeffs, (degree+1.0)/2.0+degree);
	//CalCoeffForStandardKernel(degree, coeffs);
	NektarBaseClass nbc;
	nbc.printNekArray(coeffs,0);

	CalCoeffForWideSymKernel( degree, 2*degree+1, coeffs,0.0);
	nbc.printNekArray(coeffs,0);

	Nektar::Array<OneD,NekDouble> coeffs3(4*degree+1);
	CalCoeffForWideSymKernel( degree, 4*degree+1, coeffs3,0.0);
	nbc.printNekArray(coeffs3,0);

	Nektar::Array<OneD,NekDouble> coeffs4(2*degree+1);
	CalCoeffForWideSymKernel( degree, 2*degree+1, coeffs4, (degree+1.0)/2.0+degree);
	nbc.printNekArray(coeffs4,0);
	
	Nektar::Array<OneD,NekDouble> coeffs5(4*degree+1);
	CalCoeffForWideSymKernel( degree, 4*degree+1, coeffs5, (degree+1.0)/2.0+2.0*degree);
	nbc.printNekArray(coeffs5,0);
*/
}


void CalCoeffForStandardKernel(int deg, Nektar::Array<OneD,NekDouble> &coeffs, const NekDouble shift)
{
	Nektar::Array<OneD,NekDouble> tknots(3*deg+2);
	for (int i =0; i < tknots.num_elements();i++)
	{
		tknots[i] = -(deg+1.0)/2.0-deg +i+shift;
	}
	CalCoeffForWideSymKernel( deg, tknots, coeffs,shift);
	
}

void CalCoeffForWideSymKernel(const int deg, const int nSpl,
			Nektar::Array<OneD,NekDouble> &coeffs,const NekDouble shift)
{
	Nektar::Array<OneD,NekDouble> tknots( nSpl+deg+1);
	int jj = 0;
	for (int i =0; i < tknots.num_elements(); i++)
	{
		tknots[i] = -(deg+1.0)/2.0 - (nSpl-1.0)/2.0 + i +shift; 
	}
	CalCoeffForWideSymKernel( deg, tknots, coeffs, 0.0);
	NektarBaseClass nbc;
	nbc.printNekArray(tknots,0);
}

void CalCoeffForWideSymKernel( int deg, const Nektar::Array<OneD,NekDouble> &tknots, 
						Nektar::Array<OneD,NekDouble> &coeffs, const NekDouble shift)
{
	int jj=0;
	jj = tknots.num_elements() - 2* deg-2;
	Eigen::MatrixXd M0( deg + jj+1, deg + jj+1);
	Eigen::VectorXd e1(deg + jj+1);
	for (int r =0; r< deg+jj+1; r++)
	{
		for (int l = 0; l < deg+jj+1; l++)
		{
			int delta = r;
			int gamma = l;
			M0(r,l) = dividedDiff( gamma, gamma+deg+1, tknots, -1.0*shift, deg+delta+1);
		}
	}
	for ( int i =0; i < deg+ jj+1; i++)
	{
		e1(i) = 0.0;
	}
	e1(0) = 1.0;
	Eigen::VectorXd x = M0.colPivHouseholderQr().solve(e1);
	for (int i =0; i < deg+jj+1; i++)
	{
		coeffs[i] = x(i);
	}
}

NekDouble dividedDiff(int ii, int jj, const Nektar::Array<OneD,NekDouble> &tknots, NekDouble x, NekDouble D)
{
	NekDouble result =0.0;
	if ( ii== jj)
	{
		result = Monomial(tknots[ii], x,0,D);
	}else if( abs(tknots[ii]-tknots[jj]) < TOLERENCE)
	{
		result = Monomial(tknots[ii],x,jj-ii,D)/( (NekDouble)factorial(jj-ii) );
	}else{
		result = ( dividedDiff(ii+1,jj,tknots,x,D) - dividedDiff(ii,jj-1,tknots,x,D) )/ (tknots[jj]-tknots[ii] );
	}
	return result;
}


NekDouble Monomial(NekDouble t, NekDouble x, int der, int degree)
{
	NekDouble y = t-x;
	NekDouble result = pow(y, degree-der)* ( (NekDouble)factorial(degree) )/( (NekDouble)factorial(degree-der) );
	return result;
}

int factorial(int jj)
{
	int fact = 1;
	for (int i =1; i<= jj; i++)
	{
		fact *= i;
	}
	return fact;
}
