% P1
% quadrature cal error.
% Hex1 siac filter.
% hex union jack.
% DG


clear;

line0= '\caption{DG solution, m = 1.0, HL = L, Hex_sym, Hex(1) SIAC filter.}';
% Test errors.
Deg=1;
ord1 = 15;
ord2 = 29;
ord3 = 57;
ord4 = 113;

L2BH1_ord1 = 1.91e-2;
L2BH1_ord2 = 3.40e-3;
L2BH1_ord3 = 7.26e-4;
L2BH1_ord4 = 1.73e-4;

LinfBH1_ord1 = 5.53e-2;
LinfBH1_ord2 = 1.15e-2;
LinfBH1_ord3 = 2.56e-3;
LinfBH1_ord4 = 6.92e-4;

L2H1_ord1 = 1.62e-2;
L2H1_ord2 = 2.10e-3;
L2H1_ord3 = 2.64e-4;
L2H1_ord4 = 3.31e-5;

LinfH1_ord1 = 2.39e-2;
LinfH1_ord2 = 3.23e-3;
LinfH1_ord3 = 4.39e-4;
LinfH1_ord4 = 6.30e-5;

orderH1_L2(1) = 0;
orderH1_L2(2) = log(L2H1_ord2/L2H1_ord1)/log(ord1/ord2);
orderH1_L2(3) = log(L2H1_ord3/L2H1_ord2)/log(ord2/ord3);
orderH1_L2(4) = log(L2H1_ord4/L2H1_ord3)/log(ord3/ord4);

orderH1_Linf(1) = 0;
orderH1_Linf(2) = log(LinfH1_ord2/LinfH1_ord1)/log(ord1/ord2);
orderH1_Linf(3) = log(LinfH1_ord3/LinfH1_ord2)/log(ord2/ord3);
orderH1_Linf(4) = log(LinfH1_ord4/LinfH1_ord3)/log(ord3/ord4);

orderH1_L2
orderH1_Linf

% write code to print 


line1= strcat('\label{table:4}',...
 '\begin{tabular}{||c| c| c| c| c| c| c | c||}',...
' \hline ');
line2 = ' Mesh Ele & Deg &L2(Before) & L2(After SIAC) & order& Linf(Before) & Linf(After SIAC) & order \\ [0.5ex] ';
line3 = ' \hline\hline ';
% line 1. 
line4 = strcat('$',num2str(ord1),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2BH1_ord1,'%1.2e'), '$ & $',...
    num2str(L2H1_ord1,'%1.2e'), '$ & $',...
    num2str(orderH1_L2(1)), '$ & $',...
    num2str(LinfBH1_ord1,'%1.2e'), '$ & $',...
    num2str(LinfH1_ord1,'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(1)), '$ ',...
    '\\');
line5 = ' \hline ';
% line 2.
line6 = strcat('$',num2str(ord2),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2BH1_ord2,'%1.2e'), '$ & $',...
    num2str(L2H1_ord2,'%1.2e'), '$ & $',...
    num2str(orderH1_L2(2)), '$ & $',...
    num2str(LinfBH1_ord2,'%1.2e'), '$ & $',...
    num2str(LinfH1_ord2,'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(2)), '$ ',...
    '\\');
line7 = ' \hline ';

line8 = strcat('$',num2str(ord3),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2BH1_ord3,'%1.2e'), '$ & $',...
    num2str(L2H1_ord3,'%1.2e'), '$ & $',...
    num2str(orderH1_L2(3)), '$ & $',...
    num2str(LinfBH1_ord3,'%1.2e'), '$ & $',...
    num2str(LinfH1_ord3,'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(3)), '$ ',...
    '\\');
line9 = ' \hline ';

line10 = strcat('$',num2str(ord4),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2BH1_ord4,'%1.2e'), '$ & $',...
    num2str(L2H1_ord4,'%1.2e'), '$ & $',...
    num2str(orderH1_L2(4)), '$ & $',...
    num2str(LinfBH1_ord4,'%1.2e'), '$ & $',...
    num2str(LinfH1_ord4,'%1.2e'), '$ & $',...
    num2str(orderH1_Linf(4)), '$ ',...
    '\\');


line11= ' \hline \end{tabular} ';


fileID = fopen('P1_hexSymRot_quad_m_1_DG_Hex1.txt','w');
fwrite(fileID,line0);
fwrite(fileID,line1);
fwrite(fileID,line2);
fwrite(fileID,line3);
fwrite(fileID,line4);
fwrite(fileID,line5);
fwrite(fileID,line6);
fwrite(fileID,line7);
fwrite(fileID,line8);
fwrite(fileID,line9);
fwrite(fileID,line10);
fwrite(fileID,line11);
fclose(fileID);
