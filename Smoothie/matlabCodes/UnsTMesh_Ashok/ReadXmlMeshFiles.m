clear all;
close all;

%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 2;      % LineWidth
msz = 8;       % MarkerSize
atX = '5p8';


% figure (1);
% plotNekMesh('simpleMesh.xml');
% sm = 0.3; lm=0.7;
% line([sm,sm],[sm,lm],'lineWidth',2,'Color','r','LineStyle','-','LineWidth',lw); hold on;
% line([sm,lm],[sm,sm],'lineWidth',2,'Color','r','LineStyle','-','LineWidth',lw); hold on;
% line([lm,lm],[sm,lm],'lineWidth',2,'Color','r','LineStyle','-','LineWidth',lw); hold on;
% line([sm,lm],[lm,lm],'lineWidth',2,'Color','r','LineStyle','-','LineWidth',lw); hold on;
% axis off;
% axis equal;
% print(strcat('Images/','NPUTriMesh_Img'), '-dpng', '-r200');
% 
% 
% 
% figure(2);
% %plotNekMesh('NM2_NUTriMesh_05.xml');hold on;
% plotNekMesh('NUTriMesh_Bnd01.xml');hold on;
% %line([0,0],[0,1],'lineWidth',2)
% %xlim([0,1])
% %ylim([0,1])
% sm = 0.3; lm=0.7;
% line([sm,sm],[sm,lm],'lineWidth',2,'Color','r','LineStyle','-','LineWidth',lw); hold on;
% line([sm,lm],[sm,sm],'lineWidth',2,'Color','r','LineStyle','-','LineWidth',lw); hold on;
% line([lm,lm],[sm,lm],'lineWidth',2,'Color','r','LineStyle','-','LineWidth',lw); hold on;
% line([sm,lm],[lm,lm],'lineWidth',2,'Color','r','LineStyle','-','LineWidth',lw); hold on;
% axis off;
% axis equal;
% print(strcat('Images/','NUTriMesh_Img'), '-dpng', '-r200');


figure(3);
plotNekMesh('UTriMesh_Bnd01.xml');hold on;
%line([0,0],[0,1],'lineWidth',2)
%xlim([0,1])
%ylim([0,1])
sm = 0.3; lm=0.7;
% line([sm,sm],[sm,lm],'lineWidth',2,'Color','r','LineStyle','-','LineWidth',lw); hold on;
% line([sm,lm],[sm,sm],'lineWidth',2,'Color','r','LineStyle','-','LineWidth',lw); hold on;
% line([lm,lm],[sm,lm],'lineWidth',2,'Color','r','LineStyle','-','LineWidth',lw); hold on;
% line([sm,lm],[lm,lm],'lineWidth',2,'Color','r','LineStyle','-','LineWidth',lw); hold on;
axis off;
axis equal;
 print(strcat('Images/','UTriMesh_Img'), '-dpng', '-r200');