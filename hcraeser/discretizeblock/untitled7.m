% Input will be Number of subBlocks and axis line curve coeffecits.
% will always be divided into square blocks.
% Intialization
clear;
bd = 2;
sampleRate = 5;
sampleRateCurve = 21;
count =1;
curve_n =4;

% variables.
cbreak = linspace(-1,1,bd+1);
% curve data.
curveData = linspace(-1,1,sampleRateCurve);
[xcoeff,ycoeff,zcoeff] = pickSimpleCase(curve_n);
xdata = B_eval(xcoeff,curveData);
ydata = B_eval(ycoeff,curveData);
zdata = B_eval(zcoeff,curveData);
curvexy(:,1) = xdata; curvexy(:,2) = ydata; curvexy(:,3) = zdata;
% xderdata = B_der_eval(xcoeff,curveData);
% yderdata = B_der_eval(ycoeff,curveData);
% zderdata = B_der_eval(zcoeff,curveData);
figure(1);
for i = 1: bd
    x = linspace(-1,1,sampleRate);
    xs = (x+1)/bd+cbreak(i);
    for j = 1:bd
        y = linspace(-1,1,sampleRate);
        ys = (y+1)/bd+cbreak(j);
        for k = 1:bd
            z = linspace(-1,1,sampleRate);
            zs = (z+1)/bd+cbreak(k);
            
            mapxy(:,:) = changeGridToArray(xs,ys,zs);
            [xy,distance,t_a] = distance2curve(curvexy,mapxy,'spline');
            dataIn(i,j,k,:,:) = mapxy;
            
            x_NonCurve = B_der_eval(xcoeff,t_a);
            y_NonCurve = B_der_eval(ycoeff,t_a);
            z_NonCurve = B_der_eval(zcoeff,t_a);
            NonCurve = [x_NonCurve,y_NonCurve,z_NonCurve];
            
            VecCurveToPoint = mapxy - xy;
            %cVec = zeros(size(x_NonCurve));
            mapxyV = cross(NonCurve,VecCurveToPoint,2) + ([1./(distance+1),1./(distance+1),1./(distance+1)].*NonCurve);
            dataOut(i,j,k,:,:) = mapxyV;
            
            quiver3(mapxy(:,1),mapxy(:,2),mapxy(:,3),mapxyV(:,1),mapxyV(:,2),mapxyV(:,3));  hold on;          
            count = count +1;
        end
    end
end
