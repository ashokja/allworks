SurfaceDataCurves myLoadSufDataFile(String s)
{
  String[] lines = loadStrings(s);
  String line;
  myGNextLine =0;

  line = myGetNextLine(lines);
  int numCurves, kv, numCpts;
  String[] list = splitTokens(line," ");
  println(list);
  numCurves = Integer.parseInt(trim(list[0]));
  kv        = Integer.parseInt(trim(list[1]));
  numCpts   = Integer.parseInt(trim(list[2]));

  line = myGetNextLine(lines);
  ArrayList<Float> knotV = new ArrayList<Float> ();
  // hack it here.
  list = splitTokens(line,"=");
  if(1 != list.length )
  {
    line = list[1];
  }
  list = splitTokens(line, " ");
  for (int i=0; i < list.length; i++)
  {
    knotV.add(Float.parseFloat(trim(list[i])));
  }
  // Check if modified and change to modified if necessary.


  ArrayList<PVector> Cpts = new ArrayList<PVector> ();
  for (int i =0; i< numCurves*numCpts; i++)
  {
    PVector p = new PVector(0.0, 0.0, 1.0);
    line = myGetNextLine(lines);
    list = splitTokens(line, ", ");
    p.x = Float.parseFloat(trim(list[0]));
    p.y = Float.parseFloat(trim(list[1]));
    p.z = Float.parseFloat(trim(list[2]));
    Cpts.add(p);
  }
  
  SurfaceDataCurves sdc = new SurfaceDataCurves( kv, knotV, numCpts, Cpts, numCurves); 
  return sdc;
}


// Load surfaces.
SurfaceCurves myLoadSufFile(String s)
{
  // initialization
  String[] lines = loadStrings(s);
  String line;
  myGNextLine =0;
  println("got till here");
  // get ku,kv
  line = myGetNextLine(lines);
  String[] list = splitTokens(line, " ");
  print(list[0]+" a "+list[1]);
  int ku = Integer.parseInt(trim(list[0]));
  int kv = Integer.parseInt(trim(list[1]));

  // get tu, tv
  line = myGetNextLine(lines);
  list = splitTokens(line, " ");
  print(list[0]+" a "+list[1]);
  int tu = Integer.parseInt(trim(list[0]));
  int tv = Integer.parseInt(trim(list[1]));

  // Get Tu knot vector;
  ArrayList<Float> knotu = new ArrayList<Float>();
  line = myGetNextLine(lines);
  list = split(line, ' ');
  for (int i = 0; i < tu; i++)
  {
    float f = Float.parseFloat(trim(list[2+i]));
    knotu.add(f);
  }

  // Get Tv Knot Vector;
  ArrayList<Float> knotv = new ArrayList<Float>();
  line = myGetNextLine(lines);
  list = split(line, ' ');
  for (int i = 0; i < tv; i++)
  {
    float f = Float.parseFloat(trim(list[2+i]));
    knotv.add(f);
  }
  println("got till here");
  // Calculate number of control points in u and v direction.
  int numCpts_u = tu - ku-1;
  int numCpts_v = tv-kv-1;

  // loop through for all the porints.
  ArrayList<PVector> CPtsA = new ArrayList<PVector>();
  for (int i =0; i < numCpts_u*numCpts_v; i++)
  {
    line = myGetNextLine(lines);
    list = split(line, ',');
    float x = Float.parseFloat(trim(list[0])); 
    float y = Float.parseFloat(trim(list[1])); 
    float z = Float.parseFloat(trim(list[2]));
    PVector p = new PVector(x, y, z);
    CPtsA.add(p);
  }

  print("Ku : "+ku + "\t kv : " + kv);
  print("tu : "+ tu + "\t tv " + tv); 
  print(" knot U : "+ knotu);
  print(" knot v : "+ knotv);
  print("CptA : " + CPtsA);

  // If everything checks out add into the Surface Class.
  SurfaceCurves sc = new SurfaceCurves(ku, kv, tu, tv, knotu, knotv, CPtsA);
  return sc;
}


String[] myLoadDataFile(String s)
{
  String[] lines = loadStrings(s);
  String line; 
  boolean type2D = false;
  myGNextLine =0;
  line = myGetNextLine(lines);
  //Type and degree
  int Deg;
  String[] list = split(line, ',');
  if (list.length ==1) {
    print("2D curve"+ list[0]);
    Deg = Integer.parseInt(trim(list[0]));
    int CType =1;
    type2D = true;
  } else {
    print("3D curve"+ list[1]+"\n");
    Deg = Integer.parseInt(trim(list[1]));
    int CType=2;
    print(Deg+"\n");
    type2D = false;
  }

  line = myGetNextLine(lines);
  list = split(line, ',');
  print(list[0]);
  int NumCPt = Integer.parseInt(trim(list[0]));

  ArrayList<PVector> CPtA = new ArrayList<PVector>();
  for (int i =0; i < NumCPt; i++)
  {
    list = split(myGetNextLine(lines), ',');
    PVector p0 = new PVector(0.0, 0.0, 1.0);

    p0.x = Float.parseFloat(trim(list[0]));
    p0.y = Float.parseFloat(trim(list[1]));
    if (!type2D)
    {
      p0.z = Float.parseFloat(trim(list[2]));
    }
    CPtA.add(p0);
  }

  // Check if knot Vector is given.
  list = split(myGetNextLine(lines), ",");
  ArrayList<Float> KnotA = new ArrayList<Float> ();
  if (Integer.parseInt(trim(list[0])) == 1)
  {
    print ("\n Knot Vector given"+list[0]);
    list = split(myGetNextLine(lines), ',');
    for (int i =0; i < NumCPt+Deg+1; i++)
    {
      //      print ("\n Knot Vector given"+list[0]);
      float kv = Float.parseFloat(trim(list[i]));
      KnotA.add(kv);
    }
    KnotA.set(NumCPt+Deg, KnotA.get(NumCPt+Deg)+1);
    print("\n Knot Vector \n"+KnotA);
  } else {
    print("\n Knot Vector not given"+list[0]);
    float count =0;
    for (int i =0; i < NumCPt+Deg+1; i++)
    {
      if (i <= Deg || i >= NumCPt+1)
      {
      } else
      {
        count++;
      }
      float kv = count;
      KnotA.add(kv);
    }
    KnotA.set(NumCPt+Deg, KnotA.get(NumCPt+Deg)+1);
  }
  //print knot Vector;
  print("Knot Vector"+ KnotA+ "\n");
  print("Data "+ CPtA + "\n");
  print("Degree" + Deg+"\n");
  ArrayList<PVector> CPtFromData = dataToControlPoints(CPtA, KnotA, Deg);
  AddCurve(CPtFromData, KnotA, Deg);
  print("Ashok this "+Deg+" \n");
  print(CPtFromData+"\n");
  print(KnotA+"\n");
  return lines;
}

String[] myLoadFile(String s)
{
  String[] lines = loadStrings(s);
  String line; 
  boolean type2D = false;
  //Intialization of global variables.  
  myGNextLine =0;
  //The number of curve added(starts from 0)
  myGCurC = myGCurC + 1;

  line = myGetNextLine(lines);
  // Type and degree.
  int Deg;  
  String[] list = split(line, ',');
  if ( list.length ==1 ) {
    print("2D curve"+list[0]);
    Deg = Integer.parseInt(trim(list[0]));
    int CType = 1;
    Degs.add(Deg);
    CTypes.add(CType);
    type2D = true;
  } else {
    print("3D curve"+ list[1]+"\n");
    Deg = Integer.parseInt(trim(list[1]));
    int CType = 2;
    print(Deg+ "\n");
    Degs.add(Deg);
    CTypes.add(CType);
    type2D = false;
  }

  // Number of Points
  line = myGetNextLine(lines);
  list = split(line, ',');
  print(list[0]);
  int NumCPt = Integer.parseInt(trim(list[0]));
  NumCPts.add(NumCPt);

  ArrayList<PVector> CPtA = new ArrayList<PVector>();
  ArrayList<Draggable> D_CPtA = new ArrayList<Draggable> ();

  for (int i =0; i < NumCPt; i++)
  {
    list = split(myGetNextLine(lines), ',');
    PVector p0 = new PVector(0.0, 0.0, 1.0);

    p0.x = Float.parseFloat(trim(list[0]));
    p0.y = Float.parseFloat(trim(list[1]));
    if (!type2D)
    {
      p0.z = Float.parseFloat(trim(list[2]));
    }
    CPtA.add(p0);
    Draggable d = new Draggable(CToS(p0, 1), CToS(p0, 2), 10, 10);
    D_CPtA.add(d);
  }
  CPtAs.add(CPtA);
  D_CPtAs.add(D_CPtA);
  print("\n"+CPtA);

  // Check if knot Vector is given.
  list = split(myGetNextLine(lines), ",");
  ArrayList<Float> KnotA = new ArrayList<Float> ();
  if (Integer.parseInt(trim(list[0])) == 1)
  {
    print ("\n Knot Vector given"+list[0]);
    list = split(myGetNextLine(lines), ',');
    for (int i =0; i < NumCPt+Deg+1; i++)
    {
      //      print ("\n Knot Vector given"+list[0]);
      float kv = Float.parseFloat(trim(list[i]));
      KnotA.add(kv);
    }
    KnotA.set(NumCPt+Deg, KnotA.get(NumCPt+Deg)+1);
    print("\n Knot Vector \n"+KnotA);
  } else {
    print("\n Knot Vector not given"+list[0]);
    float count =0;
    for (int i =0; i < NumCPt+Deg+1; i++)
    {
      if (i <= Deg || i >= NumCPt+1)
      {
      } else
      {
        count++;
      }
      float kv = count;
      KnotA.add(kv);
    }
    KnotA.set(NumCPt+Deg, KnotA.get(NumCPt+Deg)+1);
  }
  KnotAs.add(KnotA);
  return lines;
}

String myGetNextLine(String[] lines)
{
  //check if myGNextLine < lines.size();
  if (lines.length <= myGNextLine)
    return null;
  // Find the next line which is not a comment.
  int b = 0;
  while (b == 0)
  {
    // if next line comment ignore and increase line number.
    if (trim(lines[myGNextLine]).equals("") || lines[myGNextLine].substring(0, 1).equals("#")  )
    {
      //       println("Comment line " + lines[myGNextLine] );
      myGNextLine++;
      continue;
    }
    myGNextLine++;
    b =1;
  }
  return lines[myGNextLine-1];
}

