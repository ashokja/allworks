#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include <math.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC1D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh1D.h"


using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

int main(int argc, char* argv[])
{
	NektarBaseClass k;
	if (argc != 6)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg conditions file." << endl;
		cout << "4rd arg meshscaling you want to use." << endl;
		//cout << "5th arg input filename to read fce from ." << endl;
		cout << "index of the id you want to debug from." << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh1D* HNM1D = new HandleNekMesh1D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();
    
    for(int i =0;i <var.size();i++)
    {
        HNM1D->LoadMesh(var[i]);
    }

    //HNM1D->LoadData(fname+".fld",var);
	
	int tNquadPts = HNM1D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

//	MetricTensor* metricT = new MetricTensor();
//	metricT->LoadMetricTensor(HNM1D);
	
	switch( HNM1D->m_expansions[0]->GetCoordim(0) )
	{
		case 1:
			HNM1D->m_expansions[0]->GetCoords(xc0);
			Vmath::Zero(tNquadPts,&xc1[0],1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 2:
			HNM1D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM1D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);

 //	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);


    float b = 7.0;
    float a = 3.5;
	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*M_PI*(xc0[i]));
		//fce[i] = erf(xc0[i]*b-a);
		//fce[i] = erf(xc0[i]);
	}

    //string inputfilname = (argv[5]);
	//k.readNekArray(fce, inputfilname);
  //  cout << "filename" << endl;
//    cout<< inputfilname << endl;

	HNM1D->m_expansions[0]->FwdTrans(fce, HNM1D->m_expansions[0]->UpdateCoeffs() );
	HNM1D->m_expansions[0]->BwdTrans( HNM1D->m_expansions[0]->GetCoeffs(),
										HNM1D->m_expansions[0]->UpdatePhys());
	 ece = HNM1D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM1D->m_expansions[0]->GetCoeffs();

	HNM1D->m_Arrays.push_back(ece);
//	HNM1D->LoadExpListIntoRTree();
	HNM1D->CalculateDynamicScaling();
// Evaluate on a new equal space grid mesh.
	SmoothieSIAC1D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM1D, atoi(argv[3]), atof(argv[4]) ); 
	SmoothieSIAC1D sm_NONSYM(SIACUtilities::eSYM_UNEVEN_2kp1, HNM1D, atoi(argv[1+2]), atof(argv[4]) );
	SmoothieSIAC1D sm_NONSYM_Der(SIACUtilities::eSYM_DER_UNEVEN_2kp1, HNM1D, atoi(argv[1+2]) , atof(argv[4]),1 );
	SmoothieSIAC1D sm_NONSYM_Der2(SIACUtilities::eSYM_DER_UNEVEN_2kp1, HNM1D, atoi(argv[1+2]) , atof(argv[4]),2 );
	SmoothieSIAC1D sm_NONSYM_Der3(SIACUtilities::eSYM_DER_UNEVEN_2kp1, HNM1D, atoi(argv[1+2]) , atof(argv[4]),3 );

    Array<OneD,NekDouble> pX(tNquadPts),pY(tNquadPts),pE(tNquadPts),pP_p(tNquadPts),pV_p(tNquadPts),coord(3,0.0);
    Array<OneD,NekDouble> pS(tNquadPts), pSDyn(tNquadPts), pSNUK(tNquadPts),pDyn(tNquadPts),pSNUKDER(tNquadPts),pSNUKDER2(tNquadPts),pSNUKDER3(tNquadPts);
    Array<OneD,NekDouble> pSY(tNquadPts), pSDynY(tNquadPts), pSNUKY(tNquadPts),pSNUK_Eig1(tNquadPts),pSNUK_Eig2(tNquadPts);
    NekDouble valY,valZ,lambda,lambda2;
    Array<OneD,NekDouble> direction(3,0.0),directionY(3,0.0),eig(3,0.0),eig2(3,0.0);
    direction[0] = 1.0;
    directionY[1] = 1.0;

    //for(int elId = atoi(argv[5]); elId < atoi(argv[5])+1; elId++)
    for(int elId = 0; elId < HNM1D->m_expansions[0]->GetExpSize(); elId++)
    {
        int physOffset = HNM1D->m_expansions[0]->GetPhys_Offset(elId);
        const Array<OneD,NekDouble> el_u_phys = ece.CreateWithOffset(ece,physOffset);

        LocalRegions::ExpansionSharedPtr lexp = HNM1D->m_expansions[0]->GetExp(elId);
        
        int numPtsEl = HNM1D->m_expansions[0]->GetExp(elId)->GetTotPoints();
        for(int i=0 ; i< numPtsEl; i++)
        {
            int index = physOffset+i;
            pX[index] = xc0[index]; 
            pY[index] = xc1[index];
            pE[index] = elId;
            coord[0] = xc0[index]; 
            coord[1] = xc1[index]; 
            pP_p[index] = lexp->PhysEvaluate(coord,el_u_phys);
            pV_p[index] = std::cos(2.0*M_PI*(xc0[index]));
		    //fce[i] = erf(xc0[i]*b-a);
            //pV_p[index] = erf(xc0[index]*b-a);
            //pV_p[index] = erf(xc0[index]);
            //pS[index] = 
            //pSDyn[index] = 
            //pSNUK[index] = 
			NekDouble dScaling = HNM1D->GetDynamicScaling(coord,elId,1.0);
			pDyn[index] = dScaling;
			sm.EvaluateAt(pX[index],pY[index],0.0,pS[index],valY,valZ,direction, atof(argv[4]) ,0);
			sm.EvaluateAt(pX[index],pY[index],0.0,pSDyn[index],valY,valZ,direction, dScaling ,0);
		
            sm_NONSYM.EvaluateAt_NUK_MetricTensor(pX[index],pY[index],0.0,pSNUK[index], 
																valY, valZ, direction, dScaling,0);

            sm_NONSYM_Der.EvaluateAt_NUK_MetricTensor(pX[index],pY[index],0.0,pSNUKDER[index], 
																valY, valZ, direction, dScaling,0);
            if ( atoi(argv[3])==3 )
            {
                sm_NONSYM_Der2.EvaluateAt_NUK_MetricTensor(pX[index],pY[index],0.0,pSNUKDER2[index], 
																valY, valZ, direction, dScaling,0);
            }
            if ( atoi(argv[3])==4 )
            {
                sm_NONSYM_Der2.EvaluateAt_NUK_MetricTensor(pX[index],pY[index],0.0,pSNUKDER2[index], 
																valY, valZ, direction, dScaling,0);
                sm_NONSYM_Der3.EvaluateAt_NUK_MetricTensor(pX[index],pY[index],0.0,pSNUKDER3[index], 
																valY, valZ, direction, dScaling,0);
            }
            
            /*
            cout << "ActualValues:" <<endl;
            cout <<"pX \t" << pX[index] << endl;
            cout <<"pY \t" << pY[index] << endl;
            cout <<"pE \t" << pE[index] << endl;
            cout <<"proj  \t" << pP_p[index] << endl;
            cout <<"value \t" << pV_p[index] << endl;
            cout <<"Lsiac \t" << pS[index] << endl;
            cout <<"ADP  \t" << pSDyn[index] << endl;
            cout << "Errors:" <<endl;
            cout <<"proj  \t" <<pV_p[index]-pP_p[index] << endl;
            cout <<"Lsiac \t" << pV_p[index]-pS[index] << endl;
            cout <<"ADP  \t" << pV_p[index]-pSDyn[index] << endl;
            */
            cout << "index\t" << index << endl; 
            cout <<"NUK  \t" << pSNUK[index] << endl;
            cout <<"NUK Err \t" << pV_p[index]-pSNUK[index] << endl;
        }
    }
	
    k.writeNekArray(xc0,fname+"_xc0_EL.txt");
	k.writeNekArray(xc1,fname+"_xc1_EL.txt");
	k.writeNekArray(fce,fname+"_fce_EL.txt");
	k.writeNekArray(ece,fname+"_ece_EL.txt");
	
    k.writeNekArray(pX,fname+"_pX_EL.txt");
	k.writeNekArray(pY,fname+"_pY_EL.txt");
	k.writeNekArray(pE,fname+"_pE_EL.txt");
	k.writeNekArray(pP_p,fname+"_pP_p_EL.txt");
	k.writeNekArray(pV_p,fname+"_pV_p_EL.txt");
    k.writeNekArray(pS,fname+"_pS_EL.txt");
    k.writeNekArray(pSDyn,fname+"_pSDyn_EL.txt");
    k.writeNekArray(pSNUKDER,fname+"_pSNUKDER_EL.txt");
    k.writeNekArray(pSNUKDER2,fname+"_pSNUKDER2_EL.txt");
    k.writeNekArray(pSNUKDER3,fname+"_pSNUKDER3_EL.txt");
    k.writeNekArray(pSNUK,fname+"_pSNUK_EL.txt");

/*
	 string out = vSession->GetSessionName() + ".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (HNM1D->m_expansions[0]->GetFieldDefinitions()).at(0);
    FieldDef[0]->m_fields.push_back("u");
    FieldDef[0]->m_fields.push_back("v");
    FieldDef[0]->m_fields.push_back("w");
    HNM1D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM1D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM1D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM1D->m_fld->Write(out, FieldDef, FieldData);
*/
	return 0;
}


