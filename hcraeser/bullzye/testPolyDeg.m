function [U,V,W] = testPolyDeg(x,y,z,Deg)
    switch nargin
        case 0,    
            x = (-1:.1:1);
            y = (-1:.1:1);
            z = (-1:.1:1);
            Deg = 3;
    end


    [X,Y,Z] = meshgrid(x,y,z);
    X = permute(X,[2,1,3]); Y = permute(Y,[2,1,3]); Z = permute(Z,[2,1,3]);

    U = zeros(size(X));V = zeros(size(Y)); W = zeros(size(Z));
    %simple summation
    for d = 1:Deg
        U = U+ (X+1).*sin(X)+(Y+1).*sin(Y)+(Z+1).*sin(Z) ;
        V = V+ Y.^(d);
        W = W+ Z.^(d);
    end

%     figure(1)
%     VizIsoSurface(x,y,z,U,V,W); hold on;
    
    
%     [sx, sy, sz] = meshgrid([0],[-1],[-1:0.05:1]);
%     %figure(2)
%     %VizStreamLines(x,y,z,U,V,W,1.73,1,0); hold on;
%     %VizStreamLines(x,y,z,U,V,W,1.75,1,0); hold on;
%     
%     VizStreamLines(xold,yold,zold,U,V,W,sx,sy,sz); hold on;




end












% % test for rothWorks
% clear;
% addpath ../createTestData/
% 
% t = (-10:.7:10);
% [X,Y,Z] = meshgrid(t,t,t);
% 
% data(:,1) = X(:);
% data(:,2) = Y(:);
% data(:,3) = Z(:);
% wa = 2; Ga = 7; R = 50;
% s_d = max(size(data));
% 
% for i = 1:s_d
%     pos = data(i,:);
%     x = pos(1); y = pos(2); z = pos(3);
%     r = sqrt(x^2+y^2)+0.5;
%     u = -Ga*y/r - wa*z*x/r^2;
%     v = Ga*x/r - wa*z*y/r^2;
%     w = wa*(1-R/r);
%     dataOut(i,:) = [u,v,w];
% end
% figure(1)
% quiver3(data(:,1),data(:,2),data(:,3),dataOut(:,1),dataOut(:,2),dataOut(:,3));
% view(-90,0);
% figure(2)
% drawStreamLine(data,dataOut,ones(6,3));
% view(-90,0);
% colorbar;

