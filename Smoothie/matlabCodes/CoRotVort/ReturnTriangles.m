function [Tri] =  ReturnTriangles(elpos,id)
    
    if nargin ==0
        elpos = linspace(1,5,100);
        elpos = floor(elpos);
        id = 3;
    end

    s = sqrt( max(size(elpos)));
    %enlpos = reshape(elpos,[s s]);

    elInd = find(elpos == id);
    
    [Iind, Jind] = ind2sub([s,s],elInd);
    %disp([elInd, Iind,Jind]);
    
    count =0;
    Tri = [];
    for e = 1:max(size(elInd))
        eid = elInd(e);
        ie = Iind(e);
        je = Jind(e);
        d =0 ; c =0; r=0;
        for ee = 1:max(size(elInd))
            if( ie+1 == Iind(ee) && je+1 == Jind(ee) )
                c=1;
            end
            if( ie+1 == Iind(ee) && je == Jind(ee) )
                d=1;
            end
            if( ie == Iind(ee) && je+1 == Jind(ee) )
                r=1;
            end
        end
        if ( c ==1 && d==1 )
            count= count+1;
            Tri(count,:) =  [eid sub2ind([s s],ie+1,je) sub2ind([s s],ie+1,je+1) ] ;
        end
        if (c ==1 && r==1 )
            count = count+1;
            Tri(count,:) =  [eid sub2ind([s s],ie,je+1) sub2ind([s s],ie+1,je+1) ] ;
        end
    end
    
%    elpos(Tri(:))
end
