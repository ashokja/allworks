function [rest] = B_der_eval(coeff,dataIn)
    rest = zeros(size(dataIn));
    n = max(size(coeff))-1;
    for k = 0:n
        rest = rest  + coeff(k+1)*B_der(n,k,dataIn);
    end 
end 
