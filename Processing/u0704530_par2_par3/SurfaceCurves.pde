class SurfaceCurves  //<>//
{
  int kv; 
  int ku;
  int Tv; 
  int Tu;
  int numCptsu; 
  int numCptsv;
  ArrayList<PVector> CptsA;
  ArrayList<Float> Knotu;
  ArrayList<Float> Knotv;

  SurfaceCurves(int tku, int tkv, int tTu, int tTv, ArrayList<Float> tKnotu, ArrayList<Float> tKnotv, ArrayList<PVector> tCptsA)
  {
    kv = tkv; 
    ku = tku; 
    Knotu = tKnotu; 
    Knotv = tKnotv; 
    CptsA = tCptsA;
    Tu = tTu; 
    Tv = tTv;
    numCptsu = Tu-ku-1;
    numCptsv = Tv-kv-1;
  } 

  ArrayList<PVector> GetRowCpts(int row)
  {
    ArrayList<PVector> pvec = new ArrayList<PVector> ();
    int numCptsv = Tv-kv-1;

    int startindex = row*numCptsv;
    for (int i = 0; i < numCptsv; i++)
    {
      pvec.add(CptsA.get(startindex+i));
    }
    //print(pvec);
    return pvec;
  } 

  ArrayList<PVector> GetColCpts(int col)
  {
    ArrayList<PVector> pvec = new ArrayList<PVector> ();
    int numCptsv = Tv-kv-1;
    int numCptsu = Tu-ku-1;
    for (int i = 0; i < numCptsu; i++)
    {
      pvec.add(CptsA.get( i*numCptsv+col));
    }
    //print(pvec);
    return pvec;
  }

  void myDrawLines(ArrayList<PVector> points)
  {
    //    println(myGLinesVisible);
    for (int i =1; i < points.size (); i++)
    {
      PVector p1, p2;
      p1 = points.get(i-1);
      p2 = points.get(i);
      //line(CToS(p1.x,1),CToS(p1.y,2),CToS(p2.x,1),CToS(p2.y,2));
      line(CToS(p1, 1), CToS(p1, 2), CToS(p2, 1), CToS(p2, 2));
    }
  }

  void DrawControlMesh()
  {
    int numCptsv = Tv-kv-1;
    int numCptsu = Tu-ku-1;
    //stroke(158);

    for (int i =0; i<numCptsu; i++)
    {
      ArrayList<PVector> pv = GetRowCpts(i);
      myDrawLines(pv);
      //DrawCurve(Knotv, pv, Tv-kv-1, kv);
    } 
    //stroke(158);
    for (int i =0; i<numCptsv; i++)
    {
      ArrayList<PVector> pu = GetColCpts(i);
      myDrawLines(pu);
      //DrawCurve(Knotu, pu, Tu-ku-1, ku);
      //DrawCurve(knotv,pu,Tu-ku-1, ku);
    }
  }

    void DrawSurKnotMesh()
  {
    //stroke(0,0,128);
    // defined in class so no need to get it.
    // curves in u direction.
    ArrayList<PVector> pf = new ArrayList<PVector>();
    ArrayList<PVector> ps;// = new ArrayList<PVector>();

    //DrawCurve( Knotv, GetRowCpts(0), Tv-kv-1, kv); 
    //int i =1;
    
    float patTknot = Knotv.get(kv);
    // for each row or Vertical curves.
    float start = Knotv.get(kv);
    float end = Knotv.get(Tv-kv-1);
    //stroke(0,245,0);
    for(int pat =kv; pat <= Tv-kv-1; pat++){
      patTknot = Knotv.get(pat); //start +(end-start)/((float)(Tv-kv-2))*pat;
      if (pat == Tv-kv-1)
      {
        patTknot -= 0.001;
      }
      ArrayList<PVector> pu = new ArrayList<PVector>();
      for ( int r =0; r< Tu-ku-1; r++)
      {
        ArrayList<PVector> pr = GetRowCpts(r);
        //println(pr);
        pu.add(CurveEvalATt(kv, Knotv, pr, patTknot, -1));
      }   
      
      DrawCurve( Knotu,pu,Tu-ku-1,ku);
    }
    
    // for each col or horizontal curves.
    start = Knotu.get(ku);
    end = Knotu.get(Tu-ku-1);
    //stroke(0,245,0);
    for(int pat = ku; pat <= Tu-ku-1; pat++){
      patTknot = Knotu.get(pat);//start +(end-start)/((float)(Tu-ku-2))*pat;
           
      if (pat == Tu-ku-1)
      {
        patTknot -= 0.01;
      }
      ArrayList<PVector> pv = new ArrayList<PVector>();
      for ( int r =0; r< Tv-kv-1; r++)
      {
        ArrayList<PVector> pr = GetColCpts(r);
        //println(pr);
        pv.add(CurveEvalATt(ku, Knotu, pr, patTknot, -1));
      }   
      DrawCurve( Knotv,pv,Tv-kv-1,kv);
    }
   
//    println(Knotu);
  //  println(Knotv);
 }


  void DrawSurMesh()
  {
    //stroke(0,0,128);
    // defined in class so no need to get it.
    // curves in u direction.
    ArrayList<PVector> pf = new ArrayList<PVector>();
    ArrayList<PVector> ps;// = new ArrayList<PVector>();

    //DrawCurve( Knotv, GetRowCpts(0), Tv-kv-1, kv); 
    //int i =1;
    
    float patTknot = Knotv.get(kv);
    // for each row or Vertical curves.
    float start = Knotv.get(kv);
    float end = Knotv.get(Tv-kv-1);
    //stroke(0,245,0);
    for(int pat =0; pat <= Tv-kv-2; pat++){
      patTknot = start +(end-start)/((float)(Tv-kv-2))*pat;
      if (pat == Tv-kv-2)
      {
        patTknot -= 0.001;
      }
      ArrayList<PVector> pu = new ArrayList<PVector>();
      for ( int r =0; r< Tu-ku-1; r++)
      {
        ArrayList<PVector> pr = GetRowCpts(r);
        //println(pr);
        pu.add(CurveEvalATt(kv, Knotv, pr, patTknot, -1));
      }   
      
      DrawCurve( Knotu,pu,Tu-ku-1,ku);
    }
    
    // for each col or horizontal curves.
    start = Knotu.get(ku);
    end = Knotu.get(Tu-ku-1);
    //stroke(0,245,0);
    for(int pat =0; pat <= Tu-ku-2; pat++){
      patTknot = start +(end-start)/((float)(Tu-ku-2))*pat;
           
      if (pat == Tu-ku-2)
      {
        patTknot -= 0.01;
      }
      ArrayList<PVector> pv = new ArrayList<PVector>();
      for ( int r =0; r< Tv-kv-1; r++)
      {
        ArrayList<PVector> pr = GetColCpts(r);
        //println(pr);
        pv.add(CurveEvalATt(ku, Knotu, pr, patTknot, -1));
      }   
      DrawCurve( Knotv,pv,Tv-kv-1,kv);
    }
   
//    println(Knotu);
  //  println(Knotv);
  }


  void DrawIsoSurMesh(int udir, int vdir)
  {

    // defined in class so no need to get it.
    // curves in u direction.
    ArrayList<PVector> pf = new ArrayList<PVector>();
    ArrayList<PVector> ps;// = new ArrayList<PVector>();

    //DrawCurve( Knotv, GetRowCpts(0), Tv-kv-1, kv); 
    //int i =1;
    float patTknot = Knotv.get(kv);
    // for each row or Vertical curves.
    float start = Knotv.get(kv);
    float end = Knotv.get(Tv-kv-1);
    //stroke(0,200,0);
    for(int pat =1; pat < udir; pat++){
      patTknot = start +(end-start)/((float)(udir))*pat;
      
      ArrayList<PVector> pu = new ArrayList<PVector>();
      for ( int r =0; r< Tu-ku-1; r++)
      {
        ArrayList<PVector> pr = GetRowCpts(r);
        pu.add(CurveEvalATt(kv, Knotv, pr, patTknot, -1));
      }   
      //stroke(0,0,255);

      DrawCurve( Knotu,pu,Tu-ku-1,ku);
    }
    
    //stroke(0,200,0);
    // for each col or horizontal curves.
    start = Knotu.get(ku);
    end = Knotu.get(Tu-ku-1);
    for(int pat =1; pat < vdir; pat++){
      patTknot = start +(end-start)/((float)(vdir))*pat;
      
      ArrayList<PVector> pv = new ArrayList<PVector>();
      for ( int r =0; r< Tv-kv-1; r++)
      {
        ArrayList<PVector> pr = GetColCpts(r);
        pv.add(CurveEvalATt(ku, Knotu, pr, patTknot, -1));
      }   
//      //stroke(0,0,255);
      DrawCurve( Knotv,pv,Tv-kv-1,kv);
    }
    
//    println(Knotu);
  //  println(Knotv);
  }
  
  
  // nodal
  void DrawSurNodalMesh()
  {
    //stroke(0,0,128);
    // defined in class so no need to get it.
    // curves in u direction.
    ArrayList<PVector> pf = new ArrayList<PVector>();
    ArrayList<PVector> ps;// = new ArrayList<PVector>();

    //DrawCurve( Knotv, GetRowCpts(0), Tv-kv-1, kv); 
    //int i =1;
    
    float patTknot; // = Knotv.get(kv);
    // for each row or Vertical curves.
    float start = Knotv.get(kv);
    float end = Knotv.get(Tv-kv-1);
    //stroke(0,245,0);
    for(int pat =0; pat < Tv-kv-1; pat++){
      patTknot =0;
      for( int d = 1; d <= kv; d++)
      {
        patTknot += Knotv.get(pat+d);
      }
      patTknot = patTknot/((float)kv);
      if(patTknot < Knotv.get(kv) || patTknot >= Knotv.get(Tv-kv-1) )
      {
        continue;
      }
      ArrayList<PVector> pu = new ArrayList<PVector>();
      for ( int r =0; r< Tu-ku-1; r++)
      {
        ArrayList<PVector> pr = GetRowCpts(r);
        //println(pr);
        pu.add(CurveEvalATt(kv, Knotv, pr, patTknot, -1));
      }   
      
      DrawCurve( Knotu,pu,Tu-ku-1,ku);
    }
    
    // for each col or horizontal curves.
    start = Knotu.get(ku);
    end = Knotu.get(Tu-ku-1);
    //stroke(0,245,0);
    for(int pat =0; pat <= Tu-ku-2; pat++){
      patTknot =0;
      for( int d = 1; d <= ku; d++)
      {
        patTknot += Knotu.get(pat+d);
      }
      patTknot = patTknot/((float)ku);
      if(patTknot < Knotu.get(ku) || patTknot >= Knotu.get(Tu-ku-1) )
      {
        continue;
      }
      ArrayList<PVector> pv = new ArrayList<PVector>();
      for ( int r =0; r< Tv-kv-1; r++)
      {
        ArrayList<PVector> pr = GetColCpts(r);
        //println(pr);
        pv.add(CurveEvalATt(ku, Knotu, pr, patTknot, -1));
      }   
      DrawCurve( Knotv,pv,Tv-kv-1,kv);
    }
   
//    println(Knotu);
  //  println(Knotv);
  }

  
  
  
}


//Input: Takes a surface object.
// Output : Returns nothing, but builds a surface and draws it.
void DrawCurvesFromSurface(SurfaceCurves sc)
{
  // Get all info you need from sc.

  // Pick a direction. lets say u =1 and v= 2 and picked dir = u =1 at;
  //   For all domain points in v direction. save the control points a PVector Array.
  int kv = sc.kv;
  ArrayList<Float> knotv = sc.Knotv; 
  ArrayList<PVector> Cpts = new ArrayList<PVector>();  
  // Pick approp knot vectors. t_u = c 

  // Find coffecients fo rv.
}




// Input Cpts, KnotVector, Deg, t
// Output PVector at that t
PVector CurveEvalATt(int Deg, ArrayList<Float> KnotA, ArrayList<PVector> CPtA, float t, int JJ)
{
  // Find J
  int J = -1; 
  int I;
  for ( int i =0; i < KnotA.size (); i++)
  {
    if (t < KnotA.get(i))
    {
      J = i -1;
      break;
    }
  }
  //print ("\n Ashok Look at J = "+J +"JJ is "+JJ+"\n");
  PVector[] p_old = new PVector[Deg+1];
  PVector[] p_new = new PVector[Deg+1];
  PVector[] p_mid ;//= new PVector[Deg+1];
  for (int i =0; i <Deg+1; i++)
  {
    p_old[i] = new PVector(0.0, 0.0, 1.0);
    p_new[i] = new PVector(0.0, 0.0, 1.0);
  }
  for (int i=0; i<=Deg; i++)
  {
    PVector p = CPtA.get(i+J-Deg); 
    p_old[i].x = p.x; 
    p_old[i].y = p.y;
    p_old[i].z = p.z;
  } 
  //print("\n\n"+t+" ");
  for (int j =1; j<= Deg; j++)
  {
    for ( int i =J-Deg+j; i<= J; i++)
    {
      I = i-J+Deg;
      float ul = t- KnotA.get(i);
      float b = KnotA.get(i+Deg+1-j) - KnotA.get(i);
      float ur = b-ul;

      p_new[I].x = p_old[I].x*ul/b + p_old[I-1].x*ur/b;
      p_new[I].y = p_old[I].y*ul/b + p_old[I-1].y*ur/b;
      p_new[I].z = p_old[I].z*ul/b + p_old[I-1].z*ur/b;
      //print("\n J = "+J+" i="+i+" I ="+I +" ul="+ul+" b"+ b+" ur="+ur);
    }
    //print("\np_old"+p_old[0]+"\t"+p_old[1]+"\t"+p_old[2]);
    //print("\np_new"+p_new[0]+"\t"+p_new[1]+"\t"+p_new[2]);               
    p_mid = p_old;
    p_old = p_new;
    p_new = p_mid;
  } 
  //  point(CToS(p_old[Deg],1) , CToS(p_old[Deg],2) ); 
  PVector p = new PVector(p_old[Deg].x, p_old[Deg].y, p_old[Deg].z ); 
  return p;
}
