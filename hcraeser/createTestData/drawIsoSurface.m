function [] = drawIsoSurface(mapxy,mapxyV,coeff,isoValue)
    switch nargin
        case 4,
            sampledata =6;
            curveSampledata = 20;
    end
    data = linspace(-1,1,sampledata);
    cdata = linspace(-1,1,curveSampledata);
    [a,~] = size(mapxy);
    b = int64(a^(1/3));
    x = reshape(mapxy(:,1),b,b,b);
    y = reshape(mapxy(:,2),b,b,b);
    z = reshape(mapxy(:,3),b,b,b);
    u = reshape(mapxyV(:,1),b,b,b);
    v = reshape(mapxyV(:,2),b,b,b);
    w = reshape(mapxyV(:,3),b,b,b);
    xmin = min(x(:)); xmax = max(x(:));
    ymin = min(y(:)); ymax = max(y(:));
    zmin = min(z(:)); zmax = max(z(:));
    
    wind_speed = sqrt(u.^2 + v.^2 + w.^2);
    if ( (max(wind_speed(:))- min(wind_speed(:))) < 10^(-2))
        wind_speed = wind_speed + rand(size(wind_speed))*10^(-3);
    end
%    wind_speed = wind_speed <isoValue;
    p = patch(isosurface(x,y,z,wind_speed,isoValue));
isonormals(x,y,z,wind_speed,p)
p.FaceColor = 'red';
p.EdgeColor = 'none';
%'FaceAlpha',.3
p.FaceAlpha = .3;
daspect([1,1,1])
view(3); %axis tight
camlight 
lighting gouraud 
hold on;
    
    %plotting the curve
    xdata = B_eval(coeff(1,:),data);    cxdata = B_eval(coeff(1,:),cdata); 
    ydata = B_eval(coeff(2,:),data);    cydata = B_eval(coeff(2,:),cdata);
    zdata = B_eval(coeff(3,:),data);    czdata = B_eval(coeff(3,:),cdata);
    xderdata = B_der_eval(coeff(1,:),data);
    yderdata = B_der_eval(coeff(2,:),data);
    zderdata = B_der_eval(coeff(3,:),data);
    
    plot3(cxdata,cydata,czdata,'-r*'); hold on;
    plotBoundingBox();
    xlabel('x'); ylabel('y'); zlabel('z');
    
    hold on;

end