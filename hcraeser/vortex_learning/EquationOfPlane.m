function [nor,d] = EquationOfPlane(coords);

% each row is point(x,y,z).

v1 = coords(1,:) - coords(2,:);
v2 = coords(1,:) - coords(3,:);

nor = cross(v1,v2);
d = dot(nor,coords(1,:));


