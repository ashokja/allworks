function er = L2(v)
    er = sqrt(sum(v(:).*v(:))/max(size(v(:))));
end