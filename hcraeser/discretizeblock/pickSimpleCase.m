function [xcoeff,ycoeff,zcoeff] = pickSimpleCase(n)
    xcoeff = rand(1,n+1)*2-1;
    ycoeff = (-1:2/n:1);
    zcoeff = rand(1,n+1)*2-1;
end