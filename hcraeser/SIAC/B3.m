function v = B3(x)
    v = zeros(size(x));
    
    %I = find(abs(x)<=.5);% & abs(x)>=1.5);
    %v(I) = (4.*abs(x(I)).^2-12.*abs(x(I))+9)./8;
    
    I = find(abs(x)<=.5);
    v(I) = (6-8.*abs(x(I)).^2)./8;
    
    I = find(abs(x)>=.5 & abs(x)<=1.5);
    v(I) = (4.*abs(x(I)).^2-12.*abs(x(I)) + 9)./8;
return