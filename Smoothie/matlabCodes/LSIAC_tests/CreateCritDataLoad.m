% Read xc0 and xc1 from file.
% Write u and v to file.

clear;
close all;
%field one;
a1 = 0.74+0.35i;
a2 = 0.68-0.59i;
a3 =-0.11-0.72i;
b1 =-0.58+0.64i;
b2 = 0.51-0.27i;
b3 =-0.12+0.84i;

%  %field two;
% a1 = 0.74+0.35i;
% a2 =-0.18-0.19i;
% a3 =-0.11-0.72i;
% b1 =-0.58+0.64i;
% b2 = 0.51-0.27i;
% %b3 =-0.12+0.84i;


%rate = 3;
%samplerate = 6;
%r = [2,3,4];
r = [2];
%samples= [6,11,21,41,81,161];
samples= [41];
for rate = r
    for samplerate = samples
        filename = strcat('P',int2str(rate),'_quad_simple_',int2str(samplerate));

        xfname = strcat('../data/NekMeshes/',filename,'_xc0','.txt' );
        yfname = strcat('../data/NekMeshes/',filename,'_xc1','.txt' );

        xV = textread(xfname, '%f\t');
        yV = textread(yfname, '%f\t');

        xV = (2.0*xV-1).*2;
        yV = (2.0*yV-1).*2;

        zV = xV+yV*1i;
        zVb = xV-yV*1i;


        Val = (zV-a1).*(zV-a2).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2;
        % Val = (zV-a1).*(zVb-a2).*(zV-a3).*(zVb-b1).*(zVb-b2);

        u = real(Val); 
        v = -1*imag(Val);


        dlmwrite(strcat('../data/NekMeshes/',filename,'_u.txt'),u','delimiter','\t','precision',25);
        dlmwrite(strcat('../data/NekMeshes/',filename,'_v.txt'),v','delimiter','\t','precision',25);
    end
end
%%
% n_uv = sqrt(u.*u+v.*v);
% u = u./n_uv;
% v = v./n_uv;
% quiver(xV,yV,u,v,0.50);







%plot3(xV,yV,u);

