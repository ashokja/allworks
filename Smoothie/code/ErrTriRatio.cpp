#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include <MultiRegions/DisContField2D.h>
#include "HandleNekMesh2D.h"
#include <boost/timer.hpp>

using namespace SIACUtilities;
using namespace std;

 
int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg is conditions file." << endl;
		cout << "3nd arg polynomial degree filter you want to apply" << endl;
		cout << "4th LSIAC 1 and 2 A-LSIAC" << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM2D->LoadMesh(var[0]);
	string fname = vSession->GetSessionName();

	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);
	cout << "fc:" << tNquadPts<< endl;

	SpatialDomains::MeshGraphSharedPtr graph2D =
             SpatialDomains::MeshGraph::Read(vSession);
	graph2D->SetExpansionsToPolyOrder( 2*(atoi(argv[1+2])-1)+1);
	MultiRegions::ExpListSharedPtr Exp_u = MemoryManager<MultiRegions::DisContField2D>
 			::AllocateSharedPtr(vSession,graph2D,vSession->GetVariable(0));
	int New_tNquadPts = Exp_u->GetTotPoints();

	HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
	Vmath::Zero(tNquadPts,&xc2[0],1);	
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sceS1,ece;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sceS1 = Array<OneD,NekDouble>(tNquadPts);

//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);

	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]));
	}
	
	HNM2D->CalculateDynamicScaling();
	
	HNM2D->m_expansions[0]->FwdTrans(fce,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	 ece = HNM2D->m_expansions[0]->GetPhys();
	HNM2D->m_Arrays.push_back(ece);
	
	HNM2D->LoadExpListIntoRTree();

	//SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[1+2]), atof(argv[1+3]) ); 
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();
	cout << "scaling " << scaling << endl;
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1, HNM2D, atoi(argv[1+2]), scaling ); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
    cout << HNM2D->m_expansions.size() << endl;

	Array<OneD,NekDouble> direction(3,0.0);
	direction[0] = 1.0;
	//NekDouble scaling = atof(argv[1+3]);
//	Find a list of elements in the interested region.
//	Loop through all quadrature points to evaluate LSIAC
	vector<int> elIds,glIds;
	HNM2D->IntersectWithBoxUsingRTree( 0.0, 0.0, -0.001,
									   1.0, 1.0, 0.001, elIds,glIds);
	Array<OneD,NekDouble> lcoord(3,0.0),glCoordTemp(3,0.0);
	Array<OneD,NekDouble> SceN(New_tNquadPts,0.0), xcN0(New_tNquadPts), xcN1(New_tNquadPts), SceDN(New_tNquadPts,0.0),fceN(New_tNquadPts,0.0), eceN(New_tNquadPts,0.0);
	Exp_u->GetCoords(xcN0,xcN1);
	
//	for ( NekDouble mu = 1.0; mu < 1.26; mu=mu+0.25)
//	{
	NekDouble t1,t2,t3;
	NekDouble mu = 1.0;	
	// Need to time each simulation.
	// 	Projection calculation with timing
	boost::timer tim1;
	for( int i=0; i< elIds.size(); i++)
	{
		int elmID = elIds[i];
		int physOffsetN = Exp_u->GetPhys_Offset(elmID);
		LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp( elmID);
		Array<OneD,NekDouble> u_phys_old = ece.CreateWithOffset(ece,HNM2D->m_expansions[0]->GetPhys_Offset(elmID));
		for(int j=0; j< Exp_u->GetExp(elmID)->GetTotPoints();j++)
		{
			int index = j+physOffsetN;
			// constant scaling
			//sm.EvaluateAt(xcN0[index],xcN1[index],0.0,SceN[index],valY,valZ,direction, mu*scaling,0);
			// dynamic scaling
			glCoordTemp[0] = xcN0[index]; glCoordTemp[1] = xcN1[index];
			//sm.EvaluateAt(xcN0[index],xcN1[index],0.0,SceDN[index],valY,valZ,direction, HNM2D->GetDynamicScaling(glCoordTemp,elmID,mu),0);
			// actual function
			fceN[index] = std::cos(2.0*M_PI*(xcN0[index]+xcN1[index]));
			eceN[index] = lexp->PhysEvaluate(glCoordTemp, u_phys_old);
		}
	}
	t1 = tim1.elapsed();
if(atoi(argv[4])==1)
{
	// timer for L-SIAC calculation.
	boost::timer tim2;
	for( int i=0; i< elIds.size(); i++)
	{
		int elmID = elIds[i];
		int physOffsetN = Exp_u->GetPhys_Offset(elmID);
		LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp( elmID);
		Array<OneD,NekDouble> u_phys_old = ece.CreateWithOffset(ece,HNM2D->m_expansions[0]->GetPhys_Offset(elmID));
		for(int j=0; j< Exp_u->GetExp(elmID)->GetTotPoints();j++)
		{
			int index = j+physOffsetN;
			// constant scaling
			sm.EvaluateAt(xcN0[index],xcN1[index],0.0,SceN[index],valY,valZ,direction, mu*scaling,0);
			// dynamic scaling
			//glCoordTemp[0] = xcN0[index]; glCoordTemp[1] = xcN1[index];
			//sm.EvaluateAt(xcN0[index],xcN1[index],0.0,SceDN[index],valY,valZ,direction, HNM2D->GetDynamicScaling(glCoordTemp,elmID,mu),0);
			// actual function
			//fceN[index] = std::cos(2.0*M_PI*(xcN0[index]+xcN1[index]));
			//eceN[index] = lexp->PhysEvaluate(glCoordTemp, u_phys_old);
		}
	}
	t2 = tim2.elapsed();
	cout << "time taken by LSIAC" << t2 << endl;
}else{
	boost::timer tim3;
	for( int i=0; i< elIds.size(); i++)
	{
		int elmID = elIds[i];
		int physOffsetN = Exp_u->GetPhys_Offset(elmID);
		LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp( elmID);
		Array<OneD,NekDouble> u_phys_old = ece.CreateWithOffset(ece,HNM2D->m_expansions[0]->GetPhys_Offset(elmID));
		for(int j=0; j< Exp_u->GetExp(elmID)->GetTotPoints();j++)
		{
			int index = j+physOffsetN;
			// constant scaling
			//sm.EvaluateAt(xcN0[index],xcN1[index],0.0,SceN[index],valY,valZ,direction, mu*scaling,0);
			// dynamic scaling
			glCoordTemp[0] = xcN0[index]; glCoordTemp[1] = xcN1[index];
			sm.EvaluateAt(xcN0[index],xcN1[index],0.0,SceDN[index],valY,valZ,direction, HNM2D->GetDynamicScaling(glCoordTemp,elmID,mu),0);
			// actual function
			//fceN[index] = std::cos(2.0*M_PI*(xcN0[index]+xcN1[index]));
			//eceN[index] = lexp->PhysEvaluate(glCoordTemp, u_phys_old);
		}
	}
	t3 = tim3.elapsed();
	cout << "time taken by A LSIAC" << t3 << endl;
}
    NekDouble L2_SIAC=0.0, L2_DG=0.0, L2_Dyn_SIAC=0.0;
    NekDouble Linf_SIAC=0.0, Linf_DG=0.0, Linf_Dyn_SIAC=0.0;
    for(int i=0; i<elIds.size() ;i++)
    {   
        int index = elIds[i];
        LocalRegions::ExpansionSharedPtr lexp = Exp_u->GetExp( index );
        int phys_offset = Exp_u->GetPhys_Offset(index);
        Array<OneD,NekDouble> el_VF = fceN.CreateWithOffset( fceN, phys_offset);
        Array<OneD,NekDouble> el_VP = eceN.CreateWithOffset( eceN, phys_offset);
        Array<OneD,NekDouble> el_VS1 = SceN.CreateWithOffset( SceN, phys_offset);
        Array<OneD,NekDouble> el_VDS = SceDN.CreateWithOffset( SceDN, phys_offset);
        L2_DG += ( lexp->L2(el_VF,el_VP)*lexp->L2(el_VF,el_VP)) ; 
        L2_SIAC += ( lexp->L2(el_VF,el_VS1)*lexp->L2(el_VF,el_VS1));
        L2_Dyn_SIAC += ( lexp->L2(el_VF,el_VDS)*lexp->L2(el_VF,el_VDS));
        Linf_DG = std::max(Linf_DG,  lexp->Linf(el_VF,el_VP)); 
        Linf_SIAC =std::max(Linf_SIAC, lexp->Linf(el_VF,el_VS1));
        Linf_Dyn_SIAC =std::max(Linf_Dyn_SIAC, lexp->Linf(el_VF,el_VDS));
//      cout << index <<"\t" << L2_DG <<"\t"<< L2_SIAC << endl;  
//      cout << index <<"\t" << lexp->L2(el_VF,el_VP) <<"\t"<< lexp->L2(el_VF,el_VS1) << endl;  
	}   

	cout << "Error calculated at selective places" << endl; 
	cout << "mu:\t" << mu << endl;   
    cout << "L2 error DG:\t" << std::sqrt(L2_DG);
    cout << "\t SIAC\t" <<  std::sqrt(L2_SIAC);
    cout << "\t SIACDyn\t" <<  std::sqrt(L2_Dyn_SIAC) << endl;

    cout << "Linf error DG:\t" << Linf_DG ;
    cout << "\t SIAC\t" <<Linf_SIAC;
    cout << "\t SIACDyn\t" <<Linf_Dyn_SIAC << endl;

	cout << "Timers(sec) DG:\\t" << t1;
	cout << "\t SIAC\t" << t2;
	cout << "\t SIACDyn\t" <<t3 << endl;

//	} // end of mu for loop
}
//	HNM2D->IntersectWithBoxUsingRTree( -1.0+0.00001, -1.0+0.00001, -0.001,
//									   1.0-0.00001, 1.0-0.00001, 0.001, elIds,glIds);
	//HNM2D->IntersectWithBoxUsingRTree( 0.25+0.00001, 0.0+0.00001, -0.001,
	//								   0.75-0.00001, 1.0-0.00001, 0.001, elIds,glIds);
	//HNM2D->IntersectWithBoxUsingRTree( 0.25+0.00001, 0.25+0.00001, -0.001,
	//								   0.75-0.00001, 0.75-0.00001, 0.001, elIds,glIds);
	//HNM2D->IntersectWithBoxUsingRTree( -0.5+0.00001, -0.5+0.00001, -0.001,
	//								   0.5-0.00001, 0.5-0.00001, 0.001, elIds,glIds);
