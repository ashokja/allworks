%
close all;
clear all;
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize

m = [0.5,1,2,4,8,16];
L2 = [0.7006, 0.0249, 0.0253, 0.7551,1.1447, 18.3147];
Linf = [3.48, 0.18, 0.19, 0.2708, 1.53, 21.83];

m = [0.5,1,2,4,8];
L2 = [0.7006, 0.0249, 0.0253, 0.7551,1.1447];
Linf = [3.48, 0.18, 0.19, 0.2708, 1.53];

DGL2 = 2.5*ones(size(m));
DGLinf = 15.93*ones(size(m));

figure(1)
plot(m, log10(DGL2),'r*--','LineWidth',lw+1,'MarkerSize',msz); hold on;
plot(m, log10(DGLinf),'ro--','LineWidth',lw+1,'MarkerSize',msz);
plot(m, log10(L2),'b*--','LineWidth',lw+1,'MarkerSize',msz); hold on;
plot(m, log10(Linf),'bo--','LineWidth',lw+1,'MarkerSize',msz);
set(gca, 'XTick',m);
set(gca,'fontsize',20)
legend('DG L^2','DG L^\infty','LSIAC L^2','LSIAC L^\infty');
xlabel('scaling factor');
ylabel('log_{10}(Error)');

print(strcat('presUseImages/','DG_LSIACVorticityErrorStudy'), '-dpng','-r500');


