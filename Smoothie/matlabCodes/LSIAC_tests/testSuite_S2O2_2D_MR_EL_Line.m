% Which file use to generate this image.

clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [11];

va = 0;
vb = 90;

poly =2;
order = poly;
count =1;

%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize


for r = rates
filename = strcat('P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_EL_Line');    
    
    xfname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_pX_2D_OneSided2kp1_EL_Line.txt' );
    yfname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_pY_2D_OneSided2kp1_EL_Line.txt' );
    ffname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_pV_2D_OneSided2kp1_EL_Line.txt' );
    pfname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_pP_2D_OneSided2kp1_EL_Line.txt' );
    sfname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_pS_2D_OneSided2kp1_EL_Line.txt' );
    efname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_pE_2D_OneSided2kp1_EL_Line.txt' );
    
    disp(xfname);
    elval = floor( textread(efname, '%f\t') + 0.0001);
    xval = textread(xfname, '%f\t');
    yval = textread(yfname, '%f\t');
    fval = textread(ffname, '%f\t');
    pval = textread(pfname, '%f\t');
    sval = textread(sfname, '%f\t');
    
    [unique_ids,id_offset,id_tags] = unique(elval);
    
    % for each tag
        % get the data.
        % vis the data
    %end
    %num_unique_ids = (max(size(unique_ids)));
    for tag = unique_ids'
        el_indices = find(elval== (zeros(size(elval))+tag) );
        el_x = xval(el_indices);
        el_y = yval(el_indices);
        el_f = fval(el_indices);
        el_p = pval(el_indices);
        el_s = sval(el_indices);
 
        figure(1);
            hold on;
           plot(el_x,el_f,'LineWidth',lw,'MarkerSize',msz);

        figure(2); hold on;
           plot(el_x,el_p,'LineWidth',lw,'MarkerSize',msz);
           
        figure(3); hold on; caxis([-1,1])
           plot(el_x,el_s,'LineWidth',lw,'MarkerSize',msz);
           
 %       s = sqrt(max(size(el_x)));
 %       el_x = reshape(el_x,[s,s]);
 %       el_y = reshape(el_y,[s,s]);
 %       el_f = reshape(el_f,[s,s]);
 %       el_p = reshape(el_p,[s,s]);
 %       el_s = reshape(el_s,[s,s]);
 
%         figure(1);
%         surf(el_x,el_y,el_f); hold on;
%         shading interp;
%         xlabel('x-axis'); ylabel('y-axis'); zlabel('z-axis')
%         grid off;
%         GridLineStyle='none';
%         
%         figure(2),
%         surf(el_x,el_y,el_p); hold on;
%         shading interp;
%         xlabel('x-axis'); ylabel('y-axis'); zlabel('z-axis')
%         grid off;
% 
%         figure(3),
%         surf(el_x,el_y,el_s); hold on;
%         shading interp;
%         xlabel('x-axis'); ylabel('y-axis'); zlabel('z-axis')
%         grid off;

    end
end
%%

figure(1)
axis off;
xlim ([0,1]); ylim([-1-0.1,1+0.1]);
ylinSpace = linspace(0,1,11);
for yy = ylinSpace
    plot([yy,yy],[-1-0.11,1+0.11],'--ok','LineWidth',lw,'MarkerSize',msz); hold on;
end
%print(strcat('paperUseImages/',filename,'_Func'), '-dpng', '-r500');
xlim([0.55,0.65]); ylim([0.35,0.55])
%print(strcat('paperUseImages/',filename,'_FuncZm'), '-dpng', '-r500');
hold off;


%%
figure(2)
axis off;  xlim ([0,1]); ylim([-1-0.1,1+0.1]);
ylinSpace = linspace(0,1,11);
for yy = ylinSpace
    plot([yy,yy],[-1-0.11,1+0.11],'--ok','LineWidth',lw,'MarkerSize',msz); hold on;
end
%print(strcat('paperUseImages/',filename,'_CG'), '-dpng', '-r500');
%xlim([0.55,0.65]); ylim([0.35,0.55])
%print(strcat('paperUseImages/',filename,'_CGZm'), '-dpng', '-r500');
hold off;
%%

figure(3)
xlim ([0,1]); ylim([-1-0.1,1+0.1]); axis off; grid on; hold on; caxis([-1,1])
ylinSpace = linspace(0,1,11);
for yy = ylinSpace
    plot([yy,yy],[-1-0.11,1+0.11],'--ok','LineWidth',lw,'MarkerSize',msz); hold on;
end
%print(strcat('paperUseImages/',filename,'_CG_SIAC_X'), '-dpng', '-r500');
%xlim([0.55,0.65]); ylim([0.35,0.55])
%print(strcat('paperUseImages/',filename,'_CG_SIACZM_X'), '-dpng', '-r500');
hold off;


%plot grid
%plot(linspace(0,1,11),0.0,'b.'); hold off;
%plot([0.5,0.5],[0,1],'-'); hold off;

%%
% % % % figure(1),
% % % % saveas(gcf,'images/2DImages/AnalyticalFunc.png');
% % % % 
% % % % figure(2),
% % % % saveas(gcf,'images/2DImages/DGFunc.png');
% % % % 
% % % % figure(3),
% % % % saveas(gcf,'images/2DImages/SIACFunc_X.png');



%     s = sqrt(max(size(xpos)));
%     xVpos = reshape(xpos,s,s);
%     yVpos = reshape(ypos,s,s);
%     fVval = reshape(fval,s,s);
%     pVval = reshape(pval,s,s);
%     sVval = reshape(sval,s,s);    