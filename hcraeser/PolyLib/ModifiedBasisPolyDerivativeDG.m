

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %
  %
  %
  %
  %
  %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  function y = ModifiedBasisPolyDerivativeDG(degree,x)

  if degree == 0
    y = -0.5*ones(size(x));
  elseif degree == 1
    y = 0.5*ones(size(x));
  else
    y = -0.5.*x.*JacobiPoly(degree-2,x,1,1) + 0.25*(1-x).*(1+x).*JacobiPolyDerivative(degree-2,x,1,1);
  end
  
  