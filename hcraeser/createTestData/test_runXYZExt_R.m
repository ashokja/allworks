function [xy,distance,t_a,mapxyV,coeff] = test_runXYZExt_R(r,mapxy)
    switch nargin,
        case 0
            r =1;
            sampledata = 50;
            dataX = linspace(-1,1,5);
            dataY = linspace(-1,1,6);
            dataZ = linspace(-1,1,7);
            mapxy = changeGridToArray(dataX,dataY,dataZ);
        case 2
            sampledata = 50;
    end
    rng('default');
    rng(r);
    
    % vortex curve.
    data = linspace(-1,1,sampledata)*1.5;
    n = 20;
    %[xcoeff,ycoeff,zcoeff] = pickRand6twice(n);
    [xcoeff,ycoeff,zcoeff] = pickSimpleRCase(n);
    coeff = [xcoeff;ycoeff;zcoeff];
    xdata = B_eval(xcoeff,data);
    ydata = B_eval(ycoeff,data);
    zdata = B_eval(zcoeff,data);
    curvexy(:,1) = xdata; curvexy(:,2) = ydata; curvexy(:,3) = zdata;
    curveNo(:,1) = B_der_eval(xcoeff,data);
    curveNo(:,2) = B_der_eval(ycoeff,data);
    curveNo(:,3) = B_der_eval(zcoeff,data);
    
    %quiver3(curvexy(:,1),curvexy(:,2),curvexy(:,3),curveNo(:,1),curveNo(:,2),curveNo(:,3));
    plot3(curvexy(:,1),curvexy(:,2),curvexy(:,3)); hold on;    plotBoundingBox();
    
    [xy,distance,t_a] = distance2curve(curvexy,mapxy);
    
    %contourslice(X,Y,Z,DISTANCE,[],curveDataIn,[]); hold on;
    %contourslice(X,Y,Z,TA,[],[],curveDataIn); hold on;
    t_a_basis = t_a*4-2;
    x_NonCurve = B_der_eval(xcoeff,t_a_basis);
    y_NonCurve = B_der_eval(ycoeff,t_a_basis);
    z_NonCurve = B_der_eval(zcoeff,t_a_basis);
    NonCurve = [x_NonCurve,y_NonCurve,z_NonCurve];
    VecCurveToPoint = mapxy - xy;
    for i = 1:max(size(NonCurve))
 %case 1;
 %        NN(i,:) = norm(NonCurve(i,:))
         NonCurve(i,:) = NonCurve(i,:)/norm(NonCurve(i,:));
         %norm(NonCurve(i,:))

%  %case 2;
%         if( norm(VecCurveToPoint(i,:)) > 10^(-3))
%             VecCurveToPoint(i,:) = VecCurveToPoint(i,:)/norm(VecCurveToPoint(i,:));
%         else
%             VecCurveToPoint(i,:) = NonCurve(i,:);
%         end
        
    end
    mapxyV = 4*cross(NonCurve,VecCurveToPoint,2)+ ([1./ones(size(distance+1)),1./ones(size(distance+1)),1./ones(size(distance+1))].*NonCurve);
            %([1./(distance+1),1./(distance+1),1./(distance+1)].*NonCurve);

%    mapxyV = NonCurve;
%    mapxyV = VecCurveToPoint;
%    figure(7)
%    quiver3(mapxy(:,1),mapxy(:,2),mapxy(:,3),mapxyV(:,1),mapxyV(:,2),mapxyV(:,3));
end

function [] = plotQuiver(data,dataV)
    quiver3(data(:,1), data(:,2), data(:,3), ...
                dataV(:,1), dataV(:,2), dataV(:,3));
end



function [data] = changeGridToArray(dataX,dataY,dataZ)
   [X,Y,Z] = meshgrid(dataX,dataY,dataZ);
   data(:,1) = reshape(X,[],1);
   data(:,2) = reshape(Y,[],1);
   data(:,3) = reshape(Z,[],1);
end

function [xcoeff,ycoeff,zcoeff] = pickSimpleCase(n)
    xcoeff = (abs((-1:2/n:1))-1);
%     ycoeff = (-1:2/n:1);
%    zcoeff = 0.4*(abs((-1:2/n:1))-1);
%   xcoeff = zeros(1,n+1);
    ycoeff = (-1:2/n:1);
    zcoeff = zeros(1,n+1);
end

function [xcoeff,ycoeff,zcoeff] = pickSimpleRCase(n)
    xcoeff = ((-1:2/n:1)-1)/2;
%     ycoeff = (-1:2/n:1);
%    zcoeff = 0.4*(abs((-1:2/n:1))-1);
%   xcoeff = zeros(1,n+1);
    ycoeff = ((-1:2/n:1)+1)/2;
    zcoeff = zeros(1,n+1);
    
    xcoeff = cos(xcoeff*pi/2)-1;
    ycoeff = cos(ycoeff*pi/2)-1;
end

function [xcoeff,ycoeff,zcoeff] = pickRand6twice(n)
    xcoeff = rand(1,n+1)*2-1;
    ycoeff = rand(1,n+1)*2-1;
    zcoeff = rand(1,n+1)*2-1;
    r = floor(rand(1,1)*6)+1;
    switch r % for starting point
        case 1, %x = -1 plane
            xcoeff(1) = -1;
        case 2, %x = =1 place
            xcoeff(1) = 1;
        case 3, %y = -1 plane
            ycoeff(1) = -1;
        case 4, %y = =1 place
            ycoeff(1) = 1;
        case 5, %z = -1 plane
            zcoeff(1) = -1;
        case 6, %z = =1 place
            zcoeff(1) = 1;
    end
    
    r = floor(rand(1,1)*6)+1;
    switch r % for end point
        case 1, %x = -1 plane
            xcoeff(n+1) = -1;
        case 2, %x = =1 place
            xcoeff(n+1) = 1;
        case 3, %y = -1 plane
            ycoeff(n+1) = -1;
        case 4, %y = =1 place
            ycoeff(n+1) = 1;
        case 5, %z = -1 plane
            zcoeff(n+1) = -1;
        case 6, %z = =1 place
            zcoeff(n+1) = 1;
    end
    
end

