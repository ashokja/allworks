function [x,ret,debugX,debugY,y]=critmultinewton(x,NumIters, xV,yV,uV,vV,uxV,uyV,vxV,vyV,latticeGrid)
%Performs multidimensional Newton?s method for the function defined in f
%starting with x and running NumIters times.
    [y,dy,ret]=critfunctionEval(x(1),x(2),xV,yV,uV,vV,uxV,uyV,vxV,vyV,latticeGrid);
    for j=1:NumIters
        s=dy\y;
        x=x-s;
        debugX(j,:) = x;
        debugY(j,:) = y;
        [y,dy,ret]=critfunctionEval(x(1),x(2),xV,yV,uV,vV,uxV,uyV,vxV,vyV,latticeGrid);
        if (0==ret)
            disp(j);
            return;
        end
        
    end

end