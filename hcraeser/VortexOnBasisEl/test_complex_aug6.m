clear;
% complex example looking for relaxation methods.

%initialize variables
n1 = 6; n2 = 6; n3 =6;
seedNum = 1;

%initialize Data
x = (-1:.1:1)';
[X,Y,Z] = meshgrid(x,x,x);
dataIn(:,1) = reshape(X,[],1);
dataIn(:,2) = reshape(Y,[],1);
dataIn(:,3) = reshape(Z,[],1);
[sx,~] = size(dataIn);
dataOut = zeros(size(dataIn));

for i = 1:sx
    coords = squeeze(dataIn(i,:));
    x =  coords(1)+1.001; y = coords(2)+1; z = coords(3);
    teta = atan(y/x);
    lambda = sqrt((x-1)^2+(y-1)^2+z^2); r =1;
    v1= [x,y,z]; v2 = [cos(teta),sin(teta),0];
    %rotational Velocity.
    %vel = v2;
    velh = 0.1*abs(sqrt(3)-lambda)*[sin(teta),-cos(teta),0];
    vel = cross(v1-v2,[-sin(teta),cos(teta),0])+velh;
    dataOut(i,:) = vel;
end

% acutal code
coeff = buildCoeff(n1,n2,n3,dataIn,dataOut);
% figure(1)
% vizualizeCoeff(n1,n2,n3,coeff);
% 

[uCoeffRed,uCoeffRot] = divideVel(n1,n2,n3,coeff,dataIn,dataOut);

% 
% figure(2);
% vizualizeCoeff(n1,n2,n3,uCoeffRed);
% 
% figure(3);
% vizualizeCoeff(n1,n2,n3,uCoeffRot);
% xlabel('x');ylabel('y');zlabel('z');
%%
x = (-1:.3:0);
y = -1-x;
z = zeros(size(x));
[~,sx] = size(x);
figure(4);
plot3(x,y,z,'g*'); hold on;
pOut = zeros(sx,3);
for i = 1:sx
    pointIn = [x(i),y(i),z(i)];
    pOut(i,:) = closestZeroP(n1,n2,n3,uCoeffRot,uCoeffRed,pointIn,0.125);
end
%%

plotBoundingBox(); hold on;
plot3(pOut(:,1),pOut(:,2),pOut(:,3),'r*');
hold off;
