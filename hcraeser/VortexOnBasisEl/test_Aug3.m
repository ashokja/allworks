% test_Aug 3.
% checking for the closest point to solve.
% test Simple Iteration.
clear;

%initialize variables
n1 = 4; n2 = 4; n3 = 4;
seedNum = 1;
d1 =10; d2 =10; d3 = 10;
f=1;

%initialize Data
x = (-1:.2:1)';
[X,Y,Z] = meshgrid(x,x,x);
dataIn(:,1) = reshape(X,[],1);
dataIn(:,2) = reshape(Y,[],1);
dataIn(:,3) = reshape(Z,[],1);
[sx,~] = size(dataIn);
dataOut = zeros(size(dataIn));
for i = 1:sx
    coords = squeeze(dataIn(i,:));
    vel = cross(coords,[0,1,0])+ [0,1,0];
    dataOut(i,:) = vel;
end

%%
coeff = buildCoeff(n1,n2,n3,dataIn,dataOut);

figure(1)
vizualizeCoeff(n1,n2,n3,coeff);

[uCoeffRed,uCoeffRot] = divideVel(n1,n2,n3,coeff,dataIn,dataOut);

figure(2);
vizualizeCoeff(n1,n2,n3,uCoeffRed);

figure(3);
vizualizeCoeff(n1,n2,n3,uCoeffRot);


%n1,n2,n3,coeffRot,coeffRed,pointIn
pOut = closestZeroP(n1,n2,n3,uCoeffRot,uCoeffRed, [0.5,0.5,0.5]);
