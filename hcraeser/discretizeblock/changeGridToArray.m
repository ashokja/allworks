function [data] = changeGridToArray(x,y,z)
    switch nargin
        case 1
            y =x; z=x;
    end
    
   [X,Y,Z] = meshgrid(x,y,z);
   data(:,1) = reshape(X,[],1);
   data(:,2) = reshape(Y,[],1);
   data(:,3) = reshape(Z,[],1);
end