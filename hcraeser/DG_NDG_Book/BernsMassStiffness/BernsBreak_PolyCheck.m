% Create a mass matrix of
% variables.
%N = 10;

for N = 2:20
    %Mu=f
    %find u;
    
    [r,w] = JacobiGQ(0,0,N);
    r = (r+1)/2;
   
    M = zeros(N,N);
    
    for i = 0:N-1
        for j = 0:N-1
            temp =0.0;
            for rv = 1:N
                temp = temp + w(rv)*B_i(i,N-1,r(rv))*B_i(j,N-1,r(rv));
            end
            M(i+1,j+1) = temp;
        end
    end
    
    % Calculated Mass matrix.
    
    % Calculate f = \integral^1_0 f(x)Bi(x) dx
    %
    
    f = zeros(N,1);
    for i = 0:N-1;
        temp =0.0;
        for rv = 1:N
            temp = temp + w(rv)*B_i(i,N-1,r(rv))* sin(r(rv)*10);
        end
        f(i+1) = temp;
    end
    
    % soved coeffecients.
    u = M\f;
    
    test_r = (0:0.01:1)';
    [s,del] = size(test_r);
    
    % projected Function.
    u = u.*(u>0.0);
    ProjF = funcEvalBerns(u,test_r);
    ActF = sin(test_r*10);
    %plot(test_r, ProjF,test_r,ActF);
    ActEr = sum(abs(ActF-ProjF))/s;
    Err(N)= ActEr;
end

plot(Err,'+-');
ylim([0 5])
title('Error f-U.')
xlabel('ploynomail order');
ylabel('Error');
grid on;


