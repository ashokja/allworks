% Graph sym jagged 8 -113 DG
% Results from PostP_hexSym_DG. Dated Dec 18th.

% ERR plot log of them.

clear;
close all;

MR = (4:2:6)*7;
MRM = (4:2:6)*6;

%MR = (4)*7;
%MRM = (4)*6;
TMres = (MR).*(MRM)*2;

poly =2;

if (poly ==1)
    TMres = TMres*6;
else
    TMres = TMres*12;
end
%P1
%[TMres'*6, MR'*3, MRM'*4,MR'.*3.*MRM'.*4 ]
%P2
%[TMres'*12, MR'*4, MRM'*6,MR'.*4.*MRM'.*6 ]

% For P1.
if (poly ==1)
    Xres = MR*3; Yres = MRM*4;
        %Xres = MR*3*4; Yres = MRM*4*4;
else
    Xres = MR*4; Yres = MRM*6;
end


i =2;
for i =1:2
%submitted for paper.
pErrfileCart = strcat('../PostP_hexSym_DG_uni/err_P',int2str(poly),'_',int2str(TMres(i)),'_',int2str(MR(i)+1),'_cart.dat');
pErrfileHex = strcat('../PostP_hexSym_DG_uni/err_P',int2str(poly),'_',int2str(TMres(i)),'_',int2str(MR(i)+1),'_hex.dat');

%Testing for review
%pErrfileCart = strcat('../Postp_hexSym_DG_Uni_dup1/err_P',int2str(poly),'_',int2str(TMres(i)),'_',int2str(MR(i)+1),'_cart.dat');
%pErrfileHex = strcat('../Postp_hexSym_DG_Uni_dup1/err_P',int2str(poly),'_',int2str(TMres(i)),'_',int2str(MR(i)+1),'_hex.dat');
%pErrfileCart = strcat('../PostP_hexSym_DG_Uni_dup3/err_P',int2str(poly),'_',int2str(TMres(i)),'_',int2str(MR(i)+1),'_cart.dat');
%pErrfileHex = strcat('../PostP_hexSym_DG_Uni_dup3/err_P',int2str(poly),'_',int2str(TMres(i)),'_',int2str(MR(i)+1),'_hex.dat');
%pErrfileCart = strcat('../PostP_hexSym_DG_2_Grap1/err_P',int2str(poly),'_',int2str(TMres(i)),'_',int2str(MR(i)+1),'_cart.dat');
%pErrfileHex = strcat('../PostP_hexSym_DG_2_Grap1/err_P',int2str(poly),'_',int2str(TMres(i)),'_',int2str(MR(i)+1),'_hex.dat');
%pErrfileCart = strcat('../PostP_hexSym_DG_2small_Grap1/err_P',int2str(poly),'_',int2str(TMres(i)),'_',int2str(MR(i)+1),'_cart.dat');
%pErrfileHex = strcat('../PostP_hexSym_DG_2small_Grap1/err_P',int2str(poly),'_',int2str(TMres(i)),'_',int2str(MR(i)+1),'_hex.dat');

disp(pErrfileCart);
fid = fopen(pErrfileCart);
Ccart = textscan(fid,'%f ');
fclose(fid);

Ccartdata = Ccart{1};
%histogram(log(Ccartdata))
lcdata = log(Ccartdata);
lcVdata = reshape( lcdata,Yres(i),Xres(i));
%lcVdata = reshape( lcdata,Xres(i),Yres(i));

disp(pErrfileHex);
fid = fopen(pErrfileHex);
Chex = textscan(fid,'%f ');
fclose(fid);

Chexdata = Chex{1};
%histogram(log(Chexdata))
lhdata = log(Chexdata);
lhVdata = reshape( lhdata,Yres(i),Xres(i));

maxN = max( max(lcVdata(:)), max(lhVdata(:)) )
minN = min( min(lcVdata(:)), min(lhVdata(:)) )
minN = -18;
maxN = -4;
ccaxis = linspace(minN,maxN,80); 
ccaxis = [-29,ccaxis];


figure(i*2+1)
%contour(lcVdata,ccaxis,'Fill','on');
contour(lcVdata,ccaxis);
% if i==1
%     contour(lcVdata(30:50,30:50),ccaxis);
% else
%     contour(lcVdata(50:70,50:70),ccaxis);
% end
caxis([minN maxN]);
contourcbar
%axis on; grid on;
axis off;
set(gca,'fontsize',15)
%print(strcat('Contours_P_',int2str(poly),'_DG_MeshEL_',int2str(TMres(i)/poly/6),'_cartSIAC'),'-dpng','-r500');
%print(strcat('Contours_P_',int2str(poly),'_DG_MeshEL_',int2str(TMres(i)/poly/6),'_cartSIAC_ZM'),'-dpng','-r500');
print(strcat('~/Desktop/','Contours_P_',int2str(poly),'_DG_MeshEL_',int2str(TMres(i)/poly/6),'_cartSIAC_KernelScaling_1'),'-dpng','-r500');


figure(i*2+2)
%contour(lhVdata,linspace(minN,maxN,20),'Fill','on')
%contour(lhVdata,ccaxis,'Fill','on')
contour(lhVdata,ccaxis)
% if i==1
%     contour(lhVdata(30:50,30:50),ccaxis);
% else
%     contour(lhVdata(50:70,50:70),ccaxis);
% end
caxis([minN maxN]);
contourcbar
%axis on; grid on;
axis off;
set(gca,'fontsize',15)
%print(strcat('Contours_P_',int2str(poly),'_DG_MeshEL_',int2str(TMres(i)/poly/6),'_hexSIAC'),'-dpng','-r500');
%print(strcat('Contours_P_',int2str(poly),'_DG_MeshEL_',int2str(TMres(i)/poly/6),'_hexSIAC_ZM'),'-dpng','-r500');



end

%surf(-1*lcVdata);






% MR = (2:2:16)*7;
% MRM = ((2:2:16))*6;
% TMres = (MR).*(MRM)*2;
% %mshEl = [168, 672, 2688, 10752, 43008];
% % ord(1) = 336;    ord(2) = 1344;    ord(3) = 3024;   ord(4) = 5376;
% % ord(5) = 8400;   ord(6) = 12096;   ord(7) = 16464;   ord(8) = 21504;
% 
% mshEl(1) = 168;    mshEl(2) = 672;    mshEl(3) = 1512;   mshEl(4) = 2688;
% mshEl(5) = 4200;   mshEl(6) = 6048;   mshEl(7) = 8232;   mshEl(8) = 10752;
% mshEl = mshEl*2;
% 
% disp(TMres);
% disp(mshEl);
% 
% % addpath('..');
% % %pdgCartfilename = '../PostP_hexSym_DG/log_hex_sym_DG_MAXSIZE_MR';
% % %pdgMaxfilename = '../PostP_hexSym_DG/log_hex_sym_DG_MAXSIZE_MR';
% % pErrfile = '../PostP_hexSym_DG_uni/err_P1_2016_15_cart.dat';
% % 
% % fid = fopen(pErrfile);
% % C = textscan(fid,'%f ');
% % fclose(fid);
% % Cdata = C{1};
% % % histogram(log(Cdata))
% % % lcdata = log(Cdata);
% % % lcVdata = reshape( lcdata,20,25);
% % % 
% % % contour(lcVdata)
% % 
% 
