#!/usr/bin/python
import sys
from decimal import Decimal 

#Exefile = "TestSuiteS2O2"
Exefile = sys.argv[1]
#filterOrder="2"
#filterOrder=sys.argv[2]

filterOrder="2"
filename= "../data/NekMeshes/quad_simple_"
filename = "../data/NekMeshes/P"+str(filterOrder)+"_quad_simple_"

rates = [10,20,40]
for r in rates:
	if (len(sys.argv)==3):
		print "./"+ Exefile +" "+ filename+str(r)+".xml "+filterOrder+" "+ str((1.0/(r-0)))+" "+sys.argv[2]
	else:
		print "./"+ Exefile +" "+ filename+str(r)+".xml "+filterOrder+" "+ str((1.0/(r-0)))

filterOrder="3"
		
filename = "../data/code/mshes/nmshs/P"+ filterOrder+"_SAMPLE1D_"
filename = "../data/NekMeshes/P"+str(filterOrder)+"_quad_simple_"
rates = [10,20,40]
for r in rates:
	if (len(sys.argv)==3):
		print "./"+ Exefile +" "+ filename+str(r)+".xml "+filterOrder+" "+ str((1.0/(r-0)))+" "+sys.argv[2]
	else:
		print "./"+ Exefile +" "+ filename+str(r)+".xml "+filterOrder+" "+ str((1.0/(r-0)))

filterOrder="4"

filename = "../data/code/mshes/nmshs/P"+ filterOrder+"_SAMPLE1D_"
filename = "../data/NekMeshes/P"+str(filterOrder)+"_quad_simple_"
rates = [10,20,40]
for r in rates:
	if (len(sys.argv)==3):
		print "./"+ Exefile +" "+ filename+str(r)+".xml "+filterOrder+" "+ str((1.0/(r-0)))+" "+sys.argv[2]
	else:
		print "./"+ Exefile +" "+ filename+str(r)+".xml "+filterOrder+" "+ str((1.0/(r-0)))


print "mv ../data/NekMeshes/*.txt ../matlabCodes/datatxt/"
