function [solAtSeeds,sol_seeds,n1,n2,n3,coeff] = test_3Dbuildsolve2ndIter(n1,n2,seedNum,d1,d2,f)
            % created a attachment and dettachment line which are not straight.
    switch nargin
        case 0,
            n1 = 6; n2 = 6; n3 = 6;
            seedNum = 3;
            d1 =10;
            d2 =10;
            d3 = 10;
            f=1;
            sol_seeds =0; Vbar =0;
    end
%     [xx,w1] = lglnodes(d2);
%     [yy,w2] = lglnodes(d1);
%     [zz,w3] = lglnodes(d3);
    xx = (-1:.2:1)';
    yy= (-1:.2:1)';
    zz = (-1:.2:1)';
    %x = (-1:2/d1:1);
    %y = (-1:2/d2:1);
    [X,Y,Z] = meshgrid(xx,yy,zz);
    [sx,~] = size(xx);
    [sy,~] = size(yy);
    [sz,~] = size(zz);

    for i = 1:sx
    	for j = 1:sy
        	for k = 1:sz
            	%rc = rand(1,3);
                 x =  X(i,j,k)+1.001; y = Y(i,j,k)+1; z = Z(i,j,k);
                 teta = atan(y/x);
                 a = [x-cos(teta),y-sin(teta),z];
                 vel = cross(a,[sin(teta),-cos(teta),0]) + [cos(teta),sin(teta),0]*.1;
                
                % vel = vel./norm(vel);
                if ( j <sx+1)
                    U(i,j,k) = vel(1) ;
                    V(i,j,k) = vel(2);
                    W(i,j,k) = vel(3);
                else
                    U(i,j,k) = 0;
                    V(i,j,k) = 1;
                    W(i,j,k) = 0;
                end
             end
        end
    end
            %Complicated function.
%     px = sin(2*X+(2*Y).^2);
%     py = cos(2*X+(2*Y).^2);
%     pz = Z;
            %Simple Saddle function.
    px = U;
    py = V;
    pz = W;

            % viz
    figure (1),
    quiver3(X,Y,Z,px,py,pz);

            %Created dataIn
    dataIn(:,1) = reshape(X,[],1);
    dataIn(:,2) = reshape(Y,[],1);
    dataIn(:,3) = reshape(Z,[],1);
    
            %Need to calcualte coeff.
    count=1;
    for k1 = 0:n1
        for k2 = 0:n2
            for k3 = 0:n3
                mat(:,count) = B3(n1,k1,n2,k2,n3,k3,dataIn);
                count = count +1;
            end
        end
    end

    px_dataIn = reshape(px,[],1);
    py_dataIn = reshape(py,[],1);
    pz_dataIn = reshape(pz,[],1);
% 
% 
    %figure (2),
   % quiver3(dataIn(:,1),dataIn(:,2),dataIn(:,3),px_dataIn,py_dataIn,pz_dataIn);
% 
    coeff(:,1) = mat\px_dataIn;
    coeff(:,2) = mat\py_dataIn;
    coeff(:,3) = mat\pz_dataIn;
% 
    dataOut = B3_eval(n1,n2,n3,coeff,dataIn);
% 
    figure (3),
    quiver3(dataIn(:,1),dataIn(:,2),dataIn(:,3),dataOut(:,1),dataOut(:,2),dataOut(:,3));
%    quiver3(dataIn(:,1),dataIn(:,2),dataIn(:,3),px_dataIn,py_dataIn,pz_dataIn);
    xlabel('x');
    ylabel('y');
    zlabel('z');

%     %Complex evaluations.
%     Vbar = B3_der_eval(n1,n2,n3,coeff,dataIn);
%     V_bar = reshape(Vbar,[],3,3);
%     %Do eigen analysis for every point.
%     [s_dIn,~] = size(dataIn);
%     dataEigComp = zeros(s_dIn,1);
%     for i =1:s_dIn
%         jj(:,:) = V_bar(i,:,:);
%         lambda = eig(jj);
%         tet_complex =false;
%         if ~isreal(lambda)
%             'tet contains complex number';
%             tet_complex = true;
%         end
%         dataEigComp(i) = tet_complex;
%         if (tet_complex)
%             % find the real rool index. 
%             real_index  = find(imag(lambda)==0);
%         end
% 
%         if (tet_complex)
%             [A,~] = eig(jj);
%             
%             n = A(:,real_index)'/norm(A(:,real_index));
%             dataOutRv(i,:) = dot(dataOut(i,:),n)*n;
%             dataOutSv(i,:) = dataOut(i,:) - dot(dataOut(i,:),n)*n;
%         else
%             dataOutRv(i,:) = dataOut(i,:);
%             dataOutSv(i,:) = zeros(1,3);
%         end
%     end
    
%     %Plots
%     %quiver3(dataIn(:,1),dataIn(:,2),dataIn(:,3),dataEigComp.*dataOut(:,1),dataEigComp.*dataOut(:,2),dataEigComp.*dataOut(:,3),'g');
%     %hold off;
%     figure(4),
%     quiver3(dataIn(:,1),dataIn(:,2),dataIn(:,3),dataEigComp.*dataOut(:,1),dataEigComp.*dataOut(:,2),dataEigComp.*dataOut(:,3),'k');
%     figure(5);
% %    quiver3(dataIn(:,1),dataIn(:,2),dataIn(:,3),dataOut(:,1),dataOut(:,2),dataOut(:,3),'g'); hold on;
%     quiver3(dataIn(:,1),dataIn(:,2),dataIn(:,3),dataOutRv(:,1),dataOutRv(:,2),dataOutRv(:,3),'b'); hold off;
%     figure(6);
%     quiver3(dataIn(:,1),dataIn(:,2),dataIn(:,3),dataOutSv(:,1),dataOutSv(:,2),dataOutSv(:,3),'r'); hold off;
    

    %face1
    [X,Y,Z] = meshgrid(-1,(-1:.1:1),(-1:.1:1));
    PlotFaceOnGrid(X,Y,Z,n1,n2,n3,coeff,1,2,3);
    
    %face2
    [X,Y,Z] = meshgrid(1,(-1:.1:1),(-1:.1:1));
    PlotFaceOnGrid(X,Y,Z,n1,n2,n3,coeff,2,2,3);
    %face3
    [X,Y,Z] = meshgrid((-1:.1:1),-1,(-1:.1:1));
    PlotFaceOnGrid(X,Y,Z,n1,n2,n3,coeff,3,1,3);
    %face4
    [X,Y,Z] = meshgrid((-1:.1:1),+1,(-1:.1:1));
    PlotFaceOnGrid(X,Y,Z,n1,n2,n3,coeff,4,1,3);
    %face5
    [X,Y,Z] = meshgrid((-1:.1:1),(-1:.1:1),-1);
    PlotFaceOnGrid(X,Y,Z,n1,n2,n3,coeff,5,1,2);
    %face6
    [X,Y,Z] = meshgrid((-1:.1:1),(-1:.1:1),+1);
    PlotFaceOnGrid(X,Y,Z,n1,n2,n3,coeff,6,1,2);

    rand('seed',407);
    seeds = (rand(seedNum,3)-0.5)/2;
    sol_seeds = zeros(seedNum,6,3);
    solAtSeeds = zeros(seedNum,6,3);
    for i = 1:seedNum
        sol_seeds(i,1,:) = MultiNewtonBasis(n1,n2,n3,coeff,seeds(i,:),10,-1,1);
        sol_seeds(i,2,:) = MultiNewtonBasis(n1,n2,n3,coeff,seeds(i,:),10,+1,1);
        sol_seeds(i,3,:) = MultiNewtonBasis(n1,n2,n3,coeff,seeds(i,:),10,-1,2);
        sol_seeds(i,4,:) = MultiNewtonBasis(n1,n2,n3,coeff,seeds(i,:),10,+1,2);
        sol_seeds(i,5,:) = MultiNewtonBasis(n1,n2,n3,coeff,seeds(i,:),10,-1,3);
        sol_seeds(i,6,:) = MultiNewtonBasis(n1,n2,n3,coeff,seeds(i,:),10,+1,3);
    end
    
    seeds
    sol_seeds
    
    for i = 1:seedNum
        for j = 1:6
            solAtSeeds(i,j,:) = B3_eval(n1,n2,n3,coeff,sol_seeds(i,j,:));
        end
    end
    solAtSeeds
    
    function rest = PlotFaceOnGrid( X,Y,Z,n1,n2,n3,coeff,c,p1,p2)
        %face1;
        dataInF(:,1) = reshape(X,[],1);
        dataInF(:,2) = reshape(Y,[],1);
        dataInF(:,3) = reshape(Z,[],1);
        dataOutF = B3_eval(n1,n2,n3,coeff,dataInF);
        figure(8),
        subplot(2,3,c),quiver(dataInF(:,p1),dataInF(:,p2),dataOutF(:,p1),dataOutF(:,p2));
    end
    
    function x=MultiNewtonBasis(n1,n2,n3,coeff,x,NumIters,val,id)
    %Performs multidimensional Newton's method for the function defined in f
    %starting with x and running NumIters times.
        y = B3_eval(n1,n2,n3,coeff,x);
        ko = B3_der_eval(n1,n2,n3,coeff,x);
        dy = reshape(ko,3,3);
        %[res1,res2] = f_der(
        %dy = ;
        for j=1:NumIters
            s=pinv(dy')*y';
            x=x-s';
            x(id) = val;
            y = B3_eval(n1,n2,n3,coeff,x);
            dy = reshape(B3_der_eval(n1,n2,n3,coeff,x),3,3);
        end 
    end

    

    function [rest] =  B2(n1,k1,n2,k2,dataIn)
        v1 = B(n1,k1,dataIn(:,1));
        v2 = B(n2,k2,dataIn(:,2));
        rest = v1.*v2;
    end
    
    function [rest] = B3(n1,k1,n2,k2,n3,k3,dataIn)
        v1 = B(n1,k1,dataIn(:,1));
        v2 = B(n2,k2,dataIn(:,2));
        v3 = B(n3,k3,dataIn(:,3));
        rest = v1.*v2.*v3;    
    end
    
    
    function [rest] = B2_der(n1,k1,n2,k2,dataIn)
        v1 =     B(n1,k1,dataIn(:,1));
        v1_der = B_der(n1,k1,dataIn(:,1));
        v2 =     B(n2,k2,dataIn(:,2));
        v2_der = B_der(n2,k2,dataIn(:,2));
        
        rest(:,1) = v1_der.*v2;
        rest(:,2) = v1.*v2_der;
    end

    function [rest] = B3_der(n1,k1,n2,k2,n3,k3,dataIn)
        v1 =     B(n1,k1,dataIn(:,1));
        v1_der = B_der(n1,k1,dataIn(:,1));
        v2 =     B(n2,k2,dataIn(:,2));
        v2_der = B_der(n2,k2,dataIn(:,2));
        v3 =     B(n3,k3,dataIn(:,3));
        v3_der = B_der(n3,k3,dataIn(:,3));
        rest(:,1) = v1_der.*v2.*v3;
        rest(:,2) = v1.*v2_der.*v3;
        rest(:,3) = v1.*v2.*v3_der;
    end

    function [rest] = B2_eval(n1,n2,coeff,dataIn)
       count =1;
       rest = zeros(size(dataIn));
       for k1 = 0:n1
           for k2 = 0:n2
               rest(:,1) = rest(:,1)  + coeff(count,1)*B2(n1,k1,n2,k2,dataIn);
               rest(:,2) = rest(:,2)  + coeff(count,2)*B2(n1,k1,n2,k2,dataIn);
               count= count +1;
           end
       end
    end

    function [rest] = B2_eval_3(n1,n2,coeff,dataIn)
       count =1;
       [a,b] = size(dataIn);
       rest = zeros(a,b+1);
       for k1 = 0:n1
           for k2 = 0:n2
               rest(:,1) = rest(:,1)  + coeff(count,1)*B2(n1,k1,n2,k2,dataIn);
               rest(:,2) = rest(:,2)  + coeff(count,2)*B2(n1,k1,n2,k2,dataIn);
               rest(:,3) = rest(:,3)  + coeff(count,3)*B2(n1,k1,n2,k2,dataIn);
               count= count +1;
           end
       end
    end

    function [rest] = B3_eval(n1,n2,n3,coeff,dataIn)
       count =1;
       rest = zeros(size(dataIn));
       for k1 = 0:n1
           for k2 = 0:n2
               for k3= 0:n3
                   rest(:,1) = rest(:,1)  + coeff(count,1)*B3(n1,k1,n2,k2,n3,k3,dataIn);
                   rest(:,2) = rest(:,2)  + coeff(count,2)*B3(n1,k1,n2,k2,n3,k3,dataIn);
                   rest(:,3) = rest(:,3)  + coeff(count,3)*B3(n1,k1,n2,k2,n3,k3,dataIn);
                   count= count +1;
               end
           end
       end
    end

    function [rest] = B3_der_eval(n1,n2,n3,coeff,dataIn)
        count = 1;
       [a,b] = size(dataIn);
       rest = zeros(a,b+6);
       for k1 = 0:n1
           for k2 = 0:n2
               for k3 = 0:n3
                    rest(:,1:3) = rest(:,1:3)  + coeff(count,1)*B3_der(n1,k1,n2,k2,n3,k3,dataIn);
                    rest(:,4:6) = rest(:,4:6)  + coeff(count,2)*B3_der(n1,k1,n2,k2,n3,k3,dataIn);
                    rest(:,7:9) = rest(:,7:9)  + coeff(count,3)*B3_der(n1,k1,n2,k2,n3,k3,dataIn);
                    count= count +1;
               end
           end
       end        
    end

    function [rest] = B2_der_eval(n1,n2,coeff,dataIn)
       count =1;
       [a,b] = size(dataIn);
       rest = zeros(a,b+2);
       for k1 = 0:n1
           for k2 = 0:n2
               rest(:,1:2) = rest(:,1:2)  + coeff(count,1)*B2_der(n1,k1,n2,k2,dataIn);
               rest(:,3:4) = rest(:,3:4)  + coeff(count,2)*B2_der(n1,k1,n2,k2,dataIn);
               count= count +1;
           end
       end
    end

    function [res] = B(n,k,data)
        % assumed data varies from -1 to 1
        if n<k || k<0
            res = zeros(size(data));
            return
        end
        data = (data+1)/2;
        res = nchoosek(n,k)*((data.^k).*((1-data).^(n-k)));
    end

    function [res] = B_der(n,k,data)
        res = n*(B(n-1,k-1,data) - B(n-1,k,data))/2;
    end
    

end