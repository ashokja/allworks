function [Npts] = FindLargestCompVIZ(pts,id)
            resolution = 30;
    
    ptsKp = pts;
    spts = max(size(pts));
    pts = (pts +1)/2*resolution -0.0001;
    pts = floor(pts)+1;
    
    [val,idx] = min(pts(:));

    if(val == 0)
        pts(pts==0)=1;
    end
    
    Img = zeros(resolution,resolution,resolution);
    CImg = Img;
    
    for i = 1:spts
        p = pts(i,:);
        Img(p(1),p(2),p(3)) =1;
    end

    
    % Largest Component.
    CC = bwconncomp(Img);
    numPixels = cellfun(@numel,CC.PixelIdxList);
    
    [~,idx] = max(numPixels);
    CImg(CC.PixelIdxList{id}) = 1;
    % End largest Component code.
    
    % Need to change this into finding center tube.
    
    
    count = 1;
    for i = 1:spts
        p = pts(i,:);
        if CImg(p(1),p(2),p(3))==1
            Npts(count,:) = ptsKp(i,:);
        else
            Npts(count,:) = [1,1,1];
        end
        count= count+1;
    end

end
