% This function calls Evaluate to calculate basis function.

function [C_out] = EvalBasis(X,d,n,basis)

    C_alpha = CreateAlpha(d+1,n);
    [num_C_alpha,de] = size(C_alpha);
    %num_C_alpha
    for i = 1:num_C_alpha
        subsCell = num2cell(C_alpha(i,1:end-1)+1);
        if (i ==basis )
            C_out(subsCell{:}) = 1;
        else
            C_out(subsCell{:}) = 0;
        end
    end
    %C_out
    C_out = Evaluate(C_out,X,d,n);
end
