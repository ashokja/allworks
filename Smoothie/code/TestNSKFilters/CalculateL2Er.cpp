#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include <MultiRegions/DisContField2D.h>
#include <math.h>
#include <boost/timer.hpp>
#include "MetricTensor.h"

#include <iomanip> // std::setprecision
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
#define TOLERENCE_VIZ 1e-6

using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 6)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg is conditions file" << endl;
		cout << "3rd arg polynomial degree filter you want to apply" << endl;
		cout << "4th arg Type of L-SIAC filter \t" << endl <<  
								"\t \t \t 1-LSIAC-MAxElm"<< endl <<
								"\t \t \t 2-LSIAC-AdaptiveL" << endl <<
								"\t \t \t 3 LSIAC-Metric EIG1" << endl << 
								"\t \t \t 4 LSIAC-Metric EIG2" << endl;
		cout << "5th arg m for scaling of LSIAC." << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM2D->LoadMesh(var[0]);
	string fname = vSession->GetSessionName();

	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	// Code to increase the quadrature
	SpatialDomains::MeshGraphSharedPtr graph2D =
            SpatialDomains::MeshGraph::Read(vSession);
    const SpatialDomains::ExpansionMap &expansions = graph2D->GetExpansions();
     LibUtilities::BasisKey              bkey0      =   
           expansions.begin()->second->m_basisKeyVector[0];
    cout <<"OldNumModes"<<bkey0.GetNumModes() << endl;
     int poly = bkey0.GetNumModes()-1;
    graph2D->SetExpansionsToPolyOrder( 2*poly+1+1 );

    MultiRegions::ExpListSharedPtr Exp_u, Exp_v;    
    Exp_u = MemoryManager<MultiRegions::DisContField2D>
            ::AllocateSharedPtr(vSession,graph2D,vSession->GetVariable(0));

	int New_tNquadPts = Exp_u->GetTotPoints();

    Array<OneD,NekDouble> U_raw(New_tNquadPts), U_dg(New_tNquadPts), U_siac(New_tNquadPts);
    Array<OneD,NekDouble> Nxc0(New_tNquadPts), Nxc1(New_tNquadPts), Nxc2(New_tNquadPts);
	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Exp_u->GetCoords(Nxc0,Nxc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			Vmath::Zero(New_tNquadPts,&Nxc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			Exp_u->GetCoords(Nxc0,Nxc1,Nxc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}


      // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);

	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]));
	}
	
	for (int i=0;  i< New_tNquadPts; i++)
	{
		U_raw[i] = std::cos(2.0*M_PI*( Nxc0[i]+Nxc1[i] ));
	}
	
	HNM2D->m_expansions[0]->FwdTrans(fce,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	ece = HNM2D->m_expansions[0]->GetPhys();
	HNM2D->m_Arrays.push_back(ece);
	HNM2D->LoadExpListIntoRTree();
    HNM2D->CalculateDynamicScaling();
    
	MetricTensor* metricT = new MetricTensor();
    metricT->LoadMetricTensor(HNM2D);
    metricT->CalculateMetricTensorAtNodes();

	Array<OneD,NekDouble> ece_Coeffs =  HNM2D->m_expansions[0]->GetCoeffs();

	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength(); //*sqrt(2.0);
	//NekDouble scaling = HNM2D->GetMeshLargestEdgeLength()*sqrt(2.0);
	
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[1+2]), scaling );
	SmoothieSIAC2D sm_NONSYM(SIACUtilities::eSYM_UNEVEN_2kp1, HNM2D, atoi(argv[1+2]), scaling );
    SmoothieSIAC2D sm_NONSYM_NSK2(SIACUtilities::eSYM_UNEVEN_2kp1, HNM2D, atoi(argv[1+2]), scaling );

	Array<OneD,NekDouble> direction(3,0.0);
	direction[0] = 1.0;//(sqrt(2.0));
	//direction[0] = 1.0/(sqrt(2.0));
	//direction[1] = 1.0/(sqrt(2.0));

    vector<int> elIds,glIds;
    HNM2D->IntersectWithBoxUsingRTree( 0.0+0.000625, 0.0+0.000625, -0.01,
                                        1.0-0.000625, 1.0-0.000625, 0.01 , elIds, glIds);
	cout << HNM2D->m_expansions[0]->GetExpSize()<< "\t" <<elIds.size() << endl;
	Array<OneD,NekDouble> lcoord(3,0.0);
	NekDouble valY,valZ;
	vector<NekDouble> stPoint(3);
	stPoint[0] = 0.0; stPoint[1] = 0.0; stPoint[2] = 0.0;
	vector<NekDouble> tparams, tvals;
	tparams.push_back(0.0);
	int numQpts = atoi(argv[1+3]);
	int numQpts_1Dpoly = 2*(atoi(argv[1+2])-1)+1;
	Array<OneD,NekDouble> eig(3,0.0);
	NekDouble lambda,dynScaling;
	NekDouble mu = atof(argv[5]);
	for (int i=0; i<elIds.size();i++)
	{
		int elmID = elIds[i];
		LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elmID);
		int phys_offset_old = HNM2D->m_expansions[0]->GetPhys_Offset(elmID);
		int phys_offset_new = Exp_u->GetPhys_Offset(elmID);
		Array<OneD,NekDouble> ece_phys_old = ece.CreateWithOffset(ece,phys_offset_old);
		for(int j=0; j< Exp_u->GetExp(0)->GetTotPoints();j++)
		{
			int index = j+phys_offset_new;
			lcoord[0] = Nxc0[index]; lcoord[1] = Nxc1[index];
			U_dg[index] = lexp->PhysEvaluate(lcoord,ece_phys_old);
			switch (numQpts)
			{
				case 1: // LSIAC with maximum edge length
					sm.EvaluateAt(Nxc0[index],Nxc1[index],0.0,U_siac[index],
								valY,valZ,direction,mu*scaling,0);
					break;
				case 2: // LSIAC with Adaptive length
					dynScaling = HNM2D->GetDynamicScaling(lcoord);
					sm.EvaluateAt(Nxc0[index],Nxc1[index],0.0,U_siac[index],
								valY,valZ,direction,mu*dynScaling,0);
					break;
				case 3: // LSIAC with metric Eig 1. 
					metricT->GetEigenPair(lcoord,elmID,1,lambda,eig);
					sm.EvaluateAt(Nxc0[index],Nxc1[index],0.0,U_siac[index],
								valY,valZ,eig,mu*lambda,0);
					break;
				case 4: // LSIAC with metric Eig 2. 
					metricT->GetEigenPair(lcoord,elmID,2,lambda,eig);
					sm.EvaluateAt(
								Nxc0[index],Nxc1[index],0.0,U_siac[index],
								valY,valZ,eig,mu*lambda,0);
					break;
				case 5: // LSIAC with IP Metric Eig 1.
					metricT->GetEigenPairUsingIP(lcoord,elmID,1,lambda,eig); 
					sm.EvaluateAt(Nxc0[index],Nxc1[index],0.0,U_siac[index],
								valY,valZ,eig,mu*lambda,0);
					break;
				case 6: // LSIAC with IP Metric Eig 2. 
					metricT->GetEigenPairUsingIP(lcoord,elmID,2,lambda,eig);
					sm.EvaluateAt(Nxc0[index],Nxc1[index],0.0,U_siac[index],
								valY,valZ,eig,mu*lambda,0);
					break;
				case 7:
					metricT->GetEigenPairAtTheta(lcoord, elmID,0, lambda, eig);
					sm.EvaluateAt(Nxc0[index],Nxc1[index],0.0,U_siac[index],
								valY,valZ,eig,mu*lambda,0);
					break;
				case 8:
					metricT->GetEigenPairAtTheta(lcoord, elmID,90, lambda, eig);
					sm.EvaluateAt(Nxc0[index],Nxc1[index],0.0,U_siac[index],
								valY,valZ,eig,mu*lambda,0);
					break;
				case 9:
					metricT->GetEigenPair(lcoord,elmID,1,lambda,eig);
					sm_NONSYM_NSK2.EvaluateAt_NSK_GivenFilterLength_v1(
								Nxc0[index],Nxc1[index],0.0,U_siac[index],
								valY,valZ,eig,mu*lambda,0);
					break;
				case 10:
					metricT->GetEigenPair(lcoord,elmID,2,lambda,eig);
					sm_NONSYM_NSK2.EvaluateAt_NSK_GivenFilterLength_v1(
								Nxc0[index],Nxc1[index],0.0,U_siac[index],
								valY,valZ,eig,mu*lambda,0);
					break;
				case 11:
					metricT->GetEigenPair(lcoord,elmID,1,lambda,eig);
            		sm_NONSYM.EvaluateAt_NSK_FixedNumBSpl(
								Nxc0[index],Nxc1[index],0.0,U_siac[index],
								valY,valZ,eig,mu*lambda,0);
					break;
				case 12:
					metricT->GetEigenPair(lcoord,elmID,2,lambda,eig);
            		sm_NONSYM.EvaluateAt_NSK_FixedNumBSpl(
								Nxc0[index],Nxc1[index],0.0,U_siac[index],
								valY,valZ,eig,mu*lambda,0);
					break;
			}
		}
	}


    NekDouble L2_SIAC=0.0, L2_DG=0.0, L2_SIAC2=0.0;
    NekDouble Linf_SIAC=0.0, Linf_DG=0.0, Linf_SIAC2=0.0;
    for(int i=0; i<elIds.size() ;i++)
    {
        int index = elIds[i];
        LocalRegions::ExpansionSharedPtr lexp = Exp_u->GetExp( index );
        int phys_offset_new = Exp_u->GetPhys_Offset(index);
		
        Array<OneD,NekDouble> el_VR = U_raw.CreateWithOffset( U_raw, phys_offset_new);
        Array<OneD,NekDouble> el_VD = U_dg.CreateWithOffset( U_dg, phys_offset_new);
        Array<OneD,NekDouble> el_VS = U_siac.CreateWithOffset( U_siac, phys_offset_new);
        L2_DG += ( lexp->L2(el_VR,el_VD)*lexp->L2(el_VR,el_VD)) ;
        L2_SIAC += ( lexp->L2(el_VR,el_VS)*lexp->L2(el_VR,el_VS));
        Linf_DG = std::max(Linf_DG,  lexp->Linf(el_VR,el_VD));
        Linf_SIAC =std::max(Linf_SIAC, lexp->Linf(el_VR,el_VS));
    }
    cout << "Error at selective places" << endl;
	
	cout << "Q for 1D poly :\t" << numQpts_1Dpoly << endl;
	cout << "Q:\t" << numQpts << endl;
    cout << "L2 error DG:\t" << std::sqrt(L2_DG);
    cout << "\t SIAC\t" <<  std::sqrt(L2_SIAC) << endl;;

    cout << "Linf error DG:\t" << Linf_DG ;
    cout << "\t SIAC\t" <<Linf_SIAC << endl;

	return 0;
}

