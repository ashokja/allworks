% first say at what points you want the function to be defined.
clear;
close all;

N = [3,5,10]
%for NN = N
NN = 5
tic    
    clearvars -except NN N
    NX =NN; NY=NN; NZ=NN;
    nx=NN;ny=NN;nz=NN;
    px =6*NN; py =6*NN; pz =6*NN;
    %QX = 8; QY = 9; QZ = 10;
    addpath ../createTestData
    addpath ../PolyLib
    % call test_run to get the function defined at that location.

    [XQ,WXQ] = JacobiGLZW(NX+nx,0,0);
    [YQ,WYQ] = JacobiGLZW(NY+ny,0,0);
    [ZQ,WZQ] = JacobiGLZW(NZ+nz,0,0);
    %%
%    NX = 28; NY = 28; NZ = 28; 
%    nx = 2; ny = 2; nz = 2;
    [X,Y,Z] = meshgrid(XQ,YQ,ZQ);

    X = permute(X,[2,1,3]); Y = permute(Y,[2,1,3]); Z = permute(Z,[2,1,3]);
    mapxy(:,1) = X(:); mapxy(:,2) = Y(:); mapxy(:,3) = Z(:);
    [xy,distance,t_a,mapxyV,curveCoeff] = test_runXYZ(1,mapxy);

    UAtGrid = reshape(mapxyV(:,1),[max(size(XQ)),max(size(YQ)),max(size(ZQ))]);
    VAtGrid = reshape(mapxyV(:,2),[max(size(XQ)),max(size(YQ)),max(size(ZQ))]);
    WAtGrid = reshape(mapxyV(:,3),[max(size(XQ)),max(size(YQ)),max(size(ZQ))]);

    coeffU = CalLegendreCoeff(UAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);
    coeffV = CalLegendreCoeff(VAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);
    coeffW = CalLegendreCoeff(WAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);

    [DChebX,xcheb] = cheb(NX+nx+px);
    [DChebY,ycheb] = cheb(NY+ny+py);
    [DChebZ,zcheb] = cheb(NZ+nz+pz);

    UAtCheb = CalValueAt(coeffU,xcheb,ycheb,zcheb);
    VAtCheb = CalValueAt(coeffV,xcheb,ycheb,zcheb);
    WAtCheb = CalValueAt(coeffW,xcheb,ycheb,zcheb);
    

    UAtCheb_DX = CalDerivative(UAtCheb,DChebX,'X');
    UAtCheb_DY = CalDerivative(UAtCheb,DChebY,'Y');
    UAtCheb_DZ = CalDerivative(UAtCheb,DChebZ,'Z');

    VAtCheb_DX = CalDerivative(VAtCheb,DChebX,'X');
    VAtCheb_DY = CalDerivative(VAtCheb,DChebY,'Y');
    VAtCheb_DZ = CalDerivative(VAtCheb,DChebZ,'Z');

    WAtCheb_DX = CalDerivative(WAtCheb,DChebX,'X');
    WAtCheb_DY = CalDerivative(WAtCheb,DChebY,'Y');
    WAtCheb_DZ = CalDerivative(WAtCheb,DChebZ,'Z');
    
    % Calculate Acceleration
    AccXatCheb = UAtCheb_DX.*UAtCheb + UAtCheb_DY.*VAtCheb + UAtCheb_DZ.*WAtCheb;
    AccYatCheb = VAtCheb_DX.*UAtCheb + VAtCheb_DY.*VAtCheb + VAtCheb_DZ.*WAtCheb;
    AccZatCheb = WAtCheb_DX.*UAtCheb + WAtCheb_DY.*VAtCheb + WAtCheb_DZ.*WAtCheb;


    % Calcuate spatial Derivative D(Acc)/DS
    AccXatCheb_DX = CalDerivative(AccXatCheb,DChebX,'X');
    AccXatCheb_DY = CalDerivative(AccXatCheb,DChebY,'Y');
    AccXatCheb_DZ = CalDerivative(AccXatCheb,DChebZ,'Z');

    AccYatCheb_DX = CalDerivative(AccYatCheb,DChebX,'X');
    AccYatCheb_DY = CalDerivative(AccYatCheb,DChebY,'Y');
    AccYatCheb_DZ = CalDerivative(AccYatCheb,DChebZ,'Z');

    AccZatCheb_DX = CalDerivative(AccZatCheb,DChebX,'X');
    AccZatCheb_DY = CalDerivative(AccZatCheb,DChebY,'Y');
    AccZatCheb_DZ = CalDerivative(AccZatCheb,DChebZ,'Z');


    % Calculate biNormal
    BiNorXatCheb = AccXatCheb_DX.*UAtCheb + AccXatCheb_DY.*VAtCheb + AccXatCheb_DZ.*WAtCheb;
    BiNorYatCheb = AccYatCheb_DX.*UAtCheb + AccYatCheb_DY.*VAtCheb + AccYatCheb_DZ.*WAtCheb;
    BiNorZatCheb = AccZatCheb_DX.*UAtCheb + AccZatCheb_DY.*VAtCheb + AccZatCheb_DZ.*WAtCheb;

    % Calculate curvature
    vel(:,1) = UAtCheb(:);   vel(:,2) = VAtCheb(:);   vel(:,3) = WAtCheb(:); 
    acc(:,1) = AccXatCheb(:);acc(:,2) = AccYatCheb(:);acc(:,3) = AccZatCheb(:);
    bi(:,1)= BiNorXatCheb(:);bi(:,2)= BiNorYatCheb(:);bi(:,3)= BiNorZatCheb(:);

    v_cross_a = cross(vel,acc,2);
    divi_C = (sum(vel.^2,2)).^(3/2);
    cur = v_cross_a./[divi_C,divi_C,divi_C];
    %cur = 
    curXAtGrid = reshape(cur(:,1),[max(size(xcheb)),max(size(ycheb)),max(size(zcheb))]);
    curYAtGrid = reshape(cur(:,2),[max(size(xcheb)),max(size(ycheb)),max(size(zcheb))]);
    curZAtGrid = reshape(cur(:,3),[max(size(xcheb)),max(size(ycheb)),max(size(zcheb))]);

    %
    % calculate torsion.
    scalarTp = dot(v_cross_a,bi,2);
    divi_T = sum(v_cross_a.^2,2);
    tor_D = scalarTp./(divi_T+0.1);
    tor = reshape(tor_D,[max(size(xcheb)),max(size(ycheb)),max(size(zcheb))]);

    
%     %    % % Validation step
%     xvali = linspace(-1,1,11);
%     yvali = linspace(-1,1,11);
%     zvali = linspace(-1,1,11);
% 
%     clear mapxy;
%     [X,Y,Z] = meshgrid(xvali,yvali,zvali);
%     X = permute(X,[2,1,3]); Y = permute(Y,[2,1,3]); Z = permute(Z,[2,1,3]);
%     mapxy(:,1) = X(:); mapxy(:,2) = Y(:); mapxy(:,3) = Z(:);
%     [xy,distance,t_a,mapxyV,curveCoeff] = test_runXYZ(1,mapxy);
%     UAtVali = CalValueAt(coeffU,xvali,yvali,zvali);
%     VAtVali = CalValueAt(coeffV,xvali,yvali,zvali);
%     WAtVali = CalValueAt(coeffW,xvali,yvali,zvali);
%     mapxyV_Vali(:,1) = UAtVali(:); mapxyV_Vali(:,2) = VAtVali(:); mapxyV_Vali(:,3) = WAtVali(:);
%     er_cheb = sum(abs(mapxyV(:) - mapxyV_Vali(:)))


    f=1;
    f1 = figure(f); f=f+1;
    VizIsoSurface(xcheb,ycheb,zcheb,UAtCheb,VAtCheb,WAtCheb);hold on;
    Plot3DCurve(curveCoeff); hold on; 
    title('norm(Velocity)')
    %figure(f); f =f+1;
    %quiver3(X,Y,Z,UAtCheb,VAtCheb,WAtCheb);
    %VizIsoSurface(xcheb,ycheb,zcheb,UAtCheb,VAtCheb,WAtCheb);hold on;


    f2= figure(f); f = f+1;
    VizIsoSurface(xcheb,ycheb,zcheb,AccXatCheb,AccYatCheb,AccZatCheb);hold on;
    Plot3DCurve(curveCoeff); title('norm(Acceleration)');

    f3 = figure(f); f= f+1;
    VizIsoSurface(xcheb,ycheb,zcheb,BiNorXatCheb,BiNorYatCheb,BiNorZatCheb);hold on;
    Plot3DCurve(curveCoeff); title('norm(Binormal)');

    f4 = figure(4);
    VizIsoSurface(xcheb,ycheb,zcheb,curXAtGrid,curYAtGrid,curZAtGrid);hold on;
    Plot3DCurve(curveCoeff); title('norm(Curvature)');
 
    f5 = figure(5);
    VizIsoSurface(xcheb,ycheb,zcheb,tor,tor,tor);hold on;
    Plot3DCurve(curveCoeff); title('Torsion');
    
   
    
%     savefig(f1,strcat('images/vel_B',int2str(NN)));
%     savefig(f2,strcat('images/acc_B',int2str(NN)));
%     savefig(f3,strcat('images/binor_B',int2str(NN)));
%     savefig(f4,strcat('images/cur_B',int2str(NN)));
%     savefig(f5,strcat('images/tor_B',int2str(NN)));
%      close all;
%      save(strcat('testdata/data_matB',int2str(NN),'_.mat'));
   toc
%end
%%
% print('-f1','1Vel_15','-djpeg');
% print('-f2','2Acc_15','-djpeg');
% print('-f3','3binor_b2','-djpeg');
% print('-f4','4cur_b2','-djpeg');
% print('-f5','5tor_b2','-djpeg');

