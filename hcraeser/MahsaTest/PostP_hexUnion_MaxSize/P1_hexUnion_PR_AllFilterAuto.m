% create file names.

MR = [15,29,57,113];
P = '1';
KF ='0';
UF = '0';
pfilename = 'hex_test_unionjack_periodic_quadrature_points_projection_MAXSIZE_MR';
addpath('..');

Deg=1;
ord1 = 672;
ord2 = 2688;
ord3 = 10752;
ord4 = 43008;

KF ='0';
count =1;
for mr = MR
    filename = strcat(pfilename,num2str(mr),'_P',P,'_KF',KF,'_UF',UF,'.txt');
    C(count,:) = ReadFile(filename);
    count= count+1;
end


L2B_ord = C(:,1);
L2_ord = C(:,2);
LinfB_ord = C(:,3);
Linf_ord = C(:,4);
