function [Npts] = FindLargestCompTor(pts,resolution)
    switch nargin
        case 0,
            pts = rand(10,3)*2-1;            
            resolution = 10;
        case 1,
            resolution = 10;
    end
    
    ptsKp = pts;
    spts = max(size(pts));
    pts = (pts +1)/2*resolution -0.0001;
    pts = floor(pts)+1;
    
    [val,idx] = min(pts(:));

    if(val == 0)
        pts(pts==0)=1;
    end
    
    Img = zeros(resolution,resolution,resolution);
    CImg = Img;
    
    for i = 1:spts
        p = pts(i,:);
        Img(p(1),p(2),p(3)) =1;
    end

%     % Largest Component.
%     CC = bwconncomp(Img);
%     numPixels = cellfun(@numel,CC.PixelIdxList);
%     
%     [~,idx] = max(numPixels);
%     CImg(CC.PixelIdxList{idx}) = 1;
% 
%     % End largest Component code.
    
    %Instead of picking largest commponent. I want to pick the closest from
    %center.
    
    %Find Avg center point closest to center line.
%     
%     % testcase
%     Img = imread('text.png');
%     CC = bwconncomp(Img);
%     resolution = 256;
%     CImg = zeros(size(Img));
%     %testcase;
    
    CC = bwconncomp(Img);    
    s_IdxList = max(size(CC.PixelIdxList));
    resb2 = resolution/2;
    CCdist = zeros(s_IdxList,1);
    for idx = 1:s_IdxList
        sumCount = 0;
        count =1;
        IdList = CC.PixelIdxList{idx};
        for j = 1:max(size(IdList))
            [ix,iy,iz] = ind2sub(size(Img),IdList(j));
            sumCount = sumCount +abs(ix-resb2) + abs(iy-resb2) + abs(iz-resb2);
            count = count +1;
        end
        CCdist(idx) = sumCount/count;
    end
    [~,idx] = min(CCdist);
    CImg(CC.PixelIdxList{idx}) = 1;
    % Need to change this into finding center tube.
  

    count = 1;
    for i = 1:spts
        p = pts(i,:);
        if CImg(p(1),p(2),p(3))==1
            Npts(count,:) = ptsKp(i,:);
            count= count+1;
        end
    end

end
