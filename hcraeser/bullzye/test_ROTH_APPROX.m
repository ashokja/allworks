% For testing polynomail approximation of actual Roth function.

clear;
close all;

load('testData/jacX');
load('testData/jacWX');

va = -70
vb = 10
N = [4:1:9]

for NN = 5
    for    n = 3;
    tic    
    %    clearvars -except NN N
        NX =NN; NY=NN; NZ=NN;
        nx=n;ny=n;nz=n;
        px =6; py =6; pz =6;
        %QX = 8; QY = 9; QZ = 10
        addpath ../createTestData
        addpath ../PolyLib
        % call test_run to get the function defined at that location.

            XQ = XQN{NX+2+nx}; WXQ = WXQN{NX+2+nx};
            YQ = XQN{NY+2+ny}; WYQ = WXQN{NY+2+ny};
            ZQ = XQN{NZ+2+nz}; WZQ = WXQN{NZ+2+nz};

        [X,Y,Z] = meshgrid(XQ,YQ,ZQ);
        X = permute(X,[2,1,3]); Y = permute(Y,[2,1,3]); Z = permute(Z,[2,1,3]);
    %    [xy,distance,t_a,mapxyV,curveCoeff] = test_runXYZ(1,mapxy);
        [UAtGrid,VAtGrid,WAtGrid] = testRoth(XQ,YQ,ZQ);

        coeffU = CalLegendreCoeff(UAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);
        coeffV = CalLegendreCoeff(VAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);
        coeffW = CalLegendreCoeff(WAtGrid,NX,NY,NZ,nx,ny,nz,XQ,WXQ,YQ,WYQ,ZQ,WZQ);

        [DChebX,xcheb] = cheb(px);
        [DChebY,ycheb] = cheb(py);
        [DChebZ,zcheb] = cheb(pz);

        UAtCheb = CalValueAt(coeffU,xcheb,ycheb,zcheb);
        VAtCheb = CalValueAt(coeffV,xcheb,ycheb,zcheb);
        WAtCheb = CalValueAt(coeffW,xcheb,ycheb,zcheb);

    %    [XC,YC,ZC] = meshgrid(xcheb,ycheb,zcheb);
    %    XC = permute(XC,[2,1,3]); YC = permute(YC,[2,1,3]); ZC = permute(ZC,[2,1,3]);

        
    
        [UCGrid,VCGrid,WCGrid] = testRoth(xcheb,ycheb,zcheb);
        
%        VizIsoSurface(xcheb,ycheb,zcheb, UCGrid,VCGrid,WCGrid); hold on;
        data = linspace(0,1,50);
        xd = cos(data*pi/2)-1; yd = sin(data*pi/2)-1; zd = zeros(size(data));
        plot3(xd,yd,zd,'r');
        Cdata = [xd;yd;zd];
        
        UVAL = CalValueAtPts(coeffU,Cdata');
        VVAL = CalValueAtPts(coeffV,Cdata');
        WVAL = CalValueAtPts(coeffW,Cdata');
        
        Vel = testRothSerialData(Cdata);
        
        Uer = abs(UVAL - Vel(1,:)');
        Ver = abs(VVAL - Vel(2,:)');
        Wer = abs(WVAL - Vel(3,:)');
        
%         Uer = abs(UAtCheb - UCGrid);
%         Ver = abs(VAtCheb - VCGrid);
%         Wer = abs(WAtCheb - WCGrid);
        
        er_UMax(NN,n) = max(Uer(:));
        er_VMax(NN,n) = max(Ver(:));
        er_WMax(NN,n) = max(Wer(:));

        er_U(NN,n) = sum(Uer(:))/10^(3);
        er_V(NN,n) = sum(Ver(:))/10^(3);
        er_W(NN,n) = sum(Wer(:))/10^(3);
        
        er_Max(NN,n) = max([er_UMax(NN,n),er_VMax(NN,n),er_WMax(NN,n)]);
        
    toc
    end
end
% 
% erMaxPls = er_Max;
% % 
%  %%
% % 
% er_Max = log10(erMaxPls);
% figure(1)
% plot(4+(1:10),er_Max(4,:),...
%         5+(1:10),er_Max(5,:),...
%         6+(1:10),er_Max(6,:),...
%         7+(1:10),er_Max(7,:),...
%         8+(1:10),er_Max(8,:),...
%         9+(1:10),er_Max(9,:) );
% %     ,...
% %         10+(1:10),er_Max(10,:),...
% %         11+(1:10),er_Max(11,:),...
% %         12+(1:10),er_Max(12,:),...
% %         13+(1:10),er_Max(13,:)...
% %     );
% 
%     xlabel('x =Total quadraturePoints');
%     ylabel('log10(Max Error)')
%     legend('Deg4','Deg5','Deg6', 'Deg7','Deg8','Deg9', 'Deg10','Deg11','Deg12','Deg13');
%     text(5,er_Max(4,1),' Deg4');
%     text(6,er_Max(5,1),' Deg5');
%     text(7,er_Max(6,1),' Deg6');
%     text(8,er_Max(7,1),' Deg7');
%     text(9,er_Max(8,1),' Deg8');
%     text(10,er_Max(9,1),' Deg9');
%     text(11,er_Max(10,1),' Deg10');
%     text(12,er_Max(11,1),' Deg11');
%     text(13,er_Max(12,1),' Deg12');
%     text(14,er_Max(13,1),' Deg13');
% 
%     title('log10(Max Projection Error) for Roth bent vortex)')
%     
% %   


% figure(2)
% plot(1:10,er_L2(2,:),...
%         1:10,er_L2(2,:),...
%         1:10,er_L2(3,:),...
%         1:10,er_L2(4,:),...
%         1:10,er_L2(5,:),...
%         1:10,er_L2(6,:),...
%         1:10,er_L2(7,:),...
%         1:10,er_L2(8,:),...
%         1:10,er_L2(9,:),...
%         1:10,er_L2(10,:)...
%     );
% xlabel('x = Total Points - Polynomial Degree');
% ylabel('Average(L2) Error')
%     legend('Deg2','Deg3','Deg4','Deg5','Deg6', 'Deg7','Deg8','Deg9', 'Deg10');
%     text(1,er_L2(2,1),' Deg2');
%     text(1,er_L2(3,1),' Deg3');
%     text(1,er_L2(4,1),' Deg4');
%     text(1,er_L2(5,1),' Deg5');
%     text(1,er_L2(6,1),' Deg6');
%     text(1,er_L2(7,1),' Deg7');
%     text(1,er_L2(8,1),' Deg8');
%     text(1,er_L2(9,1),' Deg9');
%     text(1,er_L2(10,1),' Deg10');
%     title('Avg Error vs (x = Total Points - Polynomial Degree)')
