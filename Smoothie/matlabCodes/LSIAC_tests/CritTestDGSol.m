% test DG solution.

% Read xc0 and xc1 from file.
% Write u and v to file.

close all;
clear all;
a1 = 0.74+0.35i;
a2 = 0.68-0.59i;
a3 =-0.11-0.72i;
b1 =-0.58+0.64i;
b2 = 0.51-0.27i;
b3 =-0.12+0.84i;


rate = 2;
filename = strcat('P',int2str(rate),'_quad_simple_80');

xfname = strcat('../data/NekMeshes/',filename,'_xc0','.txt' );
yfname = strcat('../data/NekMeshes/',filename,'_xc1','.txt' );

uDGfname = strcat('../data/NekMeshes/',filename,'_u_DG','.txt' );
vDGfname = strcat('../data/NekMeshes/',filename,'_v_DG','.txt' );

xV = textread(xfname, '%f\t');
yV = textread(yfname, '%f\t');
u_DG = textread(uDGfname, '%f\t');
v_DG = textread(vDGfname, '%f\t');

xV = 2.0*xV-1;
yV = 2.0*yV-1;

zV = xV+yV*1i;
zVb = xV-yV*1i;


Val = (zV-a1).*(zV-a2).*(zV-a3).*(zVb-b1).*(zVb-b2).*(zVb-b3).^2;

u = real(Val); 
v = -1*imag(Val);



figure(3)
plot3(xV,yV, abs(u-u_DG));
figure(4)
plot3(xV,yV, abs(v-v_DG));

L2_norm = max((sum((u-u_DG).^2)))/max(size(u(:)));
disp(L2_norm);

%  n_uv = sqrt(u.*u+v.*v);
%  u = u./n_uv;
%  v = v./n_uv;
% 
%  n_uvDG = sqrt(u_DG.*u_DG+v_DG.*v_DG);
%  u_DG = u_DG./n_uvDG;
%  v_DG = v_DG./n_uvDG;
% 
%  
% figure(1),
% quiver(xV,yV,u,v);
% figure(2)
% quiver(xV,yV,u_DG,v_DG);

%dlmwrite(strcat('../data/NekMeshes/',filename,'_u.txt'),u','delimiter','\t','precision',25);
%dlmwrite(strcat('../data/NekMeshes/',filename,'_v.txt'),v','delimiter','\t','precision',25);

%plot3(xV,yV,u);