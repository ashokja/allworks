function [] = drawOnlyCurve(mapxy,mapxyV,ywidth,sx,sy,sz)
    switch nargin
        case 2,
            ywidth = 1;
            startx = (-1:0.5:1);
            startz = (-1:0.5:1);
            starty = -1;
    [sx,sy,sz] = meshgrid(startx,starty,startz);

    end
    [a,~] = size(mapxy);
    b = int64(a^(1/3));
    x = reshape(mapxy(:,1),b,b,b);
    y = reshape(mapxy(:,2),b,b,b);
    z = reshape(mapxy(:,3),b,b,b);
    u = reshape(mapxyV(:,1),b,b,b);
    v = reshape(mapxyV(:,2),b,b,b);
    w = reshape(mapxyV(:,3),b,b,b);
%     xmin = min(x(:)); xmax = max(x(:));
%     ymin = min(y(:)); ymax = max(y(:));
%     zmin = min(z(:)); ymax = max(z(:));
%     
%     zmin = min(z(:));
%    wind_speed = sqrt(u.^2 + v.^2 + w.^2);
%     hsurfaces = slice(x,y,z,wind_speed,xmax,(ymin:ywidth:ymax),zmin);
%     set(hsurfaces,'FaceColor','interp','EdgeColor','none')
%     colormap jet
    
%     hcont = contourslice(x,y,z,wind_speed,xmax,(ymin:ywidth:ymax),zmin);
%     set(hcont,'EdgeColor',[0.7 0.7 0.7],'LineWidth',0.5)
%     
%     colorbar;

    hlines = streamline(x,y,z,u,v,w,sx(:),sy(:),sz(:));
    set(hlines,'LineWidth',2,'Color','cyan');hold on;
    plot3(sx(:),sy(:),sz(:),'r*'); hold on;
    view([-70,30])
%     p = patch(isosurface(x,y,z,wind_speed,0.21));
%     isonormals(x,y,z,v,p)
%     p.FaceColor = 'red';
%     p.EdgeColor = 'none';
%     daspect([1,1,1])
%     view(3); axis tight
%     camlight 
%     lighting gouraud
%     xlabel('x');
%     ylabel('y');
%     zlabel('z');
%     
end