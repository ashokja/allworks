function res = IntBernNew1D(evalB,coeff,X)
   numInter = 1;
   for i = 1:numInter
       %fhm = evalB(1,coeff,(m(X) ));
       %Dfhm = evalB(2,coeff,X);
       %opfhDHm = op('/',fhm,Dfhm);
       N = op('-',m(X),op('/',evalB(1,coeff,(m(X) )) ,evalB(2,coeff,X)) );
       X = rInt(X,N);
       if (isnan(X))
          break;
       end
   end
    res = X;
end