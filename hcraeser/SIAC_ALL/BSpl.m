function v_eval = BSpl(x_eval,k,j,T)
    switch nargin
        case 0
            x_eval = -1:.1:1;
            k = 0;
            j = 0;
            T = -1:1:1;
    end
    %matlab format
    j = j+1;
    
    if 0==k
        % 1 for t_j<=x<t_j+1
        if j >=1 && j<=max(size(T))-1
            v_eval = ( (x_eval >= T(j)) & (x_eval <T(j+1)) );
        else
            v_eval = zeros(size(x_eval));
            disp('T is wrong. Should not enter here.')
        end
%     elseif 1==k % case k=2 creates an interesting problem at x =0.
%         v_eval = zeros(size(x_eval));
%         I = find(abs(x_eval)<=1);
%         v_eval(I) = 1-abs(x_eval(I));
    else % for k >=1
%        v_eval = 1/k.*(((k+1)/2+x_eval).*BSpl_central(x_eval+1/2,k-1) + ...
%             ((k+1)/2-x_eval).*BSpl_central(x_eval-1/2,k-1) );
        if ( abs(T(j+k)-T(j) <1e-8 ))
            w_jlt = 0.0;
        else
            w_jlt = ( x_eval - T(j) )./(T(j+k)-T(j));
        end
   % w_jlt
    
        if ((T(j+1+k)-T(j+1) < 1e-8))
            w_j1lt =0.0;
        else
            w_j1lt = ( x_eval - T(j+1) )./(T(j+1+k)-T(j+1));
        end 
  %  w_j1lt
    
        v_eval = w_jlt.*BSpl(x_eval,k-1,j-1,T) + (1-w_j1lt).*BSpl(x_eval,k-1,j,T);
         
    end
    
end