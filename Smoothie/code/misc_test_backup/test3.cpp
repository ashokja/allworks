#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC1D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh1D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

 
int main(int argc, char* argv[])
{
	cout << "This is test case 3. To test error at arbitrary points." << endl;

	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM1D = new HandleNekMesh1D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM1D->LoadData(vSession->GetSessionName()+".fld", var);
	Array<OneD,NekDouble> direction(3,0.0);


	SmoothieSIAC1D sm(SIACUtilities::eSYM_2kp1, HNM1D, 2, 0.5); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	for (NekDouble x=0.1; x<5; x+=0.1)
	{
		cout << "x: "<<x ; 	
		sm.EvaluateAt(x,1.0,0.0,valX,valY,valZ);
		pos_x.push_back(x);
		values_x.push_back(valX);
	}
	
	NektarBaseClass k;	
	k.printNekArray(pos_x,0);
	k.printNekArray(values_x,0);

//	HandleNekMesh* mshHandle; 
//	SmoothieSIAC1D( eSYM_2kp1,mshHandle, 2);
	return 0;
}

void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
