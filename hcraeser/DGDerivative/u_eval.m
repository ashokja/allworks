function [val] = u_eval( x_p, v_x, u_hat)
	addpath('../PolyLib');
	% find which element x_p is present in ?
	if (x_p < v_x(1) || x_p >v_x(end))
		('invalid Input')
		val = NaN;
		return;
	end
	Nv = max(size(v_x));
	Ne = Nv-1;
	el = 0;
	for e=1:Ne
		if ( v_x(e+1) >= x_p && v_x(e) <= x_p)
			el =e;
			break;
		end
	end
	
	% Found the element. Use the element coeffecient to evalutate the value.
	u_coeff = u_hat(e,:);
	val =0;
	epi = 2*(x_p - v_x(el))/(v_x(el+1)-v_x(el)) -1;

	for c = 1:max(size(u_coeff))
		val =val+ u_coeff(c)*JacobiPoly(c-1,epi,0,0);
	end
	
end
