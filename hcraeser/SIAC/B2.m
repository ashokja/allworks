function v = B2(x)
    v = zeros(size(x));
    I = find(abs(x)<=1);
    v(I) = 1-abs(x(I));
return
