clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [10];

poly =2;
order = poly;
count =1;
alpha =2;


for r = rates
    fileName = strcat('P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha) );
    xfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha),'_pX_3D_OneSided2kp1.txt' );
    yfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha),'_pY_3D_OneSided2kp1.txt' );
    zfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha),'_pZ_3D_OneSided2kp1.txt' );
    ffname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha),'_pV_3D_OneSided2kp1.txt' );
    efname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha),'_pP_3D_OneSided2kp1.txt' );
    sfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha),'_pS_3D_OneSided2kp1.txt' );

    fDxfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha),'_pVDx_3D_OneSided2kp1.txt' );
    eDxfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha),'_pPDx_3D_OneSided2kp1.txt' );
    sDxfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha),'_pSDx_3D_OneSided2kp1.txt' );
        
    
    fDyfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha),'_pVDy_3D_OneSided2kp1.txt' );
    eDyfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha),'_pPDy_3D_OneSided2kp1.txt' );
    sDyfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha),'_pSDy_3D_OneSided2kp1.txt' );
   
    fDzfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha),'_pVDz_3D_OneSided2kp1.txt' );
    eDzfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha),'_pPDz_3D_OneSided2kp1.txt' );
    sDzfname = strcat('datatxt/P',int2str(poly),'_CubeGrid_',int2str(r),'_',int2str(order),'_D_',int2str(alpha),'_pSDz_3D_OneSided2kp1.txt' );
    
    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');
    zpos = textread(zfname, '%f\t');
    
    fval = textread(ffname, '%f\t');
    pval = textread(efname, '%f\t');
    sval = textread(sfname, '%f\t'); 
    
    fDxval = textread(fDxfname, '%f\t');
    pDxval = textread(eDxfname, '%f\t');
    sDxval = textread(sDxfname, '%f\t'); 
    
    fDyval = textread(fDyfname, '%f\t');
    pDyval = textread(eDyfname, '%f\t');
    sDyval = textread(sDyfname, '%f\t'); 
    
    fDzval = textread(fDzfname, '%f\t');
    pDzval = textread(eDzfname, '%f\t');
    sDzval = textread(sDzfname, '%f\t'); 
    
    
    s = floor( (max(size(xpos)))^(1/3) +0.1);
    
    xVpos = reshape(xpos,s,s,s);
    xVpos = permute(xVpos,[2,3,1]);
    yVpos = reshape(ypos,s,s,s);
    yVpos = permute(yVpos,[2,3,1]);
    zVpos = reshape(zpos,s,s,s);
    zVpos = permute(zVpos,[2,3,1]);
    
    fVval = reshape(fval,s,s,s);
    pVval = reshape(pval,s,s,s);
    sVval = reshape(sval,s,s,s); 
    sVval = permute(sVval,[2,3,1]);
    
    fDxVval = reshape(fDxval,s,s,s);
    pDxVval = reshape(pDxval,s,s,s);
    sDxVval = reshape(sDxval,s,s,s); 
    sDxVval = permute(sDxVval,[2,3,1]);
    
    fDyVval = reshape(fDyval,s,s,s);
    pDyVval = reshape(pDyval,s,s,s);
    sDyVval = reshape(sDyval,s,s,s); 
    sDyVval = permute(sDyVval,[2,3,1]);
    
    fDzVval = reshape(fDzval,s,s,s);
    pDzVval = reshape(pDzval,s,s,s);
    sDzVval = reshape(sDzval,s,s,s); 
    sDzVval = permute(sDzVval,[2,3,1]);

    
    figure,
    hsurfaces = slice(xVpos,yVpos,zVpos,fDyVval,[0,0.5,1],[0,1],[0,1]);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold off; caxis([-4*pi*pi,4*pi*pi]);
    title('Actual Function.')
    saveas(1,strcat(fileName,'A_v1'),'jpg');
    
    
    figure,
    hsurfaces = slice(xVpos,yVpos,zVpos,pDyVval,[0,0.5,1],[0,1],[0,1]);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold off; caxis([-4*pi*pi,4*pi*pi])
    title('derivative of Projected Function.')
    saveas(2,strcat(fileName,'P_v1'),'jpg');
    
    figure,
    hsurfaces = slice(xVpos,yVpos,zVpos,sDyVval,[0,0.5,1],[0,1],[0,1]);
    set(hsurfaces,'FaceColor','interp','EdgeColor','none')
    xlabel('x'); ylabel('y'); zlabel('z');
    colormap jet; colorbar; hold off; caxis([-4*pi*pi,4*pi*pi])
    title('SIAC derivative')
    saveas(3,strcat(fileName,'S_v1'),'jpg');
    
%     
%     figure,
%     hsurfaces = slice(yVpos,zVpos,xVpos,fVval,[0,0.5,1],1,0);
%     set(hsurfaces,'FaceColor','interp','EdgeColor','none')
%     xlabel('x'); ylabel('y'); zlabel('z');
%     colormap jet; colorbar; hold off;
%     
%     
%     figure,
%     hsurfaces = slice(yVpos,zVpos,xVpos,pVval,[0,0.5,1],1,0);
%     set(hsurfaces,'FaceColor','interp','EdgeColor','none')
%     xlabel('x'); ylabel('y'); zlabel('z');
%     colormap jet; colorbar; hold off;
%     
%     figure,
%     hsurfaces = slice(yVpos,zVpos,xVpos,sVval,[0,0.5,1],1,0);
%     set(hsurfaces,'FaceColor','interp','EdgeColor','none')
%     xlabel('x'); ylabel('y'); zlabel('z');
%     colormap jet; colorbar; hold off;

end
