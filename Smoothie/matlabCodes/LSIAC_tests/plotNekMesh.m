% to load and draw nekmesh.

function [result] = plotNekMesh(filename)
if nargin ==0
    filename = '../../data/CylExamples/Cy2l.xml';
end

disp(filename);

DOMnode = xmlread(filename);


%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.2;      % LineWidth
msz = 8;       % MarkerSize
atX = '5p8';



%For Vertices.
count =0;
allListitems = DOMnode.getElementsByTagName('VERTEX');
for k = 0:allListitems.getLength-1
   thisListitem = allListitems.item(k);
   thisList = thisListitem.getElementsByTagName('V');
   for ki = 0: thisList.getLength-1
       thisElement = thisList.item(ki);
       theAttributes = thisElement.getAttributes;
       vertexid = str2num(theAttributes.item(0).getValue);
       str = char(thisElement.getFirstChild.getData);
       vertexP = sscanf(str ,'%f %f %f');
%       thisElement.getData
        count = count +1;
        VerIds(count) = vertexid;
        Vertices(count,:) = vertexP;
   end
end


%For Edges.

%For Vertices.
count =0;
allListitems = DOMnode.getElementsByTagName('EDGE');
for k = 0:allListitems.getLength-1
   thisListitem = allListitems.item(k);
   thisList = thisListitem.getElementsByTagName('E');
   for ki = 0: thisList.getLength-1
       thisElement = thisList.item(ki);
       theAttributes = thisElement.getAttributes;
       vertexid = str2num(theAttributes.item(0).getValue);
       str = char(thisElement.getFirstChild.getData);
       vertexP = sscanf(str ,'%d %d');
%       thisElement.getData
        count = count +1;
        EdgIds(count) = vertexid;
        Edges(count,:) = vertexP;
   end
end


numEdges = max(size(Edges));

%figure(1),
hold on;
%for 
for eachE = 1:numEdges
    vid1 = Edges(eachE,1)+1;
    vid2 = Edges(eachE,2)+1;
    
    vP1 = Vertices(vid1,:);
    vP2 = Vertices(vid2,:);
    
    line([vP1(1),vP2(1)], [vP1(2),vP2(2)],[32,32],'Color','k','LineStyle','-','LineWidth',lw); hold on; 
    %p  = patchline([vP1(1),vP2(1)],[vP1(2),vP2(2)],[9,9],'linestyle','-','edgecolor','k','linewidth',lw,'EdgeAlpha',0.2); hold on;
end

result =0;

end