#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>

#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include <MultiRegions/DisContField2D.h>


#include <iomanip>

#define MAX_ITER 10000
#define MAX_ITER_NI 100
#define TOLERENCE_S 1e-16
#define TOLERENCE_F 1e-7
#define TOLERENCE_P 1e-14
#define TOLERENCE_A 1e-14
#define TOLERENCE_NN 1e-8
#define ALPHAMAX 1.0

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>

using namespace std;

int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2st arg xml file." << endl;
		cout << "3rd degree to outputfld." << endl;
		//cout << "3nd arg polynomial degree filter you want to apply" << endl;
		//cout << "4rd arg meshscaling you want to use." << endl;
	//	cout << "4th Angle of SIAC derivative in deg -90 to 90 allowed" << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();

    SpatialDomains::MeshGraphSharedPtr graph2D =
            SpatialDomains::MeshGraph::Read(vSession);

	HNM2D->LoadData( fname+ "_1_small.fld",var);



    int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fPhys:" << tNquadPts<< endl;
	cout << "fCoeffs:" << HNM2D->m_expansions[0]->GetNcoeffs()<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);
	Array<OneD,NekDouble> u(tNquadPts);
	Array<OneD,NekDouble> v(tNquadPts);

    //  ----------------
    int poly = atoi(argv[3]);
    graph2D->SetExpansionsToPolyOrder( poly );
    MultiRegions::ExpListSharedPtr Exp_u;    
    Exp_u = MemoryManager<MultiRegions::DisContField2D>
            ::AllocateSharedPtr(vSession,graph2D,vSession->GetVariable(0));
	int New_tNquadPts = Exp_u->GetTotPoints();
    Array<OneD,NekDouble> SU_rawX(New_tNquadPts);
    Array<OneD,NekDouble> SU_rawY(New_tNquadPts);
    Array<OneD,NekDouble> SU_rawD(New_tNquadPts);
    Array<OneD,NekDouble> SU_rawAll(New_tNquadPts);
    Array<OneD,NekDouble> SU_rawXYD(New_tNquadPts);
    Array<OneD,NekDouble> SV_rawXYD(New_tNquadPts);
    Array<OneD,NekDouble> U_raw(New_tNquadPts);
    Array<OneD,NekDouble> V_raw(New_tNquadPts);
    Array<OneD,NekDouble> Nxc0(New_tNquadPts), Nxc1(New_tNquadPts), Nxc2(New_tNquadPts);
//  -------------------

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Exp_u->GetCoords(Nxc0,Nxc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}

	Array<OneD,NekDouble> u_DG = HNM2D->m_expansions[0]->GetPhys();	
	Array<OneD,NekDouble> v_DG = HNM2D->m_expansions[1]->GetPhys();	
    HNM2D->m_Arrays.push_back(u_DG);
    HNM2D->m_Arrays.push_back(v_DG);
	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();

	Array<OneD,NekDouble> coord(3,0.0);
    Array<OneD,NekDouble> directionX(3,0.0);
    Array<OneD,NekDouble> directionY(3,0.0);
    Array<OneD,NekDouble> directionD(3,0.0);
    directionX[0]=1.0;
    directionY[1]=1.0;
    directionD[0]=1.0/sqrt(2);
    directionD[1]=1.0/sqrt(2);
	vector<Array<OneD,NekDouble>> directions, directionsXYD;
	directions.push_back(directionD);
	directions.push_back(directionX); 
	directions.push_back(directionY); 

	directionsXYD.push_back(directionX);
	directionsXYD.push_back(directionY);
	directionsXYD.push_back(directionD);

 //   direction[0]=1.0;
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp2, HNM2D, 3,0.1 );
	NekDouble valY,valZ;
    for(int index=0;index<New_tNquadPts;index++)
    {
			coord[0]= Nxc0[index]; 
			coord[1] = Nxc1[index];
            int elId = HNM2D->GetExpansionIndexUsingRTree(coord);
			int phys_offset = HNM2D->m_expansions[0]->GetPhys_Offset(elId);
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp( elId );
			Array<OneD,NekDouble> el_u_phys = u_DG.CreateWithOffset( u_DG, phys_offset );
			Array<OneD,NekDouble> el_v_phys = v_DG.CreateWithOffset( v_DG, phys_offset );
			U_raw[index] = lexp->PhysEvaluate( coord, el_u_phys);
			V_raw[index] = lexp->PhysEvaluate( coord, el_v_phys);
            
            NekDouble dynScaling = HNM2D->GetDynamicScaling(coord);
//			sm.EvaluateAt(coord[0],coord[1],0.0,SU_rawX[index],valY,valZ,directionX, dynScaling ,0);
//			sm.EvaluateAt(coord[0],coord[1],0.0,SU_rawY[index],valY,valZ,directionY, dynScaling ,0);
//			sm.EvaluateAt(coord[0],coord[1],0.0,SU_rawD[index],valY,valZ,directionD, dynScaling ,0);
//			sm.EvaluateAt(coord[0],coord[1],0.0,SU_rawAll[index],valY,valZ,directions, dynScaling ,0);
			sm.EvaluateAt(coord[0],coord[1],0.0,SU_rawXYD[index],valY,valZ,directionsXYD, dynScaling ,0);
			sm.EvaluateAt(coord[0],coord[1],0.0,SV_rawXYD[index],valY,valZ,directionsXYD, dynScaling ,1);
    }

   
    // Also write a fld file for the dG projection.
	string out = vSession->GetSessionName() + "Best_NewPoly"+argv[3]+".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (Exp_u->GetFieldDefinitions()).at(0);
 
	Exp_u->FwdTrans_IterPerExp( U_raw,Exp_u->UpdateCoeffs());
    FieldDef[0]->m_fields.push_back(var[0]);
    Exp_u->AppendFieldData(FieldDef[0], FieldData[0]);
	
	Exp_u->FwdTrans_IterPerExp( V_raw,Exp_u->UpdateCoeffs());
    FieldDef[0]->m_fields.push_back(var[1]);
    Exp_u->AppendFieldData(FieldDef[0], FieldData[0]);
/*
	Exp_u->FwdTrans_IterPerExp( SU_rawX,Exp_u->UpdateCoeffs());
    FieldDef[0]->m_fields.push_back("LSIAC-X-ArtificialVisc");
    Exp_u->AppendFieldData(FieldDef[0], FieldData[0]);
	
	Exp_u->FwdTrans_IterPerExp( SU_rawY,Exp_u->UpdateCoeffs());
    FieldDef[0]->m_fields.push_back("LSIAC-Y-ArtificialVisc");
    Exp_u->AppendFieldData(FieldDef[0], FieldData[0]);

	Exp_u->FwdTrans_IterPerExp( SU_rawD,Exp_u->UpdateCoeffs());
    FieldDef[0]->m_fields.push_back("LSIAC-D-ArtificialVisc");
    Exp_u->AppendFieldData(FieldDef[0], FieldData[0]);
	Exp_u->FwdTrans_IterPerExp( SU_rawAll,Exp_u->UpdateCoeffs());
    FieldDef[0]->m_fields.push_back("LSIAC-DXY-ArtificialVisc");
    Exp_u->AppendFieldData(FieldDef[0], FieldData[0]);
	
*/
	string LSI= "LSIAC-XYD-";
	string LSI_V= "LSIAC-XYD-";
	LSI.append(var[0]);
	LSI_V.append(var[1]);
	Exp_u->FwdTrans_IterPerExp( SU_rawXYD,Exp_u->UpdateCoeffs());
    FieldDef[0]->m_fields.push_back(LSI);
    Exp_u->AppendFieldData(FieldDef[0], FieldData[0]);
	
	Exp_u->FwdTrans_IterPerExp( SV_rawXYD,Exp_u->UpdateCoeffs());
    FieldDef[0]->m_fields.push_back(LSI_V);
    Exp_u->AppendFieldData(FieldDef[0], FieldData[0]);
   	
	HNM2D->m_fld->Write(out, FieldDef, FieldData);

	return 0;
}
