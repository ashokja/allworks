clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [20,40,80]+1;

poly =2;
order = poly;
count =1;
for r = rates
    
    xfname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_xc0_OneSided2kp2.txt' );
    ffname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_fce_OneSided2kp2.txt' );
    efname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_ece_OneSided2kp2.txt' );
    sfname = strcat('datatxt/P',int2str(poly),'_SAMPLE1D_',int2str(r),'_',int2str(order),'_sce_OneSided2kp2.txt' );

    disp(xfname);
    xpos = textread(xfname, '%f\t');
    fval = textread(ffname, '%f\t');
    pval = textread(efname, '%f\t');
    sval = textread(sfname, '%f\t');

    figure(1)
    plot(xpos,log10(abs(fval-pval)),xpos,log10(abs(fval-sval)));
    
    figure(2)
    plot(xpos,log10(abs(fval-sval)),'DisplayName',strcat('No of ele:',int2str(r)));
    hold on;

    figure(3)
    plot(xpos,pval);
    
    p_avg_err(count) = sum(abs(fval-pval))/max(size(xpos));
    s_avg_err(count) = sum(abs(fval-sval))/max(size(xpos));
    
    p_max_err(count) = max(abs(fval-pval));
    s_max_err(count) = max(abs(fval-sval));
    count = count+1;
end

figure(1),
legend('show');
figure(2),
legend('show');
title(strcat('poly-',int2str(poly),' order-',int2str(order)));
xlabel('domain');
ylabel('Cos(2*pi*x)');
saveas(2,strcat('S2O3poly_',int2str(poly),'_order_',int2str(order)),'jpg');

% figure(3)
% plot(log10(rates),log10(p_avg_err),log10(rates),log10(s_avg_err));
% figure(4)
% plot(log10(rates),log10(p_max_err),log10(rates),log10(s_max_err));