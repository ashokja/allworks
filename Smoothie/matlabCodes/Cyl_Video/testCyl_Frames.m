% what is the file name used here.
% testCyl_Vort_Perf

clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [81];

poly =2;
order = 2;
alpha =1;
count =1;

fileName = 'Cy3l_Re500_';

spacingStrgs={'0.657'};

%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize
atX = '5p8';

Cyl_Max = 6.3;
Cyl_Min = -5.2571;

for i = 0:900
for count = 1:max(size(spacingStrgs))
%for    spacingStrg = spacingStrgs
    spacingStrg = char(spacingStrgs(count));
    disp(spacingStrg);
    r = 81;
    
    filenameFrPrint = strcat(fileName,sprintf('%05d',i));
    %xfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pX_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    %yfname = strcat('datatxt/',fileName,int2str(order),'_D_',int2str(alpha),'_pY_2D_OneSided2kp1',spacingStrg,int2str(r),'.txt' );
    
    pI_pfname = strcat('data_2/',fileName,int2str(order),'_pI_p_2D_VOR_Ser_',spacingStrg,int2str(r),'_',int2str(i),'.txt' );
    pI_fname = strcat('data_2/',fileName,int2str(order),'_pI_2D_VOR_Ser_',spacingStrg,int2str(r),'_',int2str(i),'.txt' );
    
    disp(pI_pfname);
    disp(pI_fname);
    disp(i);
    
    pI_pval = textread(pI_pfname, '%f\t');     
    pI_val = textread(pI_fname, '%f\t');
    
    s = sqrt(max(size(pI_val)));
    
    pI_pVal = reshape(pI_pval,s,s);     pI_Val = reshape(pI_val,s,s);
    
    
   figure(23)
   contour( -1.0*pI_Val,(Cyl_Min :.3: Cyl_Max),'LineWidth',lw ); 
   axis off;
   print(strcat('frames_2/',filenameFrPrint), '-djpeg','-r100');
   
   figure(24)
   contour( -1.0*pI_pVal,(Cyl_Min :.3: Cyl_Max),'LineWidth',lw ); 
   axis off;
   print(strcat('frames_DG_2/',filenameFrPrint), '-djpeg','-r100');
   
%   result(count,:,:) = pI_Val;
end
end

