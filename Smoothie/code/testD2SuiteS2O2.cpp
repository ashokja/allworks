#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		cout << "4th arg Resolution of output." << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM2D->LoadMesh(var[0]);
	HNM2D->LoadMesh(var[1]);
	HNM2D->LoadMesh(var[2]);
	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);
//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);

	for (int i=0;  i< tNquadPts; i++)
	{
//		fce[i] = std::cos(2.0*2.0*M_PI*(xc0[i]+xc1[i]));
		fce[i] = std::cos(2.0*(xc0[i]+xc1[i]));
		//fce[i] = std::cos(2.0*2.0*M_PI*(xc0[i]))*std::cos(2.0*2.0*M_PI*(xc1[i]));
	}
	
	HNM2D->m_expansions[0]->FwdTrans(fce,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	 ece = HNM2D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM2D->m_expansions[0]->GetCoeffs();

	HNM2D->m_Arrays.push_back(ece);
	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();
// Evaluate on a new equal space grid mesh.
	int gPts = atoi(argv[4]);
	int totPts = gPts*gPts;
	int Nx = gPts, Ny=gPts;
	NekDouble sx = 2.0/(Nx-1.0), sy = 2.0/(Ny-1.0);
	
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]) ); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	Array<OneD,NekDouble> direction(3,0.0), coord(3,0.0);
	direction[0] = 1.0;
	Array<OneD,NekDouble> pX(totPts), pY(totPts), pV(totPts), pP(totPts), pS(totPts),pDyn(totPts),pSDyn(totPts),pE(totPts);
	Array<OneD,NekDouble> glCoords(3,0.0);
	for( int i =0; i < Nx; i++)
	{
		for(int j=0; j< Ny; j++)
		{
			int index = i*Nx+j;
			pX[index] = i*sx-1.0;
			pY[index] = j*sy-1.0;
			pV[index] = std::cos(2.0*(pX[index] + pY[index]) );
			//pV[index] = std::cos(2.0*2.0*M_PI*(pX[index]))*std::cos(2.0*2.0*M_PI*(pY[index]));
			glCoords[0] = pX[index]; glCoords[1] = pY[index];
			NekDouble dynScaling = HNM2D->GetDynamicScaling(glCoords);
			pDyn[index] = dynScaling;
			sm.EvaluateAt(pX[index],pY[index],0.0,pS[index],valY,valZ,direction, atof(argv[3]) ,0);
			sm.EvaluateAt(pX[index],pY[index],0.0,pSDyn[index],valY,valZ,direction, dynScaling ,0);
			coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = 0.0;
//			cout << setprecision(19) << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;
			int elid = HNM2D->GetExpansionIndexUsingRTree(coord);
//			cout << elid << endl;
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
			//int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
			const Array<OneD,NekDouble> el_phys = ece.CreateWithOffset(
										ece, coeffOffset);
			pP[index] = lexp->PhysEvaluate(coord,el_phys);
			pE[index] = elid;
//			cout << j << endl;
		}
		cout << i << endl;
	}
	NektarBaseClass k;
	k.writeNekArray(pX,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pX_2DDyn.txt");
	k.writeNekArray(pY,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pY_2DDyn.txt");
	k.writeNekArray(pE,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pE_2DDyn.txt");
	k.writeNekArray(pV,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pV_2DDyn.txt");
	k.writeNekArray(pP,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pP_2DDyn.txt");
	k.writeNekArray(pS,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pS_2DDyn.txt");
	k.writeNekArray(pSDyn,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pSDyn_2DDyn.txt");
	k.writeNekArray(pDyn,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pDyn_2DDyn.txt");

	cout << "Done writing plane" << endl;
	//plane completed.
	// Evaluating on line y =0;
	int gPtsLy = atoi(argv[4])*10;
	int totPtsLy = gPtsLy;
	sx = sx/10.0;
	Array<OneD,NekDouble> pLy(totPtsLy),pLNx(totPtsLy), pLPx(totPtsLy), pELy(totPtsLy);
	Array<OneD,NekDouble> pXLy(totPtsLy), pYLy(totPtsLy), pVLy(totPtsLy), pPLy(totPtsLy), pSLy(totPtsLy),pDynLy(totPtsLy),pSDynLy(totPtsLy);
	for (int i =0 ; i< gPtsLy; i++)
	{
		int index = i;
		pXLy[index] = i*sx-1.0;
		pYLy[index] = 0.0;
			pVLy[index] = std::cos(2.0*(pXLy[index] + pYLy[index]) );
			//pV[index] = std::cos(2.0*2.0*M_PI*(pX[index]))*std::cos(2.0*2.0*M_PI*(pY[index]));
			glCoords[0] = pXLy[index]; glCoords[1] = pYLy[index];
			NekDouble dynScaling = HNM2D->GetDynamicScaling(glCoords);
			pDynLy[index] = dynScaling;
			sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSLy[index],valY,valZ,direction, atof(argv[3]) ,0);
			sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSDynLy[index],valY,valZ,direction, dynScaling ,0);
			coord[0] = pXLy[index]; coord[1] = pYLy[index]; coord[2] = 0.0;
//			cout << setprecision(19) << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;
			int elid = HNM2D->GetExpansionIndexUsingRTree(coord);
//			cout << elid << endl;
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
			//int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
			const Array<OneD,NekDouble> el_phys = ece.CreateWithOffset(
										ece, coeffOffset);
			pPLy[index] = lexp->PhysEvaluate(coord,el_phys);
			pELy[index] = elid;
//			cout << j << endl;
	}



	cout << "Done writing Line" << endl;
	k.writeNekArray(pXLy,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pXLy_2DDyn.txt");
	k.writeNekArray(pYLy,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pYLy_2DDyn.txt");
	k.writeNekArray(pELy,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pELy_2DDyn.txt");
	k.writeNekArray(pVLy,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pVLy_2DDyn.txt");
	k.writeNekArray(pPLy,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pPLy_2DDyn.txt");
	k.writeNekArray(pSLy,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pSLy_2DDyn.txt");
	k.writeNekArray(pSDynLy,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pSDynLy_2DDyn.txt");
	k.writeNekArray(pDynLy,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pDynLy_2DDyn.txt");

	// Evaluate on line x =0.5; x = -0.5;

/*
	for (int i =0; i <tNquadPts;i++)
	{
		sm.EvaluateAt(xc0[i],xc1[i],xc2[i],sce[i],valY,valZ,direction);

		//cout << "\tf:\t" << abs(fce[i]-ece[i]) ;
		//cout << "\ts:\t:" << abs(fce[i] -sce[i]);
		//cout << "\ts:\t:" << sce[i];
		//cout << endl;
	}
	Vmath::Vsub(tNquadPts, &ece[0],1, &fce[0],1,&temp0[0],1);
	Vmath::Vabs(tNquadPts, &temp0[0],1, &temp0[0],1); 
	
	HNM2D->m_expansions[1]->FwdTrans(temp0,HNM2D->m_expansions[1]->UpdateCoeffs() );
	HNM2D->m_expansions[1]->BwdTrans( HNM2D->m_expansions[1]->GetCoeffs(),
										HNM2D->m_expansions[1]->UpdatePhys());
	temp0 = HNM2D->m_expansions[1]->GetPhys();

	Vmath::Vsub(tNquadPts, &sce[0],1, &fce[0],1,&temp1[0],1);
	Vmath::Vabs(tNquadPts, &temp1[0],1, &temp1[0],1); 

	HNM2D->m_expansions[2]->FwdTrans(temp1,HNM2D->m_expansions[2]->UpdateCoeffs() );
	HNM2D->m_expansions[2]->BwdTrans( HNM2D->m_expansions[2]->GetCoeffs(),
										HNM2D->m_expansions[2]->UpdatePhys());
	 temp1 = HNM2D->m_expansions[2]->GetPhys();

	k.printNekArray(xc0,0);
	k.printNekArray(fce,0);
	k.printNekArray(ece,0);
	k.printNekArray(sce,0);
	cout << fname+"_"+argv[2]+"_xc0_OneSided2kp1.txt"<< endl;
*/	

/*
	 string out = vSession->GetSessionName() + ".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (HNM2D->m_expansions[0]->GetFieldDefinitions()).at(0);
    FieldDef[0]->m_fields.push_back("u");
    FieldDef[0]->m_fields.push_back("v");
    FieldDef[0]->m_fields.push_back("w");
    HNM2D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM2D->m_fld->Write(out, FieldDef, FieldData);
*/
	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
