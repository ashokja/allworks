clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20];

%abc = '../../data/UnsTriMeshes/NM2_unsMeshvar1_0125_2_R_100_pX_2DDyn.txt';

R = 100;
poly =2;
order = poly;
count =1;
%prefixFilename = 'data/P4_triSplit6_L4_R1_';
%prefixFilename = 'data/P2_triSpli_L2_R1_';

%prefixName = '../../data/UnsTriMeshes/';
prefixName = '../../data/sQuadDyn/';

for poly = [2,3,4]
%           FileName = strcat('NM',int2str(poly),'_Mesh025_');
%           FileName = strcat('NM',int2str(poly),'_unsMeshvar1_025_');
            FileName = strcat('P',int2str(poly-1),'_QuadCross_40_');
            prefixFilename = strcat( prefixName,FileName,int2str(poly),'_R_',int2str(R));

            xfname = strcat(prefixFilename,'_pX_2DDyn.txt' );
            yfname = strcat(prefixFilename,'_pY_2DDyn.txt' );
            ffname = strcat(prefixFilename,'_pV_2DDyn.txt' );
            pfname = strcat(prefixFilename,'_pP_2DDyn.txt' );
            sfname = strcat(prefixFilename,'_pS_2DDyn.txt' );
            sDynfname = strcat(prefixFilename,'_pSDyn_2DDyn.txt' );
            dynsfname = strcat(prefixFilename,'_pDyn_2DDyn.txt' );

            xpos = textread(xfname, '%f\t');
            ypos = textread(yfname, '%f\t');
            fpos = textread(ffname, '%f\t');
            ppos = textread(pfname, '%f\t');
            spos = textread(sfname, '%f\t');
            sDynpos = textread(sDynfname, '%f\t');
            dynspos = textread(dynsfname, '%f\t');

            xVpos = reshape(xpos,[R,R]);
            yVpos = reshape(ypos,[R,R]);
            fVpos = reshape(fpos,[R,R]);
            pVpos = reshape(ppos,[R,R]);
            sVpos = reshape(spos,[R,R]);
            sDynVpos = reshape(sDynpos,[R,R]);
            dynsVpos = reshape(dynspos,[R,R]);


            figure(1),
            surf(xVpos,yVpos,fVpos);

            figure(2),
            surf(xVpos,yVpos,pVpos);

            figure(3),
            surf(xVpos,yVpos,sVpos);

            %%
            figure(4),
            surf(xVpos,yVpos,abs(fVpos-pVpos));
            title('projection errors');
            figure(5),
            surf(xVpos,yVpos,abs(fVpos-sVpos));
            title('SIAC errors'); hold off;
            figure(51),
            surf(xVpos,yVpos,abs(fVpos-sDynVpos));
            title('SIAC Dyn errors');
            %%

            figure(6),
            contourf(xVpos,yVpos,log10(abs(fVpos-pVpos)));
            title('projection errors'); colorbar(); caxis([-10,0]);
            view(0,90); axis equal;
            print(strcat('Images/',FileName,'proj'), '-dpng', '-r100');

            figure(7),
            contourf(xVpos,yVpos,log10(abs(fVpos-sVpos)));
            title(strcat('LSIAC errors')); colorbar(); caxis([-10,0]);
            view(0,90); axis equal;
            print(strcat('Images/',FileName,'LSIAC'), '-dpng', '-r100');

            figure(8),
            contourf(xVpos,yVpos,log10(abs(fVpos-sDynVpos)));
            title('LSIAC Dyn errors'); colorbar(); caxis([-10,0]);
            view(0,90);axis equal; axis tight;
            print(strcat('Images/',FileName,'LSIAC_Dyn'), '-dpng', '-r100');

             figure(9),
             surf(xVpos,yVpos, dynsVpos);
             shading interp; colorbar(); 
             view(0,90); hold off;
            %print(strcat('Images/',FileName,'LSIAC_Scale'), '-dpng', '-r100');
            
            disp('Proj');
            disp(sum(sum((abs(fVpos-pVpos)))));
            disp(max(max((abs(fVpos-pVpos)))));
            disp('SIAC');
            disp(sum(sum((abs(fVpos-sVpos)))));
            disp(max(max((abs(fVpos-sVpos)))));

            disp('SIAC Dyn');
            disp(sum(sum((abs(fVpos-sDynVpos)))));
            disp(max(max((abs(fVpos-sDynVpos)))));

end

%%
% figure (10);
%     addpath('../');
%     plotNekMesh('data/dataCos2/P2_triSplit6_L4_R1.xml');
%     xlim([-1,1]);
%     ylim([-1,1]);
%     plot3([-1,1],[0,0],[32,32],'--r')
%    print(strcat('Images/triSplit6_L4_R1_','meshFile'), '-dpng', '-r100');
%%    
    
% figure(9),
% surf(xVpos,yVpos,dynsVpos);
% title('dynscaling'); shading Interp;
% 
% figure(10),
% contourf(xVpos,yVpos,dynsVpos);
% title('dynscaling'); 


