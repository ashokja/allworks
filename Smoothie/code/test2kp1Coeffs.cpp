#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "OneSidedSIAC.h"
#include "SmoothieSIAC1D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh1D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

 
int main(int argc, char* argv[])
{
	if ( argc != 4 )
	{
		cout << "1st order			" << endl;
		cout << "2nd kernel shift	" << endl;
		cout << "3rd Type of Filter		" << endl;
		return 0;
	}

	OneSidedSIAC::OneSidedFilterType filter;
	switch (atoi(argv[3]))
	{
		case 1:
			filter = OneSidedSIAC::OneSidedFilterType::BASIC_SIAC_2kp1;
			break;
		case 2:
			filter = OneSidedSIAC::OneSidedFilterType::VAN_SIAC_4kp1;
			break;
		case 3:
			filter = OneSidedSIAC::OneSidedFilterType::XLi_SIAC_2kp2;
			break;
	}

	OneSidedSIAC oneSIAC( atoi(argv[1]), filter);	

	oneSIAC.EvaluateCoefficients(atof(argv[2]));
	oneSIAC.printNekArray( oneSIAC.m_coeffs,0);

	OneSidedSIAC oneSIAC1( atoi(argv[1]), filter );	
	oneSIAC1.EvaluateCoefficients(-1.0*atof(argv[2]));
	oneSIAC1.printNekArray( oneSIAC1.m_coeffs,0);

	int parts = 4001;
	Array<OneD,NekDouble> x_pos(parts), v_pos(parts), v_pos1(parts), v_pos2(parts), v_pos3(parts);

	for (int i =0; i < parts; i++)
	{
		x_pos[i] =  -20.0+ ( (NekDouble)i/(NekDouble)(parts-1.0))*40.0;
	}
	
	oneSIAC.writeNekArray(x_pos,"xpos.txt");

	oneSIAC.EvaluateFilter(x_pos, v_pos, 1, atof(argv[2]) );
	oneSIAC.writeNekArray(v_pos,"vpos.txt");

	oneSIAC1.EvaluateFilter(x_pos, v_pos1, 1, -1.0*atof(argv[2]) );
	oneSIAC1.writeNekArray(v_pos1,"vpos1.txt");

	NekDouble tmin, tmax; 
	oneSIAC.GetFilterRange( 1.0, tmin, tmax );
	cout << "tmin: \t" << tmin << "\t tmax:" << tmax << endl;
	oneSIAC.GetFilterRange( 1.0, tmin, tmax, atof(argv[2]) );
	cout << "tmin: \t" << tmin << "\t tmax:" << tmax << endl;

/*	
	Array<OneD,NekDouble> x_posD(3), v_posD(3),v_posL(3);	
	x_posD[0] = -0.01;
	x_posD[1] = 0.0;
	x_posD[2] = 0.01;
	oneSIAC1.EvaluateFilter(x_posD, v_posD, 1, -1.0*atof(argv[2]) );
	oneSIAC.EvaluateFilter(x_posD, v_posL, 1, atof(argv[2]) );

	cout << x_posD[0] << "\t" << v_posD[0]<< "\t"<<v_posL[0] << endl;
	cout << x_posD[1] << "\t" << v_posD[1]<< "\t"<<v_posL[1] << endl;
	cout << x_posD[2] << "\t" << v_posD[2]<< "\t"<<v_posL[2] << endl;
*/
//  Writing up the Bsplines.
	
/*

	Array<OneD,NekDouble> x_posD(3), v_posD(3),v_posL(3);	
	x_posD[0] = -0.01;
	x_posD[1] = 0.0;
	x_posD[2] = 0.01;
	
	Array<OneD,NekDouble> x_posD(1), v_posD(1),v_posL(1);	
	x_posD[0] = 0.0;
	
	vector<NekDouble> kVec;
	vector<NekDouble> kVec1;
	kVec.push_back(-1);
	kVec.push_back(0);
	kVec.push_back(0);
	kVec.push_back(0);
	kVec1.push_back(0);
	kVec1.push_back(0);
	kVec1.push_back(0);
	kVec1.push_back(1);
	GeneralBSplines bspl(3);
	bspl.BSplinesBasis ( x_posD,kVec, atoi(argv[1]), atoi(argv[2]), v_posD, 0.0, 1.0);
	bspl.BSplinesBasis ( x_posD,kVec1, atoi(argv[1]), atoi(argv[2]), v_posL, 0.0, 1.0);

	cout << x_posD[0] << "\t" << v_posD[0]<< "\t"<<v_posL[0] << endl;
//	cout << x_posD[1] << "\t" << v_posD[1]<< "\t"<<v_posL[1] << endl;
//	cout << x_posD[2] << "\t" << v_posD[2]<< "\t"<<v_posL[2] << endl;
*/

	return 0;
}
