%C++ file testNaca3D 
% sample parameter list ./testNaca3D ../data/naca_xml/P0000009.xml 2 0.05

clear;
close all;
va = -51.5;
vb = 18;

%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05' ;
filename = strcat('bkup_Jan24/P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_AllVar3D' );
filepath = strcat('data/',filename);

xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );

uxpfname = strcat(filepath,'_pUx_p.txt' );
vxpfname = strcat(filepath,'_pVx_p.txt' );
wxpfname = strcat(filepath,'_pWx_p.txt' );
uypfname = strcat(filepath,'_pUy_p.txt' );
vypfname = strcat(filepath,'_pVy_p.txt' );
wypfname = strcat(filepath,'_pWy_p.txt' );
uzpfname = strcat(filepath,'_pUz_p.txt' );
vzpfname = strcat(filepath,'_pVz_p.txt' );
wzpfname = strcat(filepath,'_pWz_p.txt' );

uxppos = textread(uxpfname,'%f\t');
uyppos = textread(uypfname,'%f\t');
uzppos = textread(uzpfname,'%f\t');
vxppos = textread(vxpfname,'%f\t');
vyppos = textread(vypfname,'%f\t');
vzppos = textread(vzpfname,'%f\t');
wxppos = textread(wxpfname,'%f\t');
wyppos = textread(wypfname,'%f\t');
wzppos = textread(wzpfname,'%f\t');

uxsfname = strcat(filepath,'_pUx_s.txt' );
vxsfname = strcat(filepath,'_pVx_s.txt' );
wxsfname = strcat(filepath,'_pWx_s.txt' );
uysfname = strcat(filepath,'_pUy_s.txt' );
vysfname = strcat(filepath,'_pVy_s.txt' );
wysfname = strcat(filepath,'_pWy_s.txt' );
uzsfname = strcat(filepath,'_pUz_s.txt' );
vzsfname = strcat(filepath,'_pVz_s.txt' );
wzsfname = strcat(filepath,'_pWz_s.txt' );

uxspos = textread(uxsfname,'%f\t');
uyspos = textread(uysfname,'%f\t');
uzspos = textread(uzsfname,'%f\t');
vxspos = textread(vxsfname,'%f\t');
vyspos = textread(vysfname,'%f\t');
vzspos = textread(vzsfname,'%f\t');
wxspos = textread(wxsfname,'%f\t');
wyspos = textread(wysfname,'%f\t');
wzspos = textread(wzsfname,'%f\t');

upfname = strcat(filepath,'_pU_s.txt' );
vpfname = strcat(filepath,'_pV_s.txt' );
wpfname = strcat(filepath,'_pW_s.txt' );

uppos = textread(upfname,'%f\t');
vppos = textread(vpfname,'%f\t');
wppos = textread(wpfname,'%f\t');

uspos = textread(upfname,'%f\t');
vspos = textread(vpfname,'%f\t');
wspos = textread(wpfname,'%f\t');


xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');

epos = floor(epos +0.0001);
s = floor( (max(size(xpos)))^(1/3) +0.1);

    xVpos = reshape(xpos,s,s,s);
    xVpos = permute(xVpos,[2,3,1]);
    yVpos = reshape(ypos,s,s,s);
    yVpos = permute(yVpos,[2,3,1]);
    zVpos = reshape(zpos,s,s,s);
    zVpos = permute(zVpos,[2,3,1]);

uxpVpos = reshape(uxppos,s,s,s);    uxpVpos = permute(uxpVpos,[2,3,1]);
uypVpos = reshape(uyppos,s,s,s);    uypVpos = permute(uypVpos,[2,3,1]);
uzpVpos = reshape(uzppos,s,s,s);    uzpVpos = permute(uzpVpos,[2,3,1]);

vxpVpos = reshape(vxppos,s,s,s);    vxpVpos = permute(vxpVpos,[2,3,1]);
vypVpos = reshape(vyppos,s,s,s);    vypVpos = permute(vypVpos,[2,3,1]);
vzpVpos = reshape(vzppos,s,s,s);    vzpVpos = permute(vzpVpos,[2,3,1]);

wxpVpos = reshape(wxppos,s,s,s);    wxpVpos = permute(wxpVpos,[2,3,1]);
wypVpos = reshape(wyppos,s,s,s);    wypVpos = permute(wypVpos,[2,3,1]);
wzpVpos = reshape(wzppos,s,s,s);    wzpVpos = permute(wzpVpos,[2,3,1]);

uxsVpos = reshape(uxspos,s,s,s);    uxsVpos = permute(uxsVpos,[2,3,1]);
uysVpos = reshape(uyspos,s,s,s);    uysVpos = permute(uysVpos,[2,3,1]);
uzsVpos = reshape(uzspos,s,s,s);    uzsVpos = permute(uzsVpos,[2,3,1]);

vxsVpos = reshape(vxspos,s,s,s);    vxsVpos = permute(vxsVpos,[2,3,1]);
vysVpos = reshape(vyspos,s,s,s);    vysVpos = permute(vysVpos,[2,3,1]);
vzsVpos = reshape(vzspos,s,s,s);    vzsVpos = permute(vzsVpos,[2,3,1]);

wxsVpos = reshape(wxspos,s,s,s);    wxsVpos = permute(wxsVpos,[2,3,1]);
wysVpos = reshape(wyspos,s,s,s);    wysVpos = permute(wysVpos,[2,3,1]);
wzsVpos = reshape(wzspos,s,s,s);    wzsVpos = permute(wzsVpos,[2,3,1]);


figure (1),
hsurfaces = slice(xVpos,yVpos,zVpos,uysVpos,[2.2,2.25,2.3,2.35],[0.2,0.25,0.3,0.35],[0.75,0.8,0.85,0.9]);
set(hsurfaces,'FaceColor','flat','EdgeColor','none')
xlabel('x'); ylabel('y'); zlabel('z');
colormap jet; colorbar; hold off; 
title('FUNC(ACT'); hold on;
axis equal;


Vorticity_p = ((uypVpos- vxpVpos).^(2) + (uzpVpos- wxpVpos).^(2) + (wypVpos- vzpVpos).^(2) ).^(1/2);

Vorticity_s = ((uysVpos- vxsVpos).^(2) + (uzsVpos- wxsVpos).^(2) + (wysVpos- vzsVpos).^(2) ).^(1/2);



% Curvature 1dir.
% calculate acceleration.
auspos = uxspos.*uspos + uyspos.*vspos + uzspos.*wspos;
avspos = vxspos.*uspos + vyspos.*vspos + vzspos.*wspos;
awspos = wxspos.*uspos + wyspos.*vspos + wzspos.*wspos;
 %
% veclocity cross acceleration. 
cuspos = vspos.*awspos - wspos.*avspos;
cvspos = wspos.*auspos - uspos.*awspos;
cwspos = uspos.*avspos - vspos.*auspos;

curvature_pos = (cuspos.^(2) + cvspos.^(2) + cwspos.^(2)).^(1/2);
curvatures_Vpos = reshape(curvature_pos,s,s,s);

%Curvature Projection
% calculate acceleration.
auspos = uxppos.*uspos + uyppos.*vspos + uzppos.*wspos;
avspos = vxppos.*uspos + vyppos.*vspos + vzppos.*wspos;
awspos = wxppos.*uspos + wyppos.*vspos + wzppos.*wspos;
 %
% veclocity cross acceleration. 
cuspos = vspos.*awspos - wspos.*avspos;
cvspos = wspos.*auspos - uspos.*awspos;
cwspos = uspos.*avspos - vspos.*auspos;

curvature_pos = (cuspos.^(2) + cvspos.^(2) + cwspos.^(2)).^(1/2);
curvaturep_Vpos = reshape(curvature_pos,s,s,s);

valueIso = 20;
c_temp = 0;
figure(6), hold off
contourslice(xVpos, yVpos, zVpos, Vorticity_s, 2.35,[], [], 90);
%contourslice(xVpos, yVpos, zVpos, Vorticity_s, 2.25,[], [], linspace(0,100,40)) ;
view(90,0);
axis off;
%axis([2.2,2.35,0.2,0.35,0.75,0.9])
hcs = contourslice(xVpos, yVpos, zVpos, Vorticity_s, 2.35,[], [], [valueIso+c_temp valueIso+c_temp]) ;
set(hcs,'EdgeColor',[1 0 0]);
set(hcs,'linewidth',2);
set(hcs,'linestyle','--');
%print('images/Vorticity_Contours_SIAC_P_2_F','-dpng', '-r500');
%%

figure(66), hold off
contourslice(xVpos, yVpos, zVpos, curvaturep_Vpos, 2.25,[], []);
%contourslice(xVpos, yVpos, zVpos, Vorticity_s, 2.25,[], [], linspace(0,100,40)) ;
view(90,0);
axis off;
%axis([2.2,2.35,0.2,0.35,0.75,0.9])
%hcs = contourslice(xVpos, yVpos, zVpos, curvatures_Vpos, 2.25,[], [], [valueIso+c_temp valueIso+c_temp]) ;
%set(hcs,'EdgeColor',[1 0 0]);
%set(hcs,'linewidth',2);
%set(hcs,'linestyle','--');



%%
figure(7), hold off
contourslice(xVpos, yVpos, zVpos, Vorticity_p, 2.25,[], [], linspace(0,100,40));
view(90,0);
axis off;
axis([2.2,2.35,0.2,0.35,0.75,0.9])
hcs = contourslice(xVpos, yVpos, zVpos, Vorticity_p, 2.25,[], [], [valueIso valueIso]) ;
set(hcs,'EdgeColor',[1 0 0]);
set(hcs,'linewidth',2);
set(hcs,'linestyle','--');
%print('images/Vorticity_Contours_DG_P_2_F','-dpng', '-r500');

%%

figure (8), hold on;
eVpos = reshape(epos,s,s,s);
eVpos = permute(eVpos,[2,3,1]);
dely = squeeze(yVpos(:,31,:));
delz = squeeze(zVpos(:,31,:));
delvp = squeeze(Vorticity_p(:,31,:));
dele = squeeze(eVpos(:,31,:));
surf(dely,delz,dele);

delyl = dely(:);
delzl = delz(:);
delvpl = delvp(:);
delel = dele(:);


[evals, elids,relids] = unique(delel(:));
addpath('..');
num_unique_ids = (max(size(evals)));
for etag = evals'
    disp(etag);
    %disp('next');
    el_indices = find(dele(:)' ==etag);
    if (max(size(el_indices)) < 7)
        continue;
    end
    disp(delel(el_indices));
    
    ytripos = delyl(el_indices);
    ztripos = delzl(el_indices);
    vtripos = delvpl(el_indices);
    tri = delaunay(ytripos,ztripos);
    
    
    figure(9), hold on;    
    trisurf(tri,ytripos,ztripos,vtripos); hold on;
    
    figure(10), hold on;
    tricontour( [ytripos,ztripos], tri, vtripos,linspace(0,100,40)); hold on;
    
    tricontour([ytripos,ztripos], tri, vtripos,[valueIso valueIso]); hold on;
    %hcs = contourslice(xVpos, yVpos, zVpos, Vorticity_p, 2.25,[], [], ) ;
    set(hcs,'EdgeColor',[1 0 0]);
    set(hcs,'linewidth',2);
    set(hcs,'linestyle','--');
    
%    tricontour(tri,ytripos,ztripos,vtripos,linspace(0,100,40)); hold on;
    
end

% 
% figure(3)
% fv = isosurface(xVpos,yVpos,zVpos,Vorticity_s,30.0); hold on;
% isonormals(xVpos,yVpos,zVpos,Vorticity_s,fv)
% fv.FaceColor = 'red';
% fv.EdgeColor = 'none';
% daspect([1,1,1])
% view(3); 
% camlight 
% lighting gouraud


% % %%
% % figure (2),
% % hsurfaces = slice(xVpos,yVpos,zVpos,psVpos,[2.5,2.8333,3.0],0.25,0.6);
% % set(hsurfaces,'FaceColor','flat','EdgeColor','none')
% % xlabel('x'); ylabel('y'); zlabel('z');
% % colormap jet; colorbar; hold off; caxis([-1,1]);
% % title('FUNC(ACT'); hold on;


%x = linspace(2.5,3,10);
%[X,Y,Z] = meshgrid(x);
% 
% sx = linspace(2.5, 2.7,3);
% sy = linspace(min(yVpos(:)), max(yVpos(:)),10);
% sz = linspace(min(zVpos(:)), max(zVpos(:)),10);
% [startx,starty,startz] = meshgrid(sx,sy,sz);
% xlabel('x');ylabel('y'); zlabel('z');
% 
% streamline(xVpos,yVpos,zVpos,upVpos,vpVpos,wpVpos,startx,starty,startz); 

% %hold on;
%  figure(3)
% % fv = isosurface(xVpos,yVpos,zVpos,ppVpos,0.0);
% % isonormals(xVpos,yVpos,zVpos,ppVpos,fv)
% % fv.FaceColor = 'red';
% % fv.EdgeColor = 'none';
% % daspect([1,1,1])
% % view(3); axis tight
% % camlight 
% % lighting gouraud
% 
% 
% %[x,y,z,v] = flow;
% p = patch(isosurface(xVpos,yVpos,zVpos,ppVpos,-0.6));
% isonormals(xVpos,yVpos,zVpos,ppVpos,p)
% p.FaceColor = 'red';
% p.EdgeColor = 'none';
% daspect([1,1,1])
% view(3); axis tight
% camlight 
% lighting gouraud
