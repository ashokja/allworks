
function y = CharacteristicPoly(x,npts,xpts)

y = ones(size(x));

for i=1:npts,
  y = y.*(x-xpts(i));
end

