% test_simple_4
% use Bspline to get Bernstein basis. (if possible.)

clear ;
close all;

deg = 3;

knots = [0,0,0,0,0.25,0.25,0.25,0.25,1,1,1,1];
cpts = [0,1,2,3,4,5,6,7];
cptsX = [0,1,2,3,3,4,5,6];
cptsY = [0,1,1,3,4.2,2.5,2,1];

% using Deboor algorithm for BSplines.
t_sel = linspace( 0.0,1.0,1000 );
b_sel = zeros(size(t_sel));

for index = 1:size(t_sel(:))
    t = t_sel(index);
%    t =0.4;
    %find k.
    for uk = 1:size(knots(:))
        if ( t < knots(uk)  )
            k = uk-1;
            break;
        end
    end
    
    if  ( (k < deg) )
        disp('screwed up knot t value.');
        break;
    end
    if ( ( k > ( size(knots(:)) -deg +1) ) )
        disp('screwed up knot t value.');
        break;
    end
    
    %load the points required.
    pts_evalX(:,1) = cptsX(k-deg:k);
    pts_evalY(:,1) = cptsY(k-deg:k);
    
%    disp(pts_eval);
    
    for l = 1:deg
        for lp = k-deg+1 + l -1 : k+1-1
            disp([l lp]);
            
            alpha = (t - knots(lp))/ (knots(lp+deg-l+1) - knots(lp));
%            pt_index = lp -l+1;
            pt_index = lp -l+1 -k+deg;
            pts_evalX( pt_index, l+1) = (1-alpha)*pts_evalX(pt_index ,l) + alpha*pts_evalX( pt_index+1,l);
            pts_evalY( pt_index, l+1) = (1-alpha)*pts_evalY(pt_index ,l) + alpha*pts_evalY( pt_index+1,l);
            
            
            %            disp ([l, lp]);
            
 %           alpha = (t - knots(lp) ) / (knots(lp-deg) - knots(lp) ) ;
 %           pts_eval( lp,l+1) = (1-alpha)* pts_eval(lp-1,l) + alpha* pts_eval(lp ,l);
        end
    end
    
    b_selX(index) = pts_evalX(1,deg+1);
    b_selY(index) = pts_evalY(1,deg+1);
    %disp(pts_eval);
%    break;
end


plot(cptsX,cptsY,'*'); hold on;
plot( b_selX,b_selY);
%plot(t_sel,b_sel ); hold on;
%plot(t_sel,zeros(size(t_sel)) ); hold on;
grid on;
grid minor;
%plot( t_sel, deg*((1-t_sel).^(deg-1)).*t_sel.^(1) )


