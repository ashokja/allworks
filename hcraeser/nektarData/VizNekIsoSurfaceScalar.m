function [fvs,pts] = VizNekIsoSurfaceScalar(X,Y,Z,U,isoValue,color)
    switch nargin
        case 5
            color = 'red';
    end
    
    X = permute(X,[2,1,3]); U = permute(U,[2,1,3]);
    Y = permute(Y,[2,1,3]); 
    Z = permute(Z,[2,1,3]); 
    normAtCheb = U;
    
    [fvs,pts] = isosurface(X,Y,Z,normAtCheb,isoValue);
    p = patch(isosurface(X,Y,Z,normAtCheb,isoValue));
    isonormals(X,Y,Z,normAtCheb,p)
    p.FaceColor = color;
    p.EdgeColor = 'none';
%'FaceAlpha',.3
    p.FaceAlpha = .3;
    daspect([1,1,1])
    view(3); %axis tight
    camlight 
    lighting gouraud 
    hold on;
    xlabel('x'); ylabel('y'); zlabel('z');
end