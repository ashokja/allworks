#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"

#define TOLERENCE_VIZ 0

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

void calCurvature(int nq, Array<OneD,NekDouble> &u_phys, Array<OneD,NekDouble> &v_phys,
	Array<OneD,NekDouble> &udx_phys, Array<OneD,NekDouble> &udy_phys,
	Array<OneD,NekDouble> &vdx_phys, Array<OneD,NekDouble> &vdy_phys,
	Array<OneD,NekDouble> &au_phys, Array<OneD,NekDouble> &av_phys,
	Array<OneD,NekDouble> &vCa )
{
	Array<OneD,NekDouble> temp1(nq), temp2(nq);
	// a acceleration
	Vmath::Vmul(nq, udx_phys,1, u_phys,1, temp1,1);
	Vmath::Vmul(nq, udy_phys,1, v_phys,1, temp2,1);
	Vmath::Vadd(nq, temp1, 1,temp2,1, au_phys,1);	
	Vmath::Vmul(nq, vdx_phys,1, u_phys,1, temp1,1);
	Vmath::Vmul(nq, vdy_phys,1, v_phys,1, temp2,1);
	Vmath::Vadd(nq, temp1, 1,temp2,1, av_phys,1);

	// cross v a 
	Vmath::Vmul(nq, u_phys,1,av_phys,1, temp1,1);
	Vmath::Vmul(nq, v_phys,1,au_phys,1, temp2,1);
	Vmath::Vsub(nq, temp1,1, temp2,1, vCa,1);
	return;
}

void calVorticity(int nq, Array<OneD,NekDouble> &udy_phys, Array<OneD,NekDouble> &vdx_phys,
	Array<OneD,NekDouble> &Vz )
{
	Vmath::Vsub(nq, udy_phys,1, vdx_phys,1,Vz ,1);
	return;
}

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg is number with _xxx" << endl;
		cout << "3rd arg is resolution in x" << endl;
		cout << "4th arg is resolution in y" << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();

	HNM2D->LoadData( fname+ argv[2]+".chk",var);
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}

	// Loaded u,v,p from file. Work on them.
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
                                        HNM2D->m_expansions[0]->UpdatePhys());

    HNM2D->m_expansions[1]->BwdTrans( HNM2D->m_expansions[1]->GetCoeffs(),
                                        HNM2D->m_expansions[1]->UpdatePhys());
    
    HNM2D->m_expansions[2]->BwdTrans( HNM2D->m_expansions[2]->GetCoeffs(),
                                        HNM2D->m_expansions[2]->UpdatePhys());

	Array<OneD,NekDouble> u_phys, v_phys, p_phys;
	Array<OneD,NekDouble> udx_phys(tNquadPts), udy_phys(tNquadPts);
	Array<OneD,NekDouble> vdx_phys(tNquadPts), vdy_phys(tNquadPts),V_phys(tNquadPts);
	Array<OneD,NekDouble> pdx_phys(tNquadPts), pdy_phys(tNquadPts);
	Array<OneD,NekDouble> au_phys(tNquadPts), av_phys(tNquadPts), vCa_phys(tNquadPts);
	
	u_phys = HNM2D->m_expansions[0]->GetPhys();
	v_phys = HNM2D->m_expansions[1]->GetPhys();
	p_phys = HNM2D->m_expansions[2]->GetPhys();

	//u and v deriv
	HNM2D->m_expansions[0]->PhysDeriv( u_phys, udx_phys, udy_phys );
	HNM2D->m_expansions[1]->PhysDeriv( v_phys, vdx_phys, vdy_phys );
	HNM2D->m_expansions[2]->PhysDeriv( p_phys, pdx_phys, pdy_phys );
	
	calCurvature(tNquadPts, u_phys, v_phys, udx_phys, udy_phys, 
		vdx_phys, vdy_phys, au_phys, av_phys, vCa_phys );
	calVorticity(tNquadPts, udy_phys, 
		vdx_phys, V_phys );

// Evaluate on a new equal space grid mesh.
	NekDouble StartX = 2.5, StartY = -4;	
	NekDouble EndX = 10.5, EndY = 4;	

	HNM2D->m_expansions[2]->SetPhys(V_phys);


	//udpate Rtree.
	// get elIds and glIds in the box and print them.
	HNM2D->LoadExpListIntoRTree();
	vector<int> elIds,glIds;
	HNM2D->IntersectWithBoxUsingRTree( StartX, StartY, -0.01, EndX,EndY,0.01 , elIds, glIds);
	HNM2D->printNekArray(elIds,0);	
	HNM2D->printNekArray(glIds,0);	

    // list of all the elId you want to use.
    // Loop through the element IDs.
    //      Loop through gridX and gridY of every element.
    //      convert to elcoord to globalcoord.
    //      for reference print el, elx, ely, glox, gloy, DG_v, SIAC_V
    //      save all invariables.
    // print all of them to file.

//	int num_exp = HNM2D->m_expansions[0]->GetExpSize();
	int num_exp = elIds.size();;
    int gpts = atoi(argv[4]);
    int totPts = gpts*gpts*num_exp;
    int Nx = gpts, Ny=gpts;
    NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0);

    Array<OneD,NekDouble> locCoord(3,0.0), locCollapsed(3,0.0), coord(3,0.0), directionX(3,0.0), directionY(3,0.0);
    Array<OneD,NekDouble> pX(totPts), pY(totPts), pE(totPts);
	Array<OneD,NekDouble> pU_s(totPts), pV_s(totPts);
	Array<OneD,NekDouble> pUx_s(totPts), pUy_s(totPts);
	Array<OneD,NekDouble> pVx_s(totPts), pVy_s(totPts);
	Array<OneD,NekDouble> pU_p(totPts), pV_p(totPts);
	Array<OneD,NekDouble> pUx_p(totPts), pUy_p(totPts);
	Array<OneD,NekDouble> pVx_p(totPts), pVy_p(totPts);

    int index = 0;
    NekDouble valY,valZ;


// Testing
//	NekDouble meshTShift;
//	HNM2D->CanTRangebeApplied( 9.49678, -6.59012444, 0.0, directionY, -1.25,1.25,meshTShift);

// Ignore after
	Array<OneD,NekDouble> pVor_p(totPts), pVor_s(totPts);

	BOOST_FOREACH( int elId, elIds)
    {
        //int elId = i;
        int physOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elId);
        const Array<OneD,NekDouble> el_u_phys = u_phys.CreateWithOffset( u_phys, physOffset );
        const Array<OneD,NekDouble> el_udx_phys = udx_phys.CreateWithOffset( udx_phys, physOffset );
        const Array<OneD,NekDouble> el_udy_phys = udy_phys.CreateWithOffset( udy_phys, physOffset );
        const Array<OneD,NekDouble> el_v_phys = v_phys.CreateWithOffset( v_phys, physOffset );
        const Array<OneD,NekDouble> el_vdx_phys = vdx_phys.CreateWithOffset( vdx_phys, physOffset );
        const Array<OneD,NekDouble> el_vdy_phys = vdy_phys.CreateWithOffset( vdy_phys, physOffset );

        LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elId);

        for( int i =0; i < Nx; i++)
        {
            for(int j=0; j< Ny; j++)
            {
                if ( 0==i)
                {
                    locCoord[0] = i*sx*2.0-1.0 + TOLERENCE_VIZ;
                }else if( Nx-1 == i)
                {
                    locCoord[0] = i*sx*2.0-1.0 - TOLERENCE_VIZ ;
                }else{
                    locCoord[0] = i*sx*2.0-1.0 ;
                }
                if ( 0==j)
                {
                    locCoord[1] = j*sy*2.0-1.0 + TOLERENCE_VIZ ;
                }else if( Ny-1 ==j)
                {
                    locCoord[1] = j*sy*2.0-1.0 - TOLERENCE_VIZ;
                }else{
                    locCoord[1] = j*sy*2.0-1.0 ;
                }

                locCoord[0] = i*sx*2.0-1.0 ;
                locCoord[1] = j*sy*2.0-1.0 ;

				if( Nektar::LibUtilities::eTriangle == lexp->GetGeom()->GetShapeType() )
				{
					locCollapsed[0] = (1.0+locCoord[0])*(1.0-locCoord[1])/2.0 -1.0;
					locCollapsed[1] = locCoord[1];
				}else
				{
					locCollapsed[0] = locCoord[0];
					locCollapsed[1] = locCoord[1];
				}
				
                lexp->GetCoord(locCollapsed, coord);
//				cout << "\tlocCoord" << endl; 
//				printNekArray( locCoord, 0);
//				cout << "\tcollapsed" << endl; 
//				printNekArray( locCollapsed, 0);
//				cout << "\tphyCoord" << endl; 
//				printNekArray( coord, 0);
//				cout << setprecision(25) << endl;
//				cout << locCoord[0] << locCoord[1] << endl;
//				cout << coord[0] << coord[1] << endl;
//				cout << setprecision(5) << endl;
                pE[index] = elId;
                pX[index] = coord[0];
                pY[index] = coord[1];

//				smD.EvaluateAt(pX[index],pY[index],0.0,pUx_s[index],valY,valZ,directionX, atof(argv[3]),0);
//				smD.EvaluateAt(pX[index],pY[index],0.0,pUy_s[index],valY,valZ,directionY, atof(argv[3]),0);

//				smD.EvaluateAt(pX[index],pY[index],0.0,pVx_s[index],valY,valZ,directionX, atof(argv[3]),1);
//				smD.EvaluateAt(pX[index],pY[index],0.0,pVy_s[index],valY,valZ,directionY, atof(argv[3]),1);
				
//				sm.EvaluateAt(pX[index],pY[index],0.0,pU_s[index],valY,valZ,directionX, atof(argv[3]),0);
//				sm.EvaluateAt(pX[index],pY[index],0.0,pV_s[index],valY,valZ,directionY, atof(argv[3]),0);

//				sm.EvaluateAt(pX[index],pY[index],0.0,pVor_s[index],valY,valZ,directionX, atof(argv[3]),2);
//				sm.EvaluateAt(pX[index],pY[index],0.0,pU_s[index],valY,valZ,directionY, atof(argv[3]),2);
//				sm.EvaluateAt(pX[index],pY[index],0.0,pV_s[index],valY,valZ,directionS, atof(argv[3]),2);
				
				pUx_p[index] = lexp->PhysEvaluate(coord,el_udx_phys);
				pVx_p[index] = lexp->PhysEvaluate(coord,el_vdx_phys);

				pUy_p[index] = lexp->PhysEvaluate(coord,el_udy_phys);
				pVy_p[index] = lexp->PhysEvaluate(coord,el_vdy_phys);

                pU_p[index] = lexp->PhysEvaluate(coord,el_u_phys);
                pV_p[index] = lexp->PhysEvaluate(coord,el_v_phys);

                //cout << locCoord[0] << "\t" << locCoord[1] << "\t" << pX[index] << "\t" << pY[index] << "\t" << pV[index] << "\t" << pP[index] << "\t" << pS[index] << "\t"<< i<< "\t"<< j << endl;

                //cout << locCoord[0] << "\t" << locCoord[1] << "\t" << pX[index] << "\t" << pY[index] << "\t" << pV[index] << "\t" << pP[index] << "\t" << pS[index] << endl;
                index++;
            }
        }
    }

	// 
	
	calVorticity( totPts, pUy_p, pVx_p, pVor_p);
	//calVorticity( totPts, pUy_s, pVx_s, pVor_s);

	cout << fname << "_"<<argv[2] << endl;
	NektarBaseClass k;
	k.writeNekArray(pE,fname+argv[2]+"_pE_EL.txt");
	k.writeNekArray(pX,fname+argv[2]+"_pX_EL.txt");
	k.writeNekArray(pY,fname+argv[2]+"_pY_EL.txt");

	k.writeNekArray(pVor_p,fname+argv[2]+"_pVor_p_EL.txt");



/*
	SmoothieSIAC2D sm(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]), atoi(argv[4])); 
	SmoothieSIAC2D sm_sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3])); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	Array<OneD,NekDouble> directionX(3,0.0), coord(3,0.0), directionY(3,0.0) ;
	directionX[0] = 1.0; directionY[1] =1.0;
	Array<OneD,NekDouble> pX(totPts), pY(totPts);
	Array<OneD,NekDouble> pU(totPts), pV(totPts), pP(totPts);
	Array<OneD,NekDouble> pUx(totPts), pVx(totPts), pPx(totPts), pAu(totPts);
	Array<OneD,NekDouble> pUy(totPts), pVy(totPts), pPy(totPts), pAv(totPts), pC(totPts),pI(totPts);
	Array<OneD,NekDouble> pU_p(totPts), pV_p(totPts), pP_p(totPts);
	Array<OneD,NekDouble> pUx_p(totPts), pVx_p(totPts), pPx_p(totPts), pAu_p(totPts);
	Array<OneD,NekDouble> pUy_p(totPts), pVy_p(totPts), pPy_p(totPts), pAv_p(totPts), pC_p(totPts),pI_p(totPts);

	for( int i =0; i < Nx; i++)
	{
		for(int j=0; j< Ny; j++)
		{
			int index = i*Nx+j;
			pX[index] = StartX + i*sx*(EndX-StartX);
			pY[index] = StartY + j*sy*(EndY-StartY);
			sm_sm.EvaluateAt(pX[index],pY[index],0.0,pU[index],valY,valZ,directionY, atof(argv[3]),0);
			sm_sm.EvaluateAt(pX[index],pY[index],0.0,pV[index],valY,valZ,directionY, atof(argv[3]),1);

			sm.EvaluateAt(pX[index],pY[index],0.0,pUx[index],valY,valZ,directionX, atof(argv[3]),0);
			sm.EvaluateAt(pX[index],pY[index],0.0,pUy[index],valY,valZ,directionY, atof(argv[3]),0);

			sm.EvaluateAt(pX[index],pY[index],0.0,pVx[index],valY,valZ,directionX, atof(argv[3]),1);
			sm.EvaluateAt(pX[index],pY[index],0.0,pVy[index],valY,valZ,directionY, atof(argv[3]),1);
			
			sm.EvaluateAt(pX[index],pY[index],0.0,pPx[index],valY,valZ,directionX, atof(argv[3]),2);
			sm.EvaluateAt(pX[index],pY[index],0.0,pPy[index],valY,valZ,directionY, atof(argv[3]),2);

			coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = 0.0;

			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(coord);
			int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elId);

			const Array<OneD,NekDouble> el_u_phys = u_phys.CreateWithOffset(u_phys,coeffOffset);
			const Array<OneD,NekDouble> el_v_phys = v_phys.CreateWithOffset(v_phys,coeffOffset);
			const Array<OneD,NekDouble> el_p_phys = p_phys.CreateWithOffset(p_phys,coeffOffset);

			const Array<OneD,NekDouble> el_udx_phys = udx_phys.CreateWithOffset(udx_phys,coeffOffset);
			const Array<OneD,NekDouble> el_vdx_phys = vdx_phys.CreateWithOffset(vdx_phys,coeffOffset);
			const Array<OneD,NekDouble> el_pdx_phys = pdx_phys.CreateWithOffset(pdx_phys,coeffOffset);

			const Array<OneD,NekDouble> el_udy_phys = udy_phys.CreateWithOffset(udy_phys,coeffOffset);
			const Array<OneD,NekDouble> el_vdy_phys = vdy_phys.CreateWithOffset(vdy_phys,coeffOffset);
			const Array<OneD,NekDouble> el_pdy_phys = pdy_phys.CreateWithOffset(pdy_phys,coeffOffset);
			
			const Array<OneD,NekDouble> el_au_phys = udy_phys.CreateWithOffset(au_phys,coeffOffset);
			const Array<OneD,NekDouble> el_av_phys = vdy_phys.CreateWithOffset(av_phys,coeffOffset);
			const Array<OneD,NekDouble> el_C_phys = pdy_phys.CreateWithOffset(vCa_phys,coeffOffset);
			const Array<OneD,NekDouble> el_V_phys = pdy_phys.CreateWithOffset(V_phys,coeffOffset);

			pU_p[index] = lexp->PhysEvaluate(coord,el_u_phys);
			pV_p[index] = lexp->PhysEvaluate(coord,el_v_phys);
			pP_p[index] = lexp->PhysEvaluate(coord,el_p_phys);

			pUx_p[index] = lexp->PhysEvaluate(coord,el_udx_phys);
			pVx_p[index] = lexp->PhysEvaluate(coord,el_vdx_phys);
			pPx_p[index] = lexp->PhysEvaluate(coord,el_pdx_phys);

			pUy_p[index] = lexp->PhysEvaluate(coord,el_udy_phys);
			pVy_p[index] = lexp->PhysEvaluate(coord,el_vdy_phys);
			pPy_p[index] = lexp->PhysEvaluate(coord,el_pdy_phys);
			
			pAu_p[index] = lexp->PhysEvaluate(coord,el_au_phys);
			pAv_p[index] = lexp->PhysEvaluate(coord,el_av_phys);
			pC_p[index]  = lexp->PhysEvaluate(coord,el_C_phys);
			
			pI_p[index]  = lexp->PhysEvaluate(coord,el_V_phys);

			//cout << j << endl;
		}
		cout << i << endl;
	}

	calCurvature(totPts, pU, pV, pUx, pUy, pVx, pVy, pAu, pAv, pC);
	calVorticity(totPts, pUy, pVx, pI);

	NektarBaseClass k;
	k.writeNekArray(pX,fname+"_"+argv[2]+"_D_"+argv[4]+"_pX_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pY,fname+"_"+argv[2]+"_D_"+argv[4]+"_pY_2D_OneSided2kp1"+argv[6]+".txt");

	k.writeNekArray(pU,fname+"_"+argv[2]+"_D_"+argv[4]+"_pU_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pV,fname+"_"+argv[2]+"_D_"+argv[4]+"_pV_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pP,fname+"_"+argv[2]+"_D_"+argv[4]+"_pP_2D_OneSided2kp1"+argv[6]+".txt");

	k.writeNekArray(pUx,fname+"_"+argv[2]+"_D_"+argv[4]+"_pUx_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pVx,fname+"_"+argv[2]+"_D_"+argv[4]+"_pVx_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pPx,fname+"_"+argv[2]+"_D_"+argv[4]+"_pPx_2D_OneSided2kp1"+argv[6]+".txt");

	k.writeNekArray(pUy,fname+"_"+argv[2]+"_D_"+argv[4]+"_pUy_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pVy,fname+"_"+argv[2]+"_D_"+argv[4]+"_pVy_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pPy,fname+"_"+argv[2]+"_D_"+argv[4]+"_pPy_2D_OneSided2kp1"+argv[6]+".txt");

	k.writeNekArray(pU_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pU_p_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pV_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pV_p_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pP_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pP_p_2D_OneSided2kp1"+argv[6]+".txt");

	k.writeNekArray(pUx_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pUx_p_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pVx_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pVx_p_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pPx_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pPx_p_2D_OneSided2kp1"+argv[6]+".txt");

	k.writeNekArray(pUy_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pUy_p_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pVy_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pVy_p_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pPy_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pPy_p_2D_OneSided2kp1"+argv[6]+".txt");

	k.writeNekArray(pAu,fname+"_"+argv[2]+"_D_"+argv[4]+"_pAu_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pAv,fname+"_"+argv[2]+"_D_"+argv[4]+"_pAv_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pC,fname+"_"+argv[2]+"_D_"+argv[4]+"_pC_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pI,fname+"_"+argv[2]+"_D_"+argv[4]+"_pI_2D_OneSided2kp1"+argv[6]+".txt");

	k.writeNekArray(pAu_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pAu_p_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pAv_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pAv_p_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pC_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pC_p_2D_OneSided2kp1"+argv[6]+".txt");
	k.writeNekArray(pI_p,fname+"_"+argv[2]+"_D_"+argv[4]+"_pI_p_2D_OneSided2kp1"+argv[6]+".txt");
*/
	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	//cout << "size of array: " << ar.num_elements()<< endl;	
	cout << "\t";
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
