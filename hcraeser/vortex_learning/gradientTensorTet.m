% find velocity gradient tensor
% given coords and their velocities.
function [c,V] = gradientTensorTet(coords, vel)

M = [1,coords(1,:);1,coords(2,:);1,coords(3,:);1,coords(4,:)];

c = zeros(3,1);
V = zeros(3,3);

u_r(1,:) = inv(M)*vel(:,1);

u_r(2,:) = inv(M)*vel(:,2);

u_r(3,:) = inv(M)*vel(:,3);

c = u_r(:,1);
V = u_r(:,2:end);

end