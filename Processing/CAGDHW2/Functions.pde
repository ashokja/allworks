
void mouseWheel(MouseEvent event)
{
  float e = event.getCount();
  ScreenXMin -= e;
  ScreenXMax += e;
  ScreenYMin -= e;
  ScreenYMax += e;
  for (int c =0; c< D_CPtAs.size(); c++)
  {
    ArrayList<Draggable> D_CPtA = D_CPtAs.get(c);
    ArrayList<PVector> CPtA = CPtAs.get(c);
    for (int pb =0; pb < D_CPtA.size(); pb++)
    {
      Draggable d = D_CPtA.get(pb);
      PVector p = CPtA.get(pb);
      d.x = CToS(p,1);
      d.y = CToS(p,2);
    }
  }
}

void MymouseWheel(float e)
{
    ScreenXMin -= e;
  ScreenXMax += e;
  ScreenYMin -= e;
  ScreenYMax += e;
  for (int c =0; c< D_CPtAs.size(); c++)
  {
    ArrayList<Draggable> D_CPtA = D_CPtAs.get(c);
    ArrayList<PVector> CPtA = CPtAs.get(c);
    for (int pb =0; pb < D_CPtA.size(); pb++)
    {
      Draggable d = D_CPtA.get(pb);
      PVector p = CPtA.get(pb);
      d.x = CToS(p,1);
      d.y = CToS(p,2);
    }
  }
}

void myDrawLines()
{
  line(ScreenX,150,ScreenX + CScreenX, 150);
  line(ScreenX,230,ScreenX + CScreenX, 230);
  line(ScreenX,350,ScreenX + CScreenX, 350);
  line(ScreenX,460,ScreenX + CScreenX, 460);
  line(ScreenX,520,ScreenX + CScreenX, 520);
  //line(ScreenX,10,ScreenX + CScreenX, ScreenY +  CScreenY);
}

void myDrawControlBox()
{  
  fill(128);
  rect( ScreenX ,0 ,CScreenX,ScreenY);
}


int CToS(PVector p, int cord)
{
  // cord 1 = x cord y = 1

    PVector p1 = new PVector(p.x,p.y,p.z);
   //new PVector((float)retx,(float)rety,1.0);
    PMatrix3D pM= new PMatrix3D();
    pM.rotateX(radians(CtetaX));
    pM.rotateY(radians(CtetaY));
    pM.rotateZ(radians(CtetaZ));
    pM.mult(p1,p1);
    int retx = (int)((p1.x-ScreenXMin)/(ScreenXMax-ScreenXMin)*((float)ScreenX));
    int rety =  (int)((p1.y-ScreenYMin)/(ScreenYMax-ScreenYMin)*((float)ScreenY));
  if( 1 == cord)
  {
    //int retx = (int)((p.x-ScreenXMin)/(ScreenXMax-ScreenXMin)*((float)ScreenX));
    //int rety =  (int)((p.y-ScreenYMin)/(ScreenYMax-ScreenYMin)*((float)ScreenY));
    return retx;
  }else
  {
    return rety;
  } 
}

float SToC(int sx,int sy, int cord)
{
  PVector p = new PVector(0.0,0.0,1.0);
  p.x = (float)( sx*(ScreenXMax-ScreenXMin)/((float)ScreenX) + ScreenXMin );
  p.y = (float)( sy*(ScreenYMax-ScreenYMin)/((float)ScreenY) + ScreenYMin );
//  PVector p1 = new PVector(p.x,p.y,p.z);//new PVector((float)retx,(float)rety,1.0);
  PMatrix3D pM= new PMatrix3D();
  pM.rotateZ(radians(-CtetaZ));
  pM.rotateY(radians(-CtetaY));
  pM.rotateX(radians(-CtetaX));
  pM.mult(p,p);
  
  if( 1 == cord)
  {
    return p.x;
  }else
  {
    return p.y;
  } 
}


void myDrawLines(ArrayList<PVector> points)
{
  if (myGLinesVisible)
  {
//    println(myGLinesVisible);
    for (int i =1; i < points.size(); i++)
    {
      PVector p1,p2;
      p1 = points.get(i-1);
      p2 = points.get(i);
      //line(CToS(p1.x,1),CToS(p1.y,2),CToS(p2.x,1),CToS(p2.y,2));
      line(CToS(p1,1),CToS(p1,2),CToS(p2,1),CToS(p2,2));
    }
  }
}

void myDrawButtons()
{
  if ( myGCPVisible)
  {
  //  println(points_b.size());
    for (int c =0; c < CPtAs.size(); c++)
    {
      ArrayList<Draggable> D_CPtA = D_CPtAs.get(c);
      ArrayList<PVector> CPtA = CPtAs.get(c);
      for (int i =0; i < D_CPtA.size(); i++)
      {
        Draggable d = D_CPtA.get(i);
        PVector p = CPtA.get(i);
        //d.x = CToS(p.x/p.z,1);
        //d.y = CToS(p.y/p.z,1);
        d.rollover(mouseX,mouseY);
        d.drag(mouseX,mouseY);
        d.display(c,i);
      }
    }
  }
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    String filepath = selection.getAbsolutePath();
    println("User selected " + filepath);
    // load file here
    myLoadFile(filepath);
//    txtFile = loadStrings(filepath);
  }
}


void AddPoint()
{
  if (GAddPoint)
  {
      int nCnum = (int) myGCN_AP.getValue();
      //check if nCnum is good curve.
      nCnum = nCnum -1;
      if (nCnum >= CPtAs.size())
      {
        print("Choose the  curve which exits.");
        return ;
      }
      ArrayList<PVector> CPtA = CPtAs.get(nCnum);
      ArrayList<Draggable> D_CPtA = D_CPtAs.get(nCnum);
      ArrayList<Float> KnotA = KnotAs.get(nCnum);
      int mx = mouseX;
      int my = mouseY;
      //find closest point
      PVector p = new PVector(SToC(mx,my,1),SToC(mx,my,2),1.0);
      float dist =999999999.0;
      int index = -1;
      for(int i=0; i< CPtA.size(); i++)
      {
        if( dist > PVector.sub(CPtA.get(i),p).magSq())
        {
          dist = PVector.sub(CPtA.get(i),p).magSq();
          index = i;
        }
      }
      print(index+" "+ dist);
      // Add new point after i;
      CPtA.add(index+1,p);
      Draggable d = new Draggable(mx, my,10,10);
      D_CPtA.add(index+1,d);
      NumCPts.set(nCnum,NumCPts.get(nCnum)+1);
      
      // Modify Knot Vector also
      KnotA.add(index+Degs.get(nCnum)+1,(KnotA.get(index+Degs.get(nCnum))+KnotA.get(index+Degs.get(nCnum)+1))/2.0);
      print("\n "+KnotA);
  }
}



