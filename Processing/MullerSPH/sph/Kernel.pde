static class Kernel
{
  static float PI = 3.14;
  static float spikyKernel( float R, float r)
  {
    float q = r/R;
    if (q > 1.0)
    {
      return 0.0;
    } else
    {
      float d = 1.0 - q;
      return 10.0/(PI*R*R) * d*d*d;
    }
  } 

  static float standardKernel(float R, float r)
  {
    float q = r/R;
    float RR = R*R;
    float qq = r*r/RR;
    if ( q>1)
    {
      return 0.0;
    } else
    {
      float dd = 1.0 - qq;
      return 4.0/(PI*RR)*dd*dd*dd;
    }
  }

  float smoothKernel(float R, float r)
  {
    float q = r/R;
    if (q>1.0)
    {
      return 0.0;
    } else
    {
      return 1.0 - q*q;
    }
  }

  float spikyKernelGradient(float R, float r)
  {
    float q = r/R;
    if ( q > 1.0)
    {
      return 0.0;
    } else
    {
      float d = 1.0-q;
      float RR = R*R;
      return -30.0/(PI*RR)*d*d;
    }
  }

  float viscosityKernelLapcian(float R, float r)
  {
    float q = r/R;
    if ( q>1.0 )
    {
      return 0.0;
    } else
    {
      float d = 1.0-q;
      float RR = R*R;
      return 60.0/(11.0*PI*RR)*d;
    }
  }
}

