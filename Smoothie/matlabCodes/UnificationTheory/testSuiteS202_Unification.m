clear;
close all;
% test 
rates = [40];
%rates = [20,40,60,80];

poly =3;
order = poly;
count =1;
fname = '_quad_2DMesh_';
res = 161;
for r = rates
    fname = strcat('P',int2str(poly),'_quad_2DMesh_',int2str(r),'_',int2str(order),'_R_',int2str(res));
    xfname = strcat('data/',fname,'_pX_2D.txt' );
    yfname = strcat('data/',fname,'_pY_2D.txt' );
    ffname = strcat('data/',fname,'_pV_2D.txt' );
    efname = strcat('data/',fname,'_pP_2D.txt' );
    sfname = strcat('data/',fname,'_pS_2D.txt' );

    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');
    fval = textread(ffname, '%f\t');
    pval = textread(efname, '%f\t');
    sval = textread(sfname, '%f\t');

    s = floor( (max(size(xpos)))^(1/2) +0.1);
    xVpos = reshape(xpos,[s s]);
    yVpos = reshape(ypos,[s s]);
    fVval = reshape(fval,[s s]);
    pVval = reshape(pval,[s s]);
    sVval = reshape(sval,[s s]);
    
    
    figure(1)
    surf(xVpos,yVpos, fVval);

    figure(2)
    surf(xVpos,yVpos, abs(fVval-pVval)); title('projection error')

    figure(3)
    surf(xVpos,yVpos, abs(fVval-sVval)); title('LSIAC error')

    disp('Projection Error');
    disp(sum(abs(fval-pval))./max(size(xpos)));
    disp(max(abs(fval-pval)));
    disp('LSIAC Error');
    disp(sum(abs(fval-sval))./max(size(xpos)));
    disp(max(abs(fval-sval)));
    
%     figure(1)
%     plot(xpos,log10(abs(fval-pval)),xpos,log10(abs(fval-sval)));
%     
%     figure(2)
%     plot(xpos,log10(abs(fval-sval)),'DisplayName',strcat('No of ele:',int2str(r)));
%     hold on;
% 
%     figure(3)
%     plot(xpos,pval);
    
%     p_avg_err(count) = sum(abs(fval-pval))/max(size(xpos));
%     s_avg_err(count) = sum(abs(fval-sval))/max(size(xpos));
%     
%     p_max_err(count) = max(abs(fval-pval));
%     s_max_err(count) = max(abs(fval-sval));
    count = count+1;
end
