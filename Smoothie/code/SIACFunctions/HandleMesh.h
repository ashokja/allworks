#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "NektarBaseClass.h"
using namespace std;


/// This is base class to handle different type of meshes.

/** Each subclass can handle one particular format of the mesh.
	This class allows users to add their own mesh format in the future purpose.
*/

class HandleMesh: public NektarBaseClass{
	protected:
	public:

};
