function testDividedDiffSpecialCase
    % test position dependent and with General B-spline.
%     clear all;
     close all;
    degree = 4;
    j =degree;
    h = @monomial;
    % knots are given with one general B-Spline.

    % Get a knot matrix.

    % knot matrix for sym filter of given degree and j
    tknots = -(degree+1)/2 - (degree+j)/2:1: (degree+1)/2+(degree+j)/2;
    tk_size = max(size(tknots));
    tmatrix = zeros(degree+j+1,degree+2);

    for loop = 1:tk_size-degree-1
        tmatrix(loop,:) = tknots(loop:loop+degree+1);
    end

    tmatrix= tmatrix-max(max(tmatrix));
    tmatrix(end+1,:) = zeros(1,degree+2);
    tmatrix(end,1) = tmatrix(end-1,end-1);
    tmatrix

    % [~,c] = bulidM_0GiventMatrix(degree,tmatrix)
    % 
    % 
    % %x_list for solution to be evaluated.
    % x_list = (min(min(tmatrix))-1.0:.01:max(max(tmatrix))+1.0);
    % v_list = zeros(size(x_list));
    % 
    % %points to calculate for central Bsplines.
    % for loop = 1:max(size(c))
    % %    delta = loop-degree-1;
    %     v_list = v_list+ c(loop)*CalBernUsingDividedDiff( degree,x_list,tmatrix(loop,:) );
    % end
    % 
    % plot(x_list,v_list); hold on;
    %
    
    x_list = (min(min(tmatrix))-1.0:.01:max(max(tmatrix))+1.0);
%    v_list = zeros(size(x_list));
    
%     count =1;
%     for xx = x_list
%         delta = 1;
%         %v_list(count) = dividedDiff(1,degree+2,tmatrix(end-1,:),xx,h,degree+delta+1);
%         count = count+1;
%     end
    v_list = CalBernUsingDividedDiff( degree,x_list,tmatrix(end,:) );
    
    
    function result = monomial(t,x,der,D)
        y = t-x;
        result = prod(D-der+1:D)*(y)^(D-der);
    end

    plot(x_list,v_list);
end