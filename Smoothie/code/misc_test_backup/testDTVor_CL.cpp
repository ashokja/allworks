#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"
#include <iomanip>

#include <boost/timer.hpp>


using namespace SIACUtilities;
//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
using namespace std;

void calCurvature(int nq, 
	Array<OneD,NekDouble> &u_phys, Array<OneD,NekDouble> &v_phys,Array<OneD,NekDouble> &w_phys,
    Array<OneD,NekDouble> &udx_phys, Array<OneD,NekDouble> &udy_phys, Array<OneD,NekDouble> &udz_phys,
    Array<OneD,NekDouble> &vdx_phys, Array<OneD,NekDouble> &vdy_phys, Array<OneD,NekDouble> &vdz_phys,
    Array<OneD,NekDouble> &wdx_phys, Array<OneD,NekDouble> &wdy_phys, Array<OneD,NekDouble> &wdz_phys,
    Array<OneD,NekDouble> &au_phys, Array<OneD,NekDouble> &av_phys,  Array<OneD,NekDouble> &aw_phys,
    Array<OneD,NekDouble> &vCa_x, Array<OneD,NekDouble> &vCa_y, Array<OneD,NekDouble> &vCa_z,
	Array<OneD,NekDouble> &curNorm2 )
{
    Array<OneD,NekDouble> temp1(nq), temp2(nq), temp3(nq);
    // a acceleration
    Vmath::Vmul(nq, udx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, udy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, udz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, au_phys,1);
    Vmath::Vadd(nq, temp3, 1,au_phys,1, au_phys,1);
   
    Vmath::Vmul(nq, vdx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, vdy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, vdz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, av_phys,1);
    Vmath::Vadd(nq, temp3, 1,av_phys,1, av_phys,1);

    Vmath::Vmul(nq, wdx_phys,1, u_phys,1, temp1,1);
    Vmath::Vmul(nq, wdy_phys,1, v_phys,1, temp2,1);
    Vmath::Vmul(nq, wdz_phys,1, w_phys,1, temp3,1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, aw_phys,1);
    Vmath::Vadd(nq, temp3, 1,aw_phys,1, aw_phys,1);


    // cross v a  = curvature.
    Vmath::Vmul(nq, v_phys,1,aw_phys,1, temp1,1);
    Vmath::Vmul(nq, w_phys,1,av_phys,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_x,1);

    Vmath::Vmul(nq, w_phys,1,au_phys,1, temp1,1);
    Vmath::Vmul(nq, u_phys,1,aw_phys,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_y,1);

    Vmath::Vmul(nq, u_phys,1,av_phys,1, temp1,1);
    Vmath::Vmul(nq, v_phys,1,au_phys,1, temp2,1);
    Vmath::Vsub(nq, temp1,1, temp2,1, vCa_z,1);

	//Curvature norm
	Vmath::Vmul(nq, vCa_x,1,vCa_x,1, temp1, 1);
	Vmath::Vmul(nq, vCa_y,1,vCa_y,1, temp2, 1);
	Vmath::Vmul(nq, vCa_z,1,vCa_z,1, temp3, 1);
    Vmath::Vadd(nq, temp1, 1,temp2,1, curNorm2,1);
    Vmath::Vadd(nq, temp3, 1,curNorm2,1, curNorm2,1);

    return;
}

void calVortMag(int nq,
    Array<OneD,NekDouble> &VorX, Array<OneD,NekDouble> &VorY, Array<OneD,NekDouble> &VorZ,
    Array<OneD,NekDouble> &VorM)
{
    Array<OneD,NekDouble> temp1(nq), temp2(nq), temp3(nq);
	Vmath::Vpow(nq, VorX,1,2.0,temp1,1);
	Vmath::Vpow(nq, VorY,1,2.0,temp2,1);
	Vmath::Vpow(nq, VorZ,1,2.0,temp3,1);

	Vmath::Vadd(nq, temp1,1,temp2,1,temp1,1);
	Vmath::Vadd(nq, temp1,1,temp3,1,temp1,1);
	Vmath::Vsqrt(nq, temp1,1,VorM,1);

}

void calVorticity(int nq,
    Array<OneD,NekDouble> &udx_phys, Array<OneD,NekDouble> &udy_phys, Array<OneD,NekDouble> &udz_phys,
    Array<OneD,NekDouble> &vdx_phys, Array<OneD,NekDouble> &vdy_phys, Array<OneD,NekDouble> &vdz_phys,
    Array<OneD,NekDouble> &wdx_phys, Array<OneD,NekDouble> &wdy_phys, Array<OneD,NekDouble> &wdz_phys,
    Array<OneD,NekDouble> &Vx,Array<OneD,NekDouble> &Vy,Array<OneD,NekDouble> &Vz )
{
    Vmath::Vsub(nq, wdy_phys,1, vdz_phys,1,Vx ,1);
    Vmath::Vsub(nq, udz_phys,1, wdx_phys,1,Vy ,1);
    Vmath::Vsub(nq, vdx_phys,1, udy_phys,1,Vz ,1);
    return;
}

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg 0/1"<<endl<<"\t 0 for writing data"<< endl<<"\t 1 for reading data." << endl;
		cout << "3nd arg polynomial degree filter you want to apply" << endl;
		cout << "4rd arg meshscaling you want to use." << endl;
	//	cout << "4th Angle of SIAC derivative in deg -90 to 90 allowed" << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();

//	HNM3D->LoadData( fname+ ".chk",var);

	HNM3D->LoadMesh(var[0]);
	HNM3D->LoadMesh(var[1]);
	HNM3D->LoadMesh(var[2]);

	HNM3D->LoadExpListIntoRTree();
	
	int tNquadPts = HNM3D->m_expansions[0]->GetTotPoints();
	int tNCoeffs= HNM3D->m_expansions[0]->GetNcoeffs();
	cout << "fPhys:" << tNquadPts<< endl;
	cout << "fCoeffs:" << HNM3D->m_expansions[0]->GetNcoeffs()<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);
	Array<OneD,NekDouble> u(tNquadPts);
	Array<OneD,NekDouble> v(tNquadPts);
	Array<OneD,NekDouble> w(tNquadPts);

	
	switch( HNM3D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
	NektarBaseClass NBC;	

	if( 0 == atoi(argv[2]) )
	{
		NBC.writeNekArray(xc0,fname+"_xc0."+"txt");
		NBC.writeNekArray(xc1,fname+"_xc1."+"txt");
		NBC.writeNekArray(xc2,fname+"_xc2."+"txt");
		return 0;
	}
	else if (2 == atoi(argv[2]) )
	{
		cout << "came for evaluation error norms" << endl;
		// load  dg derivative components.
		// load  actual derivative components.
		// load  saic derivative components.
		
		Array<OneD,NekDouble> ux_DG(tNquadPts), uy_DG(tNquadPts), uz_DG(tNquadPts);
		Array<OneD,NekDouble> vx_DG(tNquadPts), vy_DG(tNquadPts), vz_DG(tNquadPts);
		Array<OneD,NekDouble> wx_DG(tNquadPts), wy_DG(tNquadPts), wz_DG(tNquadPts);
	
		Array<OneD,NekDouble> ux_SI(tNquadPts), uy_SI(tNquadPts), uz_SI(tNquadPts);
		Array<OneD,NekDouble> vx_SI(tNquadPts), vy_SI(tNquadPts), vz_SI(tNquadPts);
		Array<OneD,NekDouble> wx_SI(tNquadPts), wy_SI(tNquadPts), wz_SI(tNquadPts);
		
		Array<OneD,NekDouble> VorX_DG(tNquadPts), VorY_DG(tNquadPts), VorZ_DG(tNquadPts);
		Array<OneD,NekDouble> VorX_SI(tNquadPts), VorY_SI(tNquadPts), VorZ_SI(tNquadPts);
		Array<OneD,NekDouble> VorX(tNquadPts), VorY(tNquadPts), VorZ(tNquadPts);
	
		Array<OneD,NekDouble> VorM(tNquadPts), VorM_DG(tNquadPts), VorM_SI(tNquadPts);
		
		NBC.readNekArray(ux_DG, fname+"_S_"+argv[3]+"ux_DG.txt");
		NBC.readNekArray(uy_DG, fname+"_S_"+argv[3]+"uy_DG.txt");
		NBC.readNekArray(uz_DG, fname+"_S_"+argv[3]+"uz_DG.txt");
		NBC.readNekArray(ux_SI, fname+"_S_"+argv[3]+"ux_SI.txt");
		NBC.readNekArray(uy_SI, fname+"_S_"+argv[3]+"uy_SI.txt");
		NBC.readNekArray(uz_SI, fname+"_S_"+argv[3]+"uz_SI.txt");
		
		NBC.readNekArray(vx_DG, fname+"_S_"+argv[3]+"vx_DG.txt");
		NBC.readNekArray(vy_DG, fname+"_S_"+argv[3]+"vy_DG.txt");
		NBC.readNekArray(vz_DG, fname+"_S_"+argv[3]+"vz_DG.txt");
		NBC.readNekArray(vx_SI, fname+"_S_"+argv[3]+"vx_SI.txt");
		NBC.readNekArray(vy_SI, fname+"_S_"+argv[3]+"vy_SI.txt");
		NBC.readNekArray(vz_SI, fname+"_S_"+argv[3]+"vz_SI.txt");

		NBC.readNekArray(wx_DG, fname+"_S_"+argv[3]+"wx_DG.txt");
		NBC.readNekArray(wy_DG, fname+"_S_"+argv[3]+"wy_DG.txt");
		NBC.readNekArray(wz_DG, fname+"_S_"+argv[3]+"wz_DG.txt");
		NBC.readNekArray(wx_SI, fname+"_S_"+argv[3]+"wx_SI.txt");
		NBC.readNekArray(wy_SI, fname+"_S_"+argv[3]+"wy_SI.txt");
		NBC.readNekArray(wz_SI, fname+"_S_"+argv[3]+"wz_SI.txt");

		NBC.readNekArray(VorX, fname+"_VorX.txt");
		NBC.readNekArray(VorY, fname+"_VorY.txt");
		NBC.readNekArray(VorZ, fname+"_VorZ.txt");

		calVorticity(tNquadPts , ux_DG, uy_DG, uz_DG, vx_DG, vy_DG, vz_DG, wx_DG,wy_DG,wz_DG, VorX_DG,VorY_DG,VorZ_DG);
		calVorticity(tNquadPts , ux_SI, uy_SI, uz_SI, vx_SI, vy_SI, vz_SI, wx_SI,wy_SI,wz_SI, VorX_SI,VorY_SI,VorZ_SI);
		
		calVortMag(tNquadPts, VorX,VorY,VorZ, VorM);
		calVortMag(tNquadPts, VorX_DG,VorY_DG,VorZ_DG, VorM_DG);
		calVortMag(tNquadPts, VorX_SI,VorY_SI,VorZ_SI, VorM_SI);
	
		NekDouble Vor_Linf_DG = HNM3D->m_expansions[0]->Linf( VorM, VorM_DG);
		NekDouble Vor_Linf_SI = HNM3D->m_expansions[0]->Linf( VorM, VorM_SI);

		NekDouble Vor_L2_DG = HNM3D->m_expansions[0]->L2( VorM, VorM_DG);
		NekDouble Vor_L2_SI = HNM3D->m_expansions[0]->L2( VorM, VorM_SI);

		cout << "L2 DG error :\t" << Vor_L2_DG << endl;	
		cout << "L2 SI error :\t" << Vor_L2_SI << endl;

		cout << "Linf DG error :\t" << Vor_Linf_DG << endl;	
		cout << "Linf SI error :\t" << Vor_Linf_SI << endl;

		return 0;
	}
	else
	{
		NBC.readNekArray(u, fname+"_u.txt");
		NBC.readNekArray(v, fname+"_v.txt");
		NBC.readNekArray(w, fname+"_w.txt");
		//k.printNekArray(v,0);
	}

	// Now use Newton-Rapson to find the zero u and zero v.
	
	HNM3D->m_expansions[0]->FwdTrans_IterPerExp( u,HNM3D->m_expansions[0]->UpdateCoeffs());
	HNM3D->m_expansions[0]->BwdTrans_IterPerExp( HNM3D->m_expansions[0]->GetCoeffs(),
			HNM3D->m_expansions[0]->UpdatePhys());
	
	HNM3D->m_expansions[1]->FwdTrans_IterPerExp( v,HNM3D->m_expansions[1]->UpdateCoeffs());
	HNM3D->m_expansions[1]->BwdTrans_IterPerExp( HNM3D->m_expansions[1]->GetCoeffs(),
			HNM3D->m_expansions[1]->UpdatePhys());
	
	HNM3D->m_expansions[2]->FwdTrans_IterPerExp( w,HNM3D->m_expansions[2]->UpdateCoeffs());
	HNM3D->m_expansions[2]->BwdTrans_IterPerExp( HNM3D->m_expansions[2]->GetCoeffs(),
			HNM3D->m_expansions[2]->UpdatePhys());


	Array<OneD,NekDouble> u_DG = HNM3D->m_expansions[0]->GetPhys();	
	Array<OneD,NekDouble> v_DG = HNM3D->m_expansions[1]->GetPhys();
	Array<OneD,NekDouble> w_DG = HNM3D->m_expansions[2]->GetPhys();

	Array<OneD,NekDouble> ux_DG(tNquadPts), uy_DG(tNquadPts), uz_DG(tNquadPts);
	Array<OneD,NekDouble> vx_DG(tNquadPts), vy_DG(tNquadPts), vz_DG(tNquadPts);
	Array<OneD,NekDouble> wx_DG(tNquadPts), wy_DG(tNquadPts), wz_DG(tNquadPts);
	
	Array<OneD,NekDouble> ux_SI(tNquadPts), uy_SI(tNquadPts), uz_SI(tNquadPts);
	Array<OneD,NekDouble> vx_SI(tNquadPts), vy_SI(tNquadPts), vz_SI(tNquadPts);
	Array<OneD,NekDouble> wx_SI(tNquadPts), wy_SI(tNquadPts), wz_SI(tNquadPts);

	HNM3D->m_expansions[0]->PhysDeriv( u_DG, ux_DG, uy_DG, uz_DG);
	HNM3D->m_expansions[1]->PhysDeriv( v_DG, vx_DG, vy_DG, vz_DG);
	HNM3D->m_expansions[2]->PhysDeriv( w_DG, wx_DG, wy_DG, wz_DG);

	Array<OneD,NekDouble> VorX_DG(tNquadPts), VorY_DG(tNquadPts), VorZ_DG(tNquadPts);
	Array<OneD,NekDouble> VorX_SI(tNquadPts), VorY_SI(tNquadPts), VorZ_SI(tNquadPts);

	HNM3D->m_Arrays.push_back(u_DG);
	HNM3D->m_Arrays.push_back(v_DG);
	HNM3D->m_Arrays.push_back(w_DG);

	
	calVorticity(tNquadPts , ux_DG, uy_DG, uz_DG, vx_DG, vy_DG, vz_DG, wx_DG,wy_DG,wz_DG, VorX_DG,VorY_DG,VorZ_DG);

	SmoothieSIAC3D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[3]), atof(argv[4]), 1);	
	NekDouble scaling = atof(argv[4]), valY,valZ;
	Array<OneD,NekDouble> dirX(3,0.0), dirY(3,0.0), dirZ(3,0.0);
	dirX[0] = 1.0; dirY[1] = 1.0; dirZ[2] = 1.0;

boost::timer t;
		
	// loop through all points to calculate siac derivatives.
	//for (int i = 0; i< 100; i++)
	for (int i = 0; i< tNquadPts; i++)
	{
//		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], ux_SI[i], valY, valZ, dirX, scaling,0);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], uy_SI[i], valY, valZ, dirY, scaling,0);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], uz_SI[i], valY, valZ, dirZ, scaling,0);
		
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], vx_SI[i], valY, valZ, dirX, scaling,1);
//		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], vy_SI[i], valY, valZ, dirY, scaling,1);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], vz_SI[i], valY, valZ, dirZ, scaling,1);
		
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], wx_SI[i], valY, valZ, dirX, scaling,2);
		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], wy_SI[i], valY, valZ, dirY, scaling,2);
//		smD.EvaluateAt(xc0[i], xc1[i], xc2[i], wz_SI[i], valY, valZ, dirZ, scaling,2);
	}	

double s = t.elapsed();
        std::cout << "time taken : "<< s << " sec"<< std::endl;

	calVorticity(tNquadPts , ux_SI, uy_SI, uz_SI, vx_SI, vy_SI, vz_SI, wx_SI,wy_SI,wz_SI, VorX_SI,VorY_SI,VorZ_SI);


	NBC.writeNekArray(ux_DG,fname+"_S_"+argv[3]+"ux_DG.txt");
	NBC.writeNekArray(uy_DG,fname+"_S_"+argv[3]+"uy_DG.txt");
	NBC.writeNekArray(uz_DG,fname+"_S_"+argv[3]+"uz_DG.txt");
	
	NBC.writeNekArray(vx_DG,fname+"_S_"+argv[3]+"vx_DG.txt");
	NBC.writeNekArray(vy_DG,fname+"_S_"+argv[3]+"vy_DG.txt");
	NBC.writeNekArray(vz_DG,fname+"_S_"+argv[3]+"vz_DG.txt");
	
	NBC.writeNekArray(wx_DG,fname+"_S_"+argv[3]+"wx_DG.txt");
	NBC.writeNekArray(wy_DG,fname+"_S_"+argv[3]+"wy_DG.txt");
	NBC.writeNekArray(wz_DG,fname+"_S_"+argv[3]+"wz_DG.txt");
	
	NBC.writeNekArray(ux_SI,fname+"_S_"+argv[3]+"ux_SI.txt");
	NBC.writeNekArray(uy_SI,fname+"_S_"+argv[3]+"uy_SI.txt");
	NBC.writeNekArray(uz_SI,fname+"_S_"+argv[3]+"uz_SI.txt");
	
	NBC.writeNekArray(vx_SI,fname+"_S_"+argv[3]+"vx_SI.txt");
	NBC.writeNekArray(vy_SI,fname+"_S_"+argv[3]+"vy_SI.txt");
	NBC.writeNekArray(vz_SI,fname+"_S_"+argv[3]+"vz_SI.txt");
	
	NBC.writeNekArray(wx_SI,fname+"_S_"+argv[3]+"wx_SI.txt");
	NBC.writeNekArray(wy_SI,fname+"_S_"+argv[3]+"wy_SI.txt");
	NBC.writeNekArray(wz_SI,fname+"_S_"+argv[3]+"wz_SI.txt");


	NBC.writeNekArray(VorX_DG,fname+"_S_"+argv[3]+"VorX_DG.txt");
	NBC.writeNekArray(VorY_DG,fname+"_S_"+argv[3]+"VorY_DG.txt");
	NBC.writeNekArray(VorZ_DG,fname+"_S_"+argv[3]+"VorZ_DG.txt");


	NBC.writeNekArray(VorX_SI,fname+"_S_"+argv[3]+"VorX_SI.txt");
	NBC.writeNekArray(VorY_SI,fname+"_S_"+argv[3]+"VorY_SI.txt");
	NBC.writeNekArray(VorZ_SI,fname+"_S_"+argv[3]+"VorZ_SI.txt");
	
	// Need to add code to compute error.


	//NektarBaseClass k;
	return 0;
}

