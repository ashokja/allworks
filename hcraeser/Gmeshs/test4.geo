Point(1) = {0, 0, 0, 1.0};
Point(2) = {0, 1, 0, 1.0};
Point(3) = {4, 0, 0, 1.0};
Point(4) = {4, 1, 0, 1.0};
Point(5) = {0.5, 0.5, 0, 1.0};
Point(6) = {0.5, 0.55, 0, 1.0};
Point(7) = {0.5, 0.45, 0, 1.0};
Point(8) = {0.45, 0.5, 0, 1.0};
Point(9) = {0.55, 0.5, 0, 1.0};
Line(1) = {1, 3};
Line(2) = {3, 4};
Line(3) = {4, 2};
Line(4) = {2, 1};
Circle(5) = {7, 5, 9};
Circle(6) = {9, 5, 6};
Circle(7) = {6, 5, 8};
Circle(8) = {8, 5, 7};
Line Loop(9) = {6, 7, 8, 5};
Line Loop(10) = {3, 4, 1, 2};
Plane Surface(11) = {9, 10};
Extrude {0, 0, .4} {
  Surface{11};
}
Line Loop(54) = {14, 15, 16, 13};
Plane Surface(55) = {54};
Plane Surface(56) = {9};
Extrude {0, 0, 0.5} {
  Surface{55, 53};
}
Extrude {0, 0, -0.5} {
  Surface{56, 11};
}
Physical Surface(185) = {115, 48, 179};
Physical Surface(186) = {107, 40, 171};
Physical Surface(187) = {111, 44, 175, 120, 119, 52, 183, 78, 184, 142};
Physical Surface(188) = {28, 36, 24, 32, 56, 55};
Physical Volume(189) = {2, 4, 5, 1, 3};
