import controlP5.*;
ControlP5 myGCP5;
ArrayList<ArrayList<Draggable>> curves_b;
Draggable d;

ArrayList<ArrayList<PVector>> curves;
ArrayList<PVector> myGCRVPoints; 
int myGNumCRVPoints;
//ArrayList<PVector> points = new ArrayList<PVector>(5);
int myGLoadedFileCRV ;
color [] colors = new color[7];
int myGNextLine =0;
int myGNumOfCurves = 0;
int myGNumOfPoints =0;
PVector[][] points2D;
PVector p1,p2,p0;
int myGLinesVisible = 1;
int myGCPVisible = 1;
float ScreenXMin = -15;
float ScreenXMax = 15;
float ScreenYMin = -15;
float ScreenYMax = 15;
int ScreenX = 800;
int ScreenY = 800;
int CScreenX = 200;
int CScreenY = 0;

void setup() 
{
  size(ScreenX + CScreenX, ScreenY +  CScreenY);
  String[] lines = myLoadFile("del1.dat");
  myGLoadedFileCRV = 0;
  println("there are "+ lines.length + " lines");
  myGCP5Setup();
  d = new Draggable(50,50,20,20);
  smooth(); 
}


void draw()
{
  //noFill();
  stroke(255, 102, 0);
  //stroke(0, 0, 0);
  background(255, 255, 255);
  //bezier(85, 20, 10, 10, 90, 90, 15, 80);
  if ( 1 == myGLoadedFileCRV)
  {
    myDrawLines(myGCRVPoints);
  }
  for (int c =0; c< curves.size(); c++)
  {
      //DrawLines

      myDrawLines( curves.get(c));
      //DrawButtons
      myDrawButtons( );
      ArrayList<PVector> points = curves.get(c);
      points2D = new PVector[points.size()][points.size()];
      //println(points.size());
      for (int T =0; T< 1001 ; T++)
      {
        for(int i =0; i< points.size(); i++)
        {
          for(int j=0; j< points.size(); j++)
          {
            PVector p = new PVector(0.0,0.0,-101.0);
            points2D[i][j] = p;
          }
        }
        float t = T/1000.0;
        //println(curves.get(c));
        PVector d = myPlotAt( curves.get(c), t, 0, curves.get(c).size()-1);
        point(CToS(d.x/d.z,'1'),CToS(d.y/d.z,'2'));
      }   
  } 
  myDrawControlBox();
}




void mousePressed() 
{
  for (int c =0; c< myGNumOfCurves; c++)
  {
    ArrayList<Draggable> points_b = curves_b.get(c);
    for (int pb =0; pb < points_b.size(); pb++)
    {
      points_b.get(pb).clicked(mouseX, mouseY);
    }
  }
  
  d.clicked(mouseX,mouseY);
}

void mouseReleased() 
{
  d.stopDragging();
  for (int c =0; c< myGNumOfCurves; c++)
  {
    ArrayList<Draggable> points_b = curves_b.get(c);
    for (int pb =0; pb < points_b.size(); pb++)
    {
      points_b.get(pb).stopDragging();
    }
  }
  
}


void mouseWheel(MouseEvent event)
{
  float e = event.getCount();
  ScreenXMin -= e;
  ScreenXMax += e;
  ScreenYMin -= e;
  ScreenYMax += e;
  for (int c =0; c< myGNumOfCurves; c++)
  {
    ArrayList<Draggable> points_b = curves_b.get(c);
    ArrayList<PVector> points = curves.get(c);
    for (int pb =0; pb < points_b.size(); pb++)
    {
      Draggable d = points_b.get(pb);
      PVector p = points.get(pb);
      d.x = CToS(p.x/p.z,1);
      d.y = CToS(p.y/p.z,2);
    }
  } 
}
