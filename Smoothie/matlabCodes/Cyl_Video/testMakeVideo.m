% Create video from images.
close all;
clear all;

workingDir = 'frame_EL/';
VideoName = 'shuttle_out_EL.avi';


%workingDir = 'frames_DG_2/';
%VideoName = 'shuttle_out_DG_2.avi';

%workingDir = 'frames_2/';
%VideoName = 'shuttle_out_2.avi';

imageNames = dir(fullfile(workingDir,'*.jpg'));
imageNames = {imageNames.name}';

outputVideo = VideoWriter(fullfile(VideoName));
outputVideo.FrameRate = 40;
open(outputVideo)

for ii = 1:length(imageNames)
    disp(fullfile(workingDir,imageNames{ii}));
   img = imread(fullfile(workingDir,imageNames{ii}));
   writeVideo(outputVideo,img)
end

close(outputVideo);
