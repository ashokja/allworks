clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];

filePoly = 3;
poly = 2;

addpath('../')

%fig properties.
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize


r = filePoly
filename = strcat( '../datatxt/Cy',int2str(r),'l_Re500_');
filenameFrPrint = strcat( 'Cy',int2str(r),'l_Re500_');
%    filenameFrPrint = strcat( 'Cy',int2str(r),'l_Re500_','Vor2ndKind_');
    
offsets = strcat( filename,'Vor2ndKind_Offset.txt');
elemts = strcat( filename,'Vor2ndKind_ElmT.txt');

xc0fname = strcat( filename,'Vor2ndKind_xc0.txt');
xc1fname = strcat( filename,'Vor2ndKind_xc1.txt');
dgVorfname = strcat( filename,'Vor2ndKind_DGVor.txt');
Vor2ndKindfname = strcat( filename,'Vor2ndKind_Vor2.txt');

Elxfname = strcat( filename,'Vor2ndKind_Elx.txt');
Elyfname = strcat( filename,'Vor2ndKind_Ely.txt');
Elvfname = strcat( filename,'Vor2ndKind_Elv.txt');
Elidfname = strcat( filename,'Vor2ndKind_Elid.txt');

%xc1fname = strcat( filename,'Vor2ndKind_xc0.txt');

 elOffsets = textread(offsets,'%f\t');
 elType = textread(elemts,'%f\t');
 xval = textread(xc0fname, '%f\t');
 yval = textread(xc1fname, '%f\t');
 dgval = textread( dgVorfname, '%f\t');
 Vor2ndKindval = textread(Vor2ndKindfname,'%f\t');
 
  Elx = textread(Elxfname, '%f\t');
  Ely = textread(Elyfname, '%f\t');
  Elv = textread(Elvfname, '%f\t');
  Elid = textread(Elidfname, '%f\t');
 
 
 
 %%
 elOffsets(2)
 % indices for each elment are .
 tricount = 0; quadcount=0;
 for el = 1:max(size(elOffsets))-1
     if ( 3 == floor(elType(el)) )
         tricount = tricount+1;
        elIndices_Tri(tricount,:) = (elOffsets(el)+1:1:elOffsets(el+1));
     else
         quadcount= quadcount+1;
        elIndices_Quad(quadcount,:) = (elOffsets(el)+1:1:elOffsets(el+1));
        
     end
 end
     if ( 3 == floor(elType(end)) )
         tricount = tricount+1;
        elIndices_Tri(tricount,:) = (max(elOffsets)+1:1:max(size(xval)));
        
     else
         quadcount= quadcount+1;
        elIndices_Quad(quadcount,:) = (max(elOffsets)+1:1:max(size(xval)));
        
     end
%%
 % plot first triangle
 figure(1)
 plot3(xval(elIndices_Tri(1,:)),yval(elIndices_Tri(1,:)), ones(size(elIndices_Tri(1,:))),'-*');hold on;
%  plot3(xval(elIndices_Tri(2,:)),yval(elIndices_Tri(2,:)), ones(size(elIndices_Tri(2,:))),'g*')
 view(0,90);axis equal;
%%
 %plot first quad
 figure(2)
  plot3(xval(elIndices_Quad(1,:)),yval(elIndices_Quad(1,:)), elIndices_Quad(1,:),'*')
 view(0,90);
 
 % plot create triangles for each triangular element;
 figure(1), hold on;
 ptCloud = [xval,yval,zeros(size(xval))];
 for t = 1:tricount
    point = [Elx(t),Ely(t),0.0];
     [a,b] = fNN(ptCloud,point);
     disp(a);
     
 end
 
 tri[]
 =  GenerateP3Triangles(elType, startIndex,Special);
 
 
 
 
 
 
 
 %%
 
 
  tri = delaunay(xval,yval);
%plot(xval,yval,'.')
%  [X,Y] = meshgrid(linspace(2.5,10.5,801),linspace(-4,4,801));
%  dgVal = interp2(xval,yval,dgval,X,Y);
%LMM = Vor2ndKindfname(xval>=2.5&xval<=10.5);
el_indices = find(xval>=2.5&xval<=10.5);
dgVal = dgval(el_indices);

[r,c] = size(tri);
disp(r)

%%
figure(1)
h = trisurf(tri, xval, yval, dgval,'LineStyle','none','LineWidth',lw);
%axis vis3d
   xlim([2.5,10.5]);
    ylim([-4,4]);
%plotNekMesh('../../data/CylExamples/Cy2l.xml'); 
view(0,90); 
caxis( [min(dgVal(:)) max(dgVal(:))] );



%%
figure(2)
h = trisurf(tri, xval, yval, Vor2ndKindval);
%axis vis3d
   xlim([2.5,10.5]);
    ylim([-4,4]);
%plotNekMesh('../../data/CylExamples/Cy2l.xml'); 
view(0,90); 
shading interp;
% caxis( [min(dgVal(:)) max(dgVal(:))] );
% axis off;
% print(strcat('../paperUseImages/',filenameFrPrint,'_Vor2style_surf'), '-dpng','-r500');
% 
% hold on;
% trisurf(tri, xval, yval, Vor2ndKindval,'LineStyle','-','LineWidth',lw,'facealpha',0,'EdgeAlpha',0.3,'EdgeColor','c');
% plotNekMesh('../../data/CylExamples/Cy2l.xml'); 
% print(strcat('../paperUseImages/',filenameFrPrint,'_Vor2style_surf_mesh'), '-dpng','-r500');

%%

multMatrix = (xval>2.5 & xval<10);
Vor2nd = Vor2ndKindval.*multMatrix;

%Vor2nd = Vor2ndKindval(xval>2.5 & xval<10);

figure(3)
tricontour( tri, xval,yval, Vor2nd,(min(Vor2nd):.3:max(Vor2nd)));
%    xlim([2.5,10.5]);
%     ylim([-4,4]);
% axis off;
% print(strcat('../paperUseImages/',filenameFrPrint,'_Vor2style_contours'), '-dpng','-r500');


%tricontour( [xval,yval],tri,Vor2nd, 10);

%% line graph.
% find all intersections of line with triangles.
xlp = [5.8, 5.8];
ylp = [-4, 4];

% Find all edges in mesh, note internal edges are repeated
F = tri;
E = sort([F(:,1) F(:,2); F(:,2) F(:,3); F(:,3) F(:,1)]')';
% determine uniqueness of edges
[u,m,n] = unique(E,'rows');



figure(4)
count = 0;
for loop = 1:max(size(u))
    edg = u(loop,:);
    edgx = [xval(edg(1)),xval(edg(2))];
    edgy = [yval(edg(1)),yval(edg(2))];
    edgVals = [Vor2ndKindval(edg(1)),Vor2ndKindval(edg(1))];
    [xin,yin] = polyxpoly(xlp, ylp, edgx, edgy);
    if(~isempty(xin))
        disp(xin);
        disp(yin);
        count=count+1;
        ylstor(count) = yin;
        pt = sum((edgx(1)-edgx(2))^2 + (edgy(1)-edgy(2))^2).^(0.5);
        t = sum((edgx(1)-xin)^2 + (edgy(1)-yin)^2).^(0.5);
        r = t/pt;
        yvalstor(count) = edgVals(1) + ( edgVals(2) - edgVals(1) )*r;
    end
end
%%
[tmp,ind]=sort(ylstor);
yvalstor_sort=yvalstor(ind);
plot(tmp,yvalstor_sort,'-r','LineWidth',lw,'MarkerSize',msz);
% set(gca,'XTick',sort(unique(tmp)),'Xticklabel','');
% ylabel('vorticity')
% set(gca,'box','off')
% axis([min(tmp) max(tmp) min(yvalstor) max(yvalstor)])
% print(strcat('../paperUseImages/',filenameFrPrint,'_Vor2style_lineGraph'), '-dpng','-r500');


