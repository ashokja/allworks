#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

//global variables.
MPI_Comm mpi_com2d;
void printB_Nodes(float* B_Nodes,int size_B,int rank,int size,int rx, int ry);
int rank2D(int rx,int ry);

void printArray(float** p, int x, int y)
{
	for (int i =0; i < x; i++)
	{
		for(int j =0; j < y; j++)
		{
			printf("%.1f ",p[i][j]);
		}
		printf("\n");
	}
}

void printArrayV(float* p, int x)
{
	for (int i =0; i < x; i++)
	{
		printf("%f \t", p[i] );
	}
	printf("\n");
}

void ProductAXB(float** A,float* X,float* B,int n)
{	
	for(int i =0; i< n; i++){
		float temp =0.0;
		for(int j=0; j<n; j++){
			temp+=A[i][j] * X[j] ;
		}
		B[i] = temp;
	}
}	
void CompEltMat(float** k,float x, float y,int elXdir,int elYdir)
{
	// create k matrix
	
	// load stiffness matrix.
	k[0][0] = (x*x+y*y)/(3*x*y);
	k[0][1] = (x*x-2*y*y)/(6*x*y);
	k[0][2] = -0.5*k[0][0];
	k[0][3] = (y*y-2*x*x)/(6*x*y);
	
	k[1][0] = k[0][1];
	k[1][1] = k[0][0];
	k[1][2] = k[0][3];
	k[1][3] = k[0][2];
	
	k[2][0] = k[0][2];
	k[2][1] = k[1][2];
	k[2][2] = k[0][0];
	k[2][3] = k[0][1];
	
	k[3][0] = k[0][3];
	k[3][1] = k[1][3];
	k[3][2] = k[2][3];
	k[3][3] = k[0][0];
 
}

int main(int argc,char* argv[] )
{
  	//Pseduo code
	int rank, size;
	MPI_Init(&argc,&argv);
	float startTime, endTime;
	startTime = MPI_Wtime();
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);

	// x=4 procs and y = 4 procs.
	int px =2; int py =2;
	int rx,ry;
	// total procs 24 processors.
	
	// Create coords
	int dims[2],periods[2], coords[2];
   dims[0] = px; periods[0] = 0;
 	dims[1] = py; periods[1] = 0;
	
	if(px*py == size)
   {
         MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 0,&mpi_com2d);
         MPI_Cart_coords(mpi_com2d,rank,2,coords);
         //printf("processing id of rank %d is  (%d,%d)\n", rank, coord[0], coord[1]);       
         rx = coords[0];
         ry = coords[1];
		//printf("serial rank:%d rx: %d ry: %d \n", rank,rx, ry );
	}// use rank to get cords. 
	else
	{
		printf("processors and cartesian not if supporting sizes");
		return 0 ;
	}


  	//Simulation grid specification.
  	float X = 10.0; float Y = 20.0;
  	float x_hi = 1.25; float y_hi = 2.5;
	
	int totalXdiv = (int)floor(X/x_hi); // 4 
	int totalYdiv = (int)floor(Y/y_hi); // 4

	//total number of elements
	int totalElements = totalXdiv * totalYdiv;

	// processor in x and y direction
   //int px =4, py =4;
   // rank of each processor
   //int rx,ry; rx=2;ry=2; // range from rx \belongs (0,..,px) and ry /belongs (0,..,py)
   
   //elements in x and y direction.
	int elemXdir = totalXdiv/px;
	int elemYdir = totalYdiv/py;

	// depends on elements per processor.
	int stelx = rx*(totalXdiv/px);
	int stely = ry*(totalYdiv/py);
	
	int endelx = (rx+1)*(totalXdiv/px);
	int endely = (ry+1)*(totalYdiv/py);

	int totalNodesPproc = (elemXdir+1) * (elemYdir+1);
	if( 0==rx && 0==ry)
	{
		printf("elemXdir:%d totalXdiv %d px:%d totalnodesPerProc:%d\n", elemXdir,totalXdiv,px,totalNodesPproc);
	}	
	
	// Create X with size totalNodesPproc
	float* X_Nodes = (float*)calloc(totalNodesPproc,sizeof(float));
	float* B_Nodes = (float*)calloc(totalNodesPproc,sizeof(float));
	float* BB_Nodes = (float*)calloc(totalNodesPproc,sizeof(float));
	float* R_Nodes = (float*)calloc(totalNodesPproc,sizeof(float));
	
	// initialized kk per process.	
	float** kk_p = (float**)calloc(totalNodesPproc,sizeof(float*));
	for (int i=0; i < totalNodesPproc; i++)
	{
/*
		if (1==rx && 0 ==ry && 5==i)
		{
			X_Nodes[i] = 1;//.0;
		}
*/
		kk_p[i] = (float*)calloc(totalNodesPproc,sizeof(float));
	}

	float** k = (float**)malloc(sizeof(float*)*(4));
	for (int i=0; i < 4; i++)
	{
		k[i] = (float*)malloc(sizeof(float)*(4));
	}
   	CompEltMat(k,x_hi,y_hi,elemXdir,elemYdir);
	int n[4];
	//printArray(k,4,4);	 
	// loop through each element to create element matrix
	// Add the element matrix(k) to kk_p

	for (int i=0; i<elemXdir; i++){
		for(int j=0; j<elemYdir; j++){
			// assume there are two loops for ni and nj
			n[0] = i*(elemXdir+1)+j;
			n[1] = n[0]+1;
			n[2] = (i+1)*(elemXdir+1)+j+1;
			n[3] = n[2]-1;
			for (int ii=0; ii<4; ii++){
				for (int jj=0; jj<4; jj++){
					kk_p[n[ii]][n[jj]] +=k[ii][jj];
				}
			}
		}
	}
	//printArray(kk_p,totalNodesPproc,totalNodesPproc);	

// Create an initial b.
	if( ry == py-1)
	{
		for( int i =0; i<=elemXdir;i++)
		{
			BB_Nodes[(elemXdir+1)*elemYdir+i] = 100*sin(0.314*(x_hi)*(rx*elemXdir + i));
			X_Nodes[(elemXdir+1)*elemYdir+i] = 100*sin(0.314*(x_hi)*(rx*elemXdir + i));
		}
	}
	
// Where jocobi will start.
for(int jc =0; jc < 300; jc++)
{
  	//multiply kk_p into x 
	ProductAXB(kk_p,X_Nodes,B_Nodes,totalNodesPproc);
	if(rx ==0 && 0 ==ry)
	{
	    printArray(kk_p,totalNodesPproc,totalNodesPproc);	
		//printArrayV(B_Nodes,totalNodesPproc);	
	}

	printf("rank: %d; Came here \n",rank);
	//Xdir Right to Left
	float* X_left_S = (float*)malloc((elemYdir+1)*sizeof(float));
	float* X_left_R = (float*)malloc((elemYdir+1)*sizeof(float));	
	if( rx != 0)
	{
		for (int j =0; j<= elemYdir; j++)
		{
			X_left_S[j] = B_Nodes[j*(elemXdir+1)]; 
		}
		MPI_Send( X_left_S, elemYdir+1, MPI_FLOAT, rank2D(rx-1,ry), 0, MPI_COMM_WORLD);
	}
	if( rx != px-1)
	{
		MPI_Recv( X_left_R, elemYdir+1, MPI_FLOAT, rank2D(rx+1,ry), 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}
	printf("rank: %d; Came here \n",rank);
	MPI_Barrier(MPI_COMM_WORLD);	
	
	//Xdir Left to Right
	float* X_right_S = (float*)malloc((elemYdir+1)*sizeof(float));
    float* X_right_R = (float*)malloc((elemYdir+1)*sizeof(float));
    if( rx != px-1)
    {
         for (int j =0; j<= elemYdir; j++)
         {
             X_right_S[j] = B_Nodes[(j+1)*(elemXdir+1)-1];
         }
         MPI_Send( X_right_S, elemYdir+1, MPI_FLOAT, rank2D(rx+1,ry), 0, MPI_COMM_WORLD);
    }
    if( rx != 0)
    {
        MPI_Recv( X_right_R, elemYdir+1, MPI_FLOAT, rank2D(rx-1,ry), 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
	
	//Ydir Top to bottom
	float* X_top_S = (float*)malloc((elemXdir+1)*sizeof(float));
	float* X_top_R = (float*)malloc((elemXdir+1)*sizeof(float));
	if (ry !=0 )
	{
		for(int i =0; i<= elemXdir; i++)
		{
			X_top_S[i] = B_Nodes[i];
		}
		MPI_Send(X_top_S,elemXdir+1, MPI_FLOAT, rank2D(rx,ry-1), 0, MPI_COMM_WORLD);
	}
	if (ry != py-1)
	{
		MPI_Recv(X_top_R,elemXdir+1, MPI_FLOAT, rank2D(rx,ry+1), 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}

	//Ydir Bottom to Top
	float* X_bot_S = (float*)malloc((elemXdir+1)*sizeof(float));
	float* X_bot_R = (float*)malloc((elemXdir+1)*sizeof(float));
	if (ry != py-1 )
	{
		for(int i =0; i<= elemXdir; i++)
		{
			X_bot_S[i] = B_Nodes[elemYdir*(elemXdir+1)+i];
		}
		MPI_Send(X_bot_S,elemXdir+1, MPI_FLOAT, rank2D(rx,ry+1), 0, MPI_COMM_WORLD);
	}
	if (ry != 0)
	{
		MPI_Recv(X_bot_R,elemXdir+1, MPI_FLOAT, rank2D(rx,ry-1), 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}

	//Corners
	//bottom left to top right
	float* X_blcor_S = (float*)malloc(sizeof(float));
	float* X_blcor_R = (float*)malloc(sizeof(float));
	if( ry != 0 && rx != 0)
	{
		X_blcor_S = &B_Nodes[0];	
		MPI_Send(X_blcor_S,1, MPI_FLOAT, rank2D(rx-1,ry-1), 0, MPI_COMM_WORLD);
	}
	if( rx != px-1 && ry != py-1)
	{
		MPI_Recv(X_blcor_R, 1, MPI_FLOAT, rank2D(rx+1, ry+1), 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}

	//top right to bottom left
	float* X_trcor_S = (float*)malloc(sizeof(float));
    float* X_trcor_R = (float*)malloc(sizeof(float));
    if( ry != py-1 && rx != px-1)
    {
    	X_trcor_S = &B_Nodes[totalNodesPproc-1];
        MPI_Send(X_trcor_S,1, MPI_FLOAT, rank2D(rx+1,ry+1), 0, MPI_COMM_WORLD);
    }
    if( ry != 0 && rx != 0)
   	{
        MPI_Recv(X_trcor_R, 1, MPI_FLOAT, rank2D(rx-1, ry-1), 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

	//bottom right  to top left 
    float* X_brcor_S = (float*)malloc(sizeof(float));
    float* X_brcor_R = (float*)malloc(sizeof(float));
    if( rx != px-1 && ry != 0)
    {
    	X_brcor_S = &B_Nodes[elemXdir];
        MPI_Send(X_brcor_S,1, MPI_FLOAT, rank2D(rx+1,ry-1), 0, MPI_COMM_WORLD);
    }
    if( rx != 0 && ry != py-1)
    {
       	MPI_Recv(X_brcor_R, 1, MPI_FLOAT, rank2D(rx-1, ry+1), 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

	//top left to bottom right
    float* X_tlcor_S = (float*)malloc(sizeof(float));
    float* X_tlcor_R = (float*)malloc(sizeof(float));
    if( rx != 0 && ry != py-1)
    {
    	X_tlcor_S = &B_Nodes[(elemXdir+1)*elemYdir];
        MPI_Send(X_tlcor_S,1, MPI_FLOAT, rank2D(rx-1,ry+1), 0, MPI_COMM_WORLD);
    }
    if( rx != px-1 && ry != 0)
    {
        MPI_Recv(X_tlcor_R, 1, MPI_FLOAT, rank2D(rx+1, ry-1), 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
	
	//Xdir Right to Left
	if( rx != px-1)
	{
		for (int j =0; j<= elemYdir; j++)
		{
			B_Nodes[(j+1)*(elemXdir+1)-1] += X_left_R[j]; 
		}
	}
	
	//Xdir Left to Right
    if( rx != 0)
    {
        for (int j =0; j<= elemYdir; j++)
        {
            B_Nodes[j*(elemXdir+1)] += X_right_R[j];
        }
    }
	
	//Ydir Top to bottom
	if (ry != py-1)
	{
		for(int i=0; i<=elemXdir;i++)
		{
			B_Nodes[(elemYdir)*(elemXdir+1) +i] += X_top_R[i];
		}
	}

	//Ydir Bottom to Top
	if (ry != 0)
	{
		for(int i=0; i<=elemXdir;i++)
		{
			B_Nodes[i] += X_bot_R[i];
		}
	}

	//Corners
	//bottom left to top right
	if( rx != px-1 && ry != py-1)
	{
		B_Nodes[(elemYdir+1)*(elemXdir+1)-1] += *X_blcor_R;
	}

	//top right to bottom left
    if( ry != 0 && rx != 0)
   	{
    	B_Nodes[0] += *X_trcor_R;
    }

	//bottom right  to top left 
    if( rx != 0 && ry != py	-1)
    {
        B_Nodes[(elemXdir+1)*elemYdir] += *X_brcor_R;
    }

	//top left to bottom right
    if( rx != px-1 && ry != 0)
    {
        B_Nodes[elemXdir] += *X_tlcor_R;
    }	
	
	// Boundary Conditions.
	if ( ry == 0)
	{
		for(int i =0; i<= elemXdir; i++)
		{
			B_Nodes[i] = 0.0;
		}
	}
	if(rx == 0)
	{
		for( int j =0; j <= elemYdir; j++)
		{
			B_Nodes[j*(elemXdir+1)] = 0.0;
		}
	}

	if( ry == py-1)
	{
		for( int i =0; i<=elemXdir;i++)
		{
			B_Nodes[(elemXdir+1)*elemYdir+i] = 100*sin(0.314*(x_hi)*(rx*elemXdir + i));
		}
	}
	float res = 0.0;
	for (int i =0 ; i < totalNodesPproc; i++)
	{
		R_Nodes[i] = BB_Nodes[i]-B_Nodes[i];
		X_Nodes[i] += R_Nodes[i]/3.333;
		res += (float)abs(R_Nodes[i]);
	}
	float global_res = 0.0;
	MPI_Allreduce( &res, &global_res, 1, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
	float err = global_res/((float)(px*py*totalNodesPproc));
	if (err < 0.5)
	{
		printf("error:%f ", err);
		break;
	}
}
	MPI_Barrier(MPI_COMM_WORLD);
	endTime = MPI_Wtime();
	printf("totalTime: %f/n", endTime-startTime);	
	//printArray(kk_p,totalNodesPproc,totalNodesPproc);	
	printB_Nodes(X_Nodes,totalNodesPproc, rank,size,rx,ry);
	MPI_Finalize();	
	return 0;
}

void printB_Nodes(float* B_Nodes,int size_B,int rank,int size, int rx, int ry)
{
	for(int i=0; i < size; i++)
	{
		MPI_Barrier(MPI_COMM_WORLD);	
		if( i == rank)
		{
			printf("rank: %d; (%d,%d)\n", rank,rx,ry);
			for (int j=0 ; j< size_B; j++)
			{
				printf("%0.2f\t",B_Nodes[j]);
			}
			printf("\n");
		}
		
	}
	
}
int rank2D(int rx,int ry)
{
	int cords[2];
	cords[0] = rx;
	cords[1] = ry;
	int rank;
	MPI_Cart_rank(mpi_com2d,cords,&rank);
	return rank; 
}

