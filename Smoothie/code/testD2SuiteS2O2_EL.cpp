#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"

#include <iomanip> // std::setprecision
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
#define TOLERENCE_VIZ 1e-6

using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM2D->LoadMesh(var[0]);
	HNM2D->LoadMesh(var[1]);
	HNM2D->LoadMesh(var[2]);
	string fname = vSession->GetSessionName();
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);
//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);

	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]));
		//fce[i] = std::cos(2.0*M_PI*(xc0[i]))*std::cos(2.0*M_PI*(xc1[i]));
	}
	
	HNM2D->m_expansions[0]->FwdTrans(fce,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	 ece = HNM2D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM2D->m_expansions[0]->GetCoeffs();

	// list of all the elId you want to use.
	// Loop through the element IDs.
	// 		Loop through gridX and gridY of every element.
	// 		convert to elcoord to globalcoord.
	//		for reference print el, elx, ely, glox, gloy, DG_v, SIAC_V
	// 		save all invariables.
	// print all of them to file.
	int num_exp = HNM2D->m_expansions[0]->GetExpSize();
	int gpts = 10;
	int totPts = gpts*gpts*num_exp;
	int Nx = gpts, Ny=gpts;
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_4kp1, HNM2D, atoi(argv[2]), atof(argv[3]) ); 
	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0);
	Array<OneD,NekDouble> locCoord(3,0.0), coord(3,0.0), direction(3,0.0);
	direction[0] = 1.0;
	Array<OneD,NekDouble> pX(totPts), pY(totPts), pV(totPts), pP(totPts), pS(totPts), pE(totPts);
	int index = 0;
	NekDouble valY,valZ;
	for ( int elId =0; elId<num_exp; elId++ )
	{
		//int elId = i;
		int physOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elId);
		const Array<OneD,NekDouble> el_phys = ece.CreateWithOffset( ece, physOffset );
		LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elId);

		for( int i =0; i < Nx; i++)
		{
			for(int j=0; j< Ny; j++)
			{
//				locCoord[0] = i*sx*2.0-1.0 ;
//				locCoord[1] = j*sy*2.0-1.0 ;

				if ( 0==i)
				{
					locCoord[0] = i*sx*2.0-1.0 + TOLERENCE_VIZ;
				}else if( Nx-1 == i) 
				{
					locCoord[0] = i*sx*2.0-1.0 - TOLERENCE_VIZ ;
				}else{
					locCoord[0] = i*sx*2.0-1.0 ;
				}
				if ( 0==j)
				{
					locCoord[1] = j*sy*2.0-1.0 + TOLERENCE_VIZ ;
				}else if( Ny-1 ==j)
				{
					locCoord[1] = j*sy*2.0-1.0 - TOLERENCE_VIZ;
				}else{
					locCoord[1] = j*sy*2.0-1.0 ;
				}
				//locCoord[0] = i*sx*2.0-1.0 ;
				//locCoord[1] = j*sy*2.0-1.0 ;

				lexp->GetCoord(locCoord,coord);
				//cout << setprecision(25) << endl;
				//cout << locCoord[0] << locCoord[1] << endl;
				//cout << coord[0] << coord[1] << endl;
				//cout << setprecision(5) << endl;
				pE[index] = elId;
				pX[index] = coord[0];
				pY[index] = coord[1];
				pV[index] = std::cos(2.0*M_PI*(pX[index] + pY[index]) );
				pP[index] = lexp->PhysEvaluate(coord,el_phys);
				//cout << locCoord[0] << "\t" << locCoord[1] << "\t" << pX[index] << "\t" << pY[index] << "\t" << pV[index] << "\t" << pP[index] << "\t" << pS[index] << "\t"<< i<< "\t"<< j << endl;
				sm.EvaluateAt(pX[index],pY[index],0.0,pS[index],valY,valZ,direction, atof(argv[3]),0);
				//cout << locCoord[0] << "\t" << locCoord[1] << "\t" << pX[index] << "\t" << pY[index] << "\t" << pV[index] << "\t" << pP[index] << "\t" << pS[index] << endl;
				index++;
			}
		}
		cout << "el id: " << elId << endl;
	}
	NektarBaseClass k;
	k.writeNekArray(pE,fname+"_"+argv[2]+"_pE_2D_OneSided2kp1_EL.txt");
	k.writeNekArray(pX,fname+"_"+argv[2]+"_pX_2D_OneSided2kp1_EL.txt");
	k.writeNekArray(pY,fname+"_"+argv[2]+"_pY_2D_OneSided2kp1_EL.txt");
	k.writeNekArray(pV,fname+"_"+argv[2]+"_pV_2D_OneSided2kp1_EL.txt");
	k.writeNekArray(pP,fname+"_"+argv[2]+"_pP_2D_OneSided2kp1_EL.txt");
	k.writeNekArray(pS,fname+"_"+argv[2]+"_pS_2D_OneSided2kp1_EL.txt");

/*

// Evaluate on a new equal space grid mesh.
	int gPts = 21;
	int totPts = gPts*gPts;
	int Nx = gPts, Ny=gPts;
	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0);
	
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]) ); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	Array<OneD,NekDouble> direction(3,0.0), coord(3,0.0);
	direction[1] = 1.0;
	Array<OneD,NekDouble> pX(totPts), pY(totPts), pV(totPts), pP(totPts), pS(totPts);
	for( int i =0; i < Nx; i++)
	{
		for(int j=0; j< Ny; j++)
		{
			int index = i*Nx+j;
			pX[index] = i*sx;
			pY[index] = j*sy;
			pV[index] = std::cos(2.0*M_PI*(pX[index] + pY[index]) );
			//pV[index] = std::cos(2.0*M_PI*(pX[index]))*std::cos(2.0*M_PI*(pY[index]));
			sm.EvaluateAt(pX[index],pY[index],0.0,pS[index],valY,valZ,direction, atof(argv[3]),0);
			coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = 0.0;
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(coord);
			int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elId);
			const Array<OneD,NekDouble> el_phys = ece.CreateWithOffset(
										ece, coeffOffset);
			pP[index] = lexp->PhysEvaluate(coord,el_phys);
			cout << j << endl;
		}
		cout << i << endl;
	}

	NektarBaseClass k;
	k.writeNekArray(pX,fname+"_"+argv[2]+"_pX_2D_OneSided2kp1.txt");
	k.writeNekArray(pY,fname+"_"+argv[2]+"_pY_2D_OneSided2kp1.txt");
	k.writeNekArray(pV,fname+"_"+argv[2]+"_pV_2D_OneSided2kp1.txt");
	k.writeNekArray(pP,fname+"_"+argv[2]+"_pP_2D_OneSided2kp1.txt");
	k.writeNekArray(pS,fname+"_"+argv[2]+"_pS_2D_OneSided2kp1.txt");
*/
	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
