#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"
#include <boost/timer.hpp>


#include <iomanip> // std::setprecision
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
#define TOLERENCE_VIZ 1e-6

using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 7)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		cout << "4th no of quad Points for integration."<< endl;
		cout << "5th Unique Number for Simulation" << endl;
		cout << "6th per unit resolution" << endl;
		return 0;
	}
	argc = 2;

clock_t startTime = clock();
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();

	cout << "loading xml file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
    startTime = clock();

	string fname = vSession->GetSessionName();
	string fldname = fname + ".fld";    
    cout << fname << endl;
    cout << fldname << endl;

	HNM3D->LoadData( fldname ,var);
    cout << "loading fld file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
    startTime = clock();

    cout << "expsize:\t"<< HNM3D->m_expansions[0]->GetExpSize() << endl;    
    HNM3D->LoadExpListIntoRTree();
    cout << "loading into R-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
    startTime = clock();


	SmoothieSIAC3D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]), atof(argv[3]) );
	SmoothieSIAC3D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]), atof(argv[3]),1 );



	// parameters part.
	int resParts = atoi(argv[6]);
	NekDouble resupdate = 1.0/((NekDouble)resParts);
	NekDouble xresMin,xresMax,yresMin,yresMax,zresMin,zresMax;
	xresMin = 0.25; xresMax = 0.3;  
	yresMin = 0.05; yresMax = 0.15;
	zresMin = -0.04; zresMax = 0.06;
	int Nx = std::floor( (xresMax-xresMin)/resupdate)+1;
	int Ny = std::floor( (yresMax-yresMin)/resupdate)+1;
	int Nz = std::floor( (zresMax-zresMin)/resupdate)+1;

	// initial setup.
	vector<NekDouble> tparamsX, tparamsY, tparamsZ, tvals;
	tparamsX.push_back(0.0);
	for ( NekDouble t =xresMin; t<= xresMax+0.001; t=t+resupdate)
	{
		tparamsX.push_back(t);
	}
	tparamsX.push_back(1.0);
	int restotPoints = (Nx+2)*Ny*Nz;
	vector<NekDouble> PX,PY,PZ;
	vector<NekDouble> PU,PV,PW;

	Array<OneD,NekDouble> directionL(3,0.0);
	directionL[0] = 1.0;
	vector<NekDouble> stPoint(3);
	vector<int>  Gids,Eids;
	vector<NekDouble> HvalT; 

	//int count = 0,county=0;
	for( NekDouble yres  = yresMin ; yres< yresMax+0.000001; yres+= resupdate)
	{
	//	county++;
			boost::timer tim;
		for( NekDouble zres  = zresMin ; zres< zresMax+0.000001; zres+= resupdate)
		{
	//		count ++;
			stPoint[0] = 0.25; stPoint[1] = yres; stPoint[2] = zres;	
			for ( NekDouble xres =xresMin; xres< xresMax+0.000001; xres+= resupdate)
			{
				PX.push_back(xres); PY.push_back(yres); PZ.push_back(zres);
			}
			switch ( atoi(argv[5])%4)
			{
				case 0:			
					sm.EvaluateUsingLineAt( stPoint, directionL, atoi(argv[4]) , atof(argv[3]),tparamsX, tvals, 0);
					PU.insert(PU.end(), tvals.begin()+1, tvals.end()-1);
					tvals.clear();
					break;
				case 1:
					sm.EvaluateUsingLineAt( stPoint, directionL, atoi(argv[4]) , atof(argv[3]),tparamsX, tvals, 1);
					PU.insert(PU.end(), tvals.begin()+1, tvals.end()-1);
					tvals.clear();
					break;
				case 2:
					sm.EvaluateUsingLineAt( stPoint, directionL, atoi(argv[4]) , atof(argv[3]),tparamsX, tvals, 2);
					PU.insert(PU.end(), tvals.begin()+1, tvals.end()-1);
					tvals.clear();
					break;
				case 3:
					sm.EvaluateUsingLineAt( stPoint, directionL, atoi(argv[4]) , atof(argv[3]),tparamsX, tvals, 3);
					PU.insert(PU.end(), tvals.begin()+1, tvals.end()-1);
					tvals.clear();
					break;
				default:
					assert( false && "something is wrong");
			}
			//cout << "y\t" << yres <<"\tz\t" << zres << endl;		
		}
		cout << "timetaken " << tim.elapsed() << endl;
	}
	
	cout << PX.size()<< endl;
	cout << PY.size()<< endl;
	cout << PZ.size()<< endl;
	cout << PU.size()<< endl;
	cout << "Nx\t" <<Nx << endl;
	cout << "Ny\t" <<Ny << endl;
	cout << "Nz\t" <<Nz << endl;

	NektarBaseClass k;
	k.writeNekArray(PX, fname+"_Speed3D_P_"+argv[2]+"_Sca_"+argv[3]+"_X_"+to_string(Nx)+"_Y_"+to_string(Ny)+"_Z_"+to_string(Nz)+"_Q_"+argv[4]+"_unqID"+argv[5]+"_pX.txt");
	k.writeNekArray(PY, fname+"_Speed3D_P_"+argv[2]+"_Sca_"+argv[3]+"_X_"+to_string(Nx)+"_Y_"+to_string(Ny)+"_Z_"+to_string(Nz)+"_Q_"+argv[4]+"_unqID"+argv[5]+"_pY.txt");
	k.writeNekArray(PZ, fname+"_Speed3D_P_"+argv[2]+"_Sca_"+argv[3]+"_X_"+to_string(Nx)+"_Y_"+to_string(Ny)+"_Z_"+to_string(Nz)+"_Q_"+argv[4]+"_unqID"+argv[5]+"_pZ.txt");
	k.writeNekArray(PU, fname+"_Speed3D_P_"+argv[2]+"_Sca_"+argv[3]+"_X_"+to_string(Nx)+"_Y_"+to_string(Ny)+"_Z_"+to_string(Nz)+"_Q_"+argv[4]+"_unqID"+argv[5]+"_pU.txt");
	


/*

	//stPoint[0] = 0.0; stPoint[1] = 0.555555555; stPoint[2] = 0.0;
	stPoint[0] = 2.1; stPoint[1] = 0.05; stPoint[2] = 0.65;
	Array<OneD,NekDouble> t_LineElm;
	
	vector<NekDouble> tparams, tvals;
	//tparams.push_back(0.0);
	for ( NekDouble t =0.5; t<= 4.1; t=t+0.0025)
	{
		tparams.push_back(t);
	}
	tparams.push_back(4.05);

	boost::timer tim1;
	sm.EvaluateUsingLineAt( stPoint, directionL, atoi(argv[4]) , atof(argv[3]),tparams, tvals, 0);
	double s1 = tim1.elapsed();
	tvals.clear();




	NekDouble valY, valZ;	
	vector<NekDouble> tparamsS(tparams.size());	
	boost::timer tim;
	for(int t =0; t< tparams.size();t++)
	{
		sm.EvaluateAt(stPoint[0]+tparams[t],stPoint[1],stPoint[2],tparamsS[t], valY,valZ, directionL, atof(argv[3]),0);
		//sm.EvaluateAt(tparams[t],0.0,0.0,tparamsS[t], valY,valZ, directionL, atof(argv[3]),0);
	}
	double sf = tim.elapsed();

	NektarBaseClass k;
	
	for(int t =0; t< tparams.size();t++)
	{
		cout << "SpL\t " << tvals[t] << "\tVal\t" << tparamsS[t] << endl;
	}
	std::cout << "speed-LSIAC time taken to evaluate at "<< tparams.size() << "is "<< s1 << " seconds"<< endl;
	std::cout << "LSIAC time taken to evaluate at "<< tparams.size() << "is "<< sf << " seconds"<< endl;
	NekDouble errorSum = 0.0;
	for(int t =0; t< tparams.size();t++)
	{
		errorSum += std::abs( tvals[t]- tparamsS[t]); 
	}
	std::cout << "Total Sum of ERROR "<< errorSum << endl;
*/
	return 0;
}

