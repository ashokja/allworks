import papaya.*;

import peasy.test.*;
import peasy.org.apache.commons.math.*;
import peasy.*;
import peasy.org.apache.commons.math.geometry.*;

// global Variables.
partSystem pSys;
PeasyCam cam;
float MASS_PER_PARTICLE =0.01;
float LAMBDA = 200;
float MU = 200;
float DAMPING = .7;
float KV = 200;
int count =0;

void setup() {
  size(640, 360, P3D);
  fill(204);
  cam = new PeasyCam(this, 100);
  cam.setMinimumDistance(0);
  cam.setMaximumDistance(1000);
  pSys = new partSystem();
  LoadSquareBox(10);
  pSys.calNeigh(10);
  pSys.init();
  println(pSys.pList.get(0).neigh);
  println(pSys.pList.get(0).neighDist);
  //pSys.printGrad();
  //pSys.printc();
  //pSys.pList.get(0).gradientAtPoint();
  //pSys.pList.get(0).mp.y = 7.0;
}

void draw() {
  //  rotateX(-.5);
  //  rotateY(-.5);
  background(255);
  fill(255, 0, 0);
  //box(30);
  pushMatrix();
  translate(0, 0, 20);
  fill(0, 0, 255);
  //box(5);
  popMatrix();
  pSys.step(0.0001);
  pSys.draw();
  if(count > 100)
  {
    println(pSys.pList.get(0).mp);    
    count =0;
  }
  count++;
  // Draw Plane;
}

void LoadSquareBox(int N)
{

  for (int i =0; i < N; i++)
  {
    for (int j=0; j< N; j++)
    {
      for (int k =0; k< N; k++)
      {

        PVector p = new PVector(i, j, k);
        particle pp = new particle(p);
        if (i ==0)
          pp.mp.x = i-.3;
        //pp.printc();
        //println(p);
        pSys.addParticle(pp);
      }
    }
  }
}

