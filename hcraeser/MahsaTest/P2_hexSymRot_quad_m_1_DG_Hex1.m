% P2
% quadrature cal error.
% cart siac filter.
% hex union jack.
% DG

clear;
% Test errors.
Deg=1;
ord1 = 15;
ord2 = 29;
ord3 = 57;
ord4 = 113;

L2B_ord1 = 7.23e-4;
L2B_ord2 = 8.88e-5;
L2B_ord3 = 1.11e-5;
L2B_ord4 = 1.39e-6;

LinfB_ord1 = 3.65e-3;
LinfB_ord2 = 4.70e-4;
LinfB_ord3 = 5.91e-5;
LinfB_ord4 = 7.40e-6;

L2_ord1 = 1.13e-4;
L2_ord2 = 2.86e-6;
L2_ord3 = 8.23e-8;
L2_ord4 = 3.54e-9;

Linf_ord1 = 1.74e-4;
Linf_ord2 = 5.03e-6;
Linf_ord3 = 2.59e-7;
Linf_ord4 = 3.13e-8;

order_L2(1) = 0;
order_L2(2) = log(L2_ord2/L2_ord1)/log(ord1/ord2);
order_L2(3) = log(L2_ord3/L2_ord2)/log(ord2/ord3);
order_L2(4) = log(L2_ord4/L2_ord3)/log(ord3/ord4);

order_Linf(1) = 0;
order_Linf(2) = log(Linf_ord2/Linf_ord1)/log(ord1/ord2);
order_Linf(3) = log(Linf_ord3/Linf_ord2)/log(ord2/ord3);
order_Linf(4) = log(Linf_ord4/Linf_ord3)/log(ord3/ord4);

order_L2
order_Linf

% write code to print 

line0= '\caption{DG solution, m = 1.0, HL = 2L, HB= 2B, Hex Union Jack, Cartesian SIAC filter.}';
line1= strcat('\label{table:4}',...
 '\begin{tabular}{||c| c| c| c| c| c| c | c||}',...
' \hline ');
line2 = ' Mesh Ele & Deg &L2(Before) & L2(After SIAC) & order& Linf(Before) & Linf(After SIAC) & order \\ [0.5ex] ';
line3 = ' \hline\hline ';
% line 1. 
line4 = strcat('$',num2str(ord1),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord1,'%1.2e'), '$ & $',...
    num2str(L2_ord1,'%1.2e'), '$ & $',...
    num2str(order_L2(1)), '$ & $',...
    num2str(LinfB_ord1,'%1.2e'), '$ & $',...
    num2str(Linf_ord1,'%1.2e'), '$ & $',...
    num2str(order_Linf(1)), '$ ',...
    '\\');
line5 = ' \hline ';
% line 2.
line6 = strcat('$',num2str(ord2),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord2,'%1.2e'), '$ & $',...
    num2str(L2_ord2,'%1.2e'), '$ & $',...
    num2str(order_L2(2)), '$ & $',...
    num2str(LinfB_ord2,'%1.2e'), '$ & $',...
    num2str(Linf_ord2,'%1.2e'), '$ & $',...
    num2str(order_Linf(2)), '$ ',...
    '\\');
line7 = ' \hline ';

line8 = strcat('$',num2str(ord3),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord3,'%1.2e'), '$ & $',...
    num2str(L2_ord3,'%1.2e'), '$ & $',...
    num2str(order_L2(3)), '$ & $',...
    num2str(LinfB_ord3,'%1.2e'), '$ & $',...
    num2str(Linf_ord3,'%1.2e'), '$ & $',...
    num2str(order_Linf(3)), '$ ',...
    '\\');
line9 = ' \hline ';

line10 = strcat('$',num2str(ord4),'^2$ & $',num2str(Deg),'$ & $',...
     num2str(L2B_ord4,'%1.2e'), '$ & $',...
    num2str(L2_ord4,'%1.2e'), '$ & $',...
    num2str(order_L2(4)), '$ & $',...
    num2str(LinfB_ord4,'%1.2e'), '$ & $',...
    num2str(Linf_ord4,'%1.2e'), '$ & $',...
    num2str(order_Linf(4)), '$ ',...
    '\\');


line11= ' \hline \end{tabular} ';


fileID = fopen('Del_P2.txt','w');
fwrite(fileID,line0);
fwrite(fileID,line1);
fwrite(fileID,line2);
fwrite(fileID,line3);
fwrite(fileID,line4);
fwrite(fileID,line5);
fwrite(fileID,line6);
fwrite(fileID,line7);
fwrite(fileID,line8);
fwrite(fileID,line9);
fwrite(fileID,line10);
fwrite(fileID,line11);
fclose(fileID);
