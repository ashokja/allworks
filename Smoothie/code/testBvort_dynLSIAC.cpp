// To be used for Bentvort_1.xml files.
// This test file will simulate any of 9 single derivatives depending on input parameters.

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"
//#include <ctime>



using namespace SIACUtilities;
using namespace std;

void calVorticity(int nq,
    const Array<OneD,NekDouble> &udx_phys, const Array<OneD,NekDouble> &udy_phys, const Array<OneD,NekDouble> &udz_phys,
    const Array<OneD,NekDouble> &vdx_phys, const Array<OneD,NekDouble> &vdy_phys, const Array<OneD,NekDouble> &vdz_phys,
    const Array<OneD,NekDouble> &wdx_phys, const Array<OneD,NekDouble> &wdy_phys, const Array<OneD,NekDouble> &wdz_phys,
    Array<OneD,NekDouble> &Vx,Array<OneD,NekDouble> &Vy,Array<OneD,NekDouble> &Vz )
{
    Vmath::Vsub(nq, wdy_phys,1, vdz_phys,1,Vx ,1);
    Vmath::Vsub(nq, udz_phys,1, wdx_phys,1,Vy ,1);
    Vmath::Vsub(nq, vdx_phys,1, udy_phys,1,Vz ,1);
    return;
}


int main(int argc, char* argv[])
{
	if (argc != 6)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		cout << "4th resolution of output" << endl;
		cout << "5th arg specifies which part of the simulation to run Select between 0-4" << endl;
		return 0;
	}
	// This is so that vSession does not interpret extra parameters absurdly.
	argc = 2;

	clock_t startTime = clock();
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);

	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	
	cout << "loading xml file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	string fname = vSession->GetSessionName();

	//string fldname = fname + ".fld/Info.xml";	
	//string fldname = "naca.fld";	
	string fldname = fname + ".fld";	
	cout << fname << endl;
	cout << fldname << endl;

	HNM3D->LoadData( fldname ,var);
	//HNM3D->LoadMesh(var[0]);
	//HNM3D->LoadMesh(var[1]);
	//HNM3D->LoadMesh(var[2]);
	//HNM3D->LoadMesh(var[3]);
	cout << "loading fld file "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
	cout << "expsize:\t"<< HNM3D->m_expansions[0]->GetExpSize() << endl;
	
	HNM3D->LoadExpListIntoRTree();
	cout << "loading into R-tree "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	/*
	for (int i =0; i< HNM3D->m_expansions[0]->GetExpSize();i++)
	{
		int gid = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetGlobalID();
		cout << "i\t" << i << "\tg\t" << gid << endl;
		cout << HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetShapeType()<< endl;
	}
	*/
	cout << "Expansion Size\t " << endl;
	cout << "Expansion Size\t" << HNM3D->m_expansions[0]->GetExpSize() << endl;
	cout << "Total Number of Points \t" << HNM3D->m_expansions[0]->GetTotPoints() << endl;
	cout << "Total number of coefficients \t" << HNM3D->m_expansions[0]->GetNcoeffs() << endl;

	
	int totPhys = HNM3D->m_expansions[0]->GetTotPoints();
	Array<OneD,NekDouble> xc0(totPhys),xc1(totPhys), xc2(totPhys);
	
	HNM3D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
	NekDouble minx = Vmath::Vmin(totPhys,xc0,1);	
	NekDouble miny = Vmath::Vmin(totPhys,xc1,1);	
	NekDouble minz = Vmath::Vmin(totPhys,xc2,1);	
	NekDouble maxx = Vmath::Vmax(totPhys,xc0,1);	
	NekDouble maxy = Vmath::Vmax(totPhys,xc1,1);	
	NekDouble maxz = Vmath::Vmax(totPhys,xc2,1);	

	cout << "min " << minx << "\t" << miny << "\t" << minz << endl;
	cout << "max " << maxx << "\t" << maxy << "\t" << maxz << endl;


	vector<int> elementIds, gl_elementIds;
	HNM3D->IntersectWithBoxUsingRTree( minx-0.0001, miny, minz, maxx+0.0001,maxy,maxz,
				elementIds, gl_elementIds);
	cout << "Total Elem in bounds " << elementIds.size() << endl;


	cout << "Time taken torsion and terms. "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();

	const Array<OneD,NekDouble> u_phys = HNM3D->m_expansions[0]->GetPhys();
	const Array<OneD,NekDouble> v_phys = HNM3D->m_expansions[1]->GetPhys();
	const Array<OneD,NekDouble> w_phys = HNM3D->m_expansions[2]->GetPhys();
	const Array<OneD,NekDouble> p_phys = HNM3D->m_expansions[3]->GetPhys();
	Array<OneD,NekDouble> ux_phys(totPhys), vx_phys(totPhys),wx_phys(totPhys);
	Array<OneD,NekDouble> uy_phys(totPhys), vy_phys(totPhys),wy_phys(totPhys);
	Array<OneD,NekDouble> uz_phys(totPhys), vz_phys(totPhys),wz_phys(totPhys);
	Array<OneD,NekDouble> VorU_phys(totPhys), VorV_phys(totPhys),VorW_phys(totPhys);
// Need to appply dG voriticity.
	HNM3D->m_expansions[0]->PhysDeriv(u_phys, ux_phys,uy_phys,uz_phys);
	HNM3D->m_expansions[1]->PhysDeriv(v_phys, vx_phys,vy_phys,vz_phys);
	HNM3D->m_expansions[2]->PhysDeriv(w_phys, wx_phys,wy_phys,wz_phys);
	calVorticity(totPhys,
    			ux_phys, uy_phys, uz_phys, vx_phys, vy_phys, vz_phys,
    			wx_phys, wy_phys, wz_phys, VorU_phys, VorV_phys, VorW_phys);
	

	
	//minx = 0.3; miny = 0.0; minz = -0.1;
	//maxx = 0.3; maxy = 0.2; maxz= 0.1;
    minx = 0.3; miny = 0.05; minz = -0.04;
    maxx = 0.3; maxy = 0.15; maxz= 0.06;
	int gPtsX =  1;   //atoi(argv[2]);
	int gPtsY =  atoi(argv[4]);
	int gPtsZ =  atoi(argv[4]);
	
	int Nx = gPtsX, Ny = gPtsY, Nz = gPtsZ;
	int totPts = Ny*Nz;
	NekDouble sx = 1.0; // /(Nx-1.0);
	NekDouble sy = 1.0/(Ny-1.0), sz=1.0/(Nz-1.0);

	Array<OneD,NekDouble> pX(totPts), pY(totPts), pZ(totPts), pE(totPts);	
	Array<OneD,NekDouble> pU_s(totPts), pV_s(totPts), pW_s(totPts), pP_s(totPts);
	Array<OneD,NekDouble> pS_sDyn(totPts);
	Array<OneD,NekDouble> pU_sDyn(totPts), pV_sDyn(totPts), pW_sDyn(totPts), pP_sDyn(totPts);
	Array<OneD,NekDouble> pUx_s(totPts), pUy_s(totPts), pUz_s(totPts);
	Array<OneD,NekDouble> pVx_s(totPts), pVy_s(totPts), pVz_s(totPts);
	Array<OneD,NekDouble> pWx_s(totPts), pWy_s(totPts), pWz_s(totPts);
	Array<OneD,NekDouble> pUx_sDyn(totPts), pUy_sDyn(totPts), pUz_sDyn(totPts);
	Array<OneD,NekDouble> pVx_sDyn(totPts), pVy_sDyn(totPts), pVz_sDyn(totPts);
	Array<OneD,NekDouble> pWx_sDyn(totPts), pWy_sDyn(totPts), pWz_sDyn(totPts);

	Array<OneD,NekDouble> pU_p(totPts), pV_p(totPts), pW_p(totPts), pP_p(totPts);
	Array<OneD,NekDouble> pUx_p(totPts), pUy_p(totPts), pUz_p(totPts);
	Array<OneD,NekDouble> pVx_p(totPts), pVy_p(totPts), pVz_p(totPts);
	Array<OneD,NekDouble> pWx_p(totPts), pWy_p(totPts), pWz_p(totPts);
	Array<OneD,NekDouble> pVorU_p(totPts), pVorV_p(totPts), pVorW_p(totPts);


	Array<OneD,NekDouble> coord(3);
	NekDouble scaling = HNM3D->GetMeshLargestEdgeLength();
	cout << "maximum Edge Length" << scaling << endl;

	scaling = atof(argv[3]) ;
	SmoothieSIAC3D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]),scaling ); 
	SmoothieSIAC3D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_2kp1, HNM3D, atoi(argv[2]),scaling,1); 
	HNM3D->CalculateDynamicScaling();	
	Array<OneD,NekDouble> directionX(3,0.0),directionY(3,0.0),directionZ(3,0.0);
	directionX[0] = 1.0;	directionY[1] = 1.0;	directionZ[2] = 1.0;
	NekDouble valY,valZ;
	int index;
	for (int i =0; i <Nx; i++)
	{
		for(int j=0; j<Ny; j++)
		{
			for(int k=0; k<Nz; k++)
			{
//				j = 24; k=23;
				index = j*Nz+k;
				//cout << index << endl;
				pX[index] = minx; // + i*sx*(maxx-minx); 
				//pX[index] = 2.25;
				pY[index] = miny + j*sy*(maxy-miny);
				pZ[index] = minz + k*sz*(maxz-minz);
				coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = pZ[index];
				//int elID = HNM3D->m_expansions[0]->GetExpIndex(coord);
				int elID = HNM3D->GetExpansionIndexUsingRTree(coord);
//				cout << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;	
//				cout << "eid: \t"<<elID<< "\t index \t"<< index << endl;
				pE[index] = elID;

				if (elID !=-1)
				{
						NekDouble dynScaling = HNM3D->GetDynamicScaling(coord,elID,1.0);
						pS_sDyn[index] = dynScaling;
					switch (atoi(argv[5]))
					{
						case 0:
							sm.EvaluateAt(pX[index],pY[index],pZ[index],pU_s[index],valY,valZ, directionX, scaling ,0);
							sm.EvaluateAt(pX[index],pY[index],pZ[index],pV_s[index],valY,valZ, directionX, scaling ,1);
							sm.EvaluateAt(pX[index],pY[index],pZ[index],pW_s[index],valY,valZ, directionX, scaling ,2);
							sm.EvaluateAt(pX[index],pY[index],pZ[index],pP_s[index],valY,valZ, directionX, scaling ,3);
					// Dynamic LSIAC
	//				cout << "dynamicScaling" << dynScaling << endl;
				
							sm.EvaluateAt(pX[index],pY[index],pZ[index],pU_sDyn[index],valY,valZ, directionX, dynScaling ,0);
							sm.EvaluateAt(pX[index],pY[index],pZ[index],pV_sDyn[index],valY,valZ, directionX, dynScaling ,1);
							sm.EvaluateAt(pX[index],pY[index],pZ[index],pW_sDyn[index],valY,valZ, directionX, dynScaling ,2);
							sm.EvaluateAt(pX[index],pY[index],pZ[index],pP_sDyn[index],valY,valZ, directionX, dynScaling ,3);
							break;
						case 1:
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pUx_s[index],valY,valZ,directionX,scaling,0);
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pUx_sDyn[index],valY,valZ,directionX,dynScaling,0);
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pUy_s[index],valY,valZ,directionY,scaling,0);
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pUy_sDyn[index],valY,valZ,directionY,dynScaling,0);
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pUz_s[index],valY,valZ,directionZ,scaling,0);
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pUz_sDyn[index],valY,valZ,directionZ,dynScaling,0);
							break;
						case 2:
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pVx_s[index],valY,valZ,directionX,scaling,1);
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pVx_sDyn[index],valY,valZ,directionX,dynScaling,1);
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pVy_s[index],valY,valZ,directionY,scaling,1);
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pVy_sDyn[index],valY,valZ,directionY,dynScaling,1);
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pVz_s[index],valY,valZ,directionZ,scaling,1);
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pVz_sDyn[index],valY,valZ,directionZ,dynScaling,1);
							break;
						case 3:
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pWx_s[index],valY,valZ,directionX,scaling,2);
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pWx_sDyn[index],valY,valZ,directionX,dynScaling,2);
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pWy_s[index],valY,valZ,directionY,scaling,2);
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pWy_sDyn[index],valY,valZ,directionY,dynScaling,2);
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pWz_s[index],valY,valZ,directionZ,scaling,2);
							smD.EvaluateAt(pX[index],pY[index],pZ[index],pWz_sDyn[index],valY,valZ,directionZ,dynScaling,2);
						case 4:
							// terms requried to calculate the dG vorticity.
							int physOffset = HNM3D->m_expansions[0]->GetPhys_Offset(elID);
							LocalRegions::ExpansionSharedPtr lexp = HNM3D->m_expansions[0]->GetExp(elID);
							const Array<OneD,NekDouble> el_u_phys = u_phys.CreateWithOffset(u_phys,physOffset);
							const Array<OneD,NekDouble> el_ux_phys = ux_phys.CreateWithOffset(ux_phys,physOffset);
							const Array<OneD,NekDouble> el_uy_phys = uy_phys.CreateWithOffset(uy_phys,physOffset);
							const Array<OneD,NekDouble> el_uz_phys = uz_phys.CreateWithOffset(uz_phys,physOffset);
							const Array<OneD,NekDouble> el_v_phys = v_phys.CreateWithOffset(v_phys,physOffset);
							const Array<OneD,NekDouble> el_vx_phys = vx_phys.CreateWithOffset(vx_phys,physOffset);
							const Array<OneD,NekDouble> el_vy_phys = vy_phys.CreateWithOffset(vy_phys,physOffset);
							const Array<OneD,NekDouble> el_vz_phys = vz_phys.CreateWithOffset(vz_phys,physOffset);
							const Array<OneD,NekDouble> el_w_phys = w_phys.CreateWithOffset(w_phys,physOffset);
							const Array<OneD,NekDouble> el_wx_phys = wx_phys.CreateWithOffset(wx_phys,physOffset);
							const Array<OneD,NekDouble> el_wy_phys = wy_phys.CreateWithOffset(wy_phys,physOffset);
							const Array<OneD,NekDouble> el_wz_phys = wz_phys.CreateWithOffset(wz_phys,physOffset);
							const Array<OneD,NekDouble> el_p_phys = p_phys.CreateWithOffset(p_phys,physOffset);
							const Array<OneD,NekDouble> el_VorU_phys = VorU_phys.CreateWithOffset(VorU_phys,physOffset);
							const Array<OneD,NekDouble> el_VorV_phys = VorV_phys.CreateWithOffset(VorV_phys,physOffset);
							const Array<OneD,NekDouble> el_VorW_phys = VorW_phys.CreateWithOffset(VorW_phys,physOffset);
							pP_p[index] = lexp->PhysEvaluate(coord,el_p_phys);
							pU_p[index] = lexp->PhysEvaluate(coord,el_u_phys);
							pU_p[index] = lexp->PhysEvaluate(coord,el_u_phys);
							pUx_p[index] = lexp->PhysEvaluate(coord,el_ux_phys);
							pUy_p[index] = lexp->PhysEvaluate(coord,el_uy_phys);
							pUz_p[index] = lexp->PhysEvaluate(coord,el_uz_phys);
							pV_p[index] = lexp->PhysEvaluate(coord,el_v_phys);
							pVx_p[index] = lexp->PhysEvaluate(coord,el_vx_phys);
							pVy_p[index] = lexp->PhysEvaluate(coord,el_vy_phys);
							pVz_p[index] = lexp->PhysEvaluate(coord,el_vz_phys);
							pW_p[index] = lexp->PhysEvaluate(coord,el_w_phys);
							pWx_p[index] = lexp->PhysEvaluate(coord,el_wx_phys);
							pWy_p[index] = lexp->PhysEvaluate(coord,el_wy_phys);
							pWz_p[index] = lexp->PhysEvaluate(coord,el_wz_phys);
							pVorU_p[index] = lexp->PhysEvaluate(coord,el_VorU_phys);
							pVorV_p[index] = lexp->PhysEvaluate(coord,el_VorV_phys);
							pVorW_p[index] = lexp->PhysEvaluate(coord,el_VorW_phys);
							break;
					}
						
				}else
				{	
					pU_p[index] = 0.0;
					pV_p[index] = 0.0;
					pW_p[index] = 0.0;
					pP_p[index] = 0.0;
					pP_s[index] = 0.0;
					cout << "out" << endl;
				}
			}
			cout << "loop for each iteration "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds for Nz = " << Nz<< endl;
			startTime = clock();
			cout << j<< endl;
		}
		cout << i << endl;
	}	
	
// Calculate acceleration,  b, torsion, Dbv for this data.

	NektarBaseClass k;
					switch (atoi(argv[5]))
					{
						case 0:	
							k.writeNekArray(pX,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pX.txt");
							k.writeNekArray(pY,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pY.txt");
							k.writeNekArray(pZ,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pZ.txt");

							k.writeNekArray(pU_s,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pU_s.txt");
							k.writeNekArray(pV_s,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pV_s.txt");
							k.writeNekArray(pW_s,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pW_s.txt");
							k.writeNekArray(pP_s,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pP_s.txt");
	
							k.writeNekArray(pU_sDyn,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pU_sDyn.txt");
							k.writeNekArray(pV_sDyn,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pV_sDyn.txt");
							k.writeNekArray(pW_sDyn,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pW_sDyn.txt");
							k.writeNekArray(pP_sDyn,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pP_sDyn.txt");
	
							k.writeNekArray(pS_sDyn,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pS_sDyn.txt");
							break;
						case 1:
							k.writeNekArray(pUx_s,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pUx_s.txt");
							k.writeNekArray(pUy_s,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pUy_s.txt");
							k.writeNekArray(pUz_s,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pUz_s.txt");
							k.writeNekArray(pUx_sDyn,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pUx_sDyn.txt");
							k.writeNekArray(pUy_sDyn,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pUy_sDyn.txt");
							k.writeNekArray(pUz_sDyn,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pUz_sDyn.txt");
							break;
						case 2:	
							k.writeNekArray(pVx_s,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pVx_s.txt");
							k.writeNekArray(pVy_s,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pVy_s.txt");
							k.writeNekArray(pVz_s,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pVz_s.txt");
							k.writeNekArray(pVx_sDyn,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pVx_sDyn.txt");
							k.writeNekArray(pVy_sDyn,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pVy_sDyn.txt");
							k.writeNekArray(pVz_sDyn,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pVz_sDyn.txt");
							break;
						case 3:
							k.writeNekArray(pWx_s,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pWx_s.txt");
							k.writeNekArray(pWy_s,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pWy_s.txt");
							k.writeNekArray(pWz_s,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pWz_s.txt");
							k.writeNekArray(pWx_sDyn,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pWx_sDyn.txt");
							k.writeNekArray(pWy_sDyn,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pWy_sDyn.txt");
							k.writeNekArray(pWz_sDyn,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pWz_sDyn.txt");
							break;
						case 4:
							k.writeNekArray(pX,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pX.txt");
							k.writeNekArray(pY,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pY.txt");
							k.writeNekArray(pZ,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pZ.txt");

							k.writeNekArray(pU_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pU_p.txt");
							k.writeNekArray(pV_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pV_p.txt");
							k.writeNekArray(pW_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pW_p.txt");
							k.writeNekArray(pP_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pP_p.txt");
							k.writeNekArray(pUx_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pUx_p.txt");
							k.writeNekArray(pUy_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pUy_p.txt");
							k.writeNekArray(pUz_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pUz_p.txt");
							k.writeNekArray(pVx_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pVx_p.txt");
							k.writeNekArray(pVy_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pVy_p.txt");
							k.writeNekArray(pVz_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pVz_p.txt");
							k.writeNekArray(pWx_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pWx_p.txt");
							k.writeNekArray(pWy_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pWy_p.txt");
							k.writeNekArray(pWz_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pWz_p.txt");
							k.writeNekArray(pVorU_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pVorU_p.txt");
							k.writeNekArray(pVorV_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pVorV_p.txt");
							k.writeNekArray(pVorW_p,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pVorW_p.txt");
							break;
						case 5:
							k.writeNekArray(pX,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pX.txt");
							k.writeNekArray(pY,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pY.txt");
							k.writeNekArray(pZ,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pZ.txt");
							
							k.writeNekArray(pE,fname+"_P_"+argv[2]+"_scaling_"+argv[3]+"_xVar_0p3_R_"+argv[4]+"_DLISAC_pE.txt");
							break;
					}


/*
	k.writeNekArray(pUx_sDyn,fname+"_P_"+argv[2]+"x_sDyncaling_"+argv[3]+"_xVar_"+std::to_string(pUx_sDyn[index])+"_DLISAC_pUx_sDyn.txt");
	k.writeNekArray(pVx_sDyn,fname+"_P_"+argv[2]+"x_sDyncaling_"+argv[3]+"_xVar_"+std::to_string(pVx_sDyn[index])+"_DLISAC_pVx_sDyn.txt");
	k.writeNekArray(pWx_sDyn,fname+"_P_"+argv[2]+"x_sDyncaling_"+argv[3]+"_xVar_"+std::to_string(pWx_sDyn[index])+"_DLISAC_pWx_sDyn.txt");
	
	k.writeNekArray(pUy_sDyn,fname+"_P_"+argv[2]+"y_sDyncaling_"+argv[3]+"_xVar_"+std::to_string(pUy_sDyn[index])+"_DLISAC_pUy_sDyn.txt");
	k.writeNekArray(pVy_sDyn,fname+"_P_"+argv[2]+"y_sDyncaling_"+argv[3]+"_xVar_"+std::to_string(pVy_sDyn[index])+"_DLISAC_pVy_sDyn.txt");
	k.writeNekArray(pWy_sDyn,fname+"_P_"+argv[2]+"y_sDyncaling_"+argv[3]+"_xVar_"+std::to_string(pWy_sDyn[index])+"_DLISAC_pWy_sDyn.txt");

	k.writeNekArray(pUz_sDyn,fname+"_P_"+argv[2]+"z_sDyncaling_"+argv[3]+"_xVar_"+std::to_string(pUz_sDyn[index])+"_DLISAC_pUz_sDyn.txt");
	k.writeNekArray(pVz_sDyn,fname+"_P_"+argv[2]+"z_sDyncaling_"+argv[3]+"_xVar_"+std::to_string(pVz_sDyn[index])+"_DLISAC_pVz_sDyn.txt");
	k.writeNekArray(pWz_sDyn,fname+"_P_"+argv[2]+"z_sDyncaling_"+argv[3]+"_xVar_"+std::to_string(pWz_sDyn[index])+"_DLISAC_pWz_sDyn.txt");
	
	k.writeNekArray(pUx_s,fname+"_P_"+argv[2]+"x_scaling_"+argv[3]+"_xVar_"+std::to_string(pUx_s[index])+"_DLISAC_pUx_s.txt");
	k.writeNekArray(pVx_s,fname+"_P_"+argv[2]+"x_scaling_"+argv[3]+"_xVar_"+std::to_string(pVx_s[index])+"_DLISAC_pVx_s.txt");
	k.writeNekArray(pWx_s,fname+"_P_"+argv[2]+"x_scaling_"+argv[3]+"_xVar_"+std::to_string(pWx_s[index])+"_DLISAC_pWx_s.txt");
	
	k.writeNekArray(pUy_s,fname+"_P_"+argv[2]+"y_scaling_"+argv[3]+"_xVar_"+std::to_string(pUy_s[index])+"_DLISAC_pUy_s.txt");
	k.writeNekArray(pVy_s,fname+"_P_"+argv[2]+"y_scaling_"+argv[3]+"_xVar_"+std::to_string(pVy_s[index])+"_DLISAC_pVy_s.txt");
	k.writeNekArray(pWy_s,fname+"_P_"+argv[2]+"y_scaling_"+argv[3]+"_xVar_"+std::to_string(pWy_s[index])+"_DLISAC_pWy_s.txt");

	k.writeNekArray(pUz_s,fname+"_P_"+argv[2]+"z_scaling_"+argv[3]+"_xVar_"+std::to_string(pUz_s[index])+"_DLISAC_pUz_s.txt");
	k.writeNekArray(pVz_s,fname+"_P_"+argv[2]+"z_scaling_"+argv[3]+"_xVar_"+std::to_string(pVz_s[index])+"_DLISAC_pVz_s.txt");
	k.writeNekArray(pWz_s,fname+"_P_"+argv[2]+"z_scaling_"+argv[3]+"_xVar_"+std::to_string(pWz_s[index])+"_DLISAC_pWz_s.txt");

	cout << "writing data out "<<double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	startTime = clock();
*/
	return 0;
}


/*
	vector<NekDouble> elementSizes;
    vector<NekDouble> centersX,centersY, centersZ;
	NekDouble sumSizes = 0.0;
    for (int j =0; j< elementIds.size();j++)
    {   
		int i = elementIds[j];
        int NumVerts = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetNumVerts();
        assert("NumVerts!=3"&& "This logic is not built for elements not tetrahedrons.");
        Nektar::SpatialDomains::PointGeomSharedPtr v0 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(0);
        Nektar::SpatialDomains::PointGeomSharedPtr v1 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(1);
        Nektar::SpatialDomains::PointGeomSharedPtr v2 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(2);
        Nektar::SpatialDomains::PointGeomSharedPtr v3 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(3);
        NekDouble ab = v0->dist(*v1);
        NekDouble ac = v0->dist(*v2);
        NekDouble ad = v0->dist(*v3);
        NekDouble bc = v1->dist(*v2);
        NekDouble bd = v1->dist(*v3);
        NekDouble cd = v2->dist(*v3);
        NekDouble max = 0.0;
        max = std::max(max,ab);
        max = std::max(max,ac);
        max = std::max(max,ad);
        max = std::max(max,bc);
        max = std::max(max,bd);
        max = std::max(max,cd);
        elementSizes.push_back(max);
        NekDouble v0x,v0y,v0z;
        NekDouble v1x,v1y,v1z;
        NekDouble v2x,v2y,v2z;
        NekDouble v3x,v3y,v3z;
        v0->GetCoords(v0x,v0y,v0z);
        v1->GetCoords(v1x,v1y,v1z);
        v2->GetCoords(v2x,v2y,v2z);
        v3->GetCoords(v3x,v3y,v3z);
        NekDouble cx = (v0x+v1x+v2x+v3x)/4.0;
        NekDouble cy = (v0y+v1y+v2y+v3y)/4.0;
        NekDouble cz = (v0z+v1z+v2z+v3z)/4.0;
        centersX.push_back(cx), centersY.push_back(cy), centersZ.push_back(cz);
		sumSizes+=max;
    }   

	NekDouble AvgScaling = sumSizes/elementIds.size() ; 
	cout << "AverageSize" << sumSizes/elementIds.size() << endl;
	NekDouble maxSize = *(std::max_element(elementSizes.begin(),elementSizes.end()));
	cout << "Max Size" << maxSize << endl; 
	
	cout << endl;

*/


/*
	vector<NekDouble> elementSizes;
    vector<NekDouble> centersX,centersY, centersZ;
	NekDouble sumSizes = 0.0;
    for (int j =0; j< elementIds.size();j++)
    {   
		int i = elementIds[j];
        int NumVerts = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetNumVerts();
        assert("NumVerts!=3"&& "This logic is not built for elements not tetrahedrons.");
        Nektar::SpatialDomains::PointGeomSharedPtr v0 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(0);
        Nektar::SpatialDomains::PointGeomSharedPtr v1 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(1);
        Nektar::SpatialDomains::PointGeomSharedPtr v2 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(2);
        Nektar::SpatialDomains::PointGeomSharedPtr v3 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(3);
        NekDouble ab = v0->dist(*v1);
        NekDouble ac = v0->dist(*v2);
        NekDouble ad = v0->dist(*v3);
        NekDouble bc = v1->dist(*v2);
        NekDouble bd = v1->dist(*v3);
        NekDouble cd = v2->dist(*v3);
        NekDouble max = 0.0;
        max = std::max(max,ab);
        max = std::max(max,ac);
        max = std::max(max,ad);
        max = std::max(max,bc);
        max = std::max(max,bd);
        max = std::max(max,cd);
        elementSizes.push_back(max);
        NekDouble v0x,v0y,v0z;
        NekDouble v1x,v1y,v1z;
        NekDouble v2x,v2y,v2z;
        NekDouble v3x,v3y,v3z;
        v0->GetCoords(v0x,v0y,v0z);
        v1->GetCoords(v1x,v1y,v1z);
        v2->GetCoords(v2x,v2y,v2z);
        v3->GetCoords(v3x,v3y,v3z);
        NekDouble cx = (v0x+v1x+v2x+v3x)/4.0;
        NekDouble cy = (v0y+v1y+v2y+v3y)/4.0;
        NekDouble cz = (v0z+v1z+v2z+v3z)/4.0;
        centersX.push_back(cx), centersY.push_back(cy), centersZ.push_back(cz);
		sumSizes+=max;
    }   

	NekDouble AvgScaling = sumSizes/elementIds.size() ; 
	cout << "AverageSize" << sumSizes/elementIds.size() << endl;
	NekDouble maxSize = *(std::max_element(elementSizes.begin(),elementSizes.end()));
	cout << "Max Size" << maxSize << endl; 
	
	cout << endl;

*/


/*
	vector<NekDouble> elementSizes;
    vector<NekDouble> centersX,centersY, centersZ;
	NekDouble sumSizes = 0.0;
    for (int j =0; j< elementIds.size();j++)
    {   
		int i = elementIds[j];
        int NumVerts = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetNumVerts();
        assert("NumVerts!=3"&& "This logic is not built for elements not tetrahedrons.");
        Nektar::SpatialDomains::PointGeomSharedPtr v0 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(0);
        Nektar::SpatialDomains::PointGeomSharedPtr v1 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(1);
        Nektar::SpatialDomains::PointGeomSharedPtr v2 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(2);
        Nektar::SpatialDomains::PointGeomSharedPtr v3 = HNM3D->m_expansions[0]->GetExp(i)->GetGeom()->GetVertex(3);
        NekDouble ab = v0->dist(*v1);
        NekDouble ac = v0->dist(*v2);
        NekDouble ad = v0->dist(*v3);
        NekDouble bc = v1->dist(*v2);
        NekDouble bd = v1->dist(*v3);
        NekDouble cd = v2->dist(*v3);
        NekDouble max = 0.0;
        max = std::max(max,ab);
        max = std::max(max,ac);
        max = std::max(max,ad);
        max = std::max(max,bc);
        max = std::max(max,bd);
        max = std::max(max,cd);
        elementSizes.push_back(max);
        NekDouble v0x,v0y,v0z;
        NekDouble v1x,v1y,v1z;
        NekDouble v2x,v2y,v2z;
        NekDouble v3x,v3y,v3z;
        v0->GetCoords(v0x,v0y,v0z);
        v1->GetCoords(v1x,v1y,v1z);
        v2->GetCoords(v2x,v2y,v2z);
        v3->GetCoords(v3x,v3y,v3z);
        NekDouble cx = (v0x+v1x+v2x+v3x)/4.0;
        NekDouble cy = (v0y+v1y+v2y+v3y)/4.0;
        NekDouble cz = (v0z+v1z+v2z+v3z)/4.0;
        centersX.push_back(cx), centersY.push_back(cy), centersZ.push_back(cz);
		sumSizes+=max;
    }   

	NekDouble AvgScaling = sumSizes/elementIds.size() ; 
	cout << "AverageSize" << sumSizes/elementIds.size() << endl;
	NekDouble maxSize = *(std::max_element(elementSizes.begin(),elementSizes.end()));
	cout << "Max Size" << maxSize << endl; 
	
	cout << endl;

*/


