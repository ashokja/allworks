#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include <boost/timer.hpp>


#include <iomanip> // std::setprecision
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
#define TOLERENCE_VIZ 1e-6

using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 8)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg is conditions file" << endl;
		cout << "3rd arg polynomial degree filter you want to apply" << endl;
		cout << "4th arg Number of sample points" << endl;
		cout << "5th arg Number of quadrature points" << endl;
		cout << "6th arg Number second quadrature points when applicable" << endl;
		cout << "7th arg Number second version 1 or 2 or 3 or 4(normal)" << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	//cout << var.size() << endl;
	//cout << var[0] << endl;
	HNM2D->LoadMesh(var[0]);
//	HNM2D->LoadMesh(var[1]);
//	HNM2D->LoadMesh(var[2]);
	string fname = vSession->GetSessionName();

	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);

	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]));
	}
	
	HNM2D->m_expansions[0]->FwdTrans(fce,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	ece = HNM2D->m_expansions[0]->GetPhys();
	HNM2D->m_Arrays.push_back(ece);
	HNM2D->LoadExpListIntoRTree();


	Array<OneD,NekDouble> ece_Coeffs =  HNM2D->m_expansions[0]->GetCoeffs();

	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();
	scaling = std::sqrt(2)*scaling;
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[1+2]), scaling );

	Array<OneD,NekDouble> directionL(3,0.0);
	directionL[0] = 1.0/sqrt(2);
	directionL[1] = 1.0/sqrt(2);
	vector<NekDouble> stPoint(3), endPoint(3);
	vector<int>  Gids,Eids;
	vector<NekDouble> HvalT; 

	Array<OneD,NekDouble> t_LineElm;

	vector<NekDouble> tparams, tvals;
	vector<NekDouble> tvals4, tvals7, tvals10;
	vector<NekDouble> tvals4_All, tvals7_All, tvals10_All;

	tparams.push_back(-0.5);
	int NptsX = atoi(argv[1+3]);
	NekDouble spaceX = 1.0/((double)(NptsX-1));
	for(size_t i=0 ;i < NptsX; i++)
	{
		tparams.push_back( 0.0 + i*spaceX*(1.0-0.0));
	}
	tparams.push_back(1.5);

	//loop through all values in y;
	int NptsY = 100;
	NekDouble spaceY = 1.0/((double)(NptsY-1));
	stPoint[0] = 0.0; stPoint[1] = 0.0; stPoint[2] = 0.0;
	double time_tvals4 = 0.0;
	NektarBaseClass k;
	int NumQpts1 = atoi(argv[1+4]);	
	int NumQpts2 = atoi(argv[1+5]);	

	cout << "version 1" << endl;
	boost::timer tim1;
	sm.EvaluateUsingLineAt(stPoint,directionL,NumQpts2,scaling,tparams,tvals4,0);
	time_tvals4 = tim1.elapsed();
	cout << "timeTaken\t"<< NumQpts1 <<"\t"<<NumQpts2 <<"\t"<< time_tvals4<< endl;
	k.writeNekArray(tvals4_All,fname+"_valsDiag_v1_"+std::to_string(NumQpts1)+"_"+std::to_string(NumQpts2)+"_ALL.txt");
	tvals4_All.clear();
	
	cout << "version 2" << endl;
	boost::timer tim2;
	sm.EvaluateUsingLineAt_v2( stPoint, directionL, NumQpts1,NumQpts2 , scaling ,tparams, tvals4, 0);
	time_tvals4 = tim2.elapsed();
	cout << "timeTaken\t"<< NumQpts1 <<"\t"<<NumQpts2 <<"\t"<< time_tvals4<< endl;
	k.writeNekArray(tvals4,fname+"_valsDiag_v2_"+to_string(NumQpts1)+"_"+to_string(NumQpts2)+"_ALL.txt");
	tvals4.clear();
	
	cout << "version 3" << endl;
	boost::timer tim3;
	sm.EvaluateUsingLineAt_v3( stPoint, directionL, NumQpts1,NumQpts2 , scaling ,tparams, tvals4, 0);
	time_tvals4 = tim3.elapsed();
	cout << "timeTaken\t"<< NumQpts1 <<"\t"<<NumQpts2 <<"\t"<< time_tvals4<< endl;
	k.writeNekArray(tvals4,fname+"_valsDiag_v3_"+to_string(NumQpts1)+"_"+to_string(NumQpts2)+"_ALL.txt");
	tvals4.clear();
	
	cout << "version 4" << endl;
	NekDouble valX,valY,valZ;
	boost::timer tim4;
		for(int i=0; i< NptsX;i++)
		{
			stPoint[0] = tparams[i];
			stPoint[1] = tparams[i];
			sm.EvaluateAt(stPoint[0], stPoint[1], stPoint[2],valX, valY,valZ, directionL, scaling ,0);
			tvals4_All.push_back(valX);
		}
	time_tvals4 = tim4.elapsed();
	cout << "timeTaken\t"<< NumQpts1 <<"\t"<<NumQpts2 <<"\t"<< time_tvals4<< endl;
	k.writeNekArray(tvals4_All,fname+"_valsDiag_v4_"+to_string(NumQpts1)+"_"+to_string(NumQpts2)+"_ALL.txt");
	tvals4.clear();

/*
	cout << "Version 1" << endl;
	NektarBaseClass k;
	for( int i =4; i<11; i++)
	{
	    int NumQpts = atoi(argv[i]);	
		boost::timer tim1;
		for (int j=0; j < NptsY; j++)
		{
			stPoint[1] = j*spaceY;
			sm.EvaluateUsingLineAt( stPoint, directionL, NumQpts , scaling ,tparams, tvals4, 0);
			copy(tvals4.begin()+1, tvals4.end()-1, back_inserter(tvals4_All));
		}
		time_tvals4 = tim1.elapsed();
//		std::cout << "speed-LSIAC time taken to evaluate at "<< tparams.size() << "is "<< time_tvals4 << " seconds"<< endl;
		//cout << tvals4_All.size()<< endl;
		k.writeNekArray(tvals4_All,fname+"_vals_v1_"+argv[i]+"_ALL.txt");
		cout << "timeTaken\t"<< argv[i] <<"\t"<< time_tvals4<< endl;
		//cleanout
		tvals4_All.clear();
	}


	cout << "Version 2" << endl;
	for( int i =5; i<11; i++)
	{
	    int NumQpts = atoi(argv[i]);	
		boost::timer tim1;
		for (int j=0; j < NptsY; j++)
		{
			stPoint[1] = j*spaceY;
			sm.EvaluateUsingLineAt_v2( stPoint, directionL,atoi(argv[4]), NumQpts , scaling ,tparams, tvals4, 0);
			copy(tvals4.begin()+1, tvals4.end()-1, back_inserter(tvals4_All));
		}
		time_tvals4 = tim1.elapsed();
//		std::cout << "speed-LSIAC time taken to evaluate at "<< tparams.size() << "is "<< time_tvals4 << " seconds"<< endl;
		//cout << tvals4_All.size()<< endl;
		k.writeNekArray(tvals4_All,fname+"_vals_v2_"+argv[4]+"_"+argv[i]+"_ALL.txt");
		cout << "timeTaken\t"<< argv[i] <<"\t"<< time_tvals4<< endl;
		//cleanout
		tvals4_All.clear();
	}


	cout << "Version 3" << endl;
	for( int i =5; i<11; i++)
	{
	    int NumQpts = atoi(argv[i]);	
		boost::timer tim1;
		for (int j=0; j < NptsY; j++)
		{
			stPoint[1] = j*spaceY;
			sm.EvaluateUsingLineAt_v3( stPoint, directionL,atoi(argv[4]), NumQpts , scaling ,tparams, tvals4, 0);
			copy(tvals4.begin()+1, tvals4.end()-1, back_inserter(tvals4_All));
		}
		time_tvals4 = tim1.elapsed();
//		std::cout << "speed-LSIAC time taken to evaluate at "<< tparams.size() << "is "<< time_tvals4 << " seconds"<< endl;
		//cout << tvals4_All.size()<< endl;
		k.writeNekArray(tvals4_All,fname+"_vals_v3_"+argv[4]+"_"+argv[i]+"_ALL.txt");
		cout << "timeTaken\t"<< argv[i] <<"\t"<< time_tvals4<< endl;
		//cleanout
		tvals4_All.clear();
	}

	cout << "Version 4" << endl;
	NekDouble valX,valY,valZ;
	boost::timer tim1;
	for (int j=0; j< NptsY; j++)
	{
		for(int i=0; i< NptsX;i++)
		{
			stPoint[0] = i*spaceX;
			stPoint[1] = j*spaceY;
			sm.EvaluateAt(stPoint[0], stPoint[1], stPoint[2],valX, valY,valZ, directionL, scaling ,0);
			tvals4_All.push_back(valX);
		}
	}
	time_tvals4 = tim1.elapsed();
	k.writeNekArray(tvals4_All,fname+"_vals_v4_"+argv[4]+"_ALL.txt");
	cout << "timeTaken\t"<< "CI" <<"\t"<< time_tvals4<< endl;
*/
	return 0;
}

