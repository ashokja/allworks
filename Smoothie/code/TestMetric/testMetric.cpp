#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include "MetricTensor.h"


using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 8)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		cout << "4th arg Resolution of output." << endl;
		cout << "5th arg startX." << endl;
		cout << "6th arg endX." << endl;
		cout << "7th arg yCoord." << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM2D->LoadMesh(var[0]);
	string fname = vSession->GetSessionName();
	MetricTensor* metricT = new MetricTensor();
	metricT->LoadMetricTensor(HNM2D);
	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);
//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);

	for (int i=0;  i< tNquadPts; i++)
	{
//		fce[i] = std::cos(2.0*2.0*M_PI*(xc0[i]+xc1[i]));
		fce[i] = std::cos(2.0*M_PI*(xc0[i]+xc1[i]));
		//fce[i] = std::cos(2.0*2.0*M_PI*(xc0[i]))*std::cos(2.0*2.0*M_PI*(xc1[i]));
	}
	
	HNM2D->m_expansions[0]->FwdTrans(fce,HNM2D->m_expansions[0]->UpdateCoeffs() );
	HNM2D->m_expansions[0]->BwdTrans( HNM2D->m_expansions[0]->GetCoeffs(),
										HNM2D->m_expansions[0]->UpdatePhys());
	 ece = HNM2D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM2D->m_expansions[0]->GetCoeffs();

	HNM2D->m_Arrays.push_back(ece);
	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();
	// Evaluating on line y =0;
	// Evaluate on a new equal space grid mesh.
	
	int gPtsLy = atoi(argv[4])+1;
	int totPtsLy = gPtsLy;
	NekDouble sx = 1.0/(gPtsLy-1);	
	Array<OneD,NekDouble> pLy(totPtsLy),pLNx(totPtsLy), pLPx(totPtsLy), pELy(totPtsLy);
	Array<OneD,NekDouble> pSMet1Ly(totPtsLy),pSMet2Ly(totPtsLy);
	Array<OneD,NekDouble> pXLy(totPtsLy), pYLy(totPtsLy), pVLy(totPtsLy), 
				pPLy(totPtsLy), pSLy(totPtsLy),pDynLy(totPtsLy),pSDynLy(totPtsLy);
	Array<OneD,NekDouble> glCoords(3,0.0);
	NekDouble valX,valY,valZ;
	Array<OneD,NekDouble> direction(3,0.0), coord(3,0.0), directionM(3,0.0);
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp2, HNM2D, atoi(argv[2]), atof(argv[3]) ); 
	direction[1] = 1.0;
	NekDouble lambda;
	Array<OneD,NekDouble> eig(3,0.0);
	NekDouble startX = atof(argv[5]);
	NekDouble endX = atof(argv[6]);
	NekDouble yCoord = atof(argv[7]);
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();
	for (int i =0 ; i< gPtsLy; i++)
	{
		int index = i;
		pXLy[index] = startX + i*sx*(endX-startX);
		pYLy[index] = yCoord;
			pVLy[index] = std::cos(2.0*M_PI*(pXLy[index] + pYLy[index]) );
			//pV[index] = std::cos(2.0*2.0*M_PI*(pX[index]))*std::cos(2.0*2.0*M_PI*(pY[index]));
			glCoords[0] = pXLy[index]; glCoords[1] = pYLy[index];
			NekDouble dynScaling = HNM2D->GetDynamicScaling(glCoords);
			pDynLy[index] = dynScaling;
			//cout << dynScaling<<endl;
			sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSLy[index],valY,valZ,direction,scaling ,0);
			sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSDynLy[index],valY,valZ,direction, dynScaling ,0);
			coord[0] = pXLy[index]; coord[1] = pYLy[index]; coord[2] = 0.0;
//			cout << setprecision(19) << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;
			int elid = HNM2D->GetExpansionIndexUsingRTree(coord);
//			cout << elid << endl;
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
			//int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
			const Array<OneD,NekDouble> el_phys = ece.CreateWithOffset(
										ece, coeffOffset);
			pPLy[index] = lexp->PhysEvaluate(coord,el_phys);
			pELy[index] = elid;
			//cout << "before into metric" <<endl;

			metricT->GetEigenPair(glCoords, elid, 1,lambda,eig);
			directionM[0] = eig[0];	directionM[1] = eig[1];
			//cout << elid <<"\t"<<1<< "\t" << lambda << "\t" << eig[0] << "\t"<< eig[1] <<"\t" << eig[2] << endl;
			sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMet1Ly[index],valY,valZ,directionM, lambda,0);
			metricT->GetEigenPair(glCoords, elid, 2,lambda,eig);
			directionM[0] = eig[0];	directionM[1] = eig[1];
			//cout << elid << "\t"<<2<<"\t"<< lambda << "\t" << eig[0] << "\t"<< eig[1] <<"\t" << eig[2] << endl;
			sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pSMet2Ly[index],valY,valZ,directionM, lambda,0);
			//cout << i << endl;
	}

	cout << "Done writing Line" << endl;
	NektarBaseClass k;
	k.writeNekArray(pXLy,fname+"_"+argv[2]+"_R_"+argv[4]+"_pXLy_2DDyn.txt");
	k.writeNekArray(pYLy,fname+"_"+argv[2]+"_R_"+argv[4]+"_pYLy_2DDyn.txt");
	k.writeNekArray(pELy,fname+"_"+argv[2]+"_R_"+argv[4]+"_pELy_2DDyn.txt");
	k.writeNekArray(pVLy,fname+"_"+argv[2]+"_R_"+argv[4]+"_pVLy_2DDyn.txt");
	k.writeNekArray(pPLy,fname+"_"+argv[2]+"_R_"+argv[4]+"_pPLy_2DDyn.txt");
	k.writeNekArray(pSLy,fname+"_"+argv[2]+"_R_"+argv[4]+"_pSLy_2DDyn.txt");
	k.writeNekArray(pSDynLy,fname+"_"+argv[2]+"_R_"+argv[4]+"_pSDynLy_2DDyn.txt");
	k.writeNekArray(pSMet1Ly,fname+"_"+argv[2]+"_R_"+argv[4]+"_pSMet1Ly_2DDyn.txt");
	k.writeNekArray(pSMet2Ly,fname+"_"+argv[2]+"_R_"+argv[4]+"_pSMet2Ly_2DDyn.txt");
	k.writeNekArray(pDynLy,fname+"_"+argv[2]+"_R_"+argv[4]+"_pDynLy_2DDyn.txt");

	// Evaluate on line x =0.5; x = -0.5;

	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}

/*
// Evaluate on a new equal space grid mesh.
	int gPts = atoi(argv[4]);
	int totPts = gPts*gPts;
	int Nx = gPts, Ny=gPts;
	NekDouble sx = 2.0/(Nx-1.0), sy = 2.0/(Ny-1.0);
	
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1_1SIDED_2kp1, HNM2D, atoi(argv[2]), atof(argv[3]) ); 
	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();
	Array<OneD,NekDouble> direction(3,0.0), coord(3,0.0);
	direction[0] = 1.0;
	Array<OneD,NekDouble> pX(totPts), pY(totPts), pV(totPts), pP(totPts), pS(totPts),pDyn(totPts),pSDyn(totPts),pE(totPts);
	Array<OneD,NekDouble> glCoords(3,0.0);
	for( int i =0; i < Nx; i++)
	{
		for(int j=0; j< Ny; j++)
		{
			int index = i*Nx+j;
			pX[index] = i*sx-1.0;
			pY[index] = j*sy-1.0;
			pV[index] = std::cos(2.0*(pX[index] + pY[index]) );
			//pV[index] = std::cos(2.0*2.0*M_PI*(pX[index]))*std::cos(2.0*2.0*M_PI*(pY[index]));
			glCoords[0] = pX[index]; glCoords[1] = pY[index];
			NekDouble dynScaling = HNM2D->GetDynamicScaling(glCoords);
			pDyn[index] = dynScaling;
			sm.EvaluateAt(pX[index],pY[index],0.0,pS[index],valY,valZ,direction, atof(argv[3]) ,0);
			sm.EvaluateAt(pX[index],pY[index],0.0,pSDyn[index],valY,valZ,direction, dynScaling ,0);
			coord[0] = pX[index]; coord[1] = pY[index]; coord[2] = 0.0;
//			cout << setprecision(19) << coord[0] << "\t" << coord[1] << "\t" << coord[2] << endl;
			int elid = HNM2D->GetExpansionIndexUsingRTree(coord);
//			cout << elid << endl;
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
			//int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int coeffOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
			const Array<OneD,NekDouble> el_phys = ece.CreateWithOffset(
										ece, coeffOffset);
			pP[index] = lexp->PhysEvaluate(coord,el_phys);
			pE[index] = elid;
//			cout << j << endl;
		}
		cout << i << endl;
	}
	NektarBaseClass k;
	k.writeNekArray(pX,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pX_2DDyn.txt");
	k.writeNekArray(pY,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pY_2DDyn.txt");
	k.writeNekArray(pE,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pE_2DDyn.txt");
	k.writeNekArray(pV,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pV_2DDyn.txt");
	k.writeNekArray(pP,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pP_2DDyn.txt");
	k.writeNekArray(pS,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pS_2DDyn.txt");
	k.writeNekArray(pSDyn,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pSDyn_2DDyn.txt");
	k.writeNekArray(pDyn,fname+"_"+argv[2]+"_S_"+argv[3]+"_R_"+argv[4]+"_pDyn_2DDyn.txt");

	cout << "Done writing plane" << endl;
	//plane completed.
*/
