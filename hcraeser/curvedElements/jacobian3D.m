function J = jacobian3D(e1,e2,e3,Xa,Xb,Xc,Xd,Xe,Xf,Xg,Xh)
    J = zeros(3,3);
    J(:,1) = -Xa*(1-e2)*(1-e3)/8 ...
                +Xb*(1-e2)*(1-e3)/8 ...
                    +Xc*(1+e2)*(1-e3)/8 ...
                        -Xd*(1+e2)*(1-e3)/8 ...
                            -Xe*(1-e2)*(1+e3)/8 ...
                                +Xf*(1-e2)*(1+e3)/8 ...
                                    +Xg*(1+e2)*(1+e3)/8 ...
                                        -Xh*(1+e2)*(1+e3)/8;
    J(:,2) = -Xa*(1-e1)*(1-e3)/8 ...
                -Xb*(1+e1)*(1-e3)/8 ...
                    +Xc*(1+e1)*(1-e3)/8 ...
                        +Xd*(1-e1)*(1-e3)/8 ...
                            -Xe*(1-e1)*(1+e3)/8 ...
                                -Xf*(1+e1)*(1+e3)/8 ...
                                    +Xg*(1+e1)*(1+e3)/8 ...
                                        +Xh*(1-e1)*(1+e3)/8;
    J(:,3) = -Xa*(1-e1)*(1-e2)/8 ...
                -Xb*(1+e1)*(1-e2)/8 ...
                    -Xc*(1+e1)*(1+e2)/8 ...
                        -Xd*(1-e1)*(1+e2)/8 ...
                            +Xe*(1-e1)*(1-e2)/8 ...
                                +Xf*(1+e1)*(1-e2)/8 ...
                                    +Xg*(1+e1)*(1+e2)/8 ...
                                        +Xh*(1-e1)*(1+e2)/8;

                                    
    %J(1,2) = -Xa*(1-e2)/4 + Xb*(1-e2)/4 +Xc(1+e2)/4 - Xd*(1+e2)/4;
    %J(:,2) = -Xa*(1-e1)/4 - Xb*(1+e1)/4 +Xc*(1+e1)/4 + Xd*(1-e1)/4;
    %J(2,2) = -Xa*(1-e2)/4 + Xb*(1-e2)/4 +Xc(1+e2)/4 - Xd*(1+e2)/4;
end