clear all;
close all;

% automate filenames.
% loop through all the files and resolutions.
%   write code to pull L2 and Linf error of all files.
% print them in latex format.

poly = [2,3,4];
reso = [11,21,41,81,161];
%folder = 'DG_diffusion_4pi_2D/';
folder = 'DG_AdvConvectionDominated_4pi_2D_check/';
filename = '_codesquare';

errs = zeros(max(size(poly)), max(size(reso)),2);
countp =0;
for p = poly
    countp=countp+1;
    countr=0;
    for r = reso
        countr=countr+1;
        fname = strcat(folder,'P',int2str(p),filename,int2str(r),'.log')
        C = ReadFile(fname);
        C(1) = C(1)/(4*pi*4*pi);
        disp(fname);
        disp(C);
        errs(countp,countr,:) = C;
    end
end

countp =0;
for p = poly
    countp=countp+1;
    countr=0;
    for r = 1:max(size(reso))-1
        errOrdL2(countp,r+1) = (log( errs(countp,r,1)/errs(countp,r+1,1))/log(2));
        errOrdLInf(countp,r+1) = (log( errs(countp,r,2)/errs(countp,r+1,2))/log(2));
        
    end
end


countp =0
for p = poly
    countp = countp+1;
    data = [reso',squeeze(errs(countp,:,1))', squeeze(errOrdL2(countp,:))', squeeze(errs(countp,:,2))', squeeze(errOrdLInf(countp,:))'];
    input.data = data;
    input.tableColLabels = {'No Mesh El','L2 error','L2 Ord','Linf error','Linf ord'};
    input.tableCaption = strcat('NumModes =',int2str(p),' Order=',int2str(p),' poly=',int2str(p-1),' Errors ');
    input.dataFormat = {'%i',1,'%2.3e',1,'%1.2f',1,'%2.3e',1,'%1.2f',1};
    latex = latexTable(input);
%    disp(data);
end
% data = [(1:3)',squeeze(errs(:,:,1))]
% input.data = data;
% % input.tableColLabels = {'No Mesh El','DG(Linf)','SIAC(C)','SIAC(M)','HSIAC(M)'};
% %input.dataFormat = {'%i',1,'%2.3e',NoOfCol};
% input.dataFormat = {'%i',1,'%2.3e',6};
% 
% % input.tableCaption = strcat('P2 Linf DG ');
% % input.tableLabel = strcat('P2_Linf_DG');
% latex = latexTable(input);
% 


