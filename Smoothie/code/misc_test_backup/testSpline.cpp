#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>
#include <limits>   //std::numeric_limits

#include <time.h>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
//#include "HandleNekMesh2D.h"
#include "HandleNekMesh3D.h"

#include <iomanip>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>

#include <boost/geometry/index/rtree.hpp>


//to store queries results.
#include <vector>

//just for output
#include <iostream>
#include <boost/foreach.hpp>
#include <boost/timer.hpp>


using namespace SIACUtilities;
using namespace std;

class bspline: NektarBaseClass
{
	public:
		int m_deg;
		vector<NekDouble> m_cpts;
		vector<NekDouble> m_knots;
		
	bspline()
	{
		m_deg = 3;
		m_knots.push_back( 0.0);  m_knots.push_back( 0.0);  m_knots.push_back( 0.0);  m_knots.push_back( 0.0);  
		m_knots.push_back( 1.0);  m_knots.push_back( 1.0);  m_knots.push_back( 1.0);  m_knots.push_back( 1.0);  
		m_cpts.push_back( 0.0);  m_cpts.push_back( 0.0);  m_cpts.push_back( 0.0);  m_cpts.push_back( 1.0);  
	}

	void findks(const NekDouble u, int &k, int &s) const 
	{
		k = 0;
		s = 0;
		for ( k =0; k < m_knots.size(); k++)
		{
			if ( (std::abs(m_knots[k]-u) < TOLERENCE) )
			{
				s++;
			}else if ( u < m_knots[k])
			{
				break;
			}
		}
		k--;
	}

	void expandSupport()
	{
		for ( int i = 0; i < m_deg; i++)
		{
			m_knots.insert(m_knots.begin(), m_knots[0]-1.0);
			m_knots.insert(m_knots.end(), *(m_knots.end())+1.0);
		}
		m_cpts.insert(m_cpts.begin(), m_deg,0.0);
		m_cpts.insert(m_cpts.end(), m_deg,0.0);
	}


	bool validk( int k, int s)
	{
		if ( ( k > m_deg-1 ) &&( k < ( m_knots.size() - m_deg -1 + s) ))
		{
			return true;
		}else
		{
			return false;
		}		
	}
	
	void EvaluateUArr( const vector<NekDouble> &uAr, vector<NekDouble> &solAr, vector<NekDouble> &pts_eval)
	{
		int k, s;
		NekDouble u, sol;
		for ( int i =0; i < uAr.size(); i++)
		{
			u = uAr[i];
			this->findks( u, k,s);
	
				// debug
		//cout << k << " and " << s << endl; 
				
		// 2. Handle if  s = deg+1; 
			if ( m_deg+1 == s)
			{
				sol = m_cpts[k - m_deg];
				//cout << "u: " << u << "exit s= deg+1 k:"<<k << endl;
				if (k == m_knots.size()-1)
				{
					solAr[i] = m_cpts[k-m_deg-1];
				}
				continue;
				//return; 
			}
	
			// 3. If k is invalid return 0.
			if ( !this->validk(k,s))
			{
				solAr[i] = 0.0;
				//return;
				continue;	
			}

			// 4. Evaualte sol.
//			vector<NekDouble> pts_eval( m_cpts.begin()+ k - m_deg, m_cpts.begin()+k - s+1);
			memcpy( &pts_eval[0], &m_cpts[k-m_deg], (m_deg-s+1)*sizeof(NekDouble));
			NekDouble alpha = 0.0;
			int ptIndex = 0;
			for ( int r =1; r <= m_deg-s; r++)
			{
				for (int i = k-m_deg +r ; i<= k-s; i++)
				{
					alpha =  ( u - m_knots[i] )/ (m_knots[i+m_deg-r+1] -m_knots[i] );
					ptIndex =  i - k + m_deg-r ;
					pts_eval[ptIndex] = (1-alpha)*pts_eval[ptIndex]+alpha*pts_eval[ptIndex+1];
//					cout << "r: "<<r << " i:"<<i<< " ptIndex:" << ptIndex << endl;
				}
			}
			solAr[i] = pts_eval[0];
		}
		return;	
	}
	
	void EvaluateUA( const NekDouble u, NekDouble &sol, vector<NekDouble> &pts_eval)
	{
		int k, s;
		this->findks( u, k,s);
	
				// debug
		//cout << k << " and " << s << endl; 
				
		// 2. Handle if  s = deg+1; 
		if ( m_deg+1 == s)
		{
			sol = m_cpts[k - m_deg];
			//cout << "u: " << u << "exit s= deg+1 k:"<<k << endl;
			if (k == m_knots.size()-1)
			{
				sol = m_cpts[k-m_deg-1];
			}
			return; 
		}

		// 3. If k is invalid return 0.
		if ( !this->validk(k,s))
		{
			sol = 0.0;
			return;	
		}

		// 4. Evaualte sol.
//		vector<NekDouble> pts_eval( m_cpts.begin()+ k - m_deg, m_cpts.begin()+k - s+1);
		memcpy( &pts_eval[0], &m_cpts[k-m_deg], (m_deg-s+1)*sizeof(NekDouble));
		NekDouble alpha = 0.0;
		int ptIndex = 0;
		for ( int r =1; r <= m_deg-s; r++)
		{
			for (int i = k-m_deg +r ; i<= k-s; i++)
			{
				alpha =  ( u - m_knots[i] )/ (m_knots[i+m_deg-r+1] -m_knots[i] );
				ptIndex =  i - k + m_deg-r ;
				pts_eval[ptIndex] = (1-alpha)*pts_eval[ptIndex]+alpha*pts_eval[ptIndex+1];
//				cout << "r: "<<r << " i:"<<i<< " ptIndex:" << ptIndex << endl;
			}
		}
		sol = pts_eval[0];	
	}
 
	void EvaluateU( const NekDouble u, NekDouble &sol)
	{
		int k, s;
		this->findks( u, k,s);
	
				// debug
		//cout << k << " and " << s << endl; 
				
		// 2. Handle if  s = deg+1; 
		if ( m_deg+1 == s)
		{
			sol = m_cpts[k - m_deg];
			//cout << "u: " << u << "exit s= deg+1 k:"<<k << endl;
			if (k == m_knots.size()-1)
			{
				sol = m_cpts[k-m_deg-1];
			}
			return; 
		}

		// 3. If k is invalid return 0.
		if ( !this->validk(k,s))
		{
			sol = 0.0;
			return;	
		}

		// 4. Evaualte sol.
		vector<NekDouble> pts_eval( m_cpts.begin()+ k - m_deg, m_cpts.begin()+k - s+1);
		NekDouble alpha = 0.0;
		int ptIndex = 0;
		for ( int r =1; r <= m_deg-s; r++)
		{
			for (int i = k-m_deg +r ; i<= k-s; i++)
			{
				alpha =  ( u - m_knots[i] )/ (m_knots[i+m_deg-r+1] -m_knots[i] );
				ptIndex =  i - k + m_deg-r ;
				pts_eval[ptIndex] = (1-alpha)*pts_eval[ptIndex]+alpha*pts_eval[ptIndex+1];
//				cout << "r: "<<r << " i:"<<i<< " ptIndex:" << ptIndex << endl;
			}
		}
		sol = pts_eval[0];	
	}
		
};

int main( int argc, char* argv[])
{
	// parameters using
	bspline bsp;
	
	int k,s;
	// 1. step one of algorithm.

	NekDouble sol; 
	bsp.EvaluateU( 0.4, sol);
	cout << "Sol :\t" << sol << endl; 

	for ( NekDouble t = 0.0; t <=1.0; t= t+0.1)
	{
		NekDouble sol;
		bsp.EvaluateU( t, sol);
		cout << sol << endl;
	}
	cout << "END of FIRST TEST CASE" << endl;
	// second test.
	vector<NekDouble> knots, cpts;
	knots.push_back( -0.3); knots.push_back(-0.2); knots.push_back(-0.1); knots.push_back(0.0);
	knots.push_back( 0.5); knots.push_back(0.5); knots.push_back(0.5); knots.push_back(0.5);
	knots.push_back( 1.0); knots.push_back(1.2); knots.push_back(1.3); knots.push_back(1.4);

	cpts.push_back( 0.1);	cpts.push_back( 0.3);	cpts.push_back( 0.7); cpts.push_back(0.5);
	cpts.push_back(0.45); cpts.push_back(0.1); cpts.push_back(0.2); cpts.push_back(1.0);

	bsp.m_knots  = (knots);
	bsp.m_cpts = (cpts);

	for ( NekDouble t = 0.0; t <=1.0; t= t+0.1)
	{
		NekDouble sol;
		bsp.EvaluateU( t, sol);
		cout << sol << endl;
	}

	bsp.EvaluateU(1.0,sol);
	cout << sol << endl;
	bsp.EvaluateU(1.2,sol);
	cout << sol << endl;

	cout << "END of Second Test CASE" << endl;
	
	knots.clear();
	cpts.clear();

	cpts.push_back(-0.083333);	cpts.push_back(1.6667);	cpts.push_back(-0.0833333);	
	cpts.push_back(-0.083333);	cpts.push_back(1.6667);	
	for (NekDouble f =-3.5; f <=3.5;f += 1.0)
	{
		knots.push_back(f);
	}

	bsp.m_deg = 2;
	bsp.m_knots  = (knots);
	bsp.m_cpts = (cpts);

	bsp.expandSupport();
	cout << endl;
	NekDouble sol1;
	vector<NekDouble> temp(10),tAr,solAr;
	for ( NekDouble t = -2.0; t <=1.9; t= t+0.1)
	{
		tAr.push_back(t);	
		solAr.push_back(t);	
	}
	boost::timer tttim;
	for ( int i = 0; i <100; i++)
	{
		bsp.EvaluateUArr( tAr, solAr, temp);
/*	
		for ( NekDouble t = -2.0; t <=1.9; t= t+0.1)
		{
			bsp.EvaluateUA( t, sol,temp);
		//	bsp.EvaluateU(t, sol1);
		//	cout << t << "\t\t"<< sol << "\t"<< endl;
		}
*/
	}
	double s_time = tttim.elapsed();
 	std::cout << "time taken : "<< s_time << " sec"<< std::endl;
/*	
	cout << endl;
	cout << endl;
		bsp.EvaluateU(-1.0 , sol);
		cout << sol << "\t";
	cout << endl;
	cout << endl;
	for ( NekDouble t = -2.5; t <=2.5; t= t+0.1)
	{
		cout << t << "\t";
	}
	cout << endl;
*/
	return 0;
}
