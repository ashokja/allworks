clear;
close all;
% test 
%rates = [10,20,40,60,80];
%rates = [20,40,60,80];
rates = [80];

poly =2;
order = poly;
count =1;
for r = rates
    
    xfname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_pX_2D_OneSided2kp1.txt' );
    yfname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_pY_2D_OneSided2kp1.txt' );
    ffname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_pV_2D_OneSided2kp1.txt' );
    efname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_pP_2D_OneSided2kp1.txt' );
    sfname = strcat('datatxt/P',int2str(poly),'_quad_simple_',int2str(r),'_',int2str(order),'_pS_2D_OneSided2kp1.txt' );

    disp(xfname);
    xpos = textread(xfname, '%f\t');
    ypos = textread(yfname, '%f\t');
    fval = textread(ffname, '%f\t');
    pval = textread(efname, '%f\t');
    sval = textread(sfname, '%f\t'); 
        
    s = sqrt(max(size(xpos)));
    xVpos = reshape(xpos,s,s);
    yVpos = reshape(ypos,s,s);
    fVval = reshape(fval,s,s);
    pVval = reshape(pval,s,s);
    sVval = reshape(sval,s,s);    
    
%     xVpos = xVpos(5:15, 5:15);
%     yVpos = yVpos(5:15, 5:15);    
%     fVval = fVval(5:15, 5:15);
%     pVval = pVval(5:15, 5:15);
%     sVval = sVval(5:15, 5:15);
    
    figure(1)
    surf(xVpos, yVpos, log10(abs(fVval-pVval))); hold on;
    title('proj error');
    
    figure(6)
    surf(xVpos, yVpos, log10(abs(fVval-sVval)) ); hold on;
    title('SIAC error');
    
    figure(7)
    surf(xVpos, yVpos, log10(abs(pVval-sVval)) ); hold on;
    title('SIAC-Proj diff');
    %plot3(xpos,ypos, log10(abs(fval-pval)),'*',xpos,ypos,log10(abs(fval-sval)),'*');
    
    figure(2)
    %plot3(xpos,ypos, log10(abs(fval-sval)),'*','DisplayName',strcat('No of ele:',int2str(r)));
    surf(xVpos,yVpos, log10(abs(fVval-sVval)),'DisplayName',strcat('No of ele:',int2str(r)));
    
    hold on;

    figure(3)
    %plot3(xpos,ypos, pval);
    surf(xVpos,yVpos, sVval); hold on;
     %   surf(xVpos,yVpos, pVval); hold off;
    caxis([-1.3 1.3]); colorbar;
    title('SIAC');

    figure(4)
    surf(xVpos,yVpos, pVval); hold on;
    caxis([-1.3 1.3]); colorbar;
    title('Proj');

    figure(5)
    surf(xVpos,yVpos, fVval); hold on;
    caxis([-1.3 1.3]); colorbar;
    title('original');
    
    
    figure(8)
    hist( log10(abs(fVval-pVval)) );
    figure(9)
    hist( log10(abs(fVval-sVval)) );
    
    dumpp1 = abs(fVval-sVval);
    dumpp2 = abs(fVval-pVval);
    d3  = dumpp2 > dumpp1;
    
    figure, imagesc(d3);
    disp(sum(d3(:))/max(size(d3(:))));
    
    p_avg_err(count) = sum(abs(fval-pval))/max(size(xpos));
    s_avg_err(count) = sum(abs(fval-sval))/max(size(xpos));
    
    p_max_err(count) = max(abs(fval-pval));
    s_max_err(count) = max(abs(fval-sval));
    count = count+1;
end

% figure(1),
% legend('show');
% figure(2),
% legend('show');
% title(strcat('poly-',int2str(poly),' order-',int2str(order)));
% xlabel('domain');
% ylabel('Cos(2*pi*x)');
% saveas(2,strcat('S2O2poly_',int2str(poly),'_order_',int2str(order)),'jpg');

% figure(3)
% plot(log10(rates),log10(p_avg_err),log10(rates),log10(s_avg_err));
% title(strcat('poly-',int2str(poly),' order-',int2str(order),'  AvgError'));
% xlabel('Mesh Samping');
% ylabel('Error');
% legend('projected','SIAC Error');
% saveas(3,strcat('S2O2poly_',int2str(poly),'_order_',int2str(order),'_AvgERplot'),'jpg');
% 
% 
% figure(4)
% plot(log10(rates),log10(p_max_err),log10(rates),log10(s_max_err));
% title(strcat('poly-',int2str(poly),' order-',int2str(order),'  MaxError'));
% xlabel('Mesh Samping');
% ylabel('Error');
% legend('projected','SIAC Error');
