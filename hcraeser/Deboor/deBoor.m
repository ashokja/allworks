% recrusive Deboor
function [p] = deBoor( k, deg, i, x, knots, ctrlPoints)
    if k == 0
        p = ctrlPoints(i+1);
        %return sol;
        return;
    else
        alpha = (x-knots(i+1))/( knots(i+deg+1-k+1) -knots(i+1));
        p =  deBoor( k-1, deg, i-1, x, knots, ctrlPoints) * (1-alpha) + deBoor(k-1, degree, i,x, knots,ctrlPoints)*alpha;
        return ;
    end
    
end