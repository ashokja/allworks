%% Test mass matrix mapping with more mapping points.
clear;
close all;
addpath('../PolyLib');

Degrees =(7);
for Deg = Degrees
%Deg = 4;
        a = 0; b = 10*pi;
    Nv = 16;
    Ne = Nv-1;
    v_x = a+ linspace(0,1,Nv)*(b-a);

    vizSamples= 800;
    np = Deg+3;
    [r,w] = JacobiGLZW(np,0,0);

    x = zeros(np,Ne);
    h_e = zeros(Ne,1);
    for e = 1:Ne
        x(:,e) = v_x(e) + (r+1)/2*(v_x(e+1)-v_x(e));
        h_e(e) = v_x(e+1)-v_x(e);
    end
    % calculate u_act using function 
    u_act = sin(x);

    %Build and use mass matrix for coeffecints.
    M = zeros(Deg+1,Deg+1);
    for i = 0:Deg
        for j =0:Deg
            M(i+1,j+1) = sum( JacobiPoly(i,r,0,0) ...
                                .*JacobiPoly(j,r,0,0) .* ...
                                w);
        end
    end
    
    rhs = zeros(Deg+1,1);
    u_hat = zeros(Deg+1,Ne);
    for e =1:Ne
        for i=0:Deg
            rhs(i+1) = sum(JacobiPoly(i,r,0,0).*w.*u_act(:,e));
        end
        u_hat(:,e) = M\rhs;
    end

    
    
    
    %
    rviz = linspace(-1,1,vizSamples);
    x_viz = zeros(vizSamples,Ne);
    for e = 1:Ne
        x_viz(:,e) = v_x(e) + (rviz+1)/2*(v_x(e+1)-v_x(e));
    end

    Vld_viz = zeros(vizSamples,Deg+1);
    for d = 0:Deg
       Vld_viz(:,d+1) = JacobiPoly(d,rviz,0,0); 
    end

    
    u_sim_viz = Vld_viz*u_hat;
    % Elementwise Local Derivative.

    Vld_viz_Der = zeros(vizSamples,Deg+1);
    for d = 0:Deg
       Vld_viz_Der(:,d+1) = JacobiPolyDerivative(d,rviz,0,0); 
    end

    u_simDer_viz = Vld_viz_Der*u_hat;
    for e=1:Ne
        u_simDer_viz(:,e) = u_simDer_viz(:,e)/h_e(e)*2;
    end

%     figure(1),
%     plot(x_viz,u_sim_viz); 
%     %hold on;
%     %plot(x_viz,sin(x_viz),'r-');
%     title('Function Projection');
%
%     figure(2),
%     plot(x_viz,u_simDer_viz); 
%     %hold on;
%     %plot(x_viz,cos(x_viz),'r.');
%     title('Function Derivative');

    u_er_P_Act(Deg) = sum(sum(abs( (u_sim_viz-sin(x_viz))/max(size(x_viz(:))))));
    u_D_er_L_Act(Deg) = sum(sum(abs((u_simDer_viz-cos(x_viz))/max(size(x_viz(:))))));
    
    
    %DG derivative when you do purely by each element.
    Vld = zeros(np,Deg+1);
    for d = 0:Deg
       Vld(:,d+1) = JacobiPoly(d,r,0,0); 
    end
    
    rhs = zeros(Deg+1,1);
    u_D_hat_DG0 = zeros(Deg+1,Ne);
    for e =1:Ne
        u_reuse = Vld*u_hat(:,e);
        for i=0:Deg
            rhs(i+1) = -1* sum( u_reuse.* ...
                    (JacobiPolyDerivative(i,r,0,0)).*w) ...
                    + (u_reuse(end)*JacobiPoly(i,+1,0,0) ...
                        - (u_reuse(1)*JacobiPoly(i,-1,0,0)));
        end
        
        u_D_hat_DG0(:,e) = (M*h_e(e)/2)\rhs;
    end
    
    u_D_er_DG0_Act(Deg) = sum(sum(abs((Vld_viz*u_D_hat_DG0-cos(x_viz))/max(size(x_viz(:))))));
    
    %DG derivative second attempt Left 
    %   \[(v,\Phi(x)) = - ( \Sigma\hat{u}\Phi_i(x), \frac{d\Phi(x)}{dx} ) +
    %       ( u(x_{j+1}^-)\Phi(x_{j+1})  - u(x_{j}^-)\Phi(x_{j}))\]
    rhs = zeros(Deg+1,1);
    u_D_hat_DG1 = zeros(Deg+1,Ne);
    u_nodes =  Vld*u_hat;
    for e =1:Ne
        for i=0:Deg
            if (e ~= 1 && e ~= Ne)
                rhs(i+1) = -1* sum( u_nodes(:,e).* ...
                    (JacobiPolyDerivative(i,r,0,0)).*w) ...
                    + (u_nodes(1,e+1)*JacobiPoly(i,+1,0,0) ...
                        - (u_nodes(1,e)*JacobiPoly(i,-1,0,0)));
            elseif (e ==1)
                rhs(i+1) = -1* sum( u_nodes(:,e).* ...
                    (JacobiPolyDerivative(i,r,0,0)).*w) ...
                    + (u_nodes(1,e+1)*JacobiPoly(i,+1,0,0) ...
                        - (u_nodes(1,e)*JacobiPoly(i,-1,0,0)));
            elseif (e == Ne)
                rhs(i+1) = -1* sum( u_nodes(:,e).* ...
                    (JacobiPolyDerivative(i,r,0,0)).*w) ...
                    + (u_nodes(end,e)*JacobiPoly(i,+1,0,0) ...
                        - (u_nodes(1,e)*JacobiPoly(i,-1,0,0)));
            end
        end
        
        u_D_hat_DG1(:,e) = (M*h_e(e)/2)\rhs;
    end
    
    u_D_er_DG1_Act(Deg) = sum(sum(abs((Vld_viz*u_D_hat_DG1-cos(x_viz))/max(size(x_viz(:))))));
    
    %DG derivative Third way.
    %   \[(v,\Phi(x)) = - ( \Sigma\hat{u}\Phi_i(x), \frac{d\Phi(x)}{dx} ) +
    %       ( u(x_{j+1}^+)\Phi(x_{j+1})  - u(x_{j}^+)\Phi(x_{j}))\]
    rhs = zeros(Deg+1,1);
    u_D_hat_DG2 = zeros(Deg+1,Ne);
    u_nodes =  Vld*u_hat;
    for e =1:Ne
        for i=0:Deg
            if (e ~= 1 && e ~= Ne)
                rhs(i+1) = -1* sum( u_nodes(:,e).* ...
                    (JacobiPolyDerivative(i,r,0,0)).*w) ...
                    + (u_nodes(end,e)*JacobiPoly(i,+1,0,0) ...
                        - (u_nodes(end,e-1)*JacobiPoly(i,-1,0,0)));
            elseif (e ==1)
                rhs(i+1) = -1* sum( u_nodes(:,e).* ...
                    (JacobiPolyDerivative(i,r,0,0)).*w) ...
                    + (u_nodes(end,e)*JacobiPoly(i,+1,0,0) ...
                        - (u_nodes(1,e)*JacobiPoly(i,-1,0,0)));
            elseif (e == Ne)
                rhs(i+1) = -1* sum( u_nodes(:,e).* ...
                    (JacobiPolyDerivative(i,r,0,0)).*w) ...
                    + (u_nodes(end,e)*JacobiPoly(i,+1,0,0) ...
                        - (u_nodes(end,e-1)*JacobiPoly(i,-1,0,0)));
            end
        end    
        u_D_hat_DG2(:,e) = (M*h_e(e)/2)\rhs;
    end
    u_D_er_DG2_Act(Deg) = sum(sum(abs((Vld_viz*u_D_hat_DG2-cos(x_viz))/max(size(x_viz(:))))));
    
    %DG derivative Third way.
    %   \[(v,\Phi(x)) = - ( \Sigma\hat{u}\Phi_i(x), \frac{d\Phi(x)}{dx} ) +
    %       ( u(x_{j+1}^+)\Phi(x_{j+1})  - u(x_{j}^+)\Phi(x_{j}))\]
    rhs = zeros(Deg+1,1);
    u_D_hat_DG3 = zeros(Deg+1,Ne);
    u_nodes =  Vld*u_hat;
    for e =1:Ne
        for i=0:Deg
            if (e ~= 1 && e ~= Ne)
                rhs(i+1) = -1* sum( u_nodes(:,e).* ...
                    (JacobiPolyDerivative(i,r,0,0)).*w) ...
                    + ((u_nodes(end,e)/2 + u_nodes(1,e+1)/2)*JacobiPoly(i,+1,0,0) ...
                        - ((u_nodes(end,e-1)/2+u_nodes(end,e-1)/2 )*JacobiPoly(i,-1,0,0)));
            elseif (e ==1)
                rhs(i+1) = -1* sum( u_nodes(:,e).* ...
                    (JacobiPolyDerivative(i,r,0,0)).*w) ...
                    + (u_nodes(end,e)*JacobiPoly(i,+1,0,0) ...
                        - (u_nodes(1,e)*JacobiPoly(i,-1,0,0)));
            elseif (e == Ne)
                rhs(i+1) = -1* sum( u_nodes(:,e).* ...
                    (JacobiPolyDerivative(i,r,0,0)).*w) ...
                    + (u_nodes(end,e)*JacobiPoly(i,+1,0,0) ...
                        - (u_nodes(end,e-1)*JacobiPoly(i,-1,0,0)));
            end
        end    
        u_D_hat_DG3(:,e) = (M*h_e(e)/2)\rhs;
    end
    u_D_er_DG3_Act(Deg) = sum(sum(abs((Vld_viz*u_D_hat_DG3-cos(x_viz))/max(size(x_viz(:))))));
    
%     figure(2),
%     plot(x_viz,u_simDer_viz,'r'); hold on;
%     plot(x_viz,Vld_viz*u_D_hat_DG1,'b'); hold on;
%     plot(x_viz,Vld_viz*u_D_hat_DG2),'c'; hold on;
%     
%     figure(3),
%     plot(x_viz,cos(x_viz)- u_simDer_viz,'r');
% 
%     figure(4),
%     plot(x_viz,cos(x_viz)- Vld_viz*u_D_hat_DG1,'r');
% 
%     figure(5),
%     plot(x_viz,cos(x_viz)- Vld_viz*u_D_hat_DG2,'r');
% plot(Degrees, (u_er_P_Act(Degrees)),'-r*'); hold on;
% plot(Degrees, (u_D_er_L_Act(Degrees)),'-g*'); hold on;
% plot(Degrees, (u_D_er_L_Act(Degrees)),'-g*'); hold on;
    figure(1)
    %plot(x_viz, (Vld_viz*u_hat-sin(x_viz)),'r'); hold on;
    figure(2),
    plot(x_viz, (u_simDer_viz- cos(x_viz)),'r'); hold on;
title('Element wise Local derivative error');
xlabel('0<x<10pi')
ylabel('(Er))')
    figure(3),
    plot(x_viz, (Vld_viz*u_D_hat_DG0-cos(x_viz)),'m'); hold on;
    title('Element wise Local derivative error');
xlabel('0<x<10pi')
ylabel('(Er))')
    figure(4),
    plot(x_viz, (Vld_viz*u_D_hat_DG1-cos(x_viz)),'c'); hold on;
    title('Element wise Local derivative DG1(Left) error');
xlabel('0<x<10pi')
ylabel('(Er))')
    figure(5),
    plot(x_viz, (Vld_viz*u_D_hat_DG2-cos(x_viz)),'b'); hold on;
    title('Element wise Local derivative DG2(Right) error');
xlabel('0<x<10pi')
ylabel('(Er))')
    figure(6),
    plot(x_viz, (Vld_viz*u_D_hat_DG3-cos(x_viz)),'k'); hold on;
    title('Element wise Local derivative DG3(Average) error');
xlabel('0<x<10pi')
ylabel('(Er))')


end

%%

% figure(1)
% plot(Degrees, log10(u_er_P_Act(Degrees)),'-r*'); hold on;
% plot(Degrees, log10(u_D_er_L_Act(Degrees)),'-ro'); hold on;
% plot(Degrees, log10(u_D_er_DG0_Act(Degrees)),'-mo'); hold on;
% plot(Degrees, log10(u_D_er_DG1_Act(Degrees)),'-co'); hold on;
% plot(Degrees, log10(u_D_er_DG2_Act(Degrees)),'-bo'); hold on;
% plot(Degrees, log10(u_D_er_DG3_Act(Degrees)),'-ko'); hold on;
% legend('y = Log_{10}(u(Projected)-u)',...
%             'y = Log_{10}(u_x(Local)-u)',...
%             'y = Log_{10}(u_x(Local DG)-u_x)',...
%             'y = Log_{10}(u_x(non-Local DG(left) )-u_x)',...
%             'y = Log_{10}(u_x(non-Local DG(right) )-u_x)',...
%             'y = Log_{10}(u_x(non-Local DG(Avg) )-u_x)')
% title('Log_{10}(Error_{Linf}) for u = sin(x); 0<x<10PI; No Ele = 15');
% xlabel('(d)Degree of projection for each element')
% ylabel('Log_{10}((Er)_{Linf})')

% figure(2)
% plot(Degrees, (u_er_P_Act(Degrees)),'-r*'); hold on;
% plot(Degrees, (u_D_er_L_Act(Degrees)),'-g*'); hold on;
% plot(Degrees, (u_D_er_DG0_Act(Degrees)),'.g*'); hold on;
% plot(Degrees, (u_D_er_DG1_Act(Degrees)),'-g*'); hold on;
% plot(Degrees, (u_D_er_DG2_Act(Degrees)),'-g*'); hold on;
% plot(Degrees, (u_D_er_DG3_Act(Degrees)),'-g*'); hold on;
