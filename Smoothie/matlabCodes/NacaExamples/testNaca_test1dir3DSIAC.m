% Read data from testNaca_test1dir3DSIAC
% s is used in variables but it ss data.

clear;
close all;


UsebkupData = 'cur_feb19/'; % has only data Ux_ss so remember to comment below also.
%UsebkupDataC = 'cur_feb12/';

%UsebkupData = 'cur_feb10/'

%UsebkupData = ''
%alpha =1;
SIACFilterPoly = 2;
scaling = '0.05';
filename = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_test1dir3DSIAC' );
%filenameC = strcat('P0000009_SO_', int2str(SIACFilterPoly),'_Sca_' , scaling,'_ACRFDBVar' );

filepath = strcat('data/',UsebkupData,filename);
%filepathC = strcat('data/',UsebkupDataC,filenameC);

xfname =  strcat(filepath,'_pX.txt' );
yfname =  strcat(filepath,'_pY.txt' );
zfname =  strcat(filepath,'_pZ.txt' );
efname =  strcat(filepath,'_pE.txt' );

upfname = strcat(filepath,'_pU_p.txt' ); % raw data projection.
usxfname = strcat(filepath,'_pU_sx.txt' ); % this variable is u with LSIAC applied in X-direction.
usyfname = strcat(filepath,'_pU_sy.txt' ); % this variable is u with LSIAC applied in X-direction.
uszfname = strcat(filepath,'_pU_sz.txt' ); % this variable is u with LSIAC applied in X-direction.

vpfname = strcat(filepath,'_pV_p.txt' ); % raw data projection.
vsxfname = strcat(filepath,'_pV_sx.txt' ); % this variable is u with LSIAC applied in X-direction.
vsyfname = strcat(filepath,'_pV_sy.txt' ); % this variable is u with LSIAC applied in X-direction.
vszfname = strcat(filepath,'_pV_sz.txt' ); % this variable is u with LSIAC applied in X-direction.

wpfname = strcat(filepath,'_pW_p.txt' ); % raw data projection.
wsxfname = strcat(filepath,'_pW_sx.txt' ); % this variable is u with LSIAC applied in X-direction.
wsyfname = strcat(filepath,'_pW_sy.txt' ); % this variable is u with LSIAC applied in X-direction.
wszfname = strcat(filepath,'_pW_sz.txt' ); % this variable is u with LSIAC applied in X-direction.


xpos = textread(xfname, '%f\t');
ypos = textread(yfname, '%f\t');
zpos = textread(zfname, '%f\t');
epos = textread(efname, '%f\t');

uppos = textread(upfname, '%f\t');
usxpos = textread(usxfname, '%f\t');
usypos = textread(usyfname, '%f\t');
uszpos = textread(uszfname, '%f\t');

vppos = textread(vpfname, '%f\t');
vsxpos = textread(vsxfname, '%f\t');
vsypos = textread(vsyfname, '%f\t');
vszpos = textread(vszfname, '%f\t');

wppos = textread(wpfname, '%f\t');
wsxpos = textread(wsxfname, '%f\t');
wsypos = textread(wsyfname, '%f\t');
wszpos = textread(wszfname, '%f\t');


epos = floor(epos +0.0001);
s = floor( (max(size(xpos)))^(1/2) +0.1);

xVpos = reshape(xpos,s,s);
yVpos = reshape(ypos,s,s);
zVpos = reshape(zpos,s,s);

upVpos = reshape(uppos,s,s);  
usxVpos = reshape(usxpos,s,s);  
usyVpos = reshape(usypos,s,s);  
uszVpos = reshape(uszpos,s,s);  

vpVpos = reshape(vppos,s,s);  
vsxVpos = reshape(vsxpos,s,s);  
vsyVpos = reshape(vsypos,s,s);  
vszVpos = reshape(vszpos,s,s);  

wpVpos = reshape(wppos,s,s);  
wsxVpos = reshape(wsxpos,s,s);  
wsyVpos = reshape(wsypos,s,s);  
wszVpos = reshape(wszpos,s,s);  

figure(1),
subplot(2,2,1), surf(yVpos,zVpos,usxVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir xLSIAC');
subplot(2,2,2), surf(yVpos,zVpos,upVpos); hold on; colorbar(); shading interp; view(0,90);
title('raw data');
subplot(2,2,3), surf(yVpos,zVpos,usyVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir yLSIAC');
subplot(2,2,4), surf(yVpos,zVpos,uszVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir zLSIAC');


figure(2),
subplot(2,2,1), surf(yVpos,zVpos,vsxVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir xLSIAC');
subplot(2,2,2), surf(yVpos,zVpos,vpVpos); hold on; colorbar(); shading interp; view(0,90);
title('raw data');
subplot(2,2,3), surf(yVpos,zVpos,vsyVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir yLSIAC');
subplot(2,2,4), surf(yVpos,zVpos,vszVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir zLSIAC');

figure(3),
subplot(2,2,1), surf(yVpos,zVpos,wsxVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir xLSIAC');
subplot(2,2,2), surf(yVpos,zVpos,wpVpos); hold on; colorbar(); shading interp; view(0,90);
title('raw data');
subplot(2,2,3), surf(yVpos,zVpos,wsyVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir yLSIAC');
subplot(2,2,4), surf(yVpos,zVpos,wszVpos); hold on; colorbar(); shading interp; view(0,90);
title('1dir zLSIAC');


figure(4),
subplot(2,2,1), contour(yVpos,zVpos,usxVpos);
title('1dir xLSIAC');
subplot(2,2,2), contour(yVpos,zVpos,upVpos); 
title('raw data');
subplot(2,2,3), contour(yVpos,zVpos,usyVpos); 
title('1dir yLSIAC');
subplot(2,2,4), contour(yVpos,zVpos,uszVpos); 
title('1dir zLSIAC');


figure(5),
subplot(2,2,1), contour(yVpos,zVpos,vsxVpos); 
title('1dir xLSIAC');
subplot(2,2,2), contour(yVpos,zVpos,vpVpos); 
title('raw data');
subplot(2,2,3), contour(yVpos,zVpos,vsyVpos); 
title('1dir yLSIAC');
subplot(2,2,4), contour(yVpos,zVpos,vszVpos); 
title('1dir zLSIAC');

figure(6),
subplot(2,2,1), contour(yVpos,zVpos,wsxVpos); 
title('1dir xLSIAC');
subplot(2,2,2), contour(yVpos,zVpos,wpVpos); 
title('raw data');
subplot(2,2,3), contour(yVpos,zVpos,wsyVpos);
title('1dir yLSIAC');
subplot(2,2,4), contour(yVpos,zVpos,wszVpos);
title('1dir zLSIAC');


