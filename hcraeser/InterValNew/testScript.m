%%%Experiment 1
% test script to test.
% %clear;
% x = (0:.1:3);
% y = x.^3 - 5*x.^2+8*x-4;
% y2 = zeros(size(x));
% plot(x,y); hold on;
% plot(x,y2,'r');


% start with given X [0,3]

%Evaluate F[X] DF[X].

%if F[X] does not contain zero. then return.

%if F[X] contains a zero and DF[X] does not contain zero
   % X1 = N(X) intersect X
   % If X1 is very small interval. print into solution.
   % Solve (X1);

%if F[X] contains a zero and DF[X] does not contain a zero.
   % X_1 = first half of X
   % X_2 = second half of X
   % Solve (X1);
   % Solve (X2);
 %X1 =IntNew1D(@funArb,@DfunArg,X)
 %%% END of Experiment 1.
 
 %%%Experiment 2
 coeff = [6,-1,1,1];
 MBasis(1,coeff,[-0.2,0.2],1)
 MSerialBasis(1,coeff,[-0.2,0.2])
% MBasis(2,coeff,[-0.2,0.2])
 
 %%% End of Experiment 2
 