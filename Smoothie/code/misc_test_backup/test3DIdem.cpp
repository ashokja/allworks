#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC3D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh3D.h"

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>

using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg polynomial degree filter you want to apply" << endl;
		cout << "3rd arg meshscaling you want to use." << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh3D* HNM3D = new HandleNekMesh3D(vSession);
	vector<string> var = vSession->GetVariables();
	HNM3D->LoadMesh(var[0]);
	//HNM3D->LoadMesh(var[1]);
	//HNM3D->LoadMesh(var[2]);
	string fname = vSession->GetSessionName();
	
HNM3D->LoadExpListIntoRTree();
	
	int tNquadPts = HNM3D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM3D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM3D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, ece1, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	ece = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);
//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);

	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*M_PI*( xc0[i]+xc1[i]+ xc2[i] ));
		//fce[i] = std::cos(2.0*M_PI*(xc0[i]))*std::cos(2.0*M_PI*(xc1[i]));
	}
	
	HNM3D->m_expansions[0]->FwdTrans(fce,HNM3D->m_expansions[0]->UpdateCoeffs() );
	HNM3D->m_expansions[0]->BwdTrans( HNM3D->m_expansions[0]->GetCoeffs(),
										HNM3D->m_expansions[0]->UpdatePhys());
	ece1 = HNM3D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM3D->m_expansions[0]->GetCoeffs();

HNM3D->m_Arrays.push_back(ece1);

	for( int i =0; i < tNquadPts; i++)
	{
		ece[i] = ece1[i];
	}

	NekDouble valY, valZ;
	SmoothieSIAC3D sm(SIACUtilities::eSYM_2kp1_1SIDED_4kp1, HNM3D, atoi(argv[2]), atof(argv[3]) );
	Array<OneD,NekDouble> direction(3,0.0), directionY(3,0.0),directionZ(3,0.0);
	Array<OneD,NekDouble> direction1(3,0.0), direction2(3,0.0),direction3(3,0.0);
	Array<OneD,NekDouble> direction4(3,0.0), direction5(3,0.0),direction6(3,0.0);
	direction1[0] = 1.0; direction1[1] = 0.0; // 0 degrees
	direction2[0] = 0.5 ; direction2[1] = 0.8660254038; // 30 degrees
	direction3[0] = 0.8660254038; direction3[1] = 0.5; // 60 degrees
	direction4[0] = 0.0; direction4[1] = 1.0; directionZ[2] = 1.0;
	//direction5[0] = 1.0; direction5[1] = 0.0;
	//direction6[0] = 1.0; direction6[1] = 0.0;
	//direction[0] = 1.0; directionY[1] = 1.0;

	NekDouble projL2Er = HNM3D->m_expansions[0]->L2( fce, ece);
	NekDouble projLinfEr = HNM3D->m_expansions[0]->Linf( fce, ece);
	
	cout << "projEr L2: " << projL2Er << "\t"<< "projEr : " << projLinfEr << endl; 

	int loop =0;
	for (int i =0 ; i < tNquadPts; i++)
	{
		sm.EvaluateAt( xc0[i], xc1[i], xc2[i], sce[i], valY, valZ, direction1, atof(argv[3]), 0);
	}
	NekDouble smtL2Er = HNM3D->m_expansions[0]->L2(fce, sce);
	NekDouble smtLinfEr = HNM3D->m_expansions[0]->Linf(fce, sce);
	HNM3D->m_expansions[0]->SetPhys(sce);
	cout << "loop : " << loop+1<< endl;	
	cout << "smtEr  L2: " << smtL2Er  << "\t"<< "SmtEr  : " << smtLinfEr  << endl;

	NekDouble safetyGap = atof(argv[3])*atof(argv[2]);
	cout << safetyGap << endl;

	NekDouble err, errl2;
	NekDouble err_e, errl2_e;
	NekDouble minX, minY, maxX,maxY, minZ, maxZ;
	int el_totPhys = HNM3D->m_expansions[0]->GetExp(0)->GetTotPoints(), count_EL;
	Array<OneD,NekDouble> el_xc0(el_totPhys), el_xc1(el_totPhys), el_xc2(el_totPhys);

/*******************0 deg *****************************/
	err =0.0; count_EL =0; err_e =0.0;
	for( int e =0; e < HNM3D->m_expansions[0]->GetExpSize() ; e++)
	{
		// check if inside the boundary calculate else continue.
		HNM3D->m_expansions[0]->GetExp(e)->GetCoords(el_xc0, el_xc1, el_xc2);
		minX = Vmath::Vmin(el_totPhys, el_xc0,1);	minY = Vmath::Vmin(el_totPhys, el_xc1,1),	minZ = Vmath::Vmin(el_totPhys, el_xc2,1);
		maxX = Vmath::Vmax(el_totPhys, el_xc0,1);	maxY = Vmath::Vmax(el_totPhys, el_xc1,1);	maxZ = Vmath::Vmax(el_totPhys, el_xc2,1);
		if (minX > safetyGap && minY > safetyGap && minZ > safetyGap 
			&& maxX < 1.0-safetyGap && maxY < 1.0-safetyGap && maxZ < 1.0-safetyGap)
		{
			int phySOffset = HNM3D->m_expansions[0]->GetPhys_Offset(e);
			Array<OneD,NekDouble> fce_el = fce.CreateWithOffset(fce, phySOffset);
			Array<OneD,NekDouble> ece_el = ece.CreateWithOffset(ece, phySOffset);
			Array<OneD,NekDouble> sce_el = sce.CreateWithOffset(sce, phySOffset);
			errl2 = HNM3D->m_expansions[0]->GetExp(e)->L2(fce_el, sce_el);
			errl2_e = HNM3D->m_expansions[0]->GetExp(e)->L2(fce_el, ece_el);
			err += errl2*errl2;
			err_e += errl2_e*errl2_e;
			count_EL++;
		}
	}
	cout << count_EL << endl;
	cout << "Proj L2 " << sqrt(err_e)/((NekDouble)count_EL*atof(argv[3])*atof(argv[3])*atof(argv[3])) << "\t";
	cout << "LSIAC 0deg L2 " << sqrt(err)/((NekDouble)count_EL*atof(argv[3])*atof(argv[3])*atof(argv[3]) ) << endl;
	
	HNM3D->m_expansions[0]->SetPhys(sce);

/******************* 30 deg *****************************/
/*
	for (int i =0 ; i < tNquadPts; i++)
	{
		if ( xc0[i] > safetyGap  && xc1[i] >safetyGap  && 
			xc0[i] < 1.0-safetyGap && xc1[i] < 1.0- safetyGap )
		{ 
			sm.EvaluateAt( xc0[i], xc1[i], xc2[i], sce[i], valY, valZ, direction2, atof(argv[3]), 0);
		}
		else
		{
//			sm.EvaluateAt( xc0[i], xc1[i], xc2[i], sce[i], valY, valZ, direction1, atof(argv[3]), 0);
		}
	}

	err =0.0; count_EL =0; err_e =0.0;
	for( int e =0; e < HNM3D->m_expansions[0]->GetExpSize() ; e++)
	{
		// check if inside the boundary calculate else continue.
		HNM3D->m_expansions[0]->GetExp(e)->GetCoords(el_xc0, el_xc1, el_xc2);
		minX = Vmath::Vmin(el_totPhys, el_xc0,1);	minY = Vmath::Vmin(el_totPhys, el_xc1,1);
		maxX = Vmath::Vmax(el_totPhys, el_xc0,1);	maxY = Vmath::Vmax(el_totPhys, el_xc1,1);
		if (minX > safetyGap && minY > safetyGap 
			&& maxX < 1.0-safetyGap && maxY < 1.0-safetyGap)
		{
			int phySOffset = HNM3D->m_expansions[0]->GetPhys_Offset(e);
			Array<OneD,NekDouble> fce_el = fce.CreateWithOffset(fce, phySOffset);
			Array<OneD,NekDouble> ece_el = ece.CreateWithOffset(ece, phySOffset);
			Array<OneD,NekDouble> sce_el = sce.CreateWithOffset(sce, phySOffset);
			errl2 = HNM3D->m_expansions[0]->GetExp(e)->L2(fce_el, sce_el);
			errl2_e = HNM3D->m_expansions[0]->GetExp(e)->L2(fce_el, ece_el);
			err += errl2*errl2;
			err_e += errl2_e*errl2_e;
			count_EL++;
		}
	}
	cout << count_EL << endl;
	cout << "Proj L2 " << sqrt(err_e)/((NekDouble)count_EL*atof(argv[3])*atof(argv[3])) << "\t";
	cout << "LSIAC 0deg L2 " << sqrt(err)/((NekDouble)count_EL*atof(argv[3])*atof(argv[3])) << endl;
	
	HNM3D->m_expansions[0]->SetPhys(sce);
*/
/******************* 60 deg *****************************/
/*
	smtL2Er = HNM3D->m_expansions[0]->L2(fce, sce);
	smtLinfEr = HNM3D->m_expansions[0]->Linf(fce, sce);
	HNM3D->m_expansions[0]->SetPhys(sce);
	cout << "loop : " << loop+1<< endl;	
	cout << "smtEr  L2: " << smtL2Er  << "\t"<< "SmtEr  : " << smtLinfEr  << endl; 
*/
/*
	for (int i =0 ; i < tNquadPts; i++)
	{
		if ( xc0[i] > safetyGap  && xc1[i] >safetyGap  && 
			xc0[i] < 1.0-safetyGap && xc1[i] < 1.0- safetyGap )
		{ 
			sm.EvaluateAt( xc0[i], xc1[i], xc2[i], sce[i], valY, valZ, direction3, atof(argv[3]), 0);
		}
		else
		{
//			sm.EvaluateAt( xc0[i], xc1[i], xc2[i], sce[i], valY, valZ, direction1, atof(argv[3]), 0);
		}
	}
	err =0.0; count_EL =0; err_e =0.0;
	for( int e =0; e < HNM3D->m_expansions[0]->GetExpSize() ; e++)
	{
		// check if inside the boundary calculate else continue.
		HNM3D->m_expansions[0]->GetExp(e)->GetCoords(el_xc0, el_xc1, el_xc2);
		minX = Vmath::Vmin(el_totPhys, el_xc0,1);	minY = Vmath::Vmin(el_totPhys, el_xc1,1);
		maxX = Vmath::Vmax(el_totPhys, el_xc0,1);	maxY = Vmath::Vmax(el_totPhys, el_xc1,1);
		if (minX > safetyGap && minY > safetyGap 
			&& maxX < 1.0-safetyGap && maxY < 1.0-safetyGap)
		{
			int phySOffset = HNM3D->m_expansions[0]->GetPhys_Offset(e);
			Array<OneD,NekDouble> fce_el = fce.CreateWithOffset(fce, phySOffset);
			Array<OneD,NekDouble> ece_el = ece.CreateWithOffset(ece, phySOffset);
			Array<OneD,NekDouble> sce_el = sce.CreateWithOffset(sce, phySOffset);
			errl2 = HNM3D->m_expansions[0]->GetExp(e)->L2(fce_el, sce_el);
			errl2_e = HNM3D->m_expansions[0]->GetExp(e)->L2(fce_el, ece_el);
			err += errl2*errl2;
			err_e += errl2_e*errl2_e;
			count_EL++;
		}
	}
	cout << count_EL << endl;
	cout << "Proj L2 " << sqrt(err_e)/((NekDouble)count_EL*atof(argv[3])*atof(argv[3])) << "\t";
	cout << "LSIAC deg L2 " << sqrt(err)/((NekDouble)count_EL*atof(argv[3])*atof(argv[3])) << endl;
	HNM3D->m_expansions[0]->SetPhys(sce);
*/

/*
	smtL2Er = HNM3D->m_expansions[0]->L2(fce, sce);
	smtLinfEr = HNM3D->m_expansions[0]->Linf(fce, sce);
	cout << "loop : " << loop+1<< endl;	
	cout << "smtEr  L2: " << smtL2Er  << "\t"<< "SmtEr  : " << smtLinfEr  << endl; 
*/

/******************* 90 deg *****************************/
	
	for (int i =0 ; i < tNquadPts; i++)
	{
			sm.EvaluateAt( xc0[i], xc1[i], xc2[i], sce[i], valY, valZ, direction4, atof(argv[3]), 0);
/*
		if ( xc0[i] > safetyGap  && xc1[i] >safetyGap  && xc2[i] > safetyGap &&
			xc0[i] < 1.0-safetyGap && xc1[i] < 1.0- safetyGap && xc2[i] < 1.0-safetyGap )
		{ 
			sm.EvaluateAt( xc0[i], xc1[i], xc2[i], sce[i], valY, valZ, direction4, atof(argv[3]), 0);
		}
		else
		{
//			sm.EvaluateAt( xc0[i], xc1[i], xc2[i], sce[i], valY, valZ, direction1, atof(argv[3]), 0);
		}
*/
	}
	err =0.0; count_EL =0; err_e = 0.0;
	for( int e =0; e < HNM3D->m_expansions[0]->GetExpSize() ; e++)
	{
		// check if inside the boundary calculate else continue.
		HNM3D->m_expansions[0]->GetExp(e)->GetCoords(el_xc0, el_xc1, el_xc2);
		minX = Vmath::Vmin(el_totPhys, el_xc0,1);	minY = Vmath::Vmin(el_totPhys, el_xc1,1),	minZ = Vmath::Vmin(el_totPhys, el_xc2,1);
		maxX = Vmath::Vmax(el_totPhys, el_xc0,1);	maxY = Vmath::Vmax(el_totPhys, el_xc1,1);	maxZ = Vmath::Vmax(el_totPhys, el_xc2,1);
		if (minX > safetyGap && minY > safetyGap && minZ > safetyGap 
			&& maxX < 1.0-safetyGap && maxY < 1.0-safetyGap && maxZ < 1.0-safetyGap)
		{
			int phySOffset = HNM3D->m_expansions[0]->GetPhys_Offset(e);
			Array<OneD,NekDouble> fce_el = fce.CreateWithOffset(fce, phySOffset);
			Array<OneD,NekDouble> ece_el = ece.CreateWithOffset(ece, phySOffset);
			Array<OneD,NekDouble> sce_el = sce.CreateWithOffset(sce, phySOffset);
			errl2 = HNM3D->m_expansions[0]->GetExp(e)->L2(fce_el, sce_el);
			errl2_e = HNM3D->m_expansions[0]->GetExp(e)->L2(fce_el, ece_el);
			err += errl2*errl2;
			err_e += errl2_e*errl2_e;
			count_EL++;
		}
	}
	HNM3D->m_expansions[0]->SetPhys(sce);

	cout << count_EL << endl;
	cout << "Proj L2 " << sqrt(err_e)/((NekDouble)count_EL*atof(argv[3])*atof(argv[3])*atof(argv[3])) << "\t";
	cout << "LSIAC 0deg L2 " << sqrt(err)/((NekDouble)count_EL*atof(argv[3])*atof(argv[3])*atof(argv[3]) ) << endl;
/******************* Z deg *****************************/
	
	for (int i =0 ; i < tNquadPts; i++)
	{
		sm.EvaluateAt( xc0[i], xc1[i], xc2[i], sce[i], valY, valZ, directionZ, atof(argv[3]), 0);
/*
		if ( xc0[i] > safetyGap  && xc1[i] >safetyGap  && xc2[i] > safetyGap &&
			xc0[i] < 1.0-safetyGap && xc1[i] < 1.0- safetyGap && xc2[i] < 1.0-safetyGap )
		{ 
			sm.EvaluateAt( xc0[i], xc1[i], xc2[i], sce[i], valY, valZ, directionZ, atof(argv[3]), 0);
		}
		else
		{
//			sm.EvaluateAt( xc0[i], xc1[i], xc2[i], sce[i], valY, valZ, direction1, atof(argv[3]), 0);
		}
*/
	}
	err =0.0; count_EL =0; err_e = 0.0;
	for( int e =0; e < HNM3D->m_expansions[0]->GetExpSize() ; e++)
	{
		// check if inside the boundary calculate else continue.
		HNM3D->m_expansions[0]->GetExp(e)->GetCoords(el_xc0, el_xc1, el_xc2);
		minX = Vmath::Vmin(el_totPhys, el_xc0,1);	minY = Vmath::Vmin(el_totPhys, el_xc1,1),	minZ = Vmath::Vmin(el_totPhys, el_xc2,1);
		maxX = Vmath::Vmax(el_totPhys, el_xc0,1);	maxY = Vmath::Vmax(el_totPhys, el_xc1,1);	maxZ = Vmath::Vmax(el_totPhys, el_xc2,1);
		if (minX > safetyGap && minY > safetyGap && minZ > safetyGap 
			&& maxX < 1.0-safetyGap && maxY < 1.0-safetyGap && maxZ < 1.0-safetyGap)
		{
			int phySOffset = HNM3D->m_expansions[0]->GetPhys_Offset(e);
			Array<OneD,NekDouble> fce_el = fce.CreateWithOffset(fce, phySOffset);
			Array<OneD,NekDouble> ece_el = ece.CreateWithOffset(ece, phySOffset);
			Array<OneD,NekDouble> sce_el = sce.CreateWithOffset(sce, phySOffset);
			errl2 = HNM3D->m_expansions[0]->GetExp(e)->L2(fce_el, sce_el);
			errl2_e = HNM3D->m_expansions[0]->GetExp(e)->L2(fce_el, ece_el);
			err += errl2*errl2;
			err_e += errl2_e*errl2_e;
			count_EL++;
		}
	}
	cout << count_EL << endl;
	cout << "Proj L2 " << sqrt(err_e)/((NekDouble)count_EL*atof(argv[3])*atof(argv[3])*atof(argv[3])) << "\t";
	cout << "LSIAC Z deg L2 " << sqrt(err)/((NekDouble)count_EL*atof(argv[3])*atof(argv[3])*atof(argv[3]) ) << endl;

/*
	smtL2Er = HNM3D->m_expansions[0]->L2(fce, sce);
	smtLinfEr = HNM3D->m_expansions[0]->Linf(fce, sce);
	cout << "loop : " << loop+1<< endl;	
	cout << "smtEr  L2: " << smtL2Er  << "\t"<< "SmtEr  : " << smtLinfEr  << endl; 

	// For L2 Error.
	//safetyGap = -1.0;
	NekDouble err, errl2;
	NekDouble err_e, errl2_e;
	int el_totPhys = HNM3D->m_expansions[0]->GetExp(0)->GetTotPoints(), count_EL;
	Array<OneD,NekDouble> el_xc0(el_totPhys), el_xc1(el_totPhys), el_xc2(el_totPhys);
	NekDouble minX, minY, maxX,maxY;

	err =0.0; count_EL =0;
	for( int e =0; e < HNM3D->m_expansions[0]->GetExpSize() ; e++)
	{
		// check if inside the boundary calculate else continue.
		HNM3D->m_expansions[0]->GetExp(e)->GetCoords(el_xc0, el_xc1, el_xc2);
		minX = Vmath::Vmin(el_totPhys, el_xc0,1);	minY = Vmath::Vmin(el_totPhys, el_xc1,1);
		maxX = Vmath::Vmax(el_totPhys, el_xc0,1);	maxY = Vmath::Vmax(el_totPhys, el_xc1,1);
		if (minX > safetyGap && minY > safetyGap 
			&& maxX < 1.0-safetyGap && maxY < 1.0-safetyGap)
		{
			int phySOffset = HNM3D->m_expansions[0]->GetPhys_Offset(e);
			Array<OneD,NekDouble> fce_el = fce.CreateWithOffset(fce, phySOffset);
			Array<OneD,NekDouble> ece_el = sce.CreateWithOffset(ece, phySOffset);
			Array<OneD,NekDouble> sce_el = sce.CreateWithOffset(sce, phySOffset);
			errl2 = HNM3D->m_expansions[0]->GetExp(e)->L2(fce_el, sce_el);
			errl2_e = HNM3D->m_expansions[0]->GetExp(e)->L2(fce_el, ece_el);
			err += errl2*errl2;
			err_e += errl2_e*errl2_e;
			count_EL++;
		}
	}
	cout << count_EL << endl;
	cout << sqrt(err) << endl;
	cout << "err Calcualted in L2 is " << sqrt(err)/((NekDouble)count_EL*atof(argv[3])*atof(argv[3])) << endl;
	cout << sqrt(err_e) << endl;
	cout << "err Calcualted in L2 is " << sqrt(err_e)/((NekDouble)count_EL*atof(argv[3])*atof(argv[3])) << endl;
*/

/*
	for (int loop =0 ; loop < 3 ;loop++)
	{
		for (int i =0 ; i < tNquadPts; i++)
		{
			sm.EvaluateAt( xc0[i], xc1[i], xc2[i], sce[i], valY, valZ, directionY, atof(argv[3]), 0);
		}
		NekDouble smtL2Er = HNM3D->m_expansions[0]->L2(fce, sce);
		NekDouble smtLinfEr = HNM3D->m_expansions[0]->Linf(fce, sce);
		HNM3D->m_expansions[0]->SetPhys(sce);
		cout << "loop : " << loop+1<< endl;	
		cout << "smtEr  L2: " << smtL2Er  << "\t"<< "SmtEr  : " << smtLinfEr  << endl; 
	}
	
	for (int loop =0 ; loop < 3 ;loop++)
	{
		for (int i =0 ; i < tNquadPts; i++)
		{
			sm.EvaluateAt( xc0[i], xc1[i], xc2[i], sce[i], valY, valZ, direction, atof(argv[3]), 0);
		}
		NekDouble smtL2Er = HNM3D->m_expansions[0]->L2(fce, sce);
		NekDouble smtLinfEr = HNM3D->m_expansions[0]->Linf(fce, sce);
		HNM3D->m_expansions[0]->SetPhys(sce);
		cout << "loop : " << loop+1<< endl;	
		cout << "smtEr  L2: " << smtL2Er  << "\t"<< "SmtEr  : " << smtLinfEr  << endl; 
	}

	NektarBaseClass k;
	k.writeNekArray(xc0,fname+ "_"+ argv[2]+"_xc0_3D_OneSided2kp1.txt");
	k.writeNekArray(xc1,fname+ "_"+ argv[2]+"_xc1_3D_OneSided2kp1.txt");
	k.writeNekArray(fce,fname+ "_"+ argv[2]+"_fce_3D_OneSided2kp1.txt");
	k.writeNekArray(ece,fname+ "_"+ argv[2]+"_ece_3D_OneSided2kp1.txt");
	k.writeNekArray(sce,fname+ "_"+ argv[2]+"_sce_3D_OneSided2kp1.txt");
*/

/*
	string out = vSession->GetSessionName() + ".fld";
    std::vector<LibUtilities::FieldDefinitionsSharedPtr> FieldDef(1);
    std::vector<std::vector<NekDouble> > FieldData(1);
    FieldDef[0] = (HNM3D->m_expansions[0]->GetFieldDefinitions()).at(0);
    FieldDef[0]->m_fields.push_back("u");
    FieldDef[0]->m_fields.push_back("v");
    FieldDef[0]->m_fields.push_back("w");
    HNM3D->m_expansions[0]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM3D->m_expansions[1]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM3D->m_expansions[2]->AppendFieldData(FieldDef[0], FieldData[0]);
    HNM3D->m_fld->Write(out, FieldDef, FieldData);
*/
	return 0;
}


void printNekArray(Array<OneD,NekDouble> &ar) 
{
	for (int i =0;i <ar.num_elements();i++)
	{
		cout << "el :"<<i<< " "<<ar[i]<<endl;
	}
}
void printNekArray(Array<OneD,NekDouble> &ar,int del) 
{
	cout << "size of array: " << ar.num_elements()<< endl;	
	for (int i =0;i <ar.num_elements();i++)
	{
		cout <<ar[i]<<"\t";
	}
	cout << endl;
}
