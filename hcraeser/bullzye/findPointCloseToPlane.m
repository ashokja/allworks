function [c_lpts,useful] = findPointCloseToPlane(index,val,pts)
    lpts_count = 1;
    [spts,~] = size(pts);

    for i = 1:spts
        if sum(abs(pts(i,index) - (val))) <0.1
            lpts(lpts_count,:) = pts(i,:);
            lpts_count = lpts_count +1;
        end
    end
    if lpts_count ==1
        useful = 0;
    else
        c_lpts = sum(lpts,1)/max(size(lpts));
        c_lpts(index) =val;
        useful = 1;
    end
end