% Need to read a file and output data.

close all;
clear;

%prefix = '../../data/NUTriMesh_proj/';
%rets =[{'05'},{'025'},{'0125'}];
%prefix = '../../data/NPUTriMesh_proj/';
%rets =[{'2'},{'5'},{'10'},{'50'},{'100'},{'500'},{'1000'}];
%rets =[{'2'},{'10'},{'100'}];
prefix = '../../data/UTriMesh_proj/';
rets =[{'05'},{'025'},{'0125'}];

for Poly = 2:4
%strt = strcat( '\hline','&','&','&','$\mathcal{P}$=',num2str(Poly),'&','&','&',' \\ \hline');
strt = strcat( '\hline','&','$\mathcal{P}$=',num2str(Poly),'&',' \\ \hline');
disp(strt);
for ret = rets    
	res = cell2mat(ret);
%    filename = strcat('log_Er_R2','_NM',int2str(Poly),'_','NUTriMesh_',res,'.log');
%    filename = strcat('log_Er_R2','_NM',int2str(Poly),'_','NPUniTriMesh05_d',res,'.log');
    filename = strcat('log_Er','_NM',int2str(Poly),'_','UniTriMesh_',res,'.log');

    logFileName = strcat(prefix,filename);
    fid = fopen(logFileName,'r'); %# open csv file for reading
    line = fgets(fid);
    %disp(line);
    line = fgets(fid);
    %disp(line);
    line = fgets(fid);
    %disp(line);
    % MuArray
    % DGL2
    % LSIACL2
    % DGLinf
    % LSIACLinf
    for i =1:4
        line = fgets(fid);
        line3 = fgets(fid);
        %disp(line3)
		if ( (line3(1) == 'm') && (line3(2) =='u') )
        A = sscanf(line3,'mu:\t%f');
        %disp(A);
        MuArray(i) = A;
		mu = A;
        line4 = fgets(fid);
        %disp(line4)
        A = textscan(line4,'%s %s %s %f %s %f %s %f');
        %A = sscanf(line3,'L2 error DG:%f SIAC %f SIACDyn %f');
    %    celldisp(A)
        DGL2 = num2str(cell2mat(A(4)),'%1.1e'); %DGL2
        SIACL2 = num2str(cell2mat(A(6)),'%1.1e'); %SIACL2
        SIACDynL2 = num2str(cell2mat(A(8)),'%1.1e'); %SIACDynL2
        LSIACDynL2(i) = cell2mat(A(8)); 
		

        line5 = fgets(fid);
    %    disp(line5)
        A = textscan(line5,'%s %s %s %f %s %f %s %f');
        %A = sscanf(line3,'L2 error DG:%f SIAC %f SIACDyn %f');
    %    celldisp(A)
        DGLinf = num2str(cell2mat(A(4)),'%1.1e');
        SIACLinf = num2str(cell2mat(A(6)),'%1.1e');
        SIACDynLinf = num2str(cell2mat(A(8)),'%1.1e');
        LSIACDynLinf(i) = cell2mat(A(8)); 
        
        line6 = fgets(fid);
        A = textscan(line6,'%s %s %s %f %s %f');
        SIACTiming = num2str(cell2mat(A(4)));
        SIACDynTiming = num2str(cell2mat(A(6)));
        speedup = num2str(cell2mat(A(4))./cell2mat(A(6)));

        %disp(line6);
        if ( abs(mu-1.0)< 0.01)
			strt = strcat( 'M',res,' & ',DGL2,' & ',DGLinf,' & ',SIACL2,' & ',SIACLinf,' & ',SIACDynL2,' & ',SIACDynLinf,' \\ \hline');
			%disp(strt);
            strt2 = strcat( 'M',res,' & ',DGL2,' & ',SIACL2,' & ',SIACDynL2,' & ',DGLinf,' & ',SIACLinf,' & ',SIACDynLinf,' \\ \hline');
            disp(strt2);
            strtTiming = strcat('M',res,' & ',SIACTiming, ' & ',SIACDynTiming,' & ' ,speedup,' \\ \hline');
            %disp(strtTiming);
			%disp(Poly);
			%disp('DG');
		end

        
        
		else
			%break;
            %disp('none');
            break;
        end
        
    end 
end


	% disp(LSIACDynL2);
%    figure(1)
%    plot(MuArray, log10(LSIACDynL2),'-*g'); hold on;
%    plot(MuArray, log10(LSIACDynLinf),'-or'); hold off;
    
%    figure(2),
%    plot(MuArray, log10(LSIACDynL2),'-*'); hold on;
    
    
 %   figure(3),
  %  plot(MuArray, log10(LSIACDynLinf),'-o'); hold on;
end