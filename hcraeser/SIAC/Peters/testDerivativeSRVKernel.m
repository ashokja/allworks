% test script to check derivative kernels.

close all;
clear all;
degree = 1;
alpha =1;
%for j = 1:degree
j = 3*degree;
    tknots = -(degree+1)/2 - (degree+j)/2:1: (degree+1)/2+(degree+j)/2;
    %shift =0;
    shift =-1*( (degree+1)/2+(degree+j)/2);
    %+ ((degree+1)/2+degree);



    tknots = tknots +shift;
    %get coeffecients.
    [M0,cbeforeDir] = bulidM_0Sym_NonStandarad(degree+alpha,tknots);
    disp(cbeforeDir);
    c = ApplyDerToCoeffs(cbeforeDir,degree+alpha,alpha, tknots);
    %c = cbeforeDir;
    disp(M0);
    disp(c);
    %x_list for solution to be evaluated.
    x_list = (min(min(tknots))):.01:max(max(tknots)) ;
    v_list = zeros(size(x_list));

    %points to calculate for central Bsplines.
    for loop = 1:max(size(c))
    %    delta = loop-degree-1;
        v_list = v_list+ c(loop)*CalBernUsingDividedDiff(degree,x_list,tknots(loop:loop+degree+1));
    end


    plot(x_list,v_list); hold on;
%end