#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"
#include <boost/math/special_functions/sinc.hpp>
#include <boost/timer.hpp>
#include <chrono>
#include "MetricTensor.h"
#include <math.h>

#define PI 3.14159265

typedef std::chrono::high_resolution_clock Clock;


using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>


using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 7)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2st arg conditions.xml file." << endl;
		cout << "3nd arg polynomial degree filter you want to apply" << endl;
		cout << "4th arg Resolution of output." << endl;
		cout << "5th arg Type of L-SIAC filter." << endl;
		cout << "6th arg m for scaling of LSIAC." << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh2D* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();
	HNM2D->LoadData(fname+".fld",var);
	MetricTensor* metricT = new MetricTensor();
	metricT->LoadMetricTensor(HNM2D);
	metricT->CalculateMetricTensorAtNodes();


	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;

	Array<OneD,NekDouble> fce = HNM2D->m_expansions[0]->GetPhys();
	
        // Define forcing function for first variable defined in file
	LibUtilities::EquationSharedPtr ffunc
					= vSession->GetFunction("ExactSolution", 0); 


	HNM2D->LoadExpListIntoRTree();
	HNM2D->CalculateDynamicScaling();

// Evaluate on a new equal space grid mesh.
	//int gPts = atoi(argv[1+3]);
	//int totPts = gPts*gPts;
	int gPtsLy = atoi(argv[1+3]);
	int totPts = gPtsLy;
	int Nx = gPtsLy, Ny=gPtsLy;
	//NekDouble sx = 0.5/(Nx-1.0), sy = 1.0/(Ny-1.0);
	NekDouble sx = 1.0/(Nx-1.0), sy = 1.0/(Ny-1.0);
	cout << "Line98" << endl;	
	NekDouble scaling = HNM2D->GetMeshLargestEdgeLength();
	SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1, HNM2D, atoi(argv[1+2]), scaling ); 
	SmoothieSIAC2D sm_NONSYM(SIACUtilities::eSYM_UNEVEN_2kp1, HNM2D, atoi(argv[1+2]), scaling ); 
	SmoothieSIAC2D sm_NONSYM_NSK2(SIACUtilities::eSYM_UNEVEN_2kp1, HNM2D, atoi(argv[1+2]), scaling ); 
	//SmoothieSIAC2D sm(SIACUtilities::eSYM_2kp1, HNM2D, atoi(argv[1+2]), scaling ); 

	cout << "came till here 1" <<endl;	
	int filterType = atoi(argv[1+4]);
	NekDouble mu = atof(argv[1+5])/10.0;
	cout << mu << endl;
	NekDouble valX,valY,ValZ;
	Array<OneD,NekDouble> direction(3,0.0);
	Array<OneD,NekDouble> coord(3,0.0);
	//direction[0] = cos(theta*PI/180);
	//direction[1] = sin(theta*PI/180);
	direction[0] = 1.0;
	direction[1] = 0.0; 
	Array<OneD,NekDouble> pXLy(totPts), pYLy(totPts), pVLy(totPts),pP(totPts), pS(totPts);
	Array<OneD,NekDouble> pLambLy(totPts), peig1_1Ly(totPts), peig1_2Ly(totPts), pNBspl(totPts);
	Array<OneD,NekDouble> glCoords(3,0.0),eig(3,0.0),eig2(3,0.0);
	NekDouble lambda,lambda2;
	NekDouble dynScaling;	
	NekDouble stX = 0.2;
	NekDouble endX = 0.8;
	//NekDouble stX = 0.4;
	//NekDouble endX = 0.6;
	cout << "came till here 2" <<endl;	
	for (int i =0 ; i< gPtsLy; i++)
	{
		int index = i;
		pXLy[index] = stX + i*sy*(endX-stX); 
		pYLy[index] = 0.5 +0.2 +0.25*cos(2*M_PI*pXLy[index]);
		// Actual value.
		pVLy[index] = ffunc->Evaluate(pXLy[index],pYLy[index],0.0,0.0);
		
		//dG Error 
		coord[0] = pXLy[index]; coord[1] = pYLy[index]; coord[2] = 0.0;
		int elid = HNM2D->GetExpansionIndexUsingRTree(coord);
		LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp(elid);
		int physOffset = HNM2D->m_expansions[0]->GetPhys_Offset(elid);
		const Array<OneD,NekDouble> el_phys = fce.CreateWithOffset(
										fce,physOffset);
		pP[index] = lexp->PhysEvaluate(coord,el_phys);
		//pELy[index] = elid;
		switch (filterType)
		{
			case 1:
				sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pS[index],
						valY,ValZ,direction, mu*scaling,0);
				break;
			case 2:
				dynScaling = HNM2D->GetDynamicScaling(coord);
				sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pS[index],
						valY,ValZ,direction, mu*dynScaling,0);
				break;
			case 3:
				metricT->GetEigenPair(coord, elid,1,lambda,eig);
				sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pS[index],
						valY,ValZ,eig, mu*lambda,0);
				break;
			case 4:
				metricT->GetEigenPair(coord, elid,2,lambda,eig);
				sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pS[index],
						valY,ValZ,eig, mu*lambda,0);
				break;
			case 5:
				metricT->GetEigenPairUsingIP(coord, elid,1,lambda,eig);
				sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pS[index],
						valY,ValZ,eig, mu*lambda,0);
				break;
			case 6:
				metricT->GetEigenPairUsingIP(coord, elid,2,lambda,eig);
				sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pS[index],
						valY,ValZ,eig, mu*lambda,0);
				break;
			case 7:
				metricT->GetEigenPairAtTheta(coord, elid,0,lambda,eig);
				sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pS[index],
						valY,ValZ,eig, mu*lambda,0);
				break;
			case 8:
				metricT->GetEigenPairAtTheta(coord, elid,90,lambda,eig);
				sm.EvaluateAt(pXLy[index],pYLy[index],0.0,pS[index],
						valY,ValZ,eig, mu*lambda,0);
				break;
			case 9:
				metricT->GetEigenPair(coord, elid,1,lambda,eig);
				sm_NONSYM_NSK2.EvaluateAt_NSK_GivenFilterLength_v1(
						pXLy[index],pYLy[index],0.0,pS[index],
						valY,ValZ,eig, mu*lambda,0);
				pLambLy[index] = lambda; peig1_1Ly[index]= eig[0];peig1_2Ly[index]=eig[2];			
				pNBspl[index] = ValZ;
				break;
			case 10:
				metricT->GetEigenPair(coord, elid,2,lambda,eig);
				sm_NONSYM_NSK2.EvaluateAt_NSK_GivenFilterLength_v1(
						pXLy[index],pYLy[index],0.0,pS[index],
						valY,ValZ,eig, mu*lambda,0);
				pLambLy[index] = lambda; peig1_1Ly[index]= eig[0];peig1_2Ly[index]=eig[2];			
				pNBspl[index] = ValZ;
				break;
			case 11:
				metricT->GetEigenPair(coord, elid,1,lambda,eig);
				sm_NONSYM.EvaluateAt_NSK_FixedNumBSpl(
						pXLy[index],pYLy[index],0.0,pS[index],
						valY,ValZ,eig, mu*lambda,0);
				break;
			case 12:
				metricT->GetEigenPair(coord, elid,2,lambda,eig);
				sm_NONSYM.EvaluateAt_NSK_FixedNumBSpl(
						pXLy[index],pYLy[index],0.0,pS[index],
						valY,ValZ,eig, mu*lambda,0);
				break;
			case 13:
				metricT->GetEigenPair(coord, elid,1,lambda,eig);
				sm_NONSYM_NSK2.EvaluateAt_NSK_GivenFilterLength_v2(
						pXLy[index],pYLy[index],0.0,pS[index],
						valY,ValZ,eig, mu*lambda,0);
				pLambLy[index] = lambda; peig1_1Ly[index]= eig[0];peig1_2Ly[index]=eig[2];			
				pNBspl[index] = ValZ;
				break;
			case 14:
				metricT->GetEigenPair(coord, elid,2,lambda,eig);
				sm_NONSYM_NSK2.EvaluateAt_NSK_GivenFilterLength_v2(
						pXLy[index],pYLy[index],0.0,pS[index],
						valY,ValZ,eig, mu*lambda,0);
				pLambLy[index] = lambda; peig1_1Ly[index]= eig[0];peig1_2Ly[index]=eig[2];			
				pNBspl[index] = ValZ;
				break;
		}
	}
	cout << "Done writing Line" << endl;

	NektarBaseClass k;
	k.writeNekArray(pXLy,fname+"_FLType_up_p2_"+argv[1+4]+"_mu_"+argv[1+5]+"_pXLy_2DDyn.txt");
	k.writeNekArray(pYLy,fname+"_FLType_up_p2_"+argv[1+4]+"_mu_"+argv[1+5]+"_pYLy_2DDyn.txt");
	k.writeNekArray(pVLy,fname+"_FLType_up_p2_"+argv[1+4]+"_mu_"+argv[1+5]+"_pVLy_2DDyn.txt");
	k.writeNekArray(pP,fname+"_FLType_up_p2_"+argv[1+4]+"_mu_"+argv[1+5]+"_pPLy_2DDyn.txt");
	k.writeNekArray(pS,fname+"_FLType_up_p2_"+argv[1+4]+"_mu_"+argv[1+5]+"_pSLy_2DDyn.txt");
	k.writeNekArray(pLambLy,fname+"_FLType_up_p2_"+argv[1+4]+"_mu_"+argv[1+5]+"_pLambLy_2DDyn.txt");
	k.writeNekArray(peig1_1Ly,fname+"_FLType_up_p2_"+argv[1+4]+"_mu_"+argv[1+5]+"_peig1_1Ly_2DDyn.txt");
	k.writeNekArray(peig1_2Ly,fname+"_FLType_up_p2_"+argv[1+4]+"_mu_"+argv[1+5]+"_peig1_2Ly_2DDyn.txt");
	k.writeNekArray(pNBspl,fname+"_FLType_up_p2_"+argv[1+4]+"_mu_"+argv[1+5]+"_pNBspl_2DDyn.txt");
	
	//maximum Error and AverageError
	NekDouble dGMaxEr,dGAvgEr;
	NekDouble SMaxEr,SAvgEr;
	dGMaxEr =-1.0; dGAvgEr=0.0;
	SMaxEr =-1.0; SAvgEr=0.0;
	for (int i =0; i < Ny ; i++)
	{
		NekDouble dGEr = std::abs(pVLy[i]-pP[i]);
		NekDouble SEr= std::abs(pVLy[i]-pS[i]);
		dGMaxEr = std::max(dGEr,dGMaxEr);
		SMaxEr = std::max(SEr,SMaxEr);
		dGAvgEr +=dGEr;
		SAvgEr +=SEr;
	}
	cout << "dG\t" << dGAvgEr/totPts << "\t" << dGMaxEr<< endl;
	cout << "S\t" << SAvgEr/totPts << "\t" << SMaxEr << endl;
	return 0;
}

