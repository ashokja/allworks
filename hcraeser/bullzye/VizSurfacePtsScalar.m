function [pts] = VizSurfacePtsScalar(xcheb,ycheb,zcheb,UAtCheb,isoValue)

    [X,Y,Z] = meshgrid(xcheb,ycheb,zcheb);
    normAtCheb = UAtCheb;
    normAtCheb = permute(normAtCheb,[2,1,3]);
    
    [~,pts] = isosurface(X,Y,Z,normAtCheb,isoValue);
end