function [p] = Plot3DCurve(coeff,curveSampledata,C)
    switch nargin
        case 1
            curveSampledata = 20;
            C = '-r*';
        case 2
            C = '-r*';
    end
    cdata = linspace(-1,1,curveSampledata);
    
    cxdata = B_eval(coeff(1,:),cdata); 
    cydata = B_eval(coeff(2,:),cdata);
    czdata = B_eval(coeff(3,:),cdata);
       
    p = plot3(cxdata,cydata,czdata,C); hold on;
    plotBoundingBox();
end
