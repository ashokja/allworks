clear;
[X,Y] = meshgrid( [-1:.1:1],[-1:.1:1] );
Z= X+Y*1i;

r1 = (Z-(0.74 +0.35i)).*(Z-(0.68 -0.59i))...
    .*(Z-(-0.11 -0.72i)).*(Z-(-0.58+0.64i))...
    .*(Z-(0.51 -0.27i)).*(Z-(-0.12+0.84i)).^2;

r2 = (Z-(0.74 +0.35i)).*(Z-(0.68 -0.59i))...
    .*(Z-(-0.11 -0.72i)).*(Z-(-0.58+0.64i))...
    .*(Z-(0.51 -0.27i));

r3 = -(Z-(0.74 +0.35i)).*(Z-(0.11 -0.11i)).^2 ...
    .*(Z-(-0.11 +0.72i)).*(Z-(-0.58+0.64i))...
    .*(Z-(0.51 -0.27i));


% plot of r1
u1 = real(r1);
v1 = -1*imag(r1);
figure(2),
FindVorticityInTriMesh(X,Y,u1,v1);

u2 = real(r2);
v2 = -1*imag(r2);
figure(4),
FindVorticityInTriMesh(X,Y,u2,v2);

u3 = real(r3);
v3 = -1*imag(r3);
figure(5),
FindVorticityInTriMesh(X,Y,u3,v3);
%         mag = sqrt(u1.*u1 + v1.*v1);
%         u1 = u1./mag;
%         v1 = v1./mag;
%         subplot(2,2,1), quiver(X,Y,u1,v1);
% 
% 
%         % plot of r2
%         u2 = real(r2);
%         v2 = -1*imag(r2);
% 
%         mag = sqrt(u2.*u2 + v2.*v2);
%         u2 = u2./mag;
%         v2 = v2./mag;
%         subplot(2,2,2), quiver(X,Y,u2,v2);
% 
% 
%         % plot of r3
%         u3 = real(r3);
%         v3 = -1*imag(r3);
% 
%         mag = sqrt(u3.*u3 + v3.*v3);
%         u3 = u3./mag;
%         v3 = v3./mag;
%         subplot(2,2,3), quiver(X,Y,u3,v3);
