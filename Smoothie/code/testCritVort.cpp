#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC2D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh2D.h"

#include <iomanip>

#define MAX_ITER 10000
#define MAX_ITER_NI 100
#define TOLERENCE_S 1e-16
#define TOLERENCE_F 1e-7
#define TOLERENCE_P 1e-14
#define TOLERENCE_A 1e-14
#define TOLERENCE_NN 1e-8
#define ALPHAMAX 1.0

using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>

using namespace std;

void calVorticity(int nq, Array<OneD,NekDouble> &udy_phys, Array<OneD,NekDouble> &vdx_phys,
    Array<OneD,NekDouble> &Vz )
{
    Vmath::Vsub(nq, udy_phys,1, vdx_phys,1,Vz ,1);
    return;
}


int main(int argc, char* argv[])
{
	if (argc != 6)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg 0/1"<<endl<<"\t 0 for writing data"<< endl<<"\t 1 for reading data." << endl;
		cout << "3rd Polynomial order of SIAC filter" << endl;
		cout << "4th Scaling of SICA filter" << endl;
		cout << "5nd arg Mesh resolution you want to write to." << endl;
		
		//cout << "3nd arg polynomial degree filter you want to apply" << endl;
		//cout << "4rd arg meshscaling you want to use." << endl;
	//	cout << "4th Angle of SIAC derivative in deg -90 to 90 allowed" << endl;
		return 0;
	}

	argc = 2;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM2D = new HandleNekMesh2D(vSession);
	vector<string> var = vSession->GetVariables();
	string fname = vSession->GetSessionName();

//	HNM2D->LoadData( fname+ ".chk",var);

	HNM2D->LoadMesh(var[0]);
	HNM2D->LoadMesh(var[1]);

	
	int tNquadPts = HNM2D->m_expansions[0]->GetTotPoints();
	cout << "fPhys:" << tNquadPts<< endl;
	cout << "fCoeffs:" << HNM2D->m_expansions[0]->GetNcoeffs()<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);
	Array<OneD,NekDouble> u(tNquadPts);
	Array<OneD,NekDouble> v(tNquadPts);

	
	switch( HNM2D->m_expansions[0]->GetCoordim(0) )
	{
		case 2:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM2D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}

	NektarBaseClass k;
	if( 0 == atoi(argv[2]) )
	{	
		k.writeNekArray(xc0,fname+"_xc0."+"txt");
		k.writeNekArray(xc1,fname+"_xc1."+"txt");
		return 0;
	}
	else
	{
		k.readNekArray(u, fname+"_u.txt");
		k.readNekArray(v, fname+"_v.txt");
		//k.printNekArray(v,0);
	}

	// Map the sample points to coeffeicents.
	HNM2D->m_expansions[0]->FwdTrans_IterPerExp( u,HNM2D->m_expansions[0]->UpdateCoeffs());
	HNM2D->m_expansions[0]->BwdTrans_IterPerExp( HNM2D->m_expansions[0]->GetCoeffs(),
			HNM2D->m_expansions[0]->UpdatePhys());
	
	HNM2D->m_expansions[1]->FwdTrans( v,HNM2D->m_expansions[1]->UpdateCoeffs());
	HNM2D->m_expansions[1]->BwdTrans( HNM2D->m_expansions[1]->GetCoeffs(),
			HNM2D->m_expansions[1]->UpdatePhys());

	// Map the sample Get physical points.
	Array<OneD,NekDouble> u_DG = HNM2D->m_expansions[0]->GetPhys();	
	Array<OneD,NekDouble> v_DG = HNM2D->m_expansions[1]->GetPhys();

	// load data in to m_Arrays.
	HNM2D-> m_Arrays.push_back(u_DG);
	HNM2D-> m_Arrays.push_back(v_DG);

	HNM2D->LoadExpListIntoRTree();
	//TODO : set the parameters.
	SmoothieSIAC2D smD(SIACUtilities::eSYM_DER_2kp1_1SIDED_4kp1, HNM2D, atoi(argv[3]), atof(argv[4]), 1);
	SmoothieSIAC2D sm(SIACUtilities::eSYM_DER_2kp1_1SIDED_4kp1, HNM2D, atoi(argv[3]), atof(argv[4]));

	// Derivatives of points.
	Array<OneD,NekDouble> ux_DG(tNquadPts), uy_DG(tNquadPts), vx_DG(tNquadPts), vy_DG(tNquadPts);
	HNM2D->m_expansions[0]->PhysDeriv( u_DG, ux_DG, uy_DG);
	HNM2D->m_expansions[1]->PhysDeriv( v_DG, vx_DG, vy_DG);
	Array<OneD,NekDouble> directionX(3,0.0), directionY(3,0.0);
	directionX[0] = 1.0;
	directionY[1] =1.0;

// The code below is sampling the data field at various points.
	int Nres = atoi(argv[5]);
	int Nx = Nres, Ny = Nres;
	int totPts = Nx*Ny;
	NekDouble sx = 1.0/(Nx-1.0), sy= 1.0/(Ny-1.0);
	vector< NekDouble> Px(totPts), Py(totPts);
	vector< NekDouble> UX(totPts), UY(totPts), U(totPts);
	vector< NekDouble> VX(totPts), VY(totPts), V(totPts);
	
	vector< NekDouble> sUX(totPts), sUY(totPts), sU(totPts);
	vector< NekDouble> sVX(totPts), sVY(totPts), sV(totPts);
	Array<OneD,NekDouble> coord(3,0.0);
	NekDouble valY=0.0, valZ=0.0;
	for(int i =0; i <Nx; i++)
	{
		for(int j=0; j< Ny; j++)
		{
			int index = i*Nx+j;
			Px[index] = i*sx; 
			Py[index] = j*sy;
			// find the index for the point.
			// el_phys_array
			// Form the local expansion
			// at i,j
			coord[0] = i*sx;
			coord[1] = j*sy;
			int elId = HNM2D->m_expansions[0]->GetExpIndex(coord);
			int phys_offset = HNM2D->m_expansions[0]->GetPhys_Offset(elId);
			LocalRegions::ExpansionSharedPtr lexp = HNM2D->m_expansions[0]->GetExp( elId );
			
			Array<OneD,NekDouble> el_u_phys = u_DG.CreateWithOffset( u_DG, phys_offset );
			Array<OneD,NekDouble> el_v_phys = v_DG.CreateWithOffset( v_DG, phys_offset );
			Array<OneD,NekDouble> el_ux_phys = uy_DG.CreateWithOffset( ux_DG, phys_offset );
			Array<OneD,NekDouble> el_uy_phys = uy_DG.CreateWithOffset( uy_DG, phys_offset );
			Array<OneD,NekDouble> el_vx_phys = vx_DG.CreateWithOffset( vx_DG, phys_offset );
			Array<OneD,NekDouble> el_vy_phys = vx_DG.CreateWithOffset( vy_DG, phys_offset );

			U[index] = lexp->PhysEvaluate( coord, el_u_phys);
			UX[index] = lexp->PhysEvaluate( coord, el_ux_phys);
			UY[index] = lexp->PhysEvaluate( coord, el_uy_phys);
			V[index] = lexp->PhysEvaluate( coord, el_v_phys);
			VX[index] = lexp->PhysEvaluate( coord, el_vx_phys);
			VY[index] = lexp->PhysEvaluate( coord, el_vy_phys);

			// SIAC results.
			sm.EvaluateAt(Px[index],Py[index], 0.0, sU[index], valY,valZ,directionX,atof(argv[4]),0);
			smD.EvaluateAt(Px[index],Py[index], 0.0, sUX[index], valY,valZ,directionX,atof(argv[4]),0);
			smD.EvaluateAt(Px[index],Py[index], 0.0, sUY[index], valY,valZ,directionY,atof(argv[4]),0);

			sm.EvaluateAt(Px[index],Py[index], 0.0, sV[index], valY,valZ,directionY,atof(argv[4]),1);
			smD.EvaluateAt(Px[index],Py[index], 0.0, sVX[index], valY,valZ,directionX,atof(argv[4]),1);
			smD.EvaluateAt(Px[index],Py[index], 0.0, sVY[index], valY,valZ,directionY,atof(argv[4]),1);
		}
	}

	HNM2D->writeNekArray(Px, fname+"_"+argv[5]+"_SampleDeriv_x.txt");	
	HNM2D->writeNekArray(Py, fname+"_"+argv[5]+"_SampleDeriv_y.txt");	
	HNM2D->writeNekArray(U, fname+"_"+argv[5]+"_SampleDeriv_u.txt");	
	HNM2D->writeNekArray(UX, fname+"_"+argv[5]+"_SampleDeriv_ux.txt");	
	HNM2D->writeNekArray(UY, fname+"_"+argv[5]+"_SampleDeriv_uy.txt");	
	HNM2D->writeNekArray(V, fname+"_"+argv[5]+"_SampleDeriv_v.txt");	
	HNM2D->writeNekArray(VX, fname+"_"+argv[5]+"_SampleDeriv_vx.txt");	
	HNM2D->writeNekArray(VY, fname+"_"+argv[5]+"_SampleDeriv_vy.txt");	
	
	HNM2D->writeNekArray(sU, fname+"_"+argv[5]+"_SampleDeriv_su.txt");	
	HNM2D->writeNekArray(sUX, fname+"_"+argv[5]+"_SampleDeriv_sux.txt");	
	HNM2D->writeNekArray(sUY, fname+"_"+argv[5]+"_SampleDeriv_suy.txt");	
	HNM2D->writeNekArray(sV, fname+"_"+argv[5]+"_SampleDeriv_sv.txt");	
	HNM2D->writeNekArray(sVX, fname+"_"+argv[5]+"_SampleDeriv_svx.txt");	
	HNM2D->writeNekArray(sVY, fname+"_"+argv[5]+"_SampleDeriv_svy.txt");	

	cout<< "written all files" << endl;	
	return 0;
}
