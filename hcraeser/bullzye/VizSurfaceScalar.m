function [fvs,pts] = VizSurfaceScalar(xcheb,ycheb,zcheb,UAtCheb,isoValue,color)
    switch nargin
        case 5
            color = 'red';
    end
    [X,Y,Z] = meshgrid(xcheb,ycheb,zcheb);
    normAtCheb = UAtCheb;
    normAtCheb = permute(normAtCheb,[2,1,3]);
    
    [fvs,pts] = isosurface(X,Y,Z,normAtCheb,isoValue);
    p = patch(isosurface(X,Y,Z,normAtCheb,isoValue));
    isonormals(X,Y,Z,normAtCheb,p)
    p.FaceColor = color;
    p.EdgeColor = 'none';
%'FaceAlpha',.3
    p.FaceAlpha = .3;
    daspect([1,1,1])
    view(3); %axis tight
    camlight 
    lighting gouraud 
    hold on;
end