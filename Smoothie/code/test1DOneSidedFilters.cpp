#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <LibUtilities/Memory/NekMemoryManager.hpp>
#include <LibUtilities/BasicUtils/SessionReader.h>
#include "GeneralBSplines.h"
#include "CentralBSplines.h"
#include "SymmetricSIAC.h"
#include "SmoothieSIAC1D.h"
#include "HandleNekMesh.h"
#include "HandleNekMesh1D.h"
#include "OneSidedSIAC.h"

#include <iomanip> // std::setprecision
using namespace SIACUtilities;

//#include <LibUtilities/Communication/Comm.h>
//#include <MultiRegions/ContField1D.h>
//#include <SpatialDomains/MeshGraph1D.h>
#define TOLERENCE_VIZ 1e-6

using namespace std;

void printNekArray(Array<OneD,NekDouble> &ar);
void printNekArray(Array<OneD,NekDouble> &ar,int del); 

int main(int argc, char* argv[])
{
	if (argc != 7)
	{
		cout << "please enter the following information" << endl;
		cout << "1st arg xml file." << endl;
		cout << "2nd arg conditions.xml file." << endl;
		cout << "3nd arg polynomial degree filter you want to apply" << endl;
		cout << "4rd arg Type of Filter to use." << endl;
		cout << "5th arg Res. " << endl;
		cout << "6th arg scaling " << endl;
		return 0;
	}

	argc = 3;
	LibUtilities::SessionReaderSharedPtr vSession
				= LibUtilities::SessionReader::CreateInstance(argc,argv);
	HandleNekMesh* HNM1D = new HandleNekMesh1D(vSession);
	vector<string> var = vSession->GetVariables();

	HNM1D->LoadMesh(var[0]);

	string fname = vSession->GetSessionName();
	int tNquadPts = HNM1D->m_expansions[0]->GetTotPoints();
	cout << "fc:" << tNquadPts<< endl;
	Array<OneD,NekDouble> xc0(tNquadPts);
	Array<OneD,NekDouble> xc1(tNquadPts);
	Array<OneD,NekDouble> xc2(tNquadPts);

	
	switch( HNM1D->m_expansions[0]->GetCoordim(0) )
	{
		case 1:
			HNM1D->m_expansions[0]->GetCoords(xc0);
			Vmath::Zero(tNquadPts,&xc1[0],1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 2:
			HNM1D->m_expansions[0]->GetCoords(xc0,xc1);
			Vmath::Zero(tNquadPts,&xc2[0],1);
			break;
		case 3:
			HNM1D->m_expansions[0]->GetCoords(xc0,xc1,xc2);
			break;
		default:
			assert( false && "looks dim not taken into account");
			cout << "opps did not plan for this" << endl;
	}
        // Define forcing function for first variable defined in file
	Array<OneD,NekDouble> fce,sce,ece, temp0, temp1;
	fce = Array<OneD,NekDouble>(tNquadPts);
	sce = Array<OneD,NekDouble>(tNquadPts);
	temp0 = Array<OneD,NekDouble>(tNquadPts);
	temp1 = Array<OneD,NekDouble>(tNquadPts);

//	LibUtilities::EquationSharedPtr ffunc
//					= vSession->GetFunction("ExactSolution", 0); 
//	ffunc->Evaluate(xc0,xc1,xc2, fce);

	for (int i=0;  i< tNquadPts; i++)
	{
		fce[i] = std::cos(2.0*M_PI*(xc0[i]));
		//fce[i] = std::cos(2.0*M_PI*(xc0[i]))*std::cos(2.0*M_PI*(xc1[i]));
	}
	
	

	HNM1D->m_expansions[0]->FwdTrans(fce,HNM1D->m_expansions[0]->UpdateCoeffs() );
	HNM1D->m_expansions[0]->BwdTrans( HNM1D->m_expansions[0]->GetCoeffs(),
										HNM1D->m_expansions[0]->UpdatePhys());
	 ece = HNM1D->m_expansions[0]->GetPhys();
	Array<OneD,NekDouble> ece_Coeffs =  HNM1D->m_expansions[0]->GetCoeffs();


	HNM1D->m_Arrays.push_back(ece);
	//HNM1D->LoadExpListIntoRTree();
	

	//NekDouble scaling = HNM1D->GetMeshLargestEdgeLength();
	NekDouble scaling = atof(argv[6]);	

// Evaluate on a new equal space grid mesh.
	int gPts = atoi(argv[5]);
	int totPts = gPts;
	int Nx = gPts;
	NekDouble sx = 1.0/(Nx-1.0);


	SIACUtilities::FilterType filter = SIACUtilities::eSYM_2kp1_1SIDED_2kp1;
	if (atoi(argv[4]) ==1 ) // 2kp1
	{
		filter = SIACUtilities::eSYM_2kp1_1SIDED_2kp1;	
	}else if (atoi(argv[4]) ==2 ) // 4kp1
	{
		filter = SIACUtilities::eSYM_2kp1_1SIDED_4kp1;	
	}else if (atoi(argv[4]) ==3 ) // Xli
	{
		filter = SIACUtilities::eSYM_2kp1_1SIDED_2kp2;	
	}
	SmoothieSIAC1D sm(filter , HNM1D, atoi(argv[3]), scaling ); 


	NekDouble valX,valY,valZ;
	vector<NekDouble> pos_x; pos_x.clear();
	vector<NekDouble> values_x; values_x.clear();

	Array<OneD,NekDouble> direction(3,0.0), coord(3,0.0);
	direction[0] = 1.0;

	Array<OneD,NekDouble> pX(totPts), pV(totPts), pP(totPts), pS(totPts);
	for(int j=0; j< Nx; j++)
	{
		int index = j;
		pX[index] = j*sx;
		pV[index] = std::cos(2.0*M_PI*(pX[index] ) );

//		sm.EvaluateAt(pX[index],0.0,0.0,pS[index],valY,valZ,direction, scaling,0);
		sm.EvaluateAt(pX[index],0.0,0.0,pS[index],valY,valZ);

		coord[0] = pX[index]; coord[1] = 0.0; coord[2] = 0.0;
		LocalRegions::ExpansionSharedPtr lexp = HNM1D->m_expansions[0]->GetExp(coord);

		int elId = HNM1D->m_expansions[0]->GetExpIndex(coord);
		int coeffOffset = HNM1D->m_expansions[0]->GetPhys_Offset(elId);
		const Array<OneD,NekDouble> el_phys = ece.CreateWithOffset(
									ece, coeffOffset);

		pP[index] = lexp->PhysEvaluate(coord,el_phys);
		cout << pV[index] << "\t" << pP[index] << "\t" << pS[index] << endl;
	}

	NektarBaseClass k;
	k.writeNekArray(pX,fname+"_"+argv[4]+"_"+argv[5]+"_pX_1D.txt");
	k.writeNekArray(pV,fname+"_"+argv[4]+"_"+argv[5]+"_pV_1D.txt");
	k.writeNekArray(pP,fname+"_"+argv[4]+"_"+argv[5]+"_pP_1D.txt");
	k.writeNekArray(pS,fname+"_"+argv[4]+"_"+argv[5]+"_pS_1D.txt");
	return 0;
}

