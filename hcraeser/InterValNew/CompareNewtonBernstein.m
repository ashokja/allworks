function [result,lcount,splits] = CompareNewtonBernstein(i,coeff,data,Y,n)

    switch nargin
        case 0,
                i = 'c';
                coeff = [-1,2,0,-2.5,-1,5,-1,-1,-1];
    end

    X = [-1,1];
    switch i
        case 'c',
            n = max(size(coeff))-1;
            data = (-1:.1:1);
            Y = B_eval(n,coeff,data);
            X = [-1,1];
    end
        
    %plot(Y);

    %mass matrix
    for i = 0:n
        MassB(:,i+1) = B(n,i,data);
    end
    coeffB = MassB\Y';
    B_Y = B_eval(n,coeffB,data);
    
    for i = 0:n
        MassM(:,i+1) = M(n,i,data);
    end
    coeffM = MassM\Y';
    M_Y = M_eval(n,coeffM,data);
    
    figure(2);
    plot(data,B_Y,'rx'); hold on;grid on;
    plot(data,M_Y); hold off;
    
    
    tic
    fprintf('Bernstein root:');
    [lcount(1),splits(1)] = solveBernNewton1D(@evalBInterval,coeffB',X);
    result(1) = toc;
    tic
    fprintf('Monomial Serial root:');
    [lcount(2),splits(2)] = solveBernNewton1D(@MSerialBasis,coeffM',X);
    result(2) = toc;
    
%     tic
%     fprintf('Monomial root:');
%     solveBernNewton1D(@MBasis,coeffM',X)
%     result(3) = toc;
%     
%     function X1 = MBasis(i,coeff,X)
%         n = max(size(coeff));
%         if i ==1
%            X1 = M_evalX(n,coeff,X);
%         else
%             X1 =DM_evalX(n,coeff,X);
%         end
%     end
    

    function X1 = M_evalX(n,coeff,X)
        X1 = zero(size(X));
        for k = 0:n
            r = op('*',[coeff(k+1),coeff(k+1)],MX(n,k,X) );
            X1 = X1+r;
        end
    end
    
    function rest = M_eval(n,coeff,dataIn)
        rest = zeros(size(dataIn));
        for k = 0:n
            r = coeff(k+1)*M(n,k,dataIn);
            rest = rest+r;
        end
    end

    function rest = B_eval(n,coeff,dataIn)
       count =1;
       rest = zeros(size(dataIn));
       for k = 0:n
           r = coeff(count)*B(n,k,dataIn);
           rest = rest  + r;
           count= count +1;          
       end
    end
    
    function X1 = MX(n,k,data)
        X1 = M(n,k,data);
    end

    function [res] = M(~,k,data)
        if k<0
            res = zeros(size(data));
            return;
        end
        data = (data+1)/2;
        res = data.^k;
    end

    function [res] = B(n,k,data)
        % assumed data varies from -1 to 1
        if n<k || k<0
            res = zeros(size(data));
            return
        end
        data = (data+1)/2;
        res = nchoosek(n,k)*((data.^k).*((1-data).^(n-k)));
    end

   
    function [res] = B_der(n,k,data)
        res = n*(B(n-1,k-1,data) - B(n-1,k,data))/2;
    end
    
    function [rest] = B_der_eval(n1,coeff,dataIn)
       count =1;
       [a,b] = size(dataIn);
       rest = zeros(a,b);
       for k1 = 0:n1
           rest = rest + coeff(count)*B_der(n1,k1,dataIn);
           count= count +1;          
       end
    end

end